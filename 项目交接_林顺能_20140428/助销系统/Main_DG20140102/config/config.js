{
 "id"             : "0000",
 "menus"          : [
  {
   "id"             : "0000",
   "menus"          : [ ],
   "icon"           : "http://220.249.190.132:21504/images/buycard_1.png",
   "name"           : "买号码",
   "package"        : "com.sunnada.haoka.activity",
   "actionType"     : "activity",
   "actionUrl"      : "com.sunnada.haoka.activity.FjMobileHaokaActivity"
  },
  {
   "id"             : "0000",
   "menus"          : [ ],
   "icon"           : "http://220.249.190.132:21504/images/recharge_2.png",
   "name"           : "交话费",
   "package"        : "com.sunnada.recharge",
   "actionType"     : "activity",
   "actionUrl"      : "com.sunnada.recharge.FjMobileRechargeActivity"
  },
  {
   "id"             : "0000",
   "menus"          : [
    {
     "id"             : "0000",
     "menus"          : [ ],
     "icon"           : "http://220.249.190.132:21504/images/password_update.png",
     "name"           : "密码修改",
     "package"        : "com.sunnada.selfservices",
     "actionType"     : "activity",
     "actionUrl"      : "com.sunnada.selfservices.ModPwdActivity"
    },
    {
     "id"             : "0000",
     "menus"          : [ ],
     "icon"           : "http://220.249.190.132:21504/images/check_account.png",
     "name"           : "对账服务",
     "package"        : "com.sunnada.selfservices",
     "actionType"     : "activity",
     "actionUrl"      : "com.sunnada.selfservices.CheckAccountActivity"
    },
    {
     "id"             : "0000",
     "menus"          : [ ],
     "icon"           : "http://220.249.190.132:21504/images/wdxx.png",
     "name"           : "网点信息",
     "package"        : "com.sunnada.selfservices",
     "actionType"     : "activity",
     "actionUrl"      : "com.sunnada.selfservices.AgentInfoActivity"
    }
   ],
   "icon"           : "http://220.249.190.132:21504/images/selfservice_3.png",
   "name"           : "自服务",
   "package"        : "com.sunnada.selfservices",
   "actionType"     : "activity",
   "actionUrl"      : "com.sunnada.selfservices.AgentInfoActivity"
  },
  {
   "id"             : "0000",
   "menus"          : [ ],
   "icon"           : "http://220.249.190.132:21504/images/system_uupdate_.png",
   "name"           : "系统更新",
   "package"        : "com.sunnada.baseframe.activity",
   "actionType"     : "activity",
   "actionUrl"      : "com.sunnada.baseframe.activity.ApkUpdateAcitivty"
  }
 ],
 "m3s"            : {
  "2"              : {
   "application"    : {
    "name"           : "CPU-M66-APP-STM32CV1.0-20130502_1130_CMCC",
    "minVersion"     : "CPU-M66-APP-STM32CV1.0-20130502_1130_CMCC",
    "tips"           : "升级提示信息",
    "url"            : "http://220.249.190.132:21504/images/CPU-M66-APP-STM32CV1.0-20130502_1130_CMCC.bin",
    "version"        : "CPU-M66-APP-STM32CV1.0-20130502_1130_CMCC",
    "size"           : "2000000"
   },
   "driver"         : {
    "name"           : "CPU-M66-DRV-STM32CV1.0-20130517_1130_CMCC",
    "minVersion"     : "CPU-M66-DRV-STM32CV1.0-20130517_1130_CMCC",
    "tips"           : "升级提示信息",
    "url"            : "http://220.249.190.132:21504/images/CPU-M66-DRV-STM32CV1.0-20130517_1130_CMCC.bin",
    "version"        : "CPU-M66-DRV-STM32CV1.0-20130517_1130_CMCC",
    "size"           : "2000000"
   }
  }
 },
 "apkVersions"    : [
  {
   "icon"           : "http://220.249.190.132:21504/images/icon_card.png",
   "psams"          : [
    "3801800000000043",
    "3801800000000045",
    "3801800000000044",
    "3801800000000046"
   ],
   "name"           : "买号码",
   "minVersion"     : "1.0",
   "package"        : "com.sunnada.haoka.activity",
   "tips"           : "升级提示信息",
   "url"            : "http://220.249.190.132:21504/images/Android-HaoKa_FJ_MobileMini_M66_20130514_1.0.02-03.apk",
   "version"        : "1.0",
   "size"           : "1563492"
  },
  {
   "icon"           : "http://220.249.190.132:21504/images/icon_recharge.png",
   "psams"          : [
    "3801800000000043",
    "3801800000000045",
    "3801800000000044",
    "3801800000000046"
   ],
   "name"           : "交话费",
   "minVersion"     : "1.0",
   "package"        : "com.sunnada.recharge",
   "tips"           : "升级提示信息",
   "url"            : "http://220.249.190.132:21504/images/Android-Recharge_FJ_MobileMini_M66_20130509_1.0.01-01.apk",
   "version"        : "1.0",
   "size"           : "592428"
  },
  {
   "icon"           : "http://220.249.190.132:21504/images/icon_recharge.png",
   "name"           : "自服务",
   "minVersion"     : "1.0",
   "package"        : "com.sunnada.selfservices",
   "tips"           : "升级提示信息",
   "url"            : "http://220.249.190.132:21504/images/Android-SelfService_FJ_MobileMini_M66_20130517_1.0.02-04.apk",
   "version"        : "1.0",
   "size"           : "599117"
  },
  {
   "icon"           : "http://220.249.190.132:21504/images/icon_recharge.png",
   "psams"          : [
    "3801800000000043",
    "3801800000000045",
    "3801800000000044",
    "3801800000000046"
   ],
   "name"           : "大厅程序",
   "minVersion"     : "1.0",
   "package"        : "com.sunnada.baseframe.activity",
   "tips"           : "升级提示信息",
   "url"            : "http://220.249.190.132:21504/images/Android-BaseFrame_FJ_MobileMini_M66_20130513_1.0.02-02.apk",
   "version"        : "1.0",
   "size"           : "1788869"
  }
 ],
 "name"           : "三元达基础框架",
 "dependentFiles" : [
  {
   "timestamp"      : "1369204182191",
   "dir"            : "/mini/font",
   "url"            : "http://220.249.190.132:21504/images/HCBCR18b2u_mob.dat"
  },
  {
   "timestamp"      : "1369204182192",
   "dir"            : "/mini/font",
   "url"            : "http://220.249.190.132:21504/images/ScanBcr_mob.cfg"
  }
 ]
}
