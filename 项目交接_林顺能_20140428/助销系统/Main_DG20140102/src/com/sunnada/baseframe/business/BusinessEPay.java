package com.sunnada.baseframe.business;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.database.ErrorDealAdapter;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.StringUtil;

import sunnada.jni.Commcenter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

// 购卡充值  协议类
public class BusinessEPay extends BusinessCharge
{
	public boolean		mIsDoEPay 				= false;
	public byte			mShowScreen				= 0x01;						// 0x00-显示是否立即充值界面	0x01-不显示立即充值界面
	public byte			mDirectRecharge 		= 0x00;

	public String		mEcardFile				= Statics.PATH_CONFIG + "/ecardinfo";
	private Handler		mHandler;
	
	public byte[] 		mEcardNo 				= new byte[20];
	public byte[] 		mEcardPswd 				= new byte[20];
	public byte[] 		mEcardEnd				= new byte[8];
	public byte[]		mErrTransType 			= new byte[1];
	public byte[]		mErrEcardIdx 			= new byte[1];
	
	public BusinessEPay(Context context, IEquipmentService equipmentService, ISocketService socketService, IDataBaseService dataBaseService, 
			Handler handler) 
	{
		super(context, equipmentService, socketService, dataBaseService, handler);
		TITLE_STR 	= "充值业务办理提醒";
		TAG			= "充值: ";
		
		mHandler		 = handler;
		mErrEcardIdx[0]  = 0x00;
		mErrTransType[0] = -1;
	}
	
	// 获取卡密过程
	public void doEPayStep1() 
	{
		if(mIsDoEPay == false) 
		{
			mIsDoEPay = true;
			// 获取流水号
			DialogUtil.showProgress("正在获取交易流水号...");
			if(bll0201_0206(0x06)) 
			{
				// 获取卡密请求
				DialogUtil.showProgress("正在获取卡密...");
				if(bll0207(Statics.MSG_GET_ELEC_FAIL)) 
				{
					// 卡密确认
					DialogUtil.showProgress("正在卡密确认...");
					for(int i=0; i<3; i++) 
					{
						// 卡密信息确认成功
						if(bll0208(i, Statics.MSG_GET_ELEC_FAIL)) 
						{
							// 立即发起充值请求
							DialogUtil.showProgress("正在发起充值请求...");
							if(bll0209(Statics.MSG_RECHARGE_FAIL)) 
							{
								// 充值成功确认
								DialogUtil.showProgress("正在发起充值确认...");
								bll020A();
								// 显示凭条打印界面
								mHandler.sendEmptyMessage(Statics.MSG_RECHARGE_OK);
							}
							break;
						}
					}
				}
			}
			mIsDoEPay = false;
			DialogUtil.closeProgress();
		}
	}
	
	/*
	// 显示卡密界面
	// szType: 		0x01-正常情况下的显示 0x02-异常情况下的显示
	// szTransType:	业务类型
	// strContent:	附加内容
	public void showEcardInfoScreen(int szType, byte szTransType, String strContent)
	{
		String msgStr = getEcardInfo(szTransType).toString();
		if(szType == 0x01) 
		{
			msgStr += strContent;
			DialogUtil.MsgBox(TITLE_STR, 
					msgStr, 
					"确定", 
					Statics.MSG_DIRECT_RECHARGE,
					"取消", 
					Statics.MSG_DIRECT_RECHARGE_CANCEL, 
					Statics.MSG_DIRECT_RECHARGE_CANCEL, 
					mHandler);
		}
		// 异常情况下, 卡密信息的打印
		else if(szType == 0x02) 
		{
			msgStr = strContent + msgStr;
			DialogUtil.MsgBox(TITLE_STR, 
					msgStr, 
					"确定", 
					Statics.MSG_PRINT_LAST_EPAY,
					"取消", 
					Statics.MSG_PRINT_LAST_EPAY_CANCEL, 
					Statics.MSG_PRINT_LAST_EPAY_CANCEL, 
					mHandler);
		}
	}
	*/
	
	/*
	// 打印至屏幕
	private void showRechargeInfoScreen() 
	{
		DialogUtil.showMessageDialog(TITLE_STR, 
				getEpayInfo().toString(), 
				"确定", 
				Statics.MSG_PRINT,
				"取消", 
				Statics.MSG_PRINT_CANCEL,
				mHandler);
	}
	*/
	
	// 接受交易类型存储，需求需要
	public String backtype()
	{
		String typename = "";
		switch(mTranceType)
		{
			case 0x01:
				typename = "手机";
				break;
			case 0x02:
				typename = "固话";
				break;
			case 0x0A:
				typename = "小灵通 ";
				break;
			case 0x0B:
				typename = "宽带ADSL";
				break;
			case 0x0C:
				typename = "宽带LAN";
				break;
			default: 
				break;
		}
		return typename+"充值";
	}
	
	// 获取卡密
	private boolean bll0207(int msgID) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("07");
		sendPackage.setId("02");
		
		Buffer buffer = new Buffer();
		buffer.add("3F00"); 								// 数据域
		buffer.add("0000"); 								// CRC先设为0
		buffer.add((byte)mTranceType); 						// 业务类型
		buffer.add("1E"); 									// 流水长度
		buffer.add(encryptSerialNumber(mStrBillNum.getBytes())); // 加密流水号
		buffer.add("00"); 									// 0: 扣款状态 1: 查询状态即扣款无响应时的查询
		buffer.add(mChargeMoney); 							// 充值金额
		buffer.add((byte)mStrTelNum.length()); 				// 帐号长度
		buffer.add(mStrTelNum.getBytes()); 					// 帐号
		
		byte[] tt = buffer.getBytes();
		byte[] bb = new byte[tt.length-4];
		System.arraycopy(tt, 4, bb, 0, tt.length-4);
		byte[] crc = Commcenter.CalcCrc16(bb, (char) 0);
		tt[2] = crc[0];
		tt[3] = crc[1];
		sendPackage.addUnit("0303", tt);
		
		// 预先记录异常
		mStrErrBill 	=  mStrBillNum; 								// 流水
		mStrErrReason 	= Commen.hax2str(new byte[] { 0x0C }); 			// 记录原因
		mStrErrType 	= Commen.hax2str(new byte[] { mTranceType }); 	// 交易类型
		mStrErrCrc 	 	= Commen.hax2str(new byte[] { crc[0], crc[1] });
		ErrorDealAdapter.setErrorInfo(context, mStrErrPsamID, mStrErrType, mStrErrBill, mStrErrReason, mStrErrCrc, "1");
		// 同时插入到数据库中
		mBusinessNum.insertData(
				App.getUserNo(), 
				mStrTelNum, 
				backtype(), 
				mStrChargeMoney, 
				"交易失败", 
				mStrErrBill,
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()), 
				mStrChargeMoney, 
				mTranceType);
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0207");
		}
		// 如果不是接收超时则更改异常交易记录的状态
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
		// 超时或者失败则立即返回
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取卡密失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.MsgBox(TITLE_STR, ss, msgID, mHandler);
			setLastknownError(ss);
			return false;
		}
		
		// 解析数据
		if(!recivePackage.containsKey("0303")) 
		{
			String ss = "获取卡密失败!\n失败原因: [服务器响应数据非法, 缺少0303命令单元]";
			DialogUtil.MsgBox(TITLE_STR, ss, msgID, mHandler);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0303");
			int offset = 0;
			int map = Commen.bytes2int(new byte[]{unit[offset], unit[offset+1]});
			offset += 2;
			// 请求信息CRC域
			if ((map & 0x01) != 0) 
			{
				offset += 2;
			}
			// 交易流水号
			if ((map & 0x02) != 0) 
			{
				offset++;
				int len = unit[offset++]&0xFF;
				offset += len;
			}
			// 交易状态
			if ((map & 0x04) != 0) 
			{
				offset++;
			}
			// 金额
			if ((map & 0x08) != 0) 
			{
				offset += 4;
			}
			// 账号字节长度
			if ((map & 0x10) != 0) 
			{
				int len = unit[offset++]&0xFF;
				// 账号
				if ((map & 0x20) != 0) 
				{
					offset += len;
				}
			}
			// 缴费前余额
			if ((map & 0x40) != 0) 
			{
				offset += 4;
			}
			// 电子卡卡密
			if((map & 0x80) != 0) 
			{
				System.arraycopy(unit, offset, mEcardNo, 0, 20);
				offset += 20;
				System.arraycopy(unit, offset, mEcardPswd, 0, 20);
				offset += 20;
				System.arraycopy(unit, offset, mEcardEnd, 0, 8);
				offset += 8;
			}
		}
		
		/*
		// 是否立即充值
		if(recivePackage.containsKey("0151") == false) 
		{
			String ss = "获取卡密失败!\n失败原因: [服务器响应数据非法, 缺少0151命令单元]";
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0151");
			mShowScreen = unit[0];
			mBusinessNum.updateBussinessNum(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()), 
					"交易失败", mStrChargeMoney, mStrErrBill, mStrChargeMoney);
			return true;
		}
		*/
		mBusinessNum.updateBussinessNum(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()), 
				"交易失败", mStrChargeMoney, mStrErrBill, mStrChargeMoney);
		return true;
	}
	
	// 确认收到卡密
	private boolean bll0208(int nTimes, int msgID) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("08");
		sendPackage.setId("02");
		
		Buffer buffer = new Buffer();
		buffer.add((byte)mTranceType); 			// 业务类型
		buffer.add("1E"); 						// 流水长度
		buffer.add(mStrBillNum.getBytes()); 	// 流水号
		sendPackage.addUnit("0202", buffer.getBytes());
		
		ErrorDealAdapter.setErrorInfoState(context, 
				mStrErrPsamID, 
				mStrErrType, 
				Commen.hax2str(new byte[] {0x0D}), 
				"1", 
				mStrErrCrc, 
				mStrErrBill);
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0208", 30*1000);
		}
		// 更新异常交易记录状态
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
		// 如果超时或者失败则直接返回
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "卡密确认失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			if(nTimes == 2) 
			{
				DialogUtil.MsgBox(TITLE_STR, ss, msgID, mHandler);
			}
			return false;
		}
		Log.i(TAG, "获取卡密信息成功确认成功");
		
		// 更改交易记录状态
		mBusinessNum.updateBussinessNum(
				new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(Calendar.getInstance().getTime()), 
				"交易成功", 
				mStrChargeMoney, 
				mStrErrBill, 
				mStrChargeMoney);
		
		// 把卡密存到文件里面去
		File file = new File(mEcardFile);
		if(file.exists()) 
		{
			file.delete();
		}
		try {
			RandomAccessFile out = new RandomAccessFile(file, "rwd");
			// 业务类型
			out.write(mTranceType);
			// 充值账户
			byte[] tempNum = new byte[50];
			System.arraycopy(mStrTelNum.getBytes(), 0, tempNum, 0, mStrTelNum.length());
			out.write(tempNum, 0, 50);
			// 帐号
			out.write(mEcardNo, 		0, 20);
			out.write(mEcardPswd, 		0, 20);
			out.write(mErrEcardIdx, 	0, 1);
			out.write(mEcardEnd, 		0, 8);
			out.write(mStrBillNum.getBytes(), 0, 30);
			
			// 银联签购单信息
			byte[] temp = new byte[1];
			/*
			if(2 == postype) 
			{
				temp[0] = 0x01;
				out.write(temp);
				savePosDealInfo(out, StringUtil.formatDate(new Date(), "yyyy/MM/dd HH:mm"), 
						String.format("%.2f", (float)CommonValue.posDealMoney/100));
			}
			else 
			*/
			{
				temp[0] = 0x00;
				out.write(temp);
			}
			out.close();
			out = null;
		}
		catch(FileNotFoundException e) 
		{
			e.printStackTrace();
			Log.e(TAG, "打开卡密文件失败!");
		}
		catch(IOException e) 
		{
			e.printStackTrace();
			Log.e(TAG, "存储卡密信息失败!");
		}
		Log.i(TAG, "存储卡密信息成功");
		return true;
	}
	
	// 立即充值
	private boolean bll0209(int msgID) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("09");
		sendPackage.setId("02");
		
		// 缴费信息
		Buffer buffer = new Buffer();
		buffer.add("3F00"); 					// 数据域
		buffer.add("0000"); 					// CRC先设为0
		buffer.add((byte)mTranceType); 			// 业务类型
		buffer.add("1E"); 						// 流水长度
		buffer.add(encryptSerialNumber(mStrBillNum.getBytes())); 	// 加密流水号
		buffer.add("00"); 						// 0: 扣款状态 1: 查询状态即扣款无响应时的查询
		buffer.add(mChargeMoney); 				// 充值金额:
		buffer.add((byte)mStrTelNum.length()); 	// 帐号长度
		buffer.add(mStrTelNum.getBytes()); 		// 帐号
		byte[] tt = buffer.getBytes();
		
		byte[] bb = new byte[tt.length - 4];
		System.arraycopy(tt, 4, bb, 0, tt.length-4);
		byte[] crc = Commcenter.CalcCrc16(bb, (char) 0);
		tt[2] = crc[0];
		tt[3] = crc[1];
		sendPackage.addUnit("0301", tt);
		// 卡密信息
		buffer.clear();
		buffer.add("8000"); 					// 数据域
		buffer.add(mEcardNo);					// 卡号
		buffer.add(mEcardPswd);					// 卡密
		buffer.add(mEcardEnd);					// 有效期
		sendPackage.addUnit("0303", buffer.getBytes());
		
		// 修改异常记录状态
		mStrErrCrc 	 = Commen.hax2str(new byte[] { crc[0], crc[1] });
		mStrErrReason = Commen.hax2str(new byte[] { 0x0C });
		ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, mStrErrReason, "1", mStrErrCrc, mStrErrBill);
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0209");
		}
		// 更改异常交易记录状态
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "受理充值请求失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.MsgBox(TITLE_STR, ss, msgID, mHandler);
			setLastknownError(ss);
			return false;
		}
		
		// 解析数据
		// 交易时间
		if(recivePackage.containsKey("0204")) 
		{
			mBillTime = recivePackage.get("0204");
		}
		// 充值缴费信息
		if(recivePackage.containsKey("0301") == false)
		{
			String ss = "受理充值请求失败!\n失败原因: [服务器响应数据非法, 缺少0301命令单元]";
			DialogUtil.MsgBox(TITLE_STR, ss, msgID, mHandler);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0301");
			int offset = 0;
			int map = Commen.bytes2int(new byte[]{unit[offset], unit[offset+1]});
			offset += 2;
			// 请求信息CRC域
			if ((map & 0x01) != 0) 
			{
				offset += 2;
			}
			// 交易流水号
			if ((map & 0x02) != 0) 
			{
				offset++;
				offset += (unit[offset]&0xFF)+1;
			}
			// 交易状态
			if ((map & 0x04) != 0) 
			{
				offset++;
			}
			// 金额
			if ((map & 0x08) != 0) 
			{
				offset += 4;
			}
			// 账号字节长度
			if ((map & 0x10) != 0) 
			{
				int len = unit[offset++]&0xFF;
				// 账号
				if ((map & 0x20) != 0) 
				{
					offset += len;
				}
			}
			// 缴费前余额
			if ((map & 0x40) != 0) 
			{
				try
				{
					byte[] money = new byte[4];
					System.arraycopy(unit, offset, money, 0, 4);
					offset += 4;
					
					mStrPreMoney = String.format("%.2f", (float)Commen.bytes2int(money)/100);
					Log.i(TAG, "缴费前金额为" + mStrPreMoney);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			
			// 对应的值状态进行改变
			//ErrorInfo.setErrorInfoState(main_activity, t_psam, t_type, "0", "0", t_crc, t_no);
			// 删除卡密文件
			File file = new File(mEcardFile);
			if(file.exists()) 
			{
				file.delete();
			}
			return true;
		}
	}
	
	// 充值成功确认
	private boolean bll020A() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("0A");
		sendPackage.setId("02");
		
		Buffer buffer = new Buffer();
		buffer.add((byte)mTranceType); 			// 业务类型
		buffer.add("1E"); 						// 流水长度
		buffer.add(mStrBillNum.getBytes()); 	// 流水号
		sendPackage.addUnit("0202", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		/*
		ret = receiveOnePackage("020A", 3*1000);
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "充值确认失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}
		*/
		Log.i(TAG, "充值成功确认成功");
		return true;
	}
	
	// 上报打印失败或未打印卡密
	// 0x01-成功 0x02-失败
	public boolean bll0230(int nPrintResult) 
	{
		sendPackage = new Package();
		//sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("30");
		sendPackage.setId("02");
		
		Buffer buffer = new Buffer();
		buffer.add("0300");                 // 数据域 
		buffer.add((byte)nPrintResult);		// 打印结果
		buffer.add("1E");                   // 流水长度
		//buffer.add(mStrBillNum.getBytes());
		buffer.add(encryptSerialNumber(mStrBillNum.getBytes()));
		sendPackage.addUnit("0221", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if (ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0230");
		}
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "上报未打印卡密信息失败!";
			if (!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}
		Log.i(TAG, "上报未打印卡密信息成功");
		return true;
	}
	
	// 获取电子卡打印信息
	public StringBuffer getEcardInfo(byte szTransType, boolean isPrint) 
	{
		StringBuffer strbuf = new StringBuffer();
		
		// 如果是打印业务凭条需要打印业务类型和充值账户信息
		if(isPrint) 
		{
			strbuf.append("业务类型: ");
			Log.i(TAG, "业务类型: " + szTransType);
			switch (szTransType) 
			{
				case 0x01:
					//strbuf.append("手机充值");
					strbuf.append("充值卡密");
					break;
				case 0x02:
					//strbuf.append("固话充值");
					strbuf.append("充值卡密");
					break;
				case 0x0A:
					//strbuf.append("小灵通充值");
					strbuf.append("充值卡密");
					break;
				case 0x0B:
					//strbuf.append("宽带ADSL充值");
					strbuf.append("充值卡密");
					break;
				case 0x0C:
					//strbuf.append("宽带LAN充值");
					strbuf.append("充值卡密");
					break;
				default:
					//strbuf.append("无效类型");
					strbuf.append("充值卡密");
					break;
			}
			
			if(!StringUtil.isEmptyOrNull(mStrTelNum)) 
			{
				strbuf.append("\n充值账户: ").append(mStrTelNum);
			    //strbuf.append("\n业务提醒: 你的充值已受理,如果次日未到账,届时请用该充值卡再次进行充值操作.\n");
			}
			strbuf.append("\n");
		}
		
		strbuf.append(isPrint? "账号: ":"账        号: ")
			.append(new String(mEcardNo), 0, Commen.endofstr(mEcardNo))
			.append(isPrint? "\n密码: ":"\n密        码: ")
			.append(new String(mEcardPswd), 0, Commen.endofstr(mEcardPswd))
			.append(isPrint? "\n面值: ":"\n面        值: ")
			.append(mStrChargeMoney + "元")
		 	.append(isPrint? "\n有效期: ":"\n有  效  期: ")
		 	.append(StringUtil.converDateString(new String(mEcardEnd))).append("\n");
		return strbuf;
	}
	
	// 打印信息
	private StringBuffer getEpayInfo() 
	{
		StringBuffer strbuf = new StringBuffer();
		strbuf.append("业务类型: ");
		switch (mTranceType) 
		{
			case 0x01:
				strbuf.append("手机充值");
				break;
			case 0x02:
				strbuf.append("固话充值");
				break;
			case 0x0A:
				strbuf.append("小灵通充值");
				break;
			case 0x0B:
				strbuf.append("宽带ADSL充值");
				break;
			case 0x0C:
				strbuf.append("宽带LAN充值");
				break;
			default:
				strbuf.append("无效类型");
				break;
		}

		strbuf.append("\n充值号码: ").append(mStrTelNum)
			.append("\n充值金额: " + mStrChargeMoney + "元")
			.append("\n交易前余额: " + mStrPreMoney + "元")
			.append("\n充值时间: ").append(StringUtil.converDateString(StringUtil.encodeWithGBK(mBillTime, 0, mBillTime.length)));
		return strbuf;
	}
	
	// 打印业务凭条
	// szTransType: 传tranceType即为正常情况下的打印, 
	// 				传mErrTransType[0]则为异常情况下的打印, 即进入购卡充值流程检测到卡密文件情况下的打印.
	public void print(int type, byte szTransType) 
	{
		StringBuffer strbuf = null;
		// 打印卡密信息
		if(type == 0x01) 
		{
			strbuf = getEcardInfo(szTransType, true);
			Log.e(TAG, "打印信息: " + strbuf.toString());
			print(strbuf, null, true);
		}
		// 打印充值信息
		else 
		{
			strbuf = getEpayInfo();
			Log.e(TAG, "打印信息: " + strbuf.toString());
			print(strbuf, null, false);
		}
		
		/*
		// 打印银联交易单
		if(2 == postype) 
		{
			if(!UILogic.isNUll(mPosDealTime[0]) || !UILogic.isNUll(mPosDealMoney[0])) 
			{
				Log.e(TAG, "111");
				this.printPosDeal(mPosDealTime[0], mPosDealMoney[0]);
			}
			else 
			{
				Log.e(TAG, "222");
				this.printPosDeal(null, null);
			}
		}
		*/
	}
}
