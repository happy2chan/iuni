package com.sunnada.baseframe.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.bean.ConstantData;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.CRCData;
import com.sunnada.baseframe.bean.ErrorInfoBean;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.database.ErrorDealAdapter;
import com.sunnada.baseframe.database.KeyValueDao;
import com.sunnada.baseframe.database.SystemParamsDao;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.MD5Util;
import com.sunnada.baseframe.util.MyDES;
import com.sunnada.baseframe.util.StringUtil;

public class Business_00 extends BaseBusiness
{
	public static final String 		TITLE_STR				= "温馨提示";
	
	public SystemParamsDao			mSystemParams;						// 数据库对象
	private boolean					loginKeyFlag			= false;	// 随机密钥解密状态
	
	private static byte				mModifySrvPswdFlag		= 0x00;		// 是否需要修改交易密码
	private static String			mStrModifySrvPswdTip	= null;		// 修改交易密码消息提示
	private static byte				mModifyUserPswdFlag		= 0x00;		// 是否需要修改工号密码
	private static String			mStrModifyUserPswdTip	= null;		// 修改工号密码消息提示

	private KeyValueDao				keyValueDao				= null;
	private boolean					needToReloadConfig		= false;	// 是否要重新加载系统配置文件
	public static int				printType				= 1;		// 打印类型：1-凭条打印，2-发票打印，默认为1
	
	private String					mStrBllNum				= null;		// 交易流水号(字符串类型)
	private byte[]					mBllNum					= null;		// 交易流水号(字节流类型)
	public static byte				mPswdScene				= 0x00;		// 密码修改场景 0x01-初始密码 0x02-工号密码 0x03-交易密码
	
	public  int                     mCheckScope             = 0x01;     // 对账范围

	public  boolean					mIsNeedConfirm;						// 是否需要用户确认
	public  String					mStrConfirmInfo;					// 异常交易详情
	
	public Business_00(Context context, IEquipmentService equipmentService, ISocketService socketService, IDataBaseService dataBaseService)
	{
		super(context, equipmentService, socketService, dataBaseService);
		
		// 初始化数据库对象
		mSystemParams = new SystemParamsDao(context);
		keyValueDao = new KeyValueDao(dataBaseService);
	}
	
	// 开机获取随机码
	public boolean bll0000(String userNo, String userPwd) 
	{
		try
		{
			sendPackage = new Package();
			sendPackage.setServiceCommandID("FFFF");
			sendPackage.setSubId("00");
			sendPackage.setId("00");
			// 工号
			Buffer buffer = new Buffer();
			buffer.add((byte)userNo.length());
			buffer.add(userNo.getBytes());
			sendPackage.addUnit("0001", buffer.getBytes());
			// 密码
			buffer.clear();
			// toUpperCase 20131218,平台加密字符串时使用大写
			String strEncryptPwd = userNo.toUpperCase() + userPwd;
			byte[] pwd = MD5Util.getMD5String(strEncryptPwd);
			Commen.printhax(pwd, pwd.length, "加密后的工号密码", 'e');
			buffer.add((byte)pwd.length);
			buffer.add(pwd);
			sendPackage.addUnit("0002", buffer.getBytes());
			
			equipmentService.getPsamInfo(0x00).setPsamID("0000000000000000");
			// 数据收发
			int ret = send(sendPackage);
			if (ret == ReturnEnum.SUCCESS) 
			{
				ret = receiveOnePackage("0000");
			}
			if (ret != ReturnEnum.SUCCESS) 
			{
				String ss = "获取随机密钥失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				DialogUtil.MsgBox(TITLE_STR, ss);
				setLastknownError(ss);
				return false;
			}
			
			if (recivePackage.containsKey("0101") == false)
			{
				String ss = "获取随机密钥失败!\n失败原因: [服务器响应数据格式非法, 缺少0101子单元!]";
				DialogUtil.MsgBox(TITLE_STR, ss);
				setLastknownError(ss);
				return false;
			}
			else
			{
				byte[] unit = recivePackage.get("0101");
				int offset = 0;
				// 密钥信息
				// 密钥长度
				int len = unit[offset++]&0xFF;
				byte[] temp = new byte[len];
				System.arraycopy(unit, offset, temp, 0, len);
				offset += len;
				Commen.printhax(temp, len, "原始随机密钥: ", 'e');
				byte[] loginKey = MyDES.des_decrypt("sunnada0".getBytes(), temp);
				Commen.printhax(loginKey, len, "随机密钥明文: ", 'e');
				equipmentService.setLoginKey(loginKey);
				// 虚拟PSAM卡信息
				len = unit[offset++]&0xFF;
				temp = new byte[len];
				System.arraycopy(unit, offset, temp, 0, len);
				offset += len;
				String newPsamID = new String(temp);
				Log.d("xxxx", "虚拟PSAM卡卡号为: " + newPsamID);
				equipmentService.getPsamInfo(0x00).setPsamID(newPsamID);
			}
			return true;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			String ss = "获取随机密钥失败!\n失败原因: [获取密钥异常!]";
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
	}
	
	// 开机上报
	public int bll0001() 
	{
		int ret = ReturnEnum.SUCCESS;
		try 
		{
			sendPackage = new Package();
			sendPackage.setServiceCommandID("FFFF");
			sendPackage.setSubId("01");
			sendPackage.setId("00");
			// 设备类型
			Buffer buffer = new Buffer();
			buffer.add((short)0xC000);
			sendPackage.addUnit("0001", buffer.getBytes());
			// IMEI号
			sendPackage.addUnit("0003", equipmentService.getIMEI().getBytes());
			// 硬件版本
			sendPackage.addUnit("0004", "3.2".getBytes());
			// 软件版本
			sendPackage.addUnit("0005", equipmentService.getFrameVersion().getBytes());
			// 终端版本区号
			String[] lacci = equipmentService.getLacCi();
			Log.e("", "开机上报lac=" + lacci[0] + ", ci=" + lacci[1]);
			// 
			buffer.clear();
			buffer.add(Integer.parseInt(lacci[0]));
			buffer.add(Integer.parseInt(lacci[1]));
			sendPackage.addUnit("0009", buffer.getBytes());
			// 上传卡类型(只支持新卡)
			sendPackage.addUnit("0011", new byte[]{ 0x01 });
			// PSAM卡卡号
			String psamID = equipmentService.getPsamInfo(0x00).getPsamID();
			buffer.clear();
			buffer.add((byte)0x08);
			buffer.add(Commen.hexstr2byte(psamID));
			sendPackage.addUnit("0012", buffer.getBytes());
			// 终端版本区号
			sendPackage.addUnit("0013", "0010".getBytes());
			// 业务展示量0014(暂时先放着)
			
			// 升级成功标志
			sendPackage.addUnit("0081", "0000".getBytes());
			// 上传终端更新检测CRC
			buffer.clear();
			buffer.add("0100");
			buffer.add(CRCData.crcmenu);
			sendPackage.addUnit("0148", buffer.getBytes());
			// 2G预付费功能新终端标示
			sendPackage.addUnit("0152", new byte[]{ 0x01 });
			// POS ID
			byte[] temp = new byte[8];
			Arrays.fill(temp, (byte)0x00);
			sendPackage.addUnit("0F22", temp);
			// 受卡方标识码
			temp = new byte[15];
			Arrays.fill(temp, (byte)0x00);
			sendPackage.addUnit("0F23", temp);
			
			// 数据收发
			ret = send(sendPackage);
			if (ret == ReturnEnum.SUCCESS) 
			{
				ret = receiveOnePackage("0001");
			}
			if (ret != ReturnEnum.SUCCESS)
			{
				setLastknownError(socketService.getLastKnowError());
				return ret;
			}
			
			// 时间校准
			if (recivePackage.containsKey("0141")) 
			{
				Log.d("", "收到校准时间命令单元");
			}
			// 终端停用启用信息
			if (recivePackage.containsKey("0142")) 
			{
				byte[] buf = recivePackage.get("0142");
				if (buf[0] == 0x01)
				{
					Log.e("", "设备状态正常");
				}
				else if (buf[0] == 0x02)
				{
					Log.e("", "设备状态停用");
					setLastknownError("终端已经停用");
				}
				else if (buf[0] == 0x03)
				{
					Log.e("", "设备状态注销");
					setLastknownError("终端已经注销");
					return ReturnEnum.FAIL;
				}
			}
			// 首先获取随机密钥，才能在菜单更新时候使用随机密钥进行加密
			if (recivePackage.containsKey("0154"))
			{
				byte[] unit = recivePackage.get("0154");
				int offset = 0;
				// 升级URL长度
				int len = unit[offset++]&0xFF;
				temp = new byte[len];
				System.arraycopy(unit, offset, temp, 0, len);
				offset += len;
				try 
				{
					// http://220.249.190.132:21506/AndroidFiles/zhuxiao/34
					String url = new String(temp, "GBK");
					Log.d("xxxx", "终端软件升级下推地址1: " + url);
					// debug
					//url = "http://220.249.190.132:21504/AndroidFiles/zhuxiao/34";
					// debug end
					String oldUrl = keyValueDao.get(KeyValueDao.KEY_UPDATE_URL);
					Log.d("xxxx", "终端软件升级下推地址2: " + url);
					Log.d("xxxx", "终端软件升级本地地址: " + oldUrl);
					// 
					needToReloadConfig = false;
					// 丢弃省份编码
					//url = url.substring(0, url.length()-3);
					if (!url.equals(oldUrl)) 
					{
						needToReloadConfig = true;
						keyValueDao.insert(KeyValueDao.KEY_UPDATE_URL, url);
						//ConstantData.setServerPath(url);
					}
				}
				catch(Exception e) 
				{
					e.printStackTrace();
				}
			}
			// 交易密码修改标识
			Statics.HAD_PWD = true;
			mModifySrvPswdFlag = 0x00;
			if (recivePackage.containsKey("0213")) 
			{
				Log.e("", "平台要求修改交易密码");
				mModifySrvPswdFlag = recivePackage.get("0213")[0];
				if (1 == mModifySrvPswdFlag)
				{
					Log.d("", "第一次使用要求修改交易密码");
					Statics.HAD_PWD = false;
					Statics.MODIFY_TYPE = 2;
				}
				else if (2 == mModifySrvPswdFlag) 
				{
					Log.d("", "平台强制要求修改交易密码");
					Statics.HAD_PWD = false;
					Statics.MODIFY_TYPE = 2;
				}
			}
			if (recivePackage.containsKey("0214"))
			{
				byte[] unit = recivePackage.get("0214");
				int offset = 0;
				// 交易密码修改提示
				int len = unit[offset++]&0xFF;
				temp = new byte[len];
				System.arraycopy(unit, offset, temp, 0, len);
				offset += len;
				try 
				{
					mStrModifySrvPswdTip = new String(temp, "GBK");
					Log.d("交易密码修改提示: ", mStrModifySrvPswdTip);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			// 工号密码修改标识
			mModifyUserPswdFlag = 0x00;
			if (recivePackage.containsKey("0215")) 
			{
				Log.e("", "平台要求修改工号密码");
				mModifyUserPswdFlag = recivePackage.get("0215")[0];
				if (1 == mModifyUserPswdFlag)
				{
					Log.d("", "第一次使用要求修改工号密码");
					Statics.HAD_PWD = false;
					Statics.MODIFY_TYPE = 1;
				}
				else if (2 == mModifyUserPswdFlag)
				{
					Log.d("", "平台强制要求修改工号密码");
				}
			}
			if (recivePackage.containsKey("0216")) 
			{
				byte[] unit = recivePackage.get("0216");
				int offset = 0;
				// 工号密码修改提示
				int len = unit[offset++]&0xFF;
				temp = new byte[len];
				System.arraycopy(unit, offset, temp, 0, len);
				offset += len;
				try 
				{
					mStrModifyUserPswdTip = new String(temp, "GBK");
					Log.d("工号密码修改提示: ", mStrModifyUserPswdTip);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
			// 获取打印形式
			if (recivePackage.containsKey("04C8"))
			{
				byte[] unit = recivePackage.get("04C8");
				printType = unit[0];
			}
			// 优惠购机图片更新地址
			if (recivePackage.containsKey("04C9")) 
			{
				byte[] unit = recivePackage.get("04C9");
				int len = unit[0]&0xFF;
				String serverPath = StringUtil.encodeWithGBK(unit, 1, len);
				ConstantData.setServerPath(serverPath);
				Log.d("优惠购机图片服务器更新地址: ", serverPath);
			}
			// 各类版本CRC信息
			if (recivePackage.containsKey("0145")) 
			{
				Log.e("", "收到各类版本CRC");
				// 从数据库里面获取各类版本CRC
				mSystemParams.GetAllCrc();
				// 业务更新
				dealcrcByStep(recivePackage.get("0145"));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ret = ReturnEnum.FAIL;
			setLastknownError("开机上报出现未知异常");
		}
		return ret;
	}
	
	// 处理各类版本CRC信息
	private void dealcrcByStep(byte[] unit)
	{				
		try 
		{
			int 	bitmap = Commen.bytes2int(new byte[]{unit[0], unit[1]});
			int 	offset = 2;
			byte[] 	crc = new byte[2];
			String 	crcstr = "";
			
			// 更新菜单版本
			DialogUtil.showProgress("正在更新菜单版本...");
			if ((bitmap & 0x0001) != 0) 
			{
				System.arraycopy(unit, offset, crc, 0, 2);
				offset += 2;
				crcstr = Commen.hax2str(crc);
				Log.e("", "平台下发菜单CRC ：" + crcstr);
				if(!crcstr.equalsIgnoreCase(CRCData.crcmenu)) 
				{
					Log.d("", String.format("开始更新菜单, 原CRC: %s, 现CRC: %s...", CRCData.crcmenu, crcstr));
					bll0021(0x01);
					Log.d("", "菜单更新完成");
				}
				else
				{
					Log.d("", "不需要更新菜单!");
				}
			}
			
			// 更新2G套餐信息
			DialogUtil.showProgress("正在更新2G套餐版本...");
			if ((bitmap & 0x0002) != 0) 
			{  
				System.arraycopy(unit, offset, crc, 0, 2);
				offset += 2;
				crcstr = Commen.hax2str(crc);
				if(!crcstr.equalsIgnoreCase(CRCData.crcmeal))
				{
					Log.d("", "开始更新2G套餐...");
					bll0021(0x02);
					Log.d("", "2G套餐更新完成");
				}
				else
				{
					Log.d("", "不需要更新2G套餐版本!");
				}
			}
			
			// 更新增值业务
			DialogUtil.showProgress("正在更新增值业务...");
			if ((bitmap & 0x0004) != 0) 
			{  
				System.arraycopy(unit, offset, crc, 0, 2);
				offset += 2;
				crcstr = Commen.hax2str(crc);
				if(!crcstr.equalsIgnoreCase(CRCData.crcincrement))
				{
					Log.d("", "开始更新增值业务...");
					bll0021(0x03);
					Log.d("", "增值业务更新完成");
				}
				else
				{
					Log.d("", "不需要更新增值业务!");
				}
			}
			
			// 更新特服业务
			DialogUtil.showProgress("正在更新特服业务...");
			if ((bitmap & 0x0008) != 0) 
			{  
				System.arraycopy(unit, offset, crc, 0, 2);
				offset += 2;
				crcstr = Commen.hax2str(crc);
				if(!crcstr.equalsIgnoreCase(CRCData.crcespeciallyservice))
				{
					Log.d("", "开始更新特服业务...");
					this.bll0021(0x04);
					Log.d("", "特服业务更新完成");
				}
				else
				{
					Log.d("", "不需要更新特服业务!");
				}
			}
			
			// 更新选号规则
			DialogUtil.showProgress("正在更新选号规则...");
			if ((bitmap & 0x0010) != 0) 
			{
				System.arraycopy(unit, offset, crc, 0, 2);
				offset += 2;
				crcstr = Commen.hax2str(crc);
				if(!crcstr.equalsIgnoreCase(CRCData.crcselnorule))
				{
					Log.d("", "开始更新选号规则...");
					this.bll0021(0x05);
					Log.d("", "选号规则更新完成");
				}
				else
				{
					Log.d("", "不需要更新选号规则!");
				}
			}
			
			// 更新品牌列表
			DialogUtil.showProgress("正在更新品牌列表...");
			if ((bitmap & 0x0020) != 0) 
			{  
				System.arraycopy(unit, offset, crc, 0, 2);
				offset += 2;
				crcstr = Commen.hax2str(crc);
				if(!crcstr.equalsIgnoreCase(CRCData.crcbrand))
				{
					Log.d("", "开始更新品牌列表...");
					this.bll0021(0x06);
					Log.d("", "品牌列表更新完成");
				}
				else
				{
					Log.d("", "不需要更新品牌列表!");
				}
			}
			
			// 更新开机欢迎语
			DialogUtil.showProgress("正在更新开机欢迎语...");
			if ((bitmap & 0x0040) != 0) 
			{  
				System.arraycopy(unit, offset, crc, 0, 2);
				offset += 2;
				crcstr = Commen.hax2str(crc);
				if(!crcstr.equalsIgnoreCase(CRCData.crcwelcome))
				{
					Log.d("", "开始更新开机欢迎语...");
					bll0021(0x07);
					Log.d("", "开机欢迎语更新完成");
				}
				else
				{
					Log.d("", "不需要更新开机欢迎语!");
				}
			}
			
			// 更新打印信息
			DialogUtil.showProgress("正在更新打印信息...");
			if ((bitmap & 0x0080) != 0) 
			{
				System.arraycopy(unit, offset, crc, 0, 2);
				offset += 2;
				crcstr = Commen.hax2str(crc);
				if(!crcstr.equalsIgnoreCase(CRCData.crcprint))
				{
					Log.d("", "开始更新凭条打印信息...");
					this.bll0021(0x08);
					Log.d("", "凭条打印信息更新完成");
				}
				else
				{
					Log.d("", "不需要更新凭条打印信息!");
				}
			}
			
			// 更新收件箱
			DialogUtil.showProgress("正在更新收件箱...");
			if ((bitmap & 0x0100) != 0) 
			{
				System.arraycopy(unit, offset, crc, 0, 2);
				offset += 2;
				crcstr = Commen.hax2str(crc);
				if(!crcstr.equalsIgnoreCase(CRCData.crcinbox))
				{
					Log.d("", "开始更新收件箱...");
					bll0021(0x09);
					Log.d("", "收件箱更新完成");
				}
				else
				{
					Log.d("", "不需要更新收件箱!");
				}
			}
			// 3G套餐
			DialogUtil.showProgress("正在更新3G套餐...");
			if((bitmap & 0x200) != 0)
			{
				System.arraycopy(unit, offset, crc, 0, 2);
				offset += 2;
				crcstr = Commen.hax2str(crc);
				if(!crcstr.equalsIgnoreCase(CRCData.crc3gmeal))
				{
					Log.d("", "开始更新3G套餐...");
					bll0021(0x0A);
					Log.d("", "3G套餐更新完成");
				}
				else
				{
					Log.d("", "不需要更新3G套餐!");
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// 是否需要修改交易密码
	public byte getModifySrvPasswordFlag()
	{
		return mModifySrvPswdFlag;
	}

	// 修改交易密码的消息提示
	public String getModifySrvPasswordMessage() 
	{
		return mStrModifySrvPswdTip;
	}
	
	// 是否需要修改工号密码
	public byte getModifyUserPasswordFlag() 
	{
		return mModifyUserPswdFlag;
	}
	
	// 修改工号密码的消息提示
	public String getModifyUserPasswordMessage() 
	{
		return mStrModifyUserPswdTip;
	}
	
	// 心跳上报_发送
	public int bll0003_Send()
	{
		Log.d("", "---------------------------------0003 run---------------------------------");
		sendPackage = new Package();
		sendPackage.setSubId("03");
		sendPackage.setId("00");
		sendPackage.setServiceCommandID("FFFF");
		
		sendPackage.addUnit("0001", new byte[]{ (byte) 0x0202 });

		int flag = send(sendPackage);
		if (flag != ReturnEnum.SUCCESS)
		{
			Log.e("", "发送失败,flag = " + flag);
			return flag;
		}
		
		int ret = bll0003_Recv();
		return ret;
	}

	/**
	 * 心跳上报_接收
	 * 
	 * @return
	 */
	public int bll0003_Recv()
	{
		int ret = receiveOnePackage("0011");
		if (ret != ReturnEnum.SUCCESS)
		{
			Log.e("", "接收心跳失败,flag = " + ret);
			return ret;
		}

		// 导购临时版本-心跳不更新
		//parseUnit0142(recivePackage);
		//parseUnit0145(recivePackage);
		return ret;
	}

	// 解析0145命令单元
	protected boolean parseUnit0145(Package package1)
	{
		boolean flag = true;

		if (package1.containsKey("0145"))
		{
			byte[] buf = recivePackage.get("0145");
			
			// 根据0145返回的CRC进行校验更新
			dealcrc(buf, null);
		}
		return flag;
	}
	
	// 处理更新数据
	private void dealcrc(byte[] unit, byte[] key)
	{
		int 	bitmap = Commen.bytes2int(new byte[]{unit[0], unit[1]});
		int 	pos = 2;
		byte[] 	crc = new byte[2];
		String 	crcstr = "";

		// 如果密钥不为空
		if (null != key)
		{
			// 如果密钥未解密过
			if (loginKeyFlag == false)
			{
				try
				{
					equipmentService.setLoginKey(key);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				//Log.d("", "android随机密钥", loginKeypwd);
				loginKeyFlag = true;
			}
		}

		if ((bitmap & 0x0001) != 0)
		{ // 菜单CRC
			System.arraycopy(unit, pos, crc, 0, 2);
			crcstr = Commen.hax2str(crc);
			if (!crcstr.equalsIgnoreCase(CRCData.crcmenu))
			{
				Log.d("", "开始更新菜单");
				this.bll0021((byte) 0x01);
				Log.d("", "菜单更新成功");
			}
			pos += 2;
		}

		/*
		 * if ((bitmap & 0x0001) != 0) { //菜单CRC System.arraycopy(unit, pos,
		 * crc, 0, 2); crcstr = Commen.hax2str(crc);
		 * if(!crcstr.equalsIgnoreCase(CRCData.crcmenu) ) { Log.log("开始更新菜单");
		 * this.bll0021_Send((byte)0x01); Log.log("菜单更新成功"); } pos += 2; } if
		 * ((bitmap & 0x0002) != 0) { //套餐（产品包）CRC System.arraycopy(unit, pos,
		 * crc, 0, 2); crcstr = Commen.hax2str(crc);
		 * if(!crcstr.equalsIgnoreCase(CRCData.crcmeal) ) { Log.log("开始更新套餐");
		 * this.bll0021_Send((byte)0x02); Log.log("套餐更新成功"); } pos += 2; } if
		 * ((bitmap & 0x0004) != 0) { //增值业务CRC System.arraycopy(unit, pos, crc,
		 * 0, 2); crcstr = Commen.hax2str(crc);
		 * if(!crcstr.equalsIgnoreCase(CRCData.crcincrement) ) {
		 * Log.log("开始更新增值业务"); this.bll0021_Send((byte)0x03);
		 * Log.log("增值业务更新成功"); } pos += 2; } if ((bitmap & 0x0008) != 0) {
		 * //特服业务CRC System.arraycopy(unit, pos, crc, 0, 2); crcstr =
		 * Commen.hax2str(crc);
		 * if(!crcstr.equalsIgnoreCase(CRCData.crcespeciallyservice) ) {
		 * Log.log("开始更新特服业务"); this.bll0021_Send((byte)0x04);
		 * Log.log("特服业务更新成功"); } pos += 2; } if ((bitmap & 0x0010) != 0) {
		 * //选号规则CRC System.arraycopy(unit, pos, crc, 0, 2); crcstr =
		 * Commen.hax2str(crc);
		 * if(!crcstr.equalsIgnoreCase(CRCData.crcselnorule) ) {
		 * Log.log("开始更新选号规则"); this.bll0021_Send((byte)0x05);
		 * Log.log("选号规则更新成功"); } pos += 2; } if ((bitmap & 0x0020) != 0) {
		 * //品牌列表CRC System.arraycopy(unit, pos, crc, 0, 2); crcstr =
		 * Commen.hax2str(crc); if(!crcstr.equalsIgnoreCase(CRCData.crcbrand) )
		 * { Log.log("开始更新品牌列表"); this.bll0021_Send((byte)0x06);
		 * Log.log("品牌列表更新成功"); } pos += 2; } if ((bitmap & 0x0040) != 0) {
		 * //开机欢迎语CRC System.arraycopy(unit, pos, crc, 0, 2); crcstr =
		 * Commen.hax2str(crc); if(!crcstr.equalsIgnoreCase(CRCData.crcwelcome)
		 * ) { Log.log("开始更新开机欢迎语"); this.bll0021_Send((byte)0x07);
		 * Log.log("开机欢迎语更新成功"); } pos += 2; } if ((bitmap & 0x0080) != 0) {
		 * //打印信息CRC System.arraycopy(unit, pos, crc, 0, 2); crcstr =
		 * Commen.hax2str(crc); if(!crcstr.equalsIgnoreCase(CRCData.crcprint) )
		 * { Log.log("开始更新打印信息"); this.bll0021_Send((byte)0x08);
		 * Log.log("打印信息更新成功"); } pos += 2; } if ((bitmap & 0x0100) != 0) {
		 * //收件箱 System.arraycopy(unit, pos, crc, 0, 2); crcstr =
		 * Commen.hax2str(crc); if(!crcstr.equalsIgnoreCase(CRCData.crcinbox) )
		 * { Log.log("开始更新收件箱"); this.bll0021_Send((byte)0x09);
		 * Log.log("收件箱更新成功"); } pos += 2; } if ((bitmap & 0x0200) != 0) {
		 * //3G套餐CRC System.arraycopy(unit, pos, crc, 0, 2); crcstr =
		 * Commen.hax2str(crc); if(!crcstr.equalsIgnoreCase(CRCData.crc3gmeal) )
		 * { Log.log("开始更新3G套餐"); this.bll0021_Send((byte)0x0A);
		 * Log.log("3G套餐更新成功"); } pos += 2; }
		 */
	}
	
	// 修改工号密码或者交易密码
	// type: 0x00-工号密码 0x01-交易密码
	public boolean bll0008(String strOldPswd, String strNewPswd, String strNewPswd2) 
	{
		try
		{
			sendPackage = new Package();
			sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
			sendPackage.setSubId("08");
			sendPackage.setId("00");
			// 场景类型
			Buffer buffer = new Buffer();
			// 初始密码
			if(mPswdScene == 0x01)
			{
				// 加密新工号密码
				String strEncryptPwd = App.getUserNo().toUpperCase() + strNewPswd;
				byte[] pwd = MD5Util.getMD5String(strEncryptPwd);
				Commen.printhax(pwd, pwd.length, "加密后的新工号密码", 'e');
				// 新工号密码
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
				// 加密新交易密码
				pwd = this.PWDEncryption(strNewPswd2);
				Commen.printhax(pwd, pwd.length, "加密后的新交易密码", 'e');
				// 新交易密码
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
			}
			// 修改工号密码
			else if(mPswdScene == 0x02) 
			{
				// 加密新工号密码
				String strEncryptPwd = App.getUserNo().toUpperCase() + strOldPswd;
				byte[] pwd = MD5Util.getMD5String(strEncryptPwd);
				Commen.printhax(pwd, pwd.length, "加密后的旧工号密码", 'e');
				// 旧工号密码
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
				// 加密新工号密码
				strEncryptPwd = App.getUserNo().toUpperCase() + strNewPswd;
				pwd = MD5Util.getMD5String(strEncryptPwd);
				Commen.printhax(pwd, pwd.length, "加密后的旧工号密码", 'e');
				// 新工号密码
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
				// 确认工号密码
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
			}
			// 修改交易密码
			else if(mPswdScene == 0x03) 
			{
				// 旧交易密码
				byte[] pwd = this.PWDEncryption(strOldPswd);
				Commen.printhax(pwd, pwd.length, "加密后的旧交易密码", 'e');
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
				// 新交易密码
				pwd = this.PWDEncryption(strNewPswd);
				Commen.printhax(pwd, pwd.length, "加密后的新交易密码", 'e');
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
				// 确认交易密码
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
			}
			// 重设交易密码
			else if(mPswdScene == 0x04) 
			{
				byte[] pwd = this.PWDEncryption(strNewPswd);
				Commen.printhax(pwd, pwd.length, "加密后的新交易密码", 'e');
				// 新交易密码
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
				// 确认交易密码
				buffer.add((byte)pwd.length);
				buffer.add(pwd);
			}
			sendPackage.addUnit("0208", buffer.getBytes());
			// 交易流水号
			buffer.clear();
			buffer.add((byte)0x10);
			buffer.add(mBllNum);
			sendPackage.addUnit("0202", buffer.getBytes());
			
			// 数据收发
			int ret = send(sendPackage);
			if (ret == ReturnEnum.SUCCESS)
			{
				ret = receiveOnePackage("0008");
			}
			if (ret != ReturnEnum.SUCCESS) 
			{
				String ss = getPswdString() + "失败!";
				if(StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				DialogUtil.MsgBox(TITLE_STR, ss);
				setLastknownError(ss);
				return false;
			}
			return true;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			// 
			String ss = getPswdString() + "失败!\n失败原因: [出现未知异常]";
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
	}
	
	// 获取密码修改描述
	private String getPswdString() 
	{
		if(mPswdScene == 0x01) 
		{
			return "设定初始密码";
		}
		else if(mPswdScene == 0x02) 
		{
			return "修改工号密码";
		}
		else if(mPswdScene == 0x03) 
		{
			return "修改交易密码";
		}
		else
		{
			return "重设交易密码";
		}
	}
	
	// 获取密码修改流水号
	// isFirst: 0x01-初始密码修改 0x02-密码设置
	// isUserPswd: 0x02-工号密码设置 0x03-交易密码设置
	public boolean bll000A(int isFirst, int isUserPswd) 
	{
		try
		{
			sendPackage = new Package();
			sendPackage.setServiceCommandID("FFFF");
			sendPackage.setSubId("0A");
			sendPackage.setId("00");
			
			// 是否初始密码设置
			sendPackage.addUnit("0101", new byte[]{(byte)isFirst});
			if(isFirst == 0x01)
			{
				mPswdScene = 0x01;
			}
			else if(isFirst == 0x02) 
			{
				sendPackage.addUnit("0102", new byte[]{(byte)isUserPswd});
			}
			
			int ret = send(sendPackage);
			if (ret == ReturnEnum.SUCCESS)
			{
				ret = receiveOnePackage("000A");
			}
			if (ret != ReturnEnum.SUCCESS)
			{
				String ss = "获取交易流水号失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				DialogUtil.MsgBox(TITLE_STR, ss);
				setLastknownError(ss);
				return false;
			}
			// 交易流水号
			if (recivePackage.containsKey("0203") == false) 
			{
				String ss = "获取交易流水号失败!\n失败原因: [服务器响应数据非法, 缺少0203命令单元!]";
				DialogUtil.MsgBox(TITLE_STR, ss);
				setLastknownError(ss);
				return false;
			}
			else
			{
				mBllNum = recivePackage.get("0203");
				// 第一个字节是长度
				int len = mBllNum[0]&0xFF;
				mStrBllNum = StringUtil.encodeWithGBK(mBllNum, 1, len);
				Log.d("修改密码", "交易流水号为: " + mStrBllNum);
			}
			// 密码修改场景
			if(isFirst == 0x02) 
			{
				if(recivePackage.containsKey("0202") == false) 
				{
					String ss = "获取交易流水号失败!\n失败原因: [服务器响应数据非法, 缺少0202命令单元!]";
					DialogUtil.MsgBox(TITLE_STR, ss);
					setLastknownError(ss);
					return false;
				}
				else
				{
					mPswdScene = recivePackage.get("0202")[0];
				}
			}
			return true;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			setLastknownError("获取流水号出现未知异常!");
			DialogUtil.MsgBox(TITLE_STR, getLastknownError());
			return false;
		}
	}
	
	// 获取是否需要展示实名制登录界面
	public boolean bll000B(byte[] isShow, int[] nAgentNoLen, byte[] szAgentNo, int[] nAgentPswdLen, byte[] szAgentPswd)  
	{
		try
		{
			sendPackage = new Package();
			sendPackage.setServiceCommandID("FFFF");
			sendPackage.setSubId("0B");
			sendPackage.setId("00");
			// 0201
			Buffer buffer = new Buffer();
			buffer.add(0x00);
			sendPackage.addUnit("0201", buffer.getBytes());
			
			int ret = send(sendPackage);
			if (ret == ReturnEnum.SUCCESS)
			{
				ret = receiveOnePackage("000B");
			}
			if (ret != ReturnEnum.SUCCESS)
			{
				setLastknownError(socketService.getLastKnowError());
				DialogUtil.MsgBox(TITLE_STR, socketService.getLastKnowError());
				return false;
			}
			// 是否需要显示实名制登录界面
			if (recivePackage.containsKey("0101") == false) 
			{
				String ss = "获取实名制场景失败!";
				ss += "\n失败原因: [" + "服务器响应数据格式非法, 缺少0101子单元!" + "]";
				DialogUtil.MsgBox(TITLE_STR, ss);
				setLastknownError(ss);
				return false;
			}
			else
			{
				byte[] unit = recivePackage.get("0101");
				// 是否需要显示
				isShow[0] = unit[0];
				// 不需要显示
				if(isShow[0] == 0x01)
				{
					Log.e("", "需要展示登录界面");
					return true;
				}
				else
				{
					Log.e("", "不需要展示登录界面, 自动登录");
					// 实名制登录信息
					if (recivePackage.containsKey("0102") == false) 
					{
						String ss = "获取实名制场景失败!";
						ss += "\n失败原因: [" + "服务器响应数据格式非法, 缺少0102子单元!" + "]";
						DialogUtil.MsgBox(TITLE_STR, ss);
						setLastknownError(ss);
						return false;
					}
					else
					{
						unit = recivePackage.get("0102");
						int offset = 0;
						// 发展人编码长度
						nAgentNoLen[0] = unit[offset++]&0xFF;
						// 发展人编码
						System.arraycopy(unit, offset, szAgentNo, 0, nAgentNoLen[0]);
						offset += nAgentNoLen[0];
						// 发展人密码长度
						nAgentPswdLen[0] = unit[offset++]&0xFF;
						// 发展人密码
						System.arraycopy(unit, offset, szAgentPswd, 0, nAgentPswdLen[0]);
						offset += nAgentPswdLen[0];
						return true;
					}
				}
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			String ss = "获取实名制场景失败!";
			ss += "\n失败原因: [" + e.getMessage() + "]";
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
	}
	
	// 终端更新发展人编码和密码到平台
	public boolean bll000C(String strAgentNo, String strAgentPswd) 
	{
		try
		{
			sendPackage = new Package();
			sendPackage.setServiceCommandID("FFFF");
			sendPackage.setSubId("0C");
			sendPackage.setId("00");
			// 0101
			Buffer buffer = new Buffer();
			buffer.add("0300");
			// 发展人编码
			buffer.add((byte)strAgentNo.length());
			buffer.add(strAgentNo.getBytes());
			// 发展人密码
			buffer.add((byte)strAgentPswd.length());
			buffer.add(strAgentPswd.getBytes());
			sendPackage.addUnit("0101", buffer.getBytes());
			
			int ret = send(sendPackage);
			if (ret == ReturnEnum.SUCCESS)
			{
				ret = receiveOnePackage("000C");
			}
			if (ret != ReturnEnum.SUCCESS)
			{
				String ss = "提交发展人信息失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				setLastknownError(ss);
				return false;
			}
			return true;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			String ss = "提交发展人信息失败!";
			ss += "\n失败原因: [" + e.getMessage() + "]";
			setLastknownError(ss);
			return false;
		}
	}
	
	// 终端发送实名登记信息给平台
	public boolean bll000D(byte szResult, String strTel, String strErrCode, String strErrDes) 
	{
		try
		{
			sendPackage = new Package();
			sendPackage.setServiceCommandID("FFFF");
			sendPackage.setSubId("0D");
			sendPackage.setId("00");
			// 0201
			Buffer buffer = new Buffer();
			buffer.add("0F00");
			// 登记结果
			buffer.add(szResult);
			// 登记号码
			buffer.add((byte)strTel.length());
			buffer.add(strTel.getBytes());
			// 错误编码
			Log.e("", "错误编码为: " + strErrCode + ", 错误描述为: " + strErrDes);
			buffer.add((byte)strErrCode.length());
			buffer.add(strErrCode.getBytes());
			// 错误描述
			try
			{
				byte[] temp = strErrDes.getBytes("GBK");
				buffer.add((byte)temp.length);
				buffer.add(temp);
			}
			catch(Exception e) 
			{
				e.printStackTrace();
			}
			sendPackage.addUnit("0101", buffer.getBytes());
			
			int ret = send(sendPackage);
			if (ret == ReturnEnum.SUCCESS) 
			{
				ret = receiveOnePackage("000D");
			}
			if (ret != ReturnEnum.SUCCESS) 
			{
				String ss = "更新返档信息失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				//DialogUtil.showMessage(TITLE_STR, ss);
				setLastknownError(ss);
				return false;
			}
			return true;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			String ss = "更新返档信息失败!";
			ss += "\n失败原因: [" + e.getMessage() + "]";
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
	}
	
	// 终端发送更新后的密码到平台
	public boolean bll000E(String strUserNo, String strAgentNo, String strAgentPswd) 
	{
		try
		{
			sendPackage = new Package();
			sendPackage.setServiceCommandID("FFFF");
			sendPackage.setSubId("0E");
			sendPackage.setId("00");
			// 0201
			Buffer buffer = new Buffer();
			buffer.add("0700");
			// 营业员工号
			buffer.add((byte)strUserNo.length());
			buffer.add(strUserNo.getBytes());
			// 发展人工号
			buffer.add((byte)strAgentNo.length());
			buffer.add(strAgentNo.getBytes());
			// 发展人密码
			buffer.add((byte)strAgentPswd.length());
			buffer.add(strAgentPswd.getBytes());
			sendPackage.addUnit("0311", buffer.getBytes());
			
			int ret = send(sendPackage);
			if (ret == ReturnEnum.SUCCESS)
			{
				ret = receiveOnePackage("000E");
			}
			if (ret != ReturnEnum.SUCCESS)
			{
				String ss = "更新密码配置信息失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				DialogUtil.MsgBox(TITLE_STR, ss);
				setLastknownError(ss);
				return false;
			}
			return true;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			String ss = "更新密码配置信息失败!";
			ss += "\n失败原因: [" + e.getMessage() + "]";
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
	}
	
	// 查询网店信息
	public List<Map<String, String>> bll000F(String strUserNo, String[] strShopName, String[] strShopAddr, String[] strAgentName, String[] strAgentTel) 
	{
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("0F");
		sendPackage.setId("00");
		
		Buffer buffer = new Buffer();
		// 营业员工号
		buffer.add((byte)strUserNo.length());
		buffer.add(strUserNo.getBytes());
		sendPackage.addUnit("0328", buffer.getBytes());
		
		int ret = send(sendPackage);
		if (ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("000F");
		}
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取网点信息失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return null;
		}
		
		// 解析数据
		if(!recivePackage.containsKey("0410")) 
		{
			setLastknownError("获取网点信息失败!\n失败原因: [服务器响应数据非法, 缺少0410命令单元]");
			return null;
		}
		else
		{
			byte[] unit = recivePackage.get("0410");
			int offset = 0;
			// 数据域
			int bitmap = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 网点名称
			if((bitmap & 0x01) != 0) 
			{
				int len = unit[offset++];
				strShopName[0] = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
				Log.e("", "网点名称为: " + strShopName[0]);
			}
			// 网点地址
			if((bitmap & 0x02) != 0) 
			{
				int len = unit[offset++];
				strShopAddr[0] = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
				Log.e("", "网点地址为: " + strShopAddr[0]);
			}
			// 代理商名称
			if((bitmap & 0x04) != 0) 
			{
				int len = unit[offset++];
				strAgentName[0] = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
				Log.e("", "代理商姓名为: " + strAgentName[0]);
			}
			// 代理商电话
			if((bitmap & 0x08) != 0) 
			{
				int len = unit[offset++];
				strAgentTel[0] = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
				Log.e("", "代理商电话为: " + strAgentTel[0]);
			}
			// 工号总数
			if((bitmap & 0x10) != 0) 
			{
				int nTotal = unit[offset++];
				if(nTotal != 0) 
				{
					for(int i=0; i<nTotal; i++) 
					{
						Map<String, String> map = new HashMap<String, String>();
						// 姓名长度
						int len = unit[offset++];
						String strUserName = StringUtil.encodeWithGBK(unit, offset, len);
						offset += len;
						map.put(Statics.KEY_USER_NAME, strUserName);
						// 号码长度
						len = unit[offset++];
						String strUserTel = StringUtil.encodeWithGBK(unit, offset, len);
						offset += len;
						map.put(Statics.KEY_USER_TEL, strUserTel);
						// 工号长度
						len = unit[offset++];
						String tempstr = StringUtil.encodeWithGBK(unit, offset, len);
						offset += len;
						map.put(Statics.KEY_USER_NO, tempstr);
						// 
						Log.e("", "营业员姓名: " + strUserName + ", 联系电话: " + strUserTel + ", 工号: " + tempstr);
						list.add(map);
						//for(int j=0; j<10; j++) 
						//{
						//	list.add(map);
						//}
					}
				}
				return list;
			}
			return null;
		}
	}
	
	// 获取当日营业额信息
	public boolean bll0015(String[] strSalesMoney) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("15");
		sendPackage.setId("00");
		
		sendPackage.addUnit("0201", new byte[] {0x00, 0x00, 0x00, 0x00});
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0015");
		}
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取当日营业额失败!";
			if(StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}
		
		// 数据解析
		if(recivePackage.containsKey("0421") == false) 
		{
			String ss = "获取当日营业额失败!\n失败原因: [服务器响应数据非法, 缺少0421命令单元]";
			setLastknownError(ss);
			Log.e("", ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0421");
			int len = unit[0]&0xFF;
			String tempstr = StringUtil.encodeWithGBK(unit, 1, len);
			strSalesMoney[0] = String.format("%.2f", ((double)Integer.parseInt(tempstr))/100);
			Log.e("", "当日营业额为: " + strSalesMoney[0]);
			return true;
		}
	}
	
	// 业务更新
	public int bll0021(int type)
	{
		byte[] crc = new byte[2];
		
		return bll0021((byte)type, crc, true);
	}
	
	public int bll0021(byte type, byte[] crc, boolean isNeedDetail) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("21");
		sendPackage.setId("00");
		
		Buffer buffer = new Buffer();
		buffer.clear();
		buffer.add("0300");
		buffer.add(type); 				// 更新类型
		buffer.add("0000");
		sendPackage.addUnit("0149", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if (ret == ReturnEnum.SUCCESS)
		{
			ret = receiveOnePackage("0021");
		}
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "业务更新请求失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
			{
				ss += "\n失败原因：[" + socketService.getLastKnowError() + "]";
			}
			Log.e("", ss);
			return ret;
		}
		
		// 解析业务更新回复
		parseUnit0146(recivePackage, crc, isNeedDetail);
		return ret;
	}
	
	// 解析0416命令单元
	private boolean parseUnit0146(Package outdata, byte[] crc, boolean isNeedDetail) 
	{
		if (!outdata.containsKey("0146")) 
		{
			return false;
		}
		else
		{
			byte[] 	unit = recivePackage.get("0146");
			int 	offset = 0;
			int 	updateType = 0xFF;
			byte[]	data = null;
			
			int bitmap = Commen.bytes2int(unit, offset, 2);
			offset += 2;
			// 参数类型
			if ((bitmap & 0x0001) != 0)
			{
				updateType = unit[offset++];
			}
			// 参数CRC
			if ((bitmap & 0x0002) != 0)
			{ 
				System.arraycopy(unit, offset, crc, 0, 2);
				offset += 2;
			}
			// 不需要解析详情
			if(isNeedDetail == false)
			{
				return true;
			}
			// 更新内容
			if ((bitmap & 0x0004) != 0)
			{
				int len = unit.length-offset;
				data = new byte[len];
				System.arraycopy(unit, offset, data, 0, len);
			}
			
			switch (updateType)
			{
			// 业务菜单
			case 0x01:
				update0146_01(data, crc);
				break;
				
			// 更新套餐
			case 0x02:
				update0146_02(data, crc);
				break;
				
			// 增值业务版本
			case 0x03:
				mSystemParams.UpdateParamsCrc("00", "03", Commen.hax2str(crc));
				break;

			// 特服业务列表
			case 0x04:
				mSystemParams.UpdateParamsCrc("00", "04", Commen.hax2str(crc));
				break;
				
			// 选号规则列表
			case 0x05:
				mSystemParams.UpdateParamsCrc("00", "05", Commen.hax2str(crc));
				break;
				
			// 品牌列表
			case 0x06:
				update0146_06(data, crc);
				break;
				
			// 开机欢迎语更新
			case 0x07:
				update0146_07(data, crc);
				break;
				
			// 打印凭条广告信息
			case 0x08:
				update0146_08(data, crc);
				break;
				
			// 终端收件箱内容更新
			case 0x09:
				update0146_09(data, crc);
				break;
				
			// 3G套餐信息
			case 0x0A:
				update0146_0A(data, crc);
				break;
				
			default:
				break;
			}
			// 在每次更新后获取新的CRC数值
			mSystemParams.GetAllCrc();
			return true;
		}
	}
	
	// 更新业务菜单
	protected void update0146_01(byte[] info, byte[] crc)
	{
		int offset = 0;
		int total = info[offset++]&0xFF;
		
		// 删除存在的菜单权限
		mSystemParams.DeleteParamsValue("01");
		for (int i = 0; i < total; i++)
		{
			// 菜单编码
			byte[] code = new byte[2];
			System.arraycopy(info, offset, code, 0, 2);
			String menuCode = Commen.sprinthax(Commen.haxchange(code), "");
			offset += 2;
			// 菜单名称长度
			int len = info[offset++];
			String name = StringUtil.encodeWithGBK(info, offset, len);
			offset += len;
			
			Log.e("", "菜单编码: " + menuCode + ", 菜单名称: " + name);
			// 添加到数据库
			mSystemParams.InsertParamsValue("01", menuCode, name, "", "");
		}
		
		// 更新数据库的菜单CRC
		String menuCrc = Commen.hax2str(crc);
		Log.e("", "菜单版本crc为" + menuCrc);
		mSystemParams.UpdateParamsCrc("00", "01", menuCrc);
	}

	// 更新号卡套餐
	private void update0146_02(byte[] info, byte[] crc) 
	{
		int offset = 0;
		int total = info[offset++]&0xFF;

		// 删除存在的套餐
		mSystemParams.DeleteParamsValue("02");
		for (int i = 0; i < total; i++) 
		{
			byte[] temp = new byte[4];
			System.arraycopy(info, offset, temp, 0, 4);
			offset += 4;
			String strPackageCode = Commen.hax2str(temp);
			// 判断预付后付
			String strPayType = "1";
			// 预付
			if ((info[offset] & 0x04) != 0) 
			{
				strPayType = "1";
			}
			// 后付
			else 
			{
				strPayType = "2";
			}
			offset += 1;
			// 品牌编码
			String strBrandCode = Commen.hax2str(new byte[]{info[offset], info[offset+1]});
			offset += 2;
			// 套餐名称长度
			int len = info[offset++]&0xFF;
			// 套餐名称
			String strPackageName = StringUtil.encodeWithGBK(info, offset, len);
			offset += len;
			Log.e("", "套餐编码: " + strPackageCode + ", 付费标识: " + strPayType 
					+ ", 品牌编码: " + strBrandCode + ", 套餐名称: " + strPackageName);
			// 添加到数据库
			mSystemParams.InsertParamsValue("02", strPackageCode, strPackageName, strBrandCode, strPayType);
		}
		
		// 更新数据库的菜单CRC
		mSystemParams.UpdateParamsCrc("00", "02", Commen.hax2str(crc));
	}

	/*
	// 更新号卡号段
	private void update0146_02(byte[] info, byte[] crc)
	{
		int pos = 0;
		int total = info[pos++] & 0xFF;

		// 删除存在的号卡号段
		mSystemParams.DeleteParamsValue("02");
		for (int i = 0; i < total; i++)
		{
			// 获取号段编码
			byte[] temp = new byte[1];
			System.arraycopy(info, pos, temp, 0, 1);
			// 直接存成字符串的方式，提交的时候再把字符串直接转化为byte[]，原样转回去。本地存的值与服务器上的看着会不一样
			String code = Commen.hax2str(temp);
			pos += 1;
			// 获取内容长度
			int len = info[pos] & 0xFF;
			pos += 1;
			// 获取内容
			byte[] name = new byte[len];
			System.arraycopy(info, pos, name, 0, len);
			String combo_name = StringUtil.encodeWithGBK(name);
			pos += len;
			Log.e("", "号段code: " + code + ", 号段name:" + combo_name);
			
			// 添加到数据库
			mSystemParams.InsertParamsValue("02", code, combo_name, null, null);
		}
		
		// 更新数据库的号段CRC
		//mSystemParams.UpdateParamsCrc("00", "02", Commen.hax2str(crc));
		mSystemParams.UpdateParamsCrc("02", "00", Commen.hax2str(crc));
	}
	*/
	
	// 更新品牌
	private void update0146_06(byte[] info, byte[] crc)
	{
		int offset = 0;
		int total = info[offset++]&0xFF;
		
		// 删除存在的品牌信息
		mSystemParams.DeleteParamsValue("06");
		for (int i = 0; i < total; i++)
		{
			// 品牌类型
			int nBrandType = info[offset++]+1;
			// 品牌编码
			String strBrandCode = Commen.hax2str(new byte[]{info[offset], info[offset+1]});
			offset += 2;
			// 品牌名称
			int len = info[offset++]&0xFF;
			String strBrandName = StringUtil.encodeWithGBK(info, offset, len);
			offset += len;
			Log.e("", "品牌类型: " + nBrandType + ", 品牌编码: " + strBrandCode + ", 品牌名称: " + strBrandName);
			// 添加到数据库
			mSystemParams.InsertParamsValue("06", strBrandCode, strBrandName, "", String.valueOf(nBrandType));
		}

		// 更新数据库的品牌CRC
		mSystemParams.UpdateParamsCrc("00", "06", Commen.hax2str(crc));
	}
	
	// 更新开机欢迎语
	private void update0146_07(byte[] info, byte[] crc)
	{
		// 删除存在的菜单权限
		int offset = 0;
		int len = info[offset++]&0xFF;
		
		String strWelcome = StringUtil.encodeWithGBK(info, offset, len);
		offset += len;
		Log.e("", "开机欢迎语为: " + strWelcome);
		
		// 删除原先的开机欢迎语信息
		mSystemParams.DeleteParamsValue("07");
		// 添加到数据库
		mSystemParams.InsertParamsValue("07", "", strWelcome, "", "");
		// 更新开机欢迎语CRC
		mSystemParams.UpdateParamsCrc("00", "07", Commen.hax2str(crc));
	}

	// 更新凭条打印信息
	private void update0146_08(byte[] info, byte[] crc)
	{
		int offset = 0;
		int len = info[offset++]&0xFF;
		
		String strPrintInfo = StringUtil.encodeWithGBK(info, offset, len);
		offset += len;
		Log.e("", "凭条打印信息为: " + strPrintInfo);
		
		// 删除原先的凭条打印信息
		mSystemParams.DeleteParamsValue("08");
		// 添加到数据库
		mSystemParams.InsertParamsValue("08", "", strPrintInfo, "", "");
		// 更新数据库的凭条打印信息CRC
		mSystemParams.UpdateParamsCrc("00", "08", Commen.hax2str(crc));
	}

	// 更新收件箱信息
	private void update0146_09(byte[] info, byte[] crc)
	{
		int offset = 0;
		int total = info[offset++]&0xFF;
		
		for (int i = 0; i < total; i++)
		{
			int nMailLen = info[offset++]&0xFF;
			if (nMailLen < 14)
			{
				offset += nMailLen;
				continue;
			}
			// 邮件类型
			String strMailType = String.valueOf(info[offset++]);
			// 邮件时间
			String strMailTime = StringUtil.encodeWithGBK(info, offset, 10);
			offset += 10;
			// 邮件内容
			String strMailRes = StringUtil.encodeWithGBK(info, offset, nMailLen-11);
			offset += (nMailLen-11);
			Log.e("", "邮件类型: " + strMailType + ", 邮件时间: " + strMailTime + ", 邮件内容: " + strMailRes);
			// 将数据存入数据库
			mSystemParams.InsertParamsValue("09", 
					strMailTime, 
					strMailRes, 
					"0", 	// 这个参数暂时定为邮件的已读未读状态("0"未读 "1"已读), 不用于邮件类型
					App.getUserNo());
		}
		// 更新收件箱CRC
		mSystemParams.UpdateParamsCrc("00", "09", Commen.hax2str(crc));
	}

	// 更新3G套餐信息
	private void update0146_0A(byte[] info, byte[] crc)
	{
		int offset = 0;
		int total = info[offset++]&0xFF;
		
		// 删除存在的菜单权限
		mSystemParams.DeleteParamsValue("0A");
		for (int i = 0; i < total; i++)
		{
			// 套餐编码
			String strPackageCode = Commen.hax2str(new byte[]{info[offset], info[offset+1], info[offset+2], info[offset+3]});
			offset += 4;
			// 付费类型
			String strPayType = String.valueOf(info[offset++]);
			// 品牌编码
			String strBrandCode = Commen.hax2str(new byte[]{info[offset], info[offset+1]});
			offset += 2;
			// 套餐名称
			int len = info[offset++] & 0xFF;
			String strPackageName = StringUtil.encodeWithGBK(info, offset, len);
			offset += len;
			Log.e("", "3G套餐编码: " + strPackageCode + ", 套餐名称: " + strPackageName 
					+ ", 套餐品牌: " + strBrandCode + ", 付费类型: " + strPayType);
			// 添加到数据库
			mSystemParams.InsertParamsValue("0A", strPackageCode, strPackageName, strBrandCode, strPayType);
		}
		// 更新3G套餐的CRC
		mSystemParams.UpdateParamsCrc("00", "0A", Commen.hax2str(crc));
	}

	/*
	// 解析0142命令单元
	private boolean parseUnit0142(Package package1)
	{
		boolean flag = false;

		if (package1.containsKey("0142"))
		{
			byte status = recivePackage.get("0142")[0];

			switch (status)
			{
				case 1:
					Log.e("", "设备正常");
					flag = true;
					break;
					
				case 2:
					setLastknownError(getString(R.string.device_stop_use));
					break;
					
				case 3:
					setLastknownError(getString(R.string.device_logoff));
					break;
					
				case 4:
					setLastknownError(getString(R.string.password_lock));
					break;

				default:
					setLastknownError(getString(R.string.device_exception));
					break;
			}

			try
			{
				equipmentService.setTerminateCanUse(flag);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return flag;
	}
	*/
	
	public boolean isNeedToReloadConfig() 
	{
		return needToReloadConfig;
	}
	
	// 对账业务 概要信息
	public List<Map<String, Object>> bll002A(int nCheckType, String checkBeginDate, String checkEndDate, String strPswd) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("2A");
		sendPackage.setId("00");
		
		// 0401
		Buffer buffer = new Buffer();
		buffer.add("0F00"); 								// 添加数据域
		buffer.add((byte)mCheckScope);                      // 对账范围
		buffer.add((byte)nCheckType); 						// 对账类型
		buffer.add(checkBeginDate.getBytes()); 				// 对账日期
		if(nCheckType == 0x03)
		{
			buffer.add(checkEndDate.getBytes());			// 对账结束日期
		}
		buffer.add((byte)0x01); 							// 对账信息类型
		sendPackage.addUnit("0401", buffer.getBytes());
		// 0215
		buffer.clear();
		byte[] szEncryptedData = PWDEncryption(strPswd);
		buffer.add((byte)szEncryptedData.length);
		buffer.add(szEncryptedData);
		sendPackage.addUnit("0215", buffer.getBytes());
		
		int ret = send(sendPackage);
		if (ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("002A");
		}
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取对账详情失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			DialogUtil.MsgBox(TITLE_STR, ss);
			return null;
		}
		
		List<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
		if (!recivePackage.containsKey("0401")) 
		{
			String ss = "获取对账详情失败!";
			ss += "\n失败原因: [服务器响应数据非法, 缺少0401子命令单元]";
			setLastknownError(ss);
			DialogUtil.MsgBox(TITLE_STR, ss);
			return null;
		}
		else
		{
			byte[] unit = recivePackage.get("0401");
			int offset = 0;
			int map = Commen.bytes2int(new byte[] {unit[0], unit[1]});
			offset += 2;
			// 概要对帐类型
			if ((map & 0x0001) != 0) 
			{
				offset += 1;
			}
			// 对帐日期
			if ((map & 0x0002) != 0) 
			{
				if (nCheckType == 0x03) 
				{
					offset += 16;
				} 
				else 
				{
					offset += 8;
				}
			}
			// 对帐信息类型
			if ((map & 0x0004) != 0) 
			{
				offset += 1;
			}
			// 详细对帐类型
			if ((map & 0x0008) != 0) 
			{
				offset += 1;
			}
			// 概要信息文本
			if ((map & 0x0010) != 0) 
			{
				int nBillTotal = unit[offset++];
				if (nBillTotal > 20) 
				{ 
					Log.e("", "分类个数超过20个：" + nBillTotal);
					return null;
				}
				for (int i = 0; i < nBillTotal; i++) 
				{
					try {
						Map<String, Object> item = new HashMap<String, Object>();
						// 分类序号
						item.put(Statics.CHECK_CATEGORY_SEQ, String.format("%02d", i+1));
						// 分类编码
						int nCateCode = unit[offset++]&0xFF;
						item.put(Statics.CHECK_CATEGORY_CODE, String.format("%02d", nCateCode));
						// 分类编码是否有详情
						String strDetail = unit[offset++] == 0? "--":"查看";
						item.put(Statics.CHECK_CATEGORY_DETAIL, strDetail);
						// 分类名称
						int nTitleLen = unit[offset++]&0xFF;
						byte[] title = new byte[nTitleLen];
						System.arraycopy(unit, offset, title, 0, nTitleLen);
						offset += nTitleLen;
						String strTitle = new String(title, "GBK");
						item.put(Statics.CHECK_CATEGORY_TITLE, strTitle);
						// 分类内容
						int nContentLen = unit[offset++]&0xFF;
						byte[] szContent = new byte[nContentLen];
						System.arraycopy(unit, offset, szContent, 0, nContentLen);
						offset += nContentLen;
						String strContent = new String(szContent, "GBK");
						item.put(Statics.CHECK_CATEGORY_CONTENT, strContent);
						
						Log.e("", "分类编码: " + nCateCode + ", 分类详情: " + strDetail + 
								", 分类名称: " + strTitle  + ", 分类内容: " + strContent);
						res.add(item); 
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
			}
		}
		return res;
	}
	
	// 对账业务 详细信息
	public List<Map<String, String>> bll002A(int nCheckType, String strBeginDate, String strEndDate, String strPswd, 
			int nCateCode, int[] nTotal, int[] nCur) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("2A");
		sendPackage.setId("00");
		
		// 0401
		Buffer buffer = new Buffer();
		buffer.add("9F00"); 						// 数据域
		buffer.add((byte)mCheckScope);              // 对账范围
		buffer.add((byte)nCheckType); 				// 对账类型
		buffer.add(strBeginDate.getBytes()); 		// 对账日期
		buffer.add(strEndDate.getBytes());
		buffer.add((byte)0x02); 					// 对账信息类型
		buffer.add((byte)nCateCode); 				// 对账编码
		// 详细信息文本：
		// uint2:内容总条数(起始填充0)
		// uint2:起始内容条数(起始填充1)
		// uint1:显示行数(根据具体终端类型填充)
		buffer.add((short)nTotal[0]);
		buffer.add((short)nCur[0]);
		buffer.add((byte)0x0A);
		sendPackage.addUnit("0401", buffer.getBytes());
		// 0215
		buffer.clear();
		byte[] szEncrypted = PWDEncryption(strPswd);
		buffer.add((byte)szEncrypted.length);
		buffer.add(szEncrypted);
		sendPackage.addUnit("0215", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if (ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("002A");
		}
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取分类详情失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			DialogUtil.MsgBox(TITLE_STR, ss);
			return null;
		}
		
		if (!recivePackage.containsKey("0401")) 
		{
			String ss = "获取分类详情失败!";
			ss += "\n失败原因: [服务器响应数据非法, 缺少0401子命令单元]";
			setLastknownError(ss);
			DialogUtil.MsgBox(TITLE_STR, ss);
			return null;
		}
		else
		{
			byte[] 	unit = recivePackage.get("0401");
			int 	offset = 0;
			List<Map<String , String>> list = new ArrayList<Map<String, String>>();
			
			int bitmap = Commen.bytes2int(new byte[] {unit[0], unit[1]});
			offset += 2;
			// 概要对帐类型
			if ((bitmap & 0x0001) != 0) 
			{
				offset += 1;
			}
			// 对帐日期
			if ((bitmap & 0x0002) != 0) 
			{
				if (nCheckType == 0x03) 
				{
					offset += 16;
				} 
				else 
				{
					offset += 8;
				}
			}
			// 对帐信息类型
			if ((bitmap & 0x0004) != 0)
			{
				offset += 1;
			}
			// 详细对帐类型
			if ((bitmap & 0x0008) != 0) 
			{
				offset += 1;
			}
			// 详细信息标题文本
			if ((bitmap & 0x0020) != 0) 
			{
				int len = unit[offset++];
				String tempstr = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
				Log.e("", "详细信息标题文本" + tempstr);
				
				String[] temp = tempstr.split("\\|");
				Map<String, String> map = new HashMap<String, String>();
				if(temp.length == 3)
				{
					map.put(Statics.CHECK_TITLE1, 	temp[0].trim());
					map.put(Statics.CHECK_TITLE2, 	temp[1].trim());
					map.put(Statics.CHECK_TITLE3, 	temp[2].trim());
				}
				else if(temp.length == 2)
				{
					map.put(Statics.CHECK_TITLE1, 	temp[0].trim());
					map.put(Statics.CHECK_TITLE2, 	temp[1].trim());
				}
				list.add(map);
			}
			// 详细信息文本
			if ((bitmap & 0x0040) != 0) 
			{
				// 内容总条数
				nTotal[0] = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
				offset += 2;
				// 起始内容条数
				int start = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
				offset += 2;
				// 显示行数
				int lines = unit[offset++]&0xFF;
				Log.e("detail", "total=" + nTotal[0] + ", start=" + start + ", lines = " + lines);
				// 无内容
				if(lines == 0) 
				{
					DialogUtil.MsgBox(TITLE_STR, "无分类详情!");
					return null;
				}
				for (int i = 0; i < lines; i++) 
				{
					// 具体内容长度
					int len = unit[offset++]&0xFF;
					// 具体内容
					String strInfo = StringUtil.encodeWithGBK(unit, offset, len);
					offset += len;
					Log.e("", "分类内容: " + strInfo);
					
					String[] temp = strInfo.split("\\|");
					//Log.e("", "序号: " + temp[0] + ", 金额: " + temp[1] + ", 时间: " + temp[2]);
					Map<String, String> map = new HashMap<String, String>();
					if(temp.length == 3)
					{
						map.put(Statics.CHECK_DATA1, 	temp[1].trim());
						map.put(Statics.CHECK_DATA2, 	temp[2].trim());
						list.add(map);
					}
					else if(temp.length == 4)
					{
						map.put(Statics.CHECK_DATA1, 	temp[1].trim());
						map.put(Statics.CHECK_DATA2, 	temp[2].trim());
						map.put(Statics.CHECK_DATA3, 	temp[3].trim());
						list.add(map);
					}
				}
			}
			return list;
		}
	}
	
	// 终端异常上报
	public boolean bll0041(String strErrPsamID, String strErrType, ErrorInfoBean bean)
	{
		try 
		{
			// 拼装主命令
			sendPackage = new Package();
			sendPackage.setServiceCommandID("FFFF");
			sendPackage.setSubId("41");
			sendPackage.setId("00");
			// 拼装子命令体
			Buffer buffer = new Buffer();
			buffer.add(Commen.hexstr2byte(strErrType));			// 交易类型
			buffer.add((byte)bean.no.length());					// 交易流水号
			buffer.add(bean.no.getBytes());
			buffer.add(Commen.hexstr2byte(bean.reason));		// 异常交易原因
			buffer.add(Commen.hexstr2byte(bean.crc));			// 异常交易CRC
			buffer.add((byte)(bean.state.getBytes()[0]-0x30));	// 处理请求
			
			Log.e("", "异常交易流水: " + bean.no + ", 类型：" + strErrType + ", CRC: " + bean.crc + ", 原因: " + bean.reason);
			sendPackage.addUnit("020B", buffer.getBytes());
			
			// 数据收发
			int ret = send(sendPackage);
			if(ret == ReturnEnum.SUCCESS) 
			{
				ret = receiveOnePackage("0041");
			}
			if(ret != ReturnEnum.SUCCESS) 
			{
				String ss = "异常交易上报失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				setLastknownError(ss);
				Log.e("", ss);
				return false;
			}
			
			// 异常交易详情展示
			if(recivePackage.containsKey("020F"))
			{
				byte[] unit = recivePackage.get("020F");
				mIsNeedConfirm = (unit[0] == 0x00? false:true);
				Log.e("", "是否需要展示交易详情: " + mIsNeedConfirm);
				// 需要展示交易详情
				if(mIsNeedConfirm) 
				{
					mStrConfirmInfo = StringUtil.encodeWithGBK(unit, 3, unit.length-3);
					Log.e("", "异常交易详情: " + mStrConfirmInfo);
					return true;
				}
			}
			// 异常交易处理状态
			return procErrorDealState(strErrPsamID, strErrType, bean);
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// 处理异常交易状态
	private boolean procErrorDealState(String strErrPsamID, String strErrType, ErrorInfoBean bean) 
	{
		// 解析数据
		if (!recivePackage.containsKey("020B")) 
		{
			String ss = "异常交易上报失败!\n失败原因: [服务器响应数据非法, 缺少020B命令单元]";
			setLastknownError(ss);
			Log.e("", ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("020B");
			int offset = 0;
			// 业务类型
            offset += 1;
            // 流水号
			int len = unit[offset++]&0xFF;
			offset += len;
			// 异常原因
			offset += 1;
			// CRC信息域
			offset += 2;
			// 处理请求 0x00-已处理 0x01-未处理
			int flag = unit[offset++];
			if(flag == 0x00) 
			{
				ErrorDealAdapter.setErrorInfoState(context, strErrPsamID, strErrType, "0", "0", bean.crc, bean.no);
				Log.d("", "异常交易处理成功!");
			}
			else
			{
				ErrorDealAdapter.setErrorInfoState(context, strErrPsamID, strErrType, bean.reason, "1", bean.crc, bean.no);
				Log.e("", "异常交易处理失败!");
			}
			return true;
		}
	}
	
	// 上报扣款
	// isPayed 0 已收款    1 未收款
	public boolean bll0042(String strErrPsamID, String strErrType, ErrorInfoBean bean, int isPayed) 
	{
		try
		{
			// 拼装主命令
			sendPackage = new Package();
			sendPackage.setServiceCommandID("FFFF"); // recivePackage.getServiceCommandID()
			sendPackage.setSubId("42");
			sendPackage.setId("00");
			// 拼装子命令体
			Buffer buffer = new Buffer();
			buffer.add(Commen.hexstr2byte(strErrType));			// 交易类型
			buffer.add((byte)bean.no.length());				// 交易流水号
			buffer.add(bean.no.getBytes());
			buffer.add(Commen.hexstr2byte(bean.reason));		// 异常交易原因
			buffer.add(Commen.hexstr2byte(bean.crc));			// 异常交易CRC
			buffer.add((byte)(bean.state.getBytes()[0]-0x30));	// 处理请求
			sendPackage.addUnit("020B", buffer.getBytes());
			Log.e("", "异常交易流水: " + bean.no + ", 类型：" 	+ strErrType + ", CRC: " + bean.crc + ", 原因: " + bean.reason);
			// 已收款 、未收款
			sendPackage.addUnit("020E", (byte)isPayed);
			// 数据收发
			int ret = send(sendPackage);
			if(ret == ReturnEnum.SUCCESS) 
			{
				ret = receiveOnePackage("0042");
			}
			if(ret != ReturnEnum.SUCCESS) 
			{
				String ss = "上报扣款结果失败!";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				setLastknownError(ss);
				Log.e("", ss);
				return false;
			}
			
			// 异常交易处理状态
			return procErrorDealState(strErrPsamID, strErrType, bean);
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return false;
		}
	}
}
