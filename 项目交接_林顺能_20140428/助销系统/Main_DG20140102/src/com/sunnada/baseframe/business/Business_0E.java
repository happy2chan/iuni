package com.sunnada.baseframe.business;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import sunnada.jni.Commcenter;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;

import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.Productzifei;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Rule;
import com.sunnada.baseframe.database.ErrorDealAdapter;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.equipment.IDevice;
import com.sunnada.baseframe.equipment.IntellgentDeviceOpr;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.Log;
import com.sunnada.baseframe.util.StringUtil;

// 店长推荐协议
public class Business_0E extends BaseBusiness 
{
	public static final String						TITLE_STR			= "店长推荐业务办理提示";
	public static final String						TAG					= "店长推荐";
	
	public IDevice									mDeviceInc			= null;
	public static final String						NORULES_CODE		= "norules_code";
	public static final String						NORULES_NAME		= "norules_name";
	
	public static int								mPayType			= 0;						// 付费类型
	
	public static List<Object>						mListNoRules 		= new ArrayList<Object>();	// 靓号规则列表
	public static String							mStrNetProtocol 	= null;					  	// 入网协议
	
	public static int								mNumberTotal		= 0;						// 选号总数
	//public static int								mNumberStart		= 1;						// 选号起始
	public static int								mNumberCnt			= 12;						// 每页显示行数
	
	public static ArrayList<String>					mNumberList			= new ArrayList<String>();	// 选号列表
	public static List<String>						mNumberDetailList	= new ArrayList<String>();	// 选号详情列表
	public static List<String>						mNumberFeeList		= new ArrayList<String>();	// 号码费用档次列表
	
	public String									mSelectNum;						// 预占号码
	public static int								mSimType 			= 0;		// 空卡类型  0x00-成卡 0x01白卡
	public static String							mStrIccid;						// 空卡卡号
	
	public static int 								mPackageCode;					// 套餐编码
	public static String							mStrPackageDetail;				// 套餐详情
	public static int								mFeeCode;						// 资费编码
	public static String							mStrFeeDetail;					// 费用详情
	public static String							mStrFeeTotal;					// 总费用
	public static int								mContractFlag 		= 0x01;		// 是否参加活动?
	public static String 							mActionCode;					// 活动编码
	public static int 								mContractCode;					// 合约计划编码
	public static int 								mContractDate;					// 合约计划期限
	public static String 							mContractName;					// 合约计划名称
	public static String 							mContractContent;				// 合约计划详情
	public static String 							mStrTermainalID;				// 终端串号
	
	public static String							mStrCustType;					// 证件类型
	public static String							mStrIdcardDeadline;				// 证件有效日期
	public static String							mStrIdcardName;					// 证件姓名
	public static String							mStrIdcardNo;					// 证件号码
	public static byte								mIcardSex;						// 证件性别
	public static String							mStrIdcardAddr;					// 证件地址
	
	public static String							mStrRecPerson;					// 推荐人
	public byte										mTranceType;					// 业务类型
	public String									mPassword;						// 输入的密码
	public String									mUserId;						// 输入的工号
	public byte[]									mBllnum;						// 业务流水号byte[]
	public String 									mStrBllnum;						// 业务流水号String
	public String									mTjPersonNo;					// 推荐人编码
	public String									mSmsNum; 						// 短信中心号码
	public String									mStrIM;							// IMSI号码String
	public byte[]									mIMSI;							// IMSI号码byte[]	
	public String				 					mPicFolder; 					// 暂存图片文件的文件夹名
	public static List<Productzifei>				mProductZifei 	= new ArrayList<Productzifei>();
	public static int								mPosOptType	= 0;				// mispos类型
	public int 										mReWriteCount 	= 3;			// 重写白卡次数
	public boolean									mIsWritting 	= false;		// 是否正在写卡
	public String									mLastKnownError;				// 错误信息
	public String									mTranceTime;
	
	public Business_0E(Context context, IEquipmentService equipmentService, ISocketService socketService, IDataBaseService dataBaseService) 
	{
		super(context, equipmentService, socketService, dataBaseService);
		mDeviceInc = new IntellgentDeviceOpr(context);
		mTranceType = 0x09;
	}
	
	// 获取靓号规则
	// 返回： 靓号规则列表和入网协议
	public boolean bll0E01() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("01");
		sendPackage.setId("0E");
		sendPackage.addUnit("0201", new byte[] {0x00, 0x00, 0x00, 0x00});
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取靓号规则失败!\n失败原因: [数据发送失败]";
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		ret = receiveOnePackage("0E01");
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取靓号规则失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		// 获取靓号规则
		if(recivePackage.containsKey("0501") == false)
		{
			String ss = "获取靓号规则失败!\n失败原因: [服务器响应数据非法, 缺少0501命令单元]";
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0501");
			int offset = 0;
			
			// 靓号规则列表是否有数据
			int flag = unit[offset++];
			if(flag == 0x01)
			{
				// 靓号规则长度
				offset += 2;
				// 靓号规则总数
				mListNoRules.clear();
				int nTotal = unit[offset++];
				for(int i=0; i<nTotal; i++) 
				{
					Rule rule = new Rule();
					//Map<String, String> map = new HashMap<String, String>();
					// 靓号规则编码
					int code = unit[offset++]&0xFF;
					rule.setId(code);
					///map.put(NORULES_CODE, String.valueOf(code));
					// 靓号规则名称
					int len = unit[offset++]&0xFF;
					String temp = StringUtil.encodeWithGBK(unit, offset, len);
					rule.setContent(temp);
					offset += len;
					mListNoRules.add(rule);
					//map.put(NORULES_NAME, temp);
					//mListNoRules.add(map);
				}
			}
		}
		
		// 入网协议
		if(recivePackage.containsKey("049E") == false) 
		{
			String ss = "获取靓号规则失败!\n失败原因: [服务器响应数据非法, 缺少049E命令单元]";
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("049E");
			int offset = 0;
			// 入网协议长度
			int len = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 入网协议
			mStrNetProtocol = StringUtil.encodeWithGBK(unit, offset, len);
			offset += len;
		} 

		// 获取入网资费
		if (recivePackage.containsKey("049C"))
		{
			int haveData = 0x00;
			int pos = 0;
			int total = 0;
			int len = 0;
			byte[] unit = recivePackage.get("049C");
			haveData = unit[0];
			if (haveData == 0)
			{
				return false;
			}
			pos = 3;
			total = unit[pos]; 
			pos++;
			mProductZifei.clear();
			if (total > 0)
			{
				// pdaAdapter.deletebytype(ParamType.CURFEE3G);
			}
			for (int i = 0; i < total; i++)
			{
				Productzifei pz = new Productzifei();
				len = 0;
				// int code = unit[pos];
				pz.setCode(unit[pos]);
				pos++;
				len = unit[pos];
				pos++;
				byte[] info = new byte[len];
				System.arraycopy(unit, pos, info, 0, len);
				pz.setName(StringUtil.encodeWithGBK(info));
				pos += len;
				mProductZifei.add(pz);
				Log.e("haoka", "编码:" + pz.getCode() + "; 内容:" + pz.getName());
			}
		}
		return true;
	}
	
	// 获取号码
	// nPayType: 	0x00-不限 0x01-3G预付费号码 0x02-3G后付费号码
	// nSelRule1:	0x00-不启用该条件查询 0x01-使用靓号规则 0x02-启用号码后4位
	// nNoRuleCode:	靓号规则编码
	// strLast4:	号码后4位
	// nSelRule2:	0x00-不启用号段查询 0x01-启用号段查询
	// strFirst3:	号段
	// nSelRule3:	0x00-不限 0x01-0~50 0x02-50~100 0x03-100~300 0x04-300~500 0x05-500~1000 0x06-1000~5000 0x07-5000以上
	// 返回: 		号码列表、号码详情列表和号码费用档次列表
	public boolean bll0E02(int nPayType, int nSelRule1, int nNoRuleCode, String strLast4, int nSelRule2, 
							String strFirst3, int nSelRule3, short starNum, boolean is0491) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("02");
		sendPackage.setId("0E");
		
		Buffer buffer = new Buffer();
		if(is0491)
		{
			// 0491
			buffer.add("0F00");								// 数据域
			buffer.add((byte)nPayType);						// 付费类型
			// 条件1
			buffer.add((byte)nSelRule1);					// 条件1 0x00-不启用 0x01-启用靓号规则 0x02-启用号码后4位
			if(nSelRule1 == 0x01)
			{
				buffer.add((byte)nNoRuleCode);				// 靓号规则编码
			}
			else if(nSelRule1 == 0x02)
			{
				buffer.add(strLast4.getBytes());			// 号码后4位
			}
			else if(nSelRule1 == 0x03)						// 同时满足两种
			{
				buffer.add((byte)nNoRuleCode);				// 靓号规则编码
				buffer.add(strLast4.getBytes());			// 号码后4位
			}
			// 条件2
			buffer.add((byte)nSelRule2);					// 条件2 0x00-不启用号段 0x01-启用号段
			buffer.add(strFirst3.getBytes());				// 号段
			// 条件3
			buffer.add((byte)nSelRule3);
			sendPackage.addUnit("0491", buffer.getBytes());
		}
		// 0492
		buffer.clear();
		buffer.add("0700");									// 数据域
		buffer.add((short)mNumberTotal);					// 号码总数
		buffer.add((short)starNum);							// 选号起始
		buffer.add((byte)mNumberCnt);						// 显示行数
		sendPackage.addUnit("0492", buffer.getBytes());
		
		// 数据发收
		int ret = send(sendPackage);
		if(ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0E02");
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取选号列表失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		
		// 选号列表
		if(recivePackage.containsKey("0492") == false)
		{
			String ss = "获取选号列表失败!\n失败原因: [服务器响应数据非法, 缺少0492命令单元]";
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0492");
			int offset = 0;
			int total = 0;
			// 数据域
			int map = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 选号总数
			if((map&0x01) != 0)
			{
				mNumberTotal = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
				offset += 2;
			}
			// 选号起始
			if((map&0x02) != 0)
			{
				offset += 2;
			}
			// 显示行数
			if((map&0x04) != 0)
			{
				total = unit[offset++];
			}
			mNumberList.clear();							
			mNumberDetailList.clear();
			// 选号列表
			if((map&0x08) != 0)
			{
				for(int i=0; i<total; i++)
				{
					// 号码长度
					int len = unit[offset++];
					// 号码
					String strTel = StringUtil.encodeWithGBK(unit, offset, len);
					Log.i("OE telnum:", strTel);
					offset += len;
					mNumberList.add(strTel);
				}
			}
			// 号码详情列表
			if((map&0x10) != 0)
			{
				for(int i=0; i<total; i++) 
				{
					// 号码详情长度
					int len = unit[offset++];
					// 号码详情内容
					String strTelDetail = StringUtil.encodeWithGBK(unit, offset, len);
					offset += len;
					mNumberDetailList.add(strTelDetail);
				}
			}
			// 号码费用档次列表
			if((map & 0x20) != 0)
			{
				mNumberFeeList.clear();
				for(int i=0; i<total; i++)
				{
					// 号码费用档次内容长度
					int len = unit[offset++];
					// 号码费用档次内容
					String strFeeDetail = StringUtil.encodeWithGBK(unit, offset, len);
					offset += len;
					mNumberFeeList.add(strFeeDetail);
				}
			}
			return true;
		}
	}
	
	// 号码预占
	// nPayType: 		0x00-无号码类型 0x01-3G后付费号码 0x02-3G预付费非套包 0x03-3G预付费套包 
	// nSelType: 		0x00-禁止使用 0x01-根据选号规则进行 0x02-自定义 0x03-随机号码列表 0x04-预约号码 0x05-靓号类型 0x06-关键字索引
	// nOccupyStatus: 	0x00-不预占 0x01-预占 0x02-预订(未付费) 0x03-预订(付费) 0x04-释放资源 0x05-变更预占
	// nFeeLevel: 		费用档次 (接入说这个选号入网才有用)
	public boolean bll0E03(String strTelNum, int nPayType, int nSelType, int nOccupyStatus, int nFeeLevel) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("03");
		sendPackage.setId("0E");
		
		// 预占手机号码
		sendPackage.addUnit("0205", strTelNum.getBytes());
		// 号码类型和选号规则类型
		Buffer buffer = new Buffer();
		buffer.add("0300");
		buffer.add((byte)nPayType);
		buffer.add((byte)nSelType);
		sendPackage.addUnit("0491", buffer.getBytes());
		// 预占状态
		sendPackage.addUnit("0211", new byte[] {(byte)nOccupyStatus});
		// 号码费用档次
		buffer.clear();
		buffer.add((short)nFeeLevel);
		sendPackage.addUnit("0212", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret != ReturnEnum.SUCCESS) 
		{
			return false;
		}
		ret = receiveOnePackage("0E03");
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "预占号码失败!\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		return true;
	}
	
	// 预占合约计划资源
	// contractDate   合约期限编码
	// actionType	     活动类型 0x02 购机入网送话费 0x03存话费送手机 20131030
	// packageCode    套餐编码
	// actionCode     活动编码
	// strTermainalID 终端串号
	public boolean bll0E04(int contractDate, int actionType, String packageCode, String actionCode, String strTermainalID) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("04");
		sendPackage.setId("0E");
		try
		{
			mPackageCode = Integer.parseInt(packageCode);
			mActionCode = actionCode;
		}
		catch (Exception e) 
		{
			String ss = "预占合约计划资源失败!\n失败原因: [服务器下发数据有误！]";
			setLastknownError(ss);
			return false;
		}
		
		Buffer buffer = new Buffer();
		buffer.add("1F00");							// 数据域 0x0003还是0x000F等待调试而定
		//buffer.add((byte)0x04);					// 合约计划编码长度
		//buffer.add(mContractCode);				// 合约计划编码
		buffer.add((byte)contractDate);				// 合约期限编码
		buffer.add((byte)actionType);				// 活动类型
		buffer.add((byte)packageCode.length());		// 套餐编码长度
		buffer.add(packageCode.getBytes());			// 套餐编码
		buffer.add((byte)actionCode.length());		// 活动编码长度
		buffer.add(actionCode.getBytes());			// 活动编码
		buffer.add((byte)strTermainalID.length());	// 终端串号长度
		buffer.add(strTermainalID.getBytes());		// 终端串号
		sendPackage.addUnit("0503", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret != ReturnEnum.SUCCESS) 
		{
			return false;
		}
		ret = receiveOnePackage("0E04");
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "预占合约计划资源失败!\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		if (!recivePackage.containsKey("0503"))
		{
			String ss = "预占合约计划资源!\n失败原因: [服务器响应数据非法, 缺少0503命令单元]";
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0503");
			int pos = 2;
			int len;
			int dataIsNull = Commen.bytes2int(unit, pos, 1);
			pos++;
			if(dataIsNull == 0)
			{
				return true;
			}
			else
			{
				// 解析合约计划名称、内容
				len = Commen.bytes2int(unit, pos, 1);
				pos++;
				mContractName = StringUtil.encodeWithGBK(unit, pos, len);
				pos += len;
				System.out.println("合约计划名称:" + mContractName);
				len = Commen.bytes2int(unit, pos, 2);
				pos += 2; 
				mContractContent = StringUtil.encodeWithGBK(unit, pos, len);
			}
		}
		return true;
	}
	
	// 获取费用信息
	public boolean bll0E05() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("05");
		sendPackage.setId("0E");
		
		// 身份证校验信息, 3G后付费省略
		if (mPayType != 5)
		{
			Buffer buffer = new Buffer();
			buffer.add("0200");
			buffer.add(mStrIdcardDeadline.getBytes());			// 证件有效日期
			buffer.add((byte)mStrIdcardNo.length());			// 证件号码长度
			buffer.add(mStrIdcardNo.getBytes());				// 证件号码
			sendPackage.addUnit("036D", buffer.getBytes());
		}
		// 04D1
		Buffer buffer = new Buffer();
		int map = 0x2738;
		// 数据域, 3G后付费不含推荐人
		if (mPayType == 5 && StringUtil.isEmptyOrNull(mStrRecPerson)) 
		{
			Log.log("3G后付费不含推荐人");
			buffer.add("3827");
			map = 0x2738;
		}
		// 数据域, 3G后付费含推荐人
		else if (mPayType == 5 && !StringUtil.isEmptyOrNull(mStrRecPerson)) 
		{
			Log.log("3G后付费含推荐人");
			buffer.add("3867");
			map = 0x6738;
		}
		else
		{
			buffer.add("387F");
			map = 0x7F38;
		}
		
		// 手机号码
		if((map & 0x08) != 0)
		{
			buffer.add(mSelectNum.getBytes());
		}
		// 空卡信息
		if((map & 0x10) != 0) 
		{
			Log.log("空卡卡号: " + mStrIccid);
			buffer.add((byte)mSimType);			// 卡类型: 0x00-成卡 0x01白卡
			buffer.add((byte)mStrIccid.length());
			buffer.add(mStrIccid.getBytes());
		}
		// 套餐编码
		if((map & 0x20) != 0)
		{
			buffer.add(mPackageCode);
		}
		// 证件类型
		if((map & 0x100) != 0) 
		{
			if (StringUtil.isEmptyOrNull(mStrCustType)) 
			{
				buffer.add((byte)(mStrIdcardNo.length()==15 ? 0x01:0x02));
			}
			else
			{
				buffer.add(mStrCustType);
			}
		}
		// 证件号码
		if((map & 0x200) != 0) 
		{
			buffer.add((byte)mStrIdcardNo.length());
			buffer.add(mStrIdcardNo.getBytes());
		}
		// 证件姓名
		if((map & 0x400) != 0)
		{
			try
			{
				byte[] name = mStrIdcardName.getBytes("GBK");
				buffer.add((byte)name.length);
				buffer.add(name);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		// 证件性别 0x01-女 02-男
		if((map & 0x800) != 0) 
		{
			buffer.add((byte)mIcardSex);
		}
		// 证件地址
		if((map & 0x1000) != 0) 
		{
			try
			{
				byte[] addr = mStrIdcardAddr.getBytes("GBK");
				buffer.add((byte)addr.length);
				buffer.add(addr);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		// 资费编码
		if((map & 0x2000) != 0) 
		{
			Log.log("资费编码: " + mFeeCode);
			buffer.add((byte)mFeeCode);
		}
		// 推荐人信息
		if((map & 0x4000) != 0)
		{
			if (!StringUtil.isEmptyOrNull(mStrRecPerson))
			{
				buffer.add((byte)mStrRecPerson.length());
				buffer.add(mStrRecPerson.getBytes());
			}
		}
		sendPackage.addUnit("04D1", buffer.getBytes());
		
		sendPackage.addUnit("0371", new byte[] { 0x01 });
		sendPackage.addUnit("0374", new byte[] { 0x01 });
		// 添加合约计划标识
		sendPackage.addUnit("0505", (byte)mContractFlag);
		
		// 数据收发
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0E05");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取费用失败,将退出当前流程！\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			bll0E16();
			return false;
		}
		// 套餐详情
		if (!recivePackage.containsKey("0495"))
		{
			String ss = "获取费用失败,将退出当前流程！\n失败原因: [服务器响应数据非法, 缺少0495命令单元]";
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			bll0E16();
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0497");
			int pos = 2;
			int bitmap = Commen.bytes2int(new byte[] {unit[0], unit[1]});
			// 详情类型
			if ((bitmap & 0x0001) != 0)
			{
				pos++;
			}
			// 详情编码
			if ((bitmap & 0x0002) != 0)
			{
				pos += 4;
			}
			// 详情内容
			if ((bitmap & 0x0004) != 0)
			{
				int len = Commen.bytes2int(new byte[] {unit[pos], unit[pos+1]});
				pos += 2;
				// 套餐详情
				mStrPackageDetail = StringUtil.encodeWithGBK(unit, pos, len);
				pos += len;
				Log.e("", "套餐详情: " + mStrPackageDetail);
			}
		}
		
		// 费用信息文本
		if (!recivePackage.containsKey("0497"))
		{
			String ss = "获取费用失败,将退出当前流程！\n失败原因: [服务器响应数据非法, 缺少0497命令单元]";
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			bll0E16();
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0497");
			int len = 0;
			int pos = 0;
			map = Commen.bytes2int(new byte[] {unit[0], unit[1]});
			pos += 2;
			if((map & 0x0001) != 0)  
			{
				len = Commen.bytes2int(unit, pos, 1);
				pos++;
				mStrFeeTotal = StringUtil.encodeWithGBK(unit, pos, len);
				pos += len;
				Log.e("", "总费用: " + mStrFeeTotal);
			}
			if((map & 0x0002) != 0) 
			{
				len = Commen.bytes2int(unit, pos, 2);
				pos += 2;
				mStrFeeDetail = StringUtil.encodeWithGBK(unit, pos, len);
				pos += len;
			}
			Log.e("", "费用详情: " + mStrFeeDetail); 
		}
		
		// 身份证校验信息
		return true;
	}
	
	// 获取流水号
	public boolean bll0E06()
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("06");
		sendPackage.setId("0E");
		sendPackage.addUnit("0202", new byte[] { mTranceType }); // 业务
		// sendPackage.addUnit("0215", this.PWDEncryption(this.password, indata));
		// sendPackage.addUnit("0215", this.PWDEncryption(this.password));
		Buffer buffer = new Buffer();
		buffer.clear();
		buffer.add((byte)PWDEncryption(mPassword).length);
		buffer.add(PWDEncryption(mPassword));
		//buffer.add((byte) mPassword.length());
		//buffer.add(mPassword.getBytes());
		sendPackage.addUnit("0215", buffer.getBytes());
		// sendPackage.addUnit("0217", (byte) this.postype);

		// 发收
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0E06");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取流水号失败!,将退出当前流程.\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}

		// 获流水号
		if (!recivePackage.containsKey("0202"))
		{
			String ss = "获取流水号失败!将退出当前流程.\n失败原因: [服务器响应数据非法, 缺少0202命令单元]";
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0202");
			int len = unit[1];
			mBllnum = new byte[len];
			System.arraycopy(unit, 2, mBllnum, 0, len);
			try
			{
				mStrBllnum = new String(mBllnum, "ASCII");  
				Log.e("业务流水号为:", mStrBllnum);
			}
			catch (UnsupportedEncodingException e)
			{
				String ss = "获取流水号失败!将退出当前流程.\n失败原因: [未知错误]";
				//DialogUtil.showMessage(TITLE_STR, ss);
				setLastknownError(ss);
				return false;
			}
		}
		return true;
	}
	
	// 开户
	public boolean bll0E07()
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("07");
		sendPackage.setId("0E");
  
		Buffer buffer = new Buffer(); 
		buffer.add("0200");
		buffer.add(mStrIdcardDeadline.getBytes()); 			// 身份证有效期
		buffer.add((byte)mStrIdcardNo.length());			// 新增
		buffer.add(mStrIdcardNo.getBytes());				// 新增
		sendPackage.addUnit("036D", buffer.getBytes());

		buffer.clear();
		buffer.add("3F7F"); 								// 数据域 待确认 xuhe
		buffer.add("0000"); 								// 金融CRC
		buffer.add(mTranceType);							// 业务类型
		//buffer.add((byte)mBllnum.length); 				// 流水号长度
		//buffer.add(mBllnum); 								// 流水号
		buffer.add((byte)(encryptSerialNumber(mBllnum)).length); // 流水号加密后长度
		buffer.add(encryptSerialNumber(mBllnum)); 			// 流水号加密
		
		buffer.add("00"); 									// 扣款状态 0扣款 1查询
		buffer.add(mSelectNum.getBytes()); 					// 手机号
		buffer.add((byte) mSimType);						// uint1:卡类型 0成卡 1白卡
		buffer.add((byte) mStrIccid.length());				// 增加：黄杰桢
		buffer.add(mStrIccid.getBytes());					// uint1:卡号长度 strN sim卡号内容
		buffer.add(mPackageCode); 							// 套餐编码 4位
 
		if ("".equals(mStrCustType)) 
		{
			buffer.add((byte) (mStrIdcardNo.length() == 15 ? 0x01 : 0x02));
		}
		else
		{
			buffer.add(mStrCustType); 						// 从前面传过来的
		}
		buffer.add((byte) mStrIdcardNo.getBytes().length); 	// 身份证长度
		buffer.add(mStrIdcardNo.getBytes()); 				// 证件号码

		try
		{
			int len = 0; 
			byte[] name = mStrIdcardName.getBytes("GBK");
			len = name.length;
			buffer.add((byte) len);
			buffer.add(name);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		buffer.add(mIcardSex); 				// 01 女 02男 

		try
		{
			int len = 0; 
			byte[] name = mStrIdcardAddr.getBytes("GBK");
			len = name.length;
			buffer.add((byte) len); 	// 地址
			buffer.add(name);
		}
		catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		buffer.add((byte)mFeeCode); 				// 资费编码

		if (!StringUtil.isEmptyOrNull(mTjPersonNo))
		{
			int tjlength = mTjPersonNo.length();
			buffer.add((byte) tjlength); 			// 推荐人长度
			buffer.add(mTjPersonNo.getBytes()); 	// 推荐人名称
		}
		else
		{
			buffer.add("00"); // 推荐人编码长度
		}

		// pdu.put("04D1", buffer.getbytes());
		byte[] tt = buffer.getBytes(); 				// CRC计算
		byte[] bb = new byte[tt.length - 4];
		System.arraycopy(tt, 4, bb, 0, tt.length - 4);
		byte[] crc = Commcenter.CalcCrc16(bb, (char) 0);
		tt[2] = crc[0];
		tt[3] = crc[1];
		sendPackage.addUnit("04D1", tt);
		sendPackage.addUnit("0371", new byte[]{ 0x01 });
		sendPackage.addUnit("0374", new byte[]{ 0x01 });

		// 预先记录异常
		mStrErrBill 	=  mStrBllnum; 									// 流水
		mStrErrReason 	= Commen.hax2str(new byte[] { 0x0C }); 			// 记录原因
		mStrErrType 	= Commen.hax2str(new byte[] { mTranceType }); 	// 交易类型
		mStrErrCrc 	 	= Commen.hax2str(new byte[] { crc[0], crc[1] });
		ErrorDealAdapter.setErrorInfo(context, mStrErrPsamID, mStrErrType, mStrErrBill, mStrErrReason, mStrErrCrc, "1");
		mBusinessNum.insertData(
				App.getUserNo(), 
				mSelectNum, 
				"店长推荐", 
				mStrFeeTotal, 
				"交易失败", 
				mStrErrBill,
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()), 
				mStrFeeTotal, 
				mTranceType);

		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			setLastknownError(socketService.getLastKnowError());
			return false;
		}

		ret = receiveOnePackage("0E07");
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "开户请求办理失败!将退出当前流程.\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		// 0203
		if(!recivePackage.containsKey("0203"))
		{
			String ss = "开户请求办理失败!将退出当前流程.\n失败原因: [服务器响应数据非法, 缺少0203命令单元]";
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			
		}
		
		// 重置交易时间
		mTranceTime = "";
		if(!recivePackage.containsKey("0204"))
		{
			String ss = "开户请求办理失败!将退出当前流程.\n失败原因: [服务器响应数据非法, 缺少0204命令单元]";
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0204");
			if(unit.length >= 14)
			{
				mTranceTime = StringUtil.encodeWithGBK(unit, 0, 14);
				mTranceTime = mTranceTime.substring(0, 4)+"-"+
							  mTranceTime.substring(4, 6)+"-"+
						      mTranceTime.substring(6,8)+" "+
						      mTranceTime.substring(8, 10)+":"+
						      mTranceTime.substring(10, 12)+":"+
						      mTranceTime.substring(12, 14);
			}
			else
			{
				mTranceTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
			}
		}
		
		// 0498
		if(!recivePackage.containsKey("0498"))
		{
			String ss = "开户请求办理失败!将退出当前流程.\n失败原因: [服务器响应数据非法, 缺少0498命令单元]";
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			
		}
		return true;
	}
	
	// 获取ISMI
	public boolean bll0E08()
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("08");
		sendPackage.setId("0E");

		Buffer buffer = new Buffer();
		buffer.add("0F00"); 						// 数据域
		buffer.add((byte) 0x00); 					// 第一次获取IMSI:0x00
		buffer.add((byte) mStrIccid.length()); 		// ICCID号长度
		buffer.add(mStrIccid.getBytes());
		buffer.add((byte) mStrIccid.length()); 		// ICCID号长度
		buffer.add(mStrIccid.getBytes());
		buffer.add((byte) mSelectNum.length()); 	// 手机号码长度
		buffer.add(mSelectNum.getBytes());

		sendPackage.addUnit("04B0", buffer.getBytes());
		sendPackage.addUnit("04B2", (byte) 0x01);
		
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}

		ret = receiveOnePackage("0E08");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取IMSI失败!\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}

		if (recivePackage.containsKey("04B0"))
		{
			byte[] buf = recivePackage.get("04B0");
			int pos = 0;
			int len = 0;
			int map = Commen.bytes2int(buf, pos, 2);
			pos += 2;

			if ((map & 0x0001) != 0)
			{
				len = buf[pos];
				pos += 1;
				String iccid = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("新ICCID号码为:" + iccid);
			}

			if ((map & 0x0002) != 0)
			{
				len = buf[pos];
				pos += 1;
				String iccid = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("旧ICCID号码为:" + iccid);
			}
			if ((map & 0x0004) != 0)
			{
				len = buf[pos];
				pos += 1;
				String str = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("手机号码为:" + str);
			}
			if ((map & 0x0008) != 0)
			{
				Log.log("文弄说这个数据不会有");
			}

			if ((map & 0x0010) != 0)
			{
				Log.log("文弄说这个数据不会有");
			}
			if ((map & 0x0020) != 0)
			{
				len = buf[pos]; 
				pos += 1;
				mSmsNum = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("短信中心号码为:" + mSmsNum);
			}
			if ((map & 0x0040) != 0)
			{
				len = buf[pos];
				pos += 1; 
				mStrIM = StringUtil.encodeWithGBK(buf, pos, len);
				mIMSI = new byte[len];
				System.arraycopy(buf, pos, mIMSI, 0, len);
				pos += len;
				Log.log("imsi号码为:" + mStrIM);
			}
			if ((map & 0x0080) != 0)
			{
				len = buf[pos];
				Log.log("sim卡类型为:" + len + " 其中1为2G	2为3G");
			}
		}
		else
		{
			String ss = "获取IMSI失败!\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}
		return true;
	}

	// 上报写卡结果
	// 成功为 0x00 失败为0x01
	public boolean bll0E09(byte result)
	{
		try
		{
			sendPackage = new Package();
			sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
			sendPackage.setSubId("09");
			sendPackage.setId("0E");

			Buffer buffer = new Buffer();
			buffer.add("3F00"); 					// 数据域
			buffer.add(result); 					// 开卡结果
			buffer.add((byte) 0x00); 				// 业务状态
			buffer.add((byte) mStrIccid.length()); 	// ICCID号长度
			buffer.add(mStrIccid.getBytes());
			buffer.add((byte) mStrIccid.length()); 	// ICCID号长度
			buffer.add(mStrIccid.getBytes());
			buffer.add((byte) mIMSI.length); 		// ISMI号码
			buffer.add(mIMSI);
			buffer.add((byte) mSelectNum.length()); // 手机号码长度
			buffer.add(mSelectNum.getBytes());
			sendPackage.addUnit("04B1", buffer.getBytes());
			
			int ret = send(sendPackage);
			if (ret != ReturnEnum.SUCCESS)
			{
				return false;
			}

			ret = receiveOnePackage("0E09");
			if (ret != ReturnEnum.SUCCESS)
			{
				/*
				String ss = "上报开卡结果失败!\n";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
				{
					ss += "失败原因: [" + socketService.getLastKnowError() + "]";
				}
				//DialogUtil.showMessage(TITLE_STR, ss);
				*/
				setLastknownError(socketService.getLastKnowError());
				return false;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			/*
			String ss = "上报开卡结果失败!\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			*/
			setLastknownError(socketService.getLastKnowError());
			return false;
		}
		return true;
	}
	
	// 送开户成功信息成功接收确认包
	public boolean bll0E10()
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("10");
		sendPackage.setId("0E");
		Buffer buffer = new Buffer();
		buffer.add(mTranceType); 	// 业务类型
		buffer.add("1E"); 			// 流水长度:
		buffer.add(mBllnum); 		// 流水号:
		sendPackage.addUnit("0202", buffer.getBytes());

		ErrorDealAdapter.setErrorInfoState(context, 
				mStrErrPsamID, 
				mStrErrType, 
				Commen.hax2str(new byte[] {0x0D}), 
				"1", 
				mStrErrCrc, 
				mStrErrBill);
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0E10");
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "接收成功信息确认包失败!\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		
		// 更改交易记录状态
		mBusinessNum.updateBussinessNum(
				new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(Calendar.getInstance().getTime()), 
				"交易成功", 
				mStrFeeTotal, 
				mStrErrBill, 
				mStrFeeTotal);
		return true;
	}
	
	// 获取机型销售量
	// modelID 机型ID
	public String bll0E11(String modelID)
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("11");
		sendPackage.setId("0E");
		Buffer buffer = new Buffer();
		buffer.add("0100");
		buffer.add((byte)modelID.length());
		buffer.add(modelID.getBytes());
		sendPackage.addUnit("0801", buffer.getBytes());
		
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return null;
		}
		ret = receiveOnePackage("0E11");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取机型销售量失败!\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			//DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return null;
		}
		else
		{
			byte[] buf = recivePackage.get("0811");
			System.out.println("buf : "+Arrays.toString(buf));
			return Integer.toString(Commen.bytes2int(buf));
		}
	}
	
	// 退出号卡销售流程
	public boolean bll0E16()
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("16");
		sendPackage.setId("0E");

		sendPackage.addUnit("0001", "0201".getBytes());
		sendPackage.addUnit("049F", "01".getBytes());

		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		return true;
	}
	
	// 退出号卡销售流程  不撤单
	public boolean bll0E16(byte type)
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("16");
		sendPackage.setId("0E");

		sendPackage.addUnit("0001", "0201".getBytes());
		sendPackage.addUnit("049F", type);

		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		return true;
	}
	
	public boolean doOpenCard(Handler handler)
	{
		boolean flag;
		DialogUtil.showProgress("正在获取流水号...");
		flag = bll0E06();// 获取流水号
		if (!flag)
		{
			return false;
		}
		DialogUtil.showProgress("正在进行开户...");
		flag = bll0E07();// 号卡请求
		if (!flag)
		{
			return false;
		}
		if (mSimType == 1)
		{
			// 如果是白卡开户则上平台上获取
			flag = doWriteCard(handler);
		}

		if (flag)
		{
			// 发送开户成功确认
			flag = bll0E10();
			// 开户成功后，修改存放身份证信息的文件夹的名称为流水号---暂时屏蔽
			File oldFile = new File(mPicFolder);
			File newFile = new File(oldFile.getParent() + "/" + mStrBllnum);
			oldFile.renameTo(newFile);
			Log.log("发送开户成功过确认包");
			// this.printScrean();//显示交易结果----暂时屏蔽
		}
		return flag;
	}
	
	private boolean mFlag = true;
	// 写白卡
	public boolean doWriteCard(final Handler handler)
	{
		boolean flag = true;
		try
		{
			Log.log("开始获取卡数据...");
			DialogUtil.showProgress("正在获取写卡数据..."); 
			// 获取卡数据
			if(bll0E08())
			{
				while(mFlag)
				{ 
					if(mIsWritting)
					{
						continue;
					}
					if(!writeToCard())
					{
						if(mReWriteCount > 0)
						{
							DialogUtil.MsgBox("温馨提示", "写卡失败，是否重试？\n剩余重试次数：" + mReWriteCount, 
							"重试", new OnClickListener()
							{
								@Override
								public void onClick(View v)
								{
									mReWriteCount --;
									mIsWritting = false;
									Log.e("重试", "重试：" + mReWriteCount);
								}
							},
							"取消", new OnClickListener()
							{ 
								@Override
								public void onClick(View v)
								{
									mFlag = false;
								}
							}, new OnClickListener()
							{
								@Override
								public void onClick(View v)
								{
									mFlag = false;
								}
							});
						}
						else
						{
							//bll0E09((byte)0x01);
							//bll0E16();
							mFlag = false;
						}
						flag = false;
					}
					else
					{
						Log.e("物理写卡", "写卡成功！");
						flag = true;
						// 只有写卡成功流程才继续往下走
						break;
					}
				}
				DialogUtil.showProgress("正在上报开卡结果...");
				if(flag == false)
				{
					Log.e("写卡", "上报失败开卡结果！");
					bll0E09((byte)0x01);
					flag = false;
					if(mReWriteCount == 0)
					{
						setLastknownError("系统异常，写卡失败");
					}
					else
					{
						setLastknownError("写卡失败");
					}
				}
				else
				{
					Log.e("写卡", "上报成功开卡结果！");
					bll0E09((byte)0x00);
					flag = true;
				}
			}
			else
			{
				Log.e("3G", "获取写卡数据失败");
				String ss = "获取写卡数据失败";
				if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
				{
					ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
				}
				setLastknownError(ss);
				flag = false;
			}
		}
		catch (Exception e)
		{
			 e.printStackTrace();
			 String ss = "开户失败";
			 if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			 {
				ss += "\n失败原因: [未知错误]";
			 }
			 setLastknownError(ss);
			 flag = false;
		}
		return flag;
	}

	boolean bIsStart = false; 
	private boolean writeToCard()
	{
		mIsWritting = true;
		if(!bIsStart)
		{
			DialogUtil.MsgBox("温馨提示", "即将写卡，请勿拔卡！", "确定", new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					bIsStart = true;
				}
			}, "", null,
			new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					bIsStart = true;
				}
			});
		}
		
		while(!bIsStart)
		{
			
		}
		// 物理写卡 
		boolean flag = true;
		Log.log("开始写卡...");
		Log.log("开始写入IMSI...");
		if(mReWriteCount == 3)
		{
			DialogUtil.showProgress("正在写卡，请勿拔卡...");
		}
		else
		{
			DialogUtil.showProgress("正在进行第" + (3 - mReWriteCount) + "次写卡尝试，请勿拔卡...");
		}
		
		if (equipmentService.writeSimCard(mIMSI, (byte) 0x02))
		{
			if(StringUtil.isEmptyOrNull(mSmsNum))
			{
				Log.e("3G", "写卡成功"); 
				flag = true;
			}
			else
			{
				Log.log("开始写入短信中心号码..."); 
				if(equipmentService.writesmscentre(mSmsNum, (byte) 0x02)) 
				{ 
					 Log.e("3G", "写卡成功"); 
					 flag = true;
				} 
				else 
				{ 
					 Log.log("3G", "写短信中心号码失败"); 
					 String ss = "写卡失败\n失败原因: [写短信中心号码失败]";
					 setLastknownError(ss);
					 flag = false;
				}
			}
		}
		else 
		{ 
			 Log.log("3G", "写IMSI失败"); 
			 String ss = "写卡失败\n失败原因: [写IMSI失败]";
			 setLastknownError(ss);
		 	 flag = false; 
		}
		DialogUtil.closeProgress();
		return flag;
	}
	
	public String getLastknownError() 
	{
		if (StringUtil.isEmptyOrNull(mLastKnownError))
		{
			mLastKnownError = "未知错误";
		}
		return mLastKnownError;
	}

	public void setLastknownError(String lastKnownError)
	{
		this.mLastKnownError = lastKnownError;
	}
	
	public Package getReceivePackage()
	{
		return recivePackage;
	}
	
}