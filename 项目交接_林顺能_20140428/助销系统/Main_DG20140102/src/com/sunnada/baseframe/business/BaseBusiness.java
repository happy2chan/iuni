package com.sunnada.baseframe.business;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.bean.PSAM_Data;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.database.BusinessNum;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.Log;
import com.sunnada.baseframe.util.MyDES;
import com.sunnada.baseframe.util.StringUtil;

/**
 * File: BaseBusiness.java
 * Description: 
 * @author 丘富铨
 * @date 2013-3-8
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public class BaseBusiness
{
	public static String 			TITLE_STR 					= "温馨提示";
	public static String			TAG							= "提示";
	public static final int 		READ_TIMEOUT 				= 1000*60;				// 超时60s
	
	protected Context 				context 					= null;
	protected IEquipmentService 	equipmentService 			= null;
	protected ISocketService 		socketService 				= null;
	protected IDataBaseService 		dataBaseService 			= null;
	private   String 			    lastknownError 				= "未知错误";
	
	protected Package 				sendPackage 				= new Package();		// 发送包
	protected Package 				recivePackage 				= new Package();		// 接收包
	
	public byte						mTranceType;										// 交易类型
	public String					mStrBillNum					= null;					// 交易流水号
	public byte[]					mBillTime					= null;					// 交易时间
	
	public String 					mStrErrPsamID 				= null;
	public String 					mStrErrBill; 										// 异常上报需要的流水
	public String 					mStrErrReason; 										// 异常上报需要的原因
	public String 					mStrErrCrc 					= "0000"; 				// 异常上报需要的开始的crc
	public String 					mStrErrType 				= null;
	
	public BusinessNum				mBusinessNum				= null;					// 交易记录
	
	/**
	 * 构造函数
	 * @param context 传入当前context
	 * @param equipmentService	设备服务
	 * @param socketService	socket服务
	 */
	public BaseBusiness(Context context, IEquipmentService equipmentService, ISocketService socketService,IDataBaseService dataBaseService)
	{
		super();
		
		this.context = context;
		this.equipmentService = equipmentService;
		this.socketService = socketService;
		this.dataBaseService = dataBaseService;
		
		if(this.equipmentService != null)
		{
			Log.e("BaseBusiness", "equipmentService :" + this.equipmentService + "  psamInfo :" + this.equipmentService.getPsamInfo(0x00));
			// 异常PSAM卡卡号
			mStrErrPsamID = this.equipmentService.getPsamInfo(0x00).getPsamID();
		}
		else
		{
			mStrErrPsamID = "";
		}
		// 交易记录存储表
		mBusinessNum = new BusinessNum(context);
	}
	
	/**
	 * 发送数据包
	 * @param dataPackage
	 * @return 0-成功  -1-失败  -2超时
	 */
	public int send(Package dataPackage) 
	{
		try
		{
			if(equipmentService.isTerminateCanUse() == false)
			{
				Log.log("终端被停用，不发送数据");
				setLastknownError(getString(R.string.device_stop_use));
				return ReturnEnum.FAIL;
			}
			
			Log.log("equipmentService = " + equipmentService + " dataPackage = " + dataPackage);
			byte[] encryptData = equipmentService.psamEncrypt(dataPackage);
			// 删除接受队列中残余的相同id的包
			socketService.removeSameIdPackages(dataPackage.getId()+dataPackage.getSubId());
			int flag = socketService.send(encryptData, 0, encryptData.length);
			
			String err = socketService.getLastKnowError();
			if(!"Unknow Error".equalsIgnoreCase(err)) 
			{
				setLastknownError(err);
			}
			return flag;
		}
		catch (Exception e)
		{
			Log.log(e);
			setLastknownError(getString(R.string.data_send_error));
			return ReturnEnum.FAIL;
		}
	}
	
	/**
	 * 接收一个包
	 * @param id 主命令单元
	 * @return
	 */
	protected int receiveOnePackage(String id)
	{
		List<Package> packages = new ArrayList<Package>();
		int flag = ReturnEnum.FAIL;
		
		flag = receive(id.toUpperCase(), packages, READ_TIMEOUT);
		if(flag == ReturnEnum.SUCCESS)
		{
			recivePackage = packages.get(0);
		}
		return flag;
	}
	
	/**
	 * 接收一个包
	 * @param id 主命令单元
	 * @return
	 */
	protected int receiveOnePackage(String id, int timeout)
	{
		List<Package> packages = new ArrayList<Package>();
		int flag = ReturnEnum.FAIL;
		
		flag = receive(id.toUpperCase(), packages, timeout);
		if(flag == ReturnEnum.SUCCESS)
		{
			recivePackage = packages.get(0);
		}
		return flag;
	}
	
	/**
	 * 接收多个包
	 * @param id 主命令单元
	 * @return
	 */
	protected int receive(String id, List<Package> packages, int timeout)
	{
		int flag = ReturnEnum.FAIL;
		
		try
		{
			packages.clear();
			Log.log("开始接收"+id+"命令包");
			
			flag = socketService.receive(id, packages, timeout);
			if(flag == ReturnEnum.SUCCESS)
			{
				Log.log(id+"命令接收到"+packages.size()+"个数据包");
			}
			else
			{
				Log.log("接收"+id+"命令包失败");
			}
		}
		catch (Exception e)
		{
			Log.log(e);
		}
		setLastknownError(socketService.getLastKnowError());
		return flag;
	}
	
	/**
	 * 对登陆密码进行加密
	 * @param password
	 * @return
	 */
	protected byte[] PWDEncryption(String password) 
	{
		byte[] szDesInData = new byte[8];
		byte[] szEncryptedData = null;
		
		try
		{
			byte[] pswdArray = password.getBytes("ASCII");
			System.arraycopy(pswdArray, 0, szDesInData, 0, pswdArray.length);
			
			// 软加密
			if (equipmentService.isSoftEncrypt()) 
			{
				szDesInData = MyDES.des_crypt(equipmentService.readPsamExtKey(), szDesInData);
			} 
			else
			{
				szDesInData = MyDES.des_crypt(equipmentService.getLoginKey(), szDesInData);
			}
			
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(szDesInData);
			szDesInData = md5.digest();
			// 加密后数据
			szEncryptedData = new byte[szDesInData.length];
			System.arraycopy(szDesInData, 0, szEncryptedData, 0, szDesInData.length);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return szEncryptedData;
	}
	
	/**
	 * 加密流水号
	 * @param num 流水号
	 * @return
	 */
	public byte[] encryptSerialNumber(byte[] num)
	{
		if (num.length != 30)
		{
			Log.e("xuhe_error", "流水号加密失败，长度不是30位");
			return null;
		}
		byte[] f24 = new byte[24];
		byte[] f30 = new byte[30];
		System.arraycopy(num, 0, f24, 0, 24);

		try
		{
			if (equipmentService.isSoftEncrypt())
			{
				f24 = MyDES.des_crypt(PSAM_Data.extKey, f24); // 软件加密
				
			} else
			{
				if (equipmentService.getLoginKey() == null)
				{
					Log.d("encryptbllnum", "使用psam进行加密");
					equipmentService.psamEncryptByte(f24, (byte) 24, (byte) 0x00);// psam卡加密
				} 
				else
				{
					Log.d("encryptbllnum", "loginpwd:" + equipmentService.getLoginKey());
					f24 = MyDES.des_crypt(equipmentService.getLoginKey(), f24); // 使用签到密钥进行加密
				}
			}
		} 
		catch (Exception e)
		{
			Log.log(e);
			setLastknownError(getString(R.string.serial_number_encrypt_faild));
		}
		System.arraycopy(num, 0, f30, 0, 30);
		System.arraycopy(f24, 0, f30, 0, 24);
		return f30;
	}
	
	public String getLastknownError()
	{
		if (StringUtil.isEmptyOrNull(lastknownError))
		{
			lastknownError = "未知错误";
		}
		return lastknownError;
	}

	public void setLastknownError(String lastknownError)
	{
		this.lastknownError = lastknownError;
	}
	
	public int getLastknownErrorCode()
	{
		return socketService.getLastknownErrCode();
	}
	
	public String getString(int resId)
	{
		return context.getString(resId);
	}
	
	// 获取随机验证码, 万能验证码 971538
	public boolean bll0101(String strTelNum) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("01");
		sendPackage.setId("01");
		
		// 手机号码
		sendPackage.addUnit("0205", strTelNum.getBytes());
		
		int ret = send(sendPackage);
		if (ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0101");
		}
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取随机码失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))  
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		return true;
	}
	
	// 随机码验证
	public boolean bll0102(String strTelNum, String strRandom) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("02");
		sendPackage.setId("01");
		
		// 手机号码
		sendPackage.addUnit("0205", strTelNum.getBytes());
		// 随机码
		sendPackage.addUnit("0207", strRandom.getBytes());
		
		int ret = send(sendPackage);
		if (ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0102");
		}
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "随机码校验失败，请重新输入!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))  
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		return true;
	}
	
	// 凭条打印
	public void print(StringBuffer sPrintBuffer, String strBillNum) 
	{
		print(sPrintBuffer, strBillNum, false);
	}
	
	// 凭条打印
	public boolean print(StringBuffer sPrintBuffer, String strBillNum, boolean bEcardPrint) 
	{
		equipmentService.printLogoFile("logo.bin");
		equipmentService.setPrintFont(2);
		equipmentService.printText("         业务凭据\n");
		// 业务内容
		String tempStr = sPrintBuffer.toString() + "\n流水号为: \n" + (StringUtil.isEmptyOrNull(strBillNum)? mStrBillNum:strBillNum);
		//sPrintBuffer.append("\n流水号为: \n" + (StringUtil.isEmptyOrNull(strBillNum)? mStrBillNum:strBillNum));
		equipmentService.printText(tempStr);
		// 开始打印
		equipmentService.startPrint();
		
		// 电子卡附加信息
		if(bEcardPrint) 
		{
			sPrintBuffer.delete(0, sPrintBuffer.length());
			sPrintBuffer.append("\n充值方式: \n")
				.append("1.使用任意电话拨打10011(香港地区拨打66110011),按语音提示操作(使用联通电话直接拨打免费). \n")
				.append("2.网上充值,请登录中国联通网上营业厅www.10010.com.\n")
				.append("3.联通手机发送 \"C2#电子卡密码 #被充值卡号码\" 到10010为手机充值.");
			equipmentService.setPrintFont(1);
			equipmentService.printText(sPrintBuffer.toString());
			sPrintBuffer.delete(0, sPrintBuffer.length());
		}
		
		/*
		equipmentService.setPrintFont(1);
		equipmentService.printText("网上营业厅: www.10010.com");
		equipmentService.printText("手机营业厅: wap.10010.com");
		equipmentService.printText("短信营业厅: 发送短信至10010\n\n");
		*/
		
		// 广告语
		equipmentService.setPrintFont(2);
		equipmentService.printText("\n" + equipmentService.getAdverInfo());
		
		// 服务网点名称
		equipmentService.printText("\n服务网点名称: " + App.getShopName());
		// 打印时间
		equipmentService.printText("打印时间: " + 
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));
		
		// 提示信息
		equipmentService.setPrintFont(1);
		equipmentService.printText("\n温馨提示: 请妥善保护好此凭条信息,防止信息外泄");
		equipmentService.printText("此凭据为热敏打印, 放置时间过长可能出现信息缺失");
		
		equipmentService.printText("\n\n\n\n\n\n\n");
		// 开始打印
		equipmentService.startPrint();
		if(!equipmentService.isPrintSuccess()) 
		{
			return false;
		}
		return true;
	}
}
