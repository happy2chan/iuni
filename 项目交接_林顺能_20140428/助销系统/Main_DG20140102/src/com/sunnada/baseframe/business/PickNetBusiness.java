package com.sunnada.baseframe.business;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import sunnada.jni.Commcenter;
import android.content.Context;
import android.view.View;

import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.ConsumePlan;
import com.sunnada.baseframe.bean.ContractData;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.PackageInfo;
import com.sunnada.baseframe.bean.Product3G;
import com.sunnada.baseframe.bean.Productzifei;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Rule;
import com.sunnada.baseframe.bean.contractBuff;
import com.sunnada.baseframe.database.ErrorDealAdapter;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.Log;
import com.sunnada.baseframe.util.StringUtil;

public class PickNetBusiness extends BaseBusiness 
{
	public   String                              iccid; 
	public   String                              slectenum;                    // 被选中的号码
	private  String                              smsNum                  = "";
	private  String                              im                      = "";
	private  String                              mLastknownError 		 = "Unkown Error";
	public   byte[]                              IMSI;
	private  String                              search_type             = null;
	public   static String                       networkProtocol         = "";  // 入网协议
	public   String                              lineNum                 = "12";
	public   List<String>                        resnum                  = new ArrayList<String>(); // 号码列表
	public   List<String>                        telPrintBuffer          = new ArrayList<String>(); // 号码费用详情
	public   int                                 telnumTotal             = 0;
	private  String                              sjNumber                = "0";
	public   String                              packageName             = null; // 套餐名称
	public   int                                 ProductCode             = 0; // 套餐编码
	public   static List<Rule>                   numberRules             = new ArrayList<Rule>(); // 靓号规则
	public   String                              Identity; // 身份证号
	public   int                                 ProductPackCode         = 0;
	public   List<Product3G>                     productList             = new ArrayList<Product3G>();

	public   int                                 mContractflag           = 0x00; // 合约标识
	public   String                              mContractContent         = ""; // 合约计划内容
	public   static List<Productzifei>           productZifei            = new ArrayList<Productzifei>();
	
	// 判断是否选择了合约计划，是否打印合约计划详情
	public   boolean                             ishyok                  = false; // 在获取费用的时候判断是否达到获取合约计划详情的条件，true条件到达
	public   int                                 pid; // 获取产品id
	public   int                                 did; // 获取合约期限id

	// 合约计划信息
	public   static List<ContractData>           contractList            = new ArrayList<ContractData>();
	public   PackageInfo                         mPackageInfo            = null;
	public   int                                 mPayType               = 0; // 付费类型

	// 个人信息
	public   String                              IdentityEndtime;
	public   String                              tjperson                = null;
	public   int                                 openCardType            = 0;
	public   String                              cust_type               = ""; // 证件类型
	public   String                              username;
	public   String                              address;
	public   String                              sex;
	public   String                              feiyong;
	public   int                                 zife; // 资费编码
	
	// 金额
	public   String								 mStrErrMoney;
	
	// POS刷卡
	public   int                                 postype                 = 0; // 刷卡类型
															                  // 0-无
															                  // 1-本地
															                  // 2-刷卡
	public   int                                 posresult               = 0; // 刷卡结果
															                  // 0-成功
																              // 1-需要冲正
	public   String                              posinfo                 = null; // 8583发起协议
	public   byte[]                              pos8583                 = null; // 8583返回结果
	
	/* POS刷卡支付佣金数 */
	public   static int                          posCommFalg             = 0; // 佣金类别：第几期佣金
	public   static int                          posCommission           = 150; // 佣金额度
	public   static byte[]                       posBanKID; // 银行卡标示号
	public   static int                          posOptType              = 0; // mispos类型
										                                      // 1:银联
										                                      // 2:易宝

	public   int                                 meclen                  = 0; // 支付机构长度
	public   byte[]                              mecbusflowno            = null; // 支付机构流水
	public   int                                 chanlen                 = 0; // 支付渠道长度
	public   byte[]                              chanbusflowno           = null; // 支付渠道流水

	public   String                              res;                                //套餐详情
	public   String                              password                = "888888"; // 输入的密码
	public   String                              user_id                 = "888888"; // 输入的工号

	public   byte[]                              interCode               = null; // 国际区域编码
	public   byte[]                              bllnum;                         // 流水号
							                                                     // 改成public,以开放给界面逻辑层获取流水

	public   int                                 taxType                 = 0;    // 发票类型
	public   String                              taxNo                   = null; // 发票号码
	public   String                              taxContent              = null; // 发票内容

	public   String                              t_crc                   = "0000"; // 异常上报需要的开始的crc
	public   byte[]                              fuwumima;  
	public   String                              fuwupwd;
	public   byte                                simtype;
	public   boolean                             isHaoyuanTypeFirst      = true; // 是否是号源规则的第一次执行
	public   StringBuffer                        sPrintBuffer            = new StringBuffer(); // 打印信息
	public   String                              sp_packagename          = null; // 获取套餐的名称
	public   StringBuffer                        proPrintBuffer          = new StringBuffer(); // 费用信息
	public   String                              Production              = " ";
            
	public   static String                       serviceCommandID0C03    = "";
	public   String                              sPicFolder              = "";
	public   boolean                             state                   = true;

	public   int                                 productID;
	public   String                              contractNo              = "";// 终端串号
	public   int                                 deadlineId;

	// 注销项目新增
	public   int                                 m0D04AddQueryTerm       = 1;
	public   int                                 mSelectContractType     = 0;
	public   List<String>                        mTelFeeLevelsList       = new ArrayList<String>(); // 号码费用档次列表
	public   List<Integer>                       mConsumeLevels          = new ArrayList<Integer>(); // 消费档次
	public   List<List<ConsumePlan>>             mConsumePlan            = new ArrayList<List<ConsumePlan>>();// 消费计划
	public   List<Integer>                       mContractType           = new ArrayList<Integer>(); // 活动类型列表
	public   List<contractBuff>                  mContractBuffs          = new ArrayList<contractBuff>();// 合约期限列表
	public   String                              mContractName           = ""; // 合约名称
	
	// 开户
	public   int                                 mSelectSexUtilPos       = 0;  // 性别组被选中的位置
	public   int                                 mSelectUtilPos          = 0;  // 资费组被选中的位置

	// 写IMSI次数统计
	public   final int                           mTryTotal               = 3;  // 写卡失败可以重试的次数
	public   int                                 mTryCount               = 0;  // 写卡失败已经尝试的次数
	public   int                                 mFlag                   = -1;
	
	// 是否可以写卡
	private  boolean                             mIsStartWriteCard       = false;
	// 交易时间
	public   String                              mTranceTime             = "";
	
	public PickNetBusiness(Context context, IEquipmentService equipmentService,
			ISocketService socketService, IDataBaseService dataBaseService) 
	{
		super(context, equipmentService, socketService, dataBaseService);
		this.mTranceType = 0x09;
	}
	
	public String getNetworkProtocol() 
	{
		return networkProtocol;
	}

	/**
	 * 获取号码请求
	 * 
	 * search_type:选号类型 selectFirstType:第一个查询类型
	 * selectFirstStr:第一个查询条件,如果无数据传""不能传NULL selectSecondType:第二个查询类型
	 * selectSecondStr:第二个查询条件,如果无数据传""不能传NULL selectThirdType:第三个查询类型
	 * paytype:付费类型 0x00:无号码类型 0x01:3G后付费号码 0x02:3G预付费号码
	 */
	public int bll0D03(int selectFirstType, String selectFirstStr,
			int selectSecondType, String selectSecondStr, int selectThirdType,
			int paytype, short startnum, boolean is0491) 
	{
		int ret = ReturnEnum.FAIL;

		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());

		sendPackage.setSubId("03");
		sendPackage.setId("0D");
		Buffer buffer = new Buffer();
		
		if (is0491) 
		{
			buffer.add("0F00");

			buffer.add((byte) paytype);
			mPayType = paytype;
			// 第一类查询条件
			buffer.add((byte) selectFirstType);

			if (!selectFirstStr.equals("")) 
			{
				buffer.add((byte) Integer.parseInt(selectFirstStr));
			} 
			else 
			{
				buffer.add(selectFirstStr.getBytes());
			}
			// 第二类查询条件
			buffer.add((byte) selectSecondType);
			buffer.add(selectSecondStr.getBytes());
			// 第三类查询条件
			buffer.add((byte) selectThirdType);
			sendPackage.addUnit("0491", buffer.getBytes());
		}

		buffer.clear();
		buffer.add("0700");
		// 总
		buffer.add("0000");
		// 起始
		buffer.add(startnum);
		// 一页的行数
		buffer.add((byte) Integer.parseInt(lineNum));
		sendPackage.addUnit("0492", buffer.getBytes());

		// 发收
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			this.setLastKnownError(socketService.getLastKnowError());
			return ret;
		}
		ret = receiveOnePackage("0D03");
		if (ret != ReturnEnum.SUCCESS) 
		{
			this.setLastKnownError(socketService.getLastKnowError());
			return ret;
		}

		// 获取号码列表
		if (!recivePackage.containsKey("0492")) 
		{
			setLastKnownError("缺少0492命令单元");
			return ReturnEnum.FAIL;
		} 
		else 
		{
			byte[] unit = recivePackage.get("0492");

			byte[] bitmap = new byte[2];
			System.arraycopy(unit, 0, bitmap, 0, 2);
			int map = Commen.bytes2int(bitmap);

			telnumTotal = unit[2] & 0xFF;
			int count = unit[6] & 0xFF;

			int begen = 7;
			resnum.clear();
			telPrintBuffer.clear();
			mTelFeeLevelsList.clear();
			for (int i = 0; i < count; i++) 
			{
				int len = unit[begen] & 0xFF;
				byte[] info = new byte[len];
				System.arraycopy(unit, ++begen, info, 0, len);
				begen += len;
				try 
				{
					resnum.add(new String(info, "ASCII"));
					Log.i("haoka", new String(info, "ASCII"));
				} 
				catch (UnsupportedEncodingException e) 
				{
					e.printStackTrace();
				}
			}

			// 获取号码详情
			if ((map & 0x0010) != 0) 
			{
				int j = 0;
				for (j = 0; j < count; j++) 
				{
					System.out.println("");
					int len = unit[begen] & 0xFF;
					if (0 == len) 
					{
						// byte[] info = "0".getBytes();
						begen++;
						begen += len;
						
						telPrintBuffer.add("");
						Log.i("haoka", "");
						/*
						try {
							telPrintBuffer.add(new String(info, "GBK"));
							Log.i("haoka", new String(info, "ASCII"));
						} 
						catch (UnsupportedEncodingException e) 
						{
							e.printStackTrace();
						}
						*/
					} 
					else 
					{
						byte[] info = new byte[len];
						System.arraycopy(unit, ++begen, info, 0, len);
						begen += len;
						try 
						{
							String str = new String(info, "GBK");
							telPrintBuffer.add(str);
							Log.i("haoka", new String(info, "ASCII"));
						} 
						catch (UnsupportedEncodingException e) 
						{
							e.printStackTrace();
						}
					}
				}
			}

			// 获取号码费用档次
			if ((map & 0x0020) != 0) 
			{
				int j = 0;
				for (j = 0; j < count; j++) 
				{
					int len = unit[begen] & 0xFF;
					if (0 == len) 
					{
						byte[] info = "0".getBytes();
						begen++;
						begen += len;
						
						try 
						{
							mTelFeeLevelsList.add(new String(info, "GBK"));
							Log.i("haoka", new String(info, "ASCII"));
						} 
						catch (UnsupportedEncodingException e) 
						{
							e.printStackTrace();
						}
					} 
					else 
					{
						byte[] info = new byte[len];
						System.arraycopy(unit, ++begen, info, 0, len);
						begen += len;
						try 
						{
							String str = new String(info, "GBK");
							mTelFeeLevelsList.add(str);
							Log.i("haoka", new String(info, "ASCII"));
						} 
						catch (UnsupportedEncodingException e) 
						{
							e.printStackTrace();
						}
					}
				}
			}
		}
		return ret;
	}

	/**
	 * 预占手机号
	 */
	public int bll0D04(String telFeeLevels) 
	{
		int ret = ReturnEnum.FAIL;

		sendPackage = new Package();

		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("04");
		sendPackage.setId("0D");
		sendPackage.addUnit("0205", slectenum.getBytes());

		Buffer buffer = new Buffer();
		buffer.add("0300");

		// 对于合约机来说只有预付费非套包和后付费
		// 后付费
		if (mPayType == 0x02)
		{
			buffer.add(((byte) 0x01));
		} 
		// 预付费
		else
		{
			buffer.add(((byte) 0x02));
		}

		if (mPayType == 3) 
		{
			Log.log("预付费套包，选好类型为0x04,号码为:" + sjNumber);
			// 选号类型
			buffer.add((byte) 0x04);
		} 
		else 
		{
			Log.log("选号类型:" + search_type);
			// 选号类型
			buffer.add((byte) 0);
		}
		
		sendPackage.addUnit("0491", buffer.getBytes());
		 //created 2013-10-08
		sendPackage.addUnit("0211", (byte) m0D04AddQueryTerm);

		buffer.clear();
		buffer.add((short) Integer.parseInt(telFeeLevels));
		sendPackage.addUnit("0212", buffer.getBytes());

		// 发收
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			this.setLastKnownError(socketService.getLastKnowError());
			return ret;
		}
		ret = receiveOnePackage("0D04");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "预占号码失败";
			setLastKnownError(ss);
			return ret;
		}

		sjNumber = slectenum;
		Log.i("haokao", "预占号码:" + slectenum + "成功");
		
		//add 2013-10-10
		if (recivePackage.containsKey("0493")) 
		{
			mConsumeLevels.clear();
			mConsumePlan.clear();
			int begin = 0;
			int map = 0;
			byte[] buf = recivePackage.get("0493");
			
			map = Commen.bytes2int(buf, 0, 2);
			begin += 2;
			
			if(map == 0)
			{
				// 消费档次数量
				int consumeLevelsCount = buf[begin]; 
				begin += 2;

				for (int i = 0; i < consumeLevelsCount; i++) 
				{
					// 消费档次
					int consumenLevel = Commen.bytes2int(buf, begin, 2);
					mConsumeLevels.add(consumenLevel);
					begin += 2;
					
					// 套餐计划数量
					int consumePlanCount = buf[begin];
					List<ConsumePlan> plans = new ArrayList<ConsumePlan>();
					begin += 1;
					for (int j = 0; j < consumePlanCount; j++) 
					{
						// 套餐计划
						ConsumePlan plan = new ConsumePlan();
						// 套餐计划编码
						int code = buf[begin];
						begin += 1;
						int planTypeLen = buf[begin];
						begin += 1;
						// 套餐计划内容 
						String planType = StringUtil.encodeWithGBK(buf, begin,
								planTypeLen);
						begin += planTypeLen;
						plan.setmCode(code);
						plan.setmType(planType);
						plans.add(plan);
					}
					mConsumePlan.add(plans);
				}
			}
			else
			{
				// 错误信息长度
				int errorLen = buf[begin];
				begin += 1;
				// 错误信息内容
				String errorContent = StringUtil.encodeWithGBK(buf, begin,
						errorLen);
				begin += errorLen;
				setLastKnownError(errorContent);
				return ReturnEnum.FAIL;
			}
		}

		return ret;
	}

	/**
	 * 获取靓号规则
	 */
	public int bll0D05() 
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("05");
		sendPackage.setId("0D");

		// 发收
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			this.setLastKnownError(socketService.getLastKnowError());
			return ret;
		}
		ret = receiveOnePackage("0D05");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取靓号规则失败";
			setLastKnownError(ss);
			return ret;
		}

		if (recivePackage.containsKey("0501")) 
		{
			Log.log("====================开始解析0501单元====================");
			byte[] buf = recivePackage.get("0501");
			int pos = 0;

			numberRules.clear();

			// 是否有靓号
			if (buf[pos] == 1) 
			{
				// 这个是客户端为了满足需求加上的
				Rule fixedRule = new Rule();
				fixedRule.setId(-1);
				fixedRule.setContent("不限");
				numberRules.add(fixedRule);
				
				// 有
				pos += 1;
				int len = Commen
						.bytes2int(new byte[] { buf[pos], buf[pos + 1] });
				pos += 2;
				// 规则总数搜索
				int count = buf[pos];
				Log.log("有" + count + "个靓号");
				pos += 1;

				for (int i = 0; i < count; i++) 
				{
					Rule rule = new Rule();
					rule.setId(buf[pos]);
					pos += 1;
					len = buf[pos];
					pos += 1;
					rule.setContent(StringUtil.encodeWithGBK(buf, pos, len));
					pos += len;
					Log.log("靓号:	编码--" + rule.getId() + "	内容--"
							+ rule.getContent());
					numberRules.add(rule);
				}
				ret = ReturnEnum.SUCCESS;
			} 
			else 
			{
				Log.log("无靓号");
			}
			Log.log("====================解析0501单元完成====================");
		} 
		else 
		{
			Log.log("xxxxxxxxxxxxxxxxxxxx无0501单元xxxxxxxxxxxxxxxxxxxx");
		}

		// 获取入网协议
		if (recivePackage.containsKey("049E")) 
		{
			byte[] buf = recivePackage.get("049E");
			int pos = 0;
			int len = Commen.bytes2int(new byte[] { buf[0], buf[1] });
			pos += 2;
			networkProtocol = StringUtil.encodeWithGBK(buf, pos, len);
			Log.log("入网协议:" + networkProtocol);
		}

		// 获取入网资费
		if (recivePackage.containsKey("049C")) 
		{
			int haveData = 0x00;
			int pos = 0;
			int total = 0;
			int len = 0;
			byte[] unit = recivePackage.get("049C");
			haveData = unit[0];
			if (haveData == 0) 
			{
				setLastKnownError("获取资费信息,返回无数据");
				return ReturnEnum.FAIL;
			}
			pos = 3;
			total = unit[pos];
			pos++;
			productZifei.clear();
			if (total > 0) 
			{
			}
			for (int i = 0; i < total; i++) 
			{
				Productzifei pz = new Productzifei();
				len = 0;
				pz.setCode(unit[pos]);
				pos++;
				len = unit[pos];
				pos++;
				byte[] info = new byte[len];
				System.arraycopy(unit, pos, info, 0, len);
				pz.setName(StringUtil.encodeWithGBK(info));
				pos += len;
				productZifei.add(pz);
				Log.e("haoka", "编码:" + pz.getCode() + "; 内容:" + pz.getName());
			}
		}
		return ret;
	}
	
	/**
	 * @created 2013-10-11
	 * @description 获取套餐详情和合约内容
	 */
	public int bll0D06(int planCode, int consumeLevel) 
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("06");
		sendPackage.setId("0D");
		Buffer buffer = new Buffer();
		buffer.add((byte) planCode);
		buffer.add((short) consumeLevel);

		sendPackage.addUnit("049D", buffer.getBytes());
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			setLastKnownError(socketService.getLastKnowError());
			return ret;
		}
		ret = receiveOnePackage("0D06");
		if (ret != ReturnEnum.SUCCESS) 
		{
			setLastKnownError(socketService.getLastKnowError());
			return ret;
		}

		// 获取号码列表
		if (!recivePackage.containsKey("049D")) 
		{
			setLastKnownError("缺少049D命令单元");
			return ReturnEnum.FAIL;
		} 
		else 
		{
			//清空活动类型和套餐包信息
			mContractType.clear();
			mPackageInfo = null;
			
			byte[] buf = recivePackage.get("049D");
			int map = 0;
			int len = 0;
			int pos = 0;

			map = Commen.bytes2int(buf, 0, 2);
			pos += 2;
			
			// 套餐编码
			if ((map & 0x01) != 0) 
			{
				ProductCode = Commen.bytes2int(buf, pos, 4);
				pos += 4;
			}
			
			// 套餐名称
			if((map & 0x02) != 0)
			{
				int packageNameLen = buf[pos];
				pos += 1;
				packageName = StringUtil.encodeWithGBK(buf, pos,packageNameLen);
				pos += packageNameLen;
			}
			
			if ((map & 0x04) != 0) 
			{
				mPackageInfo = new PackageInfo();
				// 国内语音拨打分钟数
				len = buf[pos];
				pos++;
				mPackageInfo.setmCall(StringUtil.encodeWithGBK(buf, pos, len));
				pos += len;

				// 国内流量
				len = buf[pos];
				pos++;
				mPackageInfo.setmFlow(StringUtil.encodeWithGBK(buf, pos, len));
				pos += len;

				// 国内短信发送条数
				len = buf[pos];
				pos++;
				mPackageInfo.setmMsgCount(StringUtil.encodeWithGBK(buf, pos,
						len));
				pos += len;

				// 接听免费
				len = buf[pos];
				pos++;
				mPackageInfo.setmFreeIncomingCall(StringUtil.encodeWithGBK(buf,
						pos, len));
				pos += len;
			}

			if ((map & 0x08) != 0) 
			{
				// 国内语音拨打费用（元、分钟）
				len = buf[pos];
				pos++;
				mPackageInfo.setmFeeCall(StringUtil
						.encodeWithGBK(buf, pos, len));
				pos += len;

				// 国内流量费用
				len = buf[pos];
				pos++;
				mPackageInfo.setmFeeFlow(StringUtil
						.encodeWithGBK(buf, pos, len));
				pos += len;

				// 国内可视电话拨打费用
				len = buf[pos];
				pos++;
				mPackageInfo.setmFeeVedioCall(StringUtil.encodeWithGBK(buf,
						pos, len));
				pos += len;

				// 其他业务
				len = buf[pos];
				pos++;
				mPackageInfo.setmOther(StringUtil.encodeWithGBK(buf, pos, len));
				pos += len;
			}

			// 合约类型数据
			if ((map & 0x10) != 0) 
			{
				int contractTypeCount = buf[pos];
				pos++;

				mContractType.add(0);
				for (int i = 0; i < contractTypeCount; i++) 
				{
					int actType = buf[pos];
					pos += 1;
					mContractType.add(actType);
				}
			}
		}
		return ret;
	}
	
	/**
	 * @created 2013-10-13
	 * @description 获取合约期限
	 */
	public int bll0D08(int actType) 
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("08");
		sendPackage.setId("0D");
		Buffer buffer = new Buffer();
		
		if(actType == 01)
		{
			buffer.add("0300");
			buffer.add(ProductCode);
			buffer.add(actType);
		}
		//暂时没用，预留着
		else if((actType == 02)||(actType == 03))
		{
			buffer.add("0700");
			buffer.add(ProductCode);
			buffer.add((byte) actType);
			buffer.add((byte) contractNo.length());
			buffer.add(contractNo.getBytes());
		}

		sendPackage.addUnit("0502", buffer.getBytes());
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			this.setLastKnownError(socketService.getLastKnowError());
			return ret;
		}
		ret = receiveOnePackage("0D08");
		if (ret != ReturnEnum.SUCCESS) 
		{
			setLastKnownError(socketService.getLastKnowError());
			return ret;
		}

		// 获取号码列表
		if (!recivePackage.containsKey("0502")) 
		{
			setLastKnownError("缺少0502命令单元");
			return ReturnEnum.FAIL;
		} 
		else 
		{
			if (mContractBuffs.size() != 0) 
			{
				mContractBuffs.clear();
			}

			int flag = 0;
			int pos = 0;
			int count = 0;
			int len = 0;
			int rec_id = 0;
			String rec_content = null;

			byte[] buf = recivePackage.get("0502");
			
			pos += 2;
			
			flag = buf[pos];
			pos += 1;
			
			if(flag == 1)
			{
				pos += 2;
				count = buf[pos];
				pos += 1;

				contractBuff tempConbuf = new contractBuff(-1,"未选择");
				mContractBuffs.add(tempConbuf);
				for (int i = 0; i < count; i++) 
				{
					//合约期限内容
					rec_id = buf[pos];
					pos += 1;

					len = buf[pos];
					pos += 1;

					byte[] IMSI = new byte[len];
					System.arraycopy(buf, pos, IMSI, 0, len);
					pos += len;

					//合约期限内容
					rec_content = StringUtil.encodeWithGBK(IMSI);

					contractBuff conbuf = new contractBuff(rec_id, rec_content);
					mContractBuffs.add(conbuf);
				}
			}
			else
			{
				setLastKnownError("无合约期限!");
				return ReturnEnum.FAIL;
			}
		}
		return ret;
	}

	/**
	 * 获取合约计划内容。
	 * @param deadlineId
	 * @param productID
	 */
	public int bll0D09(int contractBuffCode,int planCode) 
	{
		int ret = ReturnEnum.FAIL;

		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("09");
		sendPackage.setId("0D");

		Buffer buffer = new Buffer();

		buffer.add("0300");
		buffer.add((byte) contractBuffCode);
		buffer.add((byte) 0x04);
		buffer.add(ProductCode);

		sendPackage.addUnit("0503", buffer.getBytes());
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			setLastKnownError(socketService.getLastKnowError());
			return ret;
		}

		ret = receiveOnePackage("0D09");
		if (ret != ReturnEnum.SUCCESS) 
		{
			setLastKnownError(socketService.getLastKnowError());
			return ret;
		}

		if (!recivePackage.containsKey("0503")) 
		{
			setLastKnownError("缺少0503命令单元");
			return ReturnEnum.FAIL;
		}

		int flag = 0;
		int map = 0;
		int pos = 0;

		int len = 0;

		byte[] unit = recivePackage.get("0503");
		map = Commen.bytes2int(unit, 0, 2);
		pos += 2;

		if((map & 0x04) != 0)
		{
			flag = unit[pos];
			pos += 1;

			if (1 == flag) 
			{
				if((map & 0x08) != 0)
				{
					int contractNameLen = unit[pos];
					pos += 1;
					mContractName = StringUtil.encodeWithGBK(unit, pos, contractNameLen);
					pos += contractNameLen;
				}
				
				if((map & 0x10) != 0)
				{
					len = Commen.bytes2int(new byte[] { unit[pos], unit[pos + 1] });
					pos += 2;

					byte[] IMSI = new byte[len];
					System.arraycopy(unit, pos, IMSI, 0, len);
					pos += len;
					try 
					{
						this.mContractContent = new String(IMSI, "GBK");
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
			} 
			else 
			{
				// 合约内容为空
				setLastKnownError("合约内容为空");
				return ReturnEnum.FAIL;
			}
		}

		return ret;
	}

	/**
	 * 获取费用信息
	 */
	public int bll0D0B() 
	{
		int ret = ReturnEnum.FAIL;
		try {
			sendPackage = new Package();
			sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
			sendPackage.setSubId("0B");
			sendPackage.setId("0D");

			if (mPayType != 5) 
			{
				// 3G后付费省略
				Buffer buffer = new Buffer();
				buffer.add("0200");
				// 身份证有效期
				buffer.add(this.IdentityEndtime.getBytes()); 
				sendPackage.addUnit("036D", buffer.getBytes());
			}

			Buffer buffer = new Buffer();

			int map = 0x2738;

			// 3G后付费不含推荐人
			if (mPayType == 5 && StringUtil.isEmptyOrNull(tjperson)) 
			{
				Log.log("3G后付费不含推荐人");
				buffer.add("3827");
				map = 0x2738;
			}
			// 3G后付费含推荐人
			else if (mPayType == 5 && !StringUtil.isEmptyOrNull(tjperson)) 
			{
				Log.log("3G后付费含推荐人");
				buffer.add("3867");
				map = 0x6738;
			} 
			else 
			{
				buffer.add("387F");
				map = 0x7F38;
			}

			// 手机号
			buffer.add(slectenum.getBytes()); 

			// 卡类型0成卡、1白卡
			buffer.add((byte) openCardType);

			Log.log("iccid:" + iccid);
			buffer.add((byte) iccid.getBytes().length);
			buffer.add(iccid.getBytes());

			// 套餐编码 4位
			buffer.add(this.ProductCode); 
			if ("".equals(cust_type)) 
			{
				buffer.add((byte) (Identity.length() == 15 ? 0x01 : 0x02));
			} 
			else 
			{
				// 从前面传过来的
				buffer.add(cust_type); 
			}

			// buffer.add("01"); // 01 15位身份证 02 18位身份证 ...
			// 身份证长度n
			buffer.add((byte) Identity.length()); 
			// 证件号码
			buffer.add(this.Identity.getBytes()); 

			try 
			{
				int len = 0;
				byte[] name = this.username.getBytes("GBK");
				len = name.length;
				buffer.add((byte) len);
				buffer.add(name);
			} 
			catch (UnsupportedEncodingException e) 
			{
				e.printStackTrace();
			}

			if (map == 0x6738) 
			{
			}

			if (map == 0x7F38) 
			{
				buffer.add(this.sex);
				// 01 女 02男
				try 
				{
					int len = 0;
					byte[] name = this.address.getBytes("GBK");
					len = name.length;
					// 地址
					buffer.add((byte) len); 
					buffer.add(name);
				} 
				catch (UnsupportedEncodingException e) 
				{
					e.printStackTrace();
				}
			}

			Log.log("自费编码:" + zife);
			// 资费编码
			buffer.add((byte) this.zife); 

			if(map == 0x7F38)
			{
				if (!StringUtil.isEmptyOrNull(tjperson)) 
				{
					int tjlength = tjperson.length();
					// 推荐人长度
					buffer.add((byte) tjlength); 
					// 推荐人名称
					buffer.add(tjperson.getBytes()); 
				}
				else
				{
					buffer.add("00");
				}
			}

			sendPackage.addUnit("04D1", buffer.getBytes());

			sendPackage.addUnit("0371", new byte[] { 0x01 });
			sendPackage.addUnit("0374", new byte[] { 0x01 });

			// 添加合约计划标识
			sendPackage.addUnit("0505", (byte) this.mContractflag);

			ret = send(sendPackage);
			if (ret != ReturnEnum.SUCCESS) 
			{
				setLastKnownError(socketService.getLastKnowError());
				return ret;
			}

			ret = receiveOnePackage("0D0B");
			if (ret != ReturnEnum.SUCCESS) 
			{
				setLastKnownError(socketService.getLastKnowError());
				return ReturnEnum.FAIL;
			}

			if (!recivePackage.containsKey("0495")) 
			{
				setLastKnownError("缺少0495命令单元");
				return ReturnEnum.FAIL;
			}

			byte[] unit = recivePackage.get("0495");
			int len = 0;
			int pos = 2;
			int bitmap = Commen.bytes2int(new byte[] { unit[0], unit[1] });
			if ((bitmap & 0x0001) != 0) 
			{
				pos++;
			}
			if ((bitmap & 0x0002) != 0) 
			{
				pos += 4;
			}
			if ((bitmap & 0x0004) != 0) 
			{
				len = Commen.bytes2int(new byte[] { unit[pos], unit[pos + 1] });
				pos += 2;
				byte[] info = new byte[len];
				System.arraycopy(unit, pos, info, 0, len);
				try 
				{
					// 各类详情内容
					this.res = new String(info, "GBK");
					Log.e("haoka", new String(info, "GBK"));
				} 
				catch (UnsupportedEncodingException e) 
				{
					e.printStackTrace();
				}
			}

			if (!recivePackage.containsKey("0497")) 
			{
				this.setLastKnownError("缺少0497命令单元");
				return ReturnEnum.FAIL;
			}
			byte[] unit2 = recivePackage.get("0497");
			len = 0;
			bitmap = 0;
			pos = 0;
			
			bitmap = Commen.bytes2int(new byte[] { unit2[0], unit2[1] });
			pos += 2;
			
			if((bitmap & 0x0001) != 0)
			{
				int moneyLen = unit2[pos];
				pos += 1;
				mStrErrMoney = StringUtil.encodeWithGBK(unit2,pos,moneyLen);
				pos += moneyLen;
			}
			
			if((bitmap & 0x0002) != 0)
			{
				len = Commen.bytes2int(new byte[] { unit2[pos], unit2[pos+1] });
				pos += 2;
				byte[] info2 = new byte[len];
				System.arraycopy(unit2,pos, info2, 0, len);
				try 
				{
					// 费用信息文本列表
					this.feiyong = new String(info2, "GBK");
				} 
				catch (UnsupportedEncodingException e) 
				{
					e.printStackTrace();
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
		}
		return ret;
	}

	/**
	 * 开户，获取流水号
	 */
	public int bll0D0C() 
	{
		int ret = ReturnEnum.FAIL;

		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("0C");
		sendPackage.setId("0D");
		sendPackage.addUnit("0202", new byte[] { mTranceType }); // 业务
		
		Buffer buffer = new Buffer();
		buffer.add((byte) this.PWDEncryption(this.password).length);
		buffer.add(this.PWDEncryption(this.password));
		sendPackage.addUnit("0215", buffer.getBytes());

		// 发收
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			this.setLastKnownError(socketService.getLastKnowError());
			return ret;
		}
		ret = receiveOnePackage("0D0C");
		if (ret != ReturnEnum.SUCCESS) 
		{
			this.setLastKnownError(socketService.getLastKnowError());
			return ret;
		}

		// 获取交易状态
		if (recivePackage.containsKey("0F49")) 
		{
			int pos1 = 0;
			int len1 = 0;
			byte[] unit1 = recivePackage.get("0F49");
			byte[] bitmap = new byte[2];
			System.arraycopy(unit1, 0, bitmap, 0, 2);
			int map = Commen.bytes2int(bitmap);
			pos1 += 2;

			// 银行卡号
			if ((map & 0x0001) != 0) 
			{
				len1 = unit1[pos1];
				pos1 += 1;
				pos1 += len1;
			}
			// 查询状态
			if ((map & 0x0002) != 0) 
			{
				posOptType = unit1[pos1];
				pos1 += 1;
			}
			// 交易账号
			if ((map & 0x0004) != 0) 
			{
				len1 = unit1[pos1];
				pos1 += 1;
				pos1 += len1;
			}
			// 国际行政区域号
			if ((map & 0x0008) != 0) 
			{
				len1 = unit1[pos1];
				pos1 += 1;
				interCode = new byte[len1];
				System.arraycopy(unit1, pos1, interCode, 0, len1);
				pos1 += len1;
			}
		}

		if (!recivePackage.containsKey("0202")) 
		{
			setLastKnownError("获取流水号缺少命令单元");
			return ReturnEnum.FAIL;
		}
		
		byte[] unit = recivePackage.get("0202");

		int len = unit[1];
		bllnum = new byte[len];
		System.arraycopy(unit, 2, bllnum, 0, len);
		try 
		{
			mStrBillNum = new String(bllnum, "ASCII");
			Log.e("业务流水号为:", mStrBillNum);
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
			setLastKnownError("获取流水号未知错误");
			return ReturnEnum.FAIL;
		}

		return ret;
	}

	/**
	 * 开户
	 */
	public int bll0D0D() 
	{
		int ret = ReturnEnum.FAIL;

		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("0D");
		sendPackage.setId("0D");

		Buffer buffer = new Buffer();
		buffer.add("0200");
		buffer.add(this.IdentityEndtime.getBytes()); // 身份证有效期
		sendPackage.addUnit("036D", buffer.getBytes());

		buffer.clear();
		buffer.add("3F7F"); // 数据域 待确认 xuhe
		buffer.add("0000"); // 金融CRC
		buffer.add(this.mTranceType); // 业务类型
		buffer.add((byte)this.encryptSerialNumber(this.bllnum).length);// 流水号长度 
		buffer.add(this.encryptSerialNumber(this.bllnum)); // 流水号
		buffer.add("00"); // 扣款状态 0扣款 1查询

		buffer.add(slectenum.getBytes()); // 手机号
		buffer.add((byte) openCardType);
		buffer.add(this.iccid.getBytes());
		buffer.add(this.ProductCode); // 套餐编码 4位

		if ("".equals(cust_type)) 
		{
			buffer.add((byte) (Identity.length() == 15 ? 0x01 : 0x02));
		} 
		else 
		{
			buffer.add(cust_type); // 从前面传过来的
		}

		buffer.add((byte) Identity.getBytes().length); // 身份证长度
		buffer.add(this.Identity.getBytes()); // 证件号码

		try 
		{
			int len = 0;
			byte[] name = this.username.getBytes("GBK");
			len = name.length;
			buffer.add((byte) len);
			buffer.add(name);
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}
		// 01 女 02男
		buffer.add(this.sex); 

		try 
		{
			int len = 0;
			byte[] name = this.address.getBytes("GBK");
			len = name.length;
			buffer.add((byte) len); // 地址
			buffer.add(name);
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}

		buffer.add((byte) this.zife); // 资费编码

		if (!StringUtil.isEmptyOrNull(tjperson)) 
		{
			int tjlength = tjperson.length();
			buffer.add((byte) tjlength); // 推荐人长度
			buffer.add(tjperson.getBytes()); // 推荐人名称
		} 
		else 
		{
			buffer.add("00"); // 推荐人编码长度
		}

		byte[] tt = buffer.getBytes(); // CRC计算
		byte[] bb = new byte[tt.length - 4];
		System.arraycopy(tt, 4, bb, 0, tt.length - 4);
		byte[] crc = Commcenter.CalcCrc16(bb, (char) 0);
		tt[2] = crc[0];
		tt[3] = crc[1];
		sendPackage.addUnit("04D1", tt);

		// 这个需要提出crc，然后修改数据库中这个状态
		byte[] err_crc = new byte[3];
		err_crc[0] = crc[0];
		err_crc[1] = crc[1];
		int errint = err_crc.length;
		t_crc = Commen.hax2str(err_crc, 0, errint);

		if (2 == postype) 
		{
			sendPackage.addUnit("0F20", pos8583);
			buffer.clear();
			buffer.add((byte) meclen);
			buffer.add(mecbusflowno);
			buffer.add((byte) chanlen);
			buffer.add(chanbusflowno);
			sendPackage.addUnit("0F25", buffer.getBytes());
		}
		sendPackage.addUnit("0371", new byte[] { 0x01 });
		sendPackage.addUnit("0374", new byte[] { 0x01 });
		
		// 预先记录异常
		mStrErrBill 	=  mStrBillNum; 								// 流水
		mStrErrReason 	= Commen.hax2str(new byte[] { 0x0C }); 			// 记录原因
		mStrErrType 	= Commen.hax2str(new byte[] { mTranceType }); 	// 交易类型
		mStrErrCrc 	 	= Commen.hax2str(new byte[] { crc[0], crc[1] });
		ErrorDealAdapter.setErrorInfo(context, mStrErrPsamID, mStrErrType, mStrErrBill, mStrErrReason, mStrErrCrc, "1");
		// 同时插入到数据库中
		mBusinessNum.insertData(
				App.getUserNo(), 
				slectenum, 
				"选号入网", 
				mStrErrMoney, 
				"交易失败", 
				mStrErrBill,
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()), 
				mStrErrMoney, 
				mTranceType);

		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			setLastKnownError(socketService.getLastKnowError());
			return ret;
		}

		ret = receiveOnePackage("0D0D",90*1000);
		
		// 如果不是接收超时则更改异常交易记录的状态
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}

		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "开户请求办理错误";
			setLastKnownError(ss);
			return ret;
		}
		
		// 重置交易时间
		mTranceTime = "";
		if(recivePackage.containsKey("0204"))
		{
			byte[] unit = recivePackage.get("0204");
			if(unit.length >= 14)
			{
				mTranceTime = StringUtil.encodeWithGBK(unit, 0, 14);
				mTranceTime = mTranceTime.substring(0, 4)+"-"+
							  mTranceTime.substring(4, 6)+"-"+
						      mTranceTime.substring(6,8)+" "+
						      mTranceTime.substring(8, 10)+":"+
						      mTranceTime.substring(10, 12)+":"+
						      mTranceTime.substring(12, 14);
			}
			else
			{
				mTranceTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
			}
		}
		
		// 获取POS刷卡的冲正标示
		if (recivePackage.containsKey("0F21")) 
		{
			byte[] unit1 = recivePackage.get("0F21");
			posresult = unit1[0];
		}

		// 获取8583协议包
		if (recivePackage.containsKey("0F20")) 
		{
			pos8583 = recivePackage.get("0F20");
		}

		if (!recivePackage.containsKey("0498")) 
		{
			return ReturnEnum.FAIL;
		}
		byte[] unit = recivePackage.get("0498");

		int len = unit[0];
		fuwumima = new byte[len];
		System.arraycopy(unit, 1, fuwumima, 0, len);
		try 
		{
			fuwupwd = new String(fuwumima, "ASCII");
			Log.e("服务密码为:", fuwupwd);
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
			setLastKnownError("0608:未知错误" + 0x060D);
			ret = ReturnEnum.FAIL;
		}

		// 如果号卡成功，则把号源模式设置为首次；这样就可以重新办理号卡业务
		if (ret == ReturnEnum.FAIL) 
		{
			this.isHaoyuanTypeFirst = true;
		}

		if (recivePackage.containsKey("04C1")) 
		{
			/*********************** 发票打印 ***********************/
			int pos1 = 0;
			int len1 = 0;
			byte[] unit1 = recivePackage.get("04C1");
			byte[] bitmap = new byte[2];
			System.arraycopy(unit1, 0, bitmap, 0, 2);
			int map = Commen.bytes2int(bitmap);
			pos1 += 2;

			// 发票类型
			if ((map & 0x0001) != 0) 
			{
				pos1 += 1;
			}
			
			// 批次号
			if ((map & 0x0002) != 0) 
			{
				len1 = unit1[pos1];
				pos1 += 1;
				pos1 += len1;
			}
			
			// 发票号
			if ((map & 0x0004) != 0) 
			{
				len1 = unit1[pos1];
				pos1 += 1;
				byte[] IMSI1 = new byte[len1];
				System.arraycopy(unit1, pos1, IMSI1, 0, len1);
				pos1 += len1;
				try 
				{
					this.taxNo = new String(IMSI1);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
			
			// 发票内容
			if ((map & 0x0008) != 0) 
			{
				byte[] invoice = new byte[2];
				System.arraycopy(unit1, pos1, invoice, 0, 2);
				len1 = Commen.bytes2int(invoice);
				pos1 += 2;

				byte[] IMSI2 = new byte[len1];
				System.arraycopy(unit1, pos1, IMSI2, 0, len1);
				pos1 += len1;
				try 
				{
					this.taxContent = new String(IMSI2, "GBK");
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		}

		return ret;
	}

	/**
	 * 获取ISMI
	 */
	public int bll0D0E() 
	{
		int ret = ReturnEnum.FAIL;
		Log.log("开始获取号卡数据");

		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("0E");
		sendPackage.setId("0D");

		Buffer buffer = new Buffer();
		// 数据域
		buffer.add("0F00"); 
		// 第一次获取IMSI:0x00
		buffer.add((byte) 0x00); 
		// ICCID号长度
		buffer.add((byte) iccid.length()); 
		buffer.add(iccid.getBytes());
		// ICCID号长度
		buffer.add((byte) iccid.length());
		buffer.add(iccid.getBytes());
		// 手机号码长度
		buffer.add((byte) slectenum.length()); 
		buffer.add(slectenum.getBytes());

		sendPackage.addUnit("04B0", buffer.getBytes());
		sendPackage.addUnit("04B2", (byte) 0x01);
		ret = send(sendPackage);

		if (ret != ReturnEnum.SUCCESS) 
		{
			this.setLastKnownError(socketService.getLastKnowError());
			return ret;
		}

		ret = receiveOnePackage("0D0E");
		if (ret != ReturnEnum.SUCCESS) 
		{
			this.setLastKnownError(socketService.getLastKnowError());
			return ret;
		}

		if (recivePackage.containsKey("04B0")) 
		{
			/*********************** 短信中心号 ***********************/
			byte[] buf = recivePackage.get("04B0");
			int pos = 0;
			int len = 0;
			int map = Commen.bytes2int(buf, pos, 2);
			pos += 2;

			if ((map & 0x0001) != 0) 
			{
				len = buf[pos];
				pos += 1;
				String iccid = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("新ICCID号码为:" + iccid);
			}

			if ((map & 0x0002) != 0) 
			{
				len = buf[pos];
				pos += 1;
				String iccid = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("旧ICCID号码为:" + iccid);
			}
			
			if ((map & 0x0004) != 0) 
			{
				len = buf[pos];
				pos += 1;
				String str = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("手机号码为:" + str);
			}
			
			if ((map & 0x0008) != 0) 
			{
				Log.log("文弄说这个数据不会有");
			}

			if ((map & 0x0010) != 0) 
			{
				Log.log("文弄说这个数据不会有");
			}
			
			if ((map & 0x0020) != 0) 
			{
				len = buf[pos];
				pos += 1;
				this.smsNum = StringUtil.encodeWithGBK(buf, pos, len);
				pos += len;
				Log.log("短信中心号码为:" + smsNum);
			}
			
			if ((map & 0x0040) != 0) 
			{
				len = buf[pos];
				pos += 1;
				this.im = StringUtil.encodeWithGBK(buf, pos, len);
				IMSI = new byte[len];
				System.arraycopy(buf, pos, IMSI, 0, len);
				pos += len;
				Log.log("imsi号码为:" + im);
			}
			
			if ((map & 0x0080) != 0) 
			{
				len = buf[pos];
				Log.log("sim卡类型为:" + len + " 其中1为2G	2为3G");
			}
		}

		return ret;
	}

	/**
	 * 上报开卡接过
	 * 成功为0x00 失败wie0x01
	 */
	
	public int bll0D0F(byte result) 
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("0F");
		sendPackage.setId("0D");

		Buffer buffer = new Buffer();
		// 数据域
		buffer.add("3F00"); 
		// 开卡结果
		buffer.add(result); 
		// 业务状态
		buffer.add((byte) 0x00);
		// ICCID号长度
		buffer.add((byte) iccid.length()); 
		buffer.add(iccid.getBytes());
		// ICCID号长度
		buffer.add((byte) iccid.length()); 
		buffer.add(iccid.getBytes());

		// ISMI号码
		buffer.add((byte) IMSI.length); 
		buffer.add(IMSI);

		// 手机号码长度
		buffer.add((byte) slectenum.length()); 
		buffer.add(slectenum.getBytes());
		sendPackage.addUnit("04B1", buffer.getBytes());
		ret = send(sendPackage);

		if (ret != ReturnEnum.SUCCESS) 
		{
			if(String.valueOf(result).equals("0"))
			{
				this.setLastKnownError(socketService.getLastKnowError());
			}
			return ret;
		}

		ret = receiveOnePackage("0D0F");

		if (ret != ReturnEnum.SUCCESS) 
		{
			if(String.valueOf(result).equals("0"))
			{
				this.setLastKnownError(socketService.getLastKnowError());
			}
			return ret;
		}

		return ret;
	}

	// 开户流程
	public int playOpenCard() 
	{
		int flag = 0;

		flag = bll0D0C();// 获取流水号

		if (0 != flag) 
		{
			return 7;
		}

		if (0 == flag) 
		{
			flag = bll0D0D();// 号卡请求
			if (0 != flag) 
			{
				return 8;
			}

			if (openCardType == 1) 
			{
				// 如果是白卡开户则上平台上获取
				flag = playWriteCard();
			}
		}

		if (0 == flag) 
		{
			// 开户成功后，修改存放身份证信息的文件夹的名称为流水号---暂时屏蔽
			File oldFile = new File(sPicFolder);
			File newFile = new File(oldFile.getParent() + "/" + mStrBillNum);
			oldFile.renameTo(newFile);
			Log.log("发送开户成功过确认包");
			flag = bll0D10();
			//显示交易结果----暂时屏蔽
			//this.printScrean();
			if(flag == ReturnEnum.SUCCESS)
			{
				return flag;
			}
		} 

		Log.log("退出好卡销售流程");
		this.bll0D16();
		return flag;
	}

	/**
	 * 写白卡
	 */
	public int playWriteCard() 
	{
		int flag = -1;
		try 
		{
			DialogUtil.showProgress("正在获取写卡数据..."); 
			Log.log("开始获取卡数据...");
			// 获取卡数据
			flag = bll0D0E();

			if (flag == 0) 
			{
				DialogUtil.closeProgress();
				DialogUtil.MsgBox("提示信息", "即将写卡, 请勿拔卡!", "确定", new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						mIsStartWriteCard = true;
					}
				}, 
				null,
				null, 
				new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						mIsStartWriteCard = true;
					}
				});
				
				for(; !mIsStartWriteCard;)
				{
					Thread.sleep(1000);
				}
				
				// 重置开始写卡状态
				mIsStartWriteCard = false;
				Log.log("开始写卡...");
				// 物理写卡
				Log.log("开始写入IMSI...");	
				
				DialogUtil.showProgress("开始写卡");
				if (equipmentService.writeSimCard(IMSI, (byte) 0x02))
				{
					Log.log("开始写入短信中心号码...");
					if (equipmentService.writesmscentre(smsNum, (byte) 0x02)) 
					{
						Log.e("3G", "写卡成功");
						flag = this.bll0D0F((byte) 0x00);
					} 
					else 
					{	
						 Log.log("3G", "写卡失败"); 
						 this.bll0D0F((byte) 0x01);
						 this.setLastKnownError("写白卡失败\n请拔出重试或插入其它白卡重试!");
					 	 flag = ReturnEnum.FAIL; 
					}
				}
				else
				{
					showWriteSimCardBox();
					
					for(;(mTryCount < (mTryTotal + 1)) && (mTryCount != -1);)
					{
						Thread.sleep(1000);
					}
					
					mTryCount = 0;
					flag = mFlag;
					/*
					else
					{
						 Log.log("3G", "写卡失败"); 
						 this.bll0D0F((byte) 0x01);
						 this.setLastKnownError("写白卡失败\n请拔出重试或插入其它白卡重试!");
					 	 flag = ReturnEnum.FAIL; 
					 	 mTryCount = 0;
					}
					*/
				}
			} 
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			this.setLastKnownError("发生异常!");
			flag = ReturnEnum.FAIL;
		}
		return flag;
	}

	/**
	 * 开户成功信息
	 */
	public int bll0D10() 
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("10");
		sendPackage.setId("0D");

		Buffer buffer = new Buffer();
		buffer.add(mTranceType); // 业务类型
		buffer.add("1E"); // 流水长度:
		buffer.add(bllnum); // 流水号:
		sendPackage.addUnit("0202", buffer.getBytes());
		
		ErrorDealAdapter.setErrorInfoState(context, 
				mStrErrPsamID, 
				mStrErrType, 
				Commen.hax2str(new byte[] {0x0D}), 
				"1", 
				mStrErrCrc, 
				mStrErrBill);
		
		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			this.setLastKnownError(socketService.getLastKnowError());
			return ret;
		}

		ret = receiveOnePackage("0D10");

		// 更新异常交易记录状态
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
				
		if (ret != ReturnEnum.SUCCESS) 
		{
			this.setLastKnownError(socketService.getLastKnowError());
			return ret;
		}

		if (!recivePackage.containsKey("0202")) 
		{
			this.setLastKnownError("绝少0202命令单元!");
			return ReturnEnum.FAIL;
		}
		
		// 更改交易记录状态
		mBusinessNum.updateBussinessNum(
				new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(Calendar.getInstance().getTime()), 
				"交易成功", 
				mStrErrMoney, 
				mStrErrBill, 
				mStrErrMoney);
		
		try 
		{
			// 改为24小时制
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentDate = df.format(new Date());
			
			// 取得字符串的长度
			int sb_length = sPrintBuffer.length();
			sPrintBuffer.delete(0, sb_length);
			if (cust_type.equals("07")) 
			{
				sPrintBuffer
						.append("业务类型:3G号卡销售\n")
						.append(this.feiyong)
						.append("客户名称:")
						.append(this.username)
						.append("\n")
						.append("证件号码:")
						.append(this.Identity)
						.append("\n")
						.append("证件地址:")
						.append(this.address)
						.append("\n")
						.append("初始服务密码为:" + this.fuwupwd)
						.append("\n")
						.append("交易时间:" + currentDate).append("\n")
						.append("套餐名称:" + this.sp_packagename);
			} 
			else 
			{
				sPrintBuffer
						.append("业务类型:3G号卡销售\n")
						.append(this.feiyong)
						.append("用 户 名:")
						.append(this.username)
						.append("\n")
						.append("身份证号码:")
						.append(this.Identity)
						.append("\n")
						.append("用户地址:")
						.append(this.address)
						.append("\n")
						.append("初始服务密码为:" + this.fuwupwd)
						.append("\n")
						.append("交易时间:" + currentDate)
						.append("\n")
						.append("套餐名称:" + this.packageName);

			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		try {
			proPrintBuffer.delete(0, proPrintBuffer.length());
			Production = this.res;
			/*
			userPrintBuffer.delete(0, userPrintBuffer.length());
			userPrintBuffer
			.append("用  户  名:")
			.append(this.username)
			.append("\n")
			.append("用户性别:")
			.append("01".equals(this.sex) ? "女" :"男")
			.append("\n")
			.append("身份证号码:")
			.append(this.Identity)
			.append("\n")
			.append("身份证有效日期:")
			.append(this.IdentityEndtime)
			.append("\n")
			.append("用户地址:")
			.append(this.address)
			.append("\n")
			.append("服务密码为:" + new String(fuwumima))
			.append("\n");
			*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return ret;
	}

	/**
	 * 退出号卡销售流程
	 */
	public int bll0D16() 
	{
		int ret = ReturnEnum.FAIL;
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("16");
		sendPackage.setId("0D");

		sendPackage.addUnit("0001", "0201".getBytes());
		sendPackage.addUnit("049F", "01".getBytes());

		ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS) 
		{
			return ret;
		}

		ret = receiveOnePackage("0D16");
		if (ret != ReturnEnum.SUCCESS) 
		{
			return ret;
		}
		return ret;
	}
	
	public void showWriteSimCardBox()
	{
		new Thread(new Runnable() 
		{
			@Override
			public void run() 
			{
				Log.log("3G", "写卡失败"); 
				mFlag = ReturnEnum.FAIL; 
				// 剩余次数
				int remainCount = mTryTotal - mTryCount;
				DialogUtil.MsgBox("温馨提示", "写卡失败，是否重试？\n剩余重试次数："+remainCount, 
						 "确定", 
					    new View.OnClickListener()
				        {
							@Override
							public void onClick(View view) 
							{
								startWriteSimCard();
							}
						}, 
						"取消", 
						new View.OnClickListener() 
						{
							@Override
							public void onClick(View v) 
							{
								 failHandler();
							}
						}, 
						new View.OnClickListener() 
						{
							@Override
							public void onClick(View v) 
							{
								failHandler();
							}
						});
			}
		}).start();
	}
	
	public void startWriteSimCard()
	{
		new Thread(new Runnable() 
		{
			@Override
			public void run() 
			{
				mTryCount += 1;
				DialogUtil.showProgress("正在进行第" + mTryCount + "次写卡尝试，请勿拔卡...");
				if(equipmentService.writeSimCard(IMSI, (byte) 0x02))
				{
					mFlag = ReturnEnum.SUCCESS;
					startWriteScentre();
				}
				else
				{
					Log.log("3G", "写卡失败"); 
					mFlag = ReturnEnum.FAIL; 
					if(mTryCount < mTryTotal)
					{
						showWriteSimCardBox();
					}
					else
					{
						failHandler();
					}
				}
			}
		}).start();
	}
	
	public void startWriteScentre()
	{
		Log.log("开始写入短信中心号码...");
		if (equipmentService.writesmscentre(smsNum, (byte) 0x02)) 
		{
			Log.e("3G", "写卡成功");
			mFlag = this.bll0D0F((byte) 0x00);
		} 
		else 
		{	
			 Log.log("3G", "写卡失败"); 
			 this.bll0D0F((byte) 0x01);
			 this.setLastKnownError("写卡失败\n失败原因: [写短信中心号码失败]");
		 	 mFlag = ReturnEnum.FAIL; 
		}
		mTryCount = -1;
	}
	
	public void failHandler()
	{
		Log.log("3G", "写卡失败"); 
		this.bll0D0F((byte)0x01);
		this.setLastKnownError("系统异常，写卡失败!");
	 	mFlag = ReturnEnum.FAIL; 
	 	mTryCount = mTryTotal + 1;
	}
	
	public String getLastknownError()
	{
		if (StringUtil.isEmptyOrNull(mLastknownError))
		{
			mLastknownError = "未知错误";
		}
		return mLastknownError;
	}

	public void setLastKnownError(String lastknownError)
	{
		this.mLastknownError = lastknownError;
	}
}
