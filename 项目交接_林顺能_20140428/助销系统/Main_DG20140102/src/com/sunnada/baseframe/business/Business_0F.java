package com.sunnada.baseframe.business;

import java.util.ArrayList;
import java.util.List;
import android.content.Context;

import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.InvoiceData;
import com.sunnada.baseframe.bean.InvoiceDataMonth;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.StringUtil;

// 店长推荐协议
public class Business_0F extends BaseBusiness 
{
	public static final String						TITLE_STR			= "发票打印提示";
	public static final String						TAG					= "发票打印";
	
	public int                                      mInvoiceType;                   // 发票类型
	public String                                   mBatchNum;                      // 批次号
	public String                                   mInvoiceNum;                    // 发票号
	public String                                   mInvoiceContent;                // 发票内容
	public int                                      mInvoiceInvalidStatus;          // 原发票作废状态(重打)
	public String                                   mOrigInvoiceNum;                // 原发票号码(重打)
	
	public int                                      mBusiType;                      // 业务类型
	public String                                   mSerialNum;                     // 流水号
	
	public int				                        mQueryTotal	        = 0x00;
	public int				                        mQueryStartIdx	    = 0x01;
	public int				                        mQueryLineNum	    = 0x0A;
	public int                                      mReturnLineNum      = 0x00;     // 返回的显示的行数
	
	public int                                      mQueryType          = 0x01;     // 查询类型
	public String                                   mQueryDate;                     // 查询日期
	public String                                   mQueryNum;                      // 查询号码
	public String                                   mServicePwd;                    // 服务密码
	public List<InvoiceData>                        mInvoiceDatas;                  // 发票数据列表
	
	// public int                                      mPrintType;                     // 打印类型
	public String                                   mBilldate;                      // 账期
	public int                                      mPrintMoney;                    // 金额
	
	public String                                   mQueryMonth;                    // 查询月份
	public String                                   mUserPwd;                       // 用户密码
	public List<InvoiceDataMonth>					mInvoiceDataMonths;             // 月结发票数据列表
	
	//0F55
	public String                                   mReportInvoiceNumDate;          // 日期
	public int                                      mPlatformInovicePrintNum;       // 平台打印发票数
	public int                                      mPlatformInoviceInvalidNum;     // 平台作废发票数
	public String									mLastKnownError;				// 错误信息
	
	public Business_0F(Context context, IEquipmentService equipmentService, ISocketService socketService, IDataBaseService dataBaseService) 
	{
		super(context, equipmentService, socketService, dataBaseService);
		// mStrErrorDealType = new String[] {Commen.hax2str(new byte[] {mTranceType})};
		// checkErrorDeal();
	}
	
	/**
	 * 终端发送发票打印请求
	 * 0 打印发票
     * 1 重打发票
	 * 2 补打发票
     * 3 不打发票
	 * 4 发票初始化
	 * 5 初始发票查询
	 * 6 发票作废
	 * 7 发票校准
	 * 8 发票递增
	 */
	public int bll0F51() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		
		sendPackage.setId("0F");
		sendPackage.setSubId("51");
		
		Buffer buffer = new Buffer();
	
		if(mInvoiceType == 4)
		{
			buffer.add("0F00");
		}
		else
		{
			if(mInvoiceType == 5)
			{
				buffer.add("0300");
			}
			else
			{
				buffer.add("0B00");
			}
		}
		// 打印机串号
		buffer.add((byte)Statics.mCurMap.get(Statics.BLUETOOTH_MAC).getBytes().length);
		buffer.add(Statics.mCurMap.get(Statics.BLUETOOTH_MAC).getBytes());
		// 发票类型
		buffer.add((byte) mInvoiceType);
		// 批次号
		if(mInvoiceType == 4)
		{
			buffer.add((byte) mBatchNum.getBytes().length);
			buffer.add(mBatchNum.getBytes());
		}
		// 发票号码
		if(mInvoiceType != 5)
		{
			buffer.add((byte) mInvoiceNum.getBytes().length);
			buffer.add(mInvoiceNum.getBytes());
		}
		sendPackage.addUnit("04C1", buffer.getBytes());
		
		if(mInvoiceType == 0 || mInvoiceType == 1 || mInvoiceType == 2)
		{
			buffer.clear();
			buffer.add((byte) mBusiType);
			buffer.add((byte) mSerialNum.getBytes().length);
			buffer.add(mSerialNum.getBytes());
			sendPackage.addUnit("0202", buffer.getBytes());
		}
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "失败原因:数据发送失败";
			setLastknownError(ss);
			return ret;
		}
		ret = receiveOnePackage("0F51");
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss = "失败原因:" + socketService.getLastKnowError();
			}
			setLastknownError(ss);
			return ret;
		}
		
		// 选号列表
		if(recivePackage.containsKey("04C1") == false)
		{
			String ss = "失败原因:服务器响应数据非法, 缺少04C1命令单元";
			setLastknownError(ss);
			return ReturnEnum.FAIL;
		}
		else
		{
			byte[] unit = recivePackage.get("04C1");
			int len = 0;
			int offset = 0;
			
			int map =  Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			
			if((map & 0x01) != 0)
			{
				offset += 1;
			}
			
			if((map & 0x02) != 0)
			{
				len = unit[offset];
				offset += 1;
				mBatchNum = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
			}
			
			if((map & 0x04) != 0)
			{
				len = unit[offset];
				offset += 1;
				// 发票号码
				mInvoiceNum = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
			}
			if((map & 0x08) != 0)
			{
				len = unit[offset];
				offset += 1;
				// 发票内容
				mInvoiceContent = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
			}
			
			if((map & 0x10) != 0)
			{
				// 重打原发票作废状态 
				mInvoiceInvalidStatus = unit[offset];
				offset += 1;
			}
			
			if((map & 0x20) != 0)
			{
				// 重打原发票号码
				len = unit[offset];
				offset += 1;
				mOrigInvoiceNum = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
			}
		}
		return ret;
	}
	
    // 终端发送手机交易查询
	public int bll0F52(boolean isFirst) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setId("0F");
		sendPackage.setSubId("52");
		
		Buffer buffer = new Buffer();
		if(isFirst)
		{
			buffer.add("1F00");
		}
		else
		{
			buffer.add("0F00");
		}

		// 查询类型
		buffer.add((byte) mQueryType);	
		// 查询日期
		buffer.add(mQueryDate.getBytes());
		// 查询号码
		buffer.add((byte) mQueryNum.getBytes().length);
		buffer.add(mQueryNum.getBytes());
		// 总数
		buffer.add((short) mQueryTotal);
		// 起始数
		buffer.add((short) mQueryStartIdx);
		// 显示行数
		buffer.add((byte) mQueryLineNum);
		
		// 第一次查询发
		if(isFirst)
		{
			buffer.add((byte) mServicePwd.getBytes().length);
			buffer.add(mServicePwd.getBytes());
		}
		sendPackage.addUnit("04C2", buffer.getBytes());
		
		if(isFirst)
		{
			sendPackage.addUnit("0208", (byte)mBusiType);
		}
		
		// 数据发收
		int ret = send(sendPackage);
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "现金发票查询失败!\n失败原因:数据发送失败";
			setLastknownError(ss);
			return ret;
		}
		ret = receiveOnePackage("0F52");
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "现金发票查询失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因:" + socketService.getLastKnowError();
			}
			setLastknownError(ss);
			return ret;
		}
		
		if(recivePackage.containsKey("04C2") == false)
		{
			String ss = "现金发票查询失败!\n失败原因:服务器响应数据非法, 缺少04C2命令单元";
			setLastknownError(ss);
			return ret;
		}
		else
		{
			byte[] unit = recivePackage.get("04C2");
			int offset = 0;
			int len = 0;
			// 数据域
			int map = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			
			// 查询类型
			if ((map & 0x01) != 0) 
			{
				offset += 1;
			}
			// 查询时间
			if ((map & 0x02) != 0) 
			{
				offset += 16;
			}
			// 查询号码
			if ((map & 0x04) != 0) 
			{
				len = unit[offset];
				offset += 1;
				offset += len;
			}
			
			// 翻页信息
			if ((map & 0x08) != 0)
			{
				mQueryTotal = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
				offset += 5;
			}
			
			// 交易信息
			if ((map & 0x0010) != 0)
			{
				// 发票数据列表
				// mInvoiceDatas = new ArrayList<InvoiceData>();
				offset += 2;
				int iTotal = Commen.bytes2int(new byte[]{unit[offset], unit[offset+1]});
				offset += 2;
				for (int j = 0; j < iTotal; j++) 
				{
					String  serailNum   = null;  // 流水号
					String  busiType    = null;  // 交易类型
					int     money       = 0;     // 金额(以分为单位)
					String  cMoney      = null;  // 金额(以元为单位)
					String  busiTime    = null;  // 交易时间
					String  deviceType  = null;  // 设备类型
					int     printCount  = 0;     // 打印次数

					// 获取流水
					len = unit[offset];
					offset += 1;
					serailNum = StringUtil.encodeWithGBK(unit, offset, len);
					offset += len;
					// 获取交易类型
					len = unit[offset];
					offset += 1;
					busiType = StringUtil.encodeWithGBK(unit, offset, len);
					offset += len;
					// 获取金额
					money = Commen.bytes2int(unit, 0, 4);
					// 显示的时候分转化为元
					cMoney = String.format("%.2f", (float) money/ 100); 
					
					offset += 4;
					// 获取时间
					len = unit[offset];
					offset += 1;
					busiTime = StringUtil.encodeWithGBK(unit, offset, len);
					offset += len;
					// 设备类型
					len = unit[offset];
					offset += 1;
					deviceType = StringUtil.encodeWithGBK(unit, offset, len);
					offset += len;
					// 打印次数
					printCount = unit[offset];
					offset += 1;
					
					InvoiceData invoiceData = new InvoiceData(serailNum, busiType, 
							money, cMoney, busiTime, deviceType, printCount);
					mInvoiceDatas.add(invoiceData);
				}
			}
		}
		return ret;
	}
	
	// 终端发送月结账单发票打印
	public int bll0F53() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setId("0F");
		sendPackage.setSubId("53");
		
		Buffer buffer = new Buffer();
		// 数据域
		buffer.add("FF00");
		// 打印机串号
		buffer.add((byte)Statics.mCurMap.get(Statics.BLUETOOTH_MAC).getBytes().length);
		buffer.add(Statics.mCurMap.get(Statics.BLUETOOTH_MAC).getBytes());
		// 发票类型
		buffer.add(mInvoiceType);
		// 发票号码
		buffer.add((byte) mInvoiceNum.getBytes().length);
		buffer.add(mInvoiceNum.getBytes());
		// 查询号码
		buffer.add((byte) mQueryNum.getBytes().length);
		buffer.add(mQueryNum.getBytes());
		// 打印类型
		buffer.add((byte) 0x00);
		// 账期
		buffer.add(mBilldate.getBytes());
		// 金额
		buffer.add(mPrintMoney);
		// 查询流水
		buffer.add((byte) mSerialNum.getBytes().length);
		buffer.add(mSerialNum.getBytes());
		
		sendPackage.addUnit("04C1", buffer.getBytes());
		
		buffer.clear();
		buffer.add((byte) mBusiType);
		sendPackage.addUnit("0208", buffer.getBytes());
		// 数据收发
		int ret = send(sendPackage);
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "发送月结账单发票打印失败!\n失败原因:数据发送失败";
			setLastknownError(ss);
			return ret;
		}
		ret = receiveOnePackage("0F53");
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "发送月结账单发票打印失败!\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "失败原因:" + socketService.getLastKnowError();
			}
			setLastknownError(ss);
			return ret;
		}
		
		if(recivePackage.containsKey("04C1") == false)
		{
			String ss = "发送月结账单发票打印失败!\n失败原因:服务器响应数据非法, 缺少04C1命令单元";
			setLastknownError(ss);
			return ret;
		}
		else
		{
			byte[] unit = recivePackage.get("04C1");
			int offset = 0;
			int len = 0;
			
			int map = Commen.bytes2int(new byte[]{unit[offset], unit[offset+1]});
			offset += 2;
			
			// 发票类型
			if ((map & 0x01) != 0) 
			{
				offset += 1;
			}

			// 发票号码
			if ((map & 0x02) != 0)
			{
				len = unit[offset];
				offset += 1;
				mInvoiceNum = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
			}
			
			// 发票内容
			if ((map & 0x04) != 0) 
			{
				len = unit[offset];
				offset += 1;
				mInvoiceContent = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
			}
		}
		return ret;
	}
	
	// 发送月结账单查询
	public int bll0F54() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setId("0F");
		sendPackage.setSubId("54");
		
		Buffer buffer = new Buffer();
		// 数据域
		buffer.add("0700");
		// 查询月份
		buffer.add(mQueryMonth);
		// 查询号码
		buffer.add((byte) mQueryNum.getBytes().length);
		buffer.add(mQueryNum.getBytes());
		// 用户密码
		buffer.add((byte) mUserPwd.getBytes().length);
		buffer.add(mUserPwd.getBytes());
		
		sendPackage.addUnit("04C2", buffer.getBytes());
		sendPackage.addUnit("0208", (byte) mBusiType);
		
		buffer.clear();
		buffer.add((byte) mServicePwd.getBytes().length);
		buffer.add(mServicePwd.getBytes());
		sendPackage.addUnit("036B", buffer.getBytes());
		// 数据收发
		int ret = send(sendPackage);
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "月结发票查询失败!\n失败原因:数据发送失败";
			setLastknownError(ss);
			return ret;
		}
		ret = receiveOnePackage("0F54");
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "月结发票查询失败!\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "失败原因:" + socketService.getLastKnowError();
			}
			setLastknownError(ss);
			return ret;
		}
		if (!recivePackage.containsKey("04C2"))
		{
			String ss = "月结发票查询失败!\n失败原因:服务器响应数据非法, 缺少04C2命令单元";
			setLastknownError(ss);
			return ret;
		}
		else
		{
			byte[] unit = recivePackage.get("04C2");
			int offset = 0;
			int len = 0;
			
			mInvoiceDataMonths = new ArrayList<InvoiceDataMonth>();
			String queryNum = null;   // 查询号码
			String serialNum = null;  // 查询流水
			String billDate;   // 账期
			int    printMoney; // 打印金额(以分为单位)
			String cPrintMoney;// 打印金额(以元为单位)
			
			int map = Commen.bytes2int(unit, offset, offset+1);
			offset += 2;
			
			// 查询日期
			if ((map & 0x01) != 0) 
			{
				offset += 12;
			}
			
			// 查询号码
			if ((map & 0x02) != 0) 
			{
				len = unit[offset];
				offset += 1;
				queryNum = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
			}
			
			// 查询流水
			if ((map & 0x04) != 0) 
			{
				len = unit[offset];
				offset += 1;
				serialNum = StringUtil.encodeWithGBK(unit, offset, len);
				offset += len;
			}
			
			// 查询交易信息
			if ((map & 0x08) != 0) 
			{
				// 信息长度
				offset += 2;
				// 信息条数
				int total = Commen.bytes2int(unit, offset, 2);
				offset += 2;
				
				for (int i = 0; i < total; i++) 
				{
					// 信息账期
					billDate = StringUtil.encodeWithGBK(unit, offset, 6);
					offset += 6;
					// 金额
					printMoney = Commen.bytes2int(unit, offset, 4);
					// 显示的时候分转化为元
					cPrintMoney = String.format("%.2f", (float) printMoney/ 100); 
					offset += 4;
					// 号码：queryNum,信息条数xxnums,账期：billdata,打印金额：cPrintMoney
					InvoiceDataMonth invoiceDataMonth = new InvoiceDataMonth(queryNum,
							serialNum, billDate, printMoney, cPrintMoney);
					mInvoiceDataMonths.add(invoiceDataMonth);
				}
			}
		}
		return ret;
	}
	
	// 终端发票数上报
	public int bll0F55(String queryDate) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setId("0F");
		sendPackage.setSubId("55");
		
		Buffer buffer = new Buffer();
		// 数据域
		buffer.add("0100");
		// 日期
		buffer.add(queryDate.getBytes());
		
		sendPackage.addUnit("04C1", buffer.getBytes());
		// 数据收发
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "终端发票数上报失败!\n失败原因:数据发送失败";
			setLastknownError(ss);
			return ret;
		}
		ret = receiveOnePackage("0F55");
		if (ret != ReturnEnum.SUCCESS)
		{
			String ss = "终端发票数上报失败!\n";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))
			{
				ss += "失败原因:" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return ret;
		}
		// 套餐详情
		if (!recivePackage.containsKey("04C1"))
		{
			String ss = "终端发票数上报失败！\n失败原因:服务器响应数据非法, 缺少04C1命令单元";
			setLastknownError(ss);
			return ret;
		}
		else
		{
			byte[] unit = recivePackage.get("04C1");
			int offset = 0;
			int bitmap = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
	        offset += 2;
			
			// 日期
			if ((bitmap & 0x01) != 0)
			{
				mReportInvoiceNumDate = StringUtil.encodeWithGBK(unit, offset, 8);
				offset += 8;
			}
			
			// 平台打印发票数
			if ((bitmap & 0x02) != 0)
			{
				mPlatformInovicePrintNum = unit[offset];
				offset += 1;
			}
			// 平台作废发票数
			if ((bitmap & 0x04) != 0)
			{
				mPlatformInoviceInvalidNum = unit[offset];
				offset += 1;
			}
		}
		return ret;
	}
	
	public String getLastknownError() 
	{
		if (StringUtil.isEmptyOrNull(mLastKnownError))
		{
			mLastKnownError = "未知错误";
		}
		return mLastKnownError;
	}

	public void setLastknownError(String lastKnownError)
	{
		this.mLastKnownError = lastKnownError;
	}
	
	public Package getReceivePackage()
	{
		return recivePackage;
	}
	
}