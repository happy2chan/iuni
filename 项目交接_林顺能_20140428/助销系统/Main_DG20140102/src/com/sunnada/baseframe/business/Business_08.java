package com.sunnada.baseframe.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.StringUtil;

import android.content.Context;
import android.util.Log;

// 增值业务
public class Business_08 extends BaseBusiness 
{
	public static final String			ADDVALUE_ID			= "addvalue_id";
	public static final String			ADDVALUE_CODE		= "addvalue_code";
	public static final String			ADDVALUE_NAME		= "addvalue_name";
	
	public String 						mStrDetail 			= null; 									// 增值业务详情
	
	public List<Map<String, String>> 	mListBusi 			= new ArrayList<Map<String, String>>();		// 查询业务结果
	public byte[] 						blltime;
	public byte[] 						bllnum;
	
	public Business_08(Context context, IEquipmentService equipmentService, ISocketService socketService, IDataBaseService dataBaseService) 
	{
		super(context, equipmentService, socketService, dataBaseService);
		TITLE_STR = "增值业务办理提示";
		TAG = "增值业务";
	}
	
	// 查询增值业务列表
	// 0 订购 1 退购
	public boolean bll0801(int addFlag, String strTelNum) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("01");
		sendPackage.setId("08");
		
		Buffer buffer = new Buffer();
		buffer.add("0100");
		buffer.add(strTelNum.getBytes());
		sendPackage.addUnit("0472", buffer.getBytes());
		
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0801");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取增值业务列表失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError()))  
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		
		// 清除mListBusi中的数据
		mListBusi.clear();
		// 增值业务数据
		if(recivePackage.containsKey("0472") == false) 
		{
			DialogUtil.showMessage(TITLE_STR, "未找到相关增值业务数据!");
			return true;
		}
		else
		{
			byte[] unit = recivePackage.get("0472");
			int offset = 0;
			int map = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 手机号码
			if((map&0x01) != 0)
			{
				offset += 11;
			}
			// 增值业务编码
			if((map&0x02) != 0)
			{
				offset += 4;
			}
			// 增值业务数据
			if((map&0x04) != 0)
			{
				// 增值业务总数
				int total = unit[offset++];
				int valid = 0;
				for (int index = 0; index < total; index++) 
				{
					Map<String, String> addMap = new HashMap<String, String>();
					// 增值业务标识
					int nAddFlag = unit[offset++]&0xFF;
					// 增值业务编码
					int nAddCode = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1], unit[offset+2], unit[offset+3]});
					offset += 4;
					// 增值业务名称
					int len = unit[offset++]&0xFF;
					String strAddName = StringUtil.encodeWithGBK(unit, offset, len);
					offset += len;
					Log.e("", "增值业务标识: " + nAddFlag + ", 增值业务编码: " + nAddCode + ", 增值业务名称: " + strAddName);
					// 
					if (nAddFlag == addFlag) 
					{
						valid++;
						addMap.put(ADDVALUE_ID, 	String.format("%02d", valid));
						addMap.put(ADDVALUE_CODE, 	String.valueOf(nAddCode));
						addMap.put(ADDVALUE_NAME, 	strAddName);
						mListBusi.add(addMap);
					}
				}
			}
		}
		return true;
	}
	
	// 增值业务类型详情
	public boolean bll0406(int nAddCode) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("06");
		sendPackage.setId("04");
		
		Buffer buffer = new Buffer();
		buffer.add("0300");
		buffer.add((byte)0x04);
		buffer.add(nAddCode);
		sendPackage.addUnit("0366", buffer.getBytes());
		
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0406");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取增值业务详情失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		
		// 增值业务详情
		if (!recivePackage.containsKey("0366")) 
		{
			DialogUtil.showMessage(TITLE_STR, "获取增值业务详情失败!\n失败原因: [服务器响应数据非法, 缺少0366命令单元]");
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0366");
			int offset = 7;
			// 详情长度
			int len = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 详情内容
			byte[] zzInfo = new byte[len];
			System.arraycopy(unit, offset, zzInfo, 0, len);
			offset += len;
			try 
			{
				mStrDetail = new String(zzInfo, "GBK");
				Log.i("", "增值业务详情为: " + mStrDetail);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}
		return true;
	}
	
	// 终端向平台发送订购信息
	public boolean bll0802(String strTelNum, int nAddCode) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("02");
		sendPackage.setId("08");
		
		Buffer buffer = new Buffer();
		buffer.add("0300");
		buffer.add(strTelNum.getBytes());
		buffer.add(nAddCode);
		sendPackage.addUnit("0472", buffer.getBytes());
		
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0802");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "订购增值业务失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		
		// 交易流水号
		if (recivePackage.containsKey("0203")) 
		{
			try 
			{
				byte[] unit = recivePackage.get("0203");
				// 增值业务订购下发的流水号为30位的，没有包括类型和长度
				bllnum = new byte[30];
				System.arraycopy(unit, 0, bllnum, 0, 30);
				Log.e("", "业务流水号为:" + new String(bllnum, "ASCII"));
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				DialogUtil.showMessage(TITLE_STR, "订购增值业务失败!\n失败原因: [转化流水号异常]");
				return false;
			}
		}
		// 获取交易时间
		if (recivePackage.containsKey("0204")) 
		{
			try 
			{
				byte[] unit = recivePackage.get("0204");
				blltime = new byte[14];
				System.arraycopy(unit, 0, blltime, 0, 14);
				
				Log.e("", "获取交易时间ASCII: " + new String(blltime, "ASCII"));
				Log.e("", "获取交易时间GBK: " + new String(blltime, "GBK"));
				Log.e("", "获取交易时间UTF-8: " + new String(blltime, "UTF-8"));
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				DialogUtil.showMessage(TITLE_STR, "订购增值业务失败!\n失败原因: [转化交易时间异常!]");
				return false;
			}
		}
		return true;
	}
	
	// 终端退订增值业务
	public boolean bll0803(String strTelNum, int nAddCode) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("03");
		sendPackage.setId("08");

		Buffer buffer = new Buffer();
		buffer.add("0300");
		buffer.add(strTelNum.getBytes());
		buffer.add(nAddCode);
		sendPackage.addUnit("0472", buffer.getBytes());
		
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0803");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "退订增值业务失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		
		// 交易流水
		if (recivePackage.containsKey("0203")) 
		{
			try 
			{
				byte[] unit = recivePackage.get("0203");
				// 增值业务下发的流水号是30位的，没有包括类型和长度
				bllnum = new byte[30];
				System.arraycopy(unit, 0, bllnum, 0, 30);
				Log.e("", "业务流水号为: " + new String(bllnum, "ASCII"));
			} 
			catch (Exception e) 
			{
				e.printStackTrace(); 
				DialogUtil.showMessage(TITLE_STR, "退订增值业务失败!\n失败原因: [转化交易流水号异常]");
				return false;
			}
		}
		// 获取交易时间
		if (recivePackage.containsKey("0204")) 
		{
			try 
			{
				byte[] unit = recivePackage.get("0204");
				blltime = new byte[14];
				System.arraycopy(unit, 0, blltime, 0, 14);
				
				Log.e("", "获取交易时间: " + new String(blltime, "ASCII"));
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				DialogUtil.showMessage(TITLE_STR, "退订增值业务失败!\n失败原因: [转化交易时间异常]");
				return false;
			} 
		}
		return true;
	}
	
	// 增值业务订购成功后给手机发送短（改变增值业务订购成功）
	public boolean bll0804(String strTelNum, int nAddCode) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("04");
		sendPackage.setId("08");
		
		Buffer buffer = new Buffer();
		buffer.add("0300");
		buffer.add(strTelNum.getBytes());
		buffer.add(nAddCode);
		sendPackage.addUnit("0472", buffer.getBytes());
		
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0804");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "发送短信通知失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		return true;
	}
	
	// 增值业务退订成功短信通知
	public boolean bll0805(String strTelNum, int nAddCode) 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("05");
		sendPackage.setId("08");
		
		Buffer buffer = new Buffer();
		buffer.add("0300");
		buffer.add(strTelNum.getBytes());
		buffer.add(nAddCode);
		sendPackage.addUnit("0472", buffer.getBytes());
		
		int ret = send(sendPackage);
		if (ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		ret = receiveOnePackage("0805");
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "发送短信通知失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.showMessage(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		return true;
	}
}
