package com.sunnada.baseframe.business;

import java.io.File;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.bean.Buffer;
import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.database.ErrorDealAdapter;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.StringUtil;

import sunnada.jni.Commcenter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

// 购买电子卡
public class BusinessBuycard extends BaseBusiness 
{
	public static final String		mEcardFile		= Statics.PATH_CONFIG+"/ecard";
	
	public byte 					mFaceIdx 		= 0x01;
	public byte 					mEcardNum 		= 0x01;
	public StringBuffer 			mEcardInfo;
	private String					mStrDealMoney;
	
	public byte						mTranceType		= 0;									// 交易类型
	private boolean					mIsDoBuyCard 	= false;
	private Handler					mHandler		= null;
	public String					mStrPswd;												// 交易密码
	
	// 构造函数
	public BusinessBuycard(Context context, IEquipmentService equipmentService, ISocketService socketService, 
			IDataBaseService dataBaseService, Handler handler) 
	{
		super(context, equipmentService, socketService, dataBaseService);
		TITLE_STR 	= "电子卡业务办理提醒";
		TAG 		= "电子卡";
		
		this.mTranceType = 0x07;
		mHandler = handler;
	}
	
	// 电子卡交易流程
	public void doBuyCard()
	{
		if(mIsDoBuyCard == false) 
		{
			mIsDoBuyCard = true;
			// 获取交易流水号
			DialogUtil.showProgress("正在获取交易流水号...");
			if(bll0301()) 
			{
				// 购卡请求
				DialogUtil.showProgress("正在发起购卡请求...");
				if(bll0302()) 
				{
					// 一次确认
					DialogUtil.showProgress("正在发起一次交易确认...");
					for(int i=0; i<3; i++) 
					{
						if(bll0303(i)) 
						{
							// 二次确认
							DialogUtil.showProgress("正在发起二次交易确认...");
							bll0304();
							DialogUtil.MsgBox(
									"购买成功, 卡密列表如下", 
									mEcardInfo.toString(), 
									"确定", 
									Statics.MSG_PRINT, 
									null, 
									0x00, 
									Statics.MSG_PRINT, 
									mHandler);
							break;
						}
					}
				}
			}
			mIsDoBuyCard = false;
			DialogUtil.closeProgress();
		}
	}
	
	// 获取流水号
	private boolean bll0301() 
	{
		// 组包 0301
		sendPackage = new Package();
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("01");
		sendPackage.setId("03");
		
		// 业务类型
		sendPackage.addUnit("0202", new byte[] {(byte)mTranceType});
		// 交易密码
		Buffer buffer = new Buffer();
		byte[] pwd = PWDEncryption(mStrPswd);
		buffer.add((byte)pwd.length);
		buffer.add(pwd);
		sendPackage.addUnit("0215", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0301");
		}
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "获取交易流水号失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.MsgBox(TITLE_STR, ss, Statics.MSG_PWD_FAIL, mHandler);
			setLastknownError(ss);
			return false;
		}
		
		// 业务流水号
		if (recivePackage.containsKey("0202") == false) 
		{
			String ss = "获取交易流水号失败!\n失败原因: [服务器响应数据非法, 缺少0202命令单元]";
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0202");
			int len  = unit[1]&0xFF;
			mStrBillNum = StringUtil.encodeWithGBK(unit, 2, len);
			Log.i(TAG, "业务流水号为: " + mStrBillNum);
			return true;
		}
	}
	
	// 请求购买电子卡
	private boolean bll0302() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("02");
		sendPackage.setId("03");
		
		Buffer buffer = new Buffer();
		buffer.add("1F00");					// 数据域，提交电子卡信息数据域好像是003F，不是001F
		buffer.add("0000"); 				// 金融CRC
		buffer.add((byte)mTranceType); 		// 业务类型
		buffer.add("1E"); 					// 流水
		buffer.add(encryptSerialNumber(mStrBillNum.getBytes()));
		buffer.add("00"); 					// 扣款状态
		buffer.add(mFaceIdx); 				// 面值编码
		buffer.add(mEcardNum); 				// 购卡张数
		Log.e("", "面值编码为: " + mFaceIdx + ", 购买张数为: " + mEcardNum);

		byte[] tt = buffer.getBytes(); 		// CRC计算
		byte[] bb = new byte[tt.length-4];
		System.arraycopy(tt, 4, bb, 0, tt.length-4);
		byte[] crc = Commcenter.CalcCrc16(bb, (char)0);
		tt[2] = crc[0];
		tt[3] = crc[1];
		sendPackage.addUnit("0341", tt);
		
		// 预先记录异常
		mStrErrBill 	=  mStrBillNum; 								// 流水
		mStrErrReason 	= Commen.hax2str(new byte[] { 0x0C }); 			// 记录原因
		mStrErrType 	= Commen.hax2str(new byte[] { mTranceType }); 	// 交易类型
		mStrErrCrc 	 	= Commen.hax2str(new byte[] { crc[0], crc[1] });
		ErrorDealAdapter.setErrorInfo(context, mStrErrPsamID, mStrErrType, mStrErrBill, mStrErrReason, mStrErrCrc, "1");
		// 同时插入到数据库中
		mStrDealMoney = String.valueOf(Integer.parseInt(Statics.DZK_Type[mFaceIdx-1][1])*mEcardNum);
		mBusinessNum.insertData(
				App.getUserNo(), 
				"无", 
				"购买电子卡", 
				mStrDealMoney, 
				"交易失败", 
				mStrErrBill,
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()), 
				mStrDealMoney, 
				mTranceType);
				
		// 数据处理
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS)
		{
			ret = receiveOnePackage("0302");
		}
		// 更新异常交易记录状态
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "获取卡密信息失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "失败原因: [" + socketService.getLastKnowError() + "]";
			}
			DialogUtil.MsgBox(TITLE_STR, ss);
			setLastknownError(ss);
			return false;
		}
		
		// 数据解析
		// 交易时间
		if(recivePackage.containsKey("0204")) 
		{
			mBillTime = recivePackage.get("0204");
		}
		// 电子卡信息
		if (!recivePackage.containsKey("0341")) 
		{
			String ss = "获取卡密信息失败!\n失败原因: [服务器响应数据非法, 缺少0341命令单元]";
			DialogUtil.MsgBox(TITLE_STR, ss);
			return false;
		}
		else
		{
			byte[] unit = recivePackage.get("0341");
			int offset = 0;
			int map = Commen.bytes2int(new byte[] {unit[offset], unit[offset+1]});
			offset += 2;
			// 金融CRC
			if ((map & 0x0001) != 0) 
			{
				offset += 2;
			}
			// 流水
			if ((map & 0x0002) != 0) 
			{
				offset += 32;
			}
			// 扣款状态
			if ((map & 0x0004) != 0) 
			{ 
				offset += 1;
			}
			// 面值编码
			if ((map & 0x0008) != 0) 
			{
				offset += 1;
			}
			// 购卡张数
			if ((map & 0x0010) != 0) 
			{ 
				offset += 1;
			}
			// 交易密码
			if ((map & 0x0020) != 0) 
			{
				offset += 20;
			}
			// 卡号卡密
			if ((map & 0x0040) != 0) 
			{
				byte[] cardnum 		= new byte[20];
				byte[] cardpwd 		= new byte[20];
				byte[] cardEndtime 	= new byte[8];

				mEcardInfo = new StringBuffer();
				mEcardInfo.append("本次购卡总数为:" + mEcardNum + "张");
				File file = new File(mEcardFile);
				if(file.exists() == false) 
				{
					file.delete();
				}
				RandomAccessFile out = null;
				try
				{
					out = new RandomAccessFile(file, "rwd");
					if(out != null) 
					{
						out.write((byte)mEcardNum);
					}
				}
				catch(Exception e) 
				{
					e.printStackTrace();
				}
				
				for (int i=0; i<mEcardNum; i++) 
				{
					System.arraycopy(unit, offset, cardnum, 0, 20);
					offset += 20;
					System.arraycopy(unit, offset, cardpwd, 0, 20);
					offset += 20;
					System.arraycopy(unit, offset, cardEndtime, 0, 8);
					offset += 8;

					try 
					{
						if(out != null) 
						{
							out.write(cardnum);			// 卡号
							out.write(cardpwd);			// 卡密
							out.write(mFaceIdx);		// 面值编码
							out.write(cardEndtime);		// 有效期
						}
					}
					catch(Exception e) 
					{
						e.printStackTrace();
					}
					mEcardInfo.append("\n账号: ")
							 .append(new String(cardnum), 0, Commen.endofstr(cardnum))
							 .append("\n密码: ")
							 .append(new String(cardpwd), 0, Commen.endofstr(cardpwd))
							 .append("\n面值: ")
							 .append(Statics.DZK_Type[mFaceIdx-1][1] + "元")
							 .append("\n有效期: ")
							 .append(StringUtil.converDateString(new String(cardEndtime))).append("\n");
				}
				try 
				{
					if(out != null) 
					{
						out.write(mStrBillNum.getBytes(), 0, 30);
						// 银联签购单信息
						byte[] temp = new byte[1];
						/*
						if(2 == postype) 
						{
							temp[0] = 0x01;
							out.write(temp);
							savePosDealInfo(out, StringUtil.formatDate(new Date(), "yyyy/MM/dd HH:mm"), 
									String.format("%.2f", (float)CommonValue.posDealMoney/100));
						}
						else 
						*/
						{
							temp[0] = 0x00;
							out.write(temp);
						}
						out.close();
						out = null;
					}
				}
				catch(Exception e) 
				{
					e.printStackTrace();
				}
				Log.i(TAG, mEcardInfo.toString());
			}
			
			mBusinessNum.updateBussinessNum(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()), 
					"交易失败", mStrDealMoney, mStrErrBill, mStrDealMoney);
			return true;
		}
	}
	
	// 成功交易上报
	private boolean bll0303(int nTimes)  
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("03");
		sendPackage.setId("03");
		
		Buffer buffer = new Buffer();
		buffer.add((byte)mTranceType); 			// 业务类型
		buffer.add("1E"); 						// 流水长度
		buffer.add(mStrBillNum.getBytes()); 	// 流水号
		sendPackage.addUnit("0202", buffer.getBytes());

		// 更新异常记录状态
		ErrorDealAdapter.setErrorInfoState(context, 
				mStrErrPsamID, 
				mStrErrType, 
				Commen.hax2str(new byte[] {0x0D}), 
				"1", 
				mStrErrCrc, 
				mStrErrBill);
		
		Log.i(TAG, "一次交易确认");
		// 数据收发
		int ret = send(sendPackage);
		if(ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("0303");
		}
		// 更新异常交易记录状态
		if(ret != ReturnEnum.RECIEVE_TIMEOUT) 
		{
			ErrorDealAdapter.setErrorInfoState(context, mStrErrPsamID, mStrErrType, "0", "0", mStrErrCrc, mStrErrBill);
		}
		if(ret != ReturnEnum.SUCCESS) 
		{
			String ss = "发起一次交易确认失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			Log.e(TAG, ss);
			// 
			if(nTimes == 2) 
			{
				DialogUtil.MsgBox(TITLE_STR, ss);
				// 删除卡密文件
				File file = new File(mEcardFile);
				if(file.exists()) 
				{
					file.delete();
				}
			}
		}
		Log.i(TAG, "一次交易确认成功");
		// 更改交易记录状态
		mBusinessNum.updateBussinessNum(
				new SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(Calendar.getInstance().getTime()), 
				"交易成功", 
				mStrDealMoney, 
				mStrErrBill, 
				mStrDealMoney);
		return true;
	}
	
	// 成功交易上报
	private boolean bll0304() 
	{
		sendPackage = new Package();
		sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setSubId("04");
		sendPackage.setId("03");
		
		Buffer buffer = new Buffer();
		buffer.add((byte)mTranceType); 				// 业务类型
		buffer.add("1E"); 							// 流水长度
		buffer.add(mStrBillNum.getBytes()); 		// 流水号
		sendPackage.addUnit("0202", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if(ret != ReturnEnum.SUCCESS)
		{
			return false;
		}
		/*
		ret = receiveOnePackage("0304");
		if(ret != ReturnEnum.SUCCESS)
		{
			String ss = "发起二次交易确认失败!";
			if(!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			Log.e(TAG, ss);
			return false;
		}
		*/
		Log.i(TAG, "二次交易确认成功!");
		return true;
	}
	
	// 上报打印失败或未打印卡密
	public boolean bll030A(final int nResult) 
	{
		sendPackage = new Package();
		//sendPackage.setServiceCommandID(recivePackage.getServiceCommandID());
		sendPackage.setServiceCommandID("FFFF");
		sendPackage.setSubId("0A");
		sendPackage.setId("03");
		
		Buffer buffer = new Buffer();
		buffer.add("0300");                 // 数据域
		buffer.add((byte)nResult);			// 打印结果 0x01-成功 0x02-失败
		buffer.add("1E");                   // 流水长度
		//buffer.add(mStrBillNum.getBytes());
		buffer.add(encryptSerialNumber(mStrBillNum.getBytes()));
		sendPackage.addUnit("0231", buffer.getBytes());
		
		// 数据收发
		int ret = send(sendPackage);
		if (ret == ReturnEnum.SUCCESS) 
		{
			ret = receiveOnePackage("030A");
		}
		if (ret != ReturnEnum.SUCCESS) 
		{
			String ss = "上报未打印卡密信息失败!";
			if (!StringUtil.isEmptyOrNull(socketService.getLastKnowError())) 
			{
				ss += "\n失败原因: [" + socketService.getLastKnowError() + "]";
			}
			setLastknownError(ss);
			return false;
		}
		Log.i(TAG, "上报未打印卡密信息成功");
		return true;
	}
	
	// 打印电子卡信息
	public void print() 
	{
		StringBuffer strbuf = new StringBuffer();
		strbuf.append("业务类型: 购买电子卡 \n");
		strbuf.append(mEcardInfo.toString());
		Log.e("电子卡信息: ", strbuf.toString());
		// 调用基类的打印方法
		print(strbuf, null, true);
		
		/*
		// 打印银联交易单
		if(2 == postype) 
		{
			if(!UILogic.isNUll(mPosDealTime[0]) || !UILogic.isNUll(mPosDealMoney[0])) 
			{
				this.printPosDeal(mPosDealTime[0], mPosDealMoney[0]);
			}
			else 
			{
				this.printPosDeal(null, null);
			}
		}
		*/
	}
}
