package com.sunnada.baseframe.update;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.util.Xml;
import android.view.View;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.database.KeyValueDao;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.listener.BreakPointDownloadListener;
import com.sunnada.baseframe.util.HttpURLConnectionUtil;
import com.sunnada.baseframe.util.StringUtil;

// 从服务器获取XML解析并进行比对版本号  
public class CheckVersionTask implements BreakPointDownloadListener
{ 
	private static final String 	TAG 			= "软件升级";
	private static final String		PACKAGE_NAME	= "com.sunnada.baseframe.activity";
	private static final String 	TITLE 			= "软件升级提示";
	protected static final int 		DOWN_ERROR 		= 0;

	private UpdataInfo 				mUpdataInfo;
	public 	String 					sErrMsg 		= "";
	private static String 			mVersionName 	= null;
	private Activity				mMainActivity;
	private Handler					mHandler		= null;
	
	private long					mCurrentLength 	= 0;
	private long					mTotalLength	= 0;
	private String					mErrorStr		= null;
	
	public CheckVersionTask(Activity activity, Handler handler) 
	{
		mMainActivity 	= activity;
		mHandler		= handler;
		mVersionName 	= getVersionName();
	}
	
	// 获取当前程序的版本号
	public String getVersionName()
	{
		if(mVersionName != null) 
		{
			return mVersionName;
		}
		mVersionName = BaseActivity.equipmentService.getFrameVersion();
		return mVersionName;
	}
	
	// 找到历史遗留版本
	public void findHistoryVersionAndUnintall()
	{
		// 获得PackageManager对象
		PackageManager pm = mMainActivity.getPackageManager();  
		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null); 
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);  
		List<ApplicationInfo> resolveInfos = pm.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES);  
        if (resolveInfos != null) 
        {  
            for (ApplicationInfo reInfo:resolveInfos) 
            {
            	// 获得应用程序的包名
            	String pkgName = reInfo.packageName;  
                Log.v("xxx", "包名" + pkgName);
                if(PACKAGE_NAME.equals(pkgName.trim())) 
                {
                	Log.v("xxx", "检测到历史遗留版本，开始进行卸载");
                	DialogUtil.MsgBox(TITLE, "检测到早期版本的程序, 该程序已经不能使用, 我们将进行卸载", 
                	"确定", new View.OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							uninstallAPK();
						}
					}, "", null, null);
					/*
                	DialogUtil.showMessage(
                			"温馨提示", 
                			"检测到早期版本的程序, 该程序已经不能使用, 我们将进行卸载", 
                			"确定", 
                			new View.OnClickListener()
        					{
        						@Override
        						public void onClick(View v)
        						{
        							uninstallAPK();
        						}
        					});
        			*/
                	break;
                }
            }  
        }
	}
	
	// 卸载APK
	public void uninstallAPK() 
	{  
		try
		{
	        Uri uri = Uri.parse("package:" + PACKAGE_NAME);
	        Intent intent = new Intent(Intent.ACTION_DELETE, uri);
	        mMainActivity.startActivity(intent);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Log.v("提示", "删除apk: " + PACKAGE_NAME + "失败");
		}
    }
	
	// 检测提示更新
	// 升级方式：
	// 1、apk从00目录升级
	// 2、/mini/busi_deal从00目录升级
	// 3、/mini 下除/mini/busi_deal之外的目录，从省份目录升级
	public boolean checkupdate(boolean isNoticeNot) 
	{
		try 
		{
			DialogUtil.showProgress("正在检测更新...");
			// 检测是否需要更新
			// 0-不需要更新 1-提示更新 2-强制更新
			Statics.IS_FORCED_UPDATE = false;
			int flag = isApkNeedUpdate();
			if (flag == 0) 
			{
				Log.i(TAG, "版本号相同无需升级");
				if(isNoticeNot) 
				{
					// 不需要升级的时候是否提示
					DialogUtil.MsgBox(TITLE, "温馨提示：您已经安装了最新版本", "确定", 0, "", 0, 0, null);
					//DialogUtil.showMessage(TITLE, "温馨提示：您已经安装了最新版本");
				}
				else
				{
					// 取消滚动条
					DialogUtil.closeProgress();
				}
				return false;
			} 
			else if(flag == -1)
			{
				// 需要提示错误信息
				if(isNoticeNot) 
				{
					DialogUtil.MsgBox("温馨提示", sErrMsg);
				}
				DialogUtil.closeProgress();
				return false;
			}
			else 
			{
				Log.i(TAG, "版本号不同 ,提示用户升级 ");
				final StringBuffer strbuf = new StringBuffer();
				/*
				String sUpdateLog = "成功升级信息：\n升级前版本【"+mVersionName+"】，" 
				+ "升级后版本【"+mUpdataInfo.getVersion()+"】\n" 
				+ "升级后版本信息：\n"+mUpdataInfo.getDescription();
				*/
				
				strbuf.append("发现新版本，是否进行更新\n")
					  .append("当前版本为: ")
					  .append(mVersionName)
					  .append("\n")
					  .append("新版版本为: ")
					  .append(mUpdataInfo.getVersion())
					  .append("\n")
					  .append("新版本的信息如下: \n")
					  .append(mUpdataInfo.getDescription())
					  .append("\n");
				
				if (!this.checkIsUseWIFI()) 
				{
					strbuf.append("\n温馨提醒：下载可能会耗费您较大的流量");
				}
				
				// 提示更新，可以取消
				if (flag == 1) 
				{
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(
							TITLE, 
							strbuf.toString(), 
							"确定", 
							Statics.MSG_UPDATE,
							"取消",
							Statics.MSG_UPDATE_CANCEL, 
							Statics.MSG_UPDATE_CANCEL, 
							mHandler);
				}
				// 强制更新，只有确认按钮
				else if (flag == 2) 
				{
					Statics.IS_FORCED_UPDATE = true;
					strbuf.append("温馨提醒：当前已进入强制升级阶段，系统需升级后才能使用。\n");
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(
							TITLE, 
							strbuf.toString(), 
							"确定", 
							Statics.MSG_UPDATE, 
							"取消",
							Statics.MSG_UPDATE_CANCEL, 
							Statics.MSG_UPDATE_CANCEL, 
							mHandler);
				}
				return true;
			}
		} 
		catch (Exception e) 
		{
			DialogUtil.closeProgress();
			e.printStackTrace();
		}
		return false;
	}
	
	private boolean checkIsUseWIFI() 
	{
		ConnectivityManager conMan = (ConnectivityManager)mMainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
		return conMan.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI;
	}
	
	// 检测服务器上是否有更新版本,本接口是检验APK升级
	// 0-不需要更新 	1-提示更新	2-强制更新
	public int isApkNeedUpdate() 
	{
		KeyValueDao keyValueDao = new KeyValueDao(BaseActivity.dataBaseService);
		String url = keyValueDao.get(KeyValueDao.KEY_UPDATE_URL);
		System.out.println("终端软件升级地址: " + url);
		
		if(StringUtil.isEmptyOrNull(url) == false) 
		{
			try 
			{			
				// 下载升级配置文件，解析是否该升级
				getUpdataXmlInfo(url);
				if(mUpdataInfo == null)
				{
					//sErrMsg = "没有获取到服务器上的升级数据！\n升级配置文件地址：" + url + "/update/update_version_union.xml";
					sErrMsg = "获取服务器升级数据失败！";
					return -1;
				}
				// 根据服务器版本信息中的update_psams字段，判断当前设备是否纳入升级范围，如果非*，则需先连接成功外设读取psam信息后才能升级
				boolean bUpdate = false;
				String spsams = mUpdataInfo.getPsams();
				if(spsams != null && ! "".equals(spsams)) 
				{
					if("*".equals(spsams))
					{
						bUpdate = true;
					}
					else
					{
						// 获得本地的PSAM卡号
						String localPsam = BaseActivity.equipmentService.getPsamId();
						String[] psam = spsams.split(",");
						for(int i = 0; i < psam.length; i++)
						{
							if(psam[i].trim().equals(localPsam))
							{
								bUpdate = true;
								break;
							}
						}
					}
				}
				
				/*
				// 停用的终端不进行升级
				if(!main_activity.isTerCanUse)
				{
					bUpdate = false; 
				}
				*/
				
				if (bUpdate && versionCompare(mVersionName.trim(), mUpdataInfo.getVersion().trim())) 
				{
					// 如果当前版本小于最低支持版本，则强制更新
					if(versionCompare(mVersionName.trim(), mUpdataInfo.getMin_version().trim())) 
					{
						return 2;
					}
					// 提示更新
					return 1;
				}
				return 0;
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				sErrMsg = "未知错误";
				return -1;
			}
		}
		sErrMsg = "连接软件升级服务器失败";
		return -1;
	}
	
	// 获得服务器上的版本信息
	private void getUpdataXmlInfo(String mainUrl) 
	{		
		try 
		{
			// 包装成URL的对象
			URL url = new URL(mainUrl + "/update/update_version_union.xml");
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			conn.setConnectTimeout(15*1000);
			conn.setReadTimeout(30*1000);
			// 判断是否连接上了
			InputStream in = conn.getInputStream();
			// 解析各个字段
			// 具体文件格式见doc下的update_version_union_7_2.xml
			if(in != null)
			{
				mUpdataInfo = analyXmlUpdataInfo(in, mainUrl);
			}
			conn.disconnect();
		} 
		catch (Exception e) 
		{
			mUpdataInfo = null;
			e.printStackTrace();
		}
	}
	
	// 用pull解析器解析服务器返回的xml文件 (xml封装了版本号) 
	// 这里是遍历XML，首先找到area_code，只要sBaseRemotePath最后的区域编码一致。
	// 注意area_code在XML要在节点的最前。此时返回的是XML最后area_code匹配的信息
	public static UpdataInfo analyXmlUpdataInfo(InputStream is, String mainUrl) throws Exception
	{  	
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int len = 0;			// 读取
		int total_len = 0;		// 总的长度
		
		byte[] buf = new byte[1024*5];
		while((len = is.read(buf)) > 0)
		{
			baos.write(buf,0,len);
			total_len += len;
		}
		buf = baos.toByteArray();
		baos.close();
		is.close();
		// 读取的长度非法
		if(total_len <= 0) 
		{
			return null;
		}
		
		String xml = new String(buf, "GBK");
		Log.v("xml", xml);
	    XmlPullParser parser = Xml.newPullParser();
	    // 设置解析的数据源
	    parser.setInput(new StringReader(xml));
	    
	    int type = parser.getEventType();  
	    UpdataInfo info = new UpdataInfo();
	    String area_code = null;
	    
	    while(type != XmlPullParser.END_DOCUMENT)
	    {  
	        switch (type) 
	        {  
	        case XmlPullParser.START_TAG:
	        	// 获取版本号  
	            if("version".equals(parser.getName()) && area_code != null)
	            {  
	                info.setVersion(parser.nextText());
	            }
	            // 获取升级APK文件的URL地址
	            else if ("url".equals(parser.getName()) && area_code!=null)
	            {  
	                info.setUrl(parser.nextText());
	                Log.v("xxxx", "当前apk升级地址为：" + info.getUrl());
	            }
	            // 获取该文件的信息
	            else if ("description".equals(parser.getName()) && area_code != null)
	            {  
	                info.setDescription(parser.nextText());
	            }
	            // 获取强制升级时间
	            else if ("update_time".equals(parser.getName()) && area_code != null)
	            {  
	                info.setUpdate_time(parser.nextText());
	            }
	            // 需升级的PSAM, 以逗号分隔，如果是*，则所有的都升级
	            else if ("update_psams".equals(parser.getName()) && area_code != null)
	            {  
	            	info.setPsams(parser.nextText());
	            }
	            // 必须升级的最低版本号，低于此版本号则强制升级
	            else if ("min_version".equals(parser.getName()) && area_code != null)
	            {  
	            	info.setMin_version(parser.nextText());
	            }
	            else if ("area_code".equals(parser.getName())) 
	            {
	            	String tmp = parser.nextText();
	            	if(tmp != null && mainUrl.endsWith(tmp.trim()))
	            	{
	            		Log.v("xxxx", "当前的升级的区域编码为：" + tmp);
	            		// 区域编码
		            	info.setArea_code(tmp);
		            	area_code = info.getArea_code();
	            	}
	            	else
	            	{
	            		area_code = null;
	            	}
	            }
	            break;
	        }
	        type = parser.next();
	    }  
	    return info;  
	}
	
	// 版本判断, true 升级
	// 1.0.0-01
	// 1.0.2-00
	private boolean versionCompare(String strOld, String strNew) 
	{
		String str1, str2;
		int pos, pos1;
		
		try
		{
			System.out.println("strOld: " + strOld + ", strNew: " + strNew);
			// 判断前两个字段
			for (int i = 0; i < 2; i++) 
			{
				pos = strOld.indexOf(".");
				if(pos == -1)
				{
					return false;
				}
				str1 = strOld.substring(0, pos);
				str2 = strNew.substring(0, pos);
				strOld = strOld.substring(pos+1);
				strNew = strNew.substring(pos+1);
				System.out.println("str1: " + str1 + ", str2: " + str2);
				System.out.println("strOld: " + strOld + ", strNew: " + strNew);
				int result = compareStr(str1, str2);
				if(result != 0)
				{
					return result<0? true:false;
				}
			}
			pos  = strOld.indexOf('-');
			pos1 = strNew.indexOf('-');
			// 旧版本不是临时版本
			if(pos == -1)
			{
				// 新版本没有'-'
				if(pos1 == -1)
				{
					return compareStr(strOld, strNew)<0? true:false;
				}
				// 新版本有'-'
				else
				{
					str2 = strNew.substring(0, pos1);
					System.out.println("str1: " + strOld + ", str2: " + str2);
					// 比较第三个字段
					int result = compareStr(strOld, str2);
					if(result != 0)
					{
						return result<0? true:false;
					}
					return true;
				}
			}
			// 旧版本是临时版本
			else
			{
				// 新版本没有'-'
				if(pos1 == -1)
				{
					str1 = strOld.substring(0, pos);
					// 比较第三个字段
					int result = compareStr(str1, strNew);
					if(result != 0)
					{
						return result<0? true:false;
					}
					return false;
				}
				// 新版本有'-'
				else
				{
					str1 = strOld.substring(0, pos);
					str2 = strNew.substring(0, pos);
					strOld = strOld.substring(pos+1);
					strNew = strNew.substring(pos+1);
					System.out.println("str1: " + str1 + ", str2: " + str2);
					System.out.println("strOld: " + strOld + ", strNew: " + strNew);
					// 第3个字段
					int result = compareStr(str1, str2);
					if(result != 0)
					{
						return result<0? true:false;
					}
					// 第4个字段
					result = compareStr(strOld, strNew);
					if(result != 0)
					{
						return result<0? true:false;
					}
					return false;
				}
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// 比较字符串
	private int compareStr(String str1, String str2) 
	{
		int temp1 = Integer.parseInt(str1);
		int temp2 = Integer.parseInt(str2);
		
		if(temp1 < temp2) 
		{
			return -1;
		}
		else if(temp1 > temp2)
		{
			return 1;
		}
		return 0;
	}

	private static final String KEY_ISDOWNLOADSUCCESS 	= "isDownloadSuccess";
	private static final String KEY_BREAKPOINT 			= "breakPoint";
	private static final String KEY_LASTVERSION 		= "lastVersion";
	
	// 检测断点下载
	public void checkBreakDownLoad(String localPath) 
	{
		SharedPreferences sp = mMainActivity.getSharedPreferences(Statics.CONFIG, Context.MODE_PRIVATE);
		// 是否已经下载成功
		boolean isDownloadSuccess = sp.getBoolean(KEY_ISDOWNLOADSUCCESS, true);
		// 断点的长度
		final long breakPoint = sp.getLong(KEY_BREAKPOINT, 0L);
		// 当前下载前的版本
		String version = sp.getString(KEY_LASTVERSION, "0.0.0");

		Log.v("download", "isDownloadSuccess:" + isDownloadSuccess+"");
		Log.v("download", "breakPoint:" + breakPoint+"");
		Log.v("download", "lastversion:" + version+"");
		Log.v("download", "currentversion:" + mUpdataInfo.getVersion()+"");
		
		// 版本相同
		if(version.equals(mUpdataInfo.getVersion()))
		{
			if(isDownloadSuccess)
			{
				Log.v(TAG, "上次下载已完成，重新下载");
				downLoadAPKWithBreakPoint(localPath, 0);
			}
			else
			{
				Log.v(TAG, "上次下载未完成，断点下载");
				downLoadAPKWithBreakPoint(localPath, breakPoint);
			}
		}
		else
		{
			Log.v(TAG, "版本不同重新下载");
			downLoadAPKWithBreakPoint(localPath, 0);
		}
	}
	
	@Override
	public void onDownloading(long currentSize, long allSize) 
	{
		updateConfig(false, mUpdataInfo.getVersion(), currentSize);
		mCurrentLength 	= currentSize;
		mTotalLength 	= allSize;
		mHandler.sendEmptyMessage(Statics.MSG_UPDATING);
	}

	@Override
	public void onDownloadSuccess(long currentSize, long allSize) 
	{
		updateConfig(true, mUpdataInfo.getVersion(), allSize);
		mCurrentLength 	= currentSize;
		mTotalLength 	= allSize;
		mHandler.sendEmptyMessage(Statics.MSG_UPDATE_OK);
	}

	@Override
	public void onDownloadFailed(long currentSize, long allSize, String errorStr) 
	{
		Log.e("download", "breakpoint:" + currentSize);
		updateConfig(false, mUpdataInfo.getVersion(), currentSize);
		mCurrentLength 	= currentSize;
		mTotalLength 	= allSize;
		mErrorStr		= errorStr;
		mHandler.sendEmptyMessage(Statics.MSG_UPDATE_FAILED);
	}
	
	// 更新配置
	private void updateConfig(boolean status, String version, long breakpoint) 
	{
		SharedPreferences preferences = mMainActivity.getSharedPreferences(Statics.CONFIG, Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putBoolean(KEY_ISDOWNLOADSUCCESS, status);
		editor.putString(KEY_LASTVERSION, version);
		editor.putLong(KEY_BREAKPOINT, breakpoint);
		editor.commit();
	}
	
	// 获取当前下载的断点
	public long getFileCurrentLength() 
	{
		return mCurrentLength;
	}
	
	// 获取当前下载的断点
	public long getFileTotalLength() 
	{
		return mTotalLength;
	}
	
	// 获取当前下载的断点
	public String getLastErrorStr() 
	{
		return mErrorStr;
	}
	
	// 断点下载
	private void downLoadAPKWithBreakPoint(final String localPath, final long breakPoint) 
	{
		HttpURLConnectionUtil.downLoadAPKWithBreakPoint(mUpdataInfo.getUrl(), localPath, breakPoint, this);
	}
	
	// 从服务器中下载APK(后台进行)
	public void downLoadApk_bk() 
	{
		/*
		final ProgressDialog pd; //进度条对话框
		pd = ProgressDialog.getInstance(main_activity);
		pd.setIndeterminate(false);
		pd.setCancelable(false);
		pd.setTitle("请稍后");
		pd.setMessage("正在下载更新(单位：KB)");
		pd.show();
		 
		new Thread() 
		{
			@Override
			public void run() 
			{
				try 
				{
					Log.e(TAG, mUpdataInfo.getUrl());
					FileDownUtil fileDownUtil=new FileDownUtil();
					File file = fileDownUtil.getFileFromServer(mUpdataInfo.getUrl(),Environment.getExternalStorageDirectory().toString(), "/mini/apk/CUAndroidMini.apk", pd);
					sleep(3000);
					installApk(file,main_activity);
					//结束掉进度条对话框
					pd.dismiss(); 
				}
				catch (Exception e) 
				{
					//结束掉进度条对话框
					pd.dismiss(); 
					UIHandle.MsgBoxOne(TITLE, "下载更新文件失败");
					Log.e(TAG, "下载更新文件失败");
					e.printStackTrace();
				} 
			}
		}.start();
		*/
	}
	
	// 安装APK
	public static void installApk(File file, Activity activity) 
	{
		 Intent intent = new Intent();
		 // 执行动作
		 intent.setAction(Intent.ACTION_VIEW);
		 // 执行的数据类型
		 intent.setDataAndType(Uri.fromFile(file),"application/vnd.android.package-archive");
		 activity.startActivity(intent);
	}
	
	// 安装APK
	public static void installApkRequestCode(File file, Activity activity, int requestCode) 
	{
		 Intent intent = new Intent();
		 // 执行动作
		 intent.setAction(Intent.ACTION_VIEW);
		 // 执行的数据类型
		 intent.setDataAndType(Uri.fromFile(file),"application/vnd.android.package-archive");
		 activity.startActivityForResult(intent, requestCode);
	}

	public UpdataInfo getUpdataInfo() 
	{
		return mUpdataInfo;
	}

	public void setUpdataInfo(UpdataInfo updataInfo) 
	{
		this.mUpdataInfo = updataInfo;
	}
}