package com.sunnada.baseframe.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.ui.RangeImageView;

public class ImageBrowseDialog extends Dialog
{
	private LinearLayout		mLayPhotos		= null;
	private RangeImageView[] 	mRangeViews		= null;

	public ImageBrowseDialog(Context context, String[] localPaths, String[] rangePaths)
	{
		super(context, R.style.CustomProgressDialog);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_dlg_image_view);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_dlg_image_view_orange);
		}
		mLayPhotos = (LinearLayout) findViewById(R.id.lay_photoes);
		mLayPhotos.removeAllViews();
		
		mRangeViews = new RangeImageView[localPaths.length];
		for (int i=0; i<localPaths.length; i++) 
		{
			if (i != 0) 
			{
				View view = new View(context);
				LayoutParams params = new LayoutParams(20, 10);
				view.setLayoutParams(params);
				mLayPhotos.addView(view);
			}
			String localPath = localPaths[i];
			String rangePath = null;
			if (rangePaths != null && rangePaths.length > i)
			{
				rangePath = rangePaths[i];
			}

			RangeImageView iv = new RangeImageView(context);
			iv.setBackgroundColor(Color.WHITE);
			iv.show(localPath, rangePath);
			iv.setScaleType(ScaleType.FIT_CENTER);
			LayoutParams params = new LayoutParams(560, 560);
			iv.setLayoutParams(params);
			mLayPhotos.addView(iv);
			mRangeViews[i] = iv;
		}
		
		Button btnDialogClose = (Button) findViewById(R.id.btn_dlg_close);
		btnDialogClose.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				try
				{
					dismiss();
					// 
					mLayPhotos.removeAllViews();
					if(null != mRangeViews && mRangeViews.length > 0) 
					{
						for(int  i=0; i<mRangeViews.length; i++) 
						{
							mRangeViews[i].clearBitmap();
						}
					}
					System.gc();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
}
