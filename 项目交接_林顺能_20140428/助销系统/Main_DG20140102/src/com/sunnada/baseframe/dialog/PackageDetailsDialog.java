package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.ProductInfo;
import com.sunnada.baseframe.bean.RecommendPackage;
import com.sunnada.baseframe.bean.Statics;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class PackageDetailsDialog extends Dialog implements android.view.View.OnClickListener
{
	private TextView 	mTvTitle;			// 对话框标题
	private TextView	mTvFlow1;			// 国内流量
	private TextView	mTvCallTime;		// 国内拨打分钟数
	private TextView	mTvMsgCount;		// 国内短信发送条数
	private TextView	mTvFree;			// 接听免费
	private TextView	mTvCallFee;			// 国内拨打费用
	private TextView	mTvFlow2;			// 国内流量费用
	private TextView	mTvVideoCall;		// 视频通话分钟数
	private TextView	mTvOther;			// 其他业务
	private ImageView 	mIvClose;			// 关闭按钮

	public PackageDetailsDialog(Context context)
	{
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_package_details);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_package_details_orange);
		}
		mTvTitle = (TextView) findViewById(R.id.tvTitle);
		mTvCallTime = (TextView) findViewById(R.id.tv_call_time);
		mTvFlow1 = (TextView) findViewById(R.id.tv_flow1);
		mTvMsgCount = (TextView) findViewById(R.id.tv_msg_count);
		mTvFree = (TextView) findViewById(R.id.tv_free);
		mTvCallFee = (TextView) findViewById(R.id.tv_call_fee);
		mTvFlow2 = (TextView) findViewById(R.id.tv_flow2);
		mTvVideoCall = (TextView) findViewById(R.id.tv_video_call);
		mTvOther = (TextView) findViewById(R.id.tv_other);
		mIvClose = (ImageView) findViewById(R.id.ivClose);
		mIvClose.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			// 关闭对话框
			case R.id.ivClose:
				this.dismiss();
				break;

			default:
				break;
		}
	}
	
	public void show(RecommendPackage rp)
	{
		ProductInfo product = rp.getmProduct();
		mTvTitle.setText(product.getmProductName());
		mTvFlow1.setText(product.getmFlow1());
		mTvCallTime.setText(product.getmCallTime());
		mTvMsgCount.setText(product.getmMsgCount());
		mTvFree.setText(product.getmFree());
		
		mTvCallFee.setText(product.getmCallFee());
		mTvFlow2.setText(product.getmFlow2());
		mTvVideoCall.setText(product.getmVideoCall());
		mTvOther.setText(product.getmOther());
		super.show();
	}

}
