package com.sunnada.baseframe.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;

public class CommonDialog extends Dialog implements OnClickListener
{
	private Context					m_context			= null;
	private ImageView				dlg_bt_titlemap;
	private Button					dlg_bt_close;
	private Button					dlg_btcance;
	private Button					dlg_btrepet;
	private Button					dlg_btok;
	private TextView				dlg_tv_title;
	private TextView				dlg_tv_content;
	private LinearLayout			dlg_lay_content;
	private View.OnClickListener	positiveListener	= null;	// 确定按钮监听
	private View.OnClickListener	neutralListener		= null;	// neutral按钮监听
	private View.OnClickListener	negetiveListener	= null;	// 取消按钮监听
	private View.OnClickListener	dismissListenner    = null; // dismiss监听
	public boolean					isMiss				= true;
	
	public CommonDialog(Context context)
	{
		super(context, R.style.transparent_dialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_common);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_common_orange);
		}
		setCanceledOnTouchOutside(false);
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	public void initViews()
	{
		dlg_bt_titlemap 	= (ImageView) findViewById(R.id.dlg_bt_titlemap);
		dlg_bt_close 		= (Button) findViewById(R.id.dlg_bt_close);
		dlg_btcance 		= (Button) findViewById(R.id.dlg_btcance);
		dlg_btrepet 		= (Button) findViewById(R.id.dlg_btrepet);
		dlg_btok 			= (Button) findViewById(R.id.dlg_btok);
		dlg_tv_title 		= (TextView) findViewById(R.id.dlg_tv_title);
		dlg_tv_content 		= (TextView) findViewById(R.id.dlg_tv_content);
		dlg_lay_content 	= (LinearLayout) findViewById(R.id.dlg_lay_content);
		
		//dlg_bt_titlemap.setOnClickListener(this);
		dlg_bt_close.setOnClickListener(this);
		dlg_btcance.setOnClickListener(this);
		dlg_btrepet.setOnClickListener(this);
		dlg_btok.setOnClickListener(this);
	}
	
	public void setTitle(CharSequence title, Bitmap bitmap)
	{
		dlg_tv_title.setText(title);
		if (bitmap != null)
		{
			BitmapDrawable bd = new BitmapDrawable(m_context.getResources(), bitmap);
			dlg_bt_titlemap.setBackgroundDrawable(bd);
		}
	}
	
	public void setTitle(CharSequence title)
	{
		dlg_tv_title.setText(title);
	}

	public void setMessage(CharSequence content)
	{
		dlg_tv_content.setVisibility(View.VISIBLE);
		dlg_tv_content.setText(content);
	}

	public void setMessage(View content)
	{
		dlg_lay_content.setVisibility(View.VISIBLE);
		dlg_lay_content.addView(content);
	}

	public void setPositiveListener(String str, View.OnClickListener positiveListener)
	{
		dlg_btok.setText(str);
		dlg_btok.setVisibility(View.VISIBLE);
		this.positiveListener = positiveListener;
	}
	
	public void setNegetiveListener(String str, View.OnClickListener negetiveListener)
	{
		dlg_btcance.setText(str);
		dlg_btcance.setVisibility(View.VISIBLE);
		this.negetiveListener = negetiveListener;
	}
	
	public void setNeutralListener(String str, View.OnClickListener neutralListener)
	{
		dlg_btrepet.setText(str);
		dlg_btrepet.setVisibility(View.VISIBLE);
		this.neutralListener = neutralListener;
	}
	
	public void setDismissListenner(View.OnClickListener dismissListenner)
	{
		this.dismissListenner = dismissListenner;
	}
	
	@Override
	public void onClick(View v)
	{
		if (isMiss == true)
		{
			this.dismiss();
		}
		if (v == dlg_btok && positiveListener != null) 
		{
			positiveListener.onClick(v);
		}
		else if (v == dlg_btcance && negetiveListener != null)
		{
			negetiveListener.onClick(v);
		}
		else if (v == dlg_btrepet && neutralListener != null) 
		{
			neutralListener.onClick(v);
		}
		else if (v == dlg_bt_close) 
		{ 
			if(dismissListenner != null)
			{
				dismissListenner.onClick(v);
			}
			this.dismiss();
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			return false;
		}
		return false;
	}
}
