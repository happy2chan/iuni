package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.util.InputRules;
import com.sunnada.baseframe.util.StringUtil;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class ModifyPswdDialog extends Dialog implements View.OnClickListener
{
	private Business_00				mBusiness;				// 协议交互类
	
	private ImageView				mImageClose;			// 关闭密码修改对话框
	private Button					mModifyPswd;			// 修改密码
	private EditText				mEditNewUserPswd;		// 新工号密码输入框
	private EditText				mEditEnsureUserPswd;	// 新工号密码确认框
	private EditText				mEditNewSrvPswd;		// 新交易密码输入框
	private EditText				mEditEnsureSrvPswd;		// 新交易密码确认框
	private Handler					mCallbackHandler;
	
	public ModifyPswdDialog(Context context, Business_00 business) 
	{
		super(context, R.style.transparent_dialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_modify_pswd);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_modify_pswd_orange);
		}
		this.mBusiness		= business;
		// 注册监听事件
		initViews();
	}
	
	public ModifyPswdDialog(Context context, Business_00 business, Handler handler) 
	{
		super(context, R.style.transparent_dialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dlg_modify_pswd);
		this.mBusiness		= business;
		mCallbackHandler 	= handler;
		// 注册监听事件
		initViews();
	}
	
	// 初始化控件
	private void initViews() 
	{
		// 关闭密码修改对话框
		mImageClose = (ImageView)findViewById(R.id.pswd_close);
		mImageClose.setOnClickListener(this);
		// 修改密码
		mModifyPswd = (Button)findViewById(R.id.btn_modifyPswd);
		mModifyPswd.setOnClickListener(this);
		
		// 工号密码输入框
		mEditNewUserPswd 	= (EditText)findViewById(R.id.et_new_user_pswd);
		mEditEnsureUserPswd = (EditText)findViewById(R.id.et_ensure_user_pswd);
		// 交易密码输入框
		mEditNewSrvPswd 	= (EditText)findViewById(R.id.et_new_srv_pswd);
		mEditEnsureSrvPswd 	= (EditText)findViewById(R.id.et_ensure_srv_pswd);
	}
	
	// 清除编辑框
	private void clearEdit() 
	{
		mEditNewUserPswd.setText("");
		mEditEnsureUserPswd.setText("");
		mEditNewSrvPswd.setText("");
		mEditEnsureSrvPswd.setText("");
	}
	
	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
		// 关闭密码修改对话框
		case R.id.pswd_close:
			dismiss();
			if(mCallbackHandler != null)
			{
				mCallbackHandler.sendEmptyMessage(Statics.MSG_MODIFY_PSWD_CANCEL);
			}
			break;
			
		// 修改密码
		case R.id.btn_modifyPswd:
			modifyPswd();
			break;
			
		default:
			break;
		}
	}
	
	// 消息处理器
	private Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch(message.what)
			{
			case 0x01:
				// 清除编辑框
				clearEdit();
				// 销毁
				dismiss();
				if(mCallbackHandler != null)
				{
					mCallbackHandler.sendEmptyMessage(Statics.MSG_MODIFY_PSWD_SUCCESS);
				}
				break;
				
			case 0x02:
				// 清除编辑框
				clearEdit();
				break;
				
			case Statics.MSG_MODIFY_PSWD_SUCCESS:
				mHandler.sendEmptyMessage(0x01);
				break;
				
			default:
				break;
			}
		}
	};
	
	// 修改密码
	private boolean modifyPswd() 
	{
		// 检测新工号密码
		final String strNewUserPswd = mEditNewUserPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(strNewUserPswd) || strNewUserPswd.length() != 6) 
		{
			mEditNewUserPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "请输入6位新工号密码！");
			return false;
		}
		// 检测确认工号密码
		String strEnsureUserPswd = mEditEnsureUserPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(strEnsureUserPswd) || strEnsureUserPswd.length() != 6) 
		{
			mEditEnsureUserPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "请输入6位确认工号密码！");
			return false;
		}
		// 检测工号密码是否一致
		if(strNewUserPswd.equals(strEnsureUserPswd) == false) 
		{
			mEditNewUserPswd.setText("");
			mEditEnsureUserPswd.setText("");
			mEditNewUserPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "两次输入的新工号密码不一致！");
			return false;
		}
		// 检测工号密码是否过于简单
		if(InputRules.isSimple(strNewUserPswd)) 
		{
			mEditNewUserPswd.setText("");
			mEditEnsureUserPswd.setText("");
			mEditNewUserPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "新工号密码过于简单, 请重新设置！");
			return false;
		}
		// 检测新交易密码
		final String strNewSrvPswd = mEditNewSrvPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(strNewSrvPswd) || strNewSrvPswd.length() != 6) 
		{
			mEditNewSrvPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "请输入6位新交易密码！");
			return false;
		}
		// 检测确认交易密码
		String strEnsureSrvPswd = mEditEnsureSrvPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(strEnsureSrvPswd) || strEnsureSrvPswd.length() != 6)
		{
			mEditEnsureSrvPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "请输入6位确认交易密码！");
			return false;
		}
		// 检测交易密码是否一致
		if(strNewSrvPswd.equals(strEnsureSrvPswd) == false) 
		{
			mEditNewSrvPswd.setText("");
			mEditEnsureSrvPswd.setText("");
			mEditNewSrvPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "两次输入的新交易密码不一致！");
			return false;
		}
		// 检测交易密码是否过于简单
		if(InputRules.isSimple(strNewSrvPswd)) 
		{
			mEditNewSrvPswd.setText("");
			mEditEnsureSrvPswd.setText("");
			mEditNewSrvPswd.requestFocus();
			DialogUtil.MsgBox(Business_00.TITLE_STR, "新交易密码过于简单, 请重新设置！");
			return false;
		}
		
		Log.d("xxxx", "新登陆密码为: " + strNewUserPswd);
		Log.d("xxxx", "新交易密码为: " + strNewSrvPswd);
		
		new Thread() 
		{
			public void run() 
			{
				// 获取流水号
				DialogUtil.showProgress("正在获取流水号...");
				if(mBusiness.bll000A(0x01, 0x01) == false) 
				{
					DialogUtil.closeProgress();
					mHandler.sendEmptyMessage(0x02);
					return;
				}
				// 设定初始密码
				DialogUtil.showProgress("正在设定初始密码...");
				if(mBusiness.bll0008(null, strNewUserPswd, strNewSrvPswd) == false) 
				{
					DialogUtil.closeProgress();
					mHandler.sendEmptyMessage(0x02);
					return;
				}
				else
				{
					Statics.HAD_PWD = true;
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(Business_00.TITLE_STR, "初始密码修改成功！", Statics.MSG_MODIFY_PSWD_SUCCESS, mHandler);
				}
			}
		}.start();
		return true;
	}
}
