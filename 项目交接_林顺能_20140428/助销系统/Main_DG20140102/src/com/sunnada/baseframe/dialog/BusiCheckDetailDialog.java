package com.sunnada.baseframe.dialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.ui.MyCustomButton2;
import com.sunnada.baseframe.util.ARGBUtil;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class BusiCheckDetailDialog extends Dialog implements OnClickListener, OnTouchListener
{
	private TextView					mTextTitle;					// 消息框标题
	private Button						mBtnDialogClose;			// 关闭消息框
	private ListView					mListCheckDetail;			// 详情列表框
	private MyCustomButton				mBtnPrev;					// 上一页
	private MyCustomButton				mBtnNext;					// 下一页
	
	private Context						mContext;
	private String						mStrTitle;					// 标题
	private List<Map<String, String>> 	mListData;					// 对账详情数据
	private Handler						mHandler;					// 消息处理
	
	
	// 对账明细标题TextView
	private TextView                    mTvTitle1;
	private TextView					mTvTitle2;
	private TextView					mTvTitle3;
	private TextView[]                  mTvTitles             = {mTvTitle1,mTvTitle2,mTvTitle3};
	private int[]                       mTvTitleIds           = {R.id.tv_title1,R.id.tv_title2,R.id.tv_title3};
	
	public BusiCheckDetailDialog(Context context, String strTitle,List<Map<String, String>> list, Handler handler) 
	{
		super(context, R.style.transparent_dialog);
		setContentView(R.layout.dlg_busicheck_detail);
		setCanceledOnTouchOutside(false);
		
		mContext = context;
		// 标题
		mStrTitle = strTitle;
		// 对账详情数据
		mListData = list;
		// 
		mHandler = handler;
		
		// 初始化控件
		initViews();
		// 初始化显示
		initShow();
		// 刷新页面
		refreshPage();
	}
	
	// 初始化控件
	private void initViews() 
	{
		// 消息框标题
		mTextTitle = (TextView) findViewById(R.id.tv_check_title);
		// 消息框关闭
		mBtnDialogClose = (Button) findViewById(R.id.btn_dialog_close);
		mBtnDialogClose.setOnClickListener(this);
		// 列表
		mListCheckDetail = (ListView) findViewById(R.id.list_check);
		// 上一页
		mBtnPrev = (MyCustomButton) findViewById(R.id.btn_prev);
		// mBtnPrev.setImageResource(R.drawable.btn_reselect);
		mBtnPrev.setTextViewText1("上一页");
		mBtnPrev.setOnClickListener(this);
		mBtnPrev.setOnTouchListener(this);
		// 下一页
		mBtnNext = (MyCustomButton) findViewById(R.id.btn_next);
		// mBtnNext.setImageResource(R.drawable.btn_custom_check);
		mBtnNext.setTextViewText1("下一页");
		mBtnNext.setOnClickListener(this);
		mBtnNext.setOnTouchListener(this);
		
		// 对账详情标题TextView
		for(int i = 0;i < mTvTitles.length;i++)
		{
			mTvTitles[i] = (TextView)this.findViewById(mTvTitleIds[i]);
			mTvTitles[i].setVisibility(View.VISIBLE);
		}
	}
	
	// 初始化显示
	public void initShow()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mTextTitle.setTextColor(ARGBUtil.getArgb(2));
			mBtnDialogClose.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_dlg_close_selector_orange));
		}
	}
	
	// 刷新页面
	private void refreshPage() 
	{
		// 设置标题
		mTextTitle.setText(mStrTitle);
		
		// 设置对账详情标题TextView数据
		String[] tempArr = {Statics.CHECK_TITLE1,Statics.CHECK_TITLE2,Statics.CHECK_TITLE3};
		
		if(mListData.get(0).size() == 2)
		{
			mTvTitles[1].setVisibility(View.INVISIBLE);
			mTvTitles[0].setText(mListData.get(0).get(tempArr[0]));
			mTvTitles[2].setText(mListData.get(0).get(tempArr[1]));
		}
		else
		{
			for(int i=0;i<mListData.get(0).size();i++)
			{
				mTvTitles[i].setVisibility(View.VISIBLE);
				mTvTitles[i].setText(mListData.get(0).get(tempArr[i]));
			}
		}
		
		if(mListData.size() > 1)
		{
			List<Map<String,String>> tempList = new ArrayList<Map<String,String>>();
			for(int i = 1;i < mListData.size();i++)
			{
				tempList.add(mListData.get(i));
			}
			
			if(tempList.get(0).size() == 3)
			{
				BaseAdapter adapter = new MyAdapter(
						mContext, 
						R.layout.dlg_busicheck_detail_listitem, 
						tempList);
				mListCheckDetail.setAdapter(adapter);
				//DimensionUtil.setListViewHeightBasedOnChildren(mListCheckDetail);
				// 刷新列表
				/*
				SimpleAdapter adapter = new SimpleAdapter(
						mContext, 
						tempList, 
						R.layout.dlg_busicheck_detail_listitem, 
						new String[] {Statics.CHECK_DATA1,Statics.CHECK_DATA2,Statics.CHECK_DATA3}, 
						new int[] {R.id.tv_data1,R.id.tv_data2,R.id.tv_data3});
				mListCheckDetail.setAdapter(adapter);
				DimensionUtil.setListViewHeightBasedOnChildren(mListCheckDetail);
				*/
			}
			else if(tempList.get(0).size() == 2)
			{
				BaseAdapter adapter = new MyAdapter(
						mContext, 
						R.layout.dlg_busicheck_detail_listitem2, 
						tempList);
				mListCheckDetail.setAdapter(adapter);
				//DimensionUtil.setListViewHeightBasedOnChildren(mListCheckDetail);
				// 刷新列表
				/*
				SimpleAdapter adapter = new SimpleAdapter(
						mContext, 
						tempList, 
						R.layout.dlg_busicheck_detail_listitem2, 
						new String[] {Statics.CHECK_DATA1,Statics.CHECK_DATA2}, 
						new int[] {R.id.tv_data1,R.id.tv_data3});
				mListCheckDetail.setAdapter(adapter);
				DimensionUtil.setListViewHeightBasedOnChildren(mListCheckDetail);
				*/
			}
		}
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 关闭消息框
		case R.id.btn_dialog_close:
			dismiss();
			break;
			
		// 上一页
		case R.id.btn_prev:
			mHandler.sendEmptyMessage(Statics.MSG_BUSI_CHECK_PREV);
			dismiss();
			break;
			
		// 下一页
		case R.id.btn_next:
			mHandler.sendEmptyMessage(Statics.MSG_BUSI_CHECK_NEXT);
			dismiss();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		// 监听子antivity中按钮的点击样式 20131018
		if(v instanceof MyCustomButton)
		{
			((MyCustomButton)v).onTouch(event);
		}
		else
		{
			((MyCustomButton2)v).onTouch(event);
		}
		return false;
	}
	
	
	public class MyAdapter extends BaseAdapter
	{
		LayoutInflater mInflater;
		int mResource;
		
		List<Map<String,String>> mListData = new ArrayList<Map<String,String>>();
		
		public MyAdapter(Context context, int resource, List<Map<String,String>> listData) 
		{
			mInflater = LayoutInflater.from(context);
			this.mResource = resource;
			this.mListData = listData;
		}

		@Override
		public int getCount() 
		{
			return mListData.size();
		}

		@Override
		public Map<String, String> getItem(int position) 
		{
			return mListData.get(position);
		}

		@Override
		public long getItemId(int position) 
		{
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) 
		{
			View view;
			if(convertView == null)
			{
				view = mInflater.inflate(mResource, parent, false);
			}
			else
			{
				view = convertView;
			}
			
			if(mListData.get(0).size() == 3)
			{
				TextView tv_data1 = (TextView)view.findViewById(R.id.tv_data1);
				TextView tv_data2 = (TextView)view.findViewById(R.id.tv_data2);
				TextView tv_data3 = (TextView)view.findViewById(R.id.tv_data3);
				
				tv_data1.setText(mListData.get(position).get(Statics.CHECK_DATA1));
				tv_data2.setText(mListData.get(position).get(Statics.CHECK_DATA2));
				tv_data3.setText(mListData.get(position).get(Statics.CHECK_DATA3));
			}
			
			else if(mListData.get(0).size() == 2)
			{
				TextView tv_data1 = (TextView)view.findViewById(R.id.tv_data1);
				TextView tv_data2 = (TextView)view.findViewById(R.id.tv_data3);
				
				tv_data1.setText(mListData.get(position).get(Statics.CHECK_DATA1));
				tv_data2.setText(mListData.get(position).get(Statics.CHECK_DATA2));
			}
			return view;
		}
	}
}
