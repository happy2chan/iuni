package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.View;
import android.widget.ImageView;

public class ModifyPswdInfoDialog extends Dialog implements View.OnClickListener
{
	private ImageView 		mImageModifyPswd;			// 修改密码
	private ImageView		mImageModifyPswdClose;		// 关闭提示框
	private Handler			mHandler;					// 调用者的消息处理器
	
	public ModifyPswdInfoDialog(Context context, Handler handler) 
	{
		super(context, R.style.transparent_dialog2);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_modify_pswd_info);
		} 
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_modify_pswd_info_orange);
		}
		mHandler = handler;
		// 注册监听事件
		registerListener();
		//Statics.HAD_PWD = false;
		//Statics.MODIFY_TYPE = 1;
		/*
		// 设置内部控件的透明度
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.alpha = 0.5f;
		getWindow().setAttributes(lp);
		*/
	}
	
	// 注册监听事件
	private void registerListener() 
	{
		// 修改密码
		mImageModifyPswd = (ImageView) findViewById(R.id.iv_modify_pswd);
		mImageModifyPswdClose = (ImageView) findViewById(R.id.iv_modify_pswd_close);
		
		mImageModifyPswd.setOnClickListener(this);
		mImageModifyPswdClose.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 修改密码
		case R.id.iv_modify_pswd:
			dismiss();
			mHandler.sendEmptyMessage(Statics.MSG_MODIFY_PSWD);
			Log.d("xxxx", "发送密码修改的消息");
			break;
			
		// 关闭对话框
		case R.id.iv_modify_pswd_close:
			dismiss();
			mHandler.sendEmptyMessage(Statics.MSG_MODIFY_PSWD_CANCEL);
			break;
			
		default:
			break;
		}
	}
}
