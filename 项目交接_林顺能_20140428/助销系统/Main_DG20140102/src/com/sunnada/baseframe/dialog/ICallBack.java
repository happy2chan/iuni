package com.sunnada.baseframe.dialog;

public interface ICallBack 
{
	public void back();
	public void cancel();
	public void dismiss();
}
