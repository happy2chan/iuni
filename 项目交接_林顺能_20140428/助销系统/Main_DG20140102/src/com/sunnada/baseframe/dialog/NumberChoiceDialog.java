package com.sunnada.baseframe.dialog;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.business.PickNetBusiness;
import com.sunnada.baseframe.bean.Rule;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ARGBUtil;

public class NumberChoiceDialog extends Dialog implements android.view.View.OnClickListener,OnTouchListener
{
	private String						strMobTypeArr[]	=
												{ "不限", "AAAA", "AABB", "ABAB", "ABCD", "ABBA", "BBAA", "ABBB", "AAAB" };
	private String						strSystemArr[]	=
												{ "不限", "130", "131", "132", "155", "156", "185", "186" };

	private LinearLayout				lay_mobchotype;
	private LinearLayout				lay_system;
	private Context						m_context		= null;
	// private Button						bt_close		= null;
	private Button						mBtnPayPre;  // 预付费
	private Button                      mBtnPayAfter;// 后付费

	//private TextView					pricetv1, pricetv2, pricetv3, pricetv4, pricetv5, pricetv6, pricetv7, pricetv8;
	//private View						pricevi1, pricevi2, pricevi3, pricevi4, pricevi5, pricevi6, pricevi7;
	private LinearLayout		        mLayPrice1, mLayPrice2, mLayPrice3, mLayPrice4, mLayPrice5, mLayPrice6, mLayPrice7;
	//private LinearLayout				mLayPrice8;
	
	private List<Button>				btmobcholist	= new ArrayList<Button>();
	private List<Button>				btsystemlist	= new ArrayList<Button>();
	private MyCustomButton			    mBtnReSelect;
	private MyCustomButton			    mBtnSelect;

	private View						oldPriceView;
	private TextView					oldPriceTv;
	
	private Button						mBtnOldNumRules;// 靓号规则															// 靓号规则
	private Button						mBtnOldNum;	    // 号段
	//private Button					oldbt_payway;   // 付费方式

	private int[]						arrSelcet		= { -1, -1, -1, -1 };//依次为付费方式,号段,价格,靓号规则
	private String[]					arrStr			= { "", "" };//依次为号段文本,靓号规则文本

	private onChoiceGetListener	        m_listener		= null;

	public NumberChoiceDialog(Activity context, onChoiceGetListener listener)
	{
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dlg_numchoice);
		m_context = context;
		this.m_listener = listener;
		dlgInit();

	}

	public void dlgInit()
	{
		// bt_close = (Button) findViewById(R.id.bt_close);
		// bt_close.setOnClickListener(this);
		mBtnReSelect = (MyCustomButton) findViewById(R.id.btn_reelect);
		mBtnSelect = (MyCustomButton) findViewById(R.id.btn_select);
		mBtnReSelect.setTextViewText1("重选");
		mBtnReSelect.setTextViewText2("");
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnReSelect.setImageResource(R.drawable.btn_reselect_orange);
		}
		else
		{
			mBtnReSelect.setImageResource(R.drawable.btn_reselect);
		}
		mBtnSelect.setTextViewText1("查询");
		mBtnSelect.setTextViewText2("");
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnSelect.setImageResource(R.drawable.btn_custom_query_orange);
		}
		else
		{
			mBtnSelect.setImageResource(R.drawable.btn_custom_query);
		}
		
		mBtnReSelect.setOnClickListener(this);
		mBtnSelect.setOnClickListener(this);
		mBtnSelect.setOnTouchListener(this);
		mBtnReSelect.setOnTouchListener(this);
		
		payWayInit();

		lay_system = (LinearLayout) findViewById(R.id.lay_system);
		numInit();

		priceInit();

		lay_mobchotype = (LinearLayout) findViewById(R.id.lay_mobchotype);
		numRulesInit();
	}

	public void payWayInit()
	{
		mBtnPayPre = (Button) findViewById(R.id.bt_pay_pre);
		mBtnPayPre.setOnClickListener(this);
		mBtnPayAfter = (Button) findViewById(R.id.bt_pay_after);
		mBtnPayAfter.setOnClickListener(this);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnPayPre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
		}
		
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnPayAfter.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			mBtnPayAfter.setTextColor(ARGBUtil.getArgb(2));
		}
		//oldbt_payway = mBtnPayPre;
		arrSelcet[0] = 1;
	}

	// 初始化价格
	public void priceInit()
	{
		mLayPrice1 = (LinearLayout) findViewById(R.id.lay_price1);
		mLayPrice2 = (LinearLayout) findViewById(R.id.lay_price2);
		mLayPrice3 = (LinearLayout) findViewById(R.id.lay_price3);
		mLayPrice4 = (LinearLayout) findViewById(R.id.lay_price4);
		mLayPrice5 = (LinearLayout) findViewById(R.id.lay_price5);
		mLayPrice6 = (LinearLayout) findViewById(R.id.lay_price6);
		mLayPrice7 = (LinearLayout) findViewById(R.id.lay_price7);
		//mLayPrice8 = (LinearLayout) findViewById(R.id.lay_price8);
		
		mLayPrice1.setOnClickListener(this);
		mLayPrice2.setOnClickListener(this);
		mLayPrice3.setOnClickListener(this);
		mLayPrice4.setOnClickListener(this);
		mLayPrice5.setOnClickListener(this);
		mLayPrice6.setOnClickListener(this);
		mLayPrice7.setOnClickListener(this);
		//mLayPrice8.setOnClickListener(this);
		// 初始化预存话费显示
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			initPriceViewShow(mLayPrice1, 0);
			initPriceViewShow(mLayPrice2, 1);
			initPriceViewShow(mLayPrice3, 2);
			initPriceViewShow(mLayPrice4, 3);
			initPriceViewShow(mLayPrice5, 4);
			initPriceViewShow(mLayPrice6, 5);
			initPriceViewShow(mLayPrice7, 6);
		}
		// 初始化预存话费
		selectPriceView(mLayPrice1, 0);
	}

	// 靓号规则 
	public void numRulesInit()
	{
		for (int i = 0; i < PickNetBusiness.numberRules.size(); i++)
		{
			Rule rule = PickNetBusiness.numberRules.get(i);
			Button button = new Button(m_context);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			lp.leftMargin = 10;
			button.setLayoutParams(lp);
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			}
			else
			{
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
			button.setText(rule.getContent());
			button.setTextSize(22);
			button.setId(rule.getId());
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				button.setTextColor(ARGBUtil.getArgb(2));
			}
			else
			{
				button.setTextColor(Color.parseColor("#FFFFFF"));
			}
			lay_mobchotype.addView(button);
			btmobcholist.add(button);
			button.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View arg0)
				{
					mBtnOldNumRules = (Button) arg0;
					arrSelcet[3] = mBtnOldNumRules.getId();
					selectButton(btmobcholist, arg0);
				}
			});
		}
		
		// 初始化靓号规则
		if(btmobcholist != null && (btmobcholist.size() > 0))
		{
			Button rulesButton = btmobcholist.get(0);
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				rulesButton.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
				rulesButton.setTextColor(ARGBUtil.getArgb(10));
			}
			else
			{
				rulesButton.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			}
		}
	}

	// 初始号段
	public void numInit()
	{
		String[] arr = getStrSystemArr();
		for (int i = 0; i < arr.length; i++)
		{
			Button button = new Button(m_context);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			lp.leftMargin = 10;
			button.setLayoutParams(lp);
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			}
			else
			{
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
			button.setText(arr[i].trim());
			button.setTextSize(22);
			button.setId(i);
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				button.setTextColor(ARGBUtil.getArgb(2));
			}
			else
			{
				button.setTextColor(Color.parseColor("#FFFFFF"));
			}
			lay_system.addView(button);
			btsystemlist.add(button);
			button.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View arg0)
				{
					mBtnOldNum = (Button) arg0;
					arrSelcet[1] = mBtnOldNum.getId();
					selectButton(btsystemlist, arg0);
				}
			});
		}
		
		// 初始化号段
		if(btsystemlist != null && (btsystemlist.size() > 0))
		{
			Button button = btsystemlist.get(0);
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
				button.setTextColor(ARGBUtil.getArgb(10));
			}
			else
			{
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			}
		}
	}

	// 按钮选中后改变状态
	public void selectButton(List<Button> crrlist, View arg0)
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			arg0.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
			((Button) arg0).setTextColor(ARGBUtil.getArgb(10));
		}
		else
		{
			arg0.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
		}

		for (int i = 0; i < crrlist.size(); i++)
		{
			Button button = crrlist.get(i);
			if (button.getId() != arg0.getId())
			{
				if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
					button.setTextColor(ARGBUtil.getArgb(2));
				}
				else
				{
					button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
				}
			}
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.bt_close:
				// this.dismiss();
				break;

			// 选择价格1
			case R.id.lay_price1:
				selectPriceView(v, 0);
				break;
								
			// 选择价格2
			case R.id.lay_price2:
				selectPriceView(v, 1);
				break;
								
			// 选择价格3
			case R.id.lay_price3:
				selectPriceView(v, 2);
				break;
								
			// 选择价格4
			case R.id.lay_price4:
				selectPriceView(v, 3);
				break;
								
			// 选择价格5
			case R.id.lay_price5:
				selectPriceView(v, 4);
				break;
								
			// 选择价格6
			case R.id.lay_price6:
				selectPriceView(v, 5);
				break;
								
			// 选择价格7
			case R.id.lay_price7:
				selectPriceView(v, 6);
				break;
								
			// 选择价格8 最高
		    /*
			case R.id.lay_price8:
				selectPriceView(v, 7);
				break;
			*/
				
			case R.id.btn_reelect:
				cleanSelect();
				break;
				
			case R.id.btn_select:
				if (mBtnOldNum != null)
				{
					arrStr[0] = mBtnOldNum.getText().toString();
				}
				if (mBtnOldNumRules != null)
				{
					arrStr[1] = mBtnOldNumRules.getText().toString();
				}
				if (m_listener != null)
				{
					m_listener.onChoiceGot(arrSelcet, arrStr);
				}
				this.cancel();
				break;

			case R.id.bt_pay_pre:
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					mBtnPayAfter.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
					mBtnPayPre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
				}
				else
				{
					mBtnPayAfter.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
					mBtnPayPre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
					mBtnPayAfter.setTextColor(ARGBUtil.getArgb(2));
					mBtnPayPre.setTextColor(ARGBUtil.getArgb(10));
				}
				//oldbt_payway = mBtnPayPre;
				arrSelcet[0] = 1;
				break;
				
			case R.id.bt_pay_after:
				if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					mBtnPayPre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
					mBtnPayAfter.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
					
					mBtnPayAfter.setTextColor(ARGBUtil.getArgb(10));
					mBtnPayPre.setTextColor(ARGBUtil.getArgb(2));
				}
				else
				{
					mBtnPayPre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
					mBtnPayAfter.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
				}
				//oldbt_payway = mBtnPayAfter;
				arrSelcet[0] = 2;
				break;
			default:
				break;
		}
	}

	// 初始化价格显示
	public void initPriceViewShow(View view, int index)
	{
		LinearLayout lay = (LinearLayout) view;
		TextView tvPrice = (TextView) lay.getChildAt(0);
		View viewPrice = lay.getChildAt(1);
		
		viewPrice.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
		tvPrice.setTextColor(ARGBUtil.getArgb(7));
	}
	
	// 选择价格
	public void selectPriceView(View view, int index)
	{
		LinearLayout lay = (LinearLayout) view;
		TextView tvPrice = (TextView) lay.getChildAt(0);
		View viewPrice = lay.getChildAt(1);
		
		if (oldPriceView != null)
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				oldPriceView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			}
			else
			{
				oldPriceView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
		}
		if (oldPriceTv != null)
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				oldPriceTv.setTextColor(ARGBUtil.getArgb(7));
			}
			else
			{
				oldPriceTv.setTextColor(Color.parseColor("#00A6EC"));
			}
		}
		
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			tvPrice.setTextColor(ARGBUtil.getArgb(2));
			viewPrice.setBackgroundColor(ARGBUtil.getArgb(2));
		}
		else
		{
			tvPrice.setTextColor(Color.parseColor("#FF0000"));
			viewPrice.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumlvse));
		}
		oldPriceView = viewPrice;
		oldPriceTv = tvPrice;
		arrSelcet[2] = index;
	}
	
	public void cleanSelect()
	{
		// 选中价格
		if (oldPriceView != null)
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				oldPriceView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			}
			else
			{
				oldPriceView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
		}
		
		if (oldPriceTv != null)
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				oldPriceTv.setTextColor(ARGBUtil.getArgb(7));
			}
			else
			{
				oldPriceTv.setTextColor(Color.parseColor("#00A6EC"));
			}
		}

		// 初始化预存话费
		selectPriceView(mLayPrice1, 0);
		
		// 选中号段
		if (mBtnOldNum != null)
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mBtnOldNum.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
				mBtnOldNum.setTextColor(ARGBUtil.getArgb(2));
			}
			else
			{
				mBtnOldNum.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
		}
		
		// 初始化号段
		if(btsystemlist != null && (btsystemlist.size() > 0))
		{
			Button button = btsystemlist.get(0);
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
				button.setTextColor(ARGBUtil.getArgb(10));
			}
			else
			{
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			}
		}
		
		// 选中靓号规则
		if (mBtnOldNumRules != null)
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mBtnOldNumRules.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
				mBtnOldNumRules.setTextColor(ARGBUtil.getArgb(2));
			}
			else
			{
				mBtnOldNumRules.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
		}
		
		// 初始化靓号规则
		if(btmobcholist != null && (btmobcholist.size() > 0))
		{
			Button rulesButton = btmobcholist.get(0);
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				rulesButton.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
				rulesButton.setTextColor(ARGBUtil.getArgb(10));
			}
			else
			{
				rulesButton.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			}
		}
		
		if(mBtnPayPre != null)
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mBtnPayPre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
				mBtnPayPre.setTextColor(ARGBUtil.getArgb(10));
			}
			else
			{
				mBtnPayPre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			}
		}
		
		if(mBtnPayAfter != null)
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mBtnPayAfter.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
				mBtnPayAfter.setTextColor(ARGBUtil.getArgb(2));
			}
			else
			{
				mBtnPayAfter.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
		}

		arrSelcet[0] = 1;
		arrSelcet[1] = -1;
		arrSelcet[2] = -1;
		arrSelcet[3] = -1;
	}

	public String[] getStrMobTypeArr()
	{
		return strMobTypeArr;
	}

	public void setStrMobTypeArr(String[] arr)
	{
		strMobTypeArr = arr;
	}

	public String[] getStrSystemArr()
	{

		return strSystemArr;

	}

	public void setStrSystemArr(String[] arr)
	{
		strSystemArr = arr;
	}

	public interface onChoiceGetListener
	{
		public void onChoiceGot(int[] arrSelcet, String[] arrStr);
	}

	// 按钮点击监听 
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		switch (v.getId())
		{
			case R.id.btn_reelect:
				mBtnReSelect.onTouch(event);
				break;
			case R.id.btn_select:
				mBtnSelect.onTouch(event);
				break;
			default:
				break;
		}
		return false;
	}
}
