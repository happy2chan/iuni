package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

public class ActivityDetailsDialog extends Dialog implements android.view.View.OnClickListener
{
	private TextView	 mTvTitle;
	private TextView	 mTvContent;
	
	public ActivityDetailsDialog(Context context)
	{
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_custom_content);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_custom_content_orange);
		}
		ImageView ivClose = (ImageView) findViewById(R.id.ivClose);
		ivClose.setOnClickListener(this);
		mTvTitle = (TextView) findViewById(R.id.tvTitle);
		mTvContent = (TextView) findViewById(R.id.tvContent);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			// �رնԻ���
			case R.id.ivClose:
				this.dismiss();
				break;

			default:
				break;
		}
	}
	
	public void show(String title, String content)
	{
		mTvTitle.setText(title);
		mTvContent.setText(content);
		super.show();
	}

}
