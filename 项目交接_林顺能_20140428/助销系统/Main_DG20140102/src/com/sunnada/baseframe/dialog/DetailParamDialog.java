package com.sunnada.baseframe.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.PhoneModel;
import com.sunnada.baseframe.bean.RecommendPackage;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.util.EntryDisplayUtil;

public class DetailParamDialog extends Dialog implements android.view.View.OnClickListener
{
	private Context				m_context			= null;
	private ImageView			tab_base			= null;
	private ImageView			tab_entertainment	= null;
	private ImageView			tab_camera			= null;
	private ImageView			tab_other			= null;

	private LinearLayout		lay_base			= null;
	private LinearLayout		lay_entertainment	= null;
	private LinearLayout		lay_camera			= null;
	private LinearLayout		lay_other			= null;
	private EntryDisplayUtil	util				= null;
	private Button				btn_dlg_close		= null;
	private int					lastIndex			= 0;
	private PhoneModel			model;

	public DetailParamDialog(Context context, PhoneModel model)
	{
		super(context);
		m_context = context; 
		this.model = model;
		initView(); 
		initData();
	}
	
	public DetailParamDialog(Context context, RecommendPackage pack)
	{
		super(context);
		m_context = context; 
		this.model = pack.getmPhoneModel();
		initView();
		initData();
	}

	private void initView()
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_detail_param);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_detail_param_orange);
		}
		tab_base = (ImageView) findViewById(R.id.tab_base);
		tab_camera = (ImageView) findViewById(R.id.tab_camera);
		tab_entertainment = (ImageView) findViewById(R.id.tab_entertainment);
		tab_other = (ImageView) findViewById(R.id.tab_other);

		lay_base = (LinearLayout) findViewById(R.id.lay_base);
		lay_camera = (LinearLayout) findViewById(R.id.lay_camera);
		lay_entertainment = (LinearLayout) findViewById(R.id.lay_entertainment);
		lay_other = (LinearLayout) findViewById(R.id.lay_other);

		tab_base.setOnClickListener(this);
		tab_camera.setOnClickListener(this);
		tab_entertainment.setOnClickListener(this);
		tab_other.setOnClickListener(this);
	}

	private void initData()
	{
		util = new EntryDisplayUtil(m_context);
		util.setColorString("#999999");
		util.setHorizontalPadding(20);
		util.setLabelWidth(220);
		util.setTextSize(24);
		util.setTextWidth(660);
		util.setVerticalPadding(10);

		String[][] baseParam = new String[][]
		{
		{ "品牌", model.getBand() },
		{ "型号", model.getName()},
		{ "颜色", model.getColor() },
		{ "主屏尺寸(英寸)", model.getScreenSize() },
		{ "主屏参数", model.getScreenParam() },
		{ "操作系统", model.getOp() }, 
		{ "网络模式", model.getNet() },
		{ "核心数量", model.getCoreNum() },
		{ "CPU频率", model.getCpu() },
		{ "机身外观", model.getSize() },
		{ "机身内存", model.getRom() },
		{ "运行内存", model.getRam() },
		{ "最大扩展内存", model.getExtendRom() },
		{ "电池类型", model.getBattery() },
		{ "标配容量", model.getBatteryStorage() } };
		util.displayEntryInfomations(baseParam, lay_base);

		String[][] cameraParam = new String[][]
		{
		{ "主摄像头像素", model.getCamera1() },
		{ "副摄像头像素", model.getCamera2() },
		{ "摄像头变焦模式", model.getZoom() } };
		util.displayEntryInfomations(cameraParam, lay_camera);

		String[][] entertainmentParam = new String[][]
		{
		{ "音乐格式", model.getMusicFormat() },
		{ "视频格式", model.getVedioFormat() },
		{ "JAVA", model.getJava().equals("1")? "支持" : model.getJava().equals("0") ? "不支持":"", },
		{ "收音机", model.getRadio().equals("1")? "支持" : model.getJava().equals("0") ? "不支持":"",},
		{ "录音", model.getRecord().equals("1")? "支持" : model.getJava().equals("0") ? "不支持":"",} };
		util.displayEntryInfomations(entertainmentParam, lay_entertainment);

		String[][] otherParam = new String[][]
		{
		{ "数据传输", model.getDataTrans() },
		{ "GPS", model.getGps().equals("1")? "支持" : model.getJava().equals("0") ? "不支持":""},
		{ "理论通话时间", model.getCallTime() },
		{ "理论待机时间", model.getWaitTime() },
		{ "相关配件", model.getAccess() } };
		util.displayEntryInfomations(otherParam, lay_other);
		btn_dlg_close = (Button) findViewById(R.id.btn_dlg_close);
		btn_dlg_close.setOnClickListener(this);

		lastIndex = 0;
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.tab_base:
				if (lastIndex != 0)
				{
					switch (lastIndex)
					{
						case 0:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_base.setImageResource(R.drawable.tab_base);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_base.setImageResource(R.drawable.tab_base_orange);
							}
							lay_base.setVisibility(View.GONE);
							break;
						case 1:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_entertainment.setImageResource(R.drawable.tab_entertainment);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_entertainment.setImageResource(R.drawable.tab_entertainment_orange);
							}
							lay_entertainment.setVisibility(View.GONE);
							break;
						case 2:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_camera.setImageResource(R.drawable.tab_camera);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_camera.setImageResource(R.drawable.tab_camera_orange);
							}
							lay_camera.setVisibility(View.GONE);
							break;
						case 3:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_other.setImageResource(R.drawable.tab_other);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_other.setImageResource(R.drawable.tab_other_orange);
							}
							lay_other.setVisibility(View.GONE);
							break;
						default:
							break;
					}
					if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
					{
						tab_base.setImageResource(R.drawable.tab_base_selected);
					}
					else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
					{
						tab_base.setImageResource(R.drawable.tab_base_selected_orange);
					}
					lay_base.setVisibility(View.VISIBLE);
					lastIndex = 0;
				}

				break;
			case R.id.tab_entertainment:
				if (lastIndex != 1)
				{
					switch (lastIndex)
					{
						case 0:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_base.setImageResource(R.drawable.tab_base);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_base.setImageResource(R.drawable.tab_base_orange);
							}
							lay_base.setVisibility(View.GONE);
							break;
						case 1:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_entertainment.setImageResource(R.drawable.tab_entertainment);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_entertainment.setImageResource(R.drawable.tab_entertainment_orange);
							}
							lay_entertainment.setVisibility(View.GONE);
							break;
						case 2:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_camera.setImageResource(R.drawable.tab_camera);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_camera.setImageResource(R.drawable.tab_camera_orange);
							}
							lay_camera.setVisibility(View.GONE);
							break;
						case 3:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_other.setImageResource(R.drawable.tab_other);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_other.setImageResource(R.drawable.tab_other_orange);
							}
							lay_other.setVisibility(View.GONE);
							break;
						default:
							break;
					}
					if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
					{
						tab_entertainment.setImageResource(R.drawable.tab_entertainment_selected);
					}
					else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
					{
						tab_entertainment.setImageResource(R.drawable.tab_entertainment_selected_orange);
					}
					lay_entertainment.setVisibility(View.VISIBLE);
					lastIndex = 1;
				}
				break;
			case R.id.tab_camera:
				if (lastIndex != 2)
				{
					switch (lastIndex)
					{
						case 0:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_base.setImageResource(R.drawable.tab_base);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_base.setImageResource(R.drawable.tab_base_orange);
							}
							lay_base.setVisibility(View.GONE);
							break;
						case 1:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_entertainment.setImageResource(R.drawable.tab_entertainment);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_entertainment.setImageResource(R.drawable.tab_entertainment_orange);
							}
							lay_entertainment.setVisibility(View.GONE);
							break;
						case 2:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_camera.setImageResource(R.drawable.tab_camera);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_camera.setImageResource(R.drawable.tab_camera_orange);
							}
							lay_camera.setVisibility(View.GONE);
							break;
						case 3:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_other.setImageResource(R.drawable.tab_other);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_other.setImageResource(R.drawable.tab_other_orange);
							}
							lay_other.setVisibility(View.GONE);
							break;
						default:
							break;
					}
					if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
					{
						tab_camera.setImageResource(R.drawable.tab_camera_selected);
					}
					else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
					{
						tab_camera.setImageResource(R.drawable.tab_camera_selected_orange);
					}
					lay_camera.setVisibility(View.VISIBLE);
					lastIndex = 2;
				}
				break;

			case R.id.tab_other:
				if (lastIndex != 3)
				{
					switch (lastIndex)
					{
						case 0:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_base.setImageResource(R.drawable.tab_base);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_base.setImageResource(R.drawable.tab_base_orange);
							}
							lay_base.setVisibility(View.GONE);
							break;
						case 1:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_entertainment.setImageResource(R.drawable.tab_entertainment);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_entertainment.setImageResource(R.drawable.tab_entertainment_orange);
							}
							lay_entertainment.setVisibility(View.GONE);
							break;
						case 2:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_camera.setImageResource(R.drawable.tab_camera);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_camera.setImageResource(R.drawable.tab_camera_orange);
							}
							lay_camera.setVisibility(View.GONE);
							break;
						case 3:
							if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
							{
								tab_other.setImageResource(R.drawable.tab_other);
							}
							else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
							{
								tab_other.setImageResource(R.drawable.tab_other_orange);
							}
							lay_other.setVisibility(View.GONE);
							break;
						default:
							break;
					}
					if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
					{
						tab_other.setImageResource(R.drawable.tab_other_selected);
					}
					else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
					{
						tab_other.setImageResource(R.drawable.tab_other_selected_orange);
					}
					lay_other.setVisibility(View.VISIBLE);
					lastIndex = 3;
				}
				break;
			case R.id.btn_dlg_close:
				this.dismiss();
				break;
			default:
				break;
		}
	}

}
