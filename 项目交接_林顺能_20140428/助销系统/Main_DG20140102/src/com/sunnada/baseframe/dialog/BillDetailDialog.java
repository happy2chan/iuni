package com.sunnada.baseframe.dialog;

import java.util.ArrayList;
import java.util.List;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.adapter.MyCustomAdapter;
import com.sunnada.baseframe.bean.ChildBean;
import com.sunnada.baseframe.bean.GroupBean;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.util.ARGBUtil;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class BillDetailDialog extends Dialog implements View.OnClickListener 
{
	private ImageView					mDlgClose;
	private TextView					mTvTitle;
	private TextView					mTvTel;
	private TextView					mTvDate;
	private ListView					mLvBill;
	
	private String						mTitle;
	private String						mNum;
	
	//private String[]					mGroupName;
	
	private List<GroupBean>				mGroupList = new ArrayList<GroupBean>();
	private List<List<ChildBean>>		mChildList = new ArrayList<List<ChildBean>>();
	private MyCustomAdapter				mAdapter;
	private Context						mContext;
	
	
	private TextView					mTvCount1, mTvCount2, mTvCount3;
	private TextView					mTvCountResult1, mTvCountResult2, mTvCountResult3;
	private TextView[]					mTvCounts = {
														mTvCount1,mTvCount2,mTvCount3
										  			};
	private int[]						mTvCountsId = {
														R.id.tv_count1,R.id.tv_count2,R.id.tv_count3
													};
	private TextView[]					mTvCountResults = {
														mTvCountResult1,mTvCountResult2,mTvCountResult3
													};
	private int[]						mTvCountResultsId = {
														R.id.tv_count_result1,
														R.id.tv_count_result2,
														R.id.tv_count_result3
													};
	
	public BillDetailDialog(Context context, String detail, String title, String num) 
	{
		super(context);
		this.mTitle = title;
		this.mNum = num;
		this.mContext = context;
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_mobilebill_detail_orange);
		}
		else
		{
			setContentView(R.layout.dlg_mobilebill_detail);
		}
		
		// 初始化数据
		initDate(detail);
		// 注册监听事件
		registerListener();

		// 初始化控件
		initView();
		// 初始化显示
		// initShow();
	}

	public void initDate(String detail)
	{
		String[] tempStr = detail.split("\\n");
		mGroupList.clear();
		mChildList.clear();
	
		List<ChildBean> list = new ArrayList<ChildBean>();
		
		int tempLen = tempStr.length;
		
		for(int i = 0;i < tempStr.length;i++)
		{
			if(tempStr[i].startsWith("|"))
			{
				tempLen -= 1;
				break;
			}
		}
		
		for(int i=0;i<tempLen;i++)
		{
			if(!tempStr[i].startsWith("--"))
			{
				String[] groupArr = null;
				if(tempStr[i].split("\\:").length > 1)
				{
					groupArr= tempStr[i].split("\\:");
				}
				else
				{
					groupArr= tempStr[i].split("\\：");
				}
				GroupBean groupBean = new GroupBean();
				groupBean.setmSubject(groupArr[0]);
				groupBean.setmDetail(groupArr[1]);
				mGroupList.add(groupBean);
				
				
				if((i+1) < tempLen)
				{
					if(!tempStr[i+1].startsWith("--"))
					{
						mChildList.add(list);
						list = new ArrayList<ChildBean>();
					}
					
				}
				else
				{
					mChildList.add(list);
					list = new ArrayList<ChildBean>();
				}
			}
			else
			{
				ChildBean childBean = new ChildBean();
				String childStr = tempStr[i].split("--")[1];
				String[] childArr = null;
				if(childStr.split("\\:").length > 1)
				{
					childArr = childStr.split("\\:");
				}
				else
				{
					childArr = childStr.split("\\：");
				}
				childBean.setmSubject(childArr[0]);
				childBean.setmDetail(childArr[1]);
				list.add(childBean);
				
				if((i+1) < tempLen)
				{
					if(!tempStr[i+1].startsWith("--"))
					{
						mChildList.add(list);
						list = new ArrayList<ChildBean>();
					}
					
				}
				else
				{
					mChildList.add(list);
					list = new ArrayList<ChildBean>();
				}
			}
		}
	}
	
	public void initView()
	{	 
		 mTvTitle = (TextView) findViewById(R.id.tv_detail_title);
		 
		 mTvTitle.setText(mTitle+"受理成功,详细信息如下:");
		 
		 mTvTel = (TextView)findViewById(R.id.tv_tel);
		 mTvTel.setText(mNum);
		 
		 mTvDate = (TextView) findViewById(R.id.tv_date);
		 mLvBill = (ListView) findViewById(R.id.lv_bill);
		 
		 for(int i=0;i<mTvCounts.length;i++)
		 {
			 mTvCounts[i] = (TextView)this.findViewById(mTvCountsId[i]);
			 mTvCountResults[i] = (TextView)this.findViewById(mTvCountResultsId[i]);
			 
			 mTvCounts[i].setText(mGroupList.get(mGroupList.size()-i-1).getmSubject().trim());
			 mTvCountResults[i].setText(mGroupList.get(mGroupList.size()-i-1).getmDetail().trim());
		 }
		 
		 String time1 = mGroupList.get(0).getmDetail();
		 String time2 = mGroupList.get(1).getmDetail();
		 String startDate = time1.substring(0,4)+"年"+time1.substring(4,6)+"月"+time1.substring(6,8)+"日";
		 String endDate = time2.substring(0,4)+"年"+time2.substring(4,6)+"月"+time2.substring(6,8)+"日";
		 mTvDate.setText(startDate+"\t至\t"+endDate);
		 
		 List<GroupBean> tempGroupList = new ArrayList<GroupBean>();
		 List<List<ChildBean>> tempChildList = new ArrayList<List<ChildBean>>();
		 
		 for(int i = 2;i<(mGroupList.size() - 3);i++)
		 {
			 tempGroupList.add(mGroupList.get(i));
			 tempChildList.add(mChildList.get(i));
		 }
		 
		 mAdapter = new MyCustomAdapter(mContext, tempGroupList, tempChildList);
		 mLvBill.setVerticalFadingEdgeEnabled(true);
		 mLvBill.setFadingEdgeLength(60);
		 mLvBill.setAdapter(mAdapter);
		 mLvBill.setSelector(R.drawable.hide_listview_yellow_selector);
	}
	
	public void initShow()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mTvTitle.setTextColor(ARGBUtil.getArgb(2));
			mTvTel.setTextColor(ARGBUtil.getArgb(3));
			mDlgClose.setImageResource(R.drawable.btn_dlg_close_selector_orange);
		}
	}
	
	// 注册监听事件
	private void registerListener() 
	{
		// 关闭对话框
		mDlgClose = (ImageView) findViewById(R.id.iv_detail_close);
		mDlgClose.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
		// 关闭对话框
		case R.id.iv_detail_close:
			dismiss();
			break;
			
		default:
			break;
		}
	}
}
