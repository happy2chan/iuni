package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.ui.MyCustomButton2;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MessageboxDialog extends Dialog implements OnClickListener, OnTouchListener
{
	private TextView				mTextTitle;							// 消息框标题
	private Button					mBtnDialogClose;					// 关闭消息框
	private TextView				mTextInfo;							// 消息框内容
	private LinearLayout            mLayContent;                        // 动态加载View
	private MyCustomButton			mBtnCancel;							// 取消按钮
	private MyCustomButton			mBtnOk;								// 确定按钮
	
	private View.OnClickListener	mNegativeListener	= null;			// 取消按钮监听
	private View.OnClickListener	mPositiveListener	= null;			// 确定按钮监听
	private View.OnClickListener	mDismissListener	= null;
	
	public MessageboxDialog(Context context) 
	{
		super(context, R.style.transparent_dialog);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_messagebox);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_messagebox_orange);
		}
		setCanceledOnTouchOutside(false);
		
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	private void initViews() 
	{
		mTextTitle 		= (TextView) findViewById(R.id.tv_messagebox_title);
		mBtnDialogClose = (Button) findViewById(R.id.btn_dialog_close);
		mTextInfo 		= (TextView) findViewById(R.id.tv_messagebox_info);
		mLayContent     = (LinearLayout) findViewById(R.id.lay_content);
		
		// 消息框关闭
		mBtnDialogClose = (Button) findViewById(R.id.btn_dialog_close);
		mBtnDialogClose.setOnClickListener(this);
		
		mBtnCancel 	= (MyCustomButton) findViewById(R.id.btn_cancel);
		mBtnOk 		= (MyCustomButton) findViewById(R.id.btn_ok);
		
		mBtnCancel.setVisibility(View.GONE);
		mBtnOk.setVisibility(View.GONE);
	}
	
	// 设置标题
	public void setTitle(CharSequence titleStr) 
	{
		mTextTitle.setText(titleStr);
	}
	
	// 设置关闭按钮是否可见
	public void setCloseButtonVisible(boolean bVisible) 
	{
		mBtnDialogClose.setVisibility(bVisible? View.VISIBLE:View.GONE);
	}
	
	// 设置提示消息
	public void setMessage(CharSequence msgStr) 
	{
		mTextInfo.setVisibility(View.VISIBLE);
		mTextInfo.setText(msgStr);
	}
	
	// 加载外界视图
	public void setContentView(View content)
	{
		this.findViewById(R.id.lay_message).setVisibility(View.GONE);
		mLayContent.setVisibility(View.VISIBLE);
		mLayContent.addView(content);
	}
	
	// 设置取消监听事件
	public void setNegetiveListener(String cancelStr, View.OnClickListener negativeListener) 
	{
		mBtnCancel.setImageResource(R.drawable.btn_reselect);
		mBtnCancel.setTextViewText1(cancelStr);
		mBtnCancel.setOnClickListener(this);
		mBtnCancel.setOnTouchListener(this);
		mBtnCancel.setVisibility(View.VISIBLE);
		this.mNegativeListener = negativeListener;
	}
	
	// 设置确定监听事件
	public void setPositiveListener(String okStr, View.OnClickListener positiveListener)
	{
		mBtnOk.setImageResource(R.drawable.btn_custom_check);
		mBtnOk.setTextViewText1(okStr);
		mBtnOk.setOnClickListener(this);
		mBtnOk.setOnTouchListener(this);
		mBtnOk.setVisibility(View.VISIBLE);
		this.mPositiveListener = positiveListener;
	}
	
	// 设置关闭事件
	public void setDismissListener(View.OnClickListener dismissListener) 
	{
		this.mDismissListener = dismissListener;
	}
	
	// 点击事件
	@Override
	public void onClick(View v) 
	{
		// 销毁对话框
		dismiss();
		// 响应点击事件
		if (v == mBtnOk && mPositiveListener != null) 
		{
			mPositiveListener.onClick(v);
		}
		else if (v == mBtnCancel && mNegativeListener != null) 
		{
			mNegativeListener.onClick(v);
		}
		else if (v == mBtnDialogClose && mDismissListener != null) 
		{
			mDismissListener.onClick(v);
		}
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		// 监听子antivity中按钮的点击样式 20131018
		if(v instanceof MyCustomButton)
		{
			((MyCustomButton)v).onTouch(event);
		}
		else
		{
			((MyCustomButton2)v).onTouch(event);
		}
		return false;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		return true;
		//return super.onKeyDown(keyCode, event);
	}
}
