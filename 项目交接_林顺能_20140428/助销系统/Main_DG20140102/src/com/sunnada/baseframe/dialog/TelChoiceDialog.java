package com.sunnada.baseframe.dialog;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.business.Business_0C;
import com.sunnada.baseframe.bean.Rule;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.ui.MyCustomButton;

public class TelChoiceDialog extends Dialog implements android.view.View.OnClickListener, OnTouchListener
{
	private String				strMobTypeArr[]	=
												{ "不限", "AAAA", "AABB", "ABAB", "ABCD", "ABBA", "BBAA", "ABBB", "AAAB" };
	private String				strSystemArr[]	=
												{ "不限", "130", "131", "132", "155", "156", "185", "186" };

	// private LayoutInflater inflater;
	// private View dialogView;
	private LinearLayout		lay_mobchotype;
	private LinearLayout		lay_system;
	private Context				m_context		= null;
	// private Activity mobileOrderActivity;
	//private Button			bt_close		= null;
	private Button				bt_pay_pre, bt_pay_after;																	// 付费类型
	private Button				oldbt_payway;
	private LinearLayout		mLayPrice1, mLayPrice2, mLayPrice3, mLayPrice4, mLayPrice5, mLayPrice6, mLayPrice7;//, mLayPrice8;
	// private View screenvi1, screenvi2, screenvi3, screenvi4, screenvi5,
	// screenvi6, screenvi7;
	private List<Button>		btmobcholist	= new ArrayList<Button>();
	private List<Button>		btsystemlist	= new ArrayList<Button>();
	private MyCustomButton		mBtnReSelect;
	private MyCustomButton		mBtnSelect;

	private View				oldPriceView;
	// private View oldSceenView;
	private TextView			oldPriceTv;
	// private TextView oldSceenTv;
	// private Button oldbt_CPU;
	private Button				mBtnOldNumRules;																			// 靓号规则
	private Button				mBtnOldNum;																				// 号段
	private int[]				arrSelcet		=
												{ 1, -1, -1, -1 };															// 号段、价格、靓号规则
	private String[]			arrStr			=
												{ "", "" };																// 号段、靓号规则
	private onChoiceGotListener	m_listener		= null;

	public TelChoiceDialog(Activity context, onChoiceGotListener listener)
	{
		super(context);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dlg_telchoice);
		m_context = context;
		// mobileOrderActivity = context;
		this.m_listener = listener;
		// 初始化对话框内容
		dlgInit();
	}

	// 初始化对话框内容
	public void dlgInit()
	{
		//bt_close = (Button) findViewById(R.id.bt_close);
		lay_mobchotype = (LinearLayout) findViewById(R.id.lay_mobchotype);
		lay_system = (LinearLayout) findViewById(R.id.lay_system);
		mBtnReSelect = (MyCustomButton) findViewById(R.id.btn_reelect);
		mBtnReSelect.setTextViewText1("重选");
		mBtnReSelect.setTextViewText2("");
		mBtnReSelect.setOnTouchListener(this);
		
		mBtnSelect = (MyCustomButton) findViewById(R.id.btn_select);
		mBtnSelect.setTextViewText1("查询");
		mBtnSelect.setTextViewText2("");
		mBtnSelect.setOnTouchListener(this);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			mBtnReSelect.setImageResource(R.drawable.btn_reselect_orange);
			mBtnSelect.setImageResource(R.drawable.btn_custom_query_orange);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnReSelect.setImageResource(R.drawable.btn_reselect);
			mBtnSelect.setImageResource(R.drawable.btn_custom_query);
			mBtnSelect.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_common_orange));
			mBtnReSelect.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_common_orange));
		}
		// 初始化付费类型
		payWayInit();
		// 初始化号段
		numInit();
		// 初始化价格
		priceInit();
		// 初始化靓号规则
		numRulesInit();
		//bt_close.setOnClickListener(this);
		mBtnReSelect.setOnClickListener(this);
		mBtnSelect.setOnClickListener(this);
	}

	// 付费类型
	public void payWayInit()
	{
		bt_pay_pre = (Button) findViewById(R.id.bt_pay_pre);
		bt_pay_pre.setOnClickListener(this);
		bt_pay_after = (Button) findViewById(R.id.bt_pay_after);
		bt_pay_after.setOnClickListener(this);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			bt_pay_pre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			bt_pay_after.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			bt_pay_pre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
			bt_pay_after.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			bt_pay_pre.setTextColor(Color.parseColor("#FFFFFF"));
		}
	}

	// 初始号段
	public void numInit()
	{
		String[] arr = getStrSystemArr();
		for (int i = 0; i < arr.length; i++)
		{
			Button button = new Button(m_context);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			lp.leftMargin = 20;
			button.setLayoutParams(lp);
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				button.setTextColor(Color.parseColor("#FFFFFF"));
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				button.setTextColor(Color.parseColor("#F5934F"));
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			}
			button.setText(arr[i]);
			button.setTextSize(22);
			button.setId(i);
			lay_system.addView(button);
			btsystemlist.add(button);
			button.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					mBtnOldNum = (Button) view;
					arrSelcet[1] = mBtnOldNum.getId();
					selectButton(btsystemlist, view);
				}
			});
		}
		if(btsystemlist != null && btsystemlist.size() > 0)
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				btsystemlist.get(0).setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				btsystemlist.get(0).setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
				btsystemlist.get(0).setTextColor(Color.parseColor("#FFFFFF"));
			}
		}
	}

	// 初始化价格
	public void priceInit()
	{
		mLayPrice1 = (LinearLayout) findViewById(R.id.lay_price1);
		mLayPrice2 = (LinearLayout) findViewById(R.id.lay_price2);
		mLayPrice3 = (LinearLayout) findViewById(R.id.lay_price3);
		mLayPrice4 = (LinearLayout) findViewById(R.id.lay_price4);
		mLayPrice5 = (LinearLayout) findViewById(R.id.lay_price5);
		mLayPrice6 = (LinearLayout) findViewById(R.id.lay_price6);
		mLayPrice7 = (LinearLayout) findViewById(R.id.lay_price7);
		//mLayPrice8 = (LinearLayout) findViewById(R.id.lay_price8);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			initPriceView(mLayPrice1);
			initPriceView(mLayPrice2);
			initPriceView(mLayPrice3);
			initPriceView(mLayPrice4);
			initPriceView(mLayPrice5);
			initPriceView(mLayPrice6);
			initPriceView(mLayPrice7);
		}
		mLayPrice1.setOnClickListener(this);
		mLayPrice2.setOnClickListener(this);
		mLayPrice3.setOnClickListener(this);
		mLayPrice4.setOnClickListener(this);
		mLayPrice5.setOnClickListener(this);
		mLayPrice6.setOnClickListener(this);
		mLayPrice7.setOnClickListener(this);
		//mLayPrice8.setOnClickListener(this);
		mLayPrice1.performClick();
	}

	private void initPriceView(LinearLayout layPrice)
	{
		TextView tvPrice = (TextView) layPrice.getChildAt(0);
		View viewPrice = layPrice.getChildAt(1);
		
		viewPrice.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
		tvPrice.setTextColor(Color.parseColor("#F3B48C"));
	}

	// 靓号规则
	public void numRulesInit()
	{
		for (int i = 0; i < Business_0C.numberRules.size(); i++)
		{
			Rule rule = Business_0C.numberRules.get(i);
			Button button = new Button(m_context);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			lp.leftMargin = 10;
			button.setLayoutParams(lp);
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				button.setTextColor(Color.parseColor("#FFFFFF"));
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				button.setTextColor(Color.parseColor("#F5934F"));
				button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			}
			button.setText(rule.getContent());
			button.setTextSize(22);
			button.setId(rule.getId());
			lay_mobchotype.addView(button);
			btmobcholist.add(button);
			button.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					mBtnOldNumRules = (Button) view;
					arrSelcet[3] = mBtnOldNumRules.getId();
					selectButton(btmobcholist, view);
				}
			});
		}
		if(btmobcholist != null && btsystemlist.size() > 0)
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				btmobcholist.get(0).setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				btmobcholist.get(0).setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
				btmobcholist.get(0).setTextColor(Color.parseColor("#FFFFFF"));
			}
		}
	}

	// 按钮选中后改变状态
	public void selectButton(List<Button> crrlist, View view)
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			view.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			for (int i = 0; i < crrlist.size(); i++)
			{
				Button button = crrlist.get(i);
				if (button.getId() != view.getId())
				{
					button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
				}
			}
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			view.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
			((Button)view).setTextColor(Color.parseColor("#FFFFFF"));
			for (int i = 0; i < crrlist.size(); i++)
			{
				Button button = crrlist.get(i);
				if (button.getId() != view.getId())
				{
					button.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
					button.setTextColor(Color.parseColor("#F5934F"));
				}
			}
		}
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			/*
			// 关闭号码选择对话框
			case R.id.bt_close:
				this.dismiss();
				break;
			*/

			// 选择价格1
			case R.id.lay_price1:
				selectPriceView(v, 0);
				break;
				
			// 选择价格2
			case R.id.lay_price2:
				selectPriceView(v, 1);
				break;
				
			// 选择价格3
			case R.id.lay_price3:
				selectPriceView(v, 2);
				break;
				
			// 选择价格4
			case R.id.lay_price4:
				selectPriceView(v, 3);
				break;
				
			// 选择价格5
			case R.id.lay_price5:
				selectPriceView(v, 4);
				break;
				
			// 选择价格6
			case R.id.lay_price6:
				selectPriceView(v, 5);
				break;
				
			// 选择价格7
			case R.id.lay_price7:
				selectPriceView(v, 6);
				break;
				
			// 选择价格8 最高
			case R.id.lay_price8:
				selectPriceView(v, 7);
				break;

			// 重选
			case R.id.btn_reelect:
				cleanSelect();
				break;

			// 选择
			case R.id.btn_select:
				if (mBtnOldNum != null)
				{
					arrStr[0] = mBtnOldNum.getText().toString();
				}
				if (mBtnOldNumRules != null)
				{
					arrStr[1] = mBtnOldNumRules.getText().toString();
				}
				if (m_listener != null)
				{
					m_listener.onChoiceGot(arrSelcet, arrStr);
				}
				this.cancel();
				break;

			// 预付费
			case R.id.bt_pay_pre:
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					bt_pay_after.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
					bt_pay_pre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					bt_pay_pre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
					bt_pay_after.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
					bt_pay_pre.setTextColor(Color.parseColor("#FFFFFF"));
					bt_pay_after.setTextColor(Color.parseColor("#EB6221"));
				}
				oldbt_payway = bt_pay_pre;
				arrSelcet[0] = 1;
				break;

			// 后付费
			case R.id.bt_pay_after:
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					bt_pay_pre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
					bt_pay_after.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					bt_pay_pre.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
					bt_pay_after.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
					bt_pay_pre.setTextColor(Color.parseColor("#EB6221"));
					bt_pay_after.setTextColor(Color.parseColor("#FFFFFF"));
				}
				oldbt_payway = bt_pay_after;
				arrSelcet[0] = 2;
				break;
			default:
				break;
		}
	}

	// 选择价格
	public void selectPriceView(View view, int index)
	{
		LinearLayout lay = (LinearLayout) view;
		TextView tvPrice = (TextView) lay.getChildAt(0);
		View viewPrice = lay.getChildAt(1);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			if (oldPriceView != null)
			{
				oldPriceView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
			if (oldPriceTv != null)
			{
				oldPriceTv.setTextColor(Color.parseColor("#00A6EC"));
			}
			tvPrice.setTextColor(Color.parseColor("#FF0000"));
			viewPrice.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumlvse));
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			if (oldPriceView != null)
			{
				oldPriceView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			}
			if (oldPriceTv != null)
			{
				oldPriceTv.setTextColor(Color.parseColor("#F3B48C"));
			}
			tvPrice.setTextColor(Color.parseColor("#EB6221"));
			viewPrice.setBackgroundColor(Color.parseColor("#FEC26A"));
		}
		oldPriceView = viewPrice;
		oldPriceTv = tvPrice;
		arrSelcet[2] = index;
	}

	// 取消选择
	public void cleanSelect()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			if (oldPriceView != null)
			{
				oldPriceView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
			if (oldPriceTv != null)
			{
				oldPriceTv.setTextColor(Color.parseColor("#00A6EC"));
			}

			if (mBtnOldNum != null)
			{
				mBtnOldNum.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
			if (mBtnOldNumRules != null)
			{
				mBtnOldNumRules.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
			if (oldbt_payway != null)
			{
				oldbt_payway.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumblue));
			}
			
			if(btmobcholist != null && btsystemlist.size() > 0)
			{
				btmobcholist.get(0).setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			}
			if(btsystemlist != null && btsystemlist.size() > 0)
			{
				btsystemlist.get(0).setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.telnumchosed));
			}
			
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			if (oldPriceView != null)
			{
				oldPriceView.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			}
			if (oldPriceTv != null)
			{
				oldPriceTv.setTextColor(Color.parseColor("#F3B48C"));
			}
			if (mBtnOldNum != null)
			{
				mBtnOldNum.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			}
			if (mBtnOldNumRules != null)
			{
				mBtnOldNumRules.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			}
			if (oldbt_payway != null)
			{
				oldbt_payway.setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_select_orange));
			}
			
			if(btmobcholist != null && btsystemlist.size() > 0)
			{
				btmobcholist.get(0).setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
			}
			if(btsystemlist != null && btsystemlist.size() > 0)
			{
				btsystemlist.get(0).setBackgroundDrawable(m_context.getResources().getDrawable(R.drawable.btn_tip_selected_orange));
			}
		}
		
		arrSelcet[0] = 1;   // 默认为预付费
		arrSelcet[1] = -1;
		arrSelcet[2] = -1;
		arrSelcet[3] = -1;
		bt_pay_pre.performClick();
		mLayPrice1.performClick();
	}

	public String[] getStrMobTypeArr()
	{
		return strMobTypeArr;
	}

	public void setStrMobTypeArr(String[] arr)
	{
		strMobTypeArr = arr;
	}

	public String[] getStrSystemArr()
	{
		return strSystemArr;
	}

	public void setStrSystemArr(String[] arr)
	{
		strSystemArr = arr;
	}

	public interface onChoiceGotListener
	{
		public void onChoiceGot(int[] arrSelcet, String[] arrStr);
	}

	// 按钮点击监听
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		switch (v.getId())
		{
			case R.id.btn_reelect:
				mBtnReSelect.onTouch(event);
				break;
			case R.id.btn_select:
				mBtnSelect.onTouch(event);
				break;
			default:
				break;
		}
		return false;
	}
}
