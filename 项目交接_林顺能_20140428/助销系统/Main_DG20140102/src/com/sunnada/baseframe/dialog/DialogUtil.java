package com.sunnada.baseframe.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;

public class DialogUtil
{
	private static MyProgressDialog			mProgress	= null;
	private static Activity					mContext	= null;
	private static Dialog           		mDialog     = null;

	public static void init(Activity context)
	{
		if (mContext != null)
		{
			if (mContext != context)
			{
				if(mProgress != null)
				{
					mProgress.dismiss();
				}
				mProgress = new MyProgressDialog(context, null);
				mContext = context;
			}
		}
		else
		{
			mProgress = new MyProgressDialog(context, null);
			mContext = context;
		}
	}
	
	// 显示吐司
	public static void showToast(final String toastStr, final int toastType) 
	{
		mContext.runOnUiThread(new Runnable() 
		{
			public void run() 
			{
				LayoutInflater inflater = mContext.getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup)mContext.findViewById(R.id.toast_layout_root));
				// 提示字串
				TextView text = (TextView) layout.findViewById(R.id.toast_text);
				text.setText(toastStr);

				Toast toast = new Toast(mContext);
				toast.setGravity(Gravity.BOTTOM, 0, 0);
				toast.setDuration(toastType == 0? Toast.LENGTH_SHORT:Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
		});
	}
	
	// 显示进度条
	public static void showProgress(final String msg) 
	{
		mContext.runOnUiThread(new Runnable() 
		{
			public void run() 
			{
				if (!mProgress.isShowing()) 
				{
					mProgress.show();
				}
				mProgress.setText(msg);
			}
		});
	}
	
	// 显示消息框
	public static void showMessage(String msg) 
	{
		showMessageDialog("温馨提示", msg, "确定", null, null, null);
	}

	public static void showMessage(String title, String msg) 
	{
		showMessageDialog(title, msg, "确定", null, null, null);
	}
	
	// 显示只有确定按钮的消息框(不带消息响应)
	public static void showMessage(String title, String msg, String okStr) 
	{
		showMessageDialog(title, msg, okStr, null, null, null);
	}
	
	// 显示只有确定按钮的消息框(带消息响应)
	public static void showMessage(String title, String msg, String okStr, OnClickListener listener) 
	{
		showMessageDialog(title, msg, okStr, listener, null, null);
	}
	
	// okResID: 资源ID
	public static void showMessage(String title, String msg, int okResID, OnClickListener listener) 
	{
		showMessageDialog(title, msg, mContext.getString(okResID), listener, null, null);
	}
	
	// 带消息传递的消息框
	public static void showMessage(
			final String title, 
			final String msg, 
			final int okResID, 
			final int okMsgID, 
			final Handler handler) 
	{
		showMessageDialog(title, 
				msg, 
				mContext.getString(okResID), 
				new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						if(ButtonUtil.isFastDoubleClick(v.getId(), 300)) 
						{
							return;
						}
						handler.sendEmptyMessage(okMsgID);
					}
				}, 
				null, 
				null);
	}
	
	public static void showMessageAndDeal(String msg, OnClickListener listener) 
	{
		showMessageDialog(null, msg, "确定", listener, null, null);
	}
	
	public static void showSelectDialog(String msg, OnClickListener confirmListener) 
	{
		showSelectDialog(msg, confirmListener, null);
	}
	
	public static void showSelectDialog(String msg, OnClickListener confirmListener, OnClickListener cancelListener) 
	{
		showMessageDialog(null, msg, "确定", confirmListener, "取消", cancelListener);
	}
	
	// 不带消息传递的对话框
	public static void showMessageDialog(
			final String titleStr, 
			final String msgStr, 
			final String confirmStr, 
			final OnClickListener confirmListener,
			final String cancelStr, 
			final OnClickListener cancelListener) 
	{
		showMessageDialog(titleStr, msgStr, confirmStr, confirmListener, cancelStr, cancelListener, null, null);
	}
	
	// 显示带消息传递的对话框
	public static void showMessageDialog(
			final String titleStr, 
			final String msgStr, 
			final String confirmStr, 
			final int confirmID,
			final String cancelStr, 
			final int cancelID, 
			final Handler handler) 
	{
		mContext.runOnUiThread(new Runnable() 
		{
			@Override
			public void run()
			{
				CommonDialog dialog = new CommonDialog(mContext);
				if (!StringUtil.isEmptyOrNull(titleStr))
				{
					dialog.setTitle(titleStr, null);
				}
				dialog.setMessage(msgStr);
				if (!StringUtil.isEmptyOrNull(confirmStr))
				{
					dialog.setPositiveListener(confirmStr, new View.OnClickListener() 
					{
						@Override
						public void onClick(View v) 
						{
							if(ButtonUtil.isFastDoubleClick(v.getId(), 300)) 
							{
								return;
							}
							handler.sendEmptyMessage(confirmID);
						}
					});
				}
				if (!StringUtil.isEmptyOrNull(cancelStr))
				{
					dialog.setNegetiveListener(cancelStr, new View.OnClickListener() 
					{
						@Override
						public void onClick(View v) 
						{
							if(ButtonUtil.isFastDoubleClick(v.getId(), 300)) 
							{
								return;
							}
							handler.sendEmptyMessage(cancelID);
						}
					});
				}
				dialog.show();
			}
		});
	}
	
	// 显示对话框(3个按钮)
	public static void showMessageDialog(
			final String titleStr, 
			final String msgStr, 
			final String confirmStr, 
			final OnClickListener confirmListener,
			final String cancelStr, 
			final OnClickListener cancelListener,
			final String neutralStr,
			final OnClickListener neutralListener) 
	{
		mContext.runOnUiThread(new Runnable() 
		{
			@Override
			public void run()
			{
				CommonDialog dialog = new CommonDialog(mContext);
				if (!StringUtil.isEmptyOrNull(titleStr))
				{
					dialog.setTitle(titleStr, null);
				}
				dialog.setMessage(msgStr);
				if (!StringUtil.isEmptyOrNull(confirmStr))
				{
					dialog.setPositiveListener(confirmStr, confirmListener);
				}
				if (!StringUtil.isEmptyOrNull(cancelStr))
				{
					dialog.setNegetiveListener(cancelStr, cancelListener);
				}
				if (!StringUtil.isEmptyOrNull(neutralStr))
				{
					dialog.setNeutralListener(neutralStr, neutralListener);
				}
				dialog.show();
			}
		});
	}
	
	public static CommonDialog showPwdDialog(final OnPasswordGetListener listener)
	{
		final CommonDialog dialog = new CommonDialog(mContext);
		mContext.runOnUiThread(new Runnable() 
		{
			@Override
			public void run()
			{
				dialog.setTitle("请输入交易密码");
				View lay = mContext.getLayoutInflater().inflate(R.layout.lay_pwd, null);
				dialog.setMessage(lay);

				final EditText et_pwd = (EditText) dialog.findViewById(R.id.editText);
				dialog.setPositiveListener("确定", new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if(ButtonUtil.isFastDoubleClick(v.getId(), 300)) 
						{
							return;
						}
						if (listener != null) 
						{
							listener.onPasswordGet(et_pwd.getText().toString());
						}
					}
				});
				dialog.setNegetiveListener("取消", null);
				dialog.show();
			}
		});
		return dialog;
	}
	
	public static CommonDialog showCustomerDialog(
			final String titleStr,
			final View content, 
			final String confirmStr,
			final OnClickListener confirmListener, 
			final String cancelStr,
			final OnClickListener cancelListener)
	{
		return showCustomerDialog(titleStr, content, confirmStr, confirmListener, cancelStr, cancelListener, null, null, true);
	}

	public static CommonDialog showCustomerDialog(
			final String titleStr,
			final View contentStr, 
			final String confirmStr,
			final OnClickListener confirmListener, 
			final String cancelStr,
			final OnClickListener cancelListener, 
			final String middleStr,
			final OnClickListener middleListener, 
			boolean isMiss)
	{
		final CommonDialog dialog = new CommonDialog(mContext);
		mDialog = dialog;
		dialog.isMiss = isMiss;
		mContext.runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				if (!StringUtil.isEmptyOrNull(titleStr))
				{
					dialog.setTitle(titleStr);
				}
				dialog.setMessage(contentStr);
				if (!StringUtil.isEmptyOrNull(confirmStr))
				{
					dialog.setPositiveListener(confirmStr, confirmListener);
				}
				if (!StringUtil.isEmptyOrNull(cancelStr))
				{
					dialog.setNegetiveListener(cancelStr, cancelListener);
				}
				if (!StringUtil.isEmptyOrNull(middleStr))
				{
					dialog.setNeutralListener(middleStr, middleListener);
				}
				dialog.show();
			}
		});
		return dialog;
	}
	
	// 带确定、取消、dismiss监听
	public static CommonDialog showCustomerDialog(
			final String titleStr,
			final View contentStr, 
			final String confirmStr,
			final OnClickListener confirmListener, 
			final String cancelStr,
			final OnClickListener cancelListener, 
			final OnClickListener dismissListner,
			boolean isMiss)
	{
		final CommonDialog dialog = new CommonDialog(mContext);
		mDialog = dialog;
		dialog.isMiss = isMiss;
		mContext.runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				if (!StringUtil.isEmptyOrNull(titleStr))
				{
					dialog.setTitle(titleStr);
				}
				dialog.setMessage(contentStr);
				if (!StringUtil.isEmptyOrNull(confirmStr))
				{
					dialog.setPositiveListener(confirmStr, confirmListener);
				}
				if (!StringUtil.isEmptyOrNull(cancelStr))
				{
					dialog.setNegetiveListener(cancelStr, cancelListener);
				}
				if(dismissListner != null)
				{
					dialog.setDismissListenner(dismissListner);
				}
				dialog.show();
			}
		});
		return dialog;
	}
	
	public static void closeProgress()
	{
		mContext.runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				if (mProgress != null && mProgress.isShowing())
				{
					mProgress.dismiss();
				}
			}
		});
	}
	
	public interface OnPasswordGetListener
	{
		public void onPasswordGet(String password);
	}
	
	public interface OnGetListener
	{
		public void onGet(String value);
	}
	
	// 关闭消息框
	public static void dismiss() 
	{
		if(mDialog != null)
		{
			mDialog.dismiss();
		}
	}
	
	// 显示带消息传递的对话框
	public static void MsgBox(
			final String titleStr, 
			final String msgStr, 
			final String confirmStr, 
			final int confirmID,
			final String cancelStr, 
			final int cancelID, 
			final Handler handler) 
	{
		MsgBox(titleStr, msgStr, confirmStr, confirmID, cancelStr, cancelID, 0x00, handler);
	}
	
	public static void MsgBox(
			final String titleStr, 
			final String msgStr, 
			final String confirmStr, 
			final int confirmID,
			final String cancelStr, 
			final int cancelID, 
			final int dismissID, 
			final Handler handler) 
	{
		mContext.runOnUiThread(new Runnable() 
		{
			@Override
			public void run()
			{
				final MessageboxDialog dialog = new MessageboxDialog(mContext);
				mDialog = dialog;
				// 设置标题
				if (!StringUtil.isEmptyOrNull(titleStr)) 
				{
					dialog.setTitle(titleStr);
				}
				// 设置消息内容
				dialog.setMessage(msgStr);
				// 设置确定监听事件
				if (!StringUtil.isEmptyOrNull(confirmStr))
				{
					dialog.setPositiveListener(confirmStr, new View.OnClickListener() 
					{
						@Override
						public void onClick(View v) 
						{
							if(ButtonUtil.isFastDoubleClick(v.getId(), 300)) 
							{
								return;
							}
							if(handler != null)
							{
								handler.sendEmptyMessage(confirmID);
							}
						}
					});
				}
				// 设置取消监听事件
				if (!StringUtil.isEmptyOrNull(cancelStr)) 
				{
					dialog.setNegetiveListener(cancelStr, new View.OnClickListener() 
					{
						@Override
						public void onClick(View v) 
						{
							if(ButtonUtil.isFastDoubleClick(v.getId(), 300)) 
							{
								return;
							}
							if(handler != null)
							{
								handler.sendEmptyMessage(cancelID);
							}
						}
					});
				}
				// 设置关闭事件监听
				if(dismissID != 0) 
				{
					dialog.setDismissListener(new View.OnClickListener() 
					{
						@Override
						public void onClick(View v) 
						{
							if(ButtonUtil.isFastDoubleClick(v.getId(), 300)) 
							{
								return;
							}
							if(handler != null) 
							{
								handler.sendEmptyMessage(dismissID);
							}
						}
					});
				}
				dialog.show();
			}
		});
	}
	
	// 只有确定按钮, 不带消息ID的提示框
	public static void MsgBox(final String titleStr, final String msgStr) 
	{
		//MsgBox(titleStr, msgStr, "确定", null, null, null, null);
		showMessage(titleStr, msgStr);
	}
	
	// 只有确定按钮, 带消息ID的提示框
	public static void MsgBox(final String titleStr, final String msgStr, final int msgID, final Handler handler) 
	{
		showMessage(titleStr, 
				msgStr, 
				"确定", 
				new View.OnClickListener() 
				{
					@Override
					public void onClick(View arg0) 
					{
						if(handler != null) 
						{
							handler.sendEmptyMessage(msgID);
						}
					}
				});
	}
	
	// 消息提示框, 没有关闭按钮
	public static void MsgBox(
			final String titleStr, 
			final String msgStr, 
			final String confirmStr, 
			final View.OnClickListener confirmListener, 
			final String cancelStr, 
			final View.OnClickListener cancelListener) 
	{
		mContext.runOnUiThread(new Runnable() 
		{
			@Override
			public void run()
			{
				final MessageboxDialog dialog = new MessageboxDialog(mContext);
				mDialog = dialog;
				// 设置标题
				if (!StringUtil.isEmptyOrNull(titleStr)) 
				{
					dialog.setTitle(titleStr);
				}
				// 设置消息内容
				dialog.setMessage(msgStr);
				// 设置确定监听事件
				if (!StringUtil.isEmptyOrNull(confirmStr))
				{
					dialog.setPositiveListener(confirmStr, confirmListener);
				}
				// 设置取消监听事件
				if (!StringUtil.isEmptyOrNull(cancelStr)) 
				{
					dialog.setNegetiveListener(cancelStr, cancelListener);
				}
				// 设置关闭事件监听
				dialog.setCloseButtonVisible(false);
				dialog.show();
			}
		});
	}
	
	public static void MsgBox(
			final String titleStr, 
			final String msgStr, 
			final String confirmStr, 
			final View.OnClickListener confirmListener, 
			final String cancelStr, 
			final View.OnClickListener cancelListener, 
			final View.OnClickListener dismissListener) 
	{
		mContext.runOnUiThread(new Runnable() 
		{
			@Override
			public void run()
			{
				final MessageboxDialog dialog = new MessageboxDialog(mContext);
				mDialog = dialog;
				// 设置标题
				if (!StringUtil.isEmptyOrNull(titleStr)) 
				{
					dialog.setTitle(titleStr);
				}
				// 设置消息内容
				dialog.setMessage(msgStr);
				// 设置确定监听事件
				if (!StringUtil.isEmptyOrNull(confirmStr))
				{
					dialog.setPositiveListener(confirmStr, confirmListener);
				}
				// 设置取消监听事件
				if (!StringUtil.isEmptyOrNull(cancelStr)) 
				{
					dialog.setNegetiveListener(cancelStr, cancelListener);
				}
				// 设置关闭事件监听
				dialog.setDismissListener(dismissListener);
				dialog.show();
			}
		});
	}
	
	public static void MsgBox(
			final String titleStr, 
			final int resourceId, 
			final String confirmStr, 
			final String cancelStr, 
			final OnClickListener cancelListener, 
			final OnGetListener listener,
			final OnClickListener dismissListener) 
	{
		mContext.runOnUiThread(new Runnable() 
		{
			@Override
			public void run()
			{
				final MessageboxDialog dialog = new MessageboxDialog(mContext);
				mDialog = dialog;
				// 设置标题
				if (!StringUtil.isEmptyOrNull(titleStr)) 
				{
					dialog.setTitle(titleStr);
				}
				
				View view = mContext.getLayoutInflater().inflate(resourceId, null);
				final EditText editText = (EditText) view.findViewById(R.id.editText);
				// 设置消息内容
				dialog.setContentView(view);
				// 设置确定监听事件
				if (!StringUtil.isEmptyOrNull(confirmStr))
				{
					dialog.setPositiveListener(confirmStr, new View.OnClickListener() 
					{
						@Override
						public void onClick(View v) 
						{
							if(ButtonUtil.isFastDoubleClick(v.getId(), 300)) 
							{
								return;
							}
							if (listener != null)
							{
								listener.onGet(editText.getText().toString());
							}
						}
					});
				}
				// 设置取消监听事件
				if (!StringUtil.isEmptyOrNull(cancelStr)) 
				{
					dialog.setNegetiveListener(cancelStr, cancelListener);
				}
				
				// 设置关闭事件监听
				dialog.setDismissListener(dismissListener);
				dialog.show();
			}
		});
	}
	
	public static Dialog getDialog()
	{
		return mDialog;
	}
}
