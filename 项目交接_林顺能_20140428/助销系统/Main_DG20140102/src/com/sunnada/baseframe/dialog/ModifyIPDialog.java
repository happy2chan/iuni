package com.sunnada.baseframe.dialog;

import com.sunnada.baseframe.activity.LaunchActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.ui.MyCustomButton2;
import com.sunnada.baseframe.util.StringUtil;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ModifyIPDialog extends Dialog implements OnClickListener, OnTouchListener
{
	private Context					mContext;
	private TextView				mTextTitle;							// 消息框标题
	private Button					mBtnDialogClose;					// 关闭消息框
	private MyCustomButton			mBtnCancel;							// 取消按钮
	private MyCustomButton			mBtnOk;								// 确定按钮
	
	private View.OnClickListener	mNegativeListener	= null;			// 取消按钮监听
	private View.OnClickListener	mPositiveListener	= null;			// 确定按钮监听
	private View.OnClickListener	mDismissListener	= null;
	
	private EditText				mEtIP;								// 服务IP
	private EditText				mEtPort;							// 服务器端口
	private ISocketService 			mSocketService;
	
	public ModifyIPDialog(Context context, ISocketService socketService)
	{
		super(context);
		this.mContext = context;
		this.mSocketService = socketService;
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.dlg_modify_ip);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.dlg_modify_ip_orange);
		}
		setCanceledOnTouchOutside(false);
		// 初始化控件
		initViews();
	}
	
	// 初始化控件 
	private void initViews() 
	{
		mTextTitle 		= (TextView) findViewById(R.id.tv_messagebox_title);
		mBtnDialogClose = (Button) findViewById(R.id.btn_dialog_close);
		
		// 消息框关闭
		mBtnDialogClose = (Button) findViewById(R.id.btn_dialog_close);
		mBtnDialogClose.setOnClickListener(this);
		
		mBtnCancel 	= (MyCustomButton) findViewById(R.id.btn_cancel);
		mBtnOk 		= (MyCustomButton) findViewById(R.id.btn_ok);
		
		mBtnCancel.setVisibility(View.GONE);
		mBtnOk.setVisibility(View.GONE);
		
		mEtIP = (EditText) findViewById(R.id.etServerIP);
		mEtPort = (EditText) findViewById(R.id.etServerPort);
		SharedPreferences sp = mContext.getSharedPreferences(Statics.CONFIG, Activity.MODE_PRIVATE);
		String strIP = sp.getString("ip", LaunchActivity.mStrServerIP);
		int port = sp.getInt("port", LaunchActivity.mServerPort);
		mEtIP.setText(strIP);
		mEtPort.setText(port+"");
	}
	
	// 设置标题
	public void setTitle(CharSequence titleStr) 
	{
		mTextTitle.setText(titleStr);
	}

	// 设置取消监听事件
	public void setNegetiveListener(String cancelStr, View.OnClickListener negativeListener) 
	{
		mBtnCancel.setImageResource(R.drawable.btn_reselect);
		mBtnCancel.setTextViewText1(cancelStr);
		mBtnCancel.setOnClickListener(this);
		mBtnCancel.setOnTouchListener(this);
		mBtnCancel.setVisibility(View.VISIBLE);
		this.mNegativeListener = negativeListener;
	}
	
	// 设置确定监听事件
	public void setPositiveListener(String okStr, View.OnClickListener positiveListener)
	{
		mBtnOk.setImageResource(R.drawable.btn_custom_check);
		mBtnOk.setTextViewText1(okStr);
		mBtnOk.setOnClickListener(this);
		mBtnOk.setOnTouchListener(this);
		mBtnOk.setVisibility(View.VISIBLE);
		this.mPositiveListener = positiveListener;
	}
	
	// 设置关闭事件
	public void setDismissListener(View.OnClickListener dismissListener) 
	{
		this.mDismissListener = dismissListener;
	}
	
	// 点击事件
	@Override
	public void onClick(View v) 
	{
		if (v == mBtnOk && mPositiveListener != null) 
		{
			String strIP = mEtIP.getText().toString().trim();
			String strPort = mEtPort.getText().toString().trim();
			if(!StringUtil.isEmptyOrNull(strIP))
			{
				LaunchActivity.mStrServerIP = strIP;
			}
			else
			{
				DialogUtil.MsgBox("温馨提示", "请输入IP！", "确定", 0, "", 0, null);
				// DialogUtil.showMessage("请输入IP!");
				return;
			}
			if(!StringUtil.isEmptyOrNull(strPort))
			{
				LaunchActivity.mServerPort = Integer.parseInt(strPort);
			}
			else
			{
				DialogUtil.MsgBox("温馨提示", "请输入端口！", "确定", 0, "", 0, null);
				// DialogUtil.showMessage("请输入端口!");
				return;
			}
			mPositiveListener.onClick(v);
			Editor edit = mContext.getSharedPreferences(Statics.CONFIG,Activity.MODE_PRIVATE).edit();
			edit.putString("ip", strIP);
			edit.putInt("port", Integer.parseInt(strPort));
			edit.commit();
			mSocketService.disConnect();
			mSocketService.connect(true);
			DialogUtil.MsgBox("温馨提示", "接入环境地址修改成功", "确定", 0, "", 0, null);
		}
		else if (v == mBtnCancel && mNegativeListener != null) 
		{
			mNegativeListener.onClick(v);
		}
		else if (v == mBtnDialogClose && mDismissListener != null) 
		{
			mDismissListener.onClick(v);
		}
		dismiss();
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		// 监听子antivity中按钮的点击样式 20131018
		if(v instanceof MyCustomButton)
		{
			((MyCustomButton)v).onTouch(event);
		}
		else
		{
			((MyCustomButton2)v).onTouch(event);
		}
		return false;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		return true;
	}
}
