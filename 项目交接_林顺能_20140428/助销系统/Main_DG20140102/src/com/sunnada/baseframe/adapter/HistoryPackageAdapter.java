package com.sunnada.baseframe.adapter;

import java.util.List;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.RecommendPackage;
import com.sunnada.baseframe.util.StringUtil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class HistoryPackageAdapter extends BaseAdapter
{
	private Context					mContext;
	private List<RecommendPackage>	mData;
	private int						mResource;
	//private OnClickListener		mListenner;

	public HistoryPackageAdapter(Context context, List<RecommendPackage> data, int resource, OnClickListener listenner)
	{
		this.mContext 		= context;
		this.mData 			= data;
		this.mResource 		= resource;
		//this.mListenner 	= listenner;
	}

	@Override
	public int getCount()
	{
		return mData.size();
	}

	@Override
	public RecommendPackage getItem(int arg0)
	{
		return mData.get(arg0);
	}

	@Override
	public long getItemId(int arg0)
	{
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2)
	{
		ImageView mIvPreview = null;
		TextView mTvTitle, mTvDescribe, mTvPrice;
		ViewHolder vHolder;
		if (convertView == null)
		{ 
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(mResource, null);
			mIvPreview = (ImageView) convertView.findViewById(R.id.ivPreview);
			mTvTitle   = (TextView) convertView.findViewById(R.id.tvTitle);
			mTvDescribe = (TextView) convertView.findViewById(R.id.tvDescribe);
			mTvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
			vHolder = new ViewHolder(mIvPreview, mTvTitle, mTvDescribe, mTvPrice);
			convertView.setTag(vHolder);
		}
		else
		{
			vHolder = (ViewHolder) convertView.getTag();
			mIvPreview = vHolder.mIvPreview;
			mTvTitle = vHolder.mTvTitle;
			mTvDescribe = vHolder.mTvDescribe;
			mTvPrice = vHolder.mTvPrice;
		}
		final RecommendPackage rp = mData.get(position); 
		mTvTitle.setText(rp.getTitle());
		mTvDescribe.setText(StringUtil.adjustStringLength(rp.getmProduct().getmDescribe(), 20));
		/*
		if(rp.ismIsBindPhone())
		{
			//mTvPrice.setText(rp.getmPhoneModel().getPriceDes());
			//rp.getmPhoneModel().showInImageView(mIvPreview);
			if(StringUtil.isEmptyOrNull(rp.getmProduct().getmBigPic()))
			{
				// 推荐包图片为空时展示机型图片
				rp.getmPhoneModel().showInImageView(mIvPreview);
			}
			else
			{
				// 推荐包图片不为空时展示推荐包图片
				Log.e("44", "position :" + position + " pack.id :" + rp.getmId());
				rp.getmProduct().showInImageView(mIvPreview);
			}
		}
		else
		{
			rp.getmProduct().showInImageView(mIvPreview);
		}
		*/
		rp.showImageView(mIvPreview);
		mTvPrice.setText(rp.getmProduct().getmPriceDes());
		return convertView;
	}

	class ViewHolder
	{
		ImageView mIvPreview;
		TextView mTvTitle, mTvDescribe, mTvPrice;
		public ViewHolder(ImageView mIvPreview, TextView mTvTitle, TextView mTvMarketTag, TextView mTvPrice)
		{
			this.mIvPreview = mIvPreview;
			this.mTvTitle = mTvTitle;
			this.mTvDescribe = mTvMarketTag;
			this.mTvPrice = mTvPrice;
		}
	}
}
