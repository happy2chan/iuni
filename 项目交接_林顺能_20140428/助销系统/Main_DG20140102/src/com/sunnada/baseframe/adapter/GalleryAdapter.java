package com.sunnada.baseframe.adapter;

import java.util.TimerTask;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GalleryAdapter extends BaseAdapter 
{
	private Context 					mContext;
	private int 						selectItem;
	private Integer[] 					mImageIds;
	private ImageView 					imageView;

	private TextView 					teltv1;
	private TextView 					teltv2;
	private TextView 					teltv3;
	private TextView 					teltv4;
	public boolean ishandle = true;
	
	public GalleryAdapter(Context mContext, Integer[] ImageIds) 
	{
		this.mContext = mContext;
		mImageIds = ImageIds;
	}

	@Override
	public int getCount() 
	{
		//System.out.println("111");
		return mImageIds.length;
	}
	
	@Override
	public Object getItem(int position) 
	{
		//System.out.println("222");
		return position;
	}

	@Override
	public long getItemId(int position) 
	{
		//System.out.println("333");
		return position;
	}

	public void setSelectItem(int selectItem) 
	{
		if (this.selectItem != selectItem) 
		{
			this.selectItem = selectItem;
			notifyDataSetChanged();
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		imageView = new ImageView(mContext);
		imageView.setLayoutParams(new Gallery.LayoutParams(100, 150));
		imageView.setImageResource(mImageIds[position]);
		RelativeLayout lp = new RelativeLayout(mContext);
		lp.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL);
		if (selectItem == position) 
		{
			//imageView.setLayoutParams(new Gallery.LayoutParams(200, 300));
			lp.setLayoutParams(new Gallery.LayoutParams(200, 300));
			lp.addView(imageView);
			return lp;
		} 
		else 
		{
			lp.setLayoutParams(new Gallery.LayoutParams(100, 300));
			lp.addView(imageView);
			return lp;
		}
	}
	
	public class Task extends TimerTask 
	{
		@Override
		public void run() 
		{
		    Message msg = new Message();  
		    msg.what = 1;
		    handler.sendMessage(msg);  
		    ishandle = false;
		}
	}	
	
	Handler handler = new Handler() 
	{  
	    @Override  
	    public void handleMessage(Message msg) 
	    {  
	    	//hyperspaceJumpAnimation.cancel();
	    	
	    	teltv1.setVisibility(View.VISIBLE);
	   	   	teltv2.setVisibility(View.VISIBLE);
	   	   	teltv3.setVisibility(View.VISIBLE);
	   	   	teltv4.setVisibility(View.VISIBLE);
	    	//System.out.println("sdfsdfsdfsd");
	        //super.handleMessage(msg);
	    }
	};
    
    public void settv(TextView teltv1,TextView teltv2,TextView teltv3,TextView teltv4) 
    {
    	this.teltv1 = teltv1;
        this.teltv2 = teltv2;
        this.teltv3 = teltv3;
        this.teltv4 = teltv4;
    }
}
