package com.sunnada.baseframe.adapter;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;

public class MeauListAdapter extends SimpleAdapter
{
	//private int[]		colors		= new int[] { 0x30FF0000, 0x300000FF };
	private String[]	string;
	private int[]		arrryint;
	private int			resource;
	private Context		context;
	boolean				isfirst		= true;
	private int			selectItem	= -1;
	private View		oldview;

	/**
	 * 构造函数：TextButtonAdapter 函数功能: 参数说明：
	 * 
	 * @param context
	 * @param data
	 * @param resource
	 * @param from
	 * @param to
	 */
	public MeauListAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to)
	{
		super(context, data, resource, from, to);
		this.context = context;
		this.resource = resource;
		this.arrryint = to;
		this.string = from;
	}

	public void setSelectItem(int selectItem)
	{
		this.selectItem = selectItem;
		if (oldview != null)
		{
			oldview.setBackgroundDrawable(new BitmapDrawable());
		}
		// notifyDataSetChanged();
	}

	@Override
	public int getCount()
	{
		return super.getCount();
	};

	/**
	 * 函数名称 : getView 功能描述 : 功能暂不强大，小心使用 参数说明：
	 * 
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return 返回值：
	 * 
	 *         修改记录： 日期：2011-11-20 下午8:56:18 修改人：lmr 描述 ：
	 * 
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView tv1 = null, tv2 = null;

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(resource, null);

		tv1 = (TextView) convertView.findViewById(arrryint[0]);
		if (arrryint.length > 1)
		{
			tv2 = (TextView) convertView.findViewById(arrryint[1]);
		}

		@SuppressWarnings("unchecked")
		Map<String, Object> map = (Map<String, Object>) getItem(position);
		String tt1 = (String) map.get(string[0]);
		String tt2 = "";
		if (arrryint.length > 1)
		{
			tt2 = (String) map.get(string[1]);
		}

		tv1.setText(tt1);
		if (arrryint.length > 1)
		{
			tv2.setText(tt2);
		}

		if (arrryint.length > 1)
		{
			if (selectItem == position)
			{
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					convertView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.layright_1selected));
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					convertView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.layright_1selected_orange));
				}
				oldview = convertView;
			}
			else
			{
				convertView.setBackgroundDrawable(new BitmapDrawable());
			}
		}
		else
		{
			if (selectItem == position)
			{
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					convertView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.layright_2selected));
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					convertView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.layright_2selected_orange));
				}
				oldview = convertView;
			}
			else
			{
				convertView.setBackgroundDrawable(new BitmapDrawable());
			}
		}
		return convertView;
	}

}
