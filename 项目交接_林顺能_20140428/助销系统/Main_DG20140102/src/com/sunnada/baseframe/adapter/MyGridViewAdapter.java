package com.sunnada.baseframe.adapter;

import java.util.List;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.activity.dztj.RecommendActivity;
import com.sunnada.baseframe.bean.RecommendPackage;
import com.sunnada.baseframe.util.BitmapPoolUtil;
import com.sunnada.baseframe.util.StringUtil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class MyGridViewAdapter extends BaseAdapter
{
	private RecommendActivity 		mActivity;
	private int				  		mSource;
	private List<RecommendPackage> 	mData;
	private LayoutInflater			mInflater;
	
	public MyGridViewAdapter(Context activity, int layout_id, List<RecommendPackage> data)
	{
		this.mActivity 	= (RecommendActivity) activity;
		this.mSource	= layout_id;
		this.mData 		= data;
		mInflater 		= LayoutInflater.from(activity);
	}

	@Override
	public int getCount()
	{
		return mData.size();
	}

	@Override
	public RecommendPackage getItem(int position)
	{
		return mData.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		Button mBtn = null;
		ImageView mIvPic, mIvHui, mIvZen, mIvNew, mBead = null;
		TextView mTvDescribe, mTvPrice, mTvPackage;
		ViewHolder mHolder = null;
		if(convertView == null)
		{
			convertView = mInflater.inflate(mSource, null); 
			convertView.setLayoutParams(new GridView.LayoutParams((int) mActivity.getResources().getDimension(R.dimen.grid_view_item_x), (int) mActivity.getResources().getDimension(R.dimen.grid_view_item_y)));
			mBtn = (Button) convertView.findViewById(R.id.btnCompare);
			mIvPic = (ImageView) convertView.findViewById(R.id.mainPic);
			mTvPackage = (TextView) convertView.findViewById(R.id.tvPackage);
			mTvDescribe = (TextView) convertView.findViewById(R.id.tvDescribe);
			mTvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
			
			mIvHui = (ImageView) convertView.findViewById(R.id.ivHui);
			mIvZen = (ImageView) convertView.findViewById(R.id.ivZen);
			mIvNew = (ImageView) convertView.findViewById(R.id.ivNew);
			mBead  = (ImageView) convertView.findViewById(R.id.bead);
			mHolder = new ViewHolder(mBtn, mIvPic, mIvHui, mIvZen, mIvNew, mBead, mTvDescribe, mTvPrice, mTvPackage);
			convertView.setTag(mHolder);
		}
		else
		{
			mHolder = (ViewHolder) convertView.getTag();
			mBtn = mHolder.mBtn;
			mIvPic = mHolder.mIvPic;
			mTvPackage = mHolder.mTvPackage;
			mTvDescribe = mHolder.mTvDescribe;
			mTvPrice = mHolder.mTvPrice;
			mIvHui = mHolder.mIvHui;
			mIvZen = mHolder.mIvZen;
			mIvNew = mHolder.mIvNew;
			mBead = mHolder.mBead;
		}
		
		RecommendPackage pack = getItem(position);
		/*
		if(pack.ismIsBindPhone())
		{
			if(StringUtil.isEmptyOrNull(pack.getmProduct().getmBigPic()))
			{
				// 推荐包图片为空时展示机型图片
				pack.getmPhoneModel().showInImageView(mIvPic);
			}
			else
			{
				// 推荐包图片不为空时展示推荐包图片
				Log.e("44", "position :" + position + " pack.id :" + pack.getmId());
				pack.getmProduct().showInImageView(mIvPic);
			}
		}
		else
		{
			// 套餐靓号
			pack.getmProduct().showInImageView(mIvPic);
		}
		*/
		pack.showImageView(mIvPic);
		mTvPrice.setText(pack.getmProduct().getmPriceDes());
		
		if(!StringUtil.isEmptyOrNull(pack.getmProduct().getmIsGift()) 
				&& pack.getmProduct().getmIsGift().equals("1"))
		{
			// 有赠品
			mIvZen.setVisibility(View.VISIBLE);
		}
		else
		{
			mIvZen.setVisibility(View.GONE);
		}
		mTvPackage.setText(pack.getTitle());
		mTvDescribe.setText(StringUtil.adjustStringLength(pack.getmProduct().getmDescribe(), 20));
		// 营销标签
		mIvHui.setVisibility(View.GONE);
		mIvNew.setVisibility(View.GONE);
		String market_tag = pack.getmProduct().getmMarktingTag();
		String[] tags = market_tag.split(",");
		if(tags != null)
		{
			for(int i = 0; i < tags.length; i++)
			{
				/*
				if(tags[i].endsWith("01"))
				{
					// 热销
					mIvZen.setVisibility(View.VISIBLE);
				}
				*/
				if(tags[i].endsWith("03"))
				{
					// 优惠
					mIvHui.setVisibility(View.VISIBLE);
				}
				else if(tags[i].endsWith("05"))
				{
					// 新品
					mIvNew.setVisibility(View.VISIBLE);
				}
			}
		}
		
		// 加入对比之后该按钮变暗，不能点击
		if(pack.ismIsSelected())
		{
			mBtn.setEnabled(false);
			mBtn.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.btn_package_compare_done));
		}
		else
		{
			mBtn.setEnabled(true);
			mBtn.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.btn_package_compare));
		}
			
		final View view = mBtn;
		mBtn.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)  
			{
				mActivity.addToCompare(position, view);
			} 
		});
		convertView.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mActivity.selectItem(position);
			}
		});
		return convertView;
	}
	
	class ViewHolder 
	{
		Button mBtn = null;
		ImageView mIvPic, mIvHui, mIvZen, mIvNew, mBead;
		TextView mTvDescribe, mTvPrice, mTvPackage;
		public ViewHolder(Button mBtn, ImageView mIvPic, ImageView mIvHui, ImageView mIvZen, ImageView mIvNew, ImageView mBead,
				TextView mTvDescribe, TextView mTvPrice, TextView mTvPackage)
		{
			this.mBtn = mBtn;
			this.mIvPic = mIvPic;
			this.mIvHui = mIvHui;
			this.mIvZen = mIvZen;
			this.mIvNew = mIvNew;
			this.mBead = mBead;
			this.mTvDescribe = mTvDescribe;
			this.mTvPrice = mTvPrice;
			this.mTvPackage = mTvPackage;
		}
	}

	public void resycleImage()
	{
		BitmapPoolUtil.clear();
	}
}
