/**
 * $RCSfile: Apk.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-20  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.bean;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

import com.sunnada.baseframe.util.Log;

/**
 * <p>Title: Apk</p> 
 * <p>Description: </p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author 丘富铨
 * @date 2013-4-20
 * @version 1.0
 */
public class Apk implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String name = null;	//apk名称
	private String packageName = null;//apk的包名
	private String latestVersion = null;//最新版本
	private String minVersion = null;//最低支持版本
	private long size = 0L;//apk大小,字节为单位
	private String tips = null;//升级提示信息
	private String updateTime = null;//更新时间
	private String url = null;//apk升级地址
	private long currentPoint = 0L;//当前下载量
	private Set<String> psams = new HashSet<String>();//允许升级的psam卡号，如果为空则表示全部升级
	
	public Apk()
	{
		super();
	}
	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getPackageName()
	{
		return packageName;
	}
	public void setPackageName(String packageName)
	{
		this.packageName = packageName;
	}
	public String getLatestVersion()
	{
		return latestVersion;
	}
	public void setLatestVersion(String latestVersion)
	{
		this.latestVersion = latestVersion;
	}
	public String getCurrentVersion(Context context)
	{
		PackageInfo pf = null;
		try
		{
			pf = context.getPackageManager().getPackageInfo(packageName, 0);
		} catch (NameNotFoundException e)
		{
			e.printStackTrace();
			return "0";
		}
		return pf.versionName;
	}
	public String getMinVersion()
	{
		return minVersion;
	}
	public void setMinVersion(String minVersion)
	{
		this.minVersion = minVersion;
	}
	public String getTips()
	{
		return tips;
	}
	public void setTips(String tips)
	{
		this.tips = tips;
	}
	public String getUpdateTime()
	{
		return updateTime;
	}
	public void setUpdateTime(String updateTime)
	{
		this.updateTime = updateTime;
	}
	public String getUrl()
	{
		return url;
	}
	public void setUrl(String url)
	{
		this.url = url;
	}
	

	public long getCurrentPoint()
	{
		return currentPoint;
	}

	public void setCurrentPoint(long currentPoint)
	{
		this.currentPoint = currentPoint;
	}

	public long getSize()
	{
		return size;
	}

	public void setSize(long size)
	{
		this.size = size;
	}

	/**
	 * 通过json字符串解析生成一个apk对象
	 * @param json
	 * @return 生成失败返回null
	 */
	public static final Apk parse(String json)
	{
		JSONObject jsonObject = null;
		try
		{
			jsonObject = new JSONObject(json);
			
		} catch (JSONException e)
		{
			e.printStackTrace();
			Log.log(e);
			return null;
		}
		return parse(jsonObject);
	}
	
	/**
	 * 添加一个可升级的Psam
	 * @param psam
	 */
	public void addPsams(String psam)
	{
		psams.add(psam);
	}
	
	
	/**
	 * psam是否允许升级
	 * @param psam
	 * @return
	 */
	public boolean containsPsam(String psam)
	{
		return psams.contains(psam);
	}
	
	
	public Set<String> getPsams()
	{
		return psams;
	}

	/**
	 * 通过json对象解析生成一个APk的对象
	 * @param json
	 * @return 生成失败返回null
	 */
	public static final Apk parse(JSONObject json)
	{
		Apk apk = new Apk();
		
		if(json == null)
		{
			return null;
		}
		
		try
		{
			
			if(json.has("name"))
			{
				apk.setName(json.getString("name"));
			}
			
			if(json.has("minVersion"))
			{
				apk.setMinVersion(json.getString("minVersion"));
			}
			
			if(json.has("package"))
			{
				apk.setPackageName(json.getString("package"));
			}
			
			if(json.has("tips"))
			{
				apk.setTips(json.getString("tips"));
			}
			
			if(json.has("version"))
			{
				apk.setLatestVersion(json.getString("version"));
			}
			
			if(json.has("size"))
			{
				apk.setSize(json.getLong("size"));
			}
			
			if(json.has("updateTime"))
			{
				apk.setUpdateTime(json.getString("updateTime"));
			}
			
			if(json.has("url"))
			{
				apk.setUrl(json.getString("url"));
			}
			
			if(json.has("psams"))
			{
				JSONArray psamsArray = json.getJSONArray("psams");
				apk.getPsams().clear();
				if(psamsArray!= null && psamsArray.length()>0)
				{
					for(int i = 0;i<psamsArray.length();i++)
					{
						apk.addPsams(psamsArray.get(i).toString());
					}
				}
			}
			
		} catch (Exception e)
		{
			e.printStackTrace();
			Log.log(e);
		}
		
		return apk;
	}
	
	
	
}


