/**
 * $RCSfile: Menu.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-22  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.bean;

import java.io.Serializable;
import java.util.List;


/**
 * <p>Title: Menu</p> 
 * <p>Description: 菜单对象，对菜单的封装</p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author 丘富铨
 * @date 2013-4-22
 * @version 1.0
 */

public class Menu implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String id = null;//菜单编码
	private String name = null;//名称
	private String icon = null;//图标地址
	private String packageName = null;//包名
	private String actionType = null;//动作类型
	private String actionUrl = null;//动作地址
	private boolean offline = false;//是否允许离线试用
	private List<Menu> menus = null;//子菜单
	private Apk apk = null;
	
	public Menu()
	{
		super();
	}
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getIcon()
	{
		return icon;
	}
	public void setIcon(String icon)
	{
		this.icon = icon;
	}
	public String getPackageName()
	{
		return packageName;
	}
	public void setPackageName(String packageName)
	{
		this.packageName = packageName;
	}
	public String getActionType()
	{
		return actionType;
	}
	public void setActionType(String actionType)
	{
		this.actionType = actionType;
	}
	public String getActionUrl()
	{
		return actionUrl;
	}
	public void setActionUrl(String actionUrl)
	{
		this.actionUrl = actionUrl;
	}
	public List<Menu> getMenus()
	{
		return menus;
	}
	public void setMenus(List<Menu> menus)
	{
		this.menus = menus;
	}
	public Apk getApk()
	{
		return apk;
	}
	public void setApk(Apk apk)
	{
		this.apk = apk;
	}
	public boolean isOffline()
	{
		return offline;
	}
	public void setOffline(boolean offline)
	{
		this.offline = offline;
	}
	
}


