package com.sunnada.baseframe.bean;

public class ReturnInfo {
	public boolean bResult;
	public int iResult;
	public String desc;
	public ReturnInfo() {
		this.bResult = true;
		this.iResult = 0;
		this.desc = "";
	}
}
