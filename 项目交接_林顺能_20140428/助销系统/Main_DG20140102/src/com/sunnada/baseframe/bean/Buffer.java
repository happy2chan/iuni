package com.sunnada.baseframe.bean;

import java.util.ArrayList;

import com.sunnada.baseframe.util.Commen;

public class Buffer
{
	private ArrayList<Byte> list = new ArrayList<Byte>(1024);

	public void add(byte b)
	{
		list.add(b);
	}

	public void add(int idata)
	{
		this.add(Commen.int2bytes(idata));
	}

	public void add(short idata)
	{
		this.add(Commen.short2bytes(idata));
	}

	public void add(float idata)
	{
		int t = (int) idata;
		this.add(Commen.int2bytes(t));
	}

	public void add(byte[] b)
	{
		int i = 0;
		int len = b.length;
		while (i < len)
		{
			list.add(b[i]);
			i++;
		}
	}

	public void add(String str)
	{
		byte[] b = Commen.hexstr2byte(str);
		add(b);
	}

	public void clear()
	{
		list.clear();
	}

	public byte[] getBytes()
	{
		int len = list.size();
		byte[] res = new byte[len];
		for (int i = 0; i < len; i++)
		{
			res[i] = list.get(i);
		}
		return res;
	}
}
