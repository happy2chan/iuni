package com.sunnada.baseframe.bean;

import java.util.HashMap;
import java.util.Map;

import android.os.Parcel;
import android.os.Parcelable;

import com.sunnada.baseframe.util.Commen;
import com.sunnada.baseframe.util.Log;

/**
 * File: Package.java
 * Description: 
 * @author 丘富铨
 * @date 2013-3-7
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public class Package implements Parcelable
{
	
	private String id;//主命令
	private String subId;//子命令
	private String serviceCommandID = "FFFF";//自动机号码
	private int type = 2;//数据包类型   1为响应包，2主动下推包  
	private Map<String, byte[]> units = new HashMap<String, byte[]>();//pdu单元
	
	public static final Parcelable.Creator<Package> CREATOR = new Parcelable.Creator<Package>() 
	{
		public Package createFromParcel(Parcel in) 
		{
			return new Package(in);
		}
 
		public Package[] newArray(int size) 
		{
			return new Package[size];
		}
	};

	public Package()
	{
		super();
	}

	public Package(Parcel in)
	{
		readFromParcel(in);
	}
	
	public boolean containsKey(String key)
	{
		return units.containsKey(key.toUpperCase());
	}
	
	public byte[] get(String key)
	{
		return units.get(key);
	}
	
	/**
	 * 通过数据解析包
	 * @param data
	 * @return
	 */
	public static final Package parse(byte[] data)
	{
		Package pdata = new Package();
		pdata.setServiceCommandID(Commen.hax2str(data, 17, 2));
		pdata.setId(Commen.hax2str(data, 21, 1));
		pdata.setSubId(Commen.hax2str(data, 20, 1));
		
		if (data[22] == (byte)0xFF) 
		{
			pdata.setType(2); // 2 为平台主动下推包
		} 
		else 
		{
			pdata.setType(1); // 1 为响应包
		}
		
		byte[] pduData =new byte[data.length - 23];
		System.arraycopy(data, 23, pduData, 0, data.length - 23);
		
		int i = 0;
		int len = pduData.length;
		while (i < len)
		{
			int lenPdu;
			if (i + 4 > len)
			{
				Log.log("报文解析错误");
				return null;
			}
			lenPdu = (pduData[i] & 0xFF) | ((pduData[i + 1] & 0xFF) << 8);
			if (i + lenPdu > len)
			{
				Log.log("报文解析错误:目测是Pdu的长度不对");
				return null;
			}
			i += lenPdu;
		}

		i = 0;
		while (i < len)
		{
			int lenPdu;
			String key;
			byte code[] = null;

			lenPdu = (pduData[i] & 0xFF) | ((pduData[i + 1] & 0xFF) << 8);
			key = Commen.makeworld(pduData, i + 2, 2);
			code = new byte[lenPdu - 4];
			System.arraycopy(pduData, i + 4, code, 0, lenPdu - 4);
			// //////////////////////////////////////
			if ("0146".equals(key) && code[2] == (byte) 0x09) // 由于收件箱业务更新时会有多个0146，需要对多个0146进行合并
			{
				byte total = 0x00;
				byte[] oldcode = null;
				int ilen = 1;
				int p = 0;
				byte[] newcode = null;
				if (pdata.containsKey("0146"))
				{
					oldcode = pdata.get("0146");
					total = oldcode[5];
					ilen = oldcode.length;
					newcode = new byte[ilen + code.length - 4];
				} else
				{
					newcode = new byte[2 + code.length];
				}
				total++;
				System.arraycopy(code, 0, newcode, p, 5);
				p += 5;
				newcode[p] = total;
				p += 1;
				if (total > 1)
				{
					System.arraycopy(oldcode, p, newcode, p, ilen - 6);
					p += ilen - 6;
				}
				byte v_len = (byte) (code.length - 5);
				newcode[p++] = v_len;
				System.arraycopy(code, 5, newcode, p, code.length - 5);
				code = newcode;
			}
			// ///////////////////////////////////////////////////
			pdata.addUnit(key.toUpperCase(), code);
			i += lenPdu;
		}
		return pdata;
	}
	
	/**
	 * 添加子命令单元
	 * @param unitName 子命令
	 * @param data 数据
	 */
	public void addUnit(String unitName,byte[] data)
	{
		units.put(unitName, data);
	}
	
	/**
	 * 添加子命令单元
	 * @param unitName 子命令
	 * @param b 字节数据
	 */
	public void addUnit(String unitName,byte b)
	{
		units.put(unitName, new byte[]{b});
	}
	
	public void clear()
	{
		units.clear();
	}
	
	public String getServiceCommandID()
	{
		return serviceCommandID;
	}

	public void setServiceCommandID(String serviceCommandID)
	{
		this.serviceCommandID = serviceCommandID;
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(id);
		dest.writeString(subId);
		dest.writeString(serviceCommandID);
		dest.writeMap(units);
	}
	/**
	 * 读
	 * @param in
	 */
	@SuppressWarnings("unchecked")
	public void readFromParcel(Parcel in)
	{
		id = in.readString();
		subId = in.readString();
		serviceCommandID = in.readString();
		units = in.readHashMap(this.getClass().getClassLoader());
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id.toUpperCase();
	}

	public String getSubId()
	{
		return subId;
	}

	public void setSubId(String subId)
	{
		this.subId = subId.toUpperCase();
	}

	public Map<String, byte[]> getUnits()
	{
		return units;
	}

	public byte[] getPduBytes()
	{
		StringBuffer str = new StringBuffer();
		String value = null;
		String str_len = null;
		int pdu_len = 0;

		for (String key : units.keySet())
		{
			value = Commen.sprinthax(units.get(key), "");
			pdu_len = value.length() / 2 + 2 + 2;
			str_len = Integer.toHexString(pdu_len);
			int i = 4 - str_len.length();
			while (i-- != 0)
			{
				str_len = "0" + str_len;
			}

			// 数据长度+子命令标识+命令内容
			str.append(tail2head(str_len)).append(tail2head(key)).append(value);
		}
		Log.log("pdu:" + str.toString());
		return Commen.hexstr2byte(str.toString());
	}

	
	
	private String tail2head(String str) 
	{
		if (str.length() != 4)
		{
			return str;
		}
		String low = str.substring(0, 2);
		String high = str.substring(2, 4);
		str = high + low;
		return str;
	}

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}
}

