package com.sunnada.baseframe.bean;

public class InvoiceDataMonth
{
	private String             mQueryNum;   // 查询号码
	private String             mSerialNum;  // 查询流水
	private String             mBillDate;   // 账期
	private int                mPrintMoney; // 打印金额(分为单位)
	private String             mCPrintMoney;// 打印金额(元为单位)
	
	public InvoiceDataMonth(String queryNum, String serialNum,
			String billDate, int printMoney, String cPrintMoney) 
	{
		this.mQueryNum = queryNum;
		this.mSerialNum = serialNum;
		this.mBillDate = billDate;
		this.mPrintMoney = printMoney;
		this.mCPrintMoney = cPrintMoney;
	}

	public String getQueryNum() 
	{
		return mQueryNum;
	}

	public void setQueryNum(String queryNum) 
	{
		this.mQueryNum = queryNum;
	}

	public String getSerialNum() 
	{
		return mSerialNum;
	}

	public void setSerialNum(String serialNum)
	{
		this.mSerialNum = serialNum;
	}

	public String getBillDate() 
	{
		return mBillDate;
	}

	public void setBillDate(String billDate) 
	{
		this.mBillDate = billDate;
	}

	public int getPrintMoney() 
	{
		return mPrintMoney;
	}

	public void setPrintMoney(int printMoney) 
	{
		this.mPrintMoney = printMoney;
	}

	public String getCPrintMoney() 
	{
		return mCPrintMoney;
	}

	public void setCPrintMoney(String cPrintMoney) 
	{
		this.mCPrintMoney = cPrintMoney;
	}
}
