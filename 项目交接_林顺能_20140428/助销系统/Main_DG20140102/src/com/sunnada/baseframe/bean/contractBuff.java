package com.sunnada.baseframe.bean;

import java.io.Serializable;

public class contractBuff implements Serializable
{
	private static final long	serialVersionUID	= 1L;
	private int					id;
	private String				content;

	public contractBuff(int id, String content)
	{
		super();
		this.id = id;
		this.content = content;
	}

	public contractBuff()
	{
		super();
	}

	@Override
	public String toString()
	{
		return content;
	}

	public int getID()
	{
		return id;
	}

	public void setID(int id)
	{
		this.id = id;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}
}
