package com.sunnada.baseframe.bean;

public class ConsumePlan 
{
	private               int            mCode;
	private               String         mType;
	
	public int getmCode()
	{
		return mCode;
	}
	
	public void setmCode(int mCode) 
	{
		this.mCode = mCode;
	}
	
	public String getmType() {
		return mType;
	}
	
	public void setmType(String mType) {
		this.mType = mType;
	}
}
