package com.sunnada.baseframe.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class KeyTextBean implements Serializable
{    
	private String 			mKey;
	private String 			mValue;

	public KeyTextBean() 
	{
		
	}

	public KeyTextBean(String key, String text) 
	{
		this.mKey = key;
		this.mValue = text;
	}    
	
	@Override
	public String toString() 
	{
		return mValue;
	}
	
	public String getKey() 
	{
		return mKey;
	}
	
	public void setKey(String key) 
	{
		this.mKey = key;
	}
	
	public String getText() 
	{
		return mValue;
	}
	
	public void setText(String text) 
	{
		this.mValue = text;
	}
}
	 