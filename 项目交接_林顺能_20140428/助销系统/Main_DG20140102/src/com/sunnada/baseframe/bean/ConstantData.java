package com.sunnada.baseframe.bean;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;

import com.sunnada.baseframe.util.PictureShowUtils;
import com.sunnada.baseframe.util.StringUtil;

public class ConstantData 
{
	public static List<PhoneModel>			modelList				= null;	// 手机参数信息数据
	public static List<RecommendPackage>	mRecommendPackgeList 	= null; // 推荐包数据
	public static List<RecommendPackage>	mProductList 			= null;	// 套餐靓号数据
	public static String[]					mBrands					= null;	// 手机品牌
	public static String[]					mIndicator 				= null; // 搜索风向标

	public static String					rangeFilePath			= null;

	private static String					serverPath				= null; // 这个是平台的IP地址(http://172.16.200.28:8000)
	
	public static String getServerPath()
	{
		return serverPath;
	}

	public static void setServerPath(String serverPath)
	{
		ConstantData.serverPath = serverPath;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void setBands(String[] bands)
	{
		mBrands = bands;
		Comparator cmp = Collator.getInstance(java.util.Locale.CHINA);
		Arrays.sort(mBrands, cmp);
	}

	// 获取手机参数信息数据
	public static List<PhoneModel> getModelList(JSONObject json) throws JSONException
	{
		List<PhoneModel> result = new ArrayList<PhoneModel>();
		// Map<String,String> map = null;
		JSONArray arrays = json.getJSONArray("models");
		for (int i = 0; i < arrays.length(); i++)
		{
			JSONObject modelJson = arrays.getJSONObject(i);
			result.add(getModelFromJSON(modelJson, false));
		}
		return result;
	}
	
	// 解析推荐包数据，返回有绑定手机信息的推荐包
	public static List<RecommendPackage> getRecommendPackageList(JSONArray arrays, boolean isMostIn)throws JSONException
	{
		List<RecommendPackage> list = new ArrayList<RecommendPackage>();
		for(int i = 0; i < arrays.length(); i++)
		{
			JSONObject modelJson = arrays.getJSONObject(i);
			RecommendPackage pack = new RecommendPackage();
			try
			{ 
				// 分别解析机型数据和套餐包数据
				pack.setmPhoneModel(getModelFromJSON(modelJson, true)); 
				pack.setmProduct(getProductFromJSON(modelJson, isMostIn));
				pack.setmId(modelJson.getString("REC_ID"));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			list.add(pack);
		}
		return list;
	}
	
	// 解析套餐靓号信息,返回无绑定手机信息的推荐包
	public static List<RecommendPackage> getProductList(JSONArray arrays, boolean isMostIn) throws JSONException
	{
		List<RecommendPackage> list = new ArrayList<RecommendPackage>();
		for(int i = 0; i < arrays.length(); i++)
		{
			RecommendPackage pack = new RecommendPackage();
			JSONObject modelJson = arrays.getJSONObject(i);
			pack.setmProduct(getProductFromJSON(modelJson, isMostIn));
			pack.setmId(modelJson.getString("REC_ID"));
			//pack.setmIsBindPhone(false);// 无绑定手机信息
			list.add(pack);
		}
		return list;
	}
	
	// 从JSON中解析手机信息数据
	private static PhoneModel getModelFromJSON(JSONObject modelJson, boolean isDZtj)
	{
		PhoneModel model = new PhoneModel();
		try
		{
			model.setId(StringUtil.isEmptyOrNull(modelJson.getString("ID"))? "无":modelJson.getString("ID"));
			model.setName(StringUtil.isEmptyOrNull(modelJson.getString("MODEL"))? "无":modelJson.getString("MODEL"));
			model.setBand(StringUtil.isEmptyOrNull(modelJson.getString("BRAND"))? "无":modelJson.getString("BRAND"));
			model.setRadio(StringUtil.isEmptyOrNull(modelJson.getString("RADIO"))? "无":modelJson.getString("RADIO"));
			model.setOp(StringUtil.isEmptyOrNull(modelJson.getString("OS"))? "无":modelJson.getString("OS"));
			// 20131113 web下推的优惠购机和店长推荐的字段不一样，坑！！！
			if(isDZtj)
			{
				model.setPicPath(StringUtil.isEmptyOrNull(modelJson.getString("IMAGE_URL"))? "无":modelJson.getString("IMAGE_URL"));
			}
			else
			{
				model.setPicPath(StringUtil.isEmptyOrNull(modelJson.getString("IMAGEURL"))? "无":modelJson.getString("IMAGEURL"));
			}
			
			model.setBigPic(StringUtil.isEmptyOrNull(modelJson.getString("BIGIMAGE"))? "无":modelJson.getString("BIGIMAGE"));
			model.setPrice(StringUtil.isEmptyOrNull(modelJson.getString("PRICE"))? "无":modelJson.getString("PRICE"));
			model.setScreenSize(StringUtil.isEmptyOrNull(modelJson.getString("SIZES"))? "无":modelJson.getString("SIZES"));
			//model.setIsRecommened(StringUtil.isEmptyOrNull(modelJson.getString("RECOMMENDED"))? "无":modelJson.getString("RECOMMENDED"));
			model.setCpu(StringUtil.isEmptyOrNull(modelJson.getString("CPU"))? "无":modelJson.getString("CPU"));
			model.setCoreNum(StringUtil.isEmptyOrNull(modelJson.getString("CORES"))? "无":modelJson.getString("CORES"));
			model.setRom(StringUtil.isEmptyOrNull(modelJson.getString("ROM"))? "无":modelJson.getString("ROM"));
			model.setAccess(StringUtil.isEmptyOrNull(modelJson.getString("ACCESSORIES"))? "无":modelJson.getString("ACCESSORIES"));
			model.setBattery(StringUtil.isEmptyOrNull(modelJson.getString("BATTERY"))? "无":modelJson.getString("BATTERY"));
			model.setSize(StringUtil.isEmptyOrNull(modelJson.getString("APPEARANCE"))? "无":modelJson.getString("APPEARANCE"));
			model.setVedioFormat(StringUtil.isEmptyOrNull(modelJson.getString("VIDEO"))? "无":modelJson.getString("VIDEO"));
			model.setJava(StringUtil.isEmptyOrNull(modelJson.getString("JAVA"))? "无":modelJson.getString("JAVA"));
			model.setBatteryStorage(StringUtil.isEmptyOrNull(modelJson.getString("CAPACITY"))? "无":modelJson.getString("CAPACITY"));
			model.setCamera1(StringUtil.isEmptyOrNull(modelJson.getString("MAINPIXEL"))? "无":modelJson.getString("MAINPIXEL"));
			model.setCamera2(StringUtil.isEmptyOrNull(modelJson.getString("VICEPIXEL"))? "无":modelJson.getString("VICEPIXEL"));
			model.setDataTrans(StringUtil.isEmptyOrNull(modelJson.getString("TRANSDATA"))? "无":modelJson.getString("TRANSDATA"));
			model.setCallTime(StringUtil.isEmptyOrNull(modelJson.getString("CALLTIME"))? "无":modelJson.getString("CALLTIME"));
			model.setScreenParam(StringUtil.isEmptyOrNull(modelJson.getString("SCPARAM"))? "无":modelJson.getString("SCPARAM"));
			model.setZoom(StringUtil.isEmptyOrNull(modelJson.getString("ZOOMMODEL"))? "无":modelJson.getString("ZOOMMODEL"));
			model.setColor(StringUtil.isEmptyOrNull(modelJson.getString("COLOR"))? "无":modelJson.getString("COLOR"));
			model.setExtendRom(StringUtil.isEmptyOrNull(modelJson.getString("EXTMEMORY"))? "无":modelJson.getString("EXTMEMORY"));
			model.setNet(StringUtil.isEmptyOrNull(modelJson.getString("NETWORK"))? "无":modelJson.getString("NETWORK"));
			model.setRam(StringUtil.isEmptyOrNull(modelJson.getString("RAM"))? "无":modelJson.getString("RAM"));
			model.setBattery(StringUtil.isEmptyOrNull(modelJson.getString("BATTERY"))? "无":modelJson.getString("BATTERY"));
			model.setMusicFormat(StringUtil.isEmptyOrNull(modelJson.getString("MUSIC"))? "无":modelJson.getString("MUSIC"));
			model.setWaitTime(StringUtil.isEmptyOrNull(modelJson.getString("STANDBY"))? "无":modelJson.getString("STANDBY"));
			model.setGps(StringUtil.isEmptyOrNull(modelJson.getString("GPS"))? "无":modelJson.getString("GPS"));
			model.setRecord(StringUtil.isEmptyOrNull(modelJson.getString("RECORDING"))? "无":modelJson.getString("RECORDING"));
		}
		catch (JSONException e1)
		{
			e1.printStackTrace();
			return model;
		}
		model.isParamLoaded = true;
		//model.isParamLoaded = LaunchActivity.isDebuge;
		return model;
	}
	
	// 从JSON中解析套餐包信息
	private static ProductInfo getProductFromJSON(JSONObject modelJson, boolean isMostIn)
	{
		ProductInfo product = new ProductInfo();
		try
		{
			// ID号
			product.setId(StringUtil.isEmptyOrNull(modelJson.getString("REC_ID"))? "无":modelJson.getString("REC_ID"));
			// 归属地  如：全国-福建省-福州
			product.setmAreaLong(StringUtil.isEmptyOrNull(modelJson.getString("AREA_LNAME"))? "无":modelJson.getString("AREA_LNAME"));
			// 归属地  如：福州
			product.setmAreaLong(StringUtil.isEmptyOrNull(modelJson.getString("AREA_SNAME"))? "无":modelJson.getString("AREA_SNAME"));
			// 描述，宣传口号
			product.setmDescribe(StringUtil.isEmptyOrNull(modelJson.getString("RECOMMEND_DESC"))? "无":modelJson.getString("RECOMMEND_DESC"));
			// 套餐价格
			product.setmPrice(StringUtil.isEmptyOrNull(modelJson.getString("PRICE"))? "无":modelJson.getString("PRICE"));
		
			// 合约ID
			product.setmContractID(StringUtil.isEmptyOrNull(modelJson.getString("ACTIVITY_ID"))? "无":modelJson.getString("ACTIVITY_ID"));
			// 合约编码
			product.setmContractCode(StringUtil.isEmptyOrNull(modelJson.getString("ACTIVITY_CODE"))? "无":modelJson.getString("ACTIVITY_CODE"));
			// 合约名称
			product.setmContractName(StringUtil.isEmptyOrNull(modelJson.getString("ACTIVITY_NAME"))? "无":modelJson.getString("ACTIVITY_NAME"));
			// 合约期限
			product.setmContractDate(StringUtil.isEmptyOrNull(modelJson.getString("ACTPROTPER"))? "无":modelJson.getString("ACTPROTPER"));
			
			// 套餐ID
			product.setmProductID(StringUtil.isEmptyOrNull(modelJson.getString("PRO_ID"))? "无":modelJson.getString("PRO_ID"));
			// 套餐编码
			product.setmProductCode(StringUtil.isEmptyOrNull(modelJson.getString("PRO_CODE"))? "无":modelJson.getString("PRO_CODE"));
			// 套餐名称
			product.setmProductName(StringUtil.isEmptyOrNull(modelJson.getString("PRO_NAME"))? "无":modelJson.getString("PRO_NAME"));
			// 套餐档次
			//product.setmProductPrice(modelJson.getString("PRODUCT_PRICE"));
			// 套餐计划
			//product.setmProductType(modelJson.getString("PRONAME_TYPE"));
			
			// 国内拨打分钟数
			product.setmCallTime(StringUtil.isEmptyOrNull(modelJson.getString("PAY_INTERNAL_SECONDS"))? "--":modelJson.getString("PAY_INTERNAL_SECONDS"));
			// 国内流量
			product.setmFlow1(StringUtil.isEmptyOrNull(modelJson.getString("PAY_INTERNAL_OUTFLOW"))? "--":modelJson.getString("PAY_INTERNAL_OUTFLOW"));
			// 国内短信发送条数
			product.setmMsgCount(StringUtil.isEmptyOrNull(modelJson.getString("PAY_MSG_RECORDS"))? "--":modelJson.getString("PAY_MSG_RECORDS"));
			// 接听免费分钟数
			product.setmFree(StringUtil.isEmptyOrNull(modelJson.getString("PAY_RECEIVE_FREE"))? "--":modelJson.getString("PAY_RECEIVE_FREE"));
			// 国内拨打费用
			product.setmCallFee(StringUtil.isEmptyOrNull(modelJson.getString("PAY_INTERNAL_SEND_PRICE"))? "--":modelJson.getString("PAY_INTERNAL_SEND_PRICE"));
			// 国内流量费用
			product.setmFlow2(StringUtil.isEmptyOrNull(modelJson.getString("PAY_VISIBLE_SEND"))? "--":modelJson.getString("PAY_INTERNAL_OUTFLOW"));
			// 视频通话分钟数
			product.setmVideoCall(StringUtil.isEmptyOrNull(modelJson.getString("PAY_VISIBLE_SEND"))? "--":modelJson.getString("PAY_VISIBLE_SEND"));
			// 其他业务
			product.setmOther(StringUtil.isEmptyOrNull(modelJson.getString("PAY_OTHER_BUSI"))? "无":modelJson.getString("PAY_OTHER_BUSI"));
			// 是否有赠品 1 有 0 没有
			product.setmIsGift(StringUtil.isEmptyOrNull(modelJson.getString("IS_GIFT"))? "无":modelJson.getString("IS_GIFT"));
			// 赠品说明
			product.setmGiftInfo(StringUtil.isEmptyOrNull(modelJson.getString("GIFG_INFO"))? "无":modelJson.getString("GIFG_INFO"));
			// 赠送1
			product.setmGift1(StringUtil.isEmptyOrNull(modelJson.getString("PAY_GIFT1"))? "无":modelJson.getString("PAY_GIFT1"));
			// 赠送2
			product.setmGift2(StringUtil.isEmptyOrNull(modelJson.getString("PAY_GIFT2"))? "无":modelJson.getString("PAY_GIFT2"));
			// 赠送3
			product.setmGift3(StringUtil.isEmptyOrNull(modelJson.getString("PAY_GIFT3"))? "无":modelJson.getString("PAY_GIFT3"));
			// 赠送4
			product.setmGift4(StringUtil.isEmptyOrNull(modelJson.getString("PAY_GIFT4"))? "无":modelJson.getString("PAY_GIFT4"));
			// 赠送5
			product.setmGift5(StringUtil.isEmptyOrNull(modelJson.getString("PAY_GIFT5"))? "无":modelJson.getString("PAY_GIFT5"));
			
			// 活动类型
			product.setmActivityType(StringUtil.isEmptyOrNull(modelJson.getString("ACT_TYPE"))? "无":modelJson.getString("ACT_TYPE"));
			// 活动详情
			product.setmActivityDetails(StringUtil.isEmptyOrNull(modelJson.getString("ACTIVITY_DESC"))? "无":modelJson.getString("ACTIVITY_DESC"));
			// 活动 -- 1 热销 3 特惠  5 新品
			product.setmMarktingTag(StringUtil.isEmptyOrNull(modelJson.getString("MARKETING_TAG"))? "无":modelJson.getString("MARKETING_TAG"));
			
			// 多张图片  isMostIn 20131203 web要求改
			if(StringUtil.isEmptyOrNull(modelJson.getString("IMAGE_URL")) && isMostIn)
			{
				product.setmPicPath(StringUtil.isEmptyOrNull(modelJson.getString("IMAGEURL"))? "无":modelJson.getString("IMAGEURL"));
			}
			else
			{
				product.setmPicPath(StringUtil.isEmptyOrNull(modelJson.getString("IMAGE_URL"))? "无":modelJson.getString("IMAGE_URL"));
			}
			// 单张图片
			product.setmBigPic(StringUtil.isEmptyOrNull(modelJson.getString("BIGIMAGE"))? "无":modelJson.getString("BIGIMAGE"));
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			return product;
		}
		return product;
	}

	// 获取品牌
	public static String[] getBrands(JSONObject json) throws JSONException
	{
		String[] result = null;
		JSONArray arrays = json.getJSONArray("brands");
		result = new String[arrays.length()];
		for (int i = 0; i < arrays.length(); i++)
		{
			JSONObject brandJson = arrays.getJSONObject(i);
			result[i] = brandJson.getString("NAME");
		} 
		return result;
	} 

	private static Map<String, Bitmap>	mapPool	= new HashMap<String, Bitmap>();

	public static Bitmap getCacheBitmap(String picPath)
	{ 
		Bitmap bmap = mapPool.get(picPath);
		if (bmap == null)
		{
			bmap = PictureShowUtils.decodeFile(picPath, 300, 300);
			mapPool.put(picPath, bmap);
		}
		return bmap;
	}

	public static String[] getIndicator(String json) throws JSONException
	{ 
		if(StringUtil.isEmptyOrNull(json))
		{
			
			return null;
		}
		JSONArray jsonArr = new JSONArray(json);
		mIndicator = new String[jsonArr.length()];
		for(int i = 0; i < jsonArr.length(); i++)
		{
			mIndicator[i] = jsonArr.getString(i);
			System.out.println("obj"+ i + " :" + mIndicator[i].toString());
		} 
		return mIndicator;
	}
}
