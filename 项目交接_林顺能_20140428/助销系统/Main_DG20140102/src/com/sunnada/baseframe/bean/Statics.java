package com.sunnada.baseframe.bean;

import java.util.Map;

import android.os.Environment;

public class Statics
{
	// 本地基础路径
	public static final String 		PATH_BASE 				= Environment.getExternalStorageDirectory() + "/sunnada";
	// 图片存储路径
	public static final String 		PATH_IMAGE 				= PATH_BASE + "/images";
	// APK存储路径
	public static final String 		PATH_APK 				= PATH_BASE + "/apks";
	// M3存储路径
	public static final String 		PATH_M3 				= PATH_BASE + "/m3";
	// 机型、套餐等图片存储路径
	public static final String 		PATH_PIC 				= PATH_BASE + "/mobile/";
	// 配置文件存储路径
	public static final String 		PATH_CONFIG 			= PATH_BASE + "/config";
	// 安装APK文件的请求码
	public static final int			INSTALL_APK_REQUEST		= 0x01;
	// SharedPreferences 配置文件
	public static final String 		CONFIG					= "config";
	public static boolean 			IS_DEBUG				= false;
    public static byte[] 			INIT_OK_LOCK 			= new byte[0];
	
	public static final String		KEY_USER_NAME			= "user_name";		// 员工姓名
	public static final String		KEY_USER_TEL			= "user_tel";		// 员工电话
	public static final String		KEY_USER_NO				= "user_no";		// 员工工号
	
	public static final String		CHECK_SEQ				= "check_seq";
	public static final String		CHECK_MONEY				= "check_money";
	public static final String		CHECK_TIME				= "check_time";
	
	public static final String      CHECK_TITLE1            = "check_title1";
	public static final String      CHECK_TITLE2            = "check_title2";
	public static final String      CHECK_TITLE3            = "check_title3";
	
	public static final String      CHECK_DATA1             = "check_data1";
	public static final String      CHECK_DATA2             = "check_data2";
	public static final String      CHECK_DATA3             = "check_data3";
	public static final String      CHECK_DATA4             = "check_data4";
	public static final	String		IS_AUTO_LOGIN			= "is_auto_login";
	
	public static String			STR_SKIN_TYPE			= "skin_type";	
	public static int				SKIN_TYPE				= 0;
	public static final int 		SKIN_BLUE				= 0;
	public static final int 		SKIN_ORANGE				= 1;
	public static boolean			HAD_PWD					= true;
	public static int				MODIFY_TYPE				= -1;						   // 密码修改类型，1 初始化密码，2 重置交易密码
	public static boolean 			IS_FORCED_UPDATE		= false;
	
	// 用户选择修改密码
	public static final int 		MSG_MODIFY_PSWD 						= 0x0001;
	public static final int			MSG_MODIFY_PSWD_CANCEL					= 0x0002;
	public static final int			MSG_GET_PSWD_SCENE_OK					= 0x0003;
	
	public static final int			MSG_UPDATE								= 0x0004;
	public static final int			MSG_UPDATE_CANCEL						= 0x0005;
	public static final int			MSG_UPDATE_FAILED						= 0x0006;
	public static final int			MSG_UPDATING							= 0x0007;
	public static final int			MSG_UPDATE_OK							= 0x0008;
	public static final int			MSG_SECRET_CODE							= 0x0009;		// 暗码
	public static final int			MSG_MODIFY_PSWD_SUCCESS					= 0x0010;
	public static final int			MSG_RESET_PSWD							= 0x0011;
	public static final int			MSG_PWD_FAIL							= 0x0012;		// 交易密码错误
	public static final int			MSG_VERIFY_OK							= 0x0013;		// 校验验证码OK
	public static final int			MSG_INSTALL_OK							= 0x0014;		// 软件安装成功
	public static final int			MSG_INSTALL_FAILED						= 0x0015;		// 软件安装失败
	
	public static final int			MSG_GET_ELEC_FAIL						= 0x0102;		// 获取卡密失败
	public static final int			MSG_RECHARGE_FAIL						= 0x0103;		// 充值失败
	public static final int			MSG_RECHARGE_OK							= 0x0104;		// 充值成功
	public static final int			MSG_PRINT_LAST_EPAY						= 0x0105;
	public static final int			MSG_PRINT_LAST_EPAY_CANCEL				= 0x0106;
	public static final int			MSG_PRINT								= 0x0107;		// 打印
	public static final int			MSG_PRINT_CANCEL						= 0x0108;		// 打印取消
	public static final int			MSG_PRINT_OK							= 0x0109;
	public static final int			MSG_REPRINT								= 0x010A;
	public static final int			MSG_REPRINT_CANCEL						= 0x010B;
	public static final int			MSG_SEND_NOTIFY							= 0x010C;		// 发送短信通知
	public static final int			MSG_NOTIFY								= 0x010D;
	public static final int			MSG_NOTIFY_CANCEL						= 0x010E;
	public static final int			MSG_NOTIFY_OK							= 0x010F;
	public static final int			MSG_NOTIFY_FAIL							= 0x0110;
	public static final int			MSG_FINISH								= 0x0111;       // 结束业务
	
	public static final int			MSG_CHECK_PRINTER						= 0x0120;		// 检测打印机
	public static final int 		MSG_CHECK_PRINTER_CANCEL				= 0x0121;
	//public static final int		MSG_CHECK_PRINT_STATUS					= 0x0122;
	//public static final int 		MSG_CHECK_PRINT_STATUS_CANCEL			= 0x0123;
	public static final int			MSG_RECHARGE_OK_CHECK					= 0x0124;
	public static final int			MSG_RECHARGE_FAIL_PRINT					= 0x0125;
	public static final int			MSG_RECHARGE_FAIL_PRINT_CANCEL			= 0x0126;
	public static final int			MSG_RECHARGE_REPORT_PRINT_OK			= 0x0127;
	public static final int			MSG_RECHARGE_REPORT_PRINT_FAIL			= 0x0128;
	public static final int			MSG_RECHARGE_FAIL_CONNECT_BLUETOOTH		= 0x0112;		// 连接蓝牙
	public static final int			MSG_RECHARGE_FAIL_CONNECT_DONE			= 0x0113;		// 连接蓝牙完成
	public static final int			MSG_RECHARGE_OK_CONNECT_BLUETOOTH		= 0x0114;		// 连接蓝牙
	public static final int			MSG_RECHARGE_OK_CONNECT_DONE			= 0x0115;		// 连接蓝牙完成
	public static final int			MSG_BUYCARD_OK_CONNECT_BLUETOOTH		= 0x0116;		// 连接蓝牙
	public static final int			MSG_BUYCARD_OK_CONNECT_DONE				= 0x0117;		// 连接蓝牙完成
	
	public static final int			MSG_BUYCARD_OK							= 0x0131;
	public static final int			MSG_BUYCARD_CANCEL						= 0x0132;
	public static final int			MSG_BUYCARD_OK_REPORT					= 0x0133;
	public static final int			MSG_BUYCARD_OK_REPORT_OK				= 0x0134;
	public static final int			MSG_BUYCARD_OK_REPORT_FAIL				= 0x0135;
	
	public static final int			MSG_BUSI_CHECK_PREV						= 0x0201;		// 对账上一页
	public static final int			MSG_BUSI_CHECK_NEXT						= 0x0202;		// 对账下一页
	
	public static final int			MSG_WRITE_CARD							= 0x0203;		// 写卡
	public static final int			MSG_CONNECT_BLUETOOTH		            = 0x0204;		// 连接蓝牙
	public static final int			MSG_CONNECT_DONE			        	= 0x0205;       // 连接蓝牙完成
	public static final int			MSG_CONNECT_BLUETOOTH_CANCEL	        = 0x0206;		// 取消蓝牙连接
	public static final int			MSG_READ_CARD					        = 0x0207;		// 读卡
	public static final int			MSG_BUYCARD_REPORT						= 0x0208;		// 购卡上报
	
	// 发票打印
	public static final int         MSG_INVOICE_PRINT                       = 0x0301;
	public static final int         MSG_GOTO_INVOICE_VALIDATE               = 0x0302;
	
	public static String DZK_Type[][] = 
	{
		{"1", "20"},
		{"2", "30"},
		{"3", "50"},
		{"4", "100"},
		{"5", "300"},
		{"6", "500"}
	};
	
	// Spinner 对账
	public static String SELFSERVICE_CHECK_DAILY[][] = 
	{
		{"0", "日对账"},
		{"1", "月对账"},
		{"2", "历史对账"}
	};
	
	public static final String 		CHECK_CATEGORY_SEQ		= "check_seq";
	public static final String 		CHECK_CATEGORY_CODE		= "check_code";
	public static final String		CHECK_CATEGORY_DETAIL	= "check_detail";
	public static final String		CHECK_CATEGORY_TITLE	= "check_title";
	public static final String		CHECK_CATEGORY_CONTENT	= "check_content";
	public static final String		EXIT_TIP                = "确定要退出并放弃该次业务吗？";		// 退出业务提示信息
	
	public static final String		BLUETOOTH_NAME			= "name";
	public static final String		BLUETOOTH_MAC			= "mac";
	public static final String		BLUETOOTH_STATUS		= "status";
	public static Map<String, String> mCurMap;													// 当前连接的蓝牙mac地址
}
