package com.sunnada.baseframe.bean;

// 异常交易记录bean
public class ErrorInfoBean 
{
	public String no;
	public String reason;
	public String crc;
	public String state ;
	
	// 获取异常流水
	public String getNo() 
	{
		return no;
	}
	
	// 设置异常流水
	public void setNo(String no) 
	{
		this.no = no;
	}
	
	// 获取异常原因
	public String getReason() 
	{
		return reason;
	}
	
	// 设置异常原因
	public void setReason(String reason) 
	{
		this.reason = reason;
	}
	
	// 获取异常CRC
	public String getCrc() 
	{
		return crc;
	}
	
	// 设置异常CRC
	public void setCrc(String crc) 
	{
		this.crc = crc;
	}
	
	// 获取异常状态
	public String getState() 
	{
		return state;
	}
	
	// 设置异常状态
	public void setState(String state) 
	{
		this.state = state;
	}
}
