package com.sunnada.baseframe.bean;


public class ReturnEnum
{
	public static final int SUCCESS = 0;			// 成功
	public static final int FAIL = -1;				// 失败
	public static final int SEND_TIMEOUT = -2;		// 发送超时
	public static final int RECIEVE_TIMEOUT = -3;	// 接收超时
	
}
