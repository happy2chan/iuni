package com.sunnada.baseframe.signature;

import java.util.Stack;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class DrawView extends View 
{
	private static final String 	TAG 			= "DrawView";
	//private static final int 		INTERVAL 		= 500;
	private Paint 					paint;
	private Path 					path;
	private Stack<Path> 			paths;
	private float 					x;
	private float 					y;
	//private long 					t;
	private boolean 				isWriting 		= false;

	public DrawView(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		initPaint();
		Log.v(TAG, "Constructor");
	}

	public DrawView(Context context) 
	{
		super(context);
		initPaint();
	}

	private void initPaint() 
	{
		paint = new Paint();
		paint.setColor(Color.YELLOW);
		paint.setAntiAlias(true);
		paint.setStrokeWidth(6.0f);
		paint.setStyle(Paint.Style.STROKE);
		//paint.setStrokeJoin(Paint.Join.ROUND);
		//paint.setStrokeCap(Paint.Cap.ROUND);
		paths = new Stack<Path>();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) 
	{
		switch (event.getAction()) 
		{
		case MotionEvent.ACTION_DOWN:
			// if (t == 0) {
			// Log.v(TAG, "first click:" + t);
			// t = System.currentTimeMillis();
			// } else {
			// if (System.currentTimeMillis() - t < INTERVAL) {
			// clearLastPath();
			// return true;
			// }
			// Log.v(TAG, "second click:" + (System.currentTimeMillis() - t));
			// t = 0l;
			// }
			path = new Path();
			paths.push(path);
			path.moveTo(x = event.getX(), y = event.getY());
			break;
			
		case MotionEvent.ACTION_MOVE:
			path.quadTo(x, y, (x+event.getX())/2, (y+event.getY())/2);
			x = event.getX();
			y = event.getY();
			invalidate();
			isWriting = true;
			Log.d("xxxx", "writing status: " + isWriting);
			break;
			
		case MotionEvent.ACTION_UP:
			// if (isWriting)
			// paths.push(path);
			if(path.isEmpty())
			{
				paths.pop();
			}
			Log.v(TAG, "stack num = " + paths.size());
			break;
			
		case MotionEvent.ACTION_OUTSIDE:
			Log.v(TAG, "outside");
			break;
			
		case MotionEvent.ACTION_CANCEL:
			Log.v(TAG, "cancel");
			break;
			
		default:
			break;
		}
		// if (event.getAction() == MotionEvent.ACTION_DOWN) {
		//
		// lp.set((int) event.getX(), (int) event.getY());
		// // Log.v(TAG, "ACTION_DOWN:" + lp.toString());
		// }
		// if (event.getAction() == MotionEvent.ACTION_MOVE) {
		// // Log.v(TAG, "ACTION_MOVE:" + np.toString());
		// // invalidate(Math.min(lp.x, np.x), Math.min(lp.y, np.y),
		// // Math.max(lp.x, np.x), Math.max(lp.y, np.y));
		// if (path == null || path.isEmpty()) {
		// path = new Path();
		// paths.add(path);
		// path.moveTo(lp.x, lp.y);
		// } else {
		// path.quadTo(lp.x, lp.y, event.getX(), event.getY());
		// }
		//
		// invalidate();
		// }
		// if (event.getAction() == MotionEvent.ACTION_UP) {
		// // Log.v(TAG, "ACTION_UP:" + lp.toString());
		//
		// }
		return super.onTouchEvent(event);
	}

	public void clearLastPath() 
	{
		Log.v(TAG, "����");
		if (paths.isEmpty()) {
			Log.v(TAG, "��ջ");
			return;
		}
		paths.pop();
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) 
	{
		// if (lp != null) {
		// // canvas.drawLine(lp.x, lp.y, np.x, np.y, paint);
		// // lp.set(np.x, np.y);
		// canvas.drawPath(path, paint);
		// }
		if (!paths.isEmpty()) 
		{
			for (Path path : paths) 
			{
				if (path.isEmpty()) 
				{
					Log.v(TAG, "��ͼ");
				}
				canvas.drawPath(path, paint);
			}
		}
		super.onDraw(canvas);
	}
}
