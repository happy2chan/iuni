package com.sunnada.baseframe.activity.busiaccept.robospice;

import java.util.ArrayList;
import java.util.List;

import com.sunnada.baseframe.activity.busiaccept.base.DES;
import com.sunnada.baseframe.activity.busiaccept.base.DeviceUtils;

import android.util.Log;

public class TelCheckRequest extends BaseRequest 
{
	private String 		mAgentId;
	private String 		mTelephone;
	private String 		mDesKey;
	private String		mFdType;		// �������� 01-�ֻ��ŷ��� 02-ICCID����

	public TelCheckRequest(String agentId, String telphone, String desKey, String fdType) 
	{
		this.mAgentId 		= agentId;
		this.mTelephone 	= telphone;
		this.mDesKey 		= desKey;
		this.mFdType		= fdType;
	}

	@Override
	protected String getMethod() 
	{
		return "checkTelphone";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "checkTelphoneResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		Log.e("", "agentId = " + mAgentId);
		Log.e("", "telephone = " + mTelephone);
		
		params.add(new Param(COMMUNICATION_ID, 	COMMUNICA_ID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(mAgentId, mDesKey)));
		params.add(new Param(USER_TEL, 			DES.encryptDES(mTelephone, mDesKey)));
		params.add(new Param(VERSION_CODE, 		"1."+DeviceUtils.getVersionCode()));
		params.add(new Param(VERSION_NAME, 		DeviceUtils.getVersionName()));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		// 20131016 
		params.add(new Param(FD_TYPE, 			mFdType));
		params.add(new Param(CHANNEL_ID, 		mChannelID));
		return params;
	}
}
