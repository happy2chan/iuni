package com.sunnada.baseframe.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.sunnada.baseframe.dialog.DialogUtil;

public class MealSupermarketActivity extends DgActivity implements OnClickListener, OnSeekBarChangeListener
{
	Context		m_context;
	TextView	tv_wbtime;
	SeekBar		bar_wbtime;
	TextView	tv_qqtime;
	SeekBar		bar_qqtime;
	TextView	tv_emailtime;
	SeekBar		bar_emailtime;
	TextView	tv_stocktime;
	SeekBar		bar_stocktime;
	TextView	tv_videotime;
	SeekBar		bar_videotime;
	TextView	tv_musictime;
	SeekBar		bar_musictime;
	TextView	tv_domesticcallstime;
	SeekBar		bar_domesticcallstime;
	TextView	tv_localconnectiontime;
	SeekBar		bar_localconnectiontime;

	ProgressBar	flowPro;
	TextView	tv_flow;
	ProgressBar	cityPro;
	TextView	tv_citysay;
	ProgressBar	longsayPro;
	TextView	tv_longsay;

	Button		btn_choice;
	Button		btn_result;

	int			totalFlow		= 0;
	int			crrwbFlow		= 0;
	int			crrqqFlow		= 0;
	int			crremailFlow	= 0;
	int			crrstockFlow	= 0;
	int			crrvideoFlow	= 0;
	int			crrmusicFlow	= 0;

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// 设置全屏模式
		setContentView(R.layout.mealupermarket);
		m_context = this;
		init();
	}

	public void init()
	{
		tv_wbtime = (TextView) findViewById(R.id.tv_wbtime);
		bar_wbtime = (SeekBar) this.findViewById(R.id.bar_wbtime);
		bar_wbtime.setOnSeekBarChangeListener(this);
		tv_qqtime = (TextView) findViewById(R.id.tv_qqtime);
		bar_qqtime = (SeekBar) findViewById(R.id.bar_qqtime);
		bar_qqtime.setOnSeekBarChangeListener(this);
		tv_emailtime = (TextView) findViewById(R.id.tv_emailtime);
		bar_emailtime = (SeekBar) findViewById(R.id.bar_emailtime);
		bar_emailtime.setOnSeekBarChangeListener(this);
		tv_stocktime = (TextView) findViewById(R.id.tv_stocktime);
		bar_stocktime = (SeekBar) findViewById(R.id.bar_stocktime);
		bar_stocktime.setOnSeekBarChangeListener(this);
		tv_videotime = (TextView) findViewById(R.id.tv_videotime);
		bar_videotime = (SeekBar) findViewById(R.id.bar_videotime);
		bar_videotime.setOnSeekBarChangeListener(this);
		tv_musictime = (TextView) findViewById(R.id.tv_musictime);
		bar_musictime = (SeekBar) findViewById(R.id.bar_musictime);
		bar_musictime.setOnSeekBarChangeListener(this);
		tv_domesticcallstime = (TextView) findViewById(R.id.tv_domesticcallstime);
		bar_domesticcallstime = (SeekBar) findViewById(R.id.bar_domesticcallstime);
		bar_domesticcallstime.setOnSeekBarChangeListener(this);
		tv_localconnectiontime = (TextView) findViewById(R.id.tv_localconnectiontime);
		bar_localconnectiontime = (SeekBar) findViewById(R.id.bar_localconnectiontime);
		bar_localconnectiontime.setOnSeekBarChangeListener(this);

		flowPro = (ProgressBar) findViewById(R.id.flowPro);
		tv_flow = (TextView) findViewById(R.id.tv_flow);
		cityPro = (ProgressBar) findViewById(R.id.cityPro);
		tv_citysay = (TextView) findViewById(R.id.tv_citysay);
		longsayPro = (ProgressBar) findViewById(R.id.longsayPro);
		tv_longsay = (TextView) findViewById(R.id.tv_longsay);

		btn_choice = (Button) findViewById(R.id.btn_choice);
		btn_choice.setOnClickListener(this);
		btn_result = (Button) findViewById(R.id.btn_result);
		btn_result.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.btn_choice:
				cleanChoice();
				break;
			case R.id.btn_result:
				float[] result = getArrResult();
				String name = "";
				if (result[0] <= 40)
				{
					if (result[1] <= result[2])
					{
						name = "B计划46元套餐";
					}
					else
					{
						name = "C计划46元套餐";
					}
				}
				else if (result[0] > 40 && result[0] <= 60)
				{
					if (result[1] <= result[2])
					{
						name = "B计划66元套餐";
					}
					else
					{
						name = "C计划66元套餐";
					}
				}
				else if (result[0] > 60 && result[0] <= 80)
				{
					if (result[1] <= result[2])
					{
						name = "B计划96元套餐";
					}
					else
					{
						name = "C计划96元套餐";
					}
				}
				else if (result[0] > 80 && result[0] <= 100)
				{
					name = "B计划126元套餐";
				}
				else if (result[0] > 100 && result[0] <= 120)
				{
					name = "B计划156元套餐";
				}
				else if (result[0] > 120 && result[0] <= 150)
				{
					name = "B计划186元套餐";
				}
				else if (result[0] > 150 && result[0] <= 300)
				{
					name = "A计划46元套餐";
				}
				else if (result[0] > 300 && result[0] <= 400)
				{
					name = "A计划126元套餐";
				}
				else if (result[0] > 400 && result[0] <= 500)
				{
					name = "A计划156元套餐";
				}
				else if (result[0] > 500 && result[0] <= 650)
				{
					name = "A计划186元套餐";
				}
				else if (result[0] > 650 && result[0] <= 750)
				{
					name = "A计划226元套餐";
				}
				else if (result[0] > 750 && result[0] <= 950)
				{
					name = "A计划286元套餐";
				}
				else if (result[0] > 950 && result[0] <= 1300)
				{
					name = "A计划386元套餐";
				}
				else if (result[0] > 1300 && result[0] <= 2000)
				{
					name = "A计划586元套餐";
				}
				else if (result[0] > 2000)
				{
					name = "A计划886元套餐";
				}
				else
				{
					name = "A计划886元套餐";
				}
				DialogUtil.showMessage("根据您的使用习惯,为您推荐" + name);
				break;

			default:
				break;
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
	{

		switch (seekBar.getId())
		{
			case R.id.bar_wbtime:
				float i = (float) (bar_wbtime.getProgress() / 60.0);
				tv_wbtime.setText((float) (Math.round(i * 10)) / 10 + "小时/天");
				crrwbFlow = (int) (((float) (Math.round(i * 10)) / 10) * 30 * 2048);
				totalFlow = crrwbFlow + crrqqFlow + crremailFlow + crrstockFlow + crrvideoFlow + crrmusicFlow;
				tv_flow.setText((float) (Math.round(((float) (totalFlow / 1048576.0)) * 10)) / 10 + "G/月");
				flowPro.setProgress(totalFlow);
				break;
				
			case R.id.bar_qqtime:
				float j = (float) (bar_qqtime.getProgress() / 60.0);
				tv_qqtime.setText((float) (Math.round(j * 10)) / 10 + "小时/天");
				crrqqFlow = (int) (((float) (Math.round(j * 10)) / 10) * 30 * 1024);
				totalFlow = crrwbFlow + crrqqFlow + crremailFlow + crrstockFlow + crrvideoFlow + crrmusicFlow;
				tv_flow.setText((float) (Math.round(((float) (totalFlow / 1048576.0)) * 10)) / 10 + "G/月");
				flowPro.setProgress(totalFlow);
				break;
				
			case R.id.bar_emailtime:
				tv_emailtime.setText(bar_emailtime.getProgress() + "封/天");
				crremailFlow = (int) (200 * bar_emailtime.getProgress() * 30);
				totalFlow = crrwbFlow + crrqqFlow + crremailFlow + crrstockFlow + crrvideoFlow + crrmusicFlow;
				tv_flow.setText((float) (Math.round(((float) (totalFlow / 1048576.0)) * 10)) / 10 + "G/月");
				flowPro.setProgress(totalFlow);
				break;
				
			case R.id.bar_stocktime:
				float k = (float) (bar_stocktime.getProgress() / 60.0);
				tv_stocktime.setText((float) (Math.round(k * 10)) / 10 + "小时/天");
				crrstockFlow = (int) (((float) (Math.round(k * 10)) / 10) * 30 * 500);
				totalFlow = crrwbFlow + crrqqFlow + crremailFlow + crrstockFlow + crrvideoFlow + crrmusicFlow;
				tv_flow.setText((float) (Math.round(((float) (totalFlow / 1048576.0)) * 10)) / 10 + "G/月");
				flowPro.setProgress(totalFlow);
				break;
				
			case R.id.bar_videotime:
				tv_videotime.setText(bar_videotime.getProgress() + "分钟/天");
				crrvideoFlow = (int) (bar_videotime.getProgress() * 30 * 2048);
				totalFlow = crrwbFlow + crrqqFlow + crremailFlow + crrstockFlow + crrvideoFlow + crrmusicFlow;
				tv_flow.setText((float) (Math.round(((float) (totalFlow / 1048576.0)) * 10)) / 10 + "G/月");
				flowPro.setProgress(totalFlow);
				break;
				
			case R.id.bar_musictime:
				tv_musictime.setText(bar_musictime.getProgress() + "分钟/天");
				crrmusicFlow = (int) (bar_musictime.getProgress() * 30 * 500);
				totalFlow = crrwbFlow + crrqqFlow + crremailFlow + crrstockFlow + crrvideoFlow + crrmusicFlow;
				tv_flow.setText((float) (Math.round(((float) (totalFlow / 1048576.0)) * 10)) / 10 + "G/月");
				flowPro.setProgress(totalFlow);
				break;
				
			case R.id.bar_domesticcallstime:
				tv_domesticcallstime.setText(bar_domesticcallstime.getProgress() + "分钟/天");
				tv_longsay.setText(bar_domesticcallstime.getProgress() * 30 + "分钟/月");
				longsayPro.setProgress(bar_domesticcallstime.getProgress() * 30);
				break;
				
			case R.id.bar_localconnectiontime:
				tv_localconnectiontime.setText(bar_localconnectiontime.getProgress() + "分钟/天");
				tv_citysay.setText(bar_localconnectiontime.getProgress() * 30 + "分钟/月");
				cityPro.setProgress(bar_localconnectiontime.getProgress() * 30);
				break;
			default:
				break;
		}
	}

	public float[] getArrResult()
	{
		float arr[] =
		{ 0, 0, 0 };
		arr[0] = (float) (Math.round(((float) (totalFlow / 1048576.0)) * 10)) / 10;
		arr[1] = bar_localconnectiontime.getProgress() * 30;
		arr[2] = bar_domesticcallstime.getProgress() * 30;

		return arr;
	}

	public void cleanChoice()
	{
		// totalFlow = 0;
		// crrwbFlow = 0;
		// crrqqFlow = 0;
		// crremailFlow = 0;
		// crrstockFlow = 0;
		// crrvideoFlow = 0;
		// crrmusicFlow = 0;

		bar_wbtime.setProgress(0);

		bar_qqtime.setProgress(0);

		bar_emailtime.setProgress(0);

		bar_stocktime.setProgress(0);

		bar_videotime.setProgress(0);

		bar_musictime.setProgress(0);

		bar_domesticcallstime.setProgress(0);
		bar_localconnectiontime.setProgress(0);

		// flowPro.setProgress(0);
		// cityPro.setProgress(0);
		// longsayPro.setProgress(0);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar)
	{

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar)
	{

	}

	@Override
	public void onInitSuccess()
	{
	}

	@Override
	public void onInitFail()
	{
	}
}