package com.sunnada.baseframe.activity.xhrw;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;

import com.sunnada.baseframe.signature.WritePadDialog;
import com.sunnada.baseframe.ui.MyCustomButton;

import com.sunnada.baseframe.business.PickNetBusiness;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;

public class SignatureLayout extends UILogic implements OnClickListener, OnTouchListener
{
	private WritePadDialog 				dialog; 						 // 签名Dialog
	private ImageView 					iv_sign 		         = null;
	private TextView 					tv_content 		         = null;

	private PickNetBusiness 			mPickNetBusiness 	     = null; // 选号入网业务类

	private StringBuffer 				sPrintBuffer 	         = null;
	private Button                      mBtnPrevious             = null; // 回退按钮       
	private LinearLayout                mLayBack;                        // 回退LinearLayout
	
	private MyCustomButton				moblie_next 	         = null; // 完成按钮
	private MyCustomButton 				btn_cancel 		         = null; // 退出按钮

	public SignatureLayout(NumberSelectActivity2 numberSelect)
	{
		super(numberSelect);
	}
	
	protected void onCreate(Bundle savedInstanceState) 
	{
		mNumberSelect.setContentView(R.layout.lay_signature);
		
		mPickNetBusiness = (PickNetBusiness) FrameActivity.mBaseBusiness;

		// 回退LinearLayout
		mLayBack = (LinearLayout)mNumberSelect.findViewById(R.id.layBack);
		mLayBack.setOnClickListener(this);
		
		// 回退按钮
		mBtnPrevious = (Button) mNumberSelect.findViewById(R.id.btn_previous);
		mBtnPrevious.setOnClickListener(this); 
		
		// 完成按钮
		moblie_next = (MyCustomButton) mNumberSelect.findViewById(R.id.moblie_next);
		moblie_next.setTextViewText1("完      成");
		moblie_next.setImageResource(R.drawable.btn_custom_check);
		moblie_next.setOnTouchListener(this);
		moblie_next.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				/*
				back(new ICallBack() 
				{
					@Override
					public void back() 
					{
						showPwdInputDialog();
					}
					
					@Override
					public void cancel() 
					{
					}

					@Override
					public void dismiss() 
					{
					}
				}, null, "确认提交吗?", "确定", "取消", true, false, false);
				*/
			}
		});

		// 退出按钮
		btn_cancel = (MyCustomButton) mNumberSelect.findViewById(R.id.btn_cancel);
		btn_cancel.setTextViewText1("退      出");
		btn_cancel.setImageResource(R.drawable.quit);
		btn_cancel.setOnTouchListener(this);
		btn_cancel.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				/*
				back(new ICallBack() 
				{
					@Override
					public void back() 
					{
						Message msg = Message.obtain();
						msg.what = FrameActivity.FINISH;
						FrameActivity.mHandler.sendMessage(msg);
					}
					
					@Override
					public void cancel() 
					{
					}

					@Override
					public void dismiss() 
					{
					}
				}, null, null, "确定", "取消", true, false, false);
				*/
			}
		});
		
		
		tv_content = (TextView) mNumberSelect.findViewById(R.id.tv_content);
		iv_sign = (ImageView) mNumberSelect.findViewById(R.id.iv_sign);
		iv_sign.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				if (dialog == null) 
				{
					dialog = new WritePadDialog(mNumberSelect);
					dialog.setOnBitmapGetListener(new WritePadDialog.OnBitmapGetListener() 
					{
						@Override
						public void onBitmapGet(Bitmap bitmap) 
						{
							//这里不能回收原图,否则重新签名时会出错
							iv_sign.setImageBitmap(bitmap);
							iv_sign.setBackgroundDrawable(mNumberSelect.getResources().getDrawable(R.drawable.bg_signatured));
						}
					});
				}
				dialog.show();
			}
		});
	}

	// 回到上一个Activity
	/*
	private void backToPrevious()
	{
		Message msg = Message.obtain();
		//bundle.putString("className",PickNetWorkNetAccessPermitsActivity.class.getName());

		msg.what = FrameActivity.DIRECTION_PREVIOUS;
		//msg.setData(bundle);
		FrameActivity.mHandler.sendMessage(msg);
	}
	*/
	
	@Override
	public void onClick(View view)
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(view.getId(), 500)) 
		{
			return;
		}
				
		switch (view.getId()) 
		{
		case R.id.btn_previous:
			//back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
			break;
		
		case R.id.layBack:
			//back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			/*
			back(new ICallBack() 
			{
				@Override
				public void back() 
				{
					backToPrevious();
				}
				
				@Override
				public void cancel() 
				{
				}

				@Override
				public void dismiss() 
				{
				}
			}, null, null, "确定", "取消", true, false, false);
			*/
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	protected void onResume() 
	{
		sPrintBuffer = new StringBuffer();
		sPrintBuffer.append("业务类型:3G选号入网\n")
					.append("[费用信息]\n")
					.append(mPickNetBusiness.feiyong)
				    .append("[用户信息]\n")
				    .append("客户名称:")
				    .append(mPickNetBusiness.username)
				    .append("\n")
				    .append("证件号码:")
				    .append(mPickNetBusiness.Identity)
				    .append("\n")
				    .append("证件地址:")
				    .append(mPickNetBusiness.address)
				    .append("\n");

		if(!StringUtil.isEmptyOrNull(mPickNetBusiness.packageName))
		{
			sPrintBuffer.append("[套餐名称]\n")
						.append(mPickNetBusiness.packageName);
		}
	
		if (!StringUtil.isEmptyOrNull(mPickNetBusiness.res)) 
		{
			sPrintBuffer.append("\n")
						.append("[套餐信息]\n")
						.append(mPickNetBusiness.res);
		}
	
		if (mPickNetBusiness.mSelectContractType != 0) 
		{
			sPrintBuffer.append("[活动类型]\n")
						.append(adjustContractTypeShow(mPickNetBusiness.mSelectContractType))
						.append("\n");
		}
		
		tv_content.setText(sPrintBuffer.toString()+"\n\n客户已阅读上述信息，并同意支付费用。");
		
		if (dialog != null) 
		{
			dialog.clear();
		}
	}

	/*
	private void checkPwd(final String pwd) 
	{
		mPickNetBusiness.password = pwd;
		DialogUtil.showProgress("正在提交,请稍候");

		new Thread() 
		{
			public void run() 
			{
				if(Statics.IS_DEBUG)
				{
					DialogUtil.closeProgress();
					back(new ICallBack()
					{
						@Override
						public void back() 
						{
							print();
						}

						@Override
						public void cancel() 
						{
							Message msg = Message.obtain();
							msg.what = FrameActivity.FINISH;
							FrameActivity.mHandler.sendMessage(msg);
						}

						@Override
						public void dismiss() 
						{
							Message msg = Message.obtain();
							msg.what = FrameActivity.FINISH;
							FrameActivity.mHandler.sendMessage(msg);
						}
						
					}, null, "开户成功,是否打印凭条?", "确定", "取消", true, true, true);
					return;
				}
				int ret = 0;
				if (StringUtil.isEmptyOrNull(pwd) || (pwd.length() != 6)) 
				{
					DialogUtil.closeProgress();
					/*
					back(new ICallBack()
					{
						@Override
						public void back() 
						{
							showPwdInputDialog();
						}

						@Override
						public void cancel() 
						{
						}

						@Override
						public void dismiss() 
						{
						}
					}, null, "请输入六位交易密码", "确定", null, true, false, false);
					return;
				}

				mNumberSelect.runOnUiThread(new Runnable() 
				{
					public void run() 
					{
						DialogUtil.showProgress("正在进行开户");
					}
				});

				ret = mPickNetBusiness.playOpenCard();

				if (ret != ReturnEnum.SUCCESS) 
				{
					mNumberSelect.runOnUiThread(new Runnable() 
					{
						public void run() 
						{
							DialogUtil.closeProgress();
							
							if(NumberSelectActivity.socketService.getLastknownErrCode() == 0xFFFFFFFC)
							{
								back(new ICallBack() 
								{
									@Override
									public void back() 
									{
										showPwdInputDialog();
									}
									
									@Override
									public void cancel() 
									{
										Message msg = Message.obtain();
										msg.what = FrameActivity.FINISH;
										FrameActivity.mHandler.sendMessage(msg);
									}

									@Override
									public void dismiss() 
									{
										Message msg = Message.obtain();
										msg.what = FrameActivity.FINISH;
										FrameActivity.mHandler.sendMessage(msg);
									}
								}, null, "交易密码错误，请重新输入交易密码。", "重试", "放弃", true, true, true);
							}
							else
							{
								back(new ICallBack() 
								{
									@Override
									public void back() 
									{
										Message msg = Message.obtain();
										msg.what = FrameActivity.FINISH;
										FrameActivity.mHandler.sendMessage(msg);
									}
									
									@Override
									public void cancel() 
									{
									}

									@Override
									public void dismiss() 
									{
									}
								}, null, "开户失败,将退出当前流程.失败原因:["
										+ mPickNetBusiness.getLastknownError() + "]",
								"确定", null, true, false, false);
							}
						}
					});
				} 
				else 
				{
					mNumberSelect.runOnUiThread(new Runnable() 
					{
						public void run() 
						{
							DialogUtil.closeProgress();
							back(new ICallBack() 
							{	
								@Override
								public void back() 
								{
									print();
								}
								
								@Override
								public void cancel() 
								{
									Message msg = Message.obtain();
									msg.what = FrameActivity.FINISH;
									FrameActivity.mHandler.sendMessage(msg);
								}

								@Override
								public void dismiss() 
								{
									Message msg = Message.obtain();
									msg.what = FrameActivity.FINISH;
									FrameActivity.mHandler.sendMessage(msg);
								}
							}, null, "开户成功,是否打印凭条?", "确定", "取消", true, true, true);
						}
					});

				}

			}
		}.start();

	}
	*/

	/*
	private void print() 
	{
		DialogUtil.showProgress("正在打印,请稍候");
		new Thread() 
		{
			public void run() 
			{
				try 
				{
					StringBuffer tempBuff = sPrintBuffer;
					if (!StringUtil.isEmptyOrNull(mPickNetBusiness.mContractContent)) 
					{
						tempBuff.append("[合约内容]\n"
								+ mPickNetBusiness.mContractContent+"\n");
					}
					else
					{
						tempBuff.append("\n");
					}
					
					// 开始打印
					mPickNetBusiness.print(tempBuff, new String(mPickNetBusiness.bllnum, "ASCII"));
				} 
				catch (Exception e) 
				{
					DialogUtil.closeProgress();
					e.printStackTrace();
				}
				
				if(NumberSelectActivity.equipmentService.isPrintSuccess())
				{
					showRePrintDialog();
				}
				else
				{
					back(new ICallBack() 
					{
						@Override
						public void back() 
						{
							showRePrintDialog();
						}
						
						@Override
						public void cancel() 
						{
						}

						@Override
						public void dismiss() 
						{
						}
					}, null, "打印失败.失败原因:[" + equipmentService.getPrintErrorMsg() + "]",
					"确定", null, true, false, false);
				}
			};
		}.start();
	}
	*/
	
	// 调整合约类型的显示
	public String adjustContractTypeShow(int contractType) 
	{
		String contractContent = "";
		switch (contractType) 
		{
		case 0:
			contractContent = "不参加活动";
				break;
			
			case 1:
			contractContent = "存费送费";
				break;
			
			case 2:
				break;
				
			case 3:
				break;
		}
		return contractContent;
	}
	
	// 显示输入密码对话框
	/*
	private void showPwdInputDialog()
	{
		DialogUtil.MsgBox("提示信息", R.layout.lay_pwd, "确定", "取消", null, new OnGetListener() 
		{
			@Override
			public void onGet(String value) 
			{
				checkPwd(value);
			}
		}, null);
	}
	*/
	
	// 显示是否重新打印选择框
	/*
	private void showRePrintDialog()
	{
		mNumberSelect.runOnUiThread(new Runnable() 
		{
			@Override
			public void run() 
			{
				DialogUtil.closeProgress();
				
				/*
				back(new ICallBack() 
				{
					@Override
					public void back() 
					{
						print();
					}
					
					@Override
					public void cancel() 
					{
						Message msg = Message.obtain();
						msg.what = FrameActivity.FINISH;
						FrameActivity.mHandler.sendMessage(msg);
					}

					@Override
					public void dismiss() 
					{
						Message msg = Message.obtain();
						msg.what = FrameActivity.FINISH;
						FrameActivity.mHandler.sendMessage(msg);
					}
				}, null, "是否重新打印凭条?", "确定", "取消", true, true, true);
			}
		});
	}
	*/

	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		return false;
	}

	@Override
	public void go() 
	{
	}
}
