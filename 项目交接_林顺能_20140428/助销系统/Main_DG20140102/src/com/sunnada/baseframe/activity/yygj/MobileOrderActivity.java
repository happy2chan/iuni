package com.sunnada.baseframe.activity.yygj;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.adapter.MyTelAdapter;
import com.sunnada.baseframe.bean.ContractData;
import com.sunnada.baseframe.bean.PhoneModel;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_0C;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.TelChoiceDialog;
import com.sunnada.baseframe.dialog.TelChoiceDialog.onChoiceGotListener;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;

public class MobileOrderActivity extends DgActivity implements OnClickListener, OnScrollListener
{
	private static final 			String	TITLE_STR	= "温馨提示";
	private Button 					mBtnPrevious;
	private View					mLayBack;
	private MyCustomButton			mBtnNext;
	private MyCustomButton			mBtnPre;
	private MyCustomButton			mBtnSelnum;
	private Business_0C				haoka;
	// private View 				mobileTypeView;		// 机型页面

	// 选择号码界面
	private View					mTelNumView;
	private ListView				mTelNumList;
	private MyTelAdapter			mTelNumAdapter; 
	private View					mMoreView;			// 加载更多页面
	private int						mLastItem;
	// private int count;
	//private View					dlg_telNum;
	//private LinearLayout			lay_money;
	//private LinearLayout			lay_beatelnum;
	//private LinearLayout			lay_numtel; 
	//private String				telNum				= "";
	//private Button				bt_close;
	//private Button				repselect;
	//private Button				btnok;
	//MyProgressDialog 				dlg_progress 		= null;
	private int						mFirstType			= 0;
	private String					mFirstStr			= "";
	private int						mSecondType			= 0;
	private String					mSecondStr			= "";
	private int						mThirdType			= 0;
	private TextView				mTvPhoneInfo		= null;
	private TextView				mTvPackageInfo		= null;
	private List<String>			mNumList;
	private List<String>			mFeeCList;
	private int 					mPayType 			= 1;
	private ContractData			mContract;
	private boolean					mIsScrollOver;		// 号码列表下拉滚动是否结束
	//private Animation				mAnimZoomOut;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.mobileorder);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.mobileorder_orange);
		}
		haoka = (Business_0C) FrameActivity.mBaseBusiness;
		// 初始化控件
		init();
		// 弹出号码筛选对话框
		showDialog();
		/*
		Bundle bundle = this.getIntent().getBundleExtra("_data");

		PhoneModel model = (PhoneModel) bundle.getSerializable("model");
		if (model != null)
		{
			tv_phone_info.setText(model.getNameDes() + "\n" + model.getPriceDes());
		}
		*/ 
	}

	// 主界面初始化
	public void init()
	{
		//mAnimZoomOut = AnimationUtils.loadAnimation(this, R.anim.zoom_out);
		//mAnimZoomOut.setStartOffset(0);
		mLayBack = findViewById(R.id.layBack);
		mLayBack.setOnClickListener(this);
		mBtnPrevious = (Button) this.findViewById(R.id.btn_previous);
		mBtnPrevious.setOnClickListener(this);
		
		mBtnNext = (MyCustomButton) this.findViewById(R.id.moblie_next);
		mBtnNext.setTextViewText1("确认");
		mBtnNext.setImageResource(R.drawable.btn_custom_check);
		mBtnNext.setOnTouchListener(this);
		mBtnNext.setOnClickListener(this);
		
		mBtnPre = (MyCustomButton) this.findViewById(R.id.moblie_pre);
		mBtnPre.setImageResource(R.drawable.btn_back);
		mBtnPre.setTextViewText1("返回");
		mBtnPre.setOnTouchListener(this);
		mBtnPre.setOnClickListener(this);
		
		mBtnSelnum = (MyCustomButton) this.findViewById(R.id.bt_selnum);
		mBtnSelnum.setTextViewText1("筛选号码");
		mBtnSelnum.setImageResource(R.drawable.ic_action_search);
		mBtnSelnum.setOnTouchListener(this);
		mBtnSelnum.setOnClickListener(this);
		
		mTvPhoneInfo = (TextView) findViewById(R.id.tv_phone_info);
		mTvPhoneInfo.setOnClickListener(this);
		mTvPackageInfo = (TextView) findViewById(R.id.tv_package_info);
		mTvPackageInfo.setOnClickListener(this);
	}

	// 弹出号码筛选对话框
	private void showDialog()
	{
		if(Statics.IS_DEBUG)
		{
			TelChoiceDialog bm = new TelChoiceDialog(MobileOrderActivity.this, new onChoiceGotListener()
			{
				@Override
				public void onChoiceGot(int[] arrSelcet, String[] arrStr)
				{
					loadConditionData(arrSelcet, arrStr);
				}
			});
			bm.show();
			return;
		}
		DialogUtil.showProgress("获取靓号规则中, 请稍候...");
		new Thread()
		{
			public void run()
			{
				boolean ret = haoka.bll0C05();
				if (!ret)
				{
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							DialogUtil.closeProgress();
							DialogUtil.MsgBox("温馨提示", haoka.getLastknownError(), "确定", 2, "", 0, 2, mHandler);
							//DialogUtil.showMessage(haoka.getLastknownError());
						}
					});
					return;
				}
				else
				{
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							numInit();
							// 获取靓号成功后弹出号码条件选择框
							TelChoiceDialog bm = new TelChoiceDialog(MobileOrderActivity.this, new onChoiceGotListener()
							{
								@Override
								public void onChoiceGot(int[] arrSelcet, String[] arrStr)
								{
									loadConditionData(arrSelcet, arrStr);
								}
							});
							bm.show();
							DialogUtil.closeProgress();
						}
					});
				}
			}
		}.start();
	}
	
	// 号码界面初始化
	public void numInit()
	{
		if (mTelNumView != null)
		{
			try
			{
				mTelNumList.removeAllViews();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		mMoreView = getLayoutInflater().inflate(R.layout.load, null);
		mMoreView.setVisibility(View.GONE);
		
		mTelNumList = (ListView) this.findViewById(R.id.telnumList);
		mTelNumList.setDivider(null);
		mTelNumList.setDrawingCacheEnabled(false);
		mTelNumList.setVerticalFadingEdgeEnabled(true);
		mTelNumList.setFadingEdgeLength(60);
		mTelNumList.setVerticalScrollBarEnabled(false);

		List<String> numberList = getNumberList();
		List<String> feeList = getFeeList(); 
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			mTelNumAdapter = new MyTelAdapter(this, numberList, feeList, R.layout.lay_telnum_listitem);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mTelNumAdapter = new MyTelAdapter(this, numberList, feeList, R.layout.lay_telnum_listitem_orange);
		}
		// telNumAdapter = new MyTelAdapter(this, list,
		// R.layout.lay_telnum_listitem,
		// new String[] { "telnumone", "telnumtwo", "telnumthree",
		// "telnumfour" }, new int[] { R.id.telnumone,
		// R.id.telnumtwo, R.id.telnumthree, R.id.telnumfour });

		mTelNumList.addFooterView(mMoreView); // 添加底部view，一定要在setAdapter之前添加，否则会报错。
		mTelNumList.setAdapter(mTelNumAdapter);
		mTelNumList.setOnScrollListener(this); // 设置listview的滚动事件
	}

	public String getChoseNum()
	{
		if (Statics.IS_DEBUG)
		{
			return "13000000000";
		}
		return mTelNumAdapter.getSelectedNumber();
	}
	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
	{
		// Log.i("TAG", "firstVisibleItem=" + firstVisibleItem
		// + "\nvisibleItemCount=" + visibleItemCount + "\ntotalItemCount"
		// + totalItemCount);
		mLastItem = firstVisibleItem + visibleItemCount - 1; // 减1是因为上面加了个addFooterView
		// System.out.println("totalItemCount=="+totalItemCount);
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState)
	{
		if(mIsScrollOver)
		{ 
			if (scrollState == OnScrollListener.SCROLL_STATE_IDLE && mLastItem == mTelNumAdapter.getCount())
			{
				mIsScrollOver = false;
				Log.i("TAG", "拉到最底部");
				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						try
						{
							Log.e("mobileOrder", "total :" + haoka.telnumTotal + " count:" + mTelNumAdapter.getCount());
							if(haoka.telnumTotal > 12)
							{
								if (mTelNumAdapter.getCount() * 2 <= haoka.telnumTotal)
								{
									runOnUiThread(new Runnable()
									{
										@Override
										public void run()
										{
											mMoreView.setVisibility(View.VISIBLE);
										}
									});
									boolean ret = haoka.bll0C03(mFirstType, mFirstStr, mSecondType, mSecondStr,
											mThirdType, mPayType, (short) (mTelNumAdapter.getCount() * 2 + 1), false, false, (short) mContract.proMoney);
									mHandler.sendEmptyMessage(1);
									if (!ret)
									{
										DialogUtil.MsgBox(TITLE_STR, "未查询到更多的号码！", "确定", new OnClickListener()
										{
											@Override
											public void onClick(View v)
											{
												mIsScrollOver = true;
											}
										}, "", null, null);
									}
									else
									{
										mHandler.sendEmptyMessage(0);
										Log.i("TAG", "加载更多数据");
									}
								}
								else
								{
									mHandler.sendEmptyMessage(1);
									DialogUtil.MsgBox(TITLE_STR, "未查询到更多的号码！", "确定",new OnClickListener()
									{
										@Override
										public void onClick(View v)
										{
											mIsScrollOver = true;
										}
									}, "", null, null);
								}
							}	
						}
						catch (Exception e)
						{
							e.printStackTrace();
							mHandler.sendEmptyMessage(1);
							DialogUtil.MsgBox(TITLE_STR, "未查询到更多的号码！", "确定", new OnClickListener()
							{
								@Override
								public void onClick(View v)
								{
									mIsScrollOver = true;
								}
							}, "", null, null);
						}
					}
				}).start();
			}
		}
	}

	// 声明Handler
	private Handler	mHandler	= new Handler()
	{
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				case 0:
					mIsScrollOver = true;
					loadMoreData();						 // 加载更多数据，这里可以使用异步加载
					/*
					if (telNumAdapter.getCount()> 300)
					{ 
					  	Toast.makeText(MobileOrderActivity.this, "没有更多数据！", 2000) .show();
					 	telNumList.removeFooterView(moreView);// 移除底部视图
					}
					*/
					
					break;
				
				// 未查询到号码
				case 1:
					mMoreView.setVisibility(View.GONE);
					break;
				
				// 获取靓号规则失败
				case 2:
					back();
					break;
					
				// 获取查询号码列表失败
				case 3:
					mTelNumAdapter.clear();
					mTelNumAdapter.notifyDataSetChanged();
					TelChoiceDialog bm = new TelChoiceDialog(MobileOrderActivity.this, new onChoiceGotListener()
					{
						@Override
						public void onChoiceGot(int[] arrSelcet, String[] arrStr)
						{
							loadConditionData(arrSelcet, arrStr);
						}
					});
					bm.show();
					break;
				default:
					break;
			}
		};
	};

	// 加载查询剔条件
	public void loadConditionData(int[] arr, String[] arrStr)
	{
		// arr { -1, -1, -1, -1 } 付费类型、号段、价格、靓号规则
		// arrStr { "", "" } 号段、靓号规则

		System.out.println("arr :" + Arrays.toString(arr));
		System.out.println("arrStr :" + Arrays.toString(arrStr));
		// 付费类型
		if (arr[0] != -1)
		{
			mPayType = arr[0];
		}

		// 号段
		if (arr[1] != -1 && arr[1] != 0)
		{
			mSecondType = 1;
			mSecondStr = arrStr[0];
		}
		else
		{
			mSecondType = 0;
			mSecondStr = "";
		}

		// 价格
		if (arr[2] == -1)
		{
			mThirdType = 0;
		}
		else
		{
			mThirdType = arr[2];
		}

		// 靓号规则
		if (arr[3] != -1 && arr[3] != 0)
		{
			mFirstType = 1;
			mFirstStr = arr[3] + "";
		}
		else
		{
			mFirstType = 0;
			mFirstStr = "";
		}

		if(Statics.IS_DEBUG)
		{
			runOnUiThread(new Runnable()
			{
				public void run()
				{
					numInit();
					loadNewData();
					mMoreView.setVisibility(View.GONE);
				}
			});
			return;
		}
		DialogUtil.showProgress("获取号码中, 请稍候...");
		new Thread()
		{
			public void run()
			{
				System.out.println("mFirstType :" + mFirstType + " mFirstStr :" + mFirstStr
						+ " mSecondType :" + mSecondType + " mSecondStr :" + mSecondStr + " mThirdType :" + mThirdType
						+ " mPayType :" + mPayType);
				boolean ret = haoka.bll0C03(mFirstType, mFirstStr, mSecondType, mSecondStr,
						mThirdType, mPayType, (short)1, false, true, (short) mContract.proMoney);

				if (!ret)
				{
					runOnUiThread(new Runnable() 
					{
						public void run()
						{
							DialogUtil.MsgBox("温馨提示", haoka.getLastknownError(), "确定", 3, "", 0, 3, mHandler);
							DialogUtil.closeProgress();
						}
					});
				}
				else
				{
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							loadNewData();
							mMoreView.setVisibility(View.GONE);
							DialogUtil.closeProgress();
						}
					});
				}
			}
		}.start();
	}

	// 加载新数据
	private void loadNewData()
	{
		/*
		list.clear(); count = telNumAdapter.getCount(); for (int i = count; i
		< count + 20; i++) { Map<String, Object> map = new HashMap<String,
		Object>(); map.put("telnumone", "18900000000"); map.put("telnumtwo",
		i + "18900000000"); map.put("telnumthree", "18900000000");
		map.put("telnumfour", "18900000000"); list.add(map); } count =
		list.size();
		*/
		mTelNumAdapter.clear();
		mIsScrollOver = true;
		mTelNumAdapter.addData(getNumberList(), getFeeList());
	}

	// 加载更多数据
	private void loadMoreData()
	{
		// count = telNumAdapter.getCount();
		mTelNumAdapter.addData(getNumberList(), getFeeList());
	}

	private List<String> getNumberList()
	{
		mNumList = new ArrayList<String>();
		if(Statics.IS_DEBUG)
		{
			mNumList.add("13000000000");
			mNumList.add("13000000001");
			mNumList.add("13000000002");
			mNumList.add("13000000003");
			mNumList.add("13000000004");
			mNumList.add("13000000005");
			mNumList.add("13000000006");
			mNumList.add("13000000007");
			mNumList.add("13000000008");
			mNumList.add("13000000009");
			mNumList.add("13000000010");
			mNumList.add("13000000011");
			mNumList.add("13000000012");
			mNumList.add("13000000013");
			mNumList.add("13000000014");
			mNumList.add("13000000015");
			mNumList.add("13000000016");
			mNumList.add("13000000017");
			mNumList.add("13000000018");
			mNumList.add("13000000019");
		}
		else
		{
			mNumList.addAll(haoka.resnum);
		}
		return mNumList;
	}

	private List<String> getFeeList()
	{
		mFeeCList = new ArrayList<String>();
		if(Statics.IS_DEBUG)
		{
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
			mFeeCList.add("50");
		}
		else
		{
			mFeeCList.addAll(haoka.telPrintBuffer);
		}
		return mFeeCList;
	}

	@Override
	public void onClick(View v)
	{
		final Bundle bundle = getIntent().getBundleExtra("_data");
		final Message msg = new Message();
		if (ButtonUtil.isFastDoubleClick(v.getId(), 500))
		{
			showErrorMsg("您的操作速度过快！");
			return;
		}
		switch (v.getId())
		{
			case R.id.moblie_next:
				//moblie_next.startAnimation(mAnimZoomOut);
				toNext(bundle, msg);
				break;
				
			case R.id.moblie_pre:
				back();
				break;

			case R.id.bt_selnum:// 查询
				TelChoiceDialog bm = new TelChoiceDialog(this, new onChoiceGotListener()
				{
					@Override
					public void onChoiceGot(int[] arrSelcet, String[] arrStr)
					{
						loadConditionData(arrSelcet, arrStr);
					}
				});
				bm.show();
				break;

			/*
			case R.id.tv_phone_info:
				new Thread()
				{
					@Override
					public void run()
					{
						haoka.bll0C16();// 通知服务器释放资源
					}
				}.start();
				bundle.putString("className", MobileIntroduceActivity.class.getName());
				msg.what = FrameActivity.DIRECTION_PREVIOUS;
				msg.setData(bundle);
				FrameActivity.mHandler.sendMessage(msg);
				break;

			case R.id.tv_package_info:
				bundle.putString("className", PackageSelectActivity.class.getName());

				msg.setData(bundle);
				FrameActivity.mHandler.sendMessage(msg);
				break;
			*/
			
			// 返回退出
			case R.id.btn_previous:
			case R.id.layBack:
				exit();
				break;

			default:
				break;
		}
	} 
	
	// 退出
	private void exit()
	{
		DialogUtil.MsgBox(TITLE_STR, Statics.EXIT_TIP, 
		"确定", new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				new Thread()
				{
					@Override
					public void run()
					{
						haoka.bll0C16();// 通知服务器释放资源
					}
				}.start();
				Message msg = new Message();
				msg.what = FrameActivity.FINISH;
				FrameActivity.mHandler.sendMessage(msg);
			}
		}, "取消", null , null);
	}

	// 下一步
	private void toNext(final Bundle bundle, final Message msg)
	{
		if (Statics.IS_DEBUG == false)
		{
			new Thread()
			{
				public void run()
				{
					if (getChoseNum() != null && !getChoseNum().equals(""))
					{
						boolean ret;
						if(StringUtil.isEmptyOrNull(haoka.slectenum))
						{
							haoka.slectenum = getChoseNum();
						}
						DialogUtil.showProgress("正在预占号码,请稍候...");
						if (!haoka.slectenum.equals(getChoseNum()))
						{
							Log.i("号码预占", "变更预占...");
							ret = haoka.bll0C04((byte) 0x05);// 变更预占
						}
						else
						{
							ret = haoka.bll0C04((byte) 0x01);// 预占
						}
						
						if (!ret)
						{
							haoka.slectenum = "";
							DialogUtil.closeProgress();
							runOnUiThread(new Runnable()
							{
								public void run()
								{
									DialogUtil.MsgBox("温馨提示", haoka.getLastknownError());
									return;
								}
							});
						}
						else
						{
							haoka.slectenum = getChoseNum();
							DialogUtil.closeProgress();
							// bundle = this.getIntent().getBundleExtra("_data");
							Message msg = new Message();
							bundle.putString("className", BusinessIdinfoActivity.class.getName());
							bundle.putString("num", getChoseNum());
							msg.what = FrameActivity.DIRECTION_NEXT;
							msg.setData(bundle);
							FrameActivity.mHandler.sendMessage(msg);
						} 
					}
					else
					{
						DialogUtil.closeProgress();
						runOnUiThread(new Runnable()
						{
							public void run()
							{
								DialogUtil.MsgBox("温馨提示", "请选择号码！");
							}
						});
					}
				};
			}.start();
		}
		else
		{
			DialogUtil.closeProgress();
			// bundle = this.getIntent().getBundleExtra("_data");
			bundle.putString("className", BusinessIdinfoActivity.class.getName());
			bundle.putString("num", getChoseNum());
			msg.what = FrameActivity.DIRECTION_NEXT;
			msg.setData(bundle);
			FrameActivity.mHandler.sendMessage(msg);
		}
	}

	// 返回退出
	private void back()
	{
		Bundle bundle = getIntent().getBundleExtra("_data");
		Message msg = new Message();
		bundle.putString("className", PackageSelectActivity.class.getName());
		msg.what = FrameActivity.DIRECTION_PREVIOUS;
		msg.setData(bundle);
		FrameActivity.mHandler.sendMessage(msg);
	}

	@Override
	public void onInitSuccess()
	{
	}

	@Override
	public void onInitFail()
	{
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			back();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		Bundle bundle = intent.getBundleExtra("_data");

		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS)
		{
			return;
		}
		getIntent().putExtra("_data", bundle);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		haoka.resnum.clear(); // 清空上次查询信息
		DialogUtil.init(this);

		Bundle bundle = getIntent().getBundleExtra("_data");

		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS)
		{
			return;
		}
		try
		{
			PhoneModel model = (PhoneModel) bundle.getSerializable("model");
			if (model != null)
			{
				mTvPhoneInfo.setText(model.getNameDes() + "\n" + model.getPriceDes());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		try 
		{
			mContract = (ContractData) bundle.getSerializable("package");
			if (mContract != null)
			{
				mTvPackageInfo.setText(mContract.getContractDes());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/*
	// 按钮点击监听
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		switch (v.getId())
		{ 
			case R.id.moblie_next:
				((MyCustomButton)v).onTouch(event);
				break;
			case R.id.bt_selnum:
				((MyCustomButton)v).onTouch(event);
				break;

			default:
				break;
		}
		return false;
	}
	*/
}
