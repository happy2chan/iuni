package com.sunnada.baseframe.activity.charge;

import android.os.Bundle;

public class BusiChargePayFix extends BusiChargePay 
{
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		mBusinessRecharge.mTranceType = 0x05;
		
		// 显示第一步
		showPage(0x01);
		// 刷新第一步
		refreshPage(0x01);
		
		// 检测异常交易
		checkErrorDeal(new String[] {"05"});
	}
}
