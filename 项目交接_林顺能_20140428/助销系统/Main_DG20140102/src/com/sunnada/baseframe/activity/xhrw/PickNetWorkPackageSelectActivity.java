package com.sunnada.baseframe.activity.xhrw;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.ConsumePlan;
import com.sunnada.baseframe.bean.PackageInfo;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.bean.contractBuff;
import com.sunnada.baseframe.business.PickNetBusiness;
import com.sunnada.baseframe.dialog.CallBackChildHome;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.DialogUtil.OnGetListener;
import com.sunnada.baseframe.dialog.ICallBack;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ARGBUtil;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;

public class PickNetWorkPackageSelectActivity extends DgActivity implements
		OnClickListener 
	{
	private LinearLayout                           mLayBack;                       // 回退LinearLayout
	private Button                                 mBtnPrevious;                   // 回退按钮        
    private TextView                               mTextView1;                     // 套餐计划 
	private ImageView                              mImageView1;                    // 导航条
    
	private LinearLayout                           lay_type                   = null;
	private LinearLayout                           lay_ending                 = null;
	private LinearLayout                           lay_fee                    = null;
	private LinearLayout                           lay_plan                   = null;

	private TextView                               tv_type                    = null;
	private TextView                               tv_ending                  = null;
	private TextView                               tv_fee                     = null;
	private TextView                               tv_plan                    = null;

	private RelativeLayout                         lay_cover                  = null;

	private ImageView                              iv_type                    = null;
	private ImageView                              iv_ending                  = null;
	private ImageView                              iv_fee                     = null;
	private ImageView                              iv_plan                    = null;

	private ListView                               lv_type                    = null;
	private ListView                               lv_ending                  = null;
	private ListView                               lv_fee                     = null;
	private ListView                               lv_plan                    = null;

	private MyCustomButton                         mBtnPre                    = null;//上一步
	private MyCustomButton                         mBtnNext                   = null;//下一步
	private TextView                               tv_selected_num            = null;
	private TextView                               tv_package_info            = null;
	
	private ArrayAdapter<Object>                   adapter                    = null;
	
	// 套餐信息TextView
	private TextView                               tv_call_time               = null;
	private TextView                               tv_flow1                   = null;
	private TextView                               tv_flow2                   = null;
	private TextView                               tv_video_call              = null;
	private TextView                               tv_other                   = null;
	private TextView                               tv_msg_count               = null;
	private TextView                               tv_free                    = null;
	private TextView                               tv_call_fee                = null;

	private TextView                               tv_label_local             = null;
	private PickNetBusiness                        mPickNetBusiness           = null;// 选号入网业务类

	private List<Integer>                          mConsumeLevels             = new ArrayList<Integer>(); // 消费档次
	private List<List<ConsumePlan>>                mConsumePlan               = new ArrayList<List<ConsumePlan>>();// 消费计划

	private int                                    mCurrentLevel              = 0;// 当前选中的消费档次
	private int                                    mCurrentPlanCode           = 0;// 当前选中的套餐计划编码
	private String                                 mCurrentPlanType           = "";// 当前选中的套餐计划类型
	private List<ConsumePlan>                      mCurrentPlanList           = new ArrayList<ConsumePlan>();
	private int                                    mCurrentContractType       = 0;// 当前选中的合约类型
	private int                                    mCurrentContractBuffCode   = -1;// 当前选中的合约期限编码
	    
	private String                                 mSelectedNum               = "";//被选中的号码
	private boolean                                mIsGetContractSuccess      = false;//是否成功获取合约
	private boolean                                mIsGetPackageDetailSuccess = false;//是否成功获取套餐详情
	
	// 消息处理器
	private Handler mHandler = new Handler() 
	{
		@Override
		public void handleMessage(Message msg) 
		{
			switch (msg.what) 
			{
			case 0:
				PackageInfo packageInfo = mPickNetBusiness.mPackageInfo;
				if (packageInfo != null) 
				{
					// 设置获取套餐详情状态为true
					mIsGetPackageDetailSuccess = true;
					
					tv_call_time.setText(packageInfo.getmCall());
					tv_flow1.setText(packageInfo.getmFlow());
					tv_flow2.setText(packageInfo.getmFeeFlow());
					tv_video_call.setText(packageInfo.getmFeeVedioCall());
					tv_other.setText(packageInfo.getmOther());
					tv_msg_count.setText(packageInfo.getmMsgCount());
					tv_free.setText(packageInfo.getmFreeIncomingCall());
					tv_call_fee.setText(packageInfo.getmFeeCall());
				}
				else
				{
					tv_call_time.setText("--");
					tv_flow1.setText("--");
					tv_flow2.setText("--");
					tv_video_call.setText("--");
					tv_other.setText("--");
					tv_msg_count.setText("--");
					tv_free.setText("--");
					tv_call_fee.setText("--");
				}
					

				if (mCurrentPlanType.equals("C")) 
				{
					tv_label_local.setText("本地语音拨打分钟数");
				} 
				else 
				{
					tv_label_local.setText("国内语音拨打分钟数");
				}
				break;

			case 1:
				break;

			case 2:
				mIsGetContractSuccess = true;
				showContract();
				break;
			}
			DialogUtil.closeProgress();
		}
	};

	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_picknetwork_package_select_orange);
		}
		else
		{
			setContentView(R.layout.lay_picknetwork_package_select);
		}

		mPickNetBusiness = (PickNetBusiness) FrameActivity.mBaseBusiness;
		// 初始化控件
		initView();
		// 初始化显示
		// initShow();
		// 注册监听
		setListenner();
	}

	@Override
	protected void onResume() 
	{
		super.onResume();
		Bundle bundle = getIntent().getBundleExtra("_data");
		if (bundle == null) 
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS) 
		{
			return;
		}
		
		String selectedNum = bundle.getString("num");
		mSelectedNum = selectedNum;
		tv_selected_num.setText(selectedNum);
		initData();
	}
	
	// 初始化控件
	private void initView() 
	{
		// 回退LinearLayout
		mLayBack = (LinearLayout)this.findViewById(R.id.layBack);
		// 回退按钮
		mBtnPrevious =(Button) findViewById(R.id.btn_previous);
		
		// 导航栏中的号码显示TextView
		tv_selected_num = (TextView) findViewById(R.id.tv_selected_num);
		
		lay_type = (LinearLayout) findViewById(R.id.lay_type);
		lay_ending = (LinearLayout) findViewById(R.id.lay_ending);
		lay_fee = (LinearLayout) findViewById(R.id.lay_fee);
		lay_plan = (LinearLayout) findViewById(R.id.lay_plan);

		// TextView(依次为消费档次、套餐计划、合约类型、合约期限)
		tv_type = (TextView) findViewById(R.id.tv_type);
		tv_ending = (TextView) findViewById(R.id.tv_ending);
		tv_fee = (TextView) findViewById(R.id.tv_fee);
		tv_plan = (TextView) findViewById(R.id.tv_plan);

		// 以下 ImageView和ListView的容器
		lay_cover = (RelativeLayout) findViewById(R.id.lay_cover);

		// ImageView(依次为消费档次、套餐计划、合约类型、合约期限)
		iv_type = (ImageView) findViewById(R.id.iv_type);
		iv_ending = (ImageView) findViewById(R.id.iv_ending);
		iv_fee = (ImageView) findViewById(R.id.iv_fee);
		iv_plan = (ImageView) findViewById(R.id.iv_plan);

		// ListView(依次为消费档次、套餐计划、合约类型、合约期限)
		lv_fee = (ListView) findViewById(R.id.lv_fee);
		lv_plan = (ListView) findViewById(R.id.lv_plan);
		lv_type = (ListView) findViewById(R.id.lv_type);
		lv_ending = (ListView) findViewById(R.id.lv_ending);
		
		// 界面中间显示合约内容的TextView
		tv_package_info = (TextView) findViewById(R.id.tv_package_info);

		// 套餐详情TextView
		tv_call_time = (TextView) findViewById(R.id.tv_call_time);
		tv_flow1 = (TextView) findViewById(R.id.tv_flow1);
		tv_flow2 = (TextView) findViewById(R.id.tv_flow2);
		tv_video_call = (TextView) findViewById(R.id.tv_video_call);
		tv_other = (TextView) findViewById(R.id.tv_other);
		tv_msg_count = (TextView) findViewById(R.id.tv_msg_count);
		tv_free = (TextView) findViewById(R.id.tv_free);
		tv_call_fee = (TextView) findViewById(R.id.tv_call_fee);
		tv_label_local = (TextView) findViewById(R.id.tv_label_local);
		
		// 上一步按钮
		mBtnPre = (MyCustomButton) findViewById(R.id.moblie_pre);
		mBtnPre.setTextViewText1("返回");
		mBtnPre.setImageResource(R.drawable.btn_back);
		mBtnPre.setOnTouchListener(this);
		
		// 下一步按钮
		mBtnNext = (MyCustomButton) findViewById(R.id.moblie_next);
		mBtnNext.setTextViewText1("确定");
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnNext.setImageResource(R.drawable.btn_custom_check_orange);
		}
		else
		{
			mBtnNext.setImageResource(R.drawable.btn_custom_check);
		}
		mBtnNext.setOnTouchListener(this);
	}

	public void initShow()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			// 设置背景图片
			RelativeLayout relativeBg = (RelativeLayout) findViewById(R.id.relative_bg);
			relativeBg.setBackgroundResource(R.drawable.bg_main_orange);
			// 主题
			mTextView1 = (TextView) findViewById(R.id.textView1);
			mTextView1.setTextColor(ARGBUtil.getArgb(2));
			// 导航条
			mImageView1 = (ImageView) findViewById(R.id.imageView1);
			mImageView1.setImageResource(R.drawable.step2_picknet_orange);
			
			lay_type.setBackgroundResource(R.drawable.title_type_orange);
			lay_fee.setBackgroundResource(R.drawable.title_fee_orange);
			lay_plan.setBackgroundResource(R.drawable.title_plan_orange);
		
		
			// ImageView(依次为消费档次、套餐计划、合约类型、合约期限)
			iv_type.setBackgroundResource(R.drawable.title_type_orange);
			iv_ending.setBackgroundResource(R.drawable.title_ending_orange);
			iv_fee.setBackgroundResource(R.drawable.title_fee_orange);
			iv_plan.setBackgroundResource(R.drawable.title_plan_orange);
			
			int[] ids = {R.id.tv1, R.id.textView5,
				     R.id.tv_label_local, R.id.textView3,
				     R.id.textView4, R.id.textView9,
				     R.id.textView6, R.id.textView7,
				     R.id.textView8, R.id.tv_otherbusi
				    };
		
			for(int i=0; i<ids.length; i++)
			{
				findViewById(ids[i]).setBackgroundColor(ARGBUtil.getArgb(9));
			}
			
			tv_call_time.setTextColor(ARGBUtil.getArgb(2));
			tv_flow1.setTextColor(ARGBUtil.getArgb(2));
			tv_flow2.setTextColor(ARGBUtil.getArgb(2));
			tv_video_call.setTextColor(ARGBUtil.getArgb(2));
			tv_other.setTextColor(ARGBUtil.getArgb(2));
			tv_msg_count.setTextColor(ARGBUtil.getArgb(2));
			tv_free.setTextColor(ARGBUtil.getArgb(2));
			tv_call_fee.setTextColor(ARGBUtil.getArgb(2));    
		}
	}
	
	// 注册监听
	private void setListenner() 
	{
		mBtnPrevious.setOnClickListener(this);
		mBtnPre.setOnClickListener(this);
		mBtnNext.setOnClickListener(this);
		mLayBack.setOnClickListener(this);
		
		lay_type.setOnClickListener(this);
		lay_ending.setOnClickListener(this);
		lay_fee.setOnClickListener(this);
		lay_plan.setOnClickListener(this);
		lay_cover.setOnClickListener(this);

		// 消费档次ListView点击事件
		lv_fee.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) 
			{
				// 重置部分数据
				resetDate(1);
				
				if(mConsumeLevels.size() > 0)
				{
					mCurrentLevel = mConsumeLevels.get(position);
					tv_fee.setText(mCurrentLevel + "元/月");
					
					if(mConsumePlan.size() > 0)
					{
						mCurrentPlanList = mConsumePlan.get(position);
						mCurrentPlanCode = mCurrentPlanList.get(0).getmCode();
						mCurrentPlanType = mCurrentPlanList.get(0).getmType();
						
						tv_plan.setText(mCurrentPlanList.get(0).getmType()+"套餐");
						showConstractInfo();
					}
					else
					{
						tv_plan.setText("无");
					}
				}
				else
				{
					tv_fee.setText("无");
				}
				lay_cover.setVisibility(View.GONE);
			}
		});

		// 套餐计划ListView点击事件
		lv_plan.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) 
			{
				// 重置部分数据
				resetDate(1);
				
				if(mCurrentPlanList.size() > 0)
				{
					mCurrentPlanCode = mCurrentPlanList.get(position).getmCode();
					mCurrentPlanType = mCurrentPlanList.get(position).getmType();
					
					tv_plan.setText(mCurrentPlanList.get(position).getmType()+"套餐");
					showConstractInfo();
				}
				else
				{
					tv_plan.setText("无");
				}
				lay_cover.setVisibility(View.GONE);
			}
		});

		// 合约类型ListView点击事件
		lv_type.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3)
			{
				// 重置部分数据
				resetDate(2);
				if(mPickNetBusiness.mContractType.get(position) != 0)
				{	
					// mPickNetBusiness.mContractflag为1为启用合约计划
					mPickNetBusiness.mContractflag = 0x01;
					// 设置当前合约类型
					mCurrentContractType = mPickNetBusiness.mContractType.get(position);
					// 设置当前被选中的合约类型
					mPickNetBusiness.mSelectContractType = mCurrentContractType;
					tv_type.setText(adjustContractTypeShow(mCurrentContractType));
					if(mCurrentContractType == 1)
					{
						// 存费送费
						showConstractBuffs();
					}
					else
					{
						// 需要输入串号的活动
						showIMEIDialog();
					}
				}
				else
				{
					// mPickNetBusiness.mContractflag为0为不启用合约计划
					mPickNetBusiness.mContractflag = 0x00;
					// 设置当前合约类型为0(不参加活动)
					mCurrentContractType = 0;
					// 被选中的合约类型为不参加活动
					mPickNetBusiness.mSelectContractType = 0;
					tv_type.setText("未参加活动");
				}
				lay_cover.setVisibility(View.GONE);
			}
		});

		// 合约期限ListView点击事件
		lv_ending.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3)
			{
				// 重置部分数据
				resetDate(3);
				if(!mPickNetBusiness.mContractBuffs.get(position).getContent().equals("未选择"))
				{
					mCurrentContractBuffCode = mPickNetBusiness.mContractBuffs.get(position).getID();
					
					tv_ending.setVisibility(View.VISIBLE);
					tv_ending.setEnabled(true);
					tv_ending.setText(mPickNetBusiness.mContractBuffs.get(position)
							.getContent());
					if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
					{
						lay_ending.setBackgroundDrawable(getResources().getDrawable(R.drawable.title_ending_orange));
					}
					else
					{
						lay_ending.setBackgroundDrawable(getResources().getDrawable(R.drawable.title_ending));
					}
					
					// 请求合约内容
					requestShowConstract();
				}
				else
				{
					tv_ending.setVisibility(View.GONE);
					tv_ending.setEnabled(false);
					lay_ending.setBackgroundDrawable(getResources().getDrawable(R.drawable.title_ending_noselected));
					mCurrentContractBuffCode = -1;
				}
				lay_cover.setVisibility(View.GONE);
			}
		});
	}

	// 初始化数据，在onResume时调用
	private void initData() 
	{
		if (Statics.IS_DEBUG) 
		{
		}
		else 
		{
			mConsumeLevels = mPickNetBusiness.mConsumeLevels;
			mConsumePlan = mPickNetBusiness.mConsumePlan;
		}

		if(mConsumePlan.size() > 0)
		{
			mCurrentLevel = mConsumeLevels.get(0);
			tv_fee.setText(mCurrentLevel+"元/月");
			
			
			if(mConsumePlan.size() > 0)
			{
				mCurrentPlanList = mConsumePlan.get(0);
				mCurrentPlanCode = ((ConsumePlan) (mConsumePlan.get(0).get(0))).getmCode();
				mCurrentPlanType = ((ConsumePlan) mCurrentPlanList.get(0)).getmType();
				
				tv_plan.setText(mCurrentPlanList.get(0).getmType()+"套餐");
			}
			else
			{
				tv_plan.setText("无");
			}
		}
		else
		{
			tv_fee.setText("无");
		}

		// 获取套餐详情、合约类型
		showConstractInfo();
	}

	@Override
	public void onClick(View v) 
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
		{
			return;
		}
				
		Bundle bundle = this.getIntent().getBundleExtra("_data");
		Message msg = Message.obtain();

		switch (v.getId()) 
		{
		case R.id.moblie_pre:
			backToPrevious();
			break;
			
		case R.id.moblie_next:
			if(Statics.IS_DEBUG)
			{
				bundle.putString("className",PickNetWorkBusinessIdinfoActivity.class.getName());
				msg.what = FrameActivity.DIRECTION_NEXT;
				msg.setData(bundle);
				FrameActivity.mHandler.sendMessage(msg);
				return;
			}
			
			if(!mIsGetPackageDetailSuccess)
			{
				back(null, null, "获取套餐详情失败,请重新获取!", "确定", null, false, false, false);
				return;
			}
			
			if(mCurrentContractType != 0)
			{
				if(mCurrentContractBuffCode == -1)
				{
					back(null, null, "请选择合约期限!", "确定", null, false, false, false);
					return;
				}
				
				if(StringUtil.isEmptyOrNull(mPickNetBusiness.mContractContent)||!mIsGetContractSuccess)
				{
					back(null, null, "获取合约内容失败,请重新获取!", "确定", null, false, false, false);
					return;
				}
			}
			
			bundle.putString("className",PickNetWorkBusinessIdinfoActivity.class.getName());
			bundle.putString("num",mSelectedNum);

			msg.what = FrameActivity.DIRECTION_NEXT;
			msg.setData(bundle);
			FrameActivity.mHandler.sendMessage(msg);
			break;

		case R.id.lay_fee:
			showList(0);
			break;

		case R.id.lay_plan:
			showList(1);
			break;

		case R.id.lay_type:
			showList(2);
			break;

		case R.id.lay_ending:
			showList(3);
			break;

		case R.id.lay_cover:
			lay_cover.setVisibility(View.GONE);
			break;
			
		case R.id.btn_previous:
			back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
			break;
		
		case R.id.layBack:
			back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
			break;

		default:
			break;
		}
	}

	// 根据index来显示列表(消费档次、套餐计划、合约类型、合约期限)
	private void showList(int index)
	{
		lay_cover.setVisibility(View.VISIBLE);
		List<Object> tempList = new ArrayList<Object>();

		switch (index) 
		{
		// 消费档次
		case 0:
			int lastFee = -1;
			for (int i = 0; i < mConsumeLevels.size(); i++) 
			{
				int fee = mConsumeLevels.get(i);
				if ((fee != lastFee) && !tempList.contains(fee)) 
				{
					tempList.add(fee);
					lastFee = fee;
				}
			}

			adapter = new ArrayAdapter<Object>(
					PickNetWorkPackageSelectActivity.this,
					R.layout.lay_package_list_item, R.id.tv_text, tempList);
			lv_fee.setAdapter(adapter);

			iv_fee.setVisibility(View.VISIBLE);
			lv_fee.setVisibility(View.VISIBLE);

			iv_plan.setVisibility(View.GONE);
			lv_plan.setVisibility(View.GONE);
			iv_type.setVisibility(View.GONE);
			lv_type.setVisibility(View.GONE);
			iv_ending.setVisibility(View.GONE);
			lv_ending.setVisibility(View.GONE);
			break;

		// 套餐计划
		case 1:
			String lastPlan = null;
			for (int i = 0; i < mCurrentPlanList.size(); i++) 
			{
				String plan = ((ConsumePlan) mCurrentPlanList.get(i))
						.getmType();
				if (!plan.equals(lastPlan) && !tempList.contains(plan)) 
				{
					tempList.add(plan);
					lastPlan = plan;
				}
			}

			adapter = new ArrayAdapter<Object>(
					PickNetWorkPackageSelectActivity.this,
					R.layout.lay_package_list_item, R.id.tv_text, tempList);
			lv_plan.setAdapter(adapter);
			iv_plan.setVisibility(View.VISIBLE);
			lv_plan.setVisibility(View.VISIBLE);

			iv_fee.setVisibility(View.GONE);
			lv_fee.setVisibility(View.GONE);
			iv_type.setVisibility(View.GONE);
			lv_type.setVisibility(View.GONE);
			iv_ending.setVisibility(View.GONE);
			lv_ending.setVisibility(View.GONE);
			break;

		// 合约类型
		case 2:
			String lastType = null;
			for (int i = 0; i < mPickNetBusiness.mContractType.size(); i++)
			{
				String type = null;

				type = adjustContractTypeShow(mPickNetBusiness.mContractType
						.get(i));
				if (!type.equals(lastType) && !tempList.contains(type))
				{
					tempList.add(type);
					lastType = type;
				}
			}
			adapter = new ArrayAdapter<Object>(
					PickNetWorkPackageSelectActivity.this,
					R.layout.lay_package_list_item, R.id.tv_text, tempList);
			lv_type.setAdapter(adapter);

			iv_type.setVisibility(View.VISIBLE);
			lv_type.setVisibility(View.VISIBLE);

			iv_fee.setVisibility(View.GONE);
			lv_fee.setVisibility(View.GONE);
			iv_plan.setVisibility(View.GONE);
			lv_plan.setVisibility(View.GONE);
			iv_ending.setVisibility(View.GONE);
			lv_ending.setVisibility(View.GONE);
			break;

		// 合约期限
		case 3:
			String lastContent = null;
			for (int i = 0; i < mPickNetBusiness.mContractBuffs.size(); i++) 
			{
				contractBuff buff;
				buff = mPickNetBusiness.mContractBuffs.get(i);
				String content = buff.getContent();

				if (!content.equals(lastContent) && !tempList.contains(content))
				{
					tempList.add(content);
					lastContent = content;
				}
			}

			adapter = new ArrayAdapter<Object>(
					PickNetWorkPackageSelectActivity.this,
					R.layout.lay_package_list_item, R.id.tv_text, tempList);
			lv_ending.setAdapter(adapter);
			iv_ending.setVisibility(View.VISIBLE);
			lv_ending.setVisibility(View.VISIBLE);

			iv_fee.setVisibility(View.GONE);
			lv_fee.setVisibility(View.GONE);
			iv_plan.setVisibility(View.GONE);
			lv_plan.setVisibility(View.GONE);
			iv_type.setVisibility(View.GONE);
			lv_type.setVisibility(View.GONE);
			break;
		default:
			break;
		}
	}

	// 调整合约类型的显示
	public String adjustContractTypeShow(int contractType) 
	{
		String contractContent = "";
		switch (contractType) 
		{
		case 0:
			contractContent = "不参加活动";
			break;
		case 1:
			contractContent = "存费送费";
			break;
		case 2:
			contractContent = "购机入网送话费";
			break;
		case 3:
			contractContent = "存话费送手机";
			break;
		}
		return contractContent;
	}

	// 获取套餐详情
	private void showConstractInfo() 
	{
		if(!tv_fee.getText().toString().equals("无") && !tv_plan.getText().toString().equals("无"))
		{
			DialogUtil.showProgress("获取套餐详情中,请稍候");
			new Thread() 
			{
				@Override
				public void run()
				{
					int ret = mPickNetBusiness.bll0D06(mCurrentPlanCode,
							mCurrentLevel);
					if (ret == ReturnEnum.SUCCESS) 
					{
						sendMsg(0);
					} 
					else 
					{
						runOnUiThread(new Runnable() 
						{	
							@Override
							public void run() 
							{
								mIsGetPackageDetailSuccess = false;
								resetPackageInfo();
							}
						});
						DialogUtil.closeProgress();
						back(null, null, mPickNetBusiness.getLastknownError(), "确定", null, false, false, false);
					}
				}
			}.start();
		}
		else if(tv_fee.getText().toString().equals("无"))
		{
			back(null, null, "请先选择消费档次!", "确定", null, false, false, false);
		}
		else if(tv_plan.getText().toString().equals("无"))
		{
			back(null, null, "请先选择套餐计划!", "确定", null, false, false, false);
		}
	}

	// 重置部分数据信息
	public void resetDate(int initType)
	{
		switch(initType)
		{
		// 获取套餐详情,合约类型
		case 1:
			// mPickNetBusiness.mContractflag为1为启用合约计划
			mPickNetBusiness.mContractflag = 0x00;
			// 设置到这套餐详情获取尚未成功
			mIsGetPackageDetailSuccess = false;
			mCurrentContractType = 0;
			mCurrentContractBuffCode = -1;
			mPickNetBusiness.mContractType.clear();
			mPickNetBusiness.mContractBuffs.clear();
			tv_type.setText("未参加活动");
			tv_ending.setText("未选择");
			tv_ending.setVisibility(View.GONE);
			lay_ending.setBackgroundResource(R.drawable.title_ending_noselected);
			break;
			
		// 获取合约期限
		case 2:
			mCurrentContractBuffCode = -1;
			mPickNetBusiness.mContractBuffs.clear();
			tv_ending.setText("未选择");
			tv_ending.setVisibility(View.GONE);
			lay_ending.setBackgroundResource(R.drawable.title_ending_noselected);
			
			// 如果合约名称存在，则清空合约名称
			if(!StringUtil.isEmptyOrNull(mPickNetBusiness.mContractName))
			{
				mPickNetBusiness.mContractName = "";
			}
			
			// 如果合约内容有数据,则清空合约内容数据
			if(!StringUtil.isEmptyOrNull(mPickNetBusiness.mContractContent))
			{
				mPickNetBusiness.mContractContent = "";
			}
			break;
		
		// 获取合约内容
		case 3:
			break;
		}
		// 设置到这里合约内容尚未获取成功
		mIsGetContractSuccess = false;
		tv_package_info.setText("");
	}
	
	// 重置套餐详情
	private void resetPackageInfo()
	{
		tv_call_time.setText("--");
		tv_flow1.setText("--");
		tv_flow2.setText("--");
		tv_video_call.setText("--");
		tv_other.setText("--");
		tv_msg_count.setText("--");
		tv_free.setText("--");
		tv_call_fee.setText("--");
		
		if (mCurrentPlanType.equals("C")) 
		{
			tv_label_local.setText("本地语音拨打分钟数");
		} 
		else 
		{
			tv_label_local.setText("国内语音拨打分钟数");
		}
	}
	// 通过Handler机制改变各控件的显示 
	public void sendMsg(int what) 
	{
		Message msg = Message.obtain();
		msg.what = what;
		mHandler.sendMessage(msg);
	}

	// 获取合约期限
	private void showConstractBuffs() 
	{
		DialogUtil.showProgress("获取合约期限中,请稍候");
		new Thread() 
		{
			@Override
			public void run() 
			{
				int ret = mPickNetBusiness.bll0D08(mCurrentContractType);
				if (ret == ReturnEnum.SUCCESS) 
				{
					mHandler.sendEmptyMessage(1);
				}
				else 
				{
					DialogUtil.closeProgress();
					back(null, null, mPickNetBusiness.getLastknownError(), "确定", null, false, false, false);
				}
			}
		}.start();
	}

	private void showIMEIDialog() 
	{ 
		DialogUtil.MsgBox("提示信息", R.layout.lay_input, "确定", "取消", null, new OnGetListener() 
		{
			@Override
			public void onGet(String value) 
			{
				if(!StringUtil.isEmptyOrNull(value))
				{
					DialogUtil.dismiss();
					mPickNetBusiness.contractNo = value;
					showConstractBuffs();
				}
				else
				{
					back(new ICallBack()
					{
						@Override
						public void back() 
						{
							showIMEIDialog();
						}

						@Override
						public void cancel() 
						{
						}

						@Override
						public void dismiss() 
						{
						}
					}, null, "请输入终端串号!", "确定", null, true, false, false);
				}
			}
		}, null); 
	}
			 
	// 获取合约内容
	private void requestShowConstract() 
	{
		DialogUtil.showProgress("获取合约中,请稍候");
		new Thread() 
		{
			@Override
			public void run() 
			{
				int ret = mPickNetBusiness.bll0D09(mCurrentContractBuffCode, mCurrentPlanCode);
				if (ret == ReturnEnum.SUCCESS) 
				{
					sendMsg(2);
				} 
				else 
				{
					runOnUiThread(new Runnable() 
					{	
						@Override
						public void run() 
						{
							mIsGetContractSuccess = false;
						}
					});
					
					DialogUtil.closeProgress();
					back(null, null, mPickNetBusiness.getLastknownError(), "确定", null, false, false, false);
				}
			}
		}.start();
	}

	// 显示合约内容
	public void showContract() 
	{
		if (!StringUtil.isEmptyOrNull(mPickNetBusiness.mContractContent)) 
		{
			tv_package_info.setText(mPickNetBusiness.mContractContent);
		} 
		else 
		{
			tv_package_info.setText("");
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if (keyCode == KeyEvent.KEYCODE_BACK) 
		{
			backToPrevious();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	// 回到上一个Activity
	private void backToPrevious()
	{
		// 改变0D04子命令单元单元0211中需打包的数据信息类型,5为变更预占
		mPickNetBusiness.m0D04AddQueryTerm = 5;
		Bundle bundle = getIntent().getBundleExtra("_data");
		Message msg = Message.obtain();
		bundle.putString("className",
				NumberSelectActivity.class.getName());
		msg.what = FrameActivity.DIRECTION_PREVIOUS;
		msg.setData(bundle);
		FrameActivity.mHandler.sendMessage(msg);
	}
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
	}

	@Override
	protected void onNewIntent(Intent intent) 
	{
		super.onNewIntent(intent);
		Bundle bundle = intent.getBundleExtra("_data");
		if (bundle == null) 
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS) 
		{
			return;
		}
		getIntent().putExtra("_data", bundle);
	}
	
	@Override
	public void onInitSuccess() 
	{
	}

	@Override
	public void onInitFail() 
	{
	}
}
