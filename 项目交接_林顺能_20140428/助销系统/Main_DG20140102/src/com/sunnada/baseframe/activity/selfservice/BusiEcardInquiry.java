package com.sunnada.baseframe.activity.selfservice;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.EcardInquiryData;
import com.sunnada.baseframe.bean.EcardReportData;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.BusinessEcardInquiry;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.database.EcardSharp;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ICallBack;
import com.sunnada.baseframe.dialog.ModifyPswdDialog;
import com.sunnada.baseframe.dialog.ResetSrvDialog;
import com.sunnada.baseframe.dialog.SearchBluetoothDialog;
import com.sunnada.baseframe.listener.IExcute;
import com.sunnada.baseframe.listener.MyGestureListener;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.PrintUtil;
import com.sunnada.baseframe.util.StringUtil;

// 电子卡卡密查询
public class BusiEcardInquiry extends BaseActivity implements OnClickListener
{
	private final String				TITLE_STR			      = "电子卡卡密查询提醒";
	// 电子卡卡密查询业务类
	public BusinessEcardInquiry			mBusiInquiry; 
	// 左边布局 
	private LinearLayout				mLayBack;
	// private LinearLayout                mLinearLeft;
	// private ImageView                   mIvBusiEcardInquiry;
	// 查询布局
	private LinearLayout				mLayQuery;
	// 业务类型
	private TextView					mTvBusi;
	// private ImageView				mIvBusi;
	private LinearLayout				mLayBusi;
	private ListView					mLvBusi;
	private boolean						mIsIvBusiHited          = false;
	// 交易密码
	private EditText					mEtPwd;
	// 查询
	private MyCustomButton				mBtnQuery;
	// 打印布局
	private LinearLayout				mLayPrint;
	// private LinearLayout                mLinearEcardTitle;
	// private TextView                    mTvTime;
	// private TextView                    mTvMoney;
	// private TextView                    mTvOper;
	// private TextView					mTvNum;
	private ListView					mLvEcard;
	// 页面信息
	private TextView                    mTvPageInfo;
	//private MyCustomButton				mBtnPageUp;
	//private MyCustomButton				mBtnPageDown;
	private MyCustomButton              mBtnBack;
	
	private final String[]				mStrBusiArr               = {"购买电子卡","购卡充值"};
	// private String					mSelectedNum;
	private ArrayAdapter<String>		mAdapter                  = null;
	private String						mCardKey;
	private String						mFlowNo;
	private String                      mTime;
	private String                      mMoney;
	
	private GestureDetector             mDetector;
	private MyGestureListener           mGListener;
	
	private boolean                     mIsPrintSuccess           = false; // 是否打印成功 
	private boolean                     mIsHasPrintReport         = false; // 是否已经打印上报
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_selfservice_ecard_inquiry_orange);
		}
		else
		{
			setContentView(R.layout.lay_selfservice_ecard_inquiry);
		}
		mBusiInquiry = new BusinessEcardInquiry(this, equipmentService, socketService, dataBaseService);
		// 初始化控件
		initViews();
		showPage(0x01);
		mLvBusi.setVisibility(View.GONE);
		// 初始化显示
		// initShow();
		// GestureDetector对象
		mGListener = new MyGestureListener(new IExcute() 
		{
			@Override
			public void excuteResult(int result) 
			{
				switch(result)
				{
				case 0:
					procPrev();
					break;
				
				case 1:
					procNext();
					break;
				}
			}
		});
		
		mDetector = new GestureDetector(this, mGListener);
		// 设置监听
		setListener();
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	// 初始化
	protected void initViews()
	{
	   initLeftUI();
	   initStep1();	
	   initStep2();
	}
	
	// 初始化左边布局
	public void initLeftUI()
	{
		//左边布局 
		mLayBack = (LinearLayout) this.findViewById(R.id.linear_backto_selfservice);	
		// mLinearLeft = (LinearLayout) findViewById(R.id.linear_left);
	}
	
	// 初始化第一步
	protected void initStep1()
	{
		// 查询布局
		mLayQuery = (LinearLayout) this.findViewById(R.id.linear_query);
		// 业务类型
		mTvBusi = (TextView) this.findViewById(R.id.tv_busi);
		// mIvBusi = (ImageView) this.findViewById(R.id.iv_busi);
		mLayBusi = (LinearLayout) this.findViewById(R.id.lay_busi);
		mLvBusi = (ListView) this.findViewById(R.id.lv_busi);
		// 交易密码
		mEtPwd = (EditText) this.findViewById(R.id.et_pwd);
		// 查询
		mBtnQuery = (MyCustomButton) this.findViewById(R.id.btn_query);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnQuery.setImageResource(R.drawable.btn_custom_query_orange);
		}
		else
		{
			mBtnQuery.setImageResource(R.drawable.btn_custom_query);
		}
		mBtnQuery.setTextViewText1("查询");
		mBtnQuery.setOnTouchListener(this);
	} 

	// 初始化第二步
	protected void initStep2()
	{
		mLayPrint = (LinearLayout) this.findViewById(R.id.linear_print);
		// mLinearEcardTitle = (LinearLayout) findViewById(R.id.linear_ecard_title);
		// mTvTime = (TextView) findViewById(R.id.tv_time);
		// mTvMoney = (TextView) findViewById(R.id.tv_money);
		// mTvOper = (TextView) findViewById(R.id.tv_oper);
		// mTvNum = (TextView) this.findViewById(R.id.tv_num);
		mLvEcard = (ListView) this.findViewById(R.id.ecard_list);
		
		// 设置底部透明效果
		mLvEcard.setVerticalFadingEdgeEnabled(true);
		mLvEcard.setFadingEdgeLength(60);
		
		mTvPageInfo = (TextView) this.findViewById(R.id.tv_pageinfo);
		/*
		mBtnPageUp = (MyCustomButton) this.findViewById(R.id.btn_pageup);
		mBtnPageUp.setTextViewText1("返回");
		mBtnPageUp.setOnTouchListener(this);
		*/
		mBtnBack = (MyCustomButton) this.findViewById(R.id.btn_back);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnBack.setImageResource(R.drawable.btn_reselect_orange);
		}
		else
		{
			mBtnBack.setImageResource(R.drawable.btn_reselect);
		}
		mBtnBack.setTextViewText1("返回");
		mBtnBack.setOnTouchListener(this);
	}
	
	// 显示第几页
	protected void showPage(int nPageIdx)
	{
		try
		{
			if(nPageIdx==0x01)
			{
				mLayQuery.setVisibility(View.VISIBLE);
				mLayPrint.setVisibility(View.GONE);
				mLvBusi.setVisibility(View.GONE);
				mTvBusi.setText(mStrBusiArr[0]);
				
				mEtPwd.setText("");
				mBusiInquiry.mInquiryType = 0x01;
				mBusiInquiry.mInquiryStartIdx = 0x01;
			}
			else if(nPageIdx==0x02)
			{
				mLayQuery.setVisibility(View.GONE);
				mLayPrint.setVisibility(View.VISIBLE);
				mBusiInquiry.mInquiryStartIdx = 0x01;
				
				BaseAdapter adapter = new EcardInquiryAdapter(
						BusiEcardInquiry.this, 
						new ArrayList<EcardInquiryData>(), 
						R.layout.lay_selfservice_busi_ecard_listitem
						);
				mLvEcard.setAdapter(adapter);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	private void initShow()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mLinearLeft.setBackgroundColor(ARGBUtil.getArgb(2));
			ImageView mIvBusiEcardInquiry = (ImageView) findViewById(R.id.iv_busi_ecard_inquiry);
			mIvBusiEcardInquiry.setImageResource(R.drawable.icon_busi_ecard_inquiry_orange);
			
			mLayBusi.setBackgroundColor(ARGBUtil.getArgb(2));
			// 第二步
			mLinearEcardTitle.setBackgroundColor(ARGBUtil.getArgb(7));
			mTvTime.setTextColor(ARGBUtil.getArgb(2));
			mTvMoney.setTextColor(ARGBUtil.getArgb(2));
			mTvOper.setTextColor(ARGBUtil.getArgb(2));
		}
	}
	*/
	
	// 刷新第几页
	protected void refreshPage(int nPageIdx)
	{
	}
	
	// 初始化业务类型
	private void initBusiType() 
	{
		if(!mIsIvBusiHited)
		{
			mLvBusi.setVisibility(View.VISIBLE);
			mAdapter = new ArrayAdapter<String>(
					BusiEcardInquiry.this,
					R.layout.lay_ecard_busilist_item, R.id.tv_text,mStrBusiArr);
			mLvBusi.setAdapter(mAdapter);
		}
		else
		{
			mLvBusi.setVisibility(View.GONE);
		}
		mIsIvBusiHited = !mIsIvBusiHited;
	}
	
	// 设置监听
	public void setListener()
	{
		// 左边布局
		mLayBack.setOnClickListener(this);
		// 业务类型
		// mIvBusi.setOnClickListener(this);
		mLayBusi.setOnClickListener(this);
		mLvBusi.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int pos,
					long id) 
			{
				if(pos == 0)
				{
					mBusiInquiry.mInquiryType = 0x01;
				}
				else if(pos == 1)
				{
					mBusiInquiry.mInquiryType = 0x02;
				}
				mTvBusi.setText(mStrBusiArr[pos]);
				mLvBusi.setVisibility(View.GONE);
				mIsIvBusiHited = !mIsIvBusiHited;
				mEtPwd.setText("");
			}
		});
		
		// 查询
		mBtnQuery.setOnClickListener(this);
		// 返回
		mBtnBack.setOnClickListener(this);
		mLvEcard.setOnTouchListener(this);
		//mBtnPageUp.setOnClickListener(this);
		//mBtnPageDown.setOnClickListener(this);
	}
	
	// 处理对账请求
	private void handleQuery() 
	{
		String strPwd = mEtPwd.getText().toString().trim();
		
		if(StringUtil.isEmptyOrNull(strPwd) || (strPwd.length() != 6))
		{
			back(null, TITLE_STR, "请输入6位交易密码", "确定", null, false, false, false);
			return;
		}
		else
		{
			mBusiInquiry.mStrPswd = strPwd;
		}
		
		// 异常上报
		exportException();
	}
	
	// 异常上报
	public void exportException()
	{
		final EcardSharp ecardSharp = new EcardSharp(this);
		final EcardReportData reportData = ecardSharp.getValue();
		if(reportData != null)
		{
			DialogUtil.showProgress("上笔卡密打印异常，正在异常上报!");
			new Thread(new Runnable() 
			{
				@Override
				public void run() 
				{
					int queryType = reportData.getQueryType();
					String carCode = reportData.getCardKey();
					String serialNumber = reportData.getFlowNo();
					
					int ret = mBusiInquiry.bll03A4(queryType, carCode, serialNumber);
					DialogUtil.closeProgress();
					if(ret != ReturnEnum.SUCCESS)
					{
						back(new ICallBack() 
						{
							@Override
							public void back()
							{
								showPage(0x01);
							}
							
							@Override
							public void cancel() 
							{
							}
							
							@Override
							public void dismiss() 
							{
								showPage(0x01);
							}
						}, null, "异常上报失败，失败原因:"+mBusiInquiry.getLastKnownError(), "确定", null, true, false, true);
					}
					else
					{
						ecardSharp.clearData();
						startQuery();
					}
				}
			}).start();
		}
		else
		{
			startQuery();
		}
	}
	
	// 开始查询
	public void startQuery()
	{
		DialogUtil.showProgress("正在获取卡密列表");
		new Thread() 
		{
			public void run() 
			{
				if(mBusiInquiry.bll03A1())
				{
					// DialogUtil.showProgress("正在获取电子卡卡密信息");
					int ret = mBusiInquiry.bll03A2();
					if(ret == ReturnEnum.SUCCESS)
					{
						DialogUtil.closeProgress();
						if(mBusiInquiry.mEcardInquryDataList.size() == 0)
						{
							DialogUtil.MsgBox("提示信息", "暂无未打印卡密记录！");
							return;
						}
						else
						{
							runOnUiThread(new Runnable() 
							{
								public void run() 
								{
									showPage(0x02);
									showPageInfo();
									BaseAdapter adapter = new EcardInquiryAdapter(
											BusiEcardInquiry.this, 
											mBusiInquiry.mEcardInquryDataList, 
											R.layout.lay_selfservice_busi_ecard_listitem
											);
									mLvEcard.setAdapter(adapter);
								}
							});
						}
					}
					else
					{
						DialogUtil.MsgBox("提示信息", mBusiInquiry.getLastKnownError());
					}
				}
				else
				{
					if(socketService.getLastknownErrCode() == 0xFFFFFFFC)
					{
						DialogUtil.MsgBox("提示", "获取卡密列表失败!\n失败原因：交易密码错误。");
					}
					else
					{
						DialogUtil.MsgBox("提示", "获取卡密列表失败!");
					}
				}
			}
		}.start();
	}
	
	// 核心处理
	private void procCore(final int direction) 
	{
		DialogUtil.showProgress("正在获取电子卡卡密信息");
		new Thread(new Runnable() 
		{
			@Override
			public void run() 
			{
				int ret = mBusiInquiry.bll03A2();
				if(ret == ReturnEnum.SUCCESS)
				{
					DialogUtil.closeProgress();
					
					// 刷新界面
					refreshScreen();
				}
				else
				{
					DialogUtil.closeProgress();
					back(new ICallBack() 
					{
						@Override
						public void back() 
						{
							recoverData(direction);
						}
						
						@Override
						public void cancel() 
						{
						}
						
						@Override
						public void dismiss() 
						{
							recoverData(direction);
						}
					}, null, mBusiInquiry.getLastKnownError(), "确定", null, true, false, true);
				}
			}
		}).start();
	}
	
	// 当翻页失败时，将数据恢复到翻页前的状态
	public void recoverData(int direction)
	{
		switch (direction) 
		{
		case 0:
			mBusiInquiry.mInquiryStartIdx += mBusiInquiry.mInquiryLineNum;
			break;
		
		case 1:
			mBusiInquiry.mInquiryStartIdx -= mBusiInquiry.mInquiryLineNum;
			break;
		default:
			break;
		}
	}
	
	// 刷新界面
	private void refreshScreen() 
	{
		runOnUiThread(new Runnable() 
		{
			@Override
			public void run() 
			{
				showPageInfo();
				BaseAdapter adapter = new EcardInquiryAdapter(
						BusiEcardInquiry.this, 
						mBusiInquiry.mEcardInquryDataList, 
						R.layout.lay_selfservice_busi_ecard_listitem
						);
				mLvEcard.setAdapter(adapter);
			}
		});
	}
		
	// 处理上一页
	private void procPrev() 
	{
		if(mBusiInquiry.mInquiryStartIdx <= 1) 
		{
			back(null, TITLE_STR, "已经是第一页了！", "确定", null, false, false, false);
			return;
		}
		else
		{
			mBusiInquiry.mInquiryStartIdx -= mBusiInquiry.mInquiryLineNum;
			// 核心处理
			procCore(0);
		}
	}
	
	// 处理下一页
	private void procNext() 
	{
		if(mBusiInquiry.mInquiryStartIdx + mBusiInquiry.mInquiryLineNum > mBusiInquiry.mInquiryTotal) 
		{
			back(null, TITLE_STR, "已经是最后一页了！", "确定", null, false, false, false);
			return;
		}
		else
		{
			mBusiInquiry.mInquiryStartIdx += mBusiInquiry.mInquiryLineNum;
			// 核心处理
			procCore(1);
		}
	}
	
	public void showPageInfo()
	{
		int totalLine = mBusiInquiry.mInquiryTotal;
		int pageLine = mBusiInquiry.mInquiryLineNum;
		int startLine = mBusiInquiry.mInquiryStartIdx;
		int currPage = 1;
		int totalPage = 1;
		totalPage = ((totalLine % pageLine) == 0)? (totalLine/pageLine):(totalLine/pageLine+1);
		currPage = startLine / pageLine + 1;
		mTvPageInfo.setText("第"+currPage+"页/共"+totalPage+"页");
	}
	
	// 为了触发按钮需要触发的事件
	private class EcardInquiryAdapter extends BaseAdapter 
	{
		private List<EcardInquiryData> dataList;
		private int resourceId;
		private LayoutInflater inflator;
		public EcardInquiryAdapter(Context context, List<EcardInquiryData> dataList, int resourceId) 
		{
			this.dataList = dataList;
			this.resourceId = resourceId;
			inflator = LayoutInflater.from(context);
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) 
		{
			ViewHolder viewHolder = new ViewHolder();
			
			if(convertView == null)
			{
				convertView = inflator.inflate(resourceId, null);
				TextView tv_time = (TextView) convertView.findViewById(R.id.tv_date);
				TextView tv_money = (TextView) convertView.findViewById(R.id.tv_money);
				TextView tv_has_detail = (TextView) convertView.findViewById(R.id.tv_has_detail);
				LinearLayout linear_ecard_oper = (LinearLayout) convertView.findViewById(R.id.lay_ecard_oper);
				
				viewHolder.tv_time = tv_time;
				viewHolder.tv_money = tv_money;
				viewHolder.tv_oper = tv_has_detail;
				viewHolder.linear_ecard_has_detail = linear_ecard_oper;
				convertView.setTag(viewHolder);
			}
			else
			{
				viewHolder = (ViewHolder)convertView.getTag();
			}
			
			EcardInquiryData  data = dataList.get(position);
			final int money = data.getmMoney();
			final String time = data.getmTime();
			final String cardKey = data.getmECardKey();
			final String flowNo = data.getmFlowNo();
			
			viewHolder.tv_time.setText(time);
			viewHolder.tv_money.setText(String.valueOf(money));
			
			if(StringUtil.isEmptyOrNull(cardKey))
			{
				viewHolder.tv_oper.setVisibility(View.VISIBLE);
				viewHolder.tv_oper.setText("--");
				viewHolder.tv_oper.setTextColor(Color.argb(0xff, 0xc2, 0xc6, 0xc9));
			}
			else
			{
				viewHolder.tv_oper.setVisibility(View.VISIBLE);
				viewHolder.tv_oper.setText(Html.fromHtml("<u>打印</u>"));
				viewHolder.tv_oper.setTextColor(Color.argb(0xff, 0xfe, 0x99, 0x84));
				viewHolder.linear_ecard_has_detail.setOnClickListener(new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						mCardKey = cardKey;
						mFlowNo = flowNo;
						mTime = time;
						mMoney = String.valueOf(money);
						
						back(new ICallBack()
						{
							@Override
							public void back() 
							{
								print();
							}

							@Override
							public void cancel() 
							{
								DialogUtil.MsgBox("提示信息", "请务必在30天之内通过助销系统电子卡卡密打印功能进行卡密打印！");
							}

							@Override
							public void dismiss() 
							{
								DialogUtil.MsgBox("提示信息", "请务必在30天之内通过助销系统电子卡卡密打印功能进行卡密打印！");
							}
							
						}, null, "是否打印卡密?", "确定", "取消", true, true, true);
					}
				});
			}
			return convertView;
		}

		@Override
		public int getCount() 
		{
			return dataList.size();
		}

		@Override
		public Object getItem(int position) 
		{
			return dataList.get(position);
		}

		@Override
		public long getItemId(int position) 
		{
			return position;
		}
	}
	
	class ViewHolder
	{
		public LinearLayout   linear_ecard_has_detail;
		public TextView       tv_time;
		public TextView       tv_money;
		public TextView       tv_oper;
	}
	
    // 打印
	private void print() 
	{
		DialogUtil.showProgress("正在打印,请稍候");
		new Thread() 
		{
			public void run() 
			{
				// 检测蓝牙设备
				if(equipmentService.handshake() == false) 
				{
					DialogUtil.closeProgress();
					DialogUtil.MsgBox("请连接蓝牙设备", 
							"检测到当前未连接蓝牙打印设备，是否立即搜索并连接蓝牙打印设备？", 
							"立即搜索", 
							Statics.MSG_CONNECT_BLUETOOTH, 
							"取消", 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							mHandler);
					return;
				}
				
				// DialogUtil.showProgress("正在打印卡密");
				int ret = ReturnEnum.FAIL;
				if(!mIsHasPrintReport)
				{
					ret = mBusiInquiry.bll03A3(mCardKey, mFlowNo);
				}
				else
				{
					ret = ReturnEnum.SUCCESS;
				}
				
				if(ret == ReturnEnum.SUCCESS)
				{
					try 
					{
						mIsHasPrintReport = true;
						StringBuffer sb = new StringBuffer();
						sb.append("业务类型: ")
						  .append(mStrBusiArr[mBusiInquiry.mInquiryType-1]+"\n")
						  .append("交易金额: ")
						  .append(mMoney+"元\n")
						  .append("电子卡卡密: ")
						  .append(mCardKey+"\n")
						  .append("交易时间:")
						  .append(mTime);
						// 开始打印
						mBusiInquiry.print(sb, mFlowNo, true);
					} 
					catch (Exception e) 
					{
						DialogUtil.closeProgress();
						e.printStackTrace();
					}
					
					if(equipmentService.isPrintSuccess())
					{
						mIsPrintSuccess = true;
						showRePrintDialog(PrintUtil.STR_SUC_MSG_REPRINT);
					}
					else
					{
						DialogUtil.closeProgress();
						mIsPrintSuccess = false;
						showRePrintDialog(PrintUtil.errorMsgPgAndGet(equipmentService.getPrintErrorMessage()));
					}
				}
				else if(ret == ReturnEnum.SEND_TIMEOUT)
				{
					DialogUtil.closeProgress();
					mIsPrintSuccess = false;
					mIsHasPrintReport = false;
					showPrintDialog();
				}
				else
				{
					DialogUtil.closeProgress();
					mIsPrintSuccess = false;
					mIsHasPrintReport = false;
					back(new ICallBack() 
					{
						@Override
						public void back() 
						{
							saveReportData(mCardKey, mFlowNo);
							showPage(0x01);
						}
						
						@Override
						public void cancel() 
						{
						}
						
						@Override
						public void dismiss() 
						{
							saveReportData(mCardKey, mFlowNo);
							showPage(0x01);
						}
					}, null, "打印卡密失败。失败原因:" + mBusiInquiry.getLastKnownError(), "确定", null, true, false, true);
				}
			};
		}.start();
	}
	
	// 显示是否重新打印选择框
	private void showRePrintDialog(final String msg)
	{
		BusiEcardInquiry.this.runOnUiThread(new Runnable() 
		{
			@Override
			public void run() 
			{
				DialogUtil.closeProgress();
				
				back(new ICallBack() 
				{
					@Override
					public void back() 
					{
						mIsPrintSuccess = false;
						print();
					}
					
					@Override
					public void cancel() 
					{
						handlePrintCancel();
					}

					@Override
					public void dismiss() 
					{
						handlePrintCancel();
					}
				}, 
				PrintUtil.STR_FAIL_TITLE,
				msg, 
				PrintUtil.STR_FAIL_OK, 
				PrintUtil.STR_FAIL_CANCEL,
				true, true, true);
			}
		});
	}
	
	// 显示是否打印选择框
	private void showPrintDialog()
	{
		BusiEcardInquiry.this.runOnUiThread(new Runnable() 
		{
			@Override
			public void run() 
			{
				DialogUtil.closeProgress();
				
				back(new ICallBack() 
				{
					@Override
					public void back() 
					{
						print();
					}
					
					@Override
					public void cancel() 
					{
						DialogUtil.MsgBox("提示信息", "请务必在30天之内通过助销系统电子卡卡密打印功能进行卡密打印！");
					}

					@Override
					public void dismiss() 
					{
						DialogUtil.MsgBox("提示信息", "请务必在30天之内通过助销系统电子卡卡密打印功能进行卡密打印！");
					}
				}, null, "打印卡密失败，是否重新打印卡密?", "确定", "取消", true, true, true);
			}
		});
	}
	
	// 取消打印处理
	public void handlePrintCancel()
	{
		mIsHasPrintReport = false;
		if(mIsPrintSuccess)
		{
			DialogUtil.showProgress("正在刷新列表，请稍候");
			new Thread(new Runnable() 
			{
				@Override
				public void run() 
				{
					int ret = mBusiInquiry.bll03A2();
					if(ret == ReturnEnum.SUCCESS)
					{
						DialogUtil.closeProgress();
						refreshScreen();
						if(mBusiInquiry.mReturnLineNum <= 0)
						{
							procPrev();
						}
					}
					else
					{
						DialogUtil.closeProgress();
						back(new ICallBack() 
						{
							@Override
							public void back() 
							{
								showPage(0x01);
							}
							
							@Override
							public void cancel() 
							{
							}
							
							@Override
							public void dismiss() 
							{
								showPage(0x01);
							}
						}, null, "列表刷新失败！", "确定", null, true, false ,true);
					}
				}
			}).start();
		}
		else
		{
			DialogUtil.showProgress("正在异常上报！");
			new Thread(new Runnable() 
			{
				@Override
				public void run() 
				{
					int ret = mBusiInquiry.bll03A4(mBusiInquiry.mInquiryType, mCardKey, mFlowNo);
					if(ret != ReturnEnum.SUCCESS)
					{
						DialogUtil.closeProgress();
						back(new ICallBack() 
						{
							@Override
							public void back() 
							{
								saveReportData(mCardKey, mFlowNo);
								showPage(0x01);
							}
							
							
							@Override
							public void cancel() 
							{
							}
							
							@Override
							public void dismiss() 
							{
								saveReportData(mCardKey, mFlowNo);
								showPage(0x01);
							}
						}, null, "异常上报失败！", "确定", null, true, false, true);
					}
					else
					{
						DialogUtil.closeProgress();
						DialogUtil.MsgBox("提示信息", "请务必在30天之内通过助销系统电子卡卡密打印功能进行卡密打印！");
					}
				}
			}).start();
		}
	}
	
	// 确定时的动作处理
	protected Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch (message.what) 
			{
			// 连接蓝牙设备
			case Statics.MSG_CONNECT_BLUETOOTH:
				SearchBluetoothDialog dialog = new SearchBluetoothDialog(BusiEcardInquiry.this, mBluetoothClient, equipmentService, 
						mHandler, Statics.MSG_CONNECT_DONE);
				dialog.show();
				break;
				
			// 连接蓝牙设备完成
			case Statics.MSG_CONNECT_DONE:
				new Thread() 
				{
					public void run() 
					{
						print();
					}
				}.start();
				break;
				
			// 取消蓝牙或蓝牙连接失败
			case Statics.MSG_CONNECT_BLUETOOTH_CANCEL:
				if(mIsHasPrintReport)
				{
					if(!mIsPrintSuccess)
					{
						DialogUtil.showProgress("正在异常上报！");
						new Thread(new Runnable() 
						{
							@Override
							public void run() 
							{
								int ret = mBusiInquiry.bll03A4(mBusiInquiry.mInquiryType, mCardKey, mFlowNo);
								if(ret != ReturnEnum.SUCCESS)
								{
									DialogUtil.closeProgress();
									back(new ICallBack() 
									{
										@Override
										public void back() 
										{
											saveReportData(mCardKey, mFlowNo);
											showPage(0x01);
										}
										
										
										@Override
										public void cancel() 
										{
										}
										
										@Override
										public void dismiss() 
										{
											saveReportData(mCardKey, mFlowNo);
											showPage(0x01);
										}
									}, null, "异常上报失败！", "确定", null, true, false, true);
								}
								else
								{
									mIsHasPrintReport = false;
									mIsPrintSuccess = false;
									DialogUtil.closeProgress();
									DialogUtil.MsgBox("提示信息", "请务必在30天之内通过助销系统电子卡卡密打印功能进行卡密打印！");
								}
							}
						}).start();
					}
				}
				else
				{
					mIsHasPrintReport = false;
					mIsPrintSuccess = false;
					DialogUtil.MsgBox("提示信息", "请务必在30天之内通过助销系统电子卡卡密打印功能进行卡密打印！");
				}
				break;
				
				// 初始化交易密码
			case Statics.MSG_MODIFY_PSWD:
				ModifyPswdDialog dialog1 = new ModifyPswdDialog(BusiEcardInquiry.this, 
				new Business_00(BusiEcardInquiry.this, equipmentService, socketService, dataBaseService), mHandler);
				dialog1.show();
				break;
				
			// 重置交易密码
			case Statics.MSG_RESET_PSWD:
				ResetSrvDialog dialog2 = new ResetSrvDialog(BusiEcardInquiry.this, 
				new Business_00(BusiEcardInquiry.this, equipmentService, socketService, dataBaseService), mHandler);
				dialog2.show();
				break;
				
			// 交易密码修改成功
			case Statics.MSG_MODIFY_PSWD_SUCCESS:
				mEtPwd.setText("");
				break;
				
			default:
				break;
			}
		}
	};

	// 保存异常上报需要用到的数据
	public void saveReportData(String cardKey, String flowNo)
	{
		EcardSharp sharp = new EcardSharp(this);
		sharp.putIntValue(mBusiInquiry.mInquiryType);
		sharp.putCardKeyValue(cardKey);
		sharp.putFlowNoValue(flowNo);
	}
	
	@Override
	public void onClick(View v) 
	{
		if((v.getId() != R.id.iv_busi) && (v.getId() != R.id.lay_busi))
		{
			// 防止按钮多次触发
			if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
			{
				return;
			}
		}
		
		switch(v.getId())
		{
		case R.id.linear_backto_selfservice:
			back(new ICallBack()
			{
				@Override
				public void back()
				{
					finish();
				}
				
				@Override
				public void cancel()
				{
				}
				
				@Override
				public void dismiss()
				{
				}
			}, null, null, "确定", "取消", true, false, false);
			break;
		case R.id.iv_busi:
		case R.id.lay_busi:
			initBusiType();
			break;
		
		case R.id.btn_query:
			if(Statics.HAD_PWD)
			{
				handleQuery();
			}
			else
			{
				if(Statics.MODIFY_TYPE == 1)
				{
					DialogUtil.MsgBox("温馨提示", "您还未设置过初始密码，请先进行密码初始化！", 
					"确定", Statics.MSG_MODIFY_PSWD, "取消", -1, -1, mHandler);
				}
				else if(Statics.MODIFY_TYPE == 2)
				{
					DialogUtil.MsgBox("温馨提示", "您还未执行过交易密码重置，请先重置交易密码！", 
							"确定", Statics.MSG_RESET_PSWD, "取消", -1, -1, mHandler);
				}
			}
			break;
		
		case R.id.btn_back:
			showPage(0x01);
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onTouch(View view, MotionEvent event) 
	{
		switch (view.getId()) 
		{
		case R.id.btn_query:
		case R.id.btn_back:
			return super.onTouch(view, event);
		
		case R.id.ecard_list:
			mDetector.onTouchEvent(event);
			return true;

		default:
			return true;
		}
	}

	@Override
	public void onInitFail() 
	{
	}
	
	@Override
	public void onInitSuccess() 
	{
	}

	@Override
	protected void onDestroy() 
	{
		System.gc();
		super.onDestroy();
	}
}
