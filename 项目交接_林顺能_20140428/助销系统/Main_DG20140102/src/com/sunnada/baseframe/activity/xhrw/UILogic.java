package com.sunnada.baseframe.activity.xhrw;

import com.sunnada.baseframe.activity.R;

import android.content.Intent;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * 界面逻辑 抽象类，所有的界面逻辑类继承于此类
 * 此类继承于Handler，实现OnClickListener接口
 */
public abstract class UILogic implements OnClickListener 
{
	public BackType                         p_home             = BackType.UNKNOWN; // 返回建
	protected NumberSelectActivity2         mNumberSelect;

	UILogic(NumberSelectActivity2 numberSelect) 
	{
		mNumberSelect = numberSelect;
	}

	/**
	 * 根据传递的参数，得到具体的子类
	 */
	public static UILogic getInstance(int id, NumberSelectActivity2 numberSelect) 
	{
		UILogic t = null;
		switch (id) {
		case R.layout.lay_picknetwork_package_select:
			t = new PackageSelectLayout(numberSelect);
			break;
			
		case R.layout.lay_picknetwork_business_idinfo:
			t = new BusinessIdinfoLayout(numberSelect);
			break;
			
		case R.layout.lay_netaccesspermits:
			t = new AccessPermitsLayout(numberSelect);
			break;
			
		case R.layout.lay_signature:
			t = new SignatureLayout(numberSelect);
			break;
		
		default:
			t = null;
		}
		return t;
	}

	abstract public void go();

	public void setListener() 
	{
	}

	protected void setClickListener(int id) 
	{
		View t_view = getview(id);
		t_view.setOnClickListener(this);
	}


	protected void MsgOK(int msgID) 
	{
		switch (msgID) 
		{
			case 0xFF:
				break;
	
			default:
				break;
		}
	}

	protected void MsgCancle(int msgID) 
	{
		switch (msgID) 
		{
			default:
				break;
		}
	}

	protected View getview(int id) 
	{
		return mNumberSelect.findViewById(id);
	}

	public void goLayout(int id)
	{
		mNumberSelect.setContentView(id);
	}

	public void handleMessage(Message msg) 
	{
		try {
			// 特殊消息动作的响应
			switch (msg.what) 
			{
				default:
					break;
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		return false;
	}

	/**
	 * 每个子类自定义的回调事件监听器,处理事件时优先处理该事件. 每个回调事件都是一次性的,因此每次都要重新设置监听器
	 */
	protected interface OnCallBackListener 
	{
		public void onCallBack();
	}

	protected OnCallBackListener onCallBackListener;

	boolean onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		return false;
	}
}
