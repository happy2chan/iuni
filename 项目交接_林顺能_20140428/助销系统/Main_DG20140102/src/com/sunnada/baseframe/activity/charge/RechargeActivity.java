package com.sunnada.baseframe.activity.charge;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.util.ButtonUtil;

// 充值中心主界面
public class RechargeActivity extends BaseActivity implements OnClickListener
{
	private static final byte		PAY_PHONE		= 0x01;    	// 手机充值 
	private static final byte		PAY_FIX			= 0x02;    	// 固话充值
	private static final byte		PAY_PHS			= 0x03;    	// 小灵通充值
	private static final byte		PAY_LAN			= 0x04;    	// 宽带充值
	
	private static final byte		CHARGE_PHONE	= 0x05;    	// 手机交费
	private static final byte		CHARGE_FIX		= 0x06;   	// 0x05:固话交费
	private static final byte		CHARGE_PHS		= 0x07;   	// 0x0D:小灵通交费
	private static final byte		CHARGE_LAN		= 0x08;   	// 0x06:宽带缴费
	private static final byte		BUYCARD			= 0x09;    	// 购买电子卡
	
	private int[] 					mPayResource 		= {
														R.drawable.recharge_phone_pay, 
														R.drawable.recharge_fix_pay,			// 购卡充值资源
														R.drawable.recharge_phs_pay, 
														R.drawable.recharge_lan_pay};
	private int[] 					mPayResource_orange = {
														R.drawable.recharge_phone_pay_orange, 
														R.drawable.recharge_fix_pay_orange,			// 购卡充值资源
														R.drawable.recharge_phs_pay_orange, 
														R.drawable.recharge_lan_pay_orange};
	private int[]					mPayID			= {
														PAY_PHONE, 
														PAY_FIX, 
														PAY_PHS, 
														PAY_LAN
													};
	private int[] 					mChargeResource 	= {
														R.drawable.recharge_phone_charge, 
														R.drawable.recharge_fix_charge,			// 交费资源
														R.drawable.recharge_phs_charge, 
														R.drawable.recharge_lan_charge,
														R.drawable.recharge_buycard};
	
	private int[] 					mChargeResource_orange = {
														R.drawable.recharge_phone_charge_orange, 
														R.drawable.recharge_fix_charge_orange,			// 交费资源
														R.drawable.recharge_phs_charge_orange, 
														R.drawable.recharge_lan_charge_orange,
														R.drawable.recharge_buycard_orange};
		
	private int[]					mChargeID		= {
														CHARGE_PHONE, 
														CHARGE_FIX, 
														CHARGE_PHS, 
														CHARGE_LAN, 
														BUYCARD
													};
	private byte					mBusiType		= 0x00;		// 充值交费类型
	private LinearLayout			mMenuRecharge	= null;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_recharge);
		
		// 初始化布局
		runOnUiThread(new Runnable() 
		{
			public void run() 
			{
				initViews();
			}
		});
	}
	
	// 初始化布局
	private void initViews() 
	{
		// 返回充值中心
		ImageView ivBacktoRecharge = (ImageView) findViewById(R.id.iv_backto_mainmenu);
		ivBacktoRecharge.setOnClickListener(this);
		
		mMenuRecharge = (LinearLayout)findViewById(R.id.menu_recharge);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			ivBacktoRecharge.setBackgroundDrawable(getResources().getDrawable(R.drawable.icon_recharge));
			// 购卡充值布局
			initSubLayout(mMenuRecharge, R.drawable.recharge_phone_pay1, mPayResource, mPayID);
			// 交费布局
			initSubLayout(mMenuRecharge, R.drawable.recharge_phone_charge1, mChargeResource, mChargeID);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			ivBacktoRecharge.setBackgroundDrawable(getResources().getDrawable(R.drawable.icon_recharge_orange));
			// 购卡充值布局
			initSubLayout(mMenuRecharge, R.drawable.recharge_phone_pay1_orange, mPayResource_orange, mPayID);
			// 交费布局
			initSubLayout(mMenuRecharge, R.drawable.recharge_phone_charge1_orange, mChargeResource_orange, mChargeID);
		}
	}
	
	// 初始化子布局
	private void initSubLayout(LinearLayout parentLayout, int headResID, int[] resID, int[] ctlID)
	{
		// 第一层布局
		LinearLayout pay = new LinearLayout(this);
		LayoutParams payParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		payParams.setMargins(0, 30, 0, 0);
		pay.setLayoutParams(payParams);
		// 第二层布局(左边)
		LinearLayout pay1 = new LinearLayout(this);
		LayoutParams pay1Params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		pay1.setLayoutParams(pay1Params);
		pay1.setBackgroundResource(headResID);
		pay.addView(pay1);
		// 各类充值布局(包含垂直分布的线性布局)
		LinearLayout pay2 = new LinearLayout(this);
		LayoutParams pay2Params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		pay2.setLayoutParams(pay2Params);
		pay2.setOrientation(LinearLayout.VERTICAL);
		// 
		LinearLayout pay3 = null;
		int count = 0;
		for(int i=0; i<ctlID.length; i++) 
		{
			if(!checkAuthority(ctlID[i])) 
			{
				// 是否已经结束
				if(i == ctlID.length-1)
				{
					if(pay3 != null)
					{
						pay2.addView(pay3);
					}
				}
				// 未获得菜单权限
				continue;
			}
			// 是否该换行?
			if(count%3 == 0) 
			{
				if(count > 0) 
				{
					pay2.addView(pay3);
				}
				pay3 = new LinearLayout(this);
				LayoutParams pay3Params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				pay3Params.setMargins(10, 0, 0, 5);
				pay3.setLayoutParams(pay3Params);
			}
			count++;
			
 			Button button = new Button(this);
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(5, 0, 5, 0);
			button.setLayoutParams(params);		
			button.setId(ctlID[i]);	
			button.setBackgroundResource(resID[i]);	
			button.setOnClickListener(this);
			pay3.addView(button);
			// 是否已经结束
			if(i == ctlID.length-1)
			{
				pay2.addView(pay3);
				break;
			}
		}
		pay.addView(pay2);
		parentLayout.addView(pay);
	}
	
	// 获取菜单权限
	private boolean checkAuthority(int id)
	{
		boolean result = false;
		switch (id)
		{
			// 手机充值
			case PAY_PHONE:
				result = getMenuAuthority("0510");
				break;
				
			// 固话充值
			case PAY_FIX:
				result = getMenuAuthority("0520");
				break;
				
			// 小灵通充值
			case PAY_PHS:
				result = getMenuAuthority("0530");
				break;
				
			// 宽度充值
			case PAY_LAN:
				result = getMenuAuthority("0540");
				break;
			
			// 手机交费
			case CHARGE_PHONE:
				result = getMenuAuthority("0550");
				break;
				
			// 固话交费
			case CHARGE_FIX:
				result = getMenuAuthority("0560");
				break;
				
			// 小灵通交费
			case CHARGE_PHS:
				result = getMenuAuthority("0570");
				break;
				
			// 宽度交费
			case CHARGE_LAN:
				result = getMenuAuthority("0580");
				break;
				
			// 购买电子卡
			case BUYCARD:
				result = getMenuAuthority("0590");
				break;
				
			default:
				break;
		}
		return result;
	}

	@Override
	public void onInitSuccess() 
	{
		
	}

	@Override
	public void onInitFail() 
	{
		
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK) 
		{
			FrameActivity.mHandler.obtainMessage(FrameActivity.FINISH).sendToTarget();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onDestroy() 
	{
		super.onDestroy();
		
		if(mMenuRecharge != null) 
		{
			mMenuRecharge.removeAllViews();
			mMenuRecharge = null;
		}
		// 
		System.gc();
		Log.e("", "充值类onDestroy()");
	}
	
	@Override
	public void onClick(View v)
	{
		if(ButtonUtil.isFastDoubleClick(500)) 
		{
			showErrorMsg("您的操作速度过快！");
			return;
		}
		
		switch(v.getId()) 
		{
		// 返回到主菜单
		case R.id.iv_backto_mainmenu:
			FrameActivity.mHandler.obtainMessage(FrameActivity.FINISH).sendToTarget();
			return;
			
		// 手机充值
		case PAY_PHONE:
			mBusiType = PAY_PHONE;
			break;
			
		// 固话充值
		case PAY_FIX:
			mBusiType = PAY_FIX;
			break;
			
		// 小灵通充值
		case PAY_PHS:
			mBusiType = PAY_PHS;
			break;
			
		// 宽带充值
		case PAY_LAN:
			mBusiType = PAY_LAN;
			break;
			
		// 手机交费
		case CHARGE_PHONE:
			mBusiType = CHARGE_PHONE;
			break;
			
		// 固话交费
		case CHARGE_FIX:
            mBusiType = CHARGE_FIX;
			break;
			
		// 小灵通交费
		case CHARGE_PHS:
			mBusiType = CHARGE_PHS;
			break;
			
		// 宽带交费
		case CHARGE_LAN:
			mBusiType = CHARGE_LAN;
			break;
			
		// 购买电子卡
		case BUYCARD:
			mBusiType = BUYCARD;
			break;
			
		default:
			mBusiType = PAY_PHONE;
			break;
		}
		// 启动翻转效果
		startRotation(v);
		// 跳转到具体的业务
		jumpBusiness();
	}
	
	// 跳转到具体的业务
	public void jumpBusiness() 
	{
		Timer timer = new Timer();
		timer.schedule(new TimerTask() 
		{
			@Override
			public void run() 
			{
				synchronized (Statics.INIT_OK_LOCK) 
				{
					Intent intent = null;
					Context context = RechargeActivity.this;
					if(mBusiType == PAY_PHONE)
					{
						intent = new Intent(context, BusiChargeEPayPhone.class);
					}
					else if(mBusiType == PAY_FIX)
					{
						intent = new Intent(context, BusiChargeEPayFix.class);
					}
					else if(mBusiType == PAY_PHS)
					{
						intent = new Intent(context, BusiChargeEPayPhs.class);
					}
					else if(mBusiType == PAY_LAN)
					{
						intent = new Intent(context, BusiChargeEPayKd.class);
					}
					else if(mBusiType == CHARGE_PHONE)
					{
						intent = new Intent(context, BusiChargePayPhone.class);
					}
					else if(mBusiType == CHARGE_FIX)
					{
						intent = new Intent(context, BusiChargePayFix.class);
					}
					else if(mBusiType == CHARGE_PHS)
					{
						intent = new Intent(context, BusiChargePayPhs.class);
					}
					else if(mBusiType == CHARGE_LAN)
					{
						intent = new Intent(context, BusiChargePayKd.class);
					}
					else if(mBusiType == BUYCARD)
					{
						intent = new Intent(context, BusiChargeBuycard.class);
					}
					startActivity(intent);
				}
			}
		}, 300);
	}
}
