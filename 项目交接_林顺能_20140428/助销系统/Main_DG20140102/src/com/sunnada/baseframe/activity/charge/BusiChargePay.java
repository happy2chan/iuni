package com.sunnada.baseframe.activity.charge;

import java.util.Date;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.activity.busiaccept.base.SimpleTextWatcher;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.BusinessEPay;
import com.sunnada.baseframe.business.BusinessRecharge;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ModifyPswdDialog;
import com.sunnada.baseframe.dialog.PhoneQueryDialog;
import com.sunnada.baseframe.dialog.ResetSrvDialog;
import com.sunnada.baseframe.dialog.SearchBluetoothDialog;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonGroupUtil;
import com.sunnada.baseframe.util.PrintUtil;
import com.sunnada.baseframe.util.StringUtil;
import com.sunnada.baseframe.util.ButtonGroupUtil.ButtonGroupClickListener;
import com.sunnada.baseframe.util.ButtonGroupUtil.ViewGetter;

public class BusiChargePay extends BusiCharge implements OnClickListener, ViewGetter
{
	// 标题
	protected TextView				mTextPayTitle;									// 充值标题
	protected ImageView				mImagePayIcon;									// 充值ICON
	protected LinearLayout			mLinearBacktoCharge;							// 返回到充值二级菜单
	// 第一步(充值信息输入)
	protected RelativeLayout		mLinearChargesInfo;
	protected LinearLayout			mLinearChargesPhone;
	protected LinearLayout			mLinearChargesFix;
	protected LinearLayout			mLinearChargeKdType;
	protected LinearLayout			mLinearChargesKd;
	protected ButtonGroupUtil		mBtnGroupKdType;
	protected EditText 				mEditPhone;
	protected EditText 				mEditFixPre; 									// 固话/小灵通前置号码
	protected EditText				mEditFixLast;									// 固话/小灵通后置号码
	protected EditText				mEditKdPre;
	protected EditText				mEditKdLast;
	protected ButtonGroupUtil		mBtnGroupMoney;									// 金额选择框
	protected EditText				mEditMoney;										// 金额输入框
	protected boolean				mIsOther;										// 是否是其他金额
	protected EditText				mEditPswd;										// 交易密码输入框
	
	protected PhoneQueryDialog		mPhoneQueryDialog		= null;
	protected String 				mStrChargeMoney 		= "20";
	protected String				mStrPswd;
	
	// 第二步(充值信息确认)
	protected RelativeLayout		mLinearEnsureChargesInfo;
	protected TextView				mTextEnsurePhone;
	protected LinearLayout			mLinearEnsureUserInfo;
	protected TextView				mTextEnsureUser;
	protected TextView				mTextEnsureMoney;
	
	// 第三步(充值成功信息确认)
	protected RelativeLayout		mLinearPrint;
	protected TextView				mTextPrintBusiType;								// 业务类型
	protected TextView				mTextPrintPhone;								// 手机号码
	protected TextView				mTextPrintMoney;								// 充值金额
	protected TextView				mTextPrintPreMoney;								// 充值前余额
	protected TextView				mTextPrintChargeTime;							// 充值时间
	protected TextView				mTextPrintBillNum;								// 交易流水号
	
	// 第四步
	protected RelativeLayout		mLinearSendSms;
	protected EditText				mEditNotifyNum;									// 被通知手机号码
	
	protected BusinessRecharge		mBusinessRecharge;
	protected String 				mErrorTypes[] 			= null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_charge_recharge);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_charge_recharge_orange);
		}
		mBusinessRecharge = new BusinessRecharge(this, equipmentService, socketService, dataBaseService, mHandler);
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	protected void initViews() 
	{
		// 初始化第一步
		initStep1();
		// 初始化第二步
		initStep2();
		// 初始化第三步
		initStep3();
		// 初始化第四步
		initStep4();
	}
	
	// 初始化第一步
	protected void initStep1() 
	{
		// 左边布局
		mTextPayTitle 		= (TextView) findViewById(R.id.pay_title);
		mImagePayIcon 		= (ImageView) findViewById(R.id.pay_icon);
		mLinearBacktoCharge = (LinearLayout) findViewById(R.id.linear_backto_recharge);
		mLinearBacktoCharge.setOnClickListener(this);
		
		// 第一步(用户信息输入)
		mLinearChargesInfo 	= (RelativeLayout) findViewById(R.id.layout_chargeinfo);
		mLinearChargesPhone = (LinearLayout) findViewById(R.id.linear_user_phone);
		mLinearChargesFix 	= (LinearLayout) findViewById(R.id.linear_user_fix);
		mLinearChargesKd 	= (LinearLayout) findViewById(R.id.linear_user_kd);
		mLinearChargeKdType = (LinearLayout) findViewById(R.id.linear_kd_type);
		
		mEditPhone 		= (EditText) findViewById(R.id.et_phone);
		mEditFixPre 	= (EditText) findViewById(R.id.et_fix_pre);
		mEditFixLast 	= (EditText) findViewById(R.id.et_fix_last);
		mEditKdPre 		= (EditText) findViewById(R.id.et_kd_pre);
		mEditKdLast 	= (EditText) findViewById(R.id.et_kd_last);
		mEditMoney		= (EditText) findViewById(R.id.et_money);
		mEditPswd		= (EditText) findViewById(R.id.et_password);
		
		Button btnPhoneQuery = (Button) findViewById(R.id.btn_phone_query);
		btnPhoneQuery.setOnClickListener(this);
		btnPhoneQuery.setVisibility(View.VISIBLE);
		
		Button btnFixQuery = (Button) findViewById(R.id.btn_fix_query);
		//btnFixQuery.setOnClickListener(this);
		btnFixQuery.setVisibility(View.GONE);
		
		Button btnKdQuery	= (Button) findViewById(R.id.btn_kd_query);
		//btnKdQuery.setOnClickListener(this);
		btnKdQuery.setVisibility(View.GONE);
		
		MyCustomButton btnStep1Submit = (MyCustomButton) findViewById(R.id.btn_step1_submit);
		btnStep1Submit.setImageResource(R.drawable.btn_custom_check);
		btnStep1Submit.setTextViewText1("提交");
		btnStep1Submit.setOnClickListener(this);
		btnStep1Submit.setOnTouchListener(this);
		
		// 设置前置号码监听事件
		mEditFixPre.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				String str1 = s.toString();
				if(str1.length() == 4) 
				{
					mEditFixPre.clearFocus();
					mEditFixLast.requestFocus();
				}
				/*
				// 查询余额按钮隐藏
				if(str1.length() < 3) 
				{
					mBtnFixQuery.setVisibility(View.INVISIBLE);
				}
				else
				{
					String str2 = mEditFixLast.getText().toString();
					if(str2.length() < 7) 
					{
						mBtnFixQuery.setVisibility(View.INVISIBLE);
					}
					else
					{
						mBtnFixQuery.setVisibility(View.VISIBLE);
					}
				}
				*/
			}
		});
		
		/*
		mEditFixLast.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				if(s.toString().length() >= 7) 
				{
					String str1 = mEditFixPre.getText().toString();
					if(str1.length() < 3) 
					{
						mBtnFixQuery.setVisibility(View.INVISIBLE);
					}
					else
					{
						mBtnFixQuery.setVisibility(View.VISIBLE);
					}
				}
				else
				{
					mBtnFixQuery.setVisibility(View.INVISIBLE);
				}
			}
		});
		*/
		
		// 设置宽带类型
		try
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mBtnGroupKdType = ButtonGroupUtil.createBean(this, 
						new int[] {R.id.btn_kd_adsl, R.id.btn_kd_lan}, 
						new String[] {"    ADSL    ", "     LAN    "}, 
						R.drawable.btn_tip_select, 
						R.drawable.btn_tip_selected);
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mBtnGroupKdType = ButtonGroupUtil.createBean(this, 
						new int[] {R.id.btn_kd_adsl, R.id.btn_kd_lan}, 
						new String[] {"    ADSL    ", "     LAN    "}, 
						R.drawable.btn_tip_select_orange, 
						R.drawable.btn_tip_selected_orange);
			}
			
			mBtnGroupKdType.setSelected(0);
			mBtnGroupKdType.setClickListener(new ButtonGroupClickListener() 
			{
				@Override
				public void onClick(int previousId, int newId) 
				{
					if(newId == R.id.btn_kd_adsl) 
					{
						mBusinessRecharge.mTranceType = 0x0E;
					}
					else
					{
						mBusinessRecharge.mTranceType = 0x0F;
					}
				}
			});
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		// 设置宽带输入框监听事件
		mEditKdPre.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				if(s.toString().length() == 4) 
				{
					mEditKdPre.clearFocus();
					mEditKdLast.requestFocus();
				}
			}
		});
		
		/*
		mEditKdLast.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				if(s.toString().length() > 0) 
				{
					mBtnKdQuery.setVisibility(View.VISIBLE);
				}
				else
				{
					mBtnKdQuery.setVisibility(View.INVISIBLE);
				}
			}
		});
		*/
		
		// 设置金额选择
		try
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mBtnGroupMoney = ButtonGroupUtil.createBean(this, 
						new int[] {R.id.btn_20, R.id.btn_30, R.id.btn_50, R.id.btn_100, R.id.btn_300, R.id.btn_other}, 
						new String[] {"20", "30", "50", "100", "300", "其他金额"}, 
						R.drawable.btn_tip_select, 
						R.drawable.btn_tip_selected);
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mBtnGroupMoney = ButtonGroupUtil.createBean(this, 
						new int[] {R.id.btn_20, R.id.btn_30, R.id.btn_50, R.id.btn_100, R.id.btn_300, R.id.btn_other}, 
						new String[] {"20", "30", "50", "100", "300", "其他金额"}, 
						R.drawable.btn_tip_select_orange, 
						R.drawable.btn_tip_selected_orange);
			}
			mBtnGroupMoney.setSelected(0);
			mBtnGroupMoney.setClickListener(new ButtonGroupClickListener() 
			{
				@Override
				public void onClick(int previousId, int newId) 
				{
					switch(newId) 
					{
					case R.id.btn_20:
						mStrChargeMoney = "20";
						mEditMoney.setVisibility(View.GONE);
						mIsOther = false;
						break;
						
					case R.id.btn_30:
						mStrChargeMoney = "30";
						mEditMoney.setVisibility(View.GONE);
						mIsOther = false;
						break;
						
					case R.id.btn_50:
						mStrChargeMoney = "50";
						mEditMoney.setVisibility(View.GONE);
						mIsOther = false;
						break;
						
					case R.id.btn_100:
						mStrChargeMoney = "100";
						mEditMoney.setVisibility(View.GONE);
						mIsOther = false;
						break;
						
					case R.id.btn_300:
						mStrChargeMoney = "300";
						mEditMoney.setVisibility(View.GONE);
						mIsOther = false;
						break;
						
					case R.id.btn_other:
						mEditMoney.setVisibility(View.VISIBLE);
						mEditMoney.setText("");
						mEditMoney.requestFocus();
						mIsOther = true;
						break;
						
					default:
						break;
					}
				}
			});
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		// 金额输入框监听事件
		mEditMoney.addTextChangedListener(new android.text.TextWatcher()
		{
			String mPrevStr = "";
			
			public void afterTextChanged(Editable editable) 
			{
				String str = mEditMoney.getText().toString();
				if (StringUtil.isEmptyOrNull(str)) 
				{
					return;
				}
				if (!"0".equals(str) && !StringUtil.isPattern(str, "^(?!0+(?:\\.00+)?$)(?:[1-9]\\d*|0)(?:\\.\\d{1,2})?$")
							&& str.charAt(str.length() - 1) != '.') 
				{
					setTextMoney(mPrevStr);
					return;
				}
				
				try {
					// 0.
					if(str.charAt(str.length()-1) != '.') 
					{
						Double money = Double.parseDouble(str);
						if (money < 0 || money >= 10000) 
						{
							setTextMoney(mPrevStr);
						}
					}
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					setTextMoney(mPrevStr);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) 
			{
				mPrevStr = mEditMoney.getText().toString();
			}
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				
			}
			
			// 设置金额
			private void setTextMoney(String money) 
			{
				mEditMoney.setText(money);
				mEditMoney.requestFocus();
				mEditMoney.setSelection(money.length());
			}
		});
	}
	
	// 初始化第二步
	protected void initStep2() 
	{
		mLinearEnsureChargesInfo 	= (RelativeLayout) findViewById(R.id.layout_ensure_chargeinfo);
		mTextEnsurePhone 			= (TextView) findViewById(R.id.tv_ensure_phone);
		mLinearEnsureUserInfo		= (LinearLayout) findViewById(R.id.linear_ensure_chargeinfo_user);
		mTextEnsureUser 			= (TextView) findViewById(R.id.tv_ensure_user);
		mTextEnsureMoney 			= (TextView) findViewById(R.id.tv_ensure_money);
		
		MyCustomButton btnEnsureReturn = (MyCustomButton) findViewById(R.id.btn_ensure_return);
		btnEnsureReturn.setImageResource(R.drawable.btn_reselect);
		btnEnsureReturn.setTextViewText1("返回");
		btnEnsureReturn.setOnClickListener(this);
		btnEnsureReturn.setOnTouchListener(this);
		
		MyCustomButton btnEnsureSubmit = (MyCustomButton) findViewById(R.id.btn_ensure_submit);
		btnEnsureSubmit.setImageResource(R.drawable.btn_custom_check);
		btnEnsureSubmit.setTextViewText1("确定");
		btnEnsureSubmit.setOnClickListener(this);
		btnEnsureSubmit.setOnTouchListener(this);
	}
	
	// 初始化第三步
	private void initStep3() 
	{
		mLinearPrint 		= (RelativeLayout) findViewById(R.id.layout_charge_print);
		mTextPrintBusiType 	= (TextView) findViewById(R.id.tv_print_busitype);
		mTextPrintPhone 	= (TextView) findViewById(R.id.tv_print_phone);
		mTextPrintMoney 	= (TextView) findViewById(R.id.tv_print_money);
		mTextPrintPreMoney 	= (TextView) findViewById(R.id.tv_print_pre_money);
		mTextPrintChargeTime = (TextView) findViewById(R.id.tv_print_time);
		mTextPrintBillNum 	= (TextView) findViewById(R.id.tv_print_billnum);
		
		MyCustomButton btnPrintCancel = (MyCustomButton) findViewById(R.id.btn_print_cancel);
		btnPrintCancel.setImageResource(R.drawable.btn_reselect);
		btnPrintCancel.setTextViewText1("不打印");
		btnPrintCancel.setOnClickListener(this);
		btnPrintCancel.setOnTouchListener(this);
		
		MyCustomButton btnPrint = (MyCustomButton) findViewById(R.id.btn_print);
		btnPrint.setImageResource(R.drawable.btn_custom_check);
		btnPrint.setTextViewText1("受理单打印");
		btnPrint.setOnClickListener(this);
		btnPrint.setOnTouchListener(this);
	}
	
	// 初始化第四步
	private void initStep4() 
	{
		mLinearSendSms = (RelativeLayout) findViewById(R.id.layout_send_sms);
		mEditNotifyNum = (EditText) findViewById(R.id.et_notify_phone);
		
		MyCustomButton btnSendSmsCancel = (MyCustomButton) findViewById(R.id.btn_send_cancel);
		btnSendSmsCancel.setImageResource(R.drawable.btn_reselect);
		btnSendSmsCancel.setTextViewText1("不发送");
		btnSendSmsCancel.setOnClickListener(this);
		btnSendSmsCancel.setOnTouchListener(this);
		
		MyCustomButton btnSendSms = (MyCustomButton) findViewById(R.id.btn_send);
		btnSendSms.setImageResource(R.drawable.btn_custom_check);
		btnSendSms.setTextViewText1("发送通知");
		btnSendSms.setOnClickListener(this);
		btnSendSms.setOnTouchListener(this);
	}
	
	// 显示第几页
	protected void showPage(int nPageIdx)
	{
		if(nPageIdx == 0x01)
		{
			mLinearChargesInfo.setVisibility(View.VISIBLE);
			mLinearEnsureChargesInfo.setVisibility(View.GONE);
			mLinearPrint.setVisibility(View.GONE);
			mLinearSendSms.setVisibility(View.GONE);
		}
		else if(nPageIdx == 0x02)
		{
			mLinearChargesInfo.setVisibility(View.GONE);
			mLinearEnsureChargesInfo.setVisibility(View.VISIBLE);
			mLinearPrint.setVisibility(View.GONE);
			mLinearSendSms.setVisibility(View.GONE);
		}
		else if(nPageIdx == 0x03)
		{
			mLinearChargesInfo.setVisibility(View.GONE);
			mLinearEnsureChargesInfo.setVisibility(View.GONE);
			mLinearPrint.setVisibility(View.VISIBLE);
			mLinearSendSms.setVisibility(View.GONE);
		}
		else if(nPageIdx == 0x04)
		{
			mLinearChargesInfo.setVisibility(View.GONE);
			mLinearEnsureChargesInfo.setVisibility(View.GONE);
			mLinearPrint.setVisibility(View.GONE);
			mLinearSendSms.setVisibility(View.VISIBLE);
		}
	}
	
	// 清空密码 并获取焦点
	protected void clearPWD()
	{
		mEditPswd.setText("");
		mEditPswd.requestFocus();
	}
	
	// 刷新第几页
	protected void refreshPage(int nPageIdx) 
	{
		// 第一步 输入充值信息
		if(nPageIdx == 0x01) 
		{
			// 手机交费
			if(mBusinessRecharge.mTranceType == 0x04) 
			{
				mLinearChargesPhone.setVisibility(View.VISIBLE);
				mLinearChargesFix.setVisibility(View.GONE);
				mLinearChargesKd.setVisibility(View.GONE);
				mLinearChargeKdType.setVisibility(View.GONE);
				
				// 清空输入信息
				clearStep1();
				
				mTextPayTitle.setText("手机交费");
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					mImagePayIcon.setImageResource(R.drawable.pay_icon_phone);
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					mImagePayIcon.setImageResource(R.drawable.pay_icon_phone_orange);
				}
			}
			// 固话交费
			else if(mBusinessRecharge.mTranceType == 0x05) 
			{
				mLinearChargesPhone.setVisibility(View.GONE);
				mLinearChargesFix.setVisibility(View.VISIBLE);
				mLinearChargesKd.setVisibility(View.GONE);
				mLinearChargeKdType.setVisibility(View.GONE);
				
				mTextPayTitle.setText("固话交费");
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					mImagePayIcon.setImageResource(R.drawable.pay_icon_fix);
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					mImagePayIcon.setImageResource(R.drawable.pay_icon_fix_orange);
				}
			}
			// 小灵通交费
			else if(mBusinessRecharge.mTranceType == 0x0D) 
			{
				mLinearChargesPhone.setVisibility(View.GONE);
				mLinearChargesFix.setVisibility(View.VISIBLE);
				mLinearChargesKd.setVisibility(View.GONE);
				mLinearChargeKdType.setVisibility(View.GONE);
				
				mTextPayTitle.setText("小灵通交费");
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					mImagePayIcon.setImageResource(R.drawable.pay_icon_phs);
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					mImagePayIcon.setImageResource(R.drawable.pay_icon_phs_orange);
				}
			}
			// 宽带交费
			else 
			{
				mLinearChargesPhone.setVisibility(View.GONE);
				mLinearChargesFix.setVisibility(View.GONE);
				mLinearChargesKd.setVisibility(View.VISIBLE);
				mLinearChargeKdType.setVisibility(View.VISIBLE);
				
				mTextPayTitle.setText("宽带交费");
				if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
				{
					mImagePayIcon.setImageResource(R.drawable.pay_icon_kd);
				}
				else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
				{
					mImagePayIcon.setImageResource(R.drawable.pay_icon_kd_orange);
				}
			}
			// 清空用户输入
			clearStep1();
		}
		// 第二步 确认交费信息
		else if(nPageIdx == 0x02) 
		{
			/*
			// 手机交费
			if(mBusinessRecharge.mTranceType == 0x04)
			{
				if(StringUtil.isEmptyOrNull(mStrUserInfo)) 
				{
					mLinearEnsureUserInfo.setVisibility(View.GONE);
				}
				else
				{
					mLinearEnsureUserInfo.setVisibility(View.VISIBLE);
					try 
					{
						// 取出用户名
						// 格式: "手机号码:13218008888\n用户名:测试用户\n充值前可用余额: 500.00元\n欠费/应缴费: 0元
						String[] temp1 = mStrUserInfo.split("\n");
						if(temp1 != null) 
						{
							String[] temp2 = temp1[1].split(":");
							if(!StringUtil.isEmptyOrNull(temp2[1])) 
							{
								mTextEnsureUser.setText(temp2[1]);
							}
							else
							{
								temp2 = temp1[1].split("：");
								if(!StringUtil.isEmptyOrNull(temp2[1])) 
								{
									mTextEnsureUser.setText(temp2[1]);
								}
							}
						}
					}
					catch(Exception e) 
					{
						e.printStackTrace();
					}
				}
			}
			else
			{
				mLinearEnsureUserInfo.setVisibility(View.GONE);
			}
			*/
			
			// 手机账户名
			mLinearEnsureUserInfo.setVisibility(View.GONE);
			// 手机号码
			mTextEnsurePhone.setText(mStrTelNum);
			// 交费金额
			mTextEnsureMoney.setText(mStrChargeMoney + "元");
		}
		// 第三步 凭条打印确认界面
		else if(nPageIdx == 0x03) 
		{
			// 业务类型
			mTextPrintBusiType.setText(mBusinessRecharge.backtype());
			// 充值号码
			mTextPrintPhone.setText(mStrTelNum);
			// 充值金额
			mTextPrintMoney.setText(mBusinessRecharge.mStrChargeMoney);
			// 充值前金额
			mTextPrintPreMoney.setText(mBusinessRecharge.mStrPreMoney + "元");
			// 充值时间
			mTextPrintChargeTime.setText(StringUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
			// 业务流水号
			mTextPrintBillNum.setText(mBusinessRecharge.mStrBillNum);
		}
		// 发送短信通知界面
		else if(nPageIdx == 0x04) 
		{
			if(mStrTelNum.indexOf("-") != -1)
			{
				mEditNotifyNum.setText("");
			}
			else
			{
				mEditNotifyNum.setText(mStrTelNum);
			}
		}
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 返回二级菜单
		case R.id.linear_backto_recharge:
			back2Main();
			break;
			
		// 手机账户查询
		case R.id.btn_phone_query:
			handleQueryPhone();
			break;
		
		/*
		// 固话账户查询/小灵通账户查询
		case R.id.btn_fix_query:
			handleQueryFix();
			break;
			
		// 宽带账户查询
		case R.id.btn_kd_query:
			handleQueryKd();
			break;
		*/
			
		// 确认
		case R.id.btn_step1_submit:
			handleGotoStep2();
			break;
			
		// 第二步返回上一步
		case R.id.btn_ensure_return:
			showPage(0x01);
			break;
			
		// 确认交费
		case R.id.btn_ensure_submit:
			// 清除交易密码
			clearPWD();
			// 发起交费流程
			handleCharge();
			break;
			
		// 凭条打印
		case R.id.btn_print:
			mHandler.sendEmptyMessage(Statics.MSG_PRINT);
			break;
			
		// 不打印凭条
		case R.id.btn_print_cancel:
			mHandler.sendEmptyMessage(Statics.MSG_PRINT_CANCEL);
			break;
			
		// 短信通知
		case R.id.btn_send:
			procSendSms();
			break;
			
		// 不发送短信通知
		case R.id.btn_send_cancel:
			// 显示第3页
			showPage(0x03);
			// 刷新第3页
			refreshPage(0x03);
			break;
			
		default:
			break;
		}
	}
	
	// 清空所有填写框
	protected void clearStep1() 
	{
		// 宽带类型
		mBtnGroupKdType.setSelected(0);
		
		// 手机号码
		if(mBusinessRecharge.mTranceType == 0x02) 
		{
			mEditPhone.setText("");
			mEditPhone.requestFocus();
		}
		// 固话号码/小灵通号码
		else if(mBusinessRecharge.mTranceType == 0x05 || mBusinessRecharge.mTranceType == 0x0D) 
		{
			mEditFixPre.setText("");
			mEditFixLast.setText("");
			mEditFixPre.requestFocus();
		}
		// 宽带号码
		else if(mBusinessRecharge.mTranceType == 0x0E || mBusinessRecharge.mTranceType == 0x0F) 
		{
			mEditKdPre.setText("");
			mEditKdLast.setText("");
			mEditKdPre.requestFocus();
		}
		// 充值金额
		mBtnGroupMoney.setSelected(0);
		mEditMoney.setVisibility(View.GONE);
		mEditMoney.setText("");
		mStrChargeMoney = "20";
		mIsOther = false;
		// 交易密码
		mEditPswd.setText("");
	}
	
	// 处理手机账户信息的查询
	private void handleQueryPhone() 
	{
		mStrTelNum = mEditPhone.getText().toString();
		// 判断手机号码是不是为11位
		if (mStrTelNum.length() != 11) 
		{
			DialogUtil.MsgBox(BusinessRecharge.TITLE_STR, "请输入11位手机号码！");
			return;
		}
		mBusinessRecharge.mStrTelNum = mStrTelNum;
		Log.e(BusinessEPay.TAG, "当前手机号码为: " + mStrTelNum);
		mPhoneQueryDialog = new PhoneQueryDialog(this, mStrTelNum, mHandler, mBusinessRecharge);
		mPhoneQueryDialog.show();
	}
	
	/*
	// 处理固话用户/小灵通用户的查询
	private void handleQueryFix() 
	{
		// 判断号码
		String tempstr1 = mEditFixPre.getText().toString().trim();
		if (StringUtil.isEmptyOrNull(tempstr1) || tempstr1.length() < 3) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入3位或4位区号！");
			return;
		}
		String tempstr2 = mEditFixLast.getText().toString().trim();
		if (StringUtil.isEmptyOrNull(tempstr2) || tempstr2.length() < 7) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入7位或8位号码！");
			return;
		}
		mStrTelNum = tempstr1 + "-" + tempstr2;
		mBusinessRecharge.mStrTelNum = mStrTelNum;
		Log.e(BusinessEPay.TAG, "当前固话或小灵通号码为: " + mStrTelNum);
		
		mPhoneQueryDialog = new PhoneQueryDialog(this, mStrTelNum, mHandler, mBusinessRecharge);
		mPhoneQueryDialog.show();
	}
	
	// 处理宽带用户的查询
	private void handleQueryKd() 
	{
		String tempstr1 = mEditKdPre.getText().toString().trim();
		if (StringUtil.isEmptyOrNull(tempstr1) || tempstr1.length() < 3) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入3位或者4位区号！");
			return;
		}
		String tempstr2 = mEditFixPre.getText().toString().trim();
		if (StringUtil.isEmptyOrNull(tempstr2))  
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入宽带号码！");
			return;
		}
		mStrTelNum = tempstr1 + "-" + tempstr2;
		mBusinessRecharge.mStrTelNum = mStrTelNum;
		Log.e(BusinessEPay.TAG, "当前宽带号码为: " + mStrTelNum);
		
		mPhoneQueryDialog = new PhoneQueryDialog(this, mStrTelNum, mHandler, mBusinessRecharge);
		mPhoneQueryDialog.show();
	}
	*/
	
	// 处理跳转到第二步
	private void handleGotoStep2() 
	{
		// 判断手机号码
		if(mBusinessRecharge.mTranceType == 0x04) 
		{
			mStrTelNum = mEditPhone.getText().toString();
			if(StringUtil.isEmptyOrNull(mStrTelNum)) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入手机号码！");
				return;
			}
			if(mStrTelNum.length() != 11) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入11位手机号码！");
				return;
			}
		}
		else if(mBusinessRecharge.mTranceType == 0x05 || mBusinessRecharge.mTranceType == 0x0D) 
		{
			// 区号
			String str1 = mEditFixPre.getText().toString();
			if(StringUtil.isEmptyOrNull(str1)) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入区号！");
				return;
			}
			if(str1.length() != 3 && str1.length() != 4)
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入3位或4位区号！");
				return;
			}
			// 号码
			String str2 = mEditFixLast.getText().toString();
			if(StringUtil.isEmptyOrNull(str2)) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入号码！");
				return;
			}
			if(str2.length() != 7 && str2.length() != 8) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入7位或8位号码！");
				return;
			}
			mStrTelNum = str1 + "-" + str2;
		}
		else if(mBusinessRecharge.mTranceType == 0x0E || mBusinessRecharge.mTranceType == 0x0F) 
		{
			// 区号
			String str1 = mEditKdPre.getText().toString();
			if(StringUtil.isEmptyOrNull(str1)) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入区号！");
				return;
			}
			// 号码
			String str2 = mEditKdLast.getText().toString();
			if(StringUtil.isEmptyOrNull(str2)) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入宽带号码！");
				return;
			}
			mStrTelNum = str1 + "-" + str2;
		}
		
		// 充值金额(其他金额)
		if(mIsOther == true) 
		{
			String strMoney = mEditMoney.getText().toString();
			if(StringUtil.isEmptyOrNull(strMoney)) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入金额！");
				return;
			}
			if(isIllegalMoney(strMoney))
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "您输入的的金额有误！");
				return;
			}
			
			mStrChargeMoney = strMoney;
		}
		Log.e(BusinessEPay.TAG, "交费金额为:" + mStrChargeMoney);
		mBusinessRecharge.mChargeMoney = (int)(Double.parseDouble(mStrChargeMoney)*1000/10);
		if(mBusinessRecharge.mChargeMoney == 0) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "交费金额不能为0！");
			return;
		}
		mStrChargeMoney = String.format("%.2f", (float)mBusinessRecharge.mChargeMoney/100);
		mBusinessRecharge.mStrChargeMoney = mStrChargeMoney;
		
		// 校验密码
		mStrPswd = mEditPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(mStrPswd)) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入交易密码！");
			return;
		}
		if(mStrPswd.length() != 6)
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入6位交易密码！");
			return;
		}
		
		if(!Statics.HAD_PWD)
		{
			if(Statics.MODIFY_TYPE == 1)
			{
				DialogUtil.MsgBox("温馨提示", "您还未设置过初始密码，请先进行密码初始化！", 
				"确定", Statics.MSG_MODIFY_PSWD, "取消", -1, -1, mHandler);
			}
			else if(Statics.MODIFY_TYPE == 2)
			{
				DialogUtil.MsgBox("温馨提示", "您还未执行过交易密码重置，请先重置交易密码！", 
						"确定", Statics.MSG_RESET_PSWD, "取消", -1, -1, mHandler);
			}
		}
		else
		{
			mBusinessRecharge.mStrTelNum = mStrTelNum;
			mBusinessRecharge.mStrPswd = mStrPswd;
			
			// 显示第二步
			showPage(0x02);
			// 刷新第二步
			refreshPage(0x02);
		}
	}
	
	// 处理充值请求
	private void handleCharge() 
	{
		new Thread() 
		{
			public void run() 
			{
				mBusinessRecharge.doRecharge(0x01);
			}
		}.start();
	}
	
	// 处理短信发送
	private void procSendSms() 
	{
		// 校验短信通知号码
		mBusinessRecharge.mStrNotifyNum = mEditNotifyNum.getText().toString();
		if(StringUtil.isEmptyOrNull(mBusinessRecharge.mStrNotifyNum)) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入短信通知号码！");
			
			return;
		}
		if(mBusinessRecharge.mStrNotifyNum.length() != 11) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入11位短信通知号码！");
			return;
		}
		
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在发送短信通知...");
				mBusinessRecharge.bll0205();
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 确定时的动作处理
	protected Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch (message.what)
			{
				// 初始化交易密码
				case Statics.MSG_MODIFY_PSWD:
					new ModifyPswdDialog(BusiChargePay.this, 
					new Business_00(BusiChargePay.this, equipmentService, socketService, dataBaseService), mHandler)
					.show();
					break;
					
				// 重置交易密码
				case Statics.MSG_RESET_PSWD:
					new ResetSrvDialog(BusiChargePay.this, 
					new Business_00(BusiChargePay.this, equipmentService, socketService, dataBaseService), mHandler)
					.show();
					break;
					
				// 修改交易密码成功
				case Statics.MSG_MODIFY_PSWD_SUCCESS:
					mEditPswd.setText("");
					break;
					
				// 校验验证码成功
				case Statics.MSG_VERIFY_OK:
					mPhoneQueryDialog.dismiss();
					// 查询用户信息
					queryUserInfo(mBusinessRecharge);
					break;
					
				// 密码错误或者不允许异地交费
				case Statics.MSG_PWD_FAIL:
					// 显示第1页
					showPage(0x01);
					// 密码框清空并获取焦点
					clearPWD();
					break;
					
				// 发起交费请求失败
				case Statics.MSG_RECHARGE_FAIL:
					// 跳转到第一页
					showPage(0x01);
					// 清除交易密码
					clearPWD();
					break;
					
				// 交费成功, 跳到是否发送短信通知的界面
				case Statics.MSG_RECHARGE_OK:
					// 显示第4页
					showPage(0x04);
					// 刷新第4页
					refreshPage(0x04);
					break;
					
				// 发送短信通知成功, 返回第一步
				case Statics.MSG_NOTIFY_OK:
					finish();
					// 显示第一步
					//showPage(0x01);
					// 清空用户输入
					//clearStep1();
					// 检测异常交易记录
					//mBusinessRecharge.checkErrorDeal();
					break;
					
				// 发送短信通知失败, 跳到受理单打印的界面
				case Statics.MSG_NOTIFY_FAIL:
					// 显示第3页
					showPage(0x03);
					// 刷新第3页
					refreshPage(0x03);
					break;
					
				// 充值成功, 打印交易凭条
				case Statics.MSG_PRINT:
				// 继续检测打印机状态
				case Statics.MSG_RECHARGE_OK_CHECK:
					new Thread() 
					{
						public void run() 
						{
							// 检测蓝牙设备是否存在
							DialogUtil.showProgress("正在检测蓝牙设备...");
							if(equipmentService.handshake() == false) 
							{
								DialogUtil.closeProgress();
								DialogUtil.MsgBox("请连接蓝牙设备", 
										"检测到当前无蓝牙打印设备, 是否连接蓝牙?", 
										"立即搜索", 
										Statics.MSG_RECHARGE_OK_CONNECT_BLUETOOTH, 
										"取消", 
										Statics.MSG_PRINT_CANCEL, 
										Statics.MSG_PRINT_CANCEL, 
										mHandler);
								return;
							}
							// 打印交费信息
							printRechargeInfo();
						};
					}.start();
					break;
					
				// 连接蓝牙设备
				case Statics.MSG_RECHARGE_OK_CONNECT_BLUETOOTH:
					SearchBluetoothDialog dialog = new SearchBluetoothDialog(BusiChargePay.this, mBluetoothClient, equipmentService, 
							mHandler, Statics.MSG_RECHARGE_OK_CONNECT_DONE);
					dialog.show();
					break;
					
				// 蓝牙蓝牙设备完成
				case Statics.MSG_RECHARGE_OK_CONNECT_DONE:
					new Thread() 
					{
						public void run() 
						{
							// 握手失败, 说明设备没连上
							DialogUtil.showProgress("正在检测蓝牙设备状态...");
							if(equipmentService.handshake() == false) 
							{
								DialogUtil.closeProgress();
								mHandler.sendEmptyMessage(Statics.MSG_PRINT_CANCEL);
								return;
							}
							// 打印交费信息
							printRechargeInfo();
						}
					}.start();
					break;
					
				// 取消打印
				case Statics.MSG_PRINT_CANCEL:
					mHandler.sendEmptyMessage(Statics.MSG_NOTIFY_OK);
					break;
					
				default:
					break;
			}
		}
	};
	
	// 打印交费信息
	private void printRechargeInfo() 
	{
		// 检测缺纸状态
		DialogUtil.showProgress("正在检测蓝牙打印机状态...");
		if(checkPrinter(Statics.MSG_RECHARGE_OK_CHECK, Statics.MSG_PRINT_CANCEL, mHandler) == false) 
		{
			DialogUtil.closeProgress();
			return;
		}
		// 打印
		DialogUtil.showProgress("正在打印...");
		mBusinessRecharge.print();
		DialogUtil.closeProgress();
		
		if (!equipmentService.isPrintSuccess()) 
		{
			DialogUtil.MsgBox(
					PrintUtil.STR_FAIL_TITLE, 
					PrintUtil.errorMsgPgAndGet(equipmentService.getPrintErrorMessage()), 
					PrintUtil.STR_FAIL_OK, 
					Statics.MSG_PRINT, 
					PrintUtil.STR_FAIL_CANCEL, 
					Statics.MSG_NOTIFY_OK, 
					Statics.MSG_NOTIFY_OK, 
					mHandler);
		}
		else
		{
			DialogUtil.MsgBox(PrintUtil.STR_SUCCESS_TITLE, 
					  PrintUtil.STR_SUCCESS_MSG, 
					  PrintUtil.STR_SUCCESS_OK,
					  Statics.MSG_NOTIFY_OK,
					  "",
			          0, 
			          Statics.MSG_NOTIFY_OK, 
			          mHandler);
		}
	}
}
