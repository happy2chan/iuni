package com.sunnada.baseframe.activity;

import java.util.Stack;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.sunnada.baseframe.activity.busiaccept.BusiAcceptActivity;
import com.sunnada.baseframe.activity.charge.BusiQuickCharge;
import com.sunnada.baseframe.activity.charge.RechargeActivity;
import com.sunnada.baseframe.activity.dztj.RecommendActivity;
import com.sunnada.baseframe.activity.selfservice.BusiSelfService;
import com.sunnada.baseframe.activity.xhrw.NumberSelectActivity;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.BaseBusiness;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.util.CrashHandler;
import com.sunnada.baseframe.view.TurnplateView;
import com.sunnada.baseframe.view.TurnplateView.OnTurnplateListener;

public class FrameActivity extends ActivityGroup implements OnTurnplateListener, OnClickListener
{
	private LinearLayout		mLayContent;
	private RelativeLayout		mLayMenu			= null;
	private TurnplateView		mTurnplateView;
	private Button				mBtnSwitchShow;
	private Button				mBtnSwitchHide;
	private Bitmap				mMenuBgIcon			= null;
	private Bitmap[]			mMenuIcons			= new Bitmap[3];
	private int[]				mMenuIconsId		= 
													{
														R.drawable.icon_frame_scan, 
														R.drawable.icon_frame_home, 
														R.drawable.icon_frame_bluetooth, 
														//R.drawable.icon_frame_package, 
														//R.drawable.icon_frame_num, 
														//R.drawable.icon_frame_installbd, 
														//R.drawable.icon_frame_installphone, 
														//R.drawable.icon_frame_treasurebox, 
														//R.drawable.icon_frame_downapp, 
														//R.drawable.icon_frame_smallcr 
													};
	
	private boolean				mIsShown			= false;
	private String				mStrBusiType		= "";
	private View				mCurrentView		= null;
	private Stack<String>		mClassStack			= new Stack<String>();
	
	public static Handler		mHandler;
	public static final int		DIRECTION_NEXT		= -1;
	public static final int		DIRECTION_PREVIOUS	= 1;
	public static final int		FINISH				= 2;
	public static BaseBusiness	mBaseBusiness;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			this.setContentView(R.layout.lay_newframe_orange);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			this.setContentView(R.layout.lay_newframe);
		}
		
		// 初始化控件
		initViews();
		// 
		//Log.e("", String.format("111 in %d ms", System.currentTimeMillis()));
		String type = getIntent().getStringExtra("type");
		changeView(type, 0);
		//Log.e("", String.format("222 in %d ms", System.currentTimeMillis()));
		
		// 调试情况下不开启
		CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(getApplicationContext());
		
		runOnUiThread(new Runnable() 
		{
			public void run() 
			{
				if (isMatchSize(1280, 800)) 
				{
					mTurnplateView = new TurnplateView(getApplicationContext(), 0, getScreenHeight()/2-25, 170, mMenuIcons.length);
				}
				else if(isMatchSize(1024, 552))
				{
					mTurnplateView = new TurnplateView(getApplicationContext(), 0, getScreenHeight()/2-12, 138, mMenuIcons.length);
				}
				else
				{
					mTurnplateView = new TurnplateView(getApplicationContext(), 0, getScreenHeight()/2, 170, mMenuIcons.length);
				}
				mTurnplateView.setOnTurnplateListener(FrameActivity.this);
				
				// 初始化圆环菜单ICON
				initMenuIcons();
				// 初始化圆环菜单背景图片
				initMenuBgIcon(R.drawable.bg_lay_menu);
				mTurnplateView.initDate(mMenuIcons);
				// 设置圆环菜单的背景图片
				mTurnplateView.setMenuBgIcon(mMenuBgIcon);
				mLayMenu.addView(mTurnplateView);
				
				mHandler = new Handler() 
				{
					@Override
					public void handleMessage(Message msg) 
					{
						Bundle data = msg.getData();
						switch (msg.what)
						{
							case DIRECTION_NEXT:
								try
								{
									showPage(msg.what, data);
								}
								catch (Exception e) 
								{
									e.printStackTrace();
								}
								break;
								
							case DIRECTION_PREVIOUS:
								try
								{
									showPage(msg.what, data);
								}
								catch (Exception e)
								{
									e.printStackTrace();
								}
								break;
								
							case FINISH:
								FrameActivity.this.finish();
								break;
								
							default:
								break;
						}
					}
				};
			}
		});
	}
	
	// 初始化控件
	public void initViews() 
	{
		mLayMenu = (RelativeLayout) findViewById(R.id.lay_menu);
		mLayContent = (LinearLayout) findViewById(R.id.lay_content);
		
		mBtnSwitchShow = (Button) findViewById(R.id.btn_switch_show);
		mBtnSwitchShow.setOnClickListener(this);
		
		mBtnSwitchHide = (Button) findViewById(R.id.btn_switch_hide);
		mBtnSwitchHide.setOnClickListener(this);
	}
	
	// 设置圆环菜单的背景图片
	private void initMenuBgIcon(int mMenuBgIconId) 
	{
		mMenuBgIcon = BitmapFactory.decodeResource(getResources(), mMenuBgIconId);
		if(isMatchSize(1024, 552)) 
		{
			mMenuBgIcon = scalePicture(mMenuBgIcon, 200f, 400f);
		}
		else
		{
			mMenuBgIcon = scalePicture(mMenuBgIcon, 242f, 483f);
		}
	}
	
	// 设置圆环菜单的菜单图片
	private void initMenuIcons()
	{
		float menuW = 0;
		float menuH = 0;
		
		if(isMatchSize(1024, 552)) 
		{
			menuW = 80f;
			menuH = 80f;
		}
		else
		{
			menuW = 94f;
			menuH = 94f;
		}
		
		for (int i = 0; i < mMenuIcons.length; i++)
		{
			if(mMenuIcons[i] != null) 
			{
				Log.e("", "旋转托盘图标资源释放");
				mMenuIcons[i].recycle();
				mMenuIcons[i] = null;
			}
			mMenuIcons[i] = BitmapFactory.decodeResource(getResources(), mMenuIconsId[i]);
			mMenuIcons[i] = scalePicture(mMenuIcons[i], menuW, menuH);
		}
	}
	
	// 调整图片的宽高
	public Bitmap scalePicture(Bitmap bitmap, float newWidth, float newHeight) 
	{
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		// Matrix比例
		float scaleWidth  = newWidth/width;
		float scaleHeight = newHeight/height;
		
		// 使用Matrix.postScale设置维度Resize
		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		
		// Resize图片文件至指定大小分辨率
		Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
		if(resizedBitmap != bitmap) 
		{
			bitmap.recycle();
			bitmap = null;
		}
		return resizedBitmap;
	}

	// 动态检验是否匹配指定屏幕大小
	public boolean isMatchSize(int w, int h) 
	{
		DisplayMetrics dm = new DisplayMetrics();
		this.getWindowManager().getDefaultDisplay().getMetrics(dm);
		int height = dm.heightPixels;
		int width = dm.widthPixels;
		
		if((w == width)&&(h == height))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	// 获取屏幕高度
	public int getScreenHeight()
	{
		DisplayMetrics dm = new DisplayMetrics();
		this.getWindowManager().getDefaultDisplay().getMetrics(dm);
		int height = dm.heightPixels;
		return height;
	}
	
	public void onPointTouch(int flag) 
	{
		switch (flag) 
		{
		// 扫描购机
		case 0:
			DialogUtil.MsgBox("温馨提示", "扫描购机业务暂未开放！");
			switchButton();
			break;
			
		// 返回到主界面
		case 1:
			backToHome();
			switchButton();
			break;
			
		// 蓝牙快捷链接
		case 2:
			DialogUtil.MsgBox("温馨提示", "蓝牙快捷连接暂未开放！");
			//SearchBluetoothDialog dialog = new SearchBluetoothDialog(FrameActivity.this,
			//BaseActivity.mBluetoothClient, BaseActivity.equipmentService, null, -1);
			//dialog.show();
			switchButton();
			break;
			
		case 3:
			changeView(NumberSelectActivity.class.getName(), 1);
			switchButton();
			break;
			
		case 4:
			changeView(BusiQuickCharge.class.getName(), 1);
			switchButton();
			break;
			
		case 5:
			changeView(RechargeActivity.class.getName(), 1);
			switchButton();
			break;
			
		case 6:
			changeView(BusiAcceptActivity.class.getName(), 1);
			switchButton();
			break;
			
		case 7:
			changeView(RecommendActivity.class.getName(), 1);
			switchButton();
			break;
			
		case 8:
			changeView(BusiSelfService.class.getName(), 1);
			switchButton();
			break;
			
		case -1:
			mTurnplateView.setEnableTouchStatus(false);
			switchButton();
			break;
			
		default:
			break;
		}
	}
	
	// 开关导航
	private void switchButton()
	{
		if (mIsShown)
		{
			mBtnSwitchHide.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.left_out));
			mBtnSwitchHide.setVisibility(View.GONE);

			Animation anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.left_in);
			anim.setStartOffset(50);
			mBtnSwitchShow.setVisibility(View.VISIBLE);
			mBtnSwitchShow.startAnimation(anim);

			mLayMenu.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.anm_rotate_out));
			mLayMenu.setVisibility(View.GONE);
		}
		else
		{
			mBtnSwitchShow.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.left_out));
			mBtnSwitchShow.setVisibility(View.GONE);
			
			Animation anim = AnimationUtils.loadAnimation(getBaseContext(), R.anim.left_in);
			anim.setStartOffset(10);
			mBtnSwitchHide.setVisibility(View.VISIBLE);
			mBtnSwitchHide.startAnimation(anim);
			
			Animation anim2 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.anm_rotate_in);
			anim2.setStartOffset(100);
			mLayMenu.setVisibility(View.VISIBLE);
			mLayMenu.startAnimation(anim2);
		}
		mIsShown = !mIsShown;
	}

	public void onClick(View view)
	{
		switch (view.getId())
		{
			case R.id.btn_switch_hide:
				switchButton();
				break;
				
			case R.id.btn_switch_show:
				mTurnplateView.setEnableTouchStatus(true);
				switchButton();
				break;
		}
	}

	// 左边快捷方式的快速切换
	private void changeView(final String type, int way)
	{
		// 同一个界面不切换
		if (type.equals(mStrBusiType))
		{
			return;
		}
		else
		{
			if (mClassStack.size() > 1 || (way == 1))
			{
				DialogUtil.showSelectDialog("确认要退出当前流程吗?", new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						try
						{
							Class<?> cls = Class.forName(type);
							Intent intent = new Intent(FrameActivity.this, cls);
							if (mCurrentView != null)
							{
								mCurrentView.startAnimation(AnimationUtils.loadAnimation(FrameActivity.this, R.anim.left_out));
							}
							mLayContent.removeAllViews();
							getLocalActivityManager().removeAllActivities();
							
							View activityView = getLocalActivityManager().startActivity(type, intent).getDecorView();
							mLayContent.addView(activityView);
							activityView.startAnimation(AnimationUtils.loadAnimation(FrameActivity.this, R.anim.right_in));
							
							mCurrentView = activityView;
							mClassStack.clear();
							mClassStack.add(type);
						}
						catch (Exception e) 
						{
							e.printStackTrace();
						}
						mStrBusiType = type;
					}
				});
			}
			else
			{
				try
				{
					//Log.e("", String.format("aaa in %d ms", System.currentTimeMillis()));
					Class<?> cls = Class.forName(type);
					Intent intent = new Intent(FrameActivity.this, cls);
					if (mCurrentView != null)
					{
						mCurrentView.startAnimation(AnimationUtils.loadAnimation(FrameActivity.this, R.anim.left_out));
					}
					mLayContent.removeAllViews();
					getLocalActivityManager().removeAllActivities();
					//Log.e("", String.format("bbb in %d ms", System.currentTimeMillis()));
					
					View activityView = getLocalActivityManager().startActivity(type, intent).getDecorView();
					mLayContent.addView(activityView);
					activityView.startAnimation(AnimationUtils.loadAnimation(FrameActivity.this, R.anim.right_in));
					//Log.e("", String.format("ccc in %d ms", System.currentTimeMillis()));
					
					mCurrentView = activityView;
					mClassStack.clear();
					mClassStack.add(type);
					//Log.e("", String.format("ddd in %d ms", System.currentTimeMillis()));
				}
				catch (ClassNotFoundException e)
				{
					e.printStackTrace();
				}
				mStrBusiType = type;
			}
		}
	}
	
	// 返回到主界面
	private void backToHome()
	{
		DialogUtil.MsgBox("温馨提示", 
				"是否放弃当前业务办理, 返回主界面？", 
				"确定", 
				new OnClickListener() 
				{
					@Override
					public void onClick(View v)
					{
						getLocalActivityManager().removeAllActivities();
						FrameActivity.this.finish();
					}
				}, 
				"取消", 
				null, 
				null);
	}

	// 翻页动作
	private void showPage(int direction, Bundle data) throws ClassNotFoundException
	{
		String className = data.getString("className");
		data.putInt("direction", direction);
		Class<?> cls = Class.forName(className);
		mStrBusiType = className;
		
		Log.d("1111", "***********************************************");
		Intent intent = new Intent(this, cls);
		intent.putExtra("_data", data);
		View activityView = getLocalActivityManager().startActivity(className, intent).getDecorView();
		Log.d("2222", "***********************************************");
		
		switch (direction) 
		{
			case DIRECTION_NEXT:
				if (mCurrentView != null) 
				{
					mCurrentView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.left_out));
				}
				mLayContent.removeAllViews();
				mLayContent.addView(activityView);
				activityView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.right_in));
				mCurrentView = activityView;
				mClassStack.add(className);
				break;
				
			case DIRECTION_PREVIOUS:
				if (mCurrentView != null)
				{
					mCurrentView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.right_out));
				}
				mLayContent.removeAllViews();
				try
				{
					String currcls = mClassStack.pop();
					while (!currcls.equals(className))
					{
						try
						{
							getLocalActivityManager().destroyActivity(currcls, true);
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
						currcls = mClassStack.pop();
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}

				mLayContent.addView(activityView);
				activityView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.left_in));
				mCurrentView = activityView;
				mClassStack.add(className);
				break;
				
			default:
				break;
		}
		System.gc();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		try
		{
			Activity activity = getLocalActivityManager().getCurrentActivity();
			if (activity != null)
			{
				return activity.onKeyDown(keyCode, event);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		
		mBaseBusiness = null;
		System.gc();
		Log.e("", "FrameActivity onDestroy()");
	}
}
