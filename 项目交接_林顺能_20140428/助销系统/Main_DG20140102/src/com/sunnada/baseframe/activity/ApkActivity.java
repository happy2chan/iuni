package com.sunnada.baseframe.activity;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.SimpleAdapter;

import com.sunnada.baseframe.util.Log;

/**
 * 程序列表
 * @author aquan
 */
public class ApkActivity extends Activity implements OnItemClickListener
{
	private GridView gridView = null;
	private AppsAdapter appsAdapter = null;
	private List<Map<String, Object>> dataList = new ArrayList<Map<String,Object>>();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.apk_grid);
//		gridView = (GridView) findViewById(R.id.gridView);
		
		List<ResolveInfo> mApps = loadApps();
		
		dataList.clear();
		
		for(ResolveInfo ri:mApps)
		{
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("appText", ri.activityInfo.loadLabel(getPackageManager()));
			map.put("appImage", ri.activityInfo.loadIcon(getPackageManager()));
			map.put("packageName", ri.activityInfo.packageName);
			map.put("name", ri.activityInfo.name);
			
			dataList.add(map);
		}
		
//		appsAdapter = new AppsAdapter(this, dataList, R.layout.apk_grid_item, new String[]{"appText","appImage"}, new int[]{R.id.textView,R.id.imageView});
		gridView.setAdapter(appsAdapter);
		gridView.setOnItemClickListener(this);
	}

	private List<ResolveInfo> loadApps()
	{
		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		return getPackageManager().queryIntentActivities(mainIntent, 0);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int ps, long arg3)
	{
          //该应用的包名
          String pkg = (String) dataList.get(ps).get("packageName");
          //应用的主activity类
          String cls = (String) dataList.get(ps).get("name");
          
          if(getPackageName().equals(pkg.trim()))
          {//如果点击的是本应用程序则结束自己这个activity
        	  Log.log("自己的程旭", "点击的是自己的应用程序结束自己");
        	  finish();
        	  return;
          }
           
          ComponentName componet = new ComponentName(pkg, cls);
           
          Intent i = new Intent();
          i.setComponent(componet);
          startActivity(i);
	}

	public class AppsAdapter extends SimpleAdapter
	{
		public AppsAdapter(Context context,List<? extends Map<String, ?>> data, int resource,String[] from, int[] to)
		{
			super(context, data, resource, from, to);
		}
		
		public View getView(int position, View convertView, ViewGroup parent)
		{
			View view = super.getView(position, convertView, parent);
			
//			ImageView i = (ImageView) view.findViewById(R.id.imageView);

//			i.setImageDrawable((Drawable) dataList.get(position).get("appImage"));

			return view;
		}

		public final int getCount()
		{
			return dataList.size();
		}

		public final Object getItem(int position)
		{
			return dataList.get(position);
		}

		public final long getItemId(int position)
		{
			return position;
		}

	}
}
