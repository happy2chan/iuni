package com.sunnada.baseframe.activity.xhrw;

import android.content.Intent;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;

import com.sunnada.baseframe.dialog.CallBackChildHome;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ModifyPswdDialog;
import com.sunnada.baseframe.dialog.ResetSrvDialog;
import com.sunnada.baseframe.dialog.SearchBluetoothDialog;
import com.sunnada.baseframe.dialog.DialogUtil.OnGetListener;
import com.sunnada.baseframe.dialog.ICallBack;

import com.sunnada.baseframe.signature.WritePadDialog;
import com.sunnada.baseframe.ui.MyCustomButton;

import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.business.PickNetBusiness;
import com.sunnada.baseframe.util.ARGBUtil;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.PrintUtil;
import com.sunnada.baseframe.util.StringUtil;

public class PickNetSignatureActivity extends DgActivity implements OnClickListener
{
	private WritePadDialog 				dialog; 						 // 签名Dialog
	private ImageView 					iv_sign 		         = null;
	private TextView 					tv_content 		         = null;

	private PickNetBusiness 			mPickNetBusiness 	     = null; // 选号入网业务类

	private StringBuffer 				sPrintBuffer 	         = null;
	private Button                      mBtnPrevious             = null; // 回退按钮       
	private LinearLayout                mLayBack;                        // 回退LinearLayout
	
	private MyCustomButton				moblie_next 	         = null; // 完成按钮
	private MyCustomButton 				btn_cancel 		         = null; // 退出按钮

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			this.setContentView(R.layout.lay_signature_orange);
		}
		else
		{
			this.setContentView(R.layout.lay_signature);
		}
		
		mPickNetBusiness = (PickNetBusiness) FrameActivity.mBaseBusiness;

		// 回退LinearLayout
		mLayBack = (LinearLayout)this.findViewById(R.id.layBack);
		mLayBack.setOnClickListener(this);
		
		// 回退按钮
		mBtnPrevious = (Button) findViewById(R.id.btn_previous);
		mBtnPrevious.setOnClickListener(this); 
		
		// 完成按钮
		moblie_next = (MyCustomButton) findViewById(R.id.moblie_next);
		moblie_next.setTextViewText1("完      成");
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			moblie_next.setImageResource(R.drawable.btn_custom_check_orange);
		}
		else
		{
			moblie_next.setImageResource(R.drawable.btn_custom_check);
		}
		moblie_next.setOnTouchListener(this);
		moblie_next.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				back(new ICallBack() 
				{
					@Override
					public void back() 
					{
						if(Statics.HAD_PWD)
						{
							showPwdInputDialog();
						}
						else
						{
							if(Statics.MODIFY_TYPE == 1)
							{
								DialogUtil.MsgBox("温馨提示", "您还未设置过初始密码，请先进行密码初始化！", 
								"确定", Statics.MSG_MODIFY_PSWD, "取消", -1, -1, mHandler);
							}
							else if(Statics.MODIFY_TYPE == 2)
							{
								DialogUtil.MsgBox("温馨提示", "您还未执行过交易密码重置，请先重置交易密码！", 
										"确定", Statics.MSG_RESET_PSWD, "取消", -1, -1, mHandler);
							}
						}
					}
					
					@Override
					public void cancel() 
					{
					}

					@Override
					public void dismiss() 
					{
					}
				}, null, "确认提交吗?", "确定", "取消", true, false, false);
			}
		});

		// 退出按钮
		btn_cancel = (MyCustomButton) findViewById(R.id.btn_cancel);
		btn_cancel.setTextViewText1("退      出");
		btn_cancel.setImageResource(R.drawable.quit);
		btn_cancel.setOnTouchListener(this);
		btn_cancel.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				back(new ICallBack() 
				{
					@Override
					public void back() 
					{
						Message msg = Message.obtain();
						msg.what = FrameActivity.FINISH;
						FrameActivity.mHandler.sendMessage(msg);
					}
					
					@Override
					public void cancel() 
					{
					}

					@Override
					public void dismiss() 
					{
					}
				}, null, null, "确定", "取消", true, false, false);
			}
		});
		
		
		tv_content = (TextView) findViewById(R.id.tv_content);
		iv_sign = (ImageView) findViewById(R.id.iv_sign);
		iv_sign.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				if (dialog == null) 
				{
					dialog = new WritePadDialog(PickNetSignatureActivity.this);
					dialog.setOnBitmapGetListener(new WritePadDialog.OnBitmapGetListener() 
					{
						@Override
						public void onBitmapGet(Bitmap bitmap) 
						{
							//这里不能回收原图,否则重新签名时会出错
							iv_sign.setImageBitmap(bitmap);
							iv_sign.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_signatured));
						}
					});
				}
				dialog.show();
			}
		});
		
		// 初始化显示
		// initShow();
	}

	// 初始化显示
	public void initShow()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			// 置换背景图片
			LinearLayout linearBg = (LinearLayout) findViewById(R.id.linear_bg);
			linearBg.setBackgroundResource(R.drawable.bg_main_orange);
			// 主题
			TextView textView1 = (TextView) findViewById(R.id.textView1);
			// 设置主题文字颜色为黄色
			textView1.setTextColor(ARGBUtil.getArgb(2));
		}
	}
	
	@Override
	public void onClick(View view)
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(view.getId(), 1000)) 
		{
			return;
		}
				
		switch (view.getId()) 
		{
		case R.id.btn_previous:
			back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
			break;
		
		case R.id.layBack:
			back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onNewIntent(Intent intent) 
	{
		super.onNewIntent(intent);
		
		Bundle bundle = intent.getBundleExtra("_data");
		if (bundle == null) 
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS) 
		{
			return;
		}
		getIntent().putExtra("_data", bundle);
	}

	@Override
	protected void onResume() 
	{
		super.onResume();
		
		DialogUtil.init(this);
		Bundle bundle = getIntent().getBundleExtra("_data");

		if (bundle == null) 
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS) 
		{
			return;
		}
		
		sPrintBuffer = new StringBuffer();
		sPrintBuffer.append("业务类型:3G选号入网\n")
					.append("【费用信息】\n")
					.append(mPickNetBusiness.feiyong)
				    .append("【用户信息】\n")
				    .append("客户名称:")
				    .append(mPickNetBusiness.username)
				    .append("\n")
				    .append("证件号码:")
				    .append(mPickNetBusiness.Identity)
				    .append("\n")
				    .append("证件地址:")
				    .append(mPickNetBusiness.address)
				    .append("\n");

		if(!StringUtil.isEmptyOrNull(mPickNetBusiness.packageName))
		{
			sPrintBuffer.append("【套餐名称】\n")
						.append(mPickNetBusiness.packageName);
		}
	
		if (!StringUtil.isEmptyOrNull(mPickNetBusiness.res)) 
		{
			sPrintBuffer.append("\n")
						.append("【套餐信息】\n")
						.append(mPickNetBusiness.res);
		}
	
		if (mPickNetBusiness.mSelectContractType != 0) 
		{
			sPrintBuffer.append("\n【活动类型】\n")
						.append(adjustContractTypeShow(mPickNetBusiness.mSelectContractType))
						.append("\n");
		}
		
		tv_content.setText(sPrintBuffer.toString()+"\n\n客户已阅读上述信息，并同意支付费用。");
		
		if (dialog != null) 
		{
			dialog.clear();
		}
	}

	private void checkPwd(final String pwd) 
	{
		mPickNetBusiness.password = pwd;
		DialogUtil.showProgress("正在提交,请稍候");

		new Thread() 
		{
			public void run() 
			{
				if(Statics.IS_DEBUG)
				{
					DialogUtil.closeProgress();
					back(new ICallBack()
					{
						@Override
						public void back() 
						{
							print();
						}

						@Override
						public void cancel() 
						{
							Message msg = Message.obtain();
							msg.what = FrameActivity.FINISH;
							FrameActivity.mHandler.sendMessage(msg);
						}

						@Override
						public void dismiss() 
						{
							Message msg = Message.obtain();
							msg.what = FrameActivity.FINISH;
							FrameActivity.mHandler.sendMessage(msg);
						}
					}, null, "开户成功,是否打印凭据?", "确定", "取消", true, true, true);
					return;
				}
				int ret = 0;
				if (StringUtil.isEmptyOrNull(pwd) || (pwd.length() != 6)) 
				{
					DialogUtil.closeProgress();
					back(new ICallBack()
					{
						@Override
						public void back() 
						{
							showPwdInputDialog();
						}

						@Override
						public void cancel() 
						{
						}

						@Override
						public void dismiss() 
						{
						}
					}, null, "请输入六位交易密码", "确定", null, true, false, false);
					return;
				}

				runOnUiThread(new Runnable() 
				{
					public void run() 
					{
						DialogUtil.showProgress("正在进行开户");
					}
				});

				ret = mPickNetBusiness.playOpenCard();

				if (ret != ReturnEnum.SUCCESS) 
				{
					runOnUiThread(new Runnable() 
					{
						public void run() 
						{
							DialogUtil.closeProgress();
							
							if(socketService.getLastknownErrCode() == 0xFFFFFFFC)
							{
								back(new ICallBack() 
								{
									@Override
									public void back() 
									{
										showPwdInputDialog();
									}
									
									@Override
									public void cancel() 
									{
										Message msg = Message.obtain();
										msg.what = FrameActivity.FINISH;
										FrameActivity.mHandler.sendMessage(msg);
									}

									@Override
									public void dismiss() 
									{
										Message msg = Message.obtain();
										msg.what = FrameActivity.FINISH;
										FrameActivity.mHandler.sendMessage(msg);
									}
								}, null, "交易密码错误，请重新输入交易密码。", "重试", "放弃", true, true, true);
							}
							else
							{
								back(new ICallBack() 
								{
									@Override
									public void back() 
									{
										Message msg = Message.obtain();
										msg.what = FrameActivity.FINISH;
										FrameActivity.mHandler.sendMessage(msg);
									}
									
									@Override
									public void cancel() 
									{
									}

									@Override
									public void dismiss() 
									{
										Message msg = Message.obtain();
										msg.what = FrameActivity.FINISH;
										FrameActivity.mHandler.sendMessage(msg);
									}
								}, null, "开户失败!\n失败原因:["
										+ mPickNetBusiness.getLastknownError() + "]",
								"确定", null, true, false, true);
							}
						}
					});
				} 
				else 
				{
					runOnUiThread(new Runnable() 
					{
						public void run() 
						{
							DialogUtil.closeProgress();
							
							back(new ICallBack() 
							{	
								@Override
								public void back() 
								{
									print();
								}
								
								@Override
								public void cancel() 
								{
									Message msg = Message.obtain();
									msg.what = FrameActivity.FINISH;
									FrameActivity.mHandler.sendMessage(msg);
								}

								@Override
								public void dismiss() 
								{
									Message msg = Message.obtain();
									msg.what = FrameActivity.FINISH;
									FrameActivity.mHandler.sendMessage(msg);
								}
							}, null, "开户成功,是否打印凭据?", "确定", "取消", true, true, true);
						}
					});

				}

			}
		}.start();

	}

	private void print() 
	{
		DialogUtil.showProgress("正在打印,请稍候");
		new Thread() 
		{
			public void run() 
			{
				try 
				{
					// 检测蓝牙设备
					if(equipmentService.handshake() == false) 
					{
						DialogUtil.closeProgress();
						DialogUtil.MsgBox("请连接蓝牙设备", 
								"检测到当前未连接蓝牙打印设备，是否立即搜索并连接蓝牙打印设备？", 
								"立即搜索", 
								Statics.MSG_CONNECT_BLUETOOTH, 
								"取消", 
								Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
								Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
								mHandler);
						return;
					}
					
					StringBuffer tempBuff = new StringBuffer();
					tempBuff.append(sPrintBuffer.toString());
					
					if (!StringUtil.isEmptyOrNull(mPickNetBusiness.mContractContent)) 
					{
						tempBuff.append("【合约内容】\n"
								+ mPickNetBusiness.mContractContent+"\n");
					}
					else
					{
						tempBuff.append("\n");
					}
					if(!StringUtil.isEmptyOrNull(mPickNetBusiness.mTranceTime));
					{
						tempBuff.append("【交易时间】\n"+mPickNetBusiness.mTranceTime);
					}
					
					// 开始打印
					mPickNetBusiness.print(tempBuff, new String(mPickNetBusiness.bllnum, "ASCII"));
				} 
				catch (Exception e) 
				{
					DialogUtil.closeProgress();
					e.printStackTrace();
				}
				
				DialogUtil.closeProgress();
				if(!equipmentService.isPrintSuccess())
				{
					showRePrintDialog();
				}
				else
				{
					back(new ICallBack() 
					{
						@Override
						public void back() 
						{
							goToHome();
						}
						
						@Override
						public void dismiss() 
						{
							goToHome();
						}
						
						@Override
						public void cancel() 
						{
						}
					}, 
					PrintUtil.STR_SUCCESS_TITLE, 
					PrintUtil.STR_SUCCESS_MSG, 
					PrintUtil.STR_SUCCESS_OK, 
					null , true, false, true);
				}
			};
		}.start();
	}
	
	// 调整合约类型的显示
	public String adjustContractTypeShow(int contractType) 
	{
		String contractContent = "";
		switch (contractType) 
		{
		case 0:
			contractContent = "不参加活动";
				break;
			
			case 1:
			contractContent = "存费送费";
				break;
			
			case 2:
				break;
				
			case 3:
				break;
		}
		return contractContent;
	}
	
	// 显示输入密码对话框
	private void showPwdInputDialog()
	{
		DialogUtil.MsgBox("提示信息", R.layout.lay_pwd, "确定", "取消", null, new OnGetListener() 
		{
			@Override
			public void onGet(String value) 
			{
				checkPwd(value);
			}
		}, null);
	}
	
	// 显示是否重新打印选择框
	private void showRePrintDialog()
	{
		back(new ICallBack() 
		{
			@Override
			public void back() 
			{
				print();
			}
			
			@Override
			public void cancel() 
			{
				Message msg = Message.obtain();
				msg.what = FrameActivity.FINISH;
				FrameActivity.mHandler.sendMessage(msg);
			}

			@Override
			public void dismiss() 
			{
				Message msg = Message.obtain();
				msg.what = FrameActivity.FINISH;
				FrameActivity.mHandler.sendMessage(msg);
			}
		},
		PrintUtil.STR_FAIL_TITLE,
		PrintUtil.errorMsgPgAndGet(equipmentService.getPrintErrorMessage()),
		PrintUtil.STR_FAIL_OK,
		PrintUtil.STR_FAIL_CANCEL, 
		true, true, true);
	}
	
	// 确定时的动作处理
	protected Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch (message.what) 
			{
			// 连接蓝牙设备
			case Statics.MSG_CONNECT_BLUETOOTH:
				SearchBluetoothDialog dialog = new SearchBluetoothDialog(PickNetSignatureActivity.this, mBluetoothClient, equipmentService, 
						mHandler, Statics.MSG_CONNECT_DONE);
				dialog.show();
				break;
				
			// 连接蓝牙设备完成
			case Statics.MSG_CONNECT_DONE:
				new Thread() 
				{
					public void run() 
					{
						/*
						// 检测蓝牙设备
						DialogUtil.showProgress("正在检测蓝牙设备状态...");
						if(equipmentService.handshake() == false) 
						{
							DialogUtil.closeProgress();
							mHandler.sendEmptyMessage(Statics.MSG_CONNECT_BLUETOOTH_CANCEL);
							return;
						}
						DialogUtil.closeProgress();
						*/
						print();
					}
				}.start();
				break;
				
			// 取消蓝牙或蓝牙连接失败
			case Statics.MSG_CONNECT_BLUETOOTH_CANCEL:
				runOnUiThread(new Runnable() 
				{
					public void run() 
					{
						Message msg = Message.obtain();
						msg.what = FrameActivity.FINISH;
						FrameActivity.mHandler.sendMessage(msg);
					}
				});
				break;
				
				// 初始化交易密码
			case Statics.MSG_MODIFY_PSWD:
				ModifyPswdDialog dialog1 = new ModifyPswdDialog(PickNetSignatureActivity.this, 
				new Business_00(PickNetSignatureActivity.this, equipmentService, socketService, dataBaseService), mHandler);
				dialog1.show();
				break;
				
			// 重置交易密码
			case Statics.MSG_RESET_PSWD:
				ResetSrvDialog dialog2 = new ResetSrvDialog(PickNetSignatureActivity.this, 
				new Business_00(PickNetSignatureActivity.this, equipmentService, socketService, dataBaseService), mHandler);
				dialog2.show();
				break;
				
			// 交易密码修改成功
			case Statics.MSG_MODIFY_PSWD_SUCCESS:
				showPwdInputDialog();
				break;
			default:
				break;
			}
		}
	};
		
	// 回到主界面
	private void goToHome()
	{
		Message msg = Message.obtain();
		msg.what = FrameActivity.FINISH;
		FrameActivity.mHandler.sendMessage(msg);
	}
	
	@Override
	protected void onDestroy() 
	{
		System.gc();
		super.onDestroy();
	}
	/*
	private void afterOpenCard() 
	{
		runOnUiThread(new Runnable() 
		{
			@Override
			public void run() 
			{
				DialogUtil.showProgress("正在上传图片");
			}
		});
		
		// 开卡成功!此时处理照片上传的事件
		try 
		{
			String bllNum = "";
			if (LaunchActivity.isDebuge) 
			{
			} 
			else 
			{
				bllNum = mPickNetBusiness.sBllnum; // 流水号
			}
			// 此时文件已经被转移到流水号目录下了
			File jpgFolder = new File(Environment.getExternalStorageDirectory()
					.toString()
					+ this.getString(R.string.identify_photo_path)
					+ mPickNetBusiness.sPicFolder);
			File newFolder = new File(Environment.getExternalStorageDirectory()
					.toString()
					+ this.getString(R.string.identify_photo_path)
					+ bllNum);
			jpgFolder.renameTo(newFolder);					//这里把时间戳的文件名更改为流水号的文件名

			if (jpgFolder != null && jpgFolder.exists()) 
			{
				files = jpgFolder.listFiles();

				int photoCount = files.length;
				StringBuffer fileName = new StringBuffer(); // 图片名的拼接

				// 拼装所需要的各种参数
				for (int i = 0; i < files.length; i++) 
				{
					 // 跳过bin文件
					if (!files[i].getName().endsWith(".jpg")) 
					{
						continue;
					}
					if (i != 0) 
					{
						fileName.append(",");
					}
					fileName.append(files[i].getName());
				}
				uploadImgs();
				updateImgLogInfo(bllNum, photoCount, fileName.toString());
				files = null;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		runOnUiThread(new Runnable() 
		{
			@Override
			public void run() 
			{
				DialogUtil.closeProgress();
			}
		});
	}
	*/
	/**
	 * 把要上传的文件路径等加入到本地数据库,等待上传
	 */
	/*
	private void uploadImgs() 
	{
		final String textMark = "仅供中国联通开户专用";
		String psamId = "";
		if (LaunchActivity.isDebuge) 
		{
			psamId = "51000000000001";
		} 
		else 
		{
			try 
			{
				psamId = equipmentService.getPsamId();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}

		// 要提交的目录
		StringBuffer remotePath = new StringBuffer(
				getString(R.string.ftp_image_upload_folder)); 
		remotePath.append("/" + psamId.substring(0, 2));
		remotePath.append("/" + psamId.substring(2, 4));
		remotePath.append("/" + psamId);
		if (LaunchActivity.isDebuge) 
		{
			// 目录是/Photos/省份/地市/psamid/流水号"的格式
			remotePath.append("/"+"201304301111222233334444"
					+ (int) (Math.random() * 899999 + 100000) ); 
		} 
		else 
		{
			// 目录是/Photos/省份/地市/psamid/流水号的格式
			remotePath.append("/" + mPickNetBusiness.sBllnum); 
		}
		
		for (int i = 0; i < files.length; i++) 
		{
			File img = files[i];
			// 图像文件
			if (img.isFile() && img.getName().endsWith(".jpg")) 
			{ 
				try 
				{
					// 身份证照片打水印
					PictureShowUtils.addTextMark(img.getAbsolutePath(),
							textMark); 
					imgLogAdapter.addUploadLog(img.getAbsolutePath(),
							remotePath + "/" + img.getName());
				} 
				catch (Exception e1) 
				{
					e1.printStackTrace();
				}
			} 
			
			// 身份证识别
			else if (img.isFile() && img.getName().endsWith(".bin")) 
			{ 
			//导购系统暂时不会用到这一块代码
			//try {
			//	imgLogAdapter
			//			.addUploadLog(
			//					img.getAbsolutePath(),
								getString(R.string.ftp_image_upload_folder)
										+ "/idfile/" + img.getName());
			//	} 
			//	catch (Exception e1) 
			//	{
			//		e1.printStackTrace();
			//}
			}
		}
		Log.v("xxxxxx","上传信息保存完毕");
	}
	
	
	// 本地缓存并上传照片日志信息
	private void updateImgLogInfo(final String bllNum, final int photoCount, final String fileName) 
	{
		synchronized (this) 
		{
			// 本地缓存
			imgLogAdapter.addImgLog(bllNum, String.valueOf(photoCount), fileName);

			ImgLogReport report = new ImgLogReport(this,equipmentService,socketService,dataBaseService);

			int flag = 0;
			if (LaunchActivity.isDebuge) 
			{
				flag = ReturnEnum.SUCCESS;
			} 
			else 
			{
				flag = report.bll0F80(bllNum, photoCount, fileName);
			}

			if (flag == ReturnEnum.SUCCESS) 
			{
				imgLogAdapter.deleteImgLog(bllNum);
			}
		}
	}
	*/
	
	@Override
	public void onInitSuccess() 
	{
	}

	@Override
	public void onInitFail() 
	{
	}
}
