package com.sunnada.baseframe.activity.selfservice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.InvoiceData;
import com.sunnada.baseframe.bean.InvoiceDataMonth;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.business.Business_0F;
import com.sunnada.baseframe.database.InvoicePrintSharp;
import com.sunnada.baseframe.dialog.DateTimePickerDialog;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ICallBack;
import com.sunnada.baseframe.dialog.InvoicePrintDialog;
import com.sunnada.baseframe.dialog.ModifyPswdDialog;
import com.sunnada.baseframe.dialog.ResetSrvDialog;
import com.sunnada.baseframe.dialog.SearchBluetoothDialog;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonGroupUtil;
import com.sunnada.baseframe.util.ButtonGroupUtil.ButtonGroupClickListener;
import com.sunnada.baseframe.util.ButtonGroupUtil.ViewGetter;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;

// 发票打印
public class BusiInvoicePrint extends DgActivity implements OnClickListener, ViewGetter, OnScrollListener 
{
	private Business_0F              mBusiness0F;              // 业务类
	private final String             TITLE_STR                 = "温馨提示";
	private final int                INVOICE_PRINT_STEPONE     = 0x01;
	private final int                INVOICE_PRINT_STETTWO     = 0x02;
	private final int                INIT_INVOICE              = 0x03;
	private final int                INVOICE_VALIDATE          = 0x04;
	// 左边控件
	private LinearLayout             mLinearBackToSelfservice;  // 返回到自助服务界面
	private TextView                 mTvLeftInvoice;            // 业务标题
	private ImageView                mIvLeftInvoice;            // 业务图标
	// 第一步相关
	private RelativeLayout           mLayInvoiceQuery;          // 发票查询界面基布局
	private ButtonGroupUtil		     mPrintTypeUtil;            // 打印类型组
	private final int[]              mPrintTypeUtilIds          = {R.id.btn_invoice_gave, R.id.btn_invoice_statement};
	private TextView                 mTvBusiType;
	private EditText                 mEtBusiType;               // 业务类型
	private ImageView                mIvBusiType;          
	private LinearLayout             mNonSjBusiNum;             // 非手机交易类型的交易号码基布局
	private EditText                 mEtNumPre;                 // 区号
	private EditText                 mEtNumBack;                // 号码
	private EditText                 mEtNumBack0;               // 号码
	private LinearLayout             mSjBusiNum;                // 手机交易类型的交易号码基布局
	private EditText                 mSjNum;                    // 手机号码
	private LinearLayout             mLinearGaveSearchTime;     // 现金发票查询时间基布局
    private EditText                 mEtSearchTime;             // 查询时间
    private ImageView                mIvSearchTime;
    private LinearLayout             mLinearStatementSeartTime; // 月结发票查询时间基布局
    private EditText                 mEtStartTime;              // 起始时间
    private ImageView                mIvStartTime;                 
    private EditText                 mEtEndTime;                // 结束时间
    private ImageView                mIvEndTime;
    private EditText                 mEtServerPwd;              // 服务密码
    private MyCustomButton           mBtnQuery;                 // 查询
    private Button                   mBtnInvoiceValidate;       // 发票校验
    private ListView                 mLvBusi;                   // 显示业务类型
    private String[]                 mStrBusiArr                = {
    																"手机缴费", "固话缴费",
    																"小灵通缴费", "宽带ADSL缴费",
    																"宽带LAN缴费"
    															  };
    private int[]                    mBusiTypes                 = {0x04, 0x05, 0x0D, 0x0E, 0x0F};
    private boolean                  mIsLvBusiHide              = true;   // 对账类型ListView是否已隐藏
	private int                      mPrintType                 = 0x00;   // 打印类型
	private boolean                  mIsBroadbandQuery          = false;
    // 第二步相关
    private LinearLayout             mLinearInvoicePrint;       // 发票打印界面
    private LinearLayout             mLinearInvoiceGaveTitle;
    private LinearLayout             mLinearInvoiceStatementTitle;
    private TextView                 mTvPrintNavigation;        // 打印导航
    private TextView                 mTvBusiNum;                // 交易号码
    private ListView                 mInvoiceGaveList;          // 现金发票列表
    private ListView                 mInvoiceStatementList;     // 月结发票列表
    private int                      mLastItem;
    private boolean                  mIsScrollOver              = true; // 是否滚动事件结束
    private View				     mMoreView;                 // 加载更多页面
    private int                      mGaveInvoicePos;
    private int                      mStatementInvoicePos;
    // 发票校准界面控件
    private RelativeLayout           mLayInvoiceValidate;       // 发票校准界面
    private ButtonGroupUtil          mInvoiceValidateUtil;      // 发票检验组
    private final int[]              mInvoiceValidateUtilIds    = {R.id.btn_begin_invoice_num, R.id.btn_adjust_invoice_num};
    private TextView                 mTvStartInvoiceNum;        // 起始发票号码
    private EditText                 mInputInvoiceNum;
    private TextView                 mTvInvoiceCode;            // 发票代码
    private TextView                 mInputInvoiceCode;
    private MyCustomButton           mBtnSet;                   // 设置
    private TextView                 mTvInvoiceValidateHint;    
    private TextView                 mTvCurrDeviceInvoiceNum;
    private TextView                 mTvCurrDeviceInvoiceCode;
    private Button                   mBackToInvoicePrint;       // 返回到发票打印界面 
    
    private int                      mInvoiceValidateType;      // 发票校验类型
    private final int[]              mInvoiceValidateTypes      = {0x04, 0x07}; 
    private boolean                  mIsInitInvoice             = false; // 是否是初始化发票号设置
    private boolean                  mIsValidateInvoice         = false; // 是否是调整发票号设置
    private boolean                  mIsInitInvoiceQueryWithParam = false; // 是否是带参数初始化发票查询
    private boolean                  mIsInitInvoiceQuery        = false; // 是否是初始化发票查询
    private boolean                  mIsHasInitInvoiceQuery     = false; // 是否已经进行初始化查询
    private String                   mTempBlueToothMac          = null;  // 临时保存蓝牙Mac地址
    
    private ArrayAdapter<String>     mAdapter                   = null;
    // 使用ButtonGroupUtil改变字体颜色设置
 	private final int[][]            mFontColor                 = {{0xff,0xff,0xff,0xff,0xff},{0xff,0x67,0xa7,0xc1}};
 	private final int[][]            mFontColorOrange           = {{0xff,0xff,0xff,0xff,0xff},{0xff,0xff,0x96,0x00}};
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_selfservice_invoiceprint_orange);
		}
		else
		{
			setContentView(R.layout.lay_selfservice_invoiceprint);
		}
		mBusiness0F = new Business_0F(this, equipmentService, socketService, dataBaseService);
		// 初始化控件
		initViews();
		// 初始化显示
		initShow();
		// 初始化监听
		setListener();
		showView(INVOICE_PRINT_STEPONE, 0, 0);
	}
	
	// 初始化控件
	private void initViews() 
	{
		// 初始化左边控件
		initLeftView();
		// 初始化第一步
		initStepOne();
		// 初始化第二步
		initStepTwo();
		// 初始化发票校准界面
		initInvoiceValidateView();
	}
	
	private void initLeftView()
	{
		mLinearBackToSelfservice = (LinearLayout) findViewById(R.id.linear_backto_selfservice);
		mTvLeftInvoice = (TextView) findViewById(R.id.tv_left_invoice);           
		mIvLeftInvoice = (ImageView) findViewById(R.id.iv_left_invoice);           
	}
	
	// 初始化第一步
	public void initStepOne()
	{
		mLayInvoiceQuery = (RelativeLayout) findViewById(R.id.lay_invoice_query);
		// 初始打印类型组
		try 
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mPrintTypeUtil = ButtonGroupUtil.createBean(this, mPrintTypeUtilIds, new String[]
						{ "现金发票", "月结发票" }, R.drawable.btn_tip_select_orange, R.drawable.btn_tick_selected_orange, mFontColorOrange);
			}
			else
			{
				mPrintTypeUtil = ButtonGroupUtil.createBean(this, mPrintTypeUtilIds, new String[]
						{ "现金发票", "月结发票" }, R.drawable.btn_tip_select, R.drawable.btn_tick_selected, mFontColor);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		mTvBusiType = (TextView) findViewById(R.id.tv_busitype);
		mEtBusiType = (EditText) findViewById(R.id.etBusiType);            
		mIvBusiType = (ImageView) findViewById(R.id.iv_busitype);          
		mNonSjBusiNum = (LinearLayout) findViewById(R.id.linear_non_sj_businum);           
		mEtNumPre = (EditText) findViewById(R.id.et_num_pre);              
		mEtNumBack = (EditText) findViewById(R.id.et_num_back);    
		mEtNumBack0 = (EditText) findViewById(R.id.et_num_back0);
		mSjBusiNum = (LinearLayout) findViewById(R.id.linear_sj_businum);           
		mSjNum = (EditText) findViewById(R.id.et_sjnum);                   
		mLinearGaveSearchTime = (LinearLayout) findViewById(R.id.linear_gave_searchtime);    
	    mEtSearchTime = (EditText) findViewById(R.id.et_search_time);           
	    mIvSearchTime = (ImageView) findViewById(R.id.iv_searchtime);
	    mLinearStatementSeartTime = (LinearLayout) findViewById(R.id.linear_statement_searchtime); 
	    mEtStartTime = (EditText) findViewById(R.id.et_begin_time);           
	    mIvStartTime = (ImageView) findViewById(R.id.iv_begintime);                 
	    mEtEndTime = (EditText) findViewById(R.id.et_end_time);               
	    mIvEndTime = (ImageView) findViewById(R.id.iv_endtime);
	    mEtServerPwd = (EditText) findViewById(R.id.et_server_pwd);              
	    mBtnQuery = (MyCustomButton) findViewById(R.id.btn_query);   
	    mBtnQuery.setTextViewText1("查询");
	    
	    mBtnInvoiceValidate = (Button) findViewById(R.id.btn_invoice_validate);    
	    
	    mLvBusi = (ListView) findViewById(R.id.lv_busi);
	    mLvBusi.setVisibility(View.GONE);
	    // 初始化业务类型 
	    // initLvBusi();
	}
	
	// 初始化业务类型
	public void initLvBusi()
	{
		mAdapter = new ArrayAdapter<String>(
				BusiInvoicePrint.this,
				R.layout.lay_selfservice_invoiceprint_busilistitem, 
				R.id.tv_text,
				mStrBusiArr);
		mLvBusi.setAdapter(mAdapter);
	}
	
	// 初始化第二步
	public void initStepTwo()
	{
		mLinearInvoicePrint = (LinearLayout) findViewById(R.id.linear_invoice_print);
		mLinearInvoiceGaveTitle = (LinearLayout) findViewById(R.id.linear_invoice_gave_title);
		mLinearInvoiceStatementTitle = (LinearLayout) findViewById(R.id.linear_invoice_statement_title);
		mTvPrintNavigation = (TextView) findViewById(R.id.tv_print_navigation);       
	    mTvBusiNum = (TextView) findViewById(R.id.tv_busi_num);           
	    mInvoiceGaveList = (ListView) findViewById(R.id.invoice_gave_list);
	    mInvoiceStatementList = (ListView) findViewById(R.id.invoice_statement_list);
	  
	    mMoreView = getLayoutInflater().inflate(R.layout.load, null);
		mMoreView.setVisibility(View.GONE);
	    // 添加底部view，一定要在setAdapter之前添加，否则会报错。
		// 为现金发票列表添加底部view
		mInvoiceGaveList.addFooterView(mMoreView); 	
		// 为现金发票列表添加底部透明效果
		mInvoiceGaveList.setVerticalFadingEdgeEnabled(true);
		mInvoiceGaveList.setFadingEdgeLength(100);
 		// 设置listview的滚动事件
		mInvoiceGaveList.setOnScrollListener(this); 	
		// 为月结发票列表添加底部透明效果
		mInvoiceStatementList.setVerticalFadingEdgeEnabled(true);
		mInvoiceStatementList.setFadingEdgeLength(100);
	}
	
	// 初始化发票校准界面
	public void initInvoiceValidateView()
	{
		mLayInvoiceValidate = (RelativeLayout) findViewById(R.id.lay_invoice_validate);
		try 
		{
		    if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		    {
		    	mInvoiceValidateUtil = ButtonGroupUtil.createBean(this, 
 	                   mInvoiceValidateUtilIds,
 	                   new String[]{"起始发票号", "调整发票号"},
		                   R.drawable.btn_tip_select_orange, 
		                   R.drawable.btn_tick_selected_orange, 
		                   mFontColorOrange);
		    }
		    else
		    {
		    	mInvoiceValidateUtil = ButtonGroupUtil.createBean(this, 
 	                   mInvoiceValidateUtilIds,
 	                   new String[]{"起始发票号", "调整发票号"},
		                   R.drawable.btn_tip_select, 
		                   R.drawable.btn_tick_selected, 
		                   mFontColor);
		    }
		    
		    mInvoiceValidateUtil.setClickListener(new ButtonGroupClickListener() 
		    {
				@Override
				public void onClick(int previousId, int newId)
				{
					if(previousId == newId)
					{
						return;
					}
					
					switch(newId)
					{
					case R.id.btn_begin_invoice_num:
						mInvoiceValidateType = mInvoiceValidateTypes[0];
						mBusiness0F.mInvoiceType = mInvoiceValidateTypes[0];
						mTvStartInvoiceNum.setText("起始发票号:");
						mInputInvoiceNum.setText("");
						mTvInvoiceCode.setVisibility(View.VISIBLE);
						mInputInvoiceCode.setVisibility(View.VISIBLE);
						mInputInvoiceCode.setText("");
						mTvInvoiceValidateHint.setText("");
						mTvInvoiceValidateHint.setText("新装发票纸时, 请在发票校准功能中设置发票纸起始发票号.");
						initInvoiceQuery();
						break;
					
					case R.id.btn_adjust_invoice_num:
						mInvoiceValidateType = mInvoiceValidateTypes[1];
						mBusiness0F.mInvoiceType = mInvoiceValidateTypes[1];
						mTvStartInvoiceNum.setText("调整发票号:");
						mInputInvoiceNum.setText("");
						mTvInvoiceCode.setVisibility(View.GONE);
						mInputInvoiceCode.setVisibility(View.GONE);
						mTvInvoiceValidateHint.setText("当前发票号码与系统当前发票号码不一致时,在此调整发票号码.");
						initInvoiceQuery();
						break;
					}
				}
			});
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	    mTvStartInvoiceNum = (TextView) findViewById(R.id.tv_start_invoice_num);       
	    mInputInvoiceNum = (EditText) findViewById(R.id.input_invoice_num);
	    mTvInvoiceCode = (TextView) findViewById(R.id.tv_invoice_code);     
	    mInputInvoiceCode = (EditText) findViewById(R.id.input_invoice_code);
	    mBtnSet = (MyCustomButton) findViewById(R.id.btn_set);  
	    mBtnSet.setTextViewText1("设置");
	    mTvInvoiceValidateHint = (TextView) findViewById(R.id.tv_invoice_validate_hint);    
	    mTvCurrDeviceInvoiceNum = (TextView) findViewById(R.id.tv_currdevice_invoice_num);
	    mTvCurrDeviceInvoiceCode = (TextView) findViewById(R.id.tv_currdevice_invoice_code);
	    mBackToInvoicePrint = (Button) findViewById(R.id.btn_backto_invoiceprint);     
	}
	
	public void setListener()
	{
		mLinearBackToSelfservice.setOnClickListener(this);
		/*第一步相关*/
		mPrintTypeUtil.setClickListener(new ButtonGroupClickListener()
		{
			@Override
			public void onClick(int previousId, int newId)
			{
				if(previousId == newId)
				{
					return;
				}
				
				mLvBusi.setVisibility(View.GONE);
				mIsLvBusiHide = true;
				switch (newId) 
				{
				case R.id.btn_invoice_gave:
					mPrintType = 0x00;
					showView(INVOICE_PRINT_STEPONE, 1, 0);
					mTvBusiType.setText("业务类型:");
					break;
					
				case R.id.btn_invoice_statement:
					mPrintType = 0x01;
					showView(INVOICE_PRINT_STEPONE, 1, 1);
					mTvBusiType.setText("号码类型:");
					break;
				default:
					break;
				}
			}
		});
		
		mIvBusiType.setOnClickListener(this);    
		mLvBusi.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long id) 
			{
				mLvBusi.setVisibility(View.GONE);
				mIsLvBusiHide = true;
				mBusiness0F.mBusiType = mBusiTypes[position];
				mEtBusiType.setText(mStrBusiArr[position]);
				
				switch (position) 
				{
				case 0:
					mNonSjBusiNum.setVisibility(View.GONE);
					mSjBusiNum.setVisibility(View.VISIBLE);
					mSjNum.setText("");
					break;
				
				default:
					mNonSjBusiNum.setVisibility(View.VISIBLE);
					mSjBusiNum.setVisibility(View.GONE);
					mEtNumBack.setText("");
					mEtNumPre.setText("");
					
					if((position == 1) || (position == 2))
					{
						mIsBroadbandQuery = false;
						mEtNumBack.setVisibility(View.VISIBLE);
						mEtNumBack0.setVisibility(View.GONE);
					}
					else
					{
						mIsBroadbandQuery = true;
						mEtNumBack0.setVisibility(View.VISIBLE);
						mEtNumBack.setVisibility(View.GONE);
					}
					break;
				}
				mEtSearchTime.setText("");
				mEtStartTime.setText("");
				mEtEndTime.setText("");
				mEtServerPwd.setText("");
			}
		});
		
		mEtNumPre.setOnEditorActionListener(new TextView.OnEditorActionListener() 
		{
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) 
			{
				if (actionId == R.id.nextStep) 
				{
					if(mIsBroadbandQuery)
					{
						mEtNumBack0.requestFocus();
					}
					else
					{
						mEtNumBack.requestFocus();
					}
					return true;
				}
				return false;
			}
		});
		
		mEtNumPre.addTextChangedListener(new TextWatcher() 
		{
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (mEtNumPre.getText().length() == 4) 
				{
					mEtNumPre.clearFocus();
					if(mIsBroadbandQuery)
					{
						mEtNumBack0.requestFocus();
					}
					else
					{
						mEtNumBack.requestFocus();
					}
				}
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) 
			{
			}

			public void afterTextChanged(Editable arg0) 
			{
			}
		});
		
		mIvSearchTime.setOnClickListener(this);
		mIvStartTime.setOnClickListener(this);
		mIvEndTime.setOnClickListener(this);
	    
	    // 查询
	    mBtnQuery.setOnTouchListener(this);
	    mBtnQuery.setOnClickListener(this);
	    
	    // 发票校准
	    mBtnInvoiceValidate.setOnClickListener(this);
	    /*发票校准界相关*/
	    // 设置
	    mBtnSet.setOnTouchListener(this);
	    mBtnSet.setOnClickListener(this);   
	    // 返回到发票打印界面
	    mBackToInvoicePrint.setOnClickListener(this);       
	}
	
	// 是否存在异常
	public boolean isExceptionExist()
	{
		InvoicePrintSharp invoicePrintSharp = new InvoicePrintSharp(this);
		String record = invoicePrintSharp.getRecordValue();
		
		if(!StringUtil.isEmptyOrNull(record))
		{
			return true;
		}
		return false;
	}
	
	// 清空异常记录
	public void clearExceptionData()
	{
		InvoicePrintSharp invoicePrintSharp = new InvoicePrintSharp(this);
		invoicePrintSharp.clearData();
	}
	
	private void initShow()
	{
	}
	
	// 初始化左边界面显示
	private void initLeftViewShow(int viewId)
	{
		switch (viewId)
		{
		case INVOICE_PRINT_STEPONE:
			mTvLeftInvoice.setText("发票打印");
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mIvLeftInvoice.setImageDrawable(getResources().getDrawable(R.drawable.icon_invoice_print_orange));
			}
			else
			{
				mIvLeftInvoice.setImageDrawable(getResources().getDrawable(R.drawable.icon_invoice_print));
			}
			break;

		case INIT_INVOICE:
		case INVOICE_VALIDATE:
			mTvLeftInvoice.setText("发票校准");
			if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mIvLeftInvoice.setImageDrawable(getResources().getDrawable(R.drawable.icon_invoice_validte_orange));
			}
			else
			{
				mIvLeftInvoice.setImageDrawable(getResources().getDrawable(R.drawable.icon_invoice_validte));
			}
			break;
		}
	}
	
	private void showView(int viewId, int printTypeLimit, int timeLimit)
	{
		switch(viewId)
		{
		case INVOICE_PRINT_STEPONE:
			// 初始化左边界面显示
			initLeftViewShow(INVOICE_PRINT_STEPONE);
			
			mLayInvoiceQuery.setVisibility(View.VISIBLE);
			mLinearInvoicePrint.setVisibility(View.GONE);
			mLayInvoiceValidate.setVisibility(View.GONE);
			// 打印类型
			if(printTypeLimit == 0)
			{
				mPrintTypeUtil.setSelected(0);
			}
			// 业务类型
			mEtBusiType.setText(mStrBusiArr[0]);
			mBusiness0F.mBusiType = mBusiTypes[0];
			// 交易号码
			mNonSjBusiNum.setVisibility(View.GONE);
			mSjBusiNum.setVisibility(View.VISIBLE);
			mSjNum.setText("");
			// 查询时间
			if(timeLimit == 0)
			{
				mLinearGaveSearchTime.setVisibility(View.VISIBLE);
				mLinearStatementSeartTime.setVisibility(View.GONE);
				mEtSearchTime.setText("");
			}
			else
			{
				mLinearGaveSearchTime.setVisibility(View.GONE);
				mLinearStatementSeartTime.setVisibility(View.VISIBLE);
				mEtStartTime.setText("");
				mEtEndTime.setText("");
			}
			// 服务密码
			mEtServerPwd.setText("");
			mEtServerPwd.requestFocus();
			break;
			
		case INVOICE_PRINT_STETTWO:
			mLayInvoiceQuery.setVisibility(View.GONE);
			mLinearInvoicePrint.setVisibility(View.VISIBLE);
			mLayInvoiceValidate.setVisibility(View.GONE);
			
			mTvBusiNum.setText(mBusiness0F.mQueryNum);
			
			if(mPrintType == 0x00)
			{
				mTvPrintNavigation.setText("现金发票打印");
				mLinearInvoiceGaveTitle.setVisibility(View.VISIBLE);
				mLinearInvoiceStatementTitle.setVisibility(View.GONE);
				showGaveInvoiceList();
			}
			else if(mPrintType == 0x01)
			{
				mTvPrintNavigation.setText("月结发票打印");
				mLinearInvoiceGaveTitle.setVisibility(View.GONE);
				mLinearInvoiceStatementTitle.setVisibility(View.VISIBLE);
				showStatementInvoiceList();
			}
			break;
		
		case INIT_INVOICE:
			// 初始化左边界面显示
			initLeftViewShow(INIT_INVOICE);
			
			mLayInvoiceQuery.setVisibility(View.GONE);
			mLinearInvoicePrint.setVisibility(View.GONE);
			mLayInvoiceValidate.setVisibility(View.VISIBLE);
			
			mInvoiceValidateUtil.setSelected(0);
			mInvoiceValidateType = 0x00;
			mTvStartInvoiceNum.setText("起始发票号:");
			mInputInvoiceNum.setText("");
			mTvInvoiceCode.setVisibility(View.VISIBLE);
			mInputInvoiceCode.setVisibility(View.VISIBLE);
			mInputInvoiceCode.setText("");
			initInvoiceQuery();
			break;
			
		case INVOICE_VALIDATE:
			// 初始化左边界面显示
			initLeftViewShow(INVOICE_VALIDATE);
			
			mLayInvoiceQuery.setVisibility(View.GONE);
			mLinearInvoicePrint.setVisibility(View.GONE);
			mLayInvoiceValidate.setVisibility(View.VISIBLE);
			
			mInvoiceValidateUtil.setSelected(1);
			mInvoiceValidateType = 0x01;
			mTvStartInvoiceNum.setText("调整发票号:");
			mInputInvoiceNum.setText("");
			mTvInvoiceCode.setVisibility(View.GONE);
			mInputInvoiceCode.setVisibility(View.GONE);
			
			mTvInvoiceValidateHint.setText("当前发票号码与系统当前发票号码不一致时,在此调整发票号码.");
			mTvCurrDeviceInvoiceNum.setVisibility(View.VISIBLE);
			mTvCurrDeviceInvoiceNum.setText(mBusiness0F.mInvoiceNum);
			mTvCurrDeviceInvoiceCode.setVisibility(View.GONE);
			break;
		}
	}
	
	// 显示日期对话框
    protected void showDateDialog(final EditText editText, final int dateType) 
    {
    	/*
    	if(ButtonUtil.isFastDoubleClick(editText.getId())) 
    	{
    		return;
    	}
    	*/
    	DateTimePickerDialog date = DateTimePickerDialog.getYearMonthDateDialog(this);
		if(dateType == 1)
		{
			date.setDateVisible(false);
		}
    	
		date.setOnDateTimeSelectListener(new DateTimePickerDialog.OnDateTimeSelectListener()
		{	
			@Override
			public void onDateTimeSelected(Calendar c)
			{
    			if(dateType == 1)
    			{
    				editText.setText(new StringBuilder()
    					.append(c.get(Calendar.YEAR))
    					.append("-")
    					.append((c.get(Calendar.MONTH)+1)<10? "0"+(c.get(Calendar.MONTH)+1):(c.get(Calendar.MONTH)+1)));
    			}
    			else
    			{
    				editText.setText(new StringBuilder()
    					.append(c.get(Calendar.YEAR))
    					.append("-")
    					.append((c.get(Calendar.MONTH)+1)<10? "0"+(c.get(Calendar.MONTH)+1):(c.get(Calendar.MONTH)+1))
    					.append("-")
    					.append((c.get(Calendar.DATE)<10)? "0"+c.get(Calendar.DATE):c.get(Calendar.DATE)));
    			}
			}
		});
    	date.show();	
    }
    
    // 现金发票列表适配器
 	private class GaveInvoiceAdapter extends BaseAdapter 
 	{
 		private List<InvoiceData> dataList;
 		private int resourceId;
 		private LayoutInflater inflator;
 		
 		public GaveInvoiceAdapter(Context context, List<InvoiceData> dataList, int resourceId) 
 		{
 			this.dataList = dataList;
 			this.resourceId = resourceId;
 			inflator = LayoutInflater.from(context);
 		}
 		
 		@Override
 		public View getView(final int position, View convertView, ViewGroup parent) 
 		{
 			ViewHolder viewHolder = null;
 			
 			if(convertView == null)
 			{
 				viewHolder = new ViewHolder();
 				convertView = inflator.inflate(resourceId, null);
 				TextView tvDate = (TextView) convertView.findViewById(R.id.tv_date);
 				TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
 				TextView tvMoney = (TextView) convertView.findViewById(R.id.tv_money);
 				TextView tvOper = (TextView) convertView.findViewById(R.id.tv_oper);
 				LinearLayout layInvoiceOper = (LinearLayout) convertView.findViewById(R.id.lay_invoice_oper);
 				
 				viewHolder.setTvDate(tvDate);
 				viewHolder.setTvTitle(tvTitle);
 				viewHolder.setTvMoney(tvMoney);
 				viewHolder.setTvOper(tvOper);
 				viewHolder.setLayInvoiceOper(layInvoiceOper);
 				convertView.setTag(viewHolder);
 			}
 			else
 			{
 				viewHolder = (ViewHolder)convertView.getTag();
 			}
 			
 			InvoiceData invoiceDate = (InvoiceData) dataList.get(position);
			String date = invoiceDate.getBusiTime();
			String title = invoiceDate.getBusiType();
			String money = invoiceDate.getCMoney();
 			
 			viewHolder.getTvDate().setText(date);
 			viewHolder.getTvTitle().setText(title);
 			viewHolder.getTvMoney().setText(money);
 			
 			viewHolder.getTvOper().setText(Html.fromHtml("<u>打印</u>"));
			viewHolder.getTvOper().setTextColor(Color.argb(0xff, 0xfe, 0x99, 0x84));
			viewHolder.getLayInvoiceOper().setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{
					// 防止按钮多次触发
					if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
					{
						return;
					}
					
					mGaveInvoicePos = position;
					initInvoiceQuery(position);
				}
			});
 			return convertView;
 		}
		@Override
		public int getCount() 
		{
			return dataList.size();
		}

		@Override
		public Object getItem(int position) 
		{
			return dataList.get(position);
		}

		@Override
		public long getItemId(int position) 
		{
			return position;
		}
	}
	
    // 月结发票列表适配器
  	private class StatementInvoiceAdapter extends BaseAdapter 
  	{
  		private List<InvoiceDataMonth> dataList;
  		private int resourceId;
  		private LayoutInflater inflator;
  		
  		public StatementInvoiceAdapter(Context context, List<InvoiceDataMonth> dataList, int resourceId) 
  		{
  			this.dataList = dataList;
  			this.resourceId = resourceId;
  			inflator = LayoutInflater.from(context);
  		}
  		
  		@Override
  		public View getView(final int position, View convertView, ViewGroup parent) 
  		{
  			ViewHolder viewHolder = null;
  			
  			if(convertView == null)
  			{
  				viewHolder = new ViewHolder();
  				convertView = inflator.inflate(resourceId, null);
  				TextView tvDate = (TextView) convertView.findViewById(R.id.tv_date);
  				TextView tvMoney = (TextView) convertView.findViewById(R.id.tv_money);
  				TextView tvOper = (TextView) convertView.findViewById(R.id.tv_oper);
  				LinearLayout layInvoiceOper = (LinearLayout) convertView.findViewById(R.id.lay_invoice_oper);
  				
  				viewHolder.setTvDate(tvDate);
  				viewHolder.setTvMoney(tvMoney);
  				viewHolder.setTvOper(tvOper);
  				viewHolder.setLayInvoiceOper(layInvoiceOper);
  				convertView.setTag(viewHolder);
  			}
  			else
  			{
  				viewHolder = (ViewHolder)convertView.getTag();
  			}
  			
  			InvoiceDataMonth invoiceDataMonth = (InvoiceDataMonth) dataList.get(position);
		    String date = invoiceDataMonth.getBillDate();
		    String money = invoiceDataMonth.getCPrintMoney();
  			
  			viewHolder.getTvDate().setText(date);
  			viewHolder.getTvMoney().setText(money);
  			
  			viewHolder.getTvOper().setText(Html.fromHtml("<u>打印</u>"));
 			viewHolder.getTvOper().setTextColor(Color.argb(0xff, 0xfe, 0x99, 0x84));
 			viewHolder.getLayInvoiceOper().setOnClickListener(new View.OnClickListener() 
 			{
 				@Override
 				public void onClick(View v) 
 				{
 				    // 防止按钮多次触发
 					if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
 					{
 						return;
 					}
 					
 					mStatementInvoicePos = position;
					initInvoiceQuery(position);
 				}
 			});
  			return convertView;
  		}
 		@Override
 		public int getCount() 
 		{
 			return dataList.size();
 		}

 		@Override
 		public Object getItem(int position) 
 		{
 			return dataList.get(position);
 		}

 		@Override
 		public long getItemId(int position) 
 		{
 			return position;
 		}
 	}
  	
  	// ListView复用机制中最核心部分
	class ViewHolder
	{
		private TextView tvDate;
		private TextView tvTitle;
		private TextView tvMoney;
		private TextView tvOper;
		private LinearLayout layInvoiceOper;
		
		public TextView getTvDate()
		{
			return tvDate;
		}
		
		public void setTvDate(TextView tvDate) 
		{
			this.tvDate = tvDate;
		}
		
		public TextView getTvTitle() 
		{
			return tvTitle;
		}
		
		public void setTvTitle(TextView tvTitle) 
		{
			this.tvTitle = tvTitle;
		}
		
		public TextView getTvMoney() 
		{
			return tvMoney;
		}
		
		public void setTvMoney(TextView tvMoney) 
		{
			this.tvMoney = tvMoney;
		}
		
		public LinearLayout getLayInvoiceOper() 
		{
			return layInvoiceOper;
		}
		
		public void setLayInvoiceOper(LinearLayout layInvoiceOper) 
		{
			this.layInvoiceOper = layInvoiceOper;
		}
		
		public TextView getTvOper() 
		{
			return tvOper;
		}
		
		public void setTvOper(TextView tvOper) 
		{
			this.tvOper = tvOper;
		}
	}
	
	// 初始化发票查询(这里要变动的有发票类型)
	public void initInvoiceQuery()
	{
		// 发票类型
		mBusiness0F.mInvoiceType = 0x05;
		mIsInitInvoiceQuery = true;
		
		new Thread(new Runnable() 
		{
			@Override
			public void run() 
			{
				// 检测蓝牙设备
				if(equipmentService.handshake() == false) 
				{
					DialogUtil.MsgBox("请连接蓝牙设备", 
							"检测到当前未连接蓝牙打印设备，是否立即搜索并连接蓝牙打印设备？", 
							"立即连接", 
							Statics.MSG_CONNECT_BLUETOOTH, 
							"取消", 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							mHandler);
					return;
				}
				
				if(mTempBlueToothMac == null)
				{
					mIsHasInitInvoiceQuery = false;
				}
				else if(mTempBlueToothMac.equals(Statics.mCurMap.get(Statics.BLUETOOTH_MAC)))
				{
					mIsHasInitInvoiceQuery = true;
				}
				else
				{
					mIsHasInitInvoiceQuery = false;
				}
					
				if(mIsHasInitInvoiceQuery)
				{
					if(mInvoiceValidateType == mInvoiceValidateUtilIds[0])
					{
						if(mBusiness0F.mInvoiceNum != null)
						{
							mTvCurrDeviceInvoiceNum.setVisibility(View.VISIBLE);
							mTvCurrDeviceInvoiceNum.setText("当前设备发票号:"+mBusiness0F.mInvoiceNum);
						}
						else
						{
							mTvCurrDeviceInvoiceNum.setVisibility(View.GONE);
						}
						
						if(mBusiness0F.mBatchNum != null)
						{
							mTvCurrDeviceInvoiceCode.setVisibility(View.VISIBLE);
							mTvCurrDeviceInvoiceCode.setText("当前设备发票代码:"+mBusiness0F.mBatchNum);
						}
						else
						{
							mTvCurrDeviceInvoiceCode.setVisibility(View.GONE);
						}
					}
					else if(mInvoiceValidateType == mInvoiceValidateUtilIds[1])
					{
						if(mBusiness0F.mInvoiceNum != null)
						{
							mTvCurrDeviceInvoiceNum.setVisibility(View.VISIBLE);
							mTvCurrDeviceInvoiceNum.setText("当前设备发票号:"+mBusiness0F.mInvoiceNum);
						}
						else
						{
							mTvCurrDeviceInvoiceNum.setVisibility(View.GONE);
						}
						mTvCurrDeviceInvoiceCode.setVisibility(View.GONE);
					}
					return;
				}
				
				DialogUtil.showProgress("正在初始化发票查询,请稍候");
				int ret = mBusiness0F.bll0F51();
				DialogUtil.closeProgress();
				
				if(ret == ReturnEnum.SUCCESS)
				{
					mTempBlueToothMac = Statics.mCurMap.get(Statics.BLUETOOTH_MAC);
					mIsHasInitInvoiceQuery = true;
					
					if(mInvoiceValidateType == mInvoiceValidateUtilIds[0])
					{
						if(mBusiness0F.mInvoiceNum != null)
						{
							mTvCurrDeviceInvoiceNum.setText("当前设备发票号:"+mBusiness0F.mInvoiceNum);
						}
						else
						{
							mTvCurrDeviceInvoiceNum.setVisibility(View.GONE);
						}
						
						if(mBusiness0F.mBatchNum != null)
						{
							mTvCurrDeviceInvoiceCode.setText("当前设备发票代码:"+mBusiness0F.mBatchNum);
						}
						else
						{
							mTvCurrDeviceInvoiceCode.setVisibility(View.GONE);
						}
					}
					else if(mInvoiceValidateType == mInvoiceValidateUtilIds[1])
					{
						if(mBusiness0F.mInvoiceNum != null)
						{
							mTvCurrDeviceInvoiceNum.setText("当前设备发票号:"+mBusiness0F.mInvoiceNum);
						}
						else
						{
							mTvCurrDeviceInvoiceNum.setVisibility(View.GONE);
						}
					}
				}
				else
				{
					if(mInvoiceValidateType == mInvoiceValidateUtilIds[0])
					{
						mTvCurrDeviceInvoiceNum.setVisibility(View.GONE);
						mTvCurrDeviceInvoiceCode.setVisibility(View.GONE);
					}
					else if(mInvoiceValidateType == mInvoiceValidateUtilIds[1])
					{
						mTvCurrDeviceInvoiceNum.setVisibility(View.GONE);
					}
					
					DialogUtil.MsgBox(TITLE_STR, "初始发票查询失败!\n"+mBusiness0F.getLastknownError());
				}
			}
		});
	}
		
	// 初始化发票查询(这里要变动的有发票类型、流水号)其中流水号待定
	public void initInvoiceQuery(final int position)
	{
		// 发票类型
		mBusiness0F.mInvoiceType = 0x05;
		mIsInitInvoiceQueryWithParam = true;
		
		if(Statics.IS_DEBUG)
		{
			InvoicePrintDialog dialog = new InvoicePrintDialog(BusiInvoicePrint.this , mBusiness0F, mHandler, position);
			dialog.show();
		}
		
		new Thread(new Runnable() 
		{
			@Override
			public void run() 
			{
				// 检测蓝牙设备
				if(equipmentService.handshake() == false) 
				{
					DialogUtil.MsgBox("请连接蓝牙设备", 
							"检测到当前未连接蓝牙打印设备，是否立即搜索并连接蓝牙打印设备？", 
							"立即连接", 
							Statics.MSG_CONNECT_BLUETOOTH, 
							"取消", 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							mHandler);
					return;
				}
				
				DialogUtil.showProgress("正在初始化发票查询,请稍候");
				int ret = mBusiness0F.bll0F51();
				DialogUtil.closeProgress();
				if(ret == ReturnEnum.SUCCESS)
				{
					mIsInitInvoiceQueryWithParam = false;
					// 临时保存蓝牙Mac地址
					mTempBlueToothMac = Statics.mCurMap.get(Statics.BLUETOOTH_MAC);
					// 将mIsHasInitInvoiceQuery属性设置为true,表示已经初始化发票查询
					mIsHasInitInvoiceQuery = true;
					InvoicePrintDialog dialog = new InvoicePrintDialog(BusiInvoicePrint.this , mBusiness0F, mHandler, position);
					dialog.show();
				}
				else
				{
					mIsInitInvoiceQueryWithParam = false;
					DialogUtil.MsgBox(TITLE_STR, "初始发票查询失败!\n"+mBusiness0F.getLastknownError());
				}
			}
		});
	}
	
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) 
	{
		// 减1是因为上面加了个addFooterView
		mLastItem = firstVisibleItem + visibleItemCount - 1; 
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) 
	{
		if(Statics.IS_DEBUG)
		{
			return;
		}
		
		if(mIsScrollOver)
		{	
			if(mBusiness0F.mInvoiceDatas != null)
			{
				if (scrollState == SCROLL_STATE_IDLE && mLastItem == mBusiness0F.mInvoiceDatas.size())
				{
					// 滚动翻页查询进行中
					mIsScrollOver = false;
					Log.i("TAG", "拉到最底部");
					mMoreView.setVisibility(View.VISIBLE);

					new Thread(new Runnable()
					{
						@Override
						public void run()
						{
							if (mBusiness0F.mInvoiceDatas.size() < mBusiness0F.mQueryTotal)
							{
								mBusiness0F.mQueryStartIdx = mBusiness0F.mInvoiceDatas.size() + 1;
								int ret = mBusiness0F.bll0F52(false);
								if (ret != ReturnEnum.SUCCESS)
								{
									runOnUiThread(new Runnable()
									{
										public void run()
										{
											back(new ICallBack() 
											{
												@Override
												public void back() 
												{
													mIsScrollOver = true;
												}
												
												@Override
												public void cancel() 
												{
												}

												@Override
												public void dismiss() 
												{
												}
											}, 
											null,
											mBusiness0F.getLastknownError(), 
											"确定", null, true, false, false);
											mMoreView.setVisibility(View.GONE);
										}
									});
								}
								else
								{
									// mHandler.sendEmptyMessage(0);
								}
							}
							else
							{
								runOnUiThread(new Runnable()
								{
									public void run()
									{
										back(new ICallBack() 
										{
											@Override
											public void back() 
											{
												mIsScrollOver = true;
											}
											
											@Override
											public void cancel() 
											{
											}
											
											@Override
											public void dismiss() 
											{
												mIsScrollOver = true;
											}
										}, null, "没有更多的现金发票。", "确定", null, true, false, true);
									    mMoreView.setVisibility(View.GONE);
									}
								});
							}
						}
					}).start();
				}
			}
		}
	}
	
	// 打印
	private void print() 
	{
		if(Statics.IS_DEBUG)
		{
			back(new ICallBack() 
			{
				@Override
				public void back()
				{
					showView(INVOICE_VALIDATE, -1, -1);
				}
				
				@Override
				public void cancel() 
				{
					showView(INVOICE_PRINT_STEPONE, -1, -1);
				}
				
				@Override
				public void dismiss() 
				{
					showView(INVOICE_PRINT_STEPONE, -1, -1);
				}
			}, TITLE_STR, 
			"发票打印成功!系统发票号码更新失败，请手动调整发票号",
			"调整发票号",
			"取消", true, true, true);
			return;
		}
		
		new Thread() 
		{
			public void run() 
			{
				// 检测蓝牙设备
				if(equipmentService.handshake() == false) 
				{
					DialogUtil.MsgBox("请连接蓝牙设备", 
							"检测到当前未连接蓝牙打印设备，是否立即搜索并连接蓝牙打印设备？", 
							"立即连接", 
							Statics.MSG_CONNECT_BLUETOOTH, 
							"取消", 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							mHandler);
					return;
				}
				
				if(!equipmentService.isBlackLabelExist())
				{
					back(new ICallBack() 
					{
						@Override
						public void back() 
						{
							showRePrintDialog();
						}
						
						@Override
						public void cancel() 
						{
						}
						
						@Override
						public void dismiss() 
						{
						}
					}, TITLE_STR, "黑标检测失败,请确认打印纸是否放反!", "确定", null, true, false, false);
					return;
				}
				
				int ret = ReturnEnum.FAIL;
				// 现金发票(上传需要变更的数据有发票类型、发票号码、流水号) 
				// 月结发票(上传需要变更的数据有发票类型、发票号码、账期、金额、查询流水)
				// 其中发票号码在发票初始化查询中获取
				// 发票类型
				mBusiness0F.mInvoiceType = 0x00;
				
				if(mPrintType == 0x00)
				{
					InvoiceData invoiceData = mBusiness0F.mInvoiceDatas.get(mGaveInvoicePos);
					// 流水号
					mBusiness0F.mSerialNum = invoiceData.getSerialNum();
				}
				else if(mPrintType == 0x01)
				{
					InvoiceDataMonth invoiceDataMonth = mBusiness0F.mInvoiceDataMonths.get(mStatementInvoicePos);
					// 账期
					mBusiness0F.mBilldate = invoiceDataMonth.getBillDate();
					// 金额
					mBusiness0F.mPrintMoney = invoiceDataMonth.getPrintMoney();
					// 查询流水
					mBusiness0F.mSerialNum = invoiceDataMonth.getSerialNum();
				}
				
				DialogUtil.showProgress("正在获取打印信息,请稍候");
				if(mPrintType == 0x00)
				{
					ret = mBusiness0F.bll0F51();
				}
				else if(mPrintType == 0x01)
				{
					ret = mBusiness0F.bll0F53();
				}
				
				DialogUtil.closeProgress();
				if(ret == ReturnEnum.SUCCESS)
				{
					DialogUtil.showProgress("正在打印,请稍候");
					
					StringBuffer tempBuff = new StringBuffer();
					
					tempBuff.append(mBusiness0F.mInvoiceContent);
					// 开始打印
					mBusiness0F.print(tempBuff, mBusiness0F.mSerialNum);
					DialogUtil.closeProgress();
				}
				else
				{
					DialogUtil.MsgBox(TITLE_STR, "打印信息获取失败!\n"+mBusiness0F.getLastknownError());
					return;
				}
				
				if(equipmentService.isPrintSuccess())
				{
					mBusiness0F.mQueryTotal -= 1;
					if(mPrintType == 0x00)
					{
						mBusiness0F.mInvoiceDatas.remove(mGaveInvoicePos);
						// 刷新现金发票列表
						showGaveInvoiceList();
					}
					else if(mPrintType == 0x01)
					{
						mBusiness0F.mInvoiceDataMonths.remove(mStatementInvoicePos);
						// 刷新月结发票列表
						showStatementInvoiceList();
					}
					
					// 发票递增(上传需要变更的数据有发票类型)
					mBusiness0F.mInvoiceType = 0x08;
					DialogUtil.showProgress("正在发送发票递增请求");
					ret = mBusiness0F.bll0F51();
					if(ret == ReturnEnum.FAIL)
					{
						// 异常记录
						new InvoicePrintSharp(BusiInvoicePrint.this).putRecord("error");
						back(new ICallBack() 
						{
							@Override
							public void back()
							{
								showView(INVOICE_VALIDATE, -1, -1);
							}
							
							@Override
							public void cancel() 
							{
								showView(INVOICE_PRINT_STEPONE, -1, -1);
							}
							
							@Override
							public void dismiss() 
							{
								showView(INVOICE_PRINT_STEPONE, -1, -1);
							}
						}, TITLE_STR, 
						"发票打印成功!系统发票号码更新失败，请手动调整发票号",
						"调整发票号",
						"取消", true, true, true);
					}
				}
				else
				{
					/*
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String saveDate = sdf.format(new Date());
					
					if(mPrintType == 0x00)
					{
						InvoiceData invoiceData = mBusiness0F.mInvoiceDatas.get(mGaveInvoicePos);
						InvoicePrintDao invoicePrintDao = new InvoicePrintDao(BusiInvoicePrint.this);
						invoicePrintDao.setPrintInfo(mBusiness0F.mQueryNum, 
													 invoiceData.getCMoney(), 
													 mBusiness0F.mInvoiceNum, 
													 saveDate,                    // 打印时间
													 invoiceData.getDeviceType(), // 发票类型
													 invoiceData.getBusiTime(),   // 发票日期
													 invoiceData.getSerialNum(),  // 流水号
													 "",                          // 账期
													 "现金发票",                     // 对账类型
													 "-1",                        // 打印类型
													 mBusiness0F.mInvoiceContent, // 发票内容
													 "1");                        // 打印方式
					}
					else if(mPrintType == 0x01)
					{
						InvoiceDataMonth invoiceDataMonth = mBusiness0F.mInvoiceDataMonths.get(mStatementInvoicePos);
						InvoicePrintDao invoicePrintDao = new InvoicePrintDao(BusiInvoicePrint.this);
						invoicePrintDao.setPrintInfo(mBusiness0F.mQueryNum, 
								                     invoiceDataMonth.getCPrintMoney(), 
													 mBusiness0F.mInvoiceNum, 
													 saveDate,                         // 打印时间
													 "0",                              // 发票类型
													 "",                               // 发票日期
													 invoiceDataMonth.getSerialNum(),  // 流水号
													 invoiceDataMonth.getBillDate(),   // 账期
													 "月结发票",                          // 对账类型
													 "0",                              // 打印类型
													 mBusiness0F.mInvoiceContent,      // 发票内容
													 "1");                            // 打印方式
					}
					*/
					showRePrintDialog();
				}
			};
		}.start();
	}
	
	// 显示是否重新打印选择框
	private void showRePrintDialog()
	{
		BusiInvoicePrint.this.runOnUiThread(new Runnable() 
		{
			@Override
			public void run() 
			{
				DialogUtil.closeProgress();
				
				back(new ICallBack() 
				{
					@Override
					public void back() 
					{
						print();
					}
					
					@Override
					public void cancel() 
					{
					}

					@Override
					public void dismiss() 
					{
					}
				}, null, "是否重新打印发票?", "确定", "取消", true, false, false);
			}
		});
	}
		
	// 确定时的动作处理
	protected Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch (message.what) 
			{
			// 连接蓝牙设备
			case Statics.MSG_CONNECT_BLUETOOTH:
				SearchBluetoothDialog dialog = new SearchBluetoothDialog(BusiInvoicePrint.this, mBluetoothClient, equipmentService, 
						mHandler, Statics.MSG_CONNECT_DONE);
				dialog.show();
				break;
				
			// 连接蓝牙设备完成
			case Statics.MSG_CONNECT_DONE:
				new Thread() 
				{
					public void run() 
					{
						if(mIsInitInvoiceQuery)
						{
							// 不带参数的初始化查询
							initInvoiceQuery();
						}
						else if(mIsInitInvoice)
						{
							// 初始化发票
							initInvoice();
						}
						else if(mIsValidateInvoice)
						{
							// 校准发票
							validateInvoice();
						}
						else if(mIsInitInvoiceQueryWithParam)
						{
							if(mPrintType == 0x00)
							{
								// 带参数的初始化发票查询
								initInvoiceQuery(mGaveInvoicePos);
							}
							else if(mPrintType == 0x01)
							{
								// 不带参数的初始化发票查询
								initInvoiceQuery(mStatementInvoicePos);
							}
						}
						else
						{
							// 打印
							print();
						}
					}
				}.start();
				break;
				
			// 取消蓝牙或蓝牙连接失败
			case Statics.MSG_CONNECT_BLUETOOTH_CANCEL:
				mIsInitInvoice = false;
				mIsValidateInvoice = false;
				mIsInitInvoiceQueryWithParam = false;
				mIsInitInvoiceQuery = false;
				break;
				
				// 初始化交易密码
			case Statics.MSG_MODIFY_PSWD:
				ModifyPswdDialog dialog1 = new ModifyPswdDialog(BusiInvoicePrint.this, 
				new Business_00(BusiInvoicePrint.this, equipmentService, socketService, dataBaseService), mHandler);
				dialog1.show();
				break;
				
			// 重置交易密码
			case Statics.MSG_RESET_PSWD:
				ResetSrvDialog dialog2 = new ResetSrvDialog(BusiInvoicePrint.this, 
				new Business_00(BusiInvoicePrint.this, equipmentService, socketService, dataBaseService), mHandler);
				dialog2.show();
				break;
				
			// 交易密码修改成功
			case Statics.MSG_MODIFY_PSWD_SUCCESS:
				break;
			
			// 开始打印发票
			case Statics.MSG_INVOICE_PRINT:
				print();
				break;
			
			case Statics.MSG_GOTO_INVOICE_VALIDATE:
				showView(INVOICE_VALIDATE, -1, -1);
				break;
				
			default:
				break;
			}
		}
	};
		
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	@Override
	public void onClick(View v) 
	{
		if(v.getId() != R.id.iv_busitype)
		{
			// 防止按钮多次触发
			if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
			{
				return;
			}
		}
					
		switch(v.getId()) 
		{
		// 返回到自服务
		case R.id.linear_backto_selfservice:
			finish();
			break;
			
		case R.id.iv_busitype:
			if(mIsLvBusiHide)
			{
				mLvBusi.setVisibility(View.VISIBLE);
				initLvBusi();
				mIsLvBusiHide = false;
			}
			else
			{
				mLvBusi.setVisibility(View.GONE);
				mIsLvBusiHide = true;
			}
			break;
		
		case R.id.iv_searchtime:
			showDateDialog(mEtSearchTime, 1);
			break;
			
		case R.id.iv_begintime:
			showDateDialog(mEtStartTime, 1);
			break;
		
		case R.id.iv_endtime:
			showDateDialog(mEtEndTime, 1);
			break;
		
		case R.id.btn_query:
			if(isExceptionExist())
			{
				DialogUtil.MsgBox(TITLE_STR, "上一笔发票打印异常,校准发票后,才能继续查询!");
				return;
			}
			
			if(mBusiness0F.mBusiType == mBusiTypes[0])
			{
				String sjNum = mSjNum.getText().toString().trim();
				if(StringUtil.isEmptyOrNull(sjNum) || sjNum.length() != 11)
				{
					DialogUtil.MsgBox(TITLE_STR, "输入交易号码有误,请重新输入!");
					return;
				}
				else
				{
					mBusiness0F.mQueryNum = sjNum;
				}
			}
			else
			{
				String num = "";
				String num_pre = "";
				String num_back = "";
				
				num_pre = mEtNumPre.getText().toString().trim();
				
				if(!mIsBroadbandQuery)
				{
					num_back = mEtNumBack.getText().toString().trim();
				}
				else
				{
					num_back = mEtNumBack0.getText().toString().trim();
				}
				
				num = num_pre + "-" + num_back;
				mBusiness0F.mQueryNum = num;

				if ((3 != num_pre.length()) && (4 != num_pre.length()))
				{
					DialogUtil.MsgBox(TITLE_STR, "请输入正确区号!");
					return;
				}
				
				if (!mIsBroadbandQuery)
				{
					if ((7 != num_back.length()) && (8 != num_back.length())) 
					{
						DialogUtil.MsgBox(TITLE_STR, "请输入正确号码!");
						return;
					}
				}
				else 
				{
					if (StringUtil.isEmptyOrNull(num_back)) 
					{
						DialogUtil.MsgBox(TITLE_STR, "请输入正确号码!");
						return;
					}
				}
			}
			
			if(mPrintType == 0)
			{
				String queryTime = mEtSearchTime.getText().toString().trim();
				if(StringUtil.isEmptyOrNull(queryTime))
				{
					DialogUtil.MsgBox(TITLE_STR, "请输入正确查询时间!");
					return;
				}
				else
				{
					String date = queryTime.replaceAll("-", "");
					mBusiness0F.mQueryDate = date + date;
				}
			}
			else if(mPrintType == 1)
			{
				String startTime = mEtStartTime.getText().toString().trim();
				String endTime = mEtEndTime.getText().toString().trim();
				if(StringUtil.isEmptyOrNull(startTime))
				{
					DialogUtil.MsgBox(TITLE_STR, "请输入正确开始时间!");
					return;
				}
				else if(StringUtil.isEmptyOrNull(endTime))
				{
					DialogUtil.MsgBox(TITLE_STR, "请输入正确结束时间!");
					return;
				}
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
				try 
				{
					if(sdf.parse(startTime).getTime() > sdf.parse(endTime).getTime())
					{
						DialogUtil.MsgBox(TITLE_STR, "起始月份不能大于结束月份!");
						return;
					}
				} 
				catch (ParseException e) 
				{
					e.printStackTrace();
				}
				mBusiness0F.mQueryDate = startTime.replaceAll("-", "") + endTime.replaceAll("-", "");
			}
			
			String serverPwd = mEtServerPwd.getText().toString().trim();
			if(StringUtil.isEmptyOrNull(serverPwd))
			{
				DialogUtil.MsgBox(TITLE_STR, "请输入正确服务密码!");
				return;
			}
			else
			{
				mBusiness0F.mServicePwd = serverPwd;
			}
			
			if(mPrintType == 0)
			{
				gaveInvoiceQuery(true);
			}
			else if(mPrintType == 1)
			{
				statementInvoiceQuery();
			}
			break;
		    
		case R.id.btn_invoice_validate:
			showView(INIT_INVOICE, -1, -1);
			break;
		
		case R.id.btn_set:
			// 发票号码
			String invoiceNum = mInputInvoiceNum.getText().toString().trim();
			// 发票代码
			String invoiceCode = mInputInvoiceCode.getText().toString().trim();
			switch(mInvoiceValidateType)
			{
				case 0x04:
					if(StringUtil.isEmptyOrNull(invoiceNum))
					{
						DialogUtil.MsgBox(TITLE_STR, "请输入发票号码!");
						return;
					}
					else
					{
						mBusiness0F.mInvoiceNum = invoiceNum;
					}
					
					if(StringUtil.isEmptyOrNull(invoiceCode))
					{
						DialogUtil.MsgBox(TITLE_STR, "请输入发票代码!");
						return;
					}
					else
					{
						mBusiness0F.mBatchNum = invoiceCode;
					}
					
					// 初始化发票
					initInvoice();
					break;
				
				case 0x07:
					if(StringUtil.isEmptyOrNull(invoiceNum))
					{
						DialogUtil.MsgBox(TITLE_STR, "请输入发票号码!");
						return;
					}
					else
					{
						mBusiness0F.mInvoiceNum = invoiceNum;
					}
					
					// 校准发票
					validateInvoice();
					break;
			}
			
			break;
			
		case R.id.btn_backto_invoiceprint:
			showView(INVOICE_PRINT_STEPONE, 0, 0);
			break;
			
		default:
			break;
		}
	}
	
	// 初始化发票
	public void initInvoice()
	{
		mIsInitInvoice = true;
		mIsValidateInvoice = false;
		new Thread() 
		{
			public void run() 
			{
				// 检测蓝牙设备
				if(equipmentService.handshake() == false) 
				{
					DialogUtil.MsgBox("请连接蓝牙设备", 
							"检测到当前未连接蓝牙打印设备，是否立即搜索并连接蓝牙打印设备？", 
							"立即连接", 
							Statics.MSG_CONNECT_BLUETOOTH, 
							"取消", 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							mHandler);
					return;
				}
				
				DialogUtil.showProgress("正在初始化发票, 请稍候");
				
				int ret = mBusiness0F.bll0F51();
				if(ret == ReturnEnum.SUCCESS)
				{
					mIsInitInvoice = false;
					mTvCurrDeviceInvoiceNum.setText("当前设备发票号:"+mBusiness0F.mInvoiceNum);
					mTvCurrDeviceInvoiceCode.setText("当前设备发票代码:"+mBusiness0F.mBatchNum);
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(TITLE_STR, "初始化发票成功!");
				}
				else
				{
					mIsInitInvoice = false;
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(TITLE_STR, "初始化发票失败!\n"+mBusiness0F.getLastknownError());
				}
			};
		}.start();
	}
	
	// 校准发票
	public void validateInvoice()
	{
		mIsInitInvoice = false;
		mIsValidateInvoice = true;
		new Thread() 
		{
			public void run() 
			{
				// 检测蓝牙设备
				if(equipmentService.handshake() == false) 
				{
					DialogUtil.MsgBox("请连接蓝牙设备", 
							"检测到当前未连接蓝牙打印设备，是否立即搜索并连接蓝牙打印设备？", 
							"立即连接", 
							Statics.MSG_CONNECT_BLUETOOTH, 
							"取消", 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							mHandler);
					return;
				}
				
				DialogUtil.showProgress("正在校准发票, 请稍候");
				
				int ret = mBusiness0F.bll0F51();
				if(ret == ReturnEnum.SUCCESS)
				{
					mIsValidateInvoice = false;
					mTvCurrDeviceInvoiceNum.setText("当前设备发票号:"+mBusiness0F.mInvoiceNum);
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(TITLE_STR, "校准发票成功!");
					
					if(isExceptionExist())
					{
						clearExceptionData();
					}
				}
				else
				{
					mIsValidateInvoice = false;
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(TITLE_STR, "校准发票失败!\n"+mBusiness0F.getLastknownError());
				}
			};
		}.start();
	}
	
	// 现金发票查询
	public void gaveInvoiceQuery(final boolean isFirst)
	{
		if(Statics.IS_DEBUG)
		{
			showView(INVOICE_PRINT_STETTWO, -1, -1);
			return;
		}
		
		mBusiness0F.mInvoiceDatas = new ArrayList<InvoiceData>();
		mBusiness0F.mQueryStartIdx = 0x01;
		
		new Thread(new Runnable() 
		{
			@Override
			public void run()
			{
				DialogUtil.showProgress("正在查询现金发票, 请稍候");
				int ret = mBusiness0F.bll0F52(isFirst);
				if(ret == ReturnEnum.SUCCESS)
				{
					DialogUtil.closeProgress();
					showView(INVOICE_PRINT_STETTWO, -1, -1);
				}
				else
				{
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(TITLE_STR, mBusiness0F.getLastknownError());
					return;
				}
			}
		}).start();
	}
	
	// 月结发票查询
	public void statementInvoiceQuery()
	{
		if(Statics.IS_DEBUG)
		{
			showView(INVOICE_PRINT_STETTWO, -1, -1);
			return;
		}
		
		new Thread(new Runnable() 
		{
			@Override
			public void run() 
			{
				DialogUtil.showProgress("正在查询月结发票, 请稍候");
				int ret = mBusiness0F.bll0F54();
				if(ret == ReturnEnum.SUCCESS)
				{
					DialogUtil.closeProgress();
					showView(INVOICE_PRINT_STETTWO, -1, -1);
				}
				else
				{
					DialogUtil.closeProgress();
					DialogUtil.MsgBox(TITLE_STR, mBusiness0F.getLastknownError());
				}
			}
		}).start();
	}
	
	// 显示现金发票列表
	public void showGaveInvoiceList()
	{
		BaseAdapter adapter;
		if(Statics.IS_DEBUG)
		{
			List<InvoiceData> invoiceDatas = new ArrayList<InvoiceData>();
			for(int i=0;i < 10;i++)
			{
				InvoiceData invoiceData = new InvoiceData("", "手机交费", 100, "10.0", "2013-10-09", "", 200);
				invoiceDatas.add(invoiceData);
			}
			adapter = new GaveInvoiceAdapter(this, invoiceDatas, R.layout.lay_selfservice_invoicegave_listitem);
		}
		else
		{
			if(mBusiness0F.mInvoiceDatas == null)
			{
				mBusiness0F.mInvoiceDatas = new ArrayList<InvoiceData>();
			}
			adapter = new GaveInvoiceAdapter(this,
					                  mBusiness0F.mInvoiceDatas, 
					                  R.layout.lay_selfservice_invoicegave_listitem);
		}
		mInvoiceGaveList.setAdapter(adapter);
	}
	
	// 显示月结发票列表
	public void showStatementInvoiceList()
	{
		BaseAdapter adapter;
		if(Statics.IS_DEBUG)
		{
			List<InvoiceDataMonth> invoiceDataMonths = new ArrayList<InvoiceDataMonth>();
			for(int i=0; i<10; i++)
			{
				InvoiceDataMonth invoiceDataMonth = new InvoiceDataMonth("", "", "2013-10-09", 100, "100.0元");
				invoiceDataMonths.add(invoiceDataMonth);
			}
			adapter =  new StatementInvoiceAdapter(this,
					invoiceDataMonths, 
	                R.layout.lay_selfservice_invoicestatement_listitem);
		}
		else
		{
			if(mBusiness0F.mInvoiceDataMonths == null)
			{
				mBusiness0F.mInvoiceDataMonths = new ArrayList<InvoiceDataMonth>();
			}
			adapter =  new StatementInvoiceAdapter(this,
	                mBusiness0F.mInvoiceDataMonths, 
	                R.layout.lay_selfservice_invoicestatement_listitem);
		}
		mInvoiceStatementList.setAdapter(adapter);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onInitSuccess() 
	{
		
	}
	
	@Override
	public void onInitFail() 
	{
		
	}
}
