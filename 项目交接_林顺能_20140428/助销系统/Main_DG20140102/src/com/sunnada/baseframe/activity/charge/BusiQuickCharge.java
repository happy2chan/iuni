package com.sunnada.baseframe.activity.charge;

import java.util.Date;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.BusinessEPay;
import com.sunnada.baseframe.business.BusinessRecharge;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ModifyPswdDialog;
import com.sunnada.baseframe.dialog.PhoneQueryDialog;
import com.sunnada.baseframe.dialog.ResetSrvDialog;
import com.sunnada.baseframe.dialog.SearchBluetoothDialog;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ButtonGroupUtil;
import com.sunnada.baseframe.util.PrintUtil;
import com.sunnada.baseframe.util.StringUtil;
import com.sunnada.baseframe.util.ButtonGroupUtil.ButtonGroupClickListener;
import com.sunnada.baseframe.util.ButtonGroupUtil.ViewGetter;

public class BusiQuickCharge extends BusiCharge implements OnClickListener, ViewGetter
{
	// 标题
	protected LinearLayout			mLinearBacktoMainMenu;							// 返回到首页
	protected ButtonGroupUtil		mBtnGroupQuickType;
	protected boolean				mIsQuickPay				= false;
	protected Handler				mHandler				= null;
	private   int					mTranceType				= 0x04;
	// 第一步(充值信息输入)
	protected LinearLayout			mLinearChargesInfo;
	protected EditText 				mEditPhone;
	protected ButtonGroupUtil		mBtnGroupMoney;									// 金额选择框
	protected LinearLayout			mLinearOtherMoney;								// 其他金额布局
	protected EditText				mEditMoney;										// 金额输入框
	protected boolean				mIsOther;										// 是否是其他金额
	protected EditText				mEditPswd;										// 交易密码输入框
	
	protected PhoneQueryDialog		mPhoneQueryDialog		= null;
	protected String 				mStrChargeMoney 		= "20";
	protected String				mStrPswd;
	
	// 第二步(充值信息确认)
	protected RelativeLayout		mLinearEnsureChargesInfo;
	protected TextView				mTextEnsurePhone;
	protected LinearLayout			mLinearEnsureUserInfo;
	protected TextView				mTextEnsureUser;
	protected TextView				mTextEnsureMoney;
	
	// 第三步(充值成功信息确认)
	protected RelativeLayout		mLinearPrint;
	protected TextView				mTextPrintBusiType;								// 业务类型
	protected TextView				mTextPrintPhone;								// 手机号码
	protected TextView				mTextPrintMoney;								// 充值金额
	protected TextView				mTextPrintPreMoney;								// 充值前余额
	protected TextView				mTextPrintChargeTime;							// 充值时间
	protected TextView				mTextPrintBillNum;								// 交易流水号
	
	// 第四步
	protected RelativeLayout		mLinearSendSms;
	protected EditText				mEditNotifyNum;									// 被通知手机号码
	
	protected BusinessEPay			mBusinessEPay;
	protected BusinessRecharge		mBusinessRecharge;
	protected String 				mErrorTypes[] 			= null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_charge_quick);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_charge_quick_orange);
		}
		// 
		mHandler = mRechargeHandler;
		// 交费协议
		mBusinessRecharge = new BusinessRecharge(this, equipmentService, socketService, dataBaseService, mRechargeHandler);
		mBusinessRecharge.mTranceType = 0x04;
		// 充值协议
		mBusinessEPay = new BusinessEPay(this, equipmentService, socketService, dataBaseService, mEPayHandler);
		mBusinessEPay.mTranceType = 0x01;
		// 初始化控件
		initViews();
		// 显示第一页
		showPage(0x01);
		// 刷新第一页
		refreshPage(0x01);
	}
	
	// 初始化控件
	protected void initViews() 
	{
		// 初始化第一步
		initStep1();
		// 初始化第二步
		initStep2();
		// 初始化第三步
		initStep3();
		// 初始化第四步
		initStep4();
	}
	
	// 初始化第一步
	protected void initStep1() 
	{
		// 左边布局
		mLinearBacktoMainMenu = (LinearLayout) findViewById(R.id.linear_backto_mainmenu);
		mLinearBacktoMainMenu.setOnClickListener(this);
		
		// 快捷类型
		try
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mBtnGroupQuickType = ButtonGroupUtil.createBean(this, 
						new int[] {R.id.btn_phone_charge, R.id.btn_phone_pay}, 
						new String[] {"  手机交费  ", "  手机充值  "}, 
						R.drawable.btn_tip_select, 
						R.drawable.btn_tip_selected);
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mBtnGroupQuickType = ButtonGroupUtil.createBean(this, 
						new int[] {R.id.btn_phone_charge, R.id.btn_phone_pay}, 
						new String[] {"  手机交费  ", "  手机充值  "}, 
						R.drawable.btn_tip_select_orange, 
						R.drawable.btn_tip_selected_orange);
			}
			
			mBtnGroupQuickType.setSelected(0);
			mBtnGroupQuickType.setClickListener(new ButtonGroupClickListener() 
			{
				@Override
				public void onClick(int previousId, int newId) 
				{
					// 显示第一页
					showPage(0x01);
					// 刷新第一页
					refreshPage(0x01);
					// 手机交费
					if(newId == R.id.btn_phone_charge) 
					{
						mIsQuickPay = false;
						mHandler = mRechargeHandler;
						mLinearOtherMoney.setVisibility(View.VISIBLE);
						mTranceType = 0x04;
						// 检测异常交易记录
						checkErrorDeal(new String[] {"04"});
					}
					// 手机充值
					else 
					{
						mIsQuickPay = true;
						mHandler = mEPayHandler;
						mLinearOtherMoney.setVisibility(View.GONE);
						mTranceType = 0x01;
						// 检测异常交易记录
						checkErrorDeal(new String[] {"01"});
					}
				}
			});
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		// 第一步(用户信息输入)
		mLinearChargesInfo 	= (LinearLayout) findViewById(R.id.layout_chargeinfo);
		mEditPhone 			= (EditText) findViewById(R.id.et_phone);
		mLinearOtherMoney 	= (LinearLayout) findViewById(R.id.linear_other_money);
		mEditMoney			= (EditText) findViewById(R.id.et_money);
		mEditPswd			= (EditText) findViewById(R.id.et_password);
		
		// 设置查询按钮
		Button btnPhoneQuery = (Button) findViewById(R.id.btn_phone_query);
		btnPhoneQuery.setOnClickListener(this);
		btnPhoneQuery.setVisibility(View.VISIBLE);
		
		MyCustomButton btnStep1Submit = (MyCustomButton) findViewById(R.id.btn_step1_submit);
		btnStep1Submit.setTextViewText1("提交");
		btnStep1Submit.setOnClickListener(this);
		btnStep1Submit.setOnTouchListener(this);
		
		// 设置金额选择
		try
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mBtnGroupMoney = ButtonGroupUtil.createBean(this, 
						new int[] {R.id.btn_20, R.id.btn_30, R.id.btn_50, R.id.btn_100, R.id.btn_300, R.id.btn_other}, 
						new String[] {"20", "30", "50", "100", "300", "其他金额"}, 
						R.drawable.btn_tip_select, 
						R.drawable.btn_tip_selected);
				btnStep1Submit.setImageResource(R.drawable.btn_custom_check);
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mBtnGroupMoney = ButtonGroupUtil.createBean(this, 
						new int[] {R.id.btn_20, R.id.btn_30, R.id.btn_50, R.id.btn_100, R.id.btn_300, R.id.btn_other}, 
						new String[] {"20", "30", "50", "100", "300", "其他金额"}, 
						R.drawable.btn_tip_select_orange, 
						R.drawable.btn_tip_selected_orange);
				btnStep1Submit.setImageResource(R.drawable.btn_custom_check_orange);
			}
			
			mBtnGroupMoney.setSelected(0);
			mBtnGroupMoney.setClickListener(new ButtonGroupClickListener() 
			{
				@Override
				public void onClick(int previousId, int newId) 
				{
					switch(newId) 
					{
					case R.id.btn_20:
						mStrChargeMoney = "20";
						mEditMoney.setVisibility(View.GONE);
						mIsOther = false;
						break;
						
					case R.id.btn_30:
						mStrChargeMoney = "30";
						mEditMoney.setVisibility(View.GONE);
						mIsOther = false;
						break;
						
					case R.id.btn_50:
						mStrChargeMoney = "50";
						mEditMoney.setVisibility(View.GONE);
						mIsOther = false;
						break;
						
					case R.id.btn_100:
						mStrChargeMoney = "100";
						mEditMoney.setVisibility(View.GONE);
						mIsOther = false;
						break;
						
					case R.id.btn_300:
						mStrChargeMoney = "300";
						mEditMoney.setVisibility(View.GONE);
						mIsOther = false;
						break;
						
					case R.id.btn_other:
						mEditMoney.setVisibility(View.VISIBLE);
						mEditMoney.setText("");
						mEditMoney.requestFocus();
						mIsOther = true;
						break;
						
					default:
						break;
					}
				}
			});
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		// 金额输入框监听事件
		mEditMoney.addTextChangedListener(new android.text.TextWatcher()
		{
			String mPrevStr = "";
			
			public void afterTextChanged(Editable editable) 
			{
				String str = mEditMoney.getText().toString();
				if (StringUtil.isEmptyOrNull(str)) 
				{
					return;
				}
				if (!"0".equals(str) && !StringUtil.isPattern(str, "^(?!0+(?:\\.00+)?$)(?:[1-9]\\d*|0)(?:\\.\\d{1,2})?$")
							&& str.charAt(str.length() - 1) != '.') 
				{
					setTextMoney(mPrevStr);
					return;
				}
				
				try {
					// 0.
					if(str.charAt(str.length()-1) != '.') 
					{
						Double money = Double.parseDouble(str);
						if (money < 0 || money >= 10000) 
						{
							setTextMoney(mPrevStr);
						}
					}
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					setTextMoney(mPrevStr);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) 
			{
				mPrevStr = mEditMoney.getText().toString();
			}
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				
			}
			
			// 设置金额
			private void setTextMoney(String money) 
			{
				mEditMoney.setText(money);
				mEditMoney.requestFocus();
				mEditMoney.setSelection(money.length());
			}
		});
	}
	
	// 初始化第二步
	protected void initStep2() 
	{
		mLinearEnsureChargesInfo 	= (RelativeLayout) findViewById(R.id.layout_ensure_chargeinfo);
		mTextEnsurePhone 			= (TextView) findViewById(R.id.tv_ensure_phone);
		mLinearEnsureUserInfo		= (LinearLayout) findViewById(R.id.linear_ensure_chargeinfo_user);
		mTextEnsureUser 			= (TextView) findViewById(R.id.tv_ensure_user);
		mTextEnsureMoney 			= (TextView) findViewById(R.id.tv_ensure_money);
		
		MyCustomButton btnEnsureReturn = (MyCustomButton) findViewById(R.id.btn_ensure_return);
		btnEnsureReturn.setTextViewText1("返回");
		btnEnsureReturn.setOnClickListener(this);
		btnEnsureReturn.setOnTouchListener(this);
		
		MyCustomButton btnEnsureSubmit = (MyCustomButton) findViewById(R.id.btn_ensure_submit);
		btnEnsureSubmit.setTextViewText1("确定");
		btnEnsureSubmit.setOnClickListener(this);
		btnEnsureSubmit.setOnTouchListener(this);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			btnEnsureReturn.setImageResource(R.drawable.btn_reselect);
			btnEnsureSubmit.setImageResource(R.drawable.btn_custom_check);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			btnEnsureReturn.setImageResource(R.drawable.btn_reselect_orange);
			btnEnsureSubmit.setImageResource(R.drawable.btn_custom_check_orange);
		}
	}
	
	// 初始化第三步
	private void initStep3() 
	{
		mLinearPrint 		= (RelativeLayout) findViewById(R.id.layout_charge_print);
		mTextPrintBusiType 	= (TextView) findViewById(R.id.tv_print_busitype);
		mTextPrintPhone 	= (TextView) findViewById(R.id.tv_print_phone);
		mTextPrintMoney 	= (TextView) findViewById(R.id.tv_print_money);
		mTextPrintPreMoney 	= (TextView) findViewById(R.id.tv_print_pre_money);
		mTextPrintChargeTime = (TextView) findViewById(R.id.tv_print_time);
		mTextPrintBillNum 	= (TextView) findViewById(R.id.tv_print_billnum);
		
		MyCustomButton btnPrintCancel = (MyCustomButton) findViewById(R.id.btn_print_cancel);
		btnPrintCancel.setTextViewText1("不打印");
		btnPrintCancel.setOnClickListener(this);
		btnPrintCancel.setOnTouchListener(this);
		
		MyCustomButton btnPrint = (MyCustomButton) findViewById(R.id.btn_print);
		btnPrint.setTextViewText1("受理单打印");
		btnPrint.setOnClickListener(this);
		btnPrint.setOnTouchListener(this);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			btnPrintCancel.setImageResource(R.drawable.btn_reselect);
			btnPrint.setImageResource(R.drawable.btn_custom_check);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			btnPrintCancel.setImageResource(R.drawable.btn_reselect_orange);
			btnPrint.setImageResource(R.drawable.btn_custom_check_orange);
		}
	}
	
	// 初始化第四步
	private void initStep4() 
	{
		mLinearSendSms = (RelativeLayout) findViewById(R.id.layout_send_sms);
		mEditNotifyNum = (EditText) findViewById(R.id.et_notify_phone);
		
		MyCustomButton btnSendSmsCancel = (MyCustomButton) findViewById(R.id.btn_send_cancel);
		btnSendSmsCancel.setTextViewText1("不发送");
		btnSendSmsCancel.setOnClickListener(this);
		btnSendSmsCancel.setOnTouchListener(this);
		
		MyCustomButton btnSendSms = (MyCustomButton) findViewById(R.id.btn_send);
		btnSendSms.setTextViewText1("发送通知");
		btnSendSms.setOnClickListener(this);
		btnSendSms.setOnTouchListener(this);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			btnSendSmsCancel.setImageResource(R.drawable.btn_reselect);
			btnSendSms.setImageResource(R.drawable.btn_custom_check);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			btnSendSmsCancel.setImageResource(R.drawable.btn_reselect_orange);
			btnSendSms.setImageResource(R.drawable.btn_custom_check_orange);
		}
	}
	
	// 显示第几页
	protected void showPage(int nPageIdx)
	{
		if(nPageIdx == 0x01)
		{
			mLinearChargesInfo.setVisibility(View.VISIBLE);
			mLinearEnsureChargesInfo.setVisibility(View.GONE);
			mLinearPrint.setVisibility(View.GONE);
			mLinearSendSms.setVisibility(View.GONE);
		}
		else if(nPageIdx == 0x02)
		{
			mLinearChargesInfo.setVisibility(View.GONE);
			mLinearEnsureChargesInfo.setVisibility(View.VISIBLE);
			mLinearPrint.setVisibility(View.GONE);
			mLinearSendSms.setVisibility(View.GONE);
		}
		else if(nPageIdx == 0x03)
		{
			mLinearChargesInfo.setVisibility(View.GONE);
			mLinearEnsureChargesInfo.setVisibility(View.GONE);
			mLinearPrint.setVisibility(View.VISIBLE);
			mLinearSendSms.setVisibility(View.GONE);
		}
		else if(nPageIdx == 0x04)
		{
			mLinearChargesInfo.setVisibility(View.GONE);
			mLinearEnsureChargesInfo.setVisibility(View.GONE);
			mLinearPrint.setVisibility(View.GONE);
			mLinearSendSms.setVisibility(View.VISIBLE);
		}
	}
	
	// 清空密码 并获取焦点
	protected void clearPWD()
	{
		mEditPswd.setText("");
		mEditPswd.requestFocus();
	}
	
	// 刷新第几页
	protected void refreshPage(int nPageIdx) 
	{
		// 第一步 输入充值信息
		if(nPageIdx == 0x01) 
		{
			// 清空输入信息
			clearStep1();
		}
		// 第二步 确认交费信息
		else if(nPageIdx == 0x02) 
		{
			/*
			if(StringUtil.isEmptyOrNull(mStrUserInfo)) 
			{
				mLinearEnsureUserInfo.setVisibility(View.GONE);
			}
			else
			{
				mLinearEnsureUserInfo.setVisibility(View.VISIBLE);
				try 
				{
					// 取出用户名
					// 格式: "手机号码:13218008888\n用户名:测试用户\n充值前可用余额: 500.00元\n欠费/应缴费: 0元
					String[] temp1 = mStrUserInfo.split("\n");
					if(temp1 != null) 
					{
						String[] temp2 = temp1[1].split(":");
						if(!StringUtil.isEmptyOrNull(temp2[1])) 
						{
							mTextEnsureUser.setText(temp2[1]);
						}
						else
						{
							temp2 = temp1[1].split("：");
							if(!StringUtil.isEmptyOrNull(temp2[1])) 
							{
								mTextEnsureUser.setText(temp2[1]);
							}
						}
					}
				}
				catch(Exception e) 
				{
					e.printStackTrace();
				}
			}
			*/
			
			// 手机账户名
			mLinearEnsureUserInfo.setVisibility(View.GONE);
			// 手机号码
			mTextEnsurePhone.setText(mStrTelNum);
			// 交费金额
			mTextEnsureMoney.setText(mStrChargeMoney + "元");
		}
		// 第三步 凭条打印确认界面
		else if(nPageIdx == 0x03) 
		{
			if(mTranceType == 0x04)
			{
				// 业务类型
				mTextPrintBusiType.setText(mBusinessRecharge.backtype());
				// 充值号码
				mTextPrintPhone.setText(mStrTelNum);
				// 充值金额
				mTextPrintMoney.setText(mBusinessRecharge.mStrChargeMoney);
				// 充值前金额
				mTextPrintPreMoney.setText(mBusinessRecharge.mStrPreMoney + "元");
				// 充值时间
				mTextPrintChargeTime.setText(StringUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
				// 业务流水号
				mTextPrintBillNum.setText(mBusinessRecharge.mStrBillNum);
			}
		    else if(mTranceType == 0x01) 
			{
				// 业务类型
				mTextPrintBusiType.setText(mBusinessEPay.backtype());
				// 充值号码
				mTextPrintPhone.setText(mStrTelNum);
				// 充值金额
				mTextPrintMoney.setText(mBusinessEPay.mStrChargeMoney);
				// 充值前金额
				mTextPrintPreMoney.setText(mBusinessEPay.mStrPreMoney + "元");
				// 充值时间
				mTextPrintChargeTime.setText(StringUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
				// 业务流水号
				mTextPrintBillNum.setText(mBusinessEPay.mStrBillNum);
			}
		}
		// 发送短信通知界面
		else if(nPageIdx == 0x04) 
		{
			if(mStrTelNum.indexOf("-") != -1)
			{
				mEditNotifyNum.setText("");
			}
			else
			{
				mEditNotifyNum.setText(mStrTelNum);
			}
		}
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 返回首页
		case R.id.linear_backto_mainmenu:
			back2Main();
			break;
			
		// 手机账户查询
		case R.id.btn_phone_query:
			handleQueryPhone();
			break;
			
		// 确认
		case R.id.btn_step1_submit:
			handleGotoStep2();
			break;
			
		// 第二步返回上一步
		case R.id.btn_ensure_return:
			showPage(0x01);
			break;
			
		// 确认充值
		case R.id.btn_ensure_submit:
			// 清除交易密码
			clearPWD();
			// 发起充值流程
			handleCharge();
			break;
			
		// 凭条打印
		case R.id.btn_print:
			mHandler.sendEmptyMessage(Statics.MSG_PRINT);
			break;
			
		// 不打印凭条
		case R.id.btn_print_cancel:
			mHandler.sendEmptyMessage(Statics.MSG_PRINT_CANCEL);
			break;
			
		// 短信通知
		case R.id.btn_send:
			procSendSms();
			break;
			
		// 不发送短信通知
		case R.id.btn_send_cancel:
			// 显示第3页
			showPage(0x03);
			// 刷新第3页
			refreshPage(0x03);
			break;
			
		default:
			break;
		}
	}
	
	// 清空所有填写框
	protected void clearStep1() 
	{
		// 手机号码
		if(mEditPhone != null) 
		{
			mEditPhone.setText("");
			mEditPhone.requestFocus();
		}
		// 充值金额
		mBtnGroupMoney.setSelected(0);
		mEditMoney.setVisibility(View.GONE);
		mEditMoney.setText("");
		mStrChargeMoney = "20";
		mIsOther = false;
		// 交易密码
		mEditPswd.setText("");
	}
	
	// 处理手机账户信息的查询
	private void handleQueryPhone() 
	{
		mStrTelNum = mEditPhone.getText().toString();
		Log.e(BusinessEPay.TAG, "当前手机号码为: " + mStrTelNum);
		// 判断手机号码是不是为11位
		if(mStrTelNum.length() != 11)
		{
			DialogUtil.MsgBox("温馨提示", "请输入11位手机号码！");
			return;
		}
		
		// 手机交费
		if(mIsQuickPay == false) 
		{
			mBusinessRecharge.mStrTelNum = mStrTelNum;
			mPhoneQueryDialog = new PhoneQueryDialog(this, mStrTelNum, mHandler, mBusinessRecharge);
		}
		// 手机充值
		else
		{
			mBusinessEPay.mStrTelNum = mStrTelNum;
			mPhoneQueryDialog = new PhoneQueryDialog(this, mStrTelNum, mHandler, mBusinessEPay);
		}
		mPhoneQueryDialog.show();
	}
	
	// 处理跳转到第二步
	private void handleGotoStep2() 
	{
		// 判断手机号码
		mStrTelNum = mEditPhone.getText().toString();
		if(StringUtil.isEmptyOrNull(mStrTelNum)) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入手机号码！");
			return;
		}
		if(mStrTelNum.length() != 11) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入11位手机号码！");
			return;
		}
		
		// 充值金额(其他金额)
		if(mIsOther == true) 
		{
			String strMoney = mEditMoney.getText().toString();
			if(StringUtil.isEmptyOrNull(strMoney)) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入金额！");
				return;
			}
			if(isIllegalMoney(strMoney)) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "您输入的金额有误！");
				return;
			}
			mStrChargeMoney = strMoney;
		}
		
		// 手机交费
		if(mIsQuickPay == false) 
		{
			Log.e(BusinessEPay.TAG, "交费金额为:" + mStrChargeMoney);
			mBusinessRecharge.mChargeMoney = (int)(Double.parseDouble(mStrChargeMoney)*1000/10);
			if(mBusinessRecharge.mChargeMoney == 0) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "交费金额不能为0！");
				return;
			}
			mStrChargeMoney = String.format("%.2f", (float)mBusinessRecharge.mChargeMoney/100);
			mBusinessRecharge.mStrChargeMoney = mStrChargeMoney;
		}
		// 手机充值
		else
		{
			Log.e(BusinessEPay.TAG, "充值金额为:" + mStrChargeMoney);
			mBusinessEPay.mChargeMoney = (int)(Double.parseDouble(mStrChargeMoney)*1000/10);
			if(mBusinessEPay.mChargeMoney == 0) 
			{
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "交费金额不能为0！");
				return;
			}
			mStrChargeMoney = String.format("%.2f", (float)mBusinessEPay.mChargeMoney/100);
			mBusinessEPay.mStrChargeMoney = mStrChargeMoney;
		}
		
		// 校验密码
		mStrPswd = mEditPswd.getText().toString();
		if(StringUtil.isEmptyOrNull(mStrPswd)) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入交易密码！");
			return;
		}
		if(mStrPswd.length() != 6)
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入6位交易密码！");
			return;
		}
		if(!Statics.HAD_PWD)
		{
			if(Statics.MODIFY_TYPE == 1)
			{
				DialogUtil.MsgBox("温馨提示", "您还未设置过初始密码，请先进行密码初始化！", 
				"确定", Statics.MSG_MODIFY_PSWD, "取消", -1, -1, mHandler);
			}
			else if(Statics.MODIFY_TYPE == 2)
			{
				DialogUtil.MsgBox("温馨提示", "您还未执行过交易密码重置，请先重置交易密码！", 
						"确定", Statics.MSG_RESET_PSWD, "取消", -1, -1, mHandler);
			}
		}
		else
		{
			// 手机交费
			if(mIsQuickPay == false) 
			{
				mBusinessRecharge.mStrTelNum = mStrTelNum;
				mBusinessRecharge.mStrPswd = mStrPswd;
			}
			// 手机充值
			else
			{
				mBusinessEPay.mStrTelNum = mStrTelNum;
				mBusinessEPay.mStrPswd = mStrPswd;
			}
			
			// 显示第二步
			showPage(0x02);
			// 刷新第二步
			refreshPage(0x02);
		}
	}
	
	// 处理充值请求
	private void handleCharge() 
	{
		new Thread() 
		{
			public void run() 
			{
				// 手机交费
				if(mIsQuickPay == false) 
				{
					mBusinessRecharge.doRecharge(0x01);
				}
				// 手机充值
				else
				{
					mBusinessEPay.doEPayStep1();
				}
			}
		}.start();
	}
	
	// 处理短信发送
	private void procSendSms() 
	{
		// 校验短信通知号码
		mBusinessRecharge.mStrNotifyNum = mEditNotifyNum.getText().toString();
		mBusinessEPay.mStrNotifyNum = mEditNotifyNum.getText().toString();
		if(StringUtil.isEmptyOrNull(mBusinessRecharge.mStrNotifyNum)) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入短信通知号码！");
			return;
		}
		if(mBusinessRecharge.mStrNotifyNum.length() != 11) 
		{
			DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "请输入11位短信通知号码！");
			return;
		}
		
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在发送短信通知...");
				// 手机交费
				if(mIsQuickPay == false) 
				{
					mBusinessRecharge.bll0205();
				}
				// 手机充值
				else
				{
					mBusinessEPay.bll0205();
				}
				DialogUtil.closeProgress();
			}
		}.start();
	}
	
	// 交费Handler
	protected Handler mRechargeHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch (message.what)
			{
				
			// 初始化交易密码
			case Statics.MSG_MODIFY_PSWD:
				new ModifyPswdDialog(BusiQuickCharge.this, 
				new Business_00(BusiQuickCharge.this, equipmentService, socketService, dataBaseService), mHandler)
				.show();
				break;
				
			// 重置交易密码
			case Statics.MSG_RESET_PSWD:
				new ResetSrvDialog(BusiQuickCharge.this, 
				new Business_00(BusiQuickCharge.this, equipmentService, socketService, dataBaseService), mHandler)
				.show();
				break;
				
			// 修改交易密码成功
			case Statics.MSG_MODIFY_PSWD_SUCCESS:
				mEditPswd.setText("");
				break;
					
			// 校验验证码成功
			case Statics.MSG_VERIFY_OK:
				mPhoneQueryDialog.dismiss();
				// 查询用户信息
				queryUserInfo(mBusinessRecharge);
				break;
				
			// 密码错误或者不允许异地交费
			case Statics.MSG_PWD_FAIL:
				// 显示第1页
				showPage(0x01);
				// 密码框清空并获取焦点
				clearPWD();
				break;
				
			// 发起交费请求失败
			case Statics.MSG_RECHARGE_FAIL:
				// 跳转到第一页
				showPage(0x01);
				// 清除交易密码
				clearPWD();
				break;
				
			// 交费成功, 跳到是否发送短信通知的界面
			case Statics.MSG_RECHARGE_OK:
				// 显示第4页
				showPage(0x04);
				// 刷新第4页
				refreshPage(0x04);
				break;
				
			// 发送短信通知成功, 返回第一步
			case Statics.MSG_NOTIFY_OK:
				// 显示第一步
				showPage(0x01);
				// 清空用户输入
				clearStep1();
				// 检测异常交易记录
				checkErrorDeal(new String[] {"04"});
				break;
				
			// 发送短信通知失败, 跳到受理单打印的界面
			case Statics.MSG_NOTIFY_FAIL:
				// 显示第3页
				showPage(0x03);
				// 刷新第3页
				refreshPage(0x03);
				break;
				
			// 充值成功, 打印交易凭条
			case Statics.MSG_PRINT:
			// 继续检测打印机状态
			case Statics.MSG_RECHARGE_OK_CHECK:
				new Thread() 
				{
					public void run() 
					{
						// 检测蓝牙设备是否存在
						DialogUtil.showProgress("正在检测蓝牙设备...");
						if(equipmentService.handshake() == false) 
						{
							DialogUtil.closeProgress();
							DialogUtil.MsgBox("请连接蓝牙设备", 
									"检测到当前无蓝牙打印设备, 是否连接蓝牙?", 
									"立即搜索", 
									Statics.MSG_RECHARGE_OK_CONNECT_BLUETOOTH, 
									"取消", 
									Statics.MSG_PRINT_CANCEL, 
									Statics.MSG_PRINT_CANCEL, 
									mHandler);
							return;
						}
						// 打印交费信息
						printRechargeInfo();
					};
				}.start();
				break;
				
			// 连接蓝牙设备
			case Statics.MSG_RECHARGE_OK_CONNECT_BLUETOOTH:
				SearchBluetoothDialog dialog = new SearchBluetoothDialog(BusiQuickCharge.this, mBluetoothClient, equipmentService, 
						mHandler, Statics.MSG_RECHARGE_OK_CONNECT_DONE);
				dialog.show();
				break;
				
			// 蓝牙蓝牙设备完成
			case Statics.MSG_RECHARGE_OK_CONNECT_DONE:
				new Thread() 
				{
					public void run() 
					{
						// 握手失败, 说明设备没连上
						DialogUtil.showProgress("正在检测蓝牙设备状态...");
						if(equipmentService.handshake() == false) 
						{
							DialogUtil.closeProgress();
							mHandler.sendEmptyMessage(Statics.MSG_PRINT_CANCEL);
							return;
						}
						// 打印交费信息
						printRechargeInfo();
					}
				}.start();
				break;
				
			// 取消打印
			case Statics.MSG_PRINT_CANCEL:
				mHandler.sendEmptyMessage(Statics.MSG_NOTIFY_OK);
				break;
				
			default:
				break;
			}
		}
	};
	
	// 充值信息处理器
	protected Handler mEPayHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch (message.what)
			{
				
			// 初始化交易密码
			case Statics.MSG_MODIFY_PSWD:
				new ModifyPswdDialog(BusiQuickCharge.this, 
				new Business_00(BusiQuickCharge.this, equipmentService, socketService, dataBaseService), mHandler)
				.show();
				break;
				
			// 重置交易密码
			case Statics.MSG_RESET_PSWD:
				new ResetSrvDialog(BusiQuickCharge.this, 
				new Business_00(BusiQuickCharge.this, equipmentService, socketService, dataBaseService), mHandler)
				.show();
				break;
				
			// 修改交易密码成功
			case Statics.MSG_MODIFY_PSWD_SUCCESS:
				mEditPswd.setText("");
				break;
					
			// 业务开始检测打印机状态不正常按<确定>退出
			case Statics.MSG_CHECK_PRINTER:
			case Statics.MSG_CHECK_PRINTER_CANCEL:
				finish();
				break;
				
			// 校验验证码成功
			case Statics.MSG_VERIFY_OK:
				mPhoneQueryDialog.dismiss();
				// 查询用户信息
				queryUserInfo(mBusinessEPay);
				break;
				
			// 交易密码错误或者异地交费
			case Statics.MSG_PWD_FAIL:
			// 获取卡密失败
			case Statics.MSG_GET_ELEC_FAIL:
				// 显示第1页
				showPage(0x01);
				// 密码框清空并获取焦点
				clearPWD();
				break;
				
			// 发起充值请求失败
			case Statics.MSG_RECHARGE_FAIL:
				// 跳转到第一页
				showPage(0x01);
				// 刷新第一页
				refreshPage(0x01);
				// 提示充值异常
				String ss = "充值出现异常, 请拨打10011, 用以下卡密进行充值: \n\n";
				ss += mBusinessEPay.getEcardInfo((byte)0x01, false).toString();
				DialogUtil.MsgBox("请记录充值卡密", 
						ss, 
						"打印卡密", 
						Statics.MSG_RECHARGE_FAIL_PRINT, 
						"不打印", 
						Statics.MSG_RECHARGE_FAIL_PRINT_CANCEL, 
						Statics.MSG_RECHARGE_FAIL_PRINT_CANCEL, 
						mHandler);
				break;
				
			// 不打印卡密信息
			case Statics.MSG_RECHARGE_FAIL_PRINT_CANCEL:
				DialogUtil.MsgBox(BusinessEPay.TITLE_STR, 
						"请务必在30天之内通过助销系统电子卡卡密打印功能进行卡密打印！", 
						"确定", 
						Statics.MSG_RECHARGE_REPORT_PRINT_FAIL, 
						null, 
						0x00, 
						Statics.MSG_RECHARGE_REPORT_PRINT_FAIL, 
						mHandler);
				break;
				
			// 上报未打印卡密
			case Statics.MSG_RECHARGE_REPORT_PRINT_FAIL:
				reportEcardBill(0x02, Statics.MSG_RECHARGE_REPORT_PRINT_FAIL);
				break;
				
			// 上报打印成功卡密
			case Statics.MSG_RECHARGE_REPORT_PRINT_OK:
				reportEcardBill(0x01, Statics.MSG_RECHARGE_REPORT_PRINT_OK);
				break;
				
			// 打印卡密信息
			case Statics.MSG_RECHARGE_FAIL_PRINT:
				new Thread() 
				{
					public void run() 
					{
						// 检测蓝牙打印机是否存在
						if(equipmentService.handshake() == false) 
						{
							DialogUtil.closeProgress();
							DialogUtil.MsgBox("请连接蓝牙设备", 
									"检测到当前无蓝牙打印设备, 是否连接蓝牙?", 
									"立即搜索", 
									Statics.MSG_RECHARGE_FAIL_CONNECT_BLUETOOTH, 
									"取消", 
									Statics.MSG_RECHARGE_FAIL_PRINT_CANCEL, 
									Statics.MSG_RECHARGE_FAIL_PRINT_CANCEL, 
									mHandler);
							return;
						}
						// 打印卡密信息
						printEcardInfo();
					}
				}.start();
				break;
				
			// 连接蓝牙
			case Statics.MSG_RECHARGE_FAIL_CONNECT_BLUETOOTH:
				SearchBluetoothDialog dialog1 = new SearchBluetoothDialog(BusiQuickCharge.this, mBluetoothClient, equipmentService, 
						mHandler, Statics.MSG_RECHARGE_FAIL_CONNECT_DONE);
				dialog1.show();
				break;
				
			// 连接蓝牙完成
			case Statics.MSG_RECHARGE_FAIL_CONNECT_DONE:
				new Thread() 
				{
					public void run() 
					{
						// 检测蓝牙打印机是否存在
						DialogUtil.showProgress("正在检测蓝牙设备状态...");
						if(equipmentService.handshake() == false) 
						{
							DialogUtil.closeProgress();
							mHandler.sendEmptyMessage(Statics.MSG_RECHARGE_FAIL_PRINT_CANCEL);
							return;
						}
						// 打印卡密信息
						printEcardInfo();
					}
				}.start();
				break;
				
			// 充值成功显示短信通知的界面
			case Statics.MSG_RECHARGE_OK:
				// 显示第三页
				showPage(0x04);
				// 刷新第三页
				refreshPage(0x04);
				break;
				
			// 发送短信通知成功:
			case Statics.MSG_NOTIFY_OK:
				// 显示第一步
				showPage(0x01);
				// 清空用户输入
				clearStep1();
				// 检测异常交易记录
				checkErrorDeal(new String[] {"01"});
				break;
				
			// 发送短信通知失败, 显示打印受理单的界面
			case Statics.MSG_NOTIFY_FAIL:
				// 显示第3页
				showPage(0x03);
				// 刷新第3页
				refreshPage(0x03);
				break;
				
			// 充值成功, 打印交易凭条
			case Statics.MSG_PRINT:
			// 继续检测打印机状态
			case Statics.MSG_RECHARGE_OK_CHECK:
				new Thread() 
				{
					public void run() 
					{
						// 检测蓝牙设备是否存在
						DialogUtil.showProgress("正在检测蓝牙打印设备...");
						if(equipmentService.handshake() == false) 
						{
							DialogUtil.closeProgress();
							DialogUtil.MsgBox("请连接蓝牙设备", 
									"检测到当前无蓝牙打印设备, 是否连接蓝牙?", 
									"立即搜索", 
									Statics.MSG_RECHARGE_OK_CONNECT_BLUETOOTH, 
									"取消", 
									Statics.MSG_PRINT_CANCEL, 
									Statics.MSG_PRINT_CANCEL, 
									mHandler);
							return;
						}
						// 打印充值信息
						printEPayInfo();
					};
				}.start();
				break;
				
			// 连接蓝牙设备
			case Statics.MSG_RECHARGE_OK_CONNECT_BLUETOOTH:
				SearchBluetoothDialog dialog2 = new SearchBluetoothDialog(BusiQuickCharge.this, mBluetoothClient, equipmentService, 
						mHandler, Statics.MSG_RECHARGE_OK_CONNECT_DONE);
				dialog2.show();
				break;
				
			// 连接蓝牙设备完成
			case Statics.MSG_RECHARGE_OK_CONNECT_DONE:
				new Thread() 
				{
					public void run() 
					{
						// 握手失败, 说明设备没连上
						DialogUtil.showProgress("正在检测蓝牙设备状态...");
						if(equipmentService.handshake() == false) 
						{
							DialogUtil.closeProgress();
							mHandler.sendEmptyMessage(Statics.MSG_PRINT_CANCEL);
							return;
						}
						// 打印充值信息
						printEPayInfo();
					}
				}.start();
				break;
				
			// 取消打印
			case Statics.MSG_PRINT_CANCEL:
				mHandler.sendEmptyMessage(Statics.MSG_NOTIFY_OK);
				break;
				
			default:
				break;
			}
		}
	};
	
	/*
	// 检测购卡充值交易异常状态(线程中调用) 
	protected void checkEcardStatus() 
	{
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在检测打印机状态...");
				if(checkPrinter(Statics.MSG_CHECK_PRINTER, Statics.MSG_CHECK_PRINTER_CANCEL) == false) 
				{
					return;
				}
				// 检测电子卡异常文件
				DialogUtil.showProgress("正在检测异常电子卡信息...");
				SystemClock.sleep(500);
				if(checkEcardFile()) 
				{
					DialogUtil.closeProgress();
					return;
				}
				// 检测异常交易
				DialogUtil.showProgress("正在检测异常交易信息...");
				SystemClock.sleep(500);
				if(mBusinessEPay.errorDealCheck()) 
				{
					DialogUtil.showProgress("正在异常交易上报...");
					if(mBusinessEPay.errorDealReport() == true) 
					{
						DialogUtil.MsgBox(BusinessEPay.TITLE_STR, "异常交易处理完成!");
					}
				}
				DialogUtil.closeProgress();
			}
		}.start();
	}
	*/
	
	/*
	// 检测卡密文件并读取
	private boolean checkEcardFile()
	{
		File file = new File(mBusinessEPay.mEcardFile);
		if(file.exists() == false) 
		{
			Log.i(BusinessEPay.TAG, "卡密信息文件不存在!");
			return false;
		}
		// 读取文件内容并显示
		try {
			FileInputStream in = new FileInputStream(file);
			if(in.available() <= 0) 
			{
				Log.e(BusinessEPay.TAG, "卡密信息文件长度非法!");
				file.delete();
				return false;
			}
			in.read(mBusinessEPay.mErrTransType, 0, 1);
			// 充值帐号
			byte[] tempNum = new byte[50];
			in.read(tempNum, 				0, 50);
			mBusinessEPay.mStrTelNum = new String(tempNum).substring(0, Commen.endofstr(tempNum));
			in.read(mBusinessEPay.mEcardNo, 		0, 20);
			in.read(mBusinessEPay.mEcardPswd, 		0, 20);
			in.read(mBusinessEPay.mErrEcardIdx, 	0, 1);
			in.read(mBusinessEPay.mEcardEnd, 		0, 8);
			try
			{
				byte[] bllnum = new byte[30];
				in.read(bllnum, 0, 30);
				mBusinessEPay.mStrBillNum = new String(bllnum, "ASCII");
			}
			catch(Exception e) 
			{
				e.printStackTrace();
			}

			// 银联签购单信息
			//byte temp = (byte)in.read();
			//if(temp == 0x01) 
			//{
			//	mEpay.postype = 2;
			//	BaseBll.readPosDealInfo(in, mEpay.mPosDealTime, mEpay.mPosDealMoney);
			//}
			in.close();
			in = null;

			try 
			{
				mBusinessEPay.mStrChargeMoney = mSelMoney[mBusinessEPay.mErrEcardIdx[0]] + ".00";
			}
			catch(Exception e) 
			{
				e.printStackTrace();
			}
			Log.i(BusinessEPay.TAG, "读取卡密信息文件成功!");
		}
		catch(FileNotFoundException e) 
		{
			e.printStackTrace();
			Log.e(BusinessEPay.TAG, "卡密文件未找到!");
			return false;
		}
		catch(IOException e) 
		{
			e.printStackTrace();
			Log.e(BusinessEPay.TAG, "读取卡密信息文件异常!");
			return false;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			Log.e(BusinessEPay.TAG, "捕获到其他异常!");
			return false;
		}
		Log.i(BusinessEPay.TAG, "检测到卡密信息, 开始界面显示");
		mBusinessEPay.showEcardInfoScreen(0x02, mBusinessEPay.mErrTransType[0], "上笔购卡充值异常, 请确认卡密信息\n\n");
		return true;
	}
	*/
	
	// 上报未打印卡密
	private void reportEcardBill(final int nPrintResult, final int msgID) 
	{
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在上报异常卡密信息...");
				boolean result = mBusinessEPay.bll0230(nPrintResult);
				DialogUtil.closeProgress();
				if(result == false) 
				{
					DialogUtil.MsgBox(BusinessEPay.TITLE_STR, 
							mBusinessEPay.getLastknownError() + "\n是否重试?", 
							"确定", 
							msgID, 
							"取消", 
							0x00, 
							0x00, 
							mHandler);
				}
			}
		}.start();
	}
	
	// 打印交费信息
	private void printRechargeInfo() 
	{
		// 检测缺纸状态
		DialogUtil.showProgress("正在检测蓝牙打印机状态...");
		if(checkPrinter(Statics.MSG_RECHARGE_OK_CHECK, Statics.MSG_PRINT_CANCEL, mHandler) == false) 
		{
			DialogUtil.closeProgress();
			return;
		}
		// 打印
		DialogUtil.showProgress("正在打印...");
		mBusinessRecharge.print();
		DialogUtil.closeProgress();
		
		if (!equipmentService.isPrintSuccess()) 
		{
			DialogUtil.MsgBox(PrintUtil.STR_FAIL_TITLE, 
					PrintUtil.errorMsgPgAndGet(equipmentService.getPrintErrorMessage()), 
					PrintUtil.STR_FAIL_OK,
					Statics.MSG_PRINT, 
					PrintUtil.STR_FAIL_CANCEL,
					Statics.MSG_NOTIFY_OK,
					Statics.MSG_NOTIFY_OK, 
					mHandler);
		}
		else
		{
			DialogUtil.MsgBox(PrintUtil.STR_SUCCESS_TITLE, 
					  PrintUtil.STR_SUCCESS_MSG, 
					  PrintUtil.STR_SUCCESS_OK,
					  Statics.MSG_NOTIFY_OK,
					  "",
			          0, 
			          Statics.MSG_NOTIFY_OK, 
			          mHandler);
		}
	}
	
	// 打印卡密信息
	private void printEcardInfo() 
	{
		// 检测打印机状态
		if(checkPrinter(Statics.MSG_RECHARGE_FAIL_PRINT, Statics.MSG_RECHARGE_FAIL_PRINT_CANCEL, mHandler) == false) 
		{
			DialogUtil.closeProgress();
			return;
		}
		// 打印
		DialogUtil.showProgress("正在打印卡密信息...");
		mBusinessEPay.print(0x01, mBusinessEPay.mTranceType);
		DialogUtil.closeProgress();
		// 打印失败
		if (!equipmentService.isPrintSuccess()) 
		{
			DialogUtil.MsgBox(
					PrintUtil.STR_FAIL_TITLE, 
					PrintUtil.errorMsgPgAndGet(equipmentService.getPrintErrorMessage()),
					PrintUtil.STR_FAIL_OK, 
					Statics.MSG_RECHARGE_FAIL_PRINT, 
					PrintUtil.STR_FAIL_CANCEL, 
					Statics.MSG_RECHARGE_FAIL_PRINT_CANCEL, 
					Statics.MSG_RECHARGE_FAIL_PRINT_CANCEL, 
					mHandler);
		}
		else 
		{
			DialogUtil.MsgBox(
					PrintUtil.STR_FAIL_TITLE, 
					PrintUtil.STR_SUC_MSG_REPRINT, 
					PrintUtil.STR_FAIL_OK, 
					Statics.MSG_RECHARGE_FAIL_PRINT, 
					PrintUtil.STR_FAIL_CANCEL, 
					Statics.MSG_RECHARGE_REPORT_PRINT_OK, 
					Statics.MSG_RECHARGE_REPORT_PRINT_OK, 
					mHandler);
		}
	}
		
	// 打印充值信息
	private void printEPayInfo() 
	{
		// 检测打印机状态
		DialogUtil.showProgress("正在检测蓝牙打印机状态...");
		if(checkPrinter(Statics.MSG_RECHARGE_OK_CHECK, Statics.MSG_PRINT_CANCEL, mHandler) == false) 
		{
			DialogUtil.closeProgress();
			return;
		}
		// 打印
		DialogUtil.showProgress("正在打印...");
		mBusinessEPay.print(0x02, mBusinessEPay.mTranceType);
		DialogUtil.closeProgress();
		
		if (!equipmentService.isPrintSuccess()) 
		{
			DialogUtil.MsgBox(PrintUtil.STR_FAIL_TITLE, 
					PrintUtil.errorMsgPgAndGet(equipmentService.getPrintErrorMessage()), 
					PrintUtil.STR_FAIL_OK,
					Statics.MSG_PRINT, 
					PrintUtil.STR_FAIL_CANCEL,
					Statics.MSG_NOTIFY_OK,
					Statics.MSG_NOTIFY_OK, 
					mHandler);
		}
		else
		{
			DialogUtil.MsgBox(PrintUtil.STR_SUCCESS_TITLE, 
					  PrintUtil.STR_SUCCESS_MSG, 
					  PrintUtil.STR_SUCCESS_OK,
					  Statics.MSG_NOTIFY_OK,
					  "",
			          0, 
			          Statics.MSG_NOTIFY_OK, 
			          mHandler);
		}
	}
}
