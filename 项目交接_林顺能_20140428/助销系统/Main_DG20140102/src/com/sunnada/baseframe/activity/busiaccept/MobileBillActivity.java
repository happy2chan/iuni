package com.sunnada.baseframe.activity.busiaccept;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.BusinessInquiry;
import com.sunnada.baseframe.dialog.BillDetailDialog;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ICallBack;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.ui.MyCustomButtonOrange;
import com.sunnada.baseframe.util.ButtonGroupUtil.ButtonGroupClickListener;
import com.sunnada.baseframe.util.ButtonGroupUtil.ViewGetter;
import com.sunnada.baseframe.util.ARGBUtil;
import com.sunnada.baseframe.util.ButtonGroupUtil;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.StringUtil;

// 统一余额播报业务
public class MobileBillActivity extends BaseActivity implements OnClickListener, ViewGetter
{
	private LinearLayout					mLinearBacktoBusiAccept;		// 返回到业务受理二级菜单
	
	// 左边布局
	private LinearLayout                    mLinearLeft;
	private ImageView                       mIvLeftIcon;
	
	private LinearLayout					mLinearGetRandom;				// 获取验证码的布局
	private EditText						mEditTel;     					// 手机号码输入框
	private EditText                        mEditRandom; 					// 验证码输入框
	private String							mStrTelNum;  					// 手机号码
	private String							mStrRandom;   					// 验证码
	private Button							mBtnGetRandom;    				// 获取验证码按钮
	private TextView                        mTvGetRandomResult;             // 获取验证码成功给出提示
	private boolean							mIsGetRandomOK;					// 获取验证码OK?
	private MyCustomButton					mBtnQuery;
	private TextView                        mTvCountDown;                 // 倒计时
	private BusinessInquiry          		mBusinessInquiry;				// 手机账单查询业务类
	
	// 实时话费信息布局
	private LinearLayout					mLinearBillDetail;				// 实时话费信息布局
	private LinearLayout                    mLinearLine1;
	private LinearLayout                    mLinearLine2;
	private LinearLayout                    mLinearLine3;
	private LinearLayout                    mLinearLine4;
	private LinearLayout                    mLinearLine5;
	private LinearLayout                    mLinearLine6;
	private LinearLayout                    mLinearLine7;
	
	private LinearLayout[]                  mLinearLines = {
														     mLinearLine1, mLinearLine2,
														     mLinearLine3, mLinearLine4,
														     mLinearLine5, mLinearLine6,
														     mLinearLine7
															};
	
	private int[]                           mLinearLinesId = {
															   R.id.lay_line1, R.id.lay_line2,
															   R.id.lay_line3, R.id.lay_line4,
															   R.id.lay_line5, R.id.lay_line6,
															   R.id.lay_line7
															 };
	
	private TextView                        mTvTitle1;
	private TextView						mTvTitle2;
	private TextView						mTvTitle3;
	private TextView                        mTvTitle4;
	private TextView						mTvTitle5;
	private TextView						mTvTitle6;
	private TextView                        mTvTitle7;
	private TextView[]                      mTvTitles  = { 
															mTvTitle1, mTvTitle2, mTvTitle3, 
															mTvTitle4, mTvTitle5, mTvTitle6,
															mTvTitle7
														 };
	private int[]                           mTvTitlesId = {
															R.id.tv_title1, R.id.tv_title2, R.id.tv_title3, 
															R.id.tv_title4, R.id.tv_title5, R.id.tv_title6,
															R.id.tv_title7
														  };
	
	private TextView                        mTvDetail1;
	private TextView						mTvDetail2;
	private TextView						mTvDetail3;
	private TextView                        mTvDetail4;
	private TextView						mTvDetail5;
	private TextView						mTvDetail6;
	private TextView[]                      mTvDetails = {
															mTvDetail1, mTvDetail2, mTvDetail3,
															mTvDetail4, mTvDetail5, mTvDetail6
														};
	private int[]                           mTvDetailsId = {
															R.id.tv_detail1, R.id.tv_detail2, R.id.tv_detail3, 
															R.id.tv_detail4, R.id.tv_detail5,R.id.tv_detail6
														};
	
	// private MyCustomButton                  mBtnDetailQuery;				// 手机账单查询按钮
	private ImageView                       mIvDetailQuery;                 // 手机账单查询控件;
	private MyCustomButtonOrange            mBtnDetailBack;					// 返回验证码获取布局
	
	// 手机账单查询布局
	private RelativeLayout					mLinearBillQuery;				// 手机账单查询布局
	private TextView						mTextBillQueryTel;				// 手机号码
	private ButtonGroupUtil					mBillGroupUtil;
	private MyCustomButtonOrange			mBtnBillQueryBack;				// 返回到实时话费信息布局
	
	private int[] 							mBtnBillIDs = {					// 按钮组ID
														R.id.btn_bill1, 
														R.id.btn_bill2, 
														R.id.btn_bill3, 
														R.id.btn_bill4, 
														R.id.btn_bill5
													};
	private int[]							mBillQueryCode;					// 手机账单查询项编码
	private String[]						mStrBillQueryName;				// 手机账单查询项名称
	
	// 计时器
	private CountDownTimer mCountDownTimer = new CountDownTimer(180*1000, 1000) 
	{
		@Override
		public void onTick(long millisUntilFinished) 
		{
			String tempstr = String.format("剩余%d秒", millisUntilFinished/1000);
			mTvCountDown.setText(tempstr);
		}
		
		@Override
		public void onFinish() 
		{
			mTvCountDown.setVisibility(View.GONE);
			mTvGetRandomResult.setText("短信验证码已失效，请重新获取!");
			mIsGetRandomOK = false;
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_busiaccept_mobilebill_orange);
		}
		else
		{
			setContentView(R.layout.lay_busiaccept_mobilebill);
		}
		
		mBusinessInquiry = new BusinessInquiry(this, equipmentService, socketService, dataBaseService);
		// 初始化左边布局
		initLeftUI();
		// 初始化布局
		initViews();
		// 显示第一页
		showPage(0x01);
		// 初始化数据
	    // initData();
	}
	
	// 初始化左边布局
	private void initLeftUI()
	{
		mLinearLeft = (LinearLayout) this.findViewById(R.id.linear_left);
		mIvLeftIcon = (ImageView) this.findViewById(R.id.iv_left_icon);
	}
	
	// 初始化布局
	private void initViews()
	{
		// 初始化用户输入布局
		initStep1();
		// 初始化实时话费信息布局
		initStep2();
		// 初始化手机账单查询布局
		initStep3();
	}
	
	// 初始化用户输入布局
	private void initStep1() 
	{
		// 返回到业务办理菜单
		mLinearBacktoBusiAccept = (LinearLayout) findViewById(R.id.linear_backto_busiaccept);
		mLinearBacktoBusiAccept.setOnClickListener(this);
		
		mLinearGetRandom = (LinearLayout) findViewById(R.id.linear_get_random);
		mEditTel    = (EditText)this.findViewById(R.id.et_phone_no);
		mEditRandom = (EditText)this.findViewById(R.id.et_random);
		
		// 获取验证码
		mBtnGetRandom = (Button)this.findViewById(R.id.btn_get_random);
		mBtnGetRandom.setOnClickListener(this);
		// 获取验证码成功时给出的提示信息
		mTvGetRandomResult = (TextView) findViewById(R.id.tv_get_random_result);
		mTvGetRandomResult.setVisibility(View.GONE);
		// 校验验证码
		mBtnQuery = (MyCustomButton) this.findViewById(R.id.btn_query);
		// 倒计时
		mTvCountDown = (TextView) findViewById(R.id.tv_countdown);
		mTvCountDown.setVisibility(View.GONE);
		
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnQuery.setImageResource(R.drawable.btn_custom_query_orange);
		}
		else
		{
			mBtnQuery.setImageResource(R.drawable.btn_custom_query);
		}
		
		mBtnQuery.setTextViewText1("查询");
		mBtnQuery.setOnTouchListener(this);
		mBtnQuery.setOnClickListener(this);
		mBtnQuery.setGravity(Gravity.CENTER_HORIZONTAL);
	}
	
	// 初始化实时话费信息布局
	private void initStep2() 
	{
		mLinearBillDetail = (LinearLayout) findViewById(R.id.linear_bill_detail);
		
		for(int i = 0; i < mLinearLines.length; i++)
		{
			mLinearLines[i] = (LinearLayout) findViewById(mLinearLinesId[i]);
		}
		
		for (int i = 0; i < mTvTitles.length; i++) 
		{
			mTvTitles[i] = (TextView) findViewById(mTvTitlesId[i]);
		}
		
		for (int i = 0; i < mTvDetailsId.length; i++) 
		{
			mTvDetails[i] = (TextView) findViewById(mTvDetailsId[i]);
		}
		
		// 手机账单查询
		mIvDetailQuery = (ImageView) findViewById(R.id.iv_bill_detail_query);
		mIvDetailQuery.setOnClickListener(this);
		//mBtnDetailQuery = (MyCustomButton) findViewById(R.id.btn_bill_detail_query);
		//mBtnDetailQuery.setTextViewText1("");
		//mBtnDetailQuery.hideWidget(0);
		//mBtnDetailQuery.setOnClickListener(this);
		//mBtnDetailQuery.setOnTouchListener(this);
		// 返回到验证码获取布局
		mBtnDetailBack = (MyCustomButtonOrange)findViewById(R.id.btn_bill_detail_back);
		mBtnDetailBack.setTextViewText1("返回");
		mBtnDetailBack.setImageResource(R.drawable.btn_back);
		mBtnDetailBack.setOnClickListener(this);
		mBtnDetailBack.setOnTouchListener(this);
	}
	
	// 初始化手机账单查询布局
	private void initStep3() 
	{
		mLinearBillQuery = (RelativeLayout) findViewById(R.id.linear_bill_query);
		mTextBillQueryTel = (TextView) findViewById(R.id.tv_bill_query_tel);
		
		// 返回到实时话费信息布局
		mBtnBillQueryBack = (MyCustomButtonOrange) findViewById(R.id.btn_bill_query_back);
		mBtnBillQueryBack.setTextViewText2("");
		mBtnBillQueryBack.setTextViewText1("返回");
		mBtnBillQueryBack.setImageResource(R.drawable.btn_back);
		mBtnBillQueryBack.setOnClickListener(this);
		mBtnBillQueryBack.setOnTouchListener(this);
	}
	
	// 显示第几页
	private void showPage(int nPageIdx) 
	{
		if(nPageIdx == 0x01)
		{
			mLinearGetRandom.setVisibility(View.VISIBLE);
			mLinearBillDetail.setVisibility(View.GONE);
			mLinearBillQuery.setVisibility(View.GONE);
		}
		else if(nPageIdx == 0x02)
		{
			mLinearGetRandom.setVisibility(View.GONE);
			mLinearBillDetail.setVisibility(View.VISIBLE);
			mLinearBillQuery.setVisibility(View.GONE);
		}
		else if(nPageIdx == 0x03)
		{
			mLinearGetRandom.setVisibility(View.GONE);
			mLinearBillDetail.setVisibility(View.GONE);
			mLinearBillQuery.setVisibility(View.VISIBLE);
		}
	}
	
	// 刷新实时话费信息的布局
	private void refreshPage2() 
	{
		try
		{
			String[] 	lines = mBusinessInquiry.mStrCurMonthDetail.split("\\n");
			String[][] 	lineItems = new String[lines.length][2];
			
			// 手机号码
			mTvDetails[0].setText(mStrTelNum);
			// 其他信息
			for (int i = 0; i < lines.length; i++) 
			{
				if (lines[i].split("\\:").length == 1) 
				{
					lineItems[i] = lines[i].split("\\：");
				} 
				else 
				{
					lineItems[i] = lines[i].split("\\:");
				}
			}
			
			// 控件先全部隐藏
			for (int i = 1; i < mTvTitles.length; i++) 
			{
				mTvTitles[i].setVisibility(View.GONE);
				mLinearLines[i].setVisibility(View.GONE);
				if(i < mTvTitles.length - 1)
				{
					mTvDetails[i].setVisibility(View.GONE);
				}
			}
			
			// 显示实时话费信息
			// 手机号码信息
			for (int i = 0; i < lines.length; i++) 
			{
				if(lineItems[i].length == 2)
				{
					mLinearLines[i+1].setVisibility(View.VISIBLE);
					// 标题
					mTvTitles[i+1].setVisibility(View.VISIBLE);
					mTvTitles[i+1].setText(lineItems[i][0] + ": ");
					// 内容
					mTvDetails[i+1].setVisibility(View.VISIBLE);
					mTvDetails[i+1].setText(lineItems[i][1]);
				}
				else
				{
					mLinearLines[mLinearLines.length - 1].setVisibility(View.VISIBLE);
					// 标题
					mTvTitles[mTvTitles.length - 1].setVisibility(View.VISIBLE);
					mTvTitles[mTvTitles.length - 1].setText(lineItems[i][0]);
					return;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// 刷新手机账单查询布局
	private void refreshPage3() 
	{
		// 设置手机号码
		mTextBillQueryTel.setText(mStrTelNum);
		
		int nTotal = mBusinessInquiry.mListBillData.size();
		// 手机账单查询项编码
		mBillQueryCode = new int[nTotal];
		for(int i=0; i<nTotal; i++) 
		{
			mBillQueryCode[i] = Integer.parseInt((String)mBusinessInquiry.mListBillData.get(i).get(BusinessInquiry.INQUIRY_CODE));
		}
		// 手机账单查询项名称
		mStrBillQueryName = new String[nTotal];
		for(int i=0; i<nTotal; i++)
		{
			mStrBillQueryName[i] = (String)mBusinessInquiry.mListBillData.get(i).get(BusinessInquiry.INQUIRY_NAME);
		}
		
		// 初始化手机账单查询月份
		try
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mBillGroupUtil = ButtonGroupUtil.createBean(this, 
						mBtnBillIDs,
						mStrBillQueryName, 
						mStrBillQueryName, 
						R.drawable.btn_tip_select, 
						R.drawable.btn_tip_selected);
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				mBillGroupUtil = ButtonGroupUtil.createBean(this, 
						mBtnBillIDs,
						mStrBillQueryName, 
						mStrBillQueryName, 
						R.drawable.btn_tip_select_orange, 
						R.drawable.btn_tip_selected_orange);
			}
			mBillGroupUtil.setSelected(0);
			// 设置监听事件
			mBillGroupUtil.setClickListener(new ButtonGroupClickListener() 
			{
				@Override
				public void onClick(final int previousId, final int newId) 
				{
					for (int i = 0; i < mBtnBillIDs.length; i++) 
					{
						if (mBtnBillIDs[i] == newId) 
						{
							// 获取手机账单查询项详情
							getBillQueryDetail(mBillQueryCode[i], mStrBillQueryName[i]);
							break;
						}
					}
				}
			});
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// 获取手机账单查询项详情
	private void getBillQueryDetail(final int nInquiryCode, final String strInquiryName) 
	{
		new Thread()
		{
			public void run()
			{
				DialogUtil.showProgress("正在获取手机账单详情...");
				// 获取手机账单详情
				boolean result = mBusinessInquiry.bll010F(mStrTelNum, nInquiryCode);
				DialogUtil.closeProgress();
				// 获取成功
				if(result) 
				{
					runOnUiThread(new Runnable() 
					{
						@Override
						public void run() 
						{
							// 显示账单详情
							new BillDetailDialog(
									MobileBillActivity.this, 
									mBusinessInquiry.mStrMobileBillContent, 
									strInquiryName, 
									mStrTelNum
									).show();
						}
					});
				}
			 };
		}.start();
	}
	
	public void initData()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			int argbBlue = ARGBUtil.getArgb(1);
			int argbOrange = ARGBUtil.getArgb(2);
		    int argbBlue2 = ARGBUtil.getArgb(3);
		    
		    mLinearLeft.setBackgroundColor(argbOrange);
		    mIvLeftIcon.setImageResource(R.drawable.icon_mobilebill_query_orange);
			// 获取验证码按钮
			mBtnGetRandom.setBackgroundColor(argbBlue);
			// 号码
			mTvDetails[0].setTextColor(argbBlue2);
			mTextBillQueryTel.setTextColor(argbBlue2);     
			// 查询月账单按钮
			mIvDetailQuery.setImageResource(R.drawable.mobilebill_query_icon_orange);
		}
	}
	
	@Override
	public void onInitSuccess() 
	{
		
	}

	@Override
	public void onInitFail() 
	{
		
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	@Override
	public void onClick(View v) 
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
		{
			return;
		}
		
		switch(v.getId()) 
		{
		// 返回到业务受理
		case R.id.linear_backto_busiaccept:
			backToPrevious();
			break;
			
		// 获取验证码
		case R.id.btn_get_random:
			handleGetRandom();
			break;
			
		// 查询手机账单
		case R.id.btn_query:
			handleQuery();
			break;
			
		// 显示手机账单布局
		case R.id.iv_bill_detail_query:
			// 显示第3页
			showPage(0x03);
			// 刷新第3页
			refreshPage3();
			break;
			
		// 返回验证信息输入布局
		case R.id.btn_bill_detail_back:
			showPage(0x01);
			break;
			
		// 返回到实时信息输入布局
		case R.id.btn_bill_query_back:
			// 显示第2页
			showPage(0x02);
			break;
			
		default:
			break;
		}
	}
	
	// 获取验证码
	private void handleGetRandom() 
	{
		// 校验手机号码
		mStrTelNum = mEditTel.getText().toString();
		if(StringUtil.isEmptyOrNull(mStrTelNum)) 
		{
			back(null, BusinessInquiry.TITLE_STR, "请输入手机号码！", "确定", null, false, false, false);
			return;
		}
		if(mStrTelNum.length() != 11) 
		{
			back(null, BusinessInquiry.TITLE_STR, "请输入11位手机号码！", "确定", null, false, false, false);
			return;
		}
		
		// 获取验证码
		new Thread() 
		{
			public void run() 
			{
				DialogUtil.showProgress("正在获取验证码");
				if(mBusinessInquiry.bll010D(mStrTelNum)) 
				{
					DialogUtil.closeProgress();
					mIsGetRandomOK = true;
					DialogUtil.showToast("获取验证码成功！", 0x00);
					runOnUiThread(new Runnable()
					{
						@Override
						public void run()
						{
							mTvGetRandomResult.setVisibility(View.VISIBLE);
							mTvGetRandomResult.setText("获取成功，请在180秒内输入短信验证码！");
							mTvCountDown.setVisibility(View.VISIBLE);
							mCountDownTimer.start();
						}
					});
				}
			}
		}.start();
	}
	
	// 处理账单查询请求
	private void handleQuery() 
	{
		// 是否已经获取过验证码?
		if(mIsGetRandomOK == false) 
		{
			back(null, BusinessInquiry.TITLE_STR, "请先获取验证码！", "确定", null, false, false, false);
			return;
		}
		
		// 校验手机号码
		String strTelNum = mEditTel.getText().toString();
		if(StringUtil.isEmptyOrNull(strTelNum)) 
		{
			back(null, BusinessInquiry.TITLE_STR, "请输入手机号码！", "确定", null, false, false, false);
			return;
		}
		if(strTelNum.length() != 11) 
		{
			back(null, BusinessInquiry.TITLE_STR, "请输入11位手机号码！", "确定", null, false, false, false);
			return;
		}
		// 校验手机号码的一致性
		if(!mStrTelNum.equals(strTelNum)) 
		{
			back(null, BusinessInquiry.TITLE_STR, "检测到手机号码发生更改, 请重新获取验证码！", "确定", null, false, false, false);
			return;
		}
		
		// 校验随机码
		mStrRandom = mEditRandom.getText().toString();
		if(StringUtil.isEmptyOrNull(mStrRandom)) 
		{
			back(null, BusinessInquiry.TITLE_STR, "请输入验证码！", "确定", null, false, false, false);
			return;
		}
		if(mStrRandom.length() != 6) 
		{
			back(null, BusinessInquiry.TITLE_STR, "请输入6位验证码！", "确定", null, false, false, false);
			return;
		}
		
		clearParticalData();
		
		// 处理查询请求
		new Thread() 
		{
			public void run() 
			{
				// 校验验证码
				DialogUtil.showProgress("正在校验验证码");
				boolean result = mBusinessInquiry.bll010E(mStrTelNum, mStrRandom);
				
				// 校验成功
				if(result)
				{
					runOnUiThread(new Runnable() 
					{
						public void run() 
						{
							// 清除编辑框
							clearEdit();
							// 显示实时话费信息布局
							showPage(0x02);
							// 刷新信息
							refreshPage2();
						}
					});
				}
			}
		}.start();
	}
	
	// 清除编辑框
	private void clearEdit() 
	{
		runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				mEditTel.setText("");
				mEditRandom.setText("");
				// 再次查询需要重新获取验证码
				mIsGetRandomOK = false;
				mTvGetRandomResult.setVisibility(View.GONE);
			}
		});
	}
	
	// 清空部分编辑框数据
	private void clearParticalData()
	{
		mCountDownTimer.cancel();
		mTvCountDown.setVisibility(View.GONE);
		mTvGetRandomResult.setVisibility(View.GONE);
		mEditRandom.setText("");
	}
	
	public void backToPrevious()
	{
		back(new ICallBack() 
		{
			@Override
			public void back() 
			{
				mCountDownTimer.cancel();
				MobileBillActivity.this.finish();
			}
			
			@Override
			public void cancel() 
			{
			}
			
			@Override
			public void dismiss() 
			{
			}
		}, null, null, "确定", "取消", true, false, false);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			backToPrevious();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}

