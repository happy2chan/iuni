package com.sunnada.baseframe.activity.busiaccept;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.util.ButtonUtil;

public class BusiAcceptActivity extends BaseActivity implements OnClickListener
{
	private final int 			BUSI_PHONE_INQUIRY		= 0x01;
	private final int			BUSI_PHONE_REGISTER		= 0x02;
	private final int			BUSI_ADDVALUE			= 0x03;
	
	private int[]				mBusiResource 			= {	
															R.drawable.busi_phone_inquiry, 
															R.drawable.busi_phone_register,
															R.drawable.busi_add
														  };
	private int[]				mBusiResourceOrange 	= {	
															R.drawable.busi_phone_inquiry_orange, 
															R.drawable.busi_phone_register_orange,
															R.drawable.busi_add_orange
														  };
	private int[]				mBusiID					= {
															BUSI_PHONE_INQUIRY, 
															BUSI_PHONE_REGISTER, 
															BUSI_ADDVALUE
														};
	
	private int					mBusiType;				// 业务类型
	private LinearLayout 		mMenuBusiAccept			= null;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_busiaccept);
		
		// 初始化布局
		runOnUiThread(new Runnable() 
		{
			public void run() 
			{
				initViews();
			}
		});
	}
	
	// 初始化布局
	private void initViews()
	{
		mMenuBusiAccept = (LinearLayout) findViewById(R.id.menu_busiaccept);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			initSubLayout(mMenuBusiAccept, mBusiResourceOrange, mBusiID);
		}
		else
		{
			initSubLayout(mMenuBusiAccept, mBusiResource, mBusiID);
		}
		
		ImageView ivBackToHome = (ImageView) findViewById(R.id.iv_backto_mainmenu);
		ivBackToHome.setOnClickListener(this);
		
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			ivBackToHome.setImageDrawable(getResources().getDrawable(R.drawable.icon_busiaccept_orange));
		}
	}
	
	// 初始化子布局
	private void initSubLayout(LinearLayout parentLayout, int[] resID, int[] ctlID)
	{
		// 二级菜单布局(包含垂直分布的线性布局)
		LinearLayout busi = new LinearLayout(this);
		LayoutParams busiParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		busiParams.setMargins(0, 50, 0, 0);
		busi.setLayoutParams(busiParams);
		busi.setOrientation(LinearLayout.VERTICAL);
		// 
		int count = 0;
		LinearLayout sub_busi = null;
		for(int i=0; i<ctlID.length; i++)
		{
			if(!checkAuthority(ctlID[i])) 
			{
				// 是否已经结束
				if(i == ctlID.length-1)
				{
					if(sub_busi != null)
					{
						busi.addView(sub_busi);
					}
				}
				// 未获得菜单权限
				continue;
			}
			// 是否该换行?
			if(count%3 == 0)
			{
				if(count > 0)
				{
					busi.addView(sub_busi);
				}
				sub_busi = new LinearLayout(this);
				LayoutParams subBusiParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				subBusiParams.setMargins(10, 0, 0, 5);
				sub_busi.setLayoutParams(subBusiParams);
			}
			count ++;
			Button button = new Button(this);
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(5, 0, 5, 0);
			button.setLayoutParams(params);
			button.setId(ctlID[i]);
			button.setBackgroundDrawable(getResources().getDrawable(resID[i]));
			button.setOnClickListener(this);
			sub_busi.addView(button);
			// 是否已经结束
			if(i == ctlID.length-1)
			{
				busi.addView(sub_busi);
				break;
			}
		}
		parentLayout.addView(busi);
	}
	
	// 获取菜单权限
	private boolean checkAuthority(int id)
	{
		boolean result = false;
		switch (id)
		{
			// 手机消费查询
			case BUSI_PHONE_INQUIRY:
				result = getMenuAuthority("0710");
				Log.e("", "0710 :" + result);
				break;
				
			// 实名制返档
			case BUSI_PHONE_REGISTER:
				result = getMenuAuthority("0720");
				Log.e("", "0720 :" + result);
				break;
				
			// 增值业务受理
			case BUSI_ADDVALUE:
				result = getMenuAuthority("0730");
				Log.e("", "0730 :" + result);
				break;
				
			default:
				break;
		}
		return result;
	}
	
	@Override
	public void onInitSuccess() 
	{
		
	}

	@Override
	public void onInitFail() 
	{
		
	}
	
	@Override
	public void onDestroy() 
	{
		super.onDestroy();
		
		mMenuBusiAccept.removeAllViews();
		mMenuBusiAccept = null;
		// 
		System.gc();
		Log.e("", "业务办理类onDestroy()");
	}
	
	@Override
	public void onClick(View v) 
	{
		if(ButtonUtil.isFastDoubleClick(v.getId(), 1000))
		{
			return;
		}
		
		switch(v.getId()) 
		{
		// 回退主界面
		case R.id.iv_backto_mainmenu:
			FrameActivity.mHandler.obtainMessage(FrameActivity.FINISH).sendToTarget();
			return;
			
		// 手机账单查询
		case BUSI_PHONE_INQUIRY:
			mBusiType = BUSI_PHONE_INQUIRY;
			break;
			
		// 实名制返档
		case BUSI_PHONE_REGISTER:
			mBusiType = BUSI_PHONE_REGISTER;
			break;
			
		// 增值业务办理
		case BUSI_ADDVALUE:
			mBusiType = BUSI_ADDVALUE;
			break;
			
		default:
			mBusiType = BUSI_PHONE_INQUIRY;
			break;
		}
		startRotation(v);
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask() 
		{
			@Override
			public void run() 
			{
				synchronized (Statics.INIT_OK_LOCK) 
				{
					// 手机账单查询
					if(mBusiType == BUSI_PHONE_INQUIRY)
					{
						Intent intent = new Intent(BusiAcceptActivity.this, MobileBillActivity.class);
						startActivity(intent);
					}
					// 实名制返档
					else if(mBusiType == BUSI_PHONE_REGISTER)
					{
						Intent intent = new Intent(BusiAcceptActivity.this, BusiPhoneRegisterLogin.class);
						startActivity(intent);
					}
					// 增值业务办理
					else if(mBusiType == BUSI_ADDVALUE)
					{
						Intent intent = new Intent(BusiAcceptActivity.this, BusiAddValue.class);
						startActivity(intent);
					}
				}
			}
		}, 300);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			FrameActivity.mHandler.obtainMessage(FrameActivity.FINISH).sendToTarget();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
	}
}
