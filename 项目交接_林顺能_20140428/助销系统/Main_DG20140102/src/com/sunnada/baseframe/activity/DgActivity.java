package com.sunnada.baseframe.activity;

import android.os.Bundle;
import android.view.View;

import com.sunnada.baseframe.database.ErrorDealAdapter;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.Log;
import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.bean.ErrorInfoBean;
import com.sunnada.baseframe.business.Business_00;

public abstract class DgActivity extends BaseActivity
{
	private Business_00 		mBusiness00;
	private String[]			mStrErrType;			// 异常交易类型
	
	@Override
	public void onInitSuccess()
	{
		
	}

	@Override
	public void onInitFail()
	{
		
	}	
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		DialogUtil.init(this);
	}
	
	// 检测异常交易
	protected void checkErrorDeal(final String[] strErrType) 
	{
		new Thread() 
		{
			public void run() 
			{
				// 异常交易处理
				errorDealCheck(strErrType);
			}
		}.start();
	}
	
	// 异常交易处理
	// 返回值: true-有异常交易存在
	// false-无异常交易存在
	protected boolean errorDealCheck(String[] strErrType) 
	{
		DialogUtil.showProgress("正在检测异常交易...");
		mStrErrType = null;
		// 检测异常交易
		for(int i=0; i<strErrType.length; i++) 
		{
			final ErrorInfoBean[] errorInfo = ErrorDealAdapter.getErrorInfo(DgActivity.this, 
					equipmentService.getPsamInfo(0x00).getPsamID(), strErrType[i]);
			if (errorInfo != null) 
			{
				DialogUtil.showProgress("正在处理异常交易...");
				mStrErrType = strErrType;
				if(mBusiness00 == null) 
				{
					mBusiness00 = new Business_00(DgActivity.this, equipmentService, socketService, dataBaseService);
				}
				for(int j=0; j<errorInfo.length; j++) 
				{
					if(mBusiness00.bll0041(equipmentService.getPsamInfo(0x00).getPsamID(), strErrType[i], errorInfo[j]) == false) 
					{
						Log.e("", "异常交易处理失败");
						DialogUtil.closeProgress();
						DialogUtil.showMessageDialog("温馨提示", 
								mBusiness00.getLastknownError(), 
								"确定", 
								new View.OnClickListener() 
								{
									@Override
									public void onClick(View view) 
									{
										if(ButtonUtil.isFastDoubleClick(view.getId(), 500)) 
										{
											return;
										}
										errorDealCheckCallBack();
									}
								}, 
								null, 
								null);
						return true;
					}
					// 是否需要展示交易详情
					if(mBusiness00.mIsNeedConfirm) 
					{
						final int index1 = i;
						final int index2 = j;
						DialogUtil.closeProgress();
						DialogUtil.MsgBox("异常交易详情", 
								mBusiness00.mStrConfirmInfo, 
								"已收款", 
								new View.OnClickListener() 
								{
									@Override
									public void onClick(View v) 
									{
										reportPayStatus(mStrErrType[index1], errorInfo[index2], 0x01);
									}
								}, 
								"未收款", 
								new View.OnClickListener() 
								{
									@Override
									public void onClick(View v) 
									{
										reportPayStatus(mStrErrType[index1], errorInfo[index2], 0x00);
									}
								});
						return true;
					}
				}
			}
		}
		DialogUtil.closeProgress();
		if(mStrErrType != null) 
		{
			DialogUtil.MsgBox("温馨提示", 
					"异常信息上报成功！", 
					"确定", 
					new View.OnClickListener() 
					{
						@Override
						public void onClick(View view) 
						{
							if(ButtonUtil.isFastDoubleClick(view.getId(), 500)) 
							{
								return;
							}
							errorDealCheckCallBack();
						}
					}, 
					null,
					null);
			return true;
		}
		return false;
	}
	
	// 上报扣款结果
	private void reportPayStatus(final String strErrType, final ErrorInfoBean bean, final int nPayStatus) 
	{
		new Thread()
		{
			public void run() 
			{
				DialogUtil.showProgress("正在上报扣款结果...");
				if(mBusiness00.bll0042(equipmentService.getPsamInfo(0x00).getPsamID(), strErrType, bean, nPayStatus) == false)  
				{
					DialogUtil.closeProgress();
					DialogUtil.MsgBox("温馨提示", mBusiness00.getLastknownError());
				}
				else
				{
					DialogUtil.closeProgress();
					DialogUtil.MsgBox("温馨提示", 
							"上报扣款结果成功！", 
							"确定", 
							new View.OnClickListener() 
							{
								@Override
								public void onClick(View v) 
								{
									checkErrorDeal(mStrErrType);
								}
							}, 
							null, 
							null, 
							new View.OnClickListener() 
							{
								@Override
								public void onClick(View v) 
								{
									checkErrorDeal(mStrErrType);
								}
							});
				}
			}
		}.start();
	}
	
	// 异常交易处理完成的回调
	protected void errorDealCheckCallBack() 
	{
		
	}
}
