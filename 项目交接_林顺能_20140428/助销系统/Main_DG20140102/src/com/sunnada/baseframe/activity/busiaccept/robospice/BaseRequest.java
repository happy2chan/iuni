package com.sunnada.baseframe.activity.busiaccept.robospice;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;
import android.util.Xml;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;
import com.sunnada.baseframe.activity.busiaccept.base.DeviceUtils;

public abstract class BaseRequest extends GoogleHttpClientSpiceRequest<String> 
{
	//private String 					mBaseUrl = "http://123.125.96.6:8090";			// 现网环境
	//private String 					mBaseUrl = "http://211.94.67.69:8012";			// 北京测试环境
	//private String 					mBaseUrl = "http://211.94.67.69:12405/WEB/";
	private String						mBaseUrl = "http://220.249.190.132:13515";		// 福州测试环境
	
	public static final int				CONNECT_TIMEOUT		= 15*1000;
	public static final int				READ_TIMEOUT		= 30*1000;
	public static final int				PROGRESS_TIMEOUT	= 45*1000;
	public static final int				HTTP_READ_TIMEOUT	= 15*1000;
	 
	public static final String			CLIENT_TYPE 		= "clientType";
	public static final String			COMMUNICATION_ID 	= "communicaID";
	public static final String			AGENT_ID 			= "agentId";
	public static final String			AGENT_TEL			= "agentTelphone";
	public static final String			AGENT_PASSWORD		= "agentPasswd";
	public static final String			IDENTIFY_ID 		= "identifyId";
	public static final String			OLD_PASSWORD		= "oldPasswd";
	public static final String			NEW_PASSWORD		= "newPasswd";
	public static final String			SEND_SMS_FLAG		= "sendSMSflag";
	public static final String			VERSION_CODE		= "versionCode";
	public static final String			VERSION_NAME		= "versionName";
	public static final String			LAC					= "lac";
	public static final String			CI					= "ci";
	
	public static final String			BEGIN_TIME			= "beginTime";
	public static final String			END_TIME			= "endTime";
	public static final String			QUERY_START			= "queryStart";
	public static final String			QUERY_COUNT			= "queryAll";
	
	public static final String			USER_TYPE			= "certificateType";
	public static final String			USER_NAME			= "certificateName";
	public static final String			USER_NO				= "certificateNum";
	public static final String			USER_TEL			= "telplone";
	public static final String			USER_ADDR			= "certificateAdd";
	
	public static final String			CONTACT_NAME		= "relationName";
	public static final String			CONTACT_TEL			= "relationTel";
	public static final String			CONTACT_ADDR		= "relationAddr";
	
	public static final String			ICCID_NUMBER		= "iccidnumber";
	public static final String			TEL_LAST4			= "telphoneTail";
	public static final String			FD_TYPE				= "fdType";
	public static final String			CHANNEL_ID			= "channelID";
	
	public static final String			TRADE_STATE			= "tradeState";
	public static final String			TRADE_STATE_OK		= "0000";
	public static final String			DESCRIPTION			= "description";
	public static final String			REGISTER_KEY		= "registerKey";
	public static final String			UPLOAD_TYPE			= "uploadType";
	public static final String			CERTIFY_FLAG		= "certifyFlag";
	
	public static final String			NOTICE_FLAG			= "noticeFlag";
	public static final String			NOTICE_TITLE		= "noticeTitle";
	public static final String			NOTICE_MSG			= "noticeMsg";
	
	public static final String			MENU_FLAG			= "menuFlag";
	
	protected static final String		mClientType 		= "01";
	protected static final String		mChannelID			= "0001";
	private Map<String, String> 		mResult 			= new HashMap<String, String>();
	protected static final String 		COMMUNICA_ID 		= "FFFF";
	public static String 				mEncryptKey 		= "sunnada0";
	
	public BaseRequest() 
	{
		super(String.class);
	}

	protected abstract String getMethod();

	protected abstract String getResponseMethod();

	protected abstract List<Param> getParams();

	private void parserResult(String content) 
	{
		XmlPullParser xmlParser = Xml.newPullParser();
		InputStream is = new ByteArrayInputStream(content.getBytes());
		try {
			xmlParser.setInput(is, "utf-8");
			int evtType = xmlParser.getEventType();
			boolean isBody = false;
			while (evtType != XmlPullParser.END_DOCUMENT) 
			{
				switch (evtType) 
				{
				case XmlPullParser.START_TAG:
					String tag = xmlParser.getName();
					//Log.d("", "Tag-->" + tag);
					if (isBody) 
					{
						String val = xmlParser.nextText();
						//Log.d("", "val:" + val);
						mResult.put(tag, val);
					}
					if (tag.equals(getResponseMethod())) 
					{
						isBody = true;
					}
					break;
				case XmlPullParser.END_TAG:
					tag = xmlParser.getName();
					if (tag.equals(getResponseMethod())) 
					{
						//Log.d("", "解析完毕");
						return;
					}
					break;
				default:
					break;
				}
				evtType = xmlParser.next();
			}
		} 
		catch (XmlPullParserException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			if (is != null) 
			{
				try {
					is.close();
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		}
	}

	public Map<String, String> getResult() 
	{
		return mResult;
	}

	private static List<Param> getCommonParams() 
	{
		List<Param> params = new ArrayList<Param>();
		String id = DeviceUtils.getDeviceId();
		if (id == null) 
		{
			id = "";
		}
		if (id.length() < 16) 
		{
			int len = 16 - id.length();
			for (int i = 0; i < len; i++) 
			{
				id += "0";
			}
		}
		params.add(new Param("deviceID", id));
		return params;
	}
	
	private String getRequestContent() 
	{
		StringBuffer sb = new StringBuffer();
		sb.append("<SOAP-ENV:Envelope ")
				//.append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"")
				//.append("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"")
				//.append("xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"")
				//.append("xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"")
				.append("xmlns:ns=\"urn:SmsWBS\">")
				.append("<SOAP-ENV:Body>")
				.append("<ns:").append(getMethod()).append(">");
		
		List<Param> params = getCommonParams();
		params.addAll(getParams());

		for (Param param:params) 
		{
			sb.append(param.toString());
		}
		
		sb.append("</ns:").append(getMethod()).append(">").append("</SOAP-ENV:Body>").append("</SOAP-ENV:Envelope>");
		Log.e("Send", sb.toString());
		return sb.toString();
	}
	
	@Override
	public String loadDataFromNetwork() throws Exception 
	{
		final String content = getRequestContent();
		Log.d("", "request:"+content);
		HttpRequest request = getHttpRequestFactory().buildPostRequest(new GenericUrl(mBaseUrl), null);
		request.setConnectTimeout(CONNECT_TIMEOUT);
		request.setReadTimeout(READ_TIMEOUT);
		request.setContent(new HttpContent() 
		{
			@Override
			public void writeTo(OutputStream os) throws IOException 
			{
				os.write(content.getBytes());
				Log.e("", "********************");
				Log.e("", "客户端发送请求完成...");
				Log.e("", "********************");
			}
			@Override
			public boolean retrySupported() 
			{
				return true;
			}
			@Override
			public String getType() 
			{
				return null;
			}
			@Override
			public long getLength() throws IOException 
			{
				ByteArrayInputStream is = new ByteArrayInputStream(content.getBytes());
				long lenght = is.available();
				is.close();
				return lenght;
			}
		});
		String result = request.execute().parseAsString();
		Log.e("Recv", result);
		
		parserResult(result);
		Log.d("", "result:" + result);
		return result;
	}
}
