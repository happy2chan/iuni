package com.sunnada.baseframe.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences.Editor;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.sunnada.baseframe.activity.busiaccept.base.App;
import com.sunnada.baseframe.bean.ConstantData;
import com.sunnada.baseframe.bean.ReturnEnum;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_00;
import com.sunnada.baseframe.business.ImgLogReport;
import com.sunnada.baseframe.database.PhoneParamsDao;
import com.sunnada.baseframe.database.ProductInfoDao;
import com.sunnada.baseframe.database.RecommendPackageDao;
import com.sunnada.baseframe.database.SystemInfoDao;
import com.sunnada.baseframe.dialog.ChangeSkinDialog;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.ModifyIPDialog;
import com.sunnada.baseframe.service.IDataBaseService;
import com.sunnada.baseframe.service.IEquipmentService;
import com.sunnada.baseframe.service.ISocketService;
import com.sunnada.baseframe.thread.Thread;
import com.sunnada.baseframe.update.CheckVersionTask;
import com.sunnada.baseframe.util.BitmapPoolUtil;
import com.sunnada.baseframe.util.CollectionUtil;
import com.sunnada.baseframe.util.FileOpr;
import com.sunnada.baseframe.util.HttpURLConnectionUtil;
import com.sunnada.baseframe.util.StringUtil;
import com.sunnada.bluetooth.BluetoothListener;

public class LaunchActivity extends DgActivity implements OnClickListener, BluetoothListener
{
	private final String		TITLE_STR			= "温馨提示";
	public static String 		mStrServerIP		= "220.249.190.132";
	public static int			mServerPort			= 9009;
	//private final String    	mStrServerIP      	= "59.56.182.92";
	//private final int         mServerPort       	= 14008;
	
	private LinearLayout		mLinearBlueTips		= null;			// 点击启用蓝牙外设的布局
	//private ImageView			mImageScanBlueList	= null;			// 启用蓝牙外设
	private LinearLayout		mLinearBlueList		= null;			// 蓝牙设备列表的布局
	private ImageView 			mIvBlueStatus 		= null;			// 蓝牙连接图标
	private LinearLayout 		mLayMove 			= null;
	private ImageView 			mImageBtListBottom 	= null;			// 蓝牙设备列表下面的箭头
	private ListView 			mListBlueDevices 	= null;			// 蓝牙设备的列表
	private LinearLayout 		mLayNoDevices	 	= null;			// 无可用蓝牙设备
	//private ImageView			mIvBlueStatus		= null;
	private RelativeLayout 		mLayBtList 			= null;
	private RelativeLayout 		mLayCover 			= null;
	private ProgressBar 		pb_linking 			= null;
	
	private Animation 			anm_lay_move;
	private Animation 			anm_iv_btbottom;
	private Animation 			anm_lay_btlist;
	
	private List<Map<String, String>> mDeviceList 	= null;
	
	private boolean 			mIsBtConnecting 	= false;
    private EditText			mEditNo				= null;
	private EditText 			mEditPwd 			= null;
	// 继承基础类
	private Business_00 		mBusiness00 		= null;
	private PopupWindow			mPopVersionInfo;
	private TextView			mTvVersionInfo;						// 版本信息中的版本号
	private CheckVersionTask	mCheckVersion		= null;
	private ChangeSkinDialog	mDlgChangeSkin 		= null;
	
	private Handler	mHandler = new Handler()
	{
		public void handleMessage(Message message)
		{
			switch(message.what)
			{
			// 暗码处理
			case Statics.MSG_SECRET_CODE:
				ModifyIPDialog dialog = new ModifyIPDialog(LaunchActivity.this, socketService);
				dialog.setTitle("接入环境变更设置");
				dialog.setPositiveListener("确定", LaunchActivity.this);
				dialog.setNegetiveListener("取消", null);
				dialog.show();
				break;
				
			// 升级
			case Statics.MSG_UPDATE:
				// 下载
				new Thread() 
				{
					public void runs() 
					{
						DialogUtil.showProgress("正在下载升级文件...");
						mCheckVersion.checkBreakDownLoad(Statics.PATH_APK+"/Main_DG.apk");
					}
				}.start();
				break;
				
			// 用户取消升级
			case Statics.MSG_UPDATE_CANCEL:
				if(Statics.IS_FORCED_UPDATE)
				{
					// 取消强制升级
					finish();
				}
				else
				{
					// 跳转到主界面
					jump2DgMain();
				}
				break;
				
			// 正在升级
			case Statics.MSG_UPDATING:
				try
				{
					DialogUtil.showProgress(String.format("下载中(%d/%d KB)", 
							mCheckVersion.getFileCurrentLength()/1024, mCheckVersion.getFileTotalLength()/1024));
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				break;
			
			// 升级失败
			case Statics.MSG_UPDATE_FAILED:
				DialogUtil.closeProgress();
				DialogUtil.MsgBox("软件升级提示", mCheckVersion.getLastErrorStr(), 
						"确定", Statics.MSG_INSTALL_FAILED, "", -1, Statics.MSG_INSTALL_FAILED, mHandler);
				break;
			
			// 升级成功
			case Statics.MSG_UPDATE_OK:
				DialogUtil.closeProgress();
				// 跳转到APK安装界面
				CheckVersionTask.installApkRequestCode(
						new File(Statics.PATH_APK+"/Main_DG.apk"), 
						LaunchActivity.this,
						Statics.INSTALL_APK_REQUEST);
				break;
				
			// 软件安装失败
			case Statics.MSG_INSTALL_FAILED:
				if(Statics.IS_FORCED_UPDATE)
				{
					finish();
				}
				else
				{
					// 跳转到主界面
					jump2DgMain();
				}
				break;
				
			default:
				break;
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// 启动服务
		serviceStart();
		super.onCreate(savedInstanceState);
		
		/*
		 Statics.SKIN_TYPE = getSharedPreferences(Statics.CONFIG, Context.MODE_PRIVATE)
	    .getInt(Statics.STR_SKIN_TYPE, Statics.SKIN_BLUE);
	    */
		
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_login);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_login_orange);
		}
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		DialogUtil.init(this);
		DialogUtil.showToast("屏幕分辨率为: " + dm.widthPixels + " * " + dm.heightPixels, 0x00);
		// 初始化
		init();
	}
	
	// 初始化
	private void init()
	{
		BitmapPoolUtil.init();
		// 显示版本
		setVersion();
		// 注册监听事件
		initViews();
		// 蓝牙相关初始化
		initBluetooth();
		// 初始化动画
		initAnimation();
		// 文件系统系统初始化
		initFileSystem();
	}

	// 显示版本
	private void setVersion() 
	{ 
		try 
		{
			TextView textVersion = (TextView) findViewById(R.id.tv_version);
			textVersion.setText("版本号: " + getVersionName()  + " >");
			textVersion.setOnClickListener(this);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	// 获取版本信息
	public String getVersionName() 
	{
		try
		{
			PackageManager pm = getPackageManager();
			PackageInfo pi = pm.getPackageInfo(getPackageName(), 0);
			// 
			String versionName = pi.versionName;
			if (StringUtil.isEmptyOrNull(versionName)) 
			{
				versionName = "0.0.0";
			}
			return versionName;
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return "0.0.0";
		}
	}
	
	private int  mOldSkinType = Statics.SKIN_TYPE;
	// 初始化控件
	private void initViews() 
	{
		Button btnChangeSkin = (Button) findViewById(R.id.btn_change_skin);
		btnChangeSkin.setOnClickListener(this);
		mDlgChangeSkin = new ChangeSkinDialog(this);
		mDlgChangeSkin.setOnDismissListener(new OnDismissListener()
		{
			@Override
			public void onDismiss(DialogInterface dialog)
			{
				if(Statics.SKIN_TYPE != mOldSkinType)
				{
					mOldSkinType = Statics.SKIN_TYPE;
					if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
					{
						setContentView(R.layout.lay_login);
						init();
						mEditNo.setText(App.getUserNo());
					}
					else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
					{
						setContentView(R.layout.lay_login_orange);
						init();
						mEditNo.setText(App.getUserNo());
					}
				}
			}
		});
		
		View contentView = null;
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			contentView  = LayoutInflater.from(this).inflate(R.layout.pop_version_info, null);
		}
		else
		{
			contentView = LayoutInflater.from(this).inflate(R.layout.pop_version_info_orange, null);
		}
		contentView.setOnClickListener(this);
		mTvVersionInfo = (TextView) contentView.findViewById(R.id.tvVersion);
		
		mPopVersionInfo = new PopupWindow(contentView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		mPopVersionInfo.setFocusable(true);
		mPopVersionInfo.setTouchable(true);
		mPopVersionInfo.setAnimationStyle(R.anim.bottom_in);
		mPopVersionInfo.setBackgroundDrawable(new BitmapDrawable());
		
		// 蓝牙助销外设启用的布局
		mLinearBlueTips	= (LinearLayout) findViewById(R.id.linearLayout_bttips);
		ImageView ivScanBlue = (ImageView) findViewById(R.id.iv_scan_bluelist);
		ivScanBlue.setOnClickListener(this);
		// 蓝牙设备列表的布局
		mLinearBlueList		= (LinearLayout) findViewById(R.id.linearLayout_btlist);
		mLayMove 			= (LinearLayout) findViewById(R.id.lay_move);
		mImageBtListBottom 	= (ImageView) findViewById(R.id.iv_btlist_bottom);
		mLayBtList 			= (RelativeLayout) findViewById(R.id.lay_btlist);
		mLayCover 			= (RelativeLayout) findViewById(R.id.lay_cover);
		pb_linking 			= (ProgressBar) findViewById(R.id.pb_linking);
		// 查看蓝牙列表
		mIvBlueStatus = (ImageView) findViewById(R.id.iv_icon_bt);
		mIvBlueStatus.setOnClickListener(this);
		// 如果已有蓝牙设备连接
		if(!CollectionUtil.isEmptyOrNull(Statics.mCurMap) && Statics.mCurMap.get(Statics.BLUETOOTH_STATUS).equals("true")) 
		{
			mLinearBlueTips.setVisibility(View.GONE);
			mLinearBlueList.setVisibility(View.VISIBLE);
			// 设置蓝牙状态图标已连接
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				mIvBlueStatus.setImageResource(R.drawable.icon_bt_connected);
			}
			else
			{
				mIvBlueStatus.setImageResource(R.drawable.icon_bt_connected_orange);
			}
		}
		else
		{
			mLinearBlueTips.setVisibility(View.VISIBLE);
			mLinearBlueList.setVisibility(View.GONE);
		}
		
		// 登录按钮
		Button btnLogin = (Button) findViewById(R.id.btn_login);
		btnLogin.setOnClickListener(this);
		// 工号和密码
		final LinearLayout layNo = (LinearLayout) findViewById(R.id.layUserNo);
		final LinearLayout layPwd = (LinearLayout) findViewById(R.id.layPwd);
		mEditNo	 = (EditText) findViewById(R.id.et_no);
		mEditPwd = (EditText) findViewById(R.id.et_pwd);
		// 设置编辑框点击后布局的背景
		mEditNo.setOnFocusChangeListener(new OnFocusChangeListener() 
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if(hasFocus)
				{
					if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
					{
						layNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_login_pwd_focused));
						layPwd.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_login_pwd));
					}
					else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
					{
						layNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_login_pwd_focused_orange));
						layPwd.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_login_pwd));
					}
				}
			}
		});
		mEditPwd.setOnFocusChangeListener(new OnFocusChangeListener() 
		{
			@Override
			public void onFocusChange(View v, boolean hasFocus)
			{
				if(hasFocus)
				{
					if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
					{
						layPwd.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_login_pwd_focused));
						layNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_login_pwd));
					}
					else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
					{
						layPwd.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_login_pwd_focused_orange));
						layNo.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_login_pwd));
					}
					
				}
			}
		});
	}
	
	// 蓝牙设备找到
	@Override
	public void onBluetoothDeviceFound(BluetoothDevice device) 
	{
		for (Map<String, String> map:mDeviceList) 
		{
			if (map.get(Statics.BLUETOOTH_MAC).equals(device.getAddress())) 
			{
				return;
			}
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put(Statics.BLUETOOTH_NAME, device.getName());
		map.put(Statics.BLUETOOTH_MAC, device.getAddress());
		map.put(Statics.BLUETOOTH_STATUS, "false");
		mDeviceList.add(map);
		mListBlueDevices.setVisibility(View.VISIBLE);
		mLayNoDevices.setVisibility(View.GONE);
		refreshList();	
	}
	
	// 蓝牙搜索完成
	@Override
	public void onBluetoothDiscoveryFinished() 
	{
		Log.e("blue", "onBluetoothDiscoveryFinished");
		mBluetoothClient.cancelDiscovery();
		if(CollectionUtil.isEmptyOrNull(mDeviceList))
		{
			mListBlueDevices.setVisibility(View.GONE);
			mLayNoDevices.setVisibility(View.VISIBLE);
		}
	}
	
	// 蓝牙相关初始化
	private void initBluetooth() 
	{
		final ImageView iv_blue_tip = (ImageView) findViewById(R.id.iv_blue_tip);
		new Timer().schedule(new TimerTask()
		{
			@Override
			public void run() 
			{
				if(iv_blue_tip.isShown())
				{
					iv_blue_tip.post(new Runnable()
					{
						public void run()
						{
							iv_blue_tip.setVisibility(View.GONE);
						}
					});
				}
			}
		}, 10000);
		mDeviceList = new ArrayList<Map<String, String>>();
		mLayNoDevices = (LinearLayout) findViewById(R.id.lay_no_devices);
		// 蓝牙设备列表
		mListBlueDevices = (ListView) findViewById(R.id.list_bluetooth_devices);
		mListBlueDevices.setDivider(null);
		mListBlueDevices.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) 
			{
				mListBlueDevices.setSelected(true);
				mBluetoothClient.cancelDiscovery();
				mIsBtConnecting = true;
				// 显示正在连接的布局
				showConnecting(view);
				// 连接蓝牙设备
				final Map<String, String> map = mDeviceList.get(position);
				// 开始连接蓝牙
				startConnect(map);
			}
		});
	}
	
	// 显示正在连接的布局
	private void showConnecting(View view)
	{
		mLayCover.setVisibility(View.VISIBLE);
		LayoutParams params = mLayCover.getLayoutParams();
		params.height = mListBlueDevices.getMeasuredHeight();
		mLayCover.setLayoutParams(params);
		// 
		int[] location = new int[2];
		view.getLocationOnScreen(location);
		int viewY = location[1]; 
		// 
		mListBlueDevices.getLocationOnScreen(location);
		int coverY = location[1];
		// 
		MarginLayoutParams params2 = (MarginLayoutParams) pb_linking.getLayoutParams();
		params2.topMargin = viewY - coverY + (64-49)/2;
		pb_linking.setLayoutParams(params2);
	}
	
	// 开始连接蓝牙
	private void startConnect(final Map<String, String> map)
	{
		new Thread() 
		{
			@Override
			public void runs() 
			{
				try 
				{
					boolean flag = false;
					mBluetoothClient.setDevice(map.get("mac"));
					equipmentService.setBluetoothClient(mBluetoothClient);
					if (!CollectionUtil.isEmptyOrNull(Statics.mCurMap) && Statics.mCurMap.get(Statics.BLUETOOTH_STATUS).equals("true"))  
					{
						// 资源卸载
						equipmentService.releaseEquiment();
						Statics.mCurMap.put(Statics.BLUETOOTH_STATUS, "false");
						// 初始化设备
						flag = equipmentService.initEquiment();
					} 
					else 
					{
						flag = equipmentService.initEquiment();
					}
					final boolean temp = flag;
					runOnUiThread(new Runnable() 
					{
						public void run() 
						{
							mLayCover.setVisibility(View.GONE);
							if (temp) 
							{
								map.put(Statics.BLUETOOTH_STATUS, "true");
								Statics.mCurMap = map;
								// 刷新蓝牙设备列表
								refreshList();
								DialogUtil.MsgBox(TITLE_STR, "蓝牙连接成功！");
								// 
								closeList();
								if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
								{
									mIvBlueStatus.setImageResource(R.drawable.icon_bt_connected);
								}
								else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
								{
									mIvBlueStatus.setImageResource(R.drawable.icon_bt_connected_orange);
								}
								//mImageScanBlueList.setBackgroundResource(R.drawable.icon_bt_connected);
							}
							else 
							{
								// 刷新蓝牙设备列表
								refreshList();
								DialogUtil.MsgBox(TITLE_STR, "蓝牙连接失败！");
								// 
								if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
								{
									mIvBlueStatus.setImageResource(R.drawable.icon_bt);
								}
								else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
								{
									mIvBlueStatus.setImageResource(R.drawable.icon_bt_orange);
								}
								
								//mImageScanBlueList.setBackgroundResource(R.drawable.icon_bt);
							}
						}
					});
					mIsBtConnecting = false;
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
		}.start();
	}
	
	// 初始化动画
	private void initAnimation() 
	{
		anm_iv_btbottom = AnimationUtils.loadAnimation(this, R.anim.bottom_in);
		anm_iv_btbottom.setStartOffset(100);
		// 
		anm_lay_btlist = AnimationUtils.loadAnimation(this, R.anim.bottom_in);
		anm_lay_btlist.setStartOffset(200);
	}
	
	// 文件系统系统初始化
	private void initFileSystem()
	{
		// 图片
		File file = new File(Statics.PATH_IMAGE);
		if(file.exists() == false)
		{
			file.mkdirs();
		}
		// APK
		file = new File(Statics.PATH_APK);
		if(file.exists() == false)
		{
			file.mkdirs();
		}
		// M3
		file = new File(Statics.PATH_M3);
		if(file.exists() == false)
		{
			file.mkdirs();
		}
		// 配置文件
		file = new File(Statics.PATH_CONFIG);
		if(file.exists() == false)
		{
			file.mkdirs();
		}
	}
	
	// 关闭动画
	private void closeAnimation() 
	{
		if (anm_iv_btbottom != null) 
		{
			anm_iv_btbottom.cancel();
		}
		if (anm_lay_btlist != null) 
		{
			anm_lay_btlist.cancel();
		}
		if (anm_lay_move != null) 
		{
			anm_lay_move.cancel();
		}
	}
	
	@Override
	public void onInitSuccess() 
	{
		Log.i("", "onInitSuccess");
		mBusiness00 = new Business_00(this, equipmentService, socketService, dataBaseService);
		
		//keyValueDao = new KeyValueDao(dataBaseService);
		//String serverPath = keyValueDao.get(KeyValueDao.KEY_UPDATE_URL);
		//ConstantData.setServerPath(serverPath);
		try
		{
			// 初始化本地数据库
			initDB();
			equipmentService.setDeviceType("blue");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		/*
		// 在后台直接开始进行硬件和网络初始化
		new Thread() 
		{
			public void runs() 
			{
				// 如果硬件和网络初始化完成
				Looper.prepare();
				synchronized (initOkLock) 
				{
					isInitOk = initEquipment();
				}
				Looper.loop();
			}
		}.start();
		*/
		
		// 启动图片上传线程
		//FTPThread thread = new FTPThread(this, equipmentService, socketService, dataBaseService);
		//thread.start();
	}

	@Override
	public void onInitFail() 
	{

	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
		App.init(this);
		// 设置工号
		mEditNo.setText(App.getUserNo());
		// 20131120 光标定位到密码输入框
		if(!StringUtil.isEmptyOrNull(App.getUserNo()))
		{
			mEditPwd.requestFocus();
		}
	}
	
	@Override
	public void onDestroy() 
	{
		// 调用基类的onDestroy
		super.onDestroy();
		// 删除用户身份证图片
		String path = FileOpr.getSdcardPath() + getString(R.string.identify_photo_path);
		deleteUserIdPhoto(path);
		//扫描整个SD卡上的多媒体文件 ,清除系统图库中缓存图片
		Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED);
		File file = Environment.getExternalStorageDirectory();  
		intent.setData(Uri.fromFile(file));
		sendBroadcast(intent);
		
		Log.e("", "登录onDestroy()");
		// 停止服务
		//serviceStop();
		// 手动调用垃圾回收器
		System.gc();
	}
	
	// 删除用户身份证图片 20131205
	private void deleteUserIdPhoto(String path)
	{
		File file = new File(path);
		try 
		{  
		   if (!file.isDirectory()) 
		   {  
			   file.delete();  
		   } 
		   else if (file.isDirectory()) 
		   {  
			    String[] filelist = file.list();  
			    for (int i = 0; i < filelist.length; i++) 
			    {  
			    	 File delfile = new File(path + "/" + filelist[i]);  
				     if (!delfile.isDirectory()) 
				     {  
				    	 delfile.delete();  
				    	 System.out.println(delfile.getAbsolutePath() + "删除文件成功");  
				     } 
				     else if (delfile.isDirectory()) 
				     {  
				    	 deleteUserIdPhoto(path + "/" + filelist[i]);  
				     }  
			    }  
			    System.out.println(file.getAbsolutePath()+"删除成功");  
			    file.delete();  
		   }  
		} 
		catch (Exception e)
		{  
			e.printStackTrace();
		}  
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Statics.INSTALL_APK_REQUEST)
		{
			if(resultCode == RESULT_CANCELED)
			{
				DialogUtil.MsgBox("温馨提示", "软件安装失败，请进行手动安装！\nAPK路径：SD卡/sunnada/apks/Main_DG.apk", 
						"确定", Statics.MSG_INSTALL_FAILED, "", -1, Statics.MSG_INSTALL_FAILED, mHandler);
			}
		}
	}
	
	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
		case R.id.btn_change_skin:
			/*
			if(mDlgChangeSkin == null)
			{
				mDlgChangeSkin = new ChangeSkinDialog(this);
			}
			mDlgChangeSkin.show();
			*/
			break;
			
		// 查看蓝牙列表
		case R.id.iv_scan_bluelist:
		case R.id.iv_icon_bt:
			if(!mBluetoothClient.getBluetoothEnabled())
			{
				DialogUtil.showMessage("当前蓝牙未开启，请先开启蓝牙！");
				return;
			}
			procBluetooth();
			break;
			
		case R.id.tv_version:
			if(mPopVersionInfo.isShowing())
			{
				closePopWindow(mPopVersionInfo);
			}
			else
			{
				mPopVersionInfo.showAtLocation(v, Gravity.BOTTOM|Gravity.RIGHT, 0, 0);
				mTvVersionInfo.setText(getVersionName());
			}
			break;
			
		// 登录
		case R.id.btn_login:
			doLogin();
			break;
			
		// 版权信息
		case R.id.layPopVersionInfo:
			closePopWindow(mPopVersionInfo);
			break;
			
		default:
			break;
		}
	}
	
	// 关闭弹出框
	private void closePopWindow(PopupWindow popWindow)
	{
		if (popWindow != null && popWindow.isShowing())
		{
			popWindow.dismiss();
		}
	}
	
	// 处理蓝牙
	private void procBluetooth() 
	{
		if (mIsBtConnecting) 
		{
			return;
		}
		// 关闭动画
		closeAnimation();
		// 
		if (mLayMove.getVisibility() == View.GONE) 
		{
			// 打开蓝牙列表
			openList();
		} 
		else 
		{
			// 关闭蓝牙列表
			closeList();
		}
	}
	
	// 打开蓝牙列表
	private void openList() 
	{
		// 隐藏助销外设启用的布局
		mLinearBlueTips.setVisibility(View.GONE);
		// 启用蓝牙设备列表的布局
		mLinearBlueList.setVisibility(View.VISIBLE);
		// 
		anm_lay_move = AnimationUtils.loadAnimation(this, R.anim.left_in);
		anm_lay_move.setFillAfter(true);
		mLayMove.setVisibility(View.VISIBLE);
		mLayMove.startAnimation(anm_lay_move);
		
		mImageBtListBottom.setVisibility(View.VISIBLE);
		mImageBtListBottom.startAnimation(anm_iv_btbottom);
		
		mLayBtList.setVisibility(View.VISIBLE);
		mListBlueDevices.setVisibility(View.VISIBLE);
		mLayBtList.startAnimation(anm_lay_btlist);
		// 开始搜寻蓝牙设备
		mBluetoothClient.startDiscovery(LaunchActivity.this, this);
		if(!CollectionUtil.isEmptyOrNull(Statics.mCurMap) && Statics.mCurMap.get(Statics.BLUETOOTH_STATUS).equals("true")) 
		{
			mDeviceList.add(Statics.mCurMap);
			// 刷新蓝牙设备列表
			refreshList();
		}
	}
	
	// 关闭列表
	private void closeList() 
	{
		anm_lay_move = AnimationUtils.loadAnimation(this, R.anim.left_out);
		anm_lay_move.setFillAfter(true);
		anm_lay_move.setStartOffset(300);
		mLayMove.setVisibility(View.GONE);
		mLayMove.startAnimation(anm_lay_move);
		
		mImageBtListBottom.setVisibility(View.GONE);
		mLayBtList.setVisibility(View.GONE);
		mListBlueDevices.setVisibility(View.GONE);
		mLayCover.setVisibility(View.GONE);
		
		// 停止搜寻蓝牙设备
		mBluetoothClient.cancelDiscovery();
		mDeviceList.clear();
		refreshList();
		mIsBtConnecting = false;
	}
	
	// 刷新蓝牙列表
	private void refreshList() 
	{
		SimpleAdapter adapter = new BluetoothListAdapter(LaunchActivity.this, 
				mDeviceList, 
				R.layout.lay_item_devicelist, 
				new String[] { Statics.BLUETOOTH_NAME, Statics.BLUETOOTH_MAC }, 
				new int[] { R.id.tv_name, R.id.tv_mac });
		mListBlueDevices.setAdapter(adapter);
	}
	
	// 蓝牙设备列表适配器
	private class BluetoothListAdapter extends SimpleAdapter 
	{
		public BluetoothListAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) 
		{
			super(context, data, resource, from, to);
		}
		
		public View getView(int position, View convertView, ViewGroup parent) 
		{
			if(convertView == null) 
			{
				convertView = super.getView(position, convertView, parent);
			}
			Map<String, String> map = mDeviceList.get(position);
			if(map.get(Statics.BLUETOOTH_STATUS).equals("true")) 
			{
				convertView.findViewById(R.id.iv_red).setVisibility(View.VISIBLE);
				((ImageView)convertView.findViewById(R.id.iv_state)).setImageResource(R.drawable.icon_bt_connected);
			}
			else
			{
				convertView.findViewById(R.id.iv_red).setVisibility(View.INVISIBLE);
				((ImageView)convertView.findViewById(R.id.iv_state)).setImageResource(R.drawable.icon_bt);
			}
			return convertView;
		}
	}
	
	// 处理登录操作
	private void doLogin() 
	{
		try 
		{
			// 调试模式
			if(Statics.IS_DEBUG) 
			{
				mEditPwd.setText(null);
				jumpMainInDebugMode();
				return;
			}
			
			// 校验工号
			final String strUserNo = mEditNo.getText().toString().trim();	
			if(StringUtil.isEmptyOrNull(strUserNo)) 
			{
				DialogUtil.MsgBox(TITLE_STR, "请输入工号！");
				return;
			}
			// 暗码处理 *  ＊    # ＃
			if(strUserNo.equals("*#210#") || strUserNo.equals("＊＃210＃")
				||strUserNo.equals("＊#210#")||strUserNo.equals("*＃210＃"))
			{
				mHandler.sendEmptyMessage(Statics.MSG_SECRET_CODE);
				mEditNo.setText(null);
				return;
			}
			
			// 校验密码
			final String strPswd = mEditPwd.getText().toString().trim();
			if (StringUtil.isEmptyOrNull(strPswd) || strPswd.length() != 6) 
			{
				DialogUtil.MsgBox(TITLE_STR, "请输入6位密码！");
				return;
			}
			mEditPwd.setText(null);
			// 保存营业员工号
			App.setUserNo(strUserNo);
			
			new Thread() 
			{
				@Override
				public void runs() 
				{
					try 
					{
						// 检测网络是否OK
						DialogUtil.showProgress("正在检测网络...");
						if(socketService.isNetworkOk() == false) 
						{
							DialogUtil.closeProgress();
							DialogUtil.MsgBox(TITLE_STR, getString(R.string.network_error));
							return;
						}
						// 这里做登陆验证动作
						loginFirst(strUserNo, strPswd);
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
			}.start();
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	// 跳转到主界面
	private void jumpMainInDebugMode() 
	{
		DialogUtil.showProgress("正在处理, 请稍候");
		new Thread() 
		{
			public void runs() 
			{
				try
				{
					ConstantData.setServerPath("http://172.16.200.28:8000");
					// 加载优惠购机的手机信息
					loadPhoneInfoFromServer(null);
					DialogUtil.closeProgress();
					// 跳转到主界面
					jump2DgMain();
				}
				catch(Exception e) 
				{
					e.printStackTrace();
					DialogUtil.closeProgress();
				}
			}
		}.start();
	}
	
	// 与平台第一次交互, 以线程形式等待密码检验的结果
	private void loginFirst(final String strUserNo, final String strUserPswd) 
	{
		try 
		{
			// 外网
			SharedPreferences sp = getSharedPreferences(Statics.CONFIG, Activity.MODE_PRIVATE);
			String strIP = sp.getString("ip", mStrServerIP);
			int port = sp.getInt("port", mServerPort);
			socketService.setIP1Adress(strIP);				// equipmentService.getIp1()
			socketService.setIP2Adress(strIP);				// equipmentService.getIp2()
			socketService.setPort1(port);					// equipmentService.getPort1()
			socketService.setPort2(port);					// equipmentService.getPort2()
			Log.i("服务器地址:", strIP + " : " + port);
			// 连接服务器
			DialogUtil.showProgress("正在连接服务器...");
			//if (socketService.isConnect() == false && socketService.connect(true) == false) 
			if (socketService.connect(true) == false) 
			{
				DialogUtil.closeProgress();
				DialogUtil.MsgBox(TITLE_STR, socketService.getLastKnowError());
				return;
			}
			// 清除登陆密钥
			equipmentService.setLoginKey(null);
			// 获取随机密钥
			DialogUtil.showProgress("正在获取安全配置...");
			if(mBusiness00.bll0000(strUserNo, strUserPswd) == false) 
			{
				Log.e("xxxx", "获取随机密钥失败!");
				DialogUtil.closeProgress();
				return;
			}
			// 获取网点信息
			DialogUtil.showProgress("正在获取网点信息...");
			String[] strShopName = new String[1];
			String[] strShopAddr = new String[1];
			String[] strAgentName = new String[1];
			String[] strAgentTel = new String[1];
			List<Map<String, String>> listData	= mBusiness00.bll000F(App.getUserNo(), strShopName, strShopAddr, strAgentName, strAgentTel);
			if(!CollectionUtil.isEmptyOrNull(listData)) 
			{
				// 更新网点名称
				App.setShopName(strShopName[0]);
				// 更新网点地址
				App.setShopAddr(strShopAddr[0]);
				// 更新网点电话
				App.setShopTel(strAgentTel[0]);
				// 代理商
				App.setAgentName(strAgentName[0]);
				
				String strUserName = null;
				for(int i=0;i<listData.size();i++)
				{
					if(!StringUtil.isEmptyOrNull(App.getUserNo()))
					{
						if(App.getUserNo().equals(listData.get(i).get(Statics.KEY_USER_NO)))
						{
							strUserName = listData.get(i).get(Statics.KEY_USER_NAME);
							break;
						}
					}
				}
				// 营业员姓名
				App.setUserName(strUserName);
				strShopName 	= null;
				strShopAddr 	= null;
				strAgentName 	= null;
				strAgentTel 	= null;
			}
			// 开机上报
			DialogUtil.showProgress("正在登录上报...");
			int flag = mBusiness00.bll0001();
			if (flag == ReturnEnum.RECIEVE_TIMEOUT) 
			{
				DialogUtil.closeProgress();
				DialogUtil.MsgBox(TITLE_STR, "开机上报超时！");
			} 
			else if (flag == ReturnEnum.FAIL) 
			{
				runOnUiThread(new Runnable() 
				{
					public void run() 
					{
						String msg = "开机上报失败\n[失败原因: " + mBusiness00.getLastknownError() + "]";
						DialogUtil.closeProgress();
						DialogUtil.MsgBox(TITLE_STR, msg);
					}
				});
			}
			else if (flag == ReturnEnum.SUCCESS) 
			{
				// 获取优惠购机的图片
				loadPhoneInfoFromServer(equipmentService.getPsamId());
				// 检测升级
				mCheckVersion = new CheckVersionTask(LaunchActivity.this, mHandler);
				// checkupdate的参数为false代表"不需要升级时不提示任何信息"
				if(mCheckVersion.checkupdate(false) == false) 
				{
					// 关闭进度条
					DialogUtil.closeProgress();
					// 跳转到主界面
					jump2DgMain();
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			DialogUtil.closeProgress();
		}
	}
	
	// 从服务器加载手机信息
	private void loadPhoneInfoFromServer(final String psam_id) 
	{
		synchronized (Statics.INIT_OK_LOCK) 
		{
			PhoneParamsDao phoneDao = new PhoneParamsDao(getBaseContext());
			String json = "";
			try 
			{
				DialogUtil.showProgress("正在获取优惠购机信息...");
				Map<String, String> param = new HashMap<String, String>(); 
				//param.put("psamid", "3801700000000014"); 
				param.put("psamid", psam_id); 
				Log.i("", "*********开始获取优惠购机手机参数信息********");
				json = HttpURLConnectionUtil.doGet(ConstantData.getServerPath()+"/minizx/getMobile.shtml", param, null, 10000);
				Log.i("优惠购机JSON数据:", json);
			}  
			catch (Exception e) 
			{
				e.printStackTrace();
				Log.i("MODEL_DOWNLOAD", "HTTP连接出错");
				ConstantData.modelList = null;
				phoneDao.clear();
			}
			
			if (StringUtil.isEmptyOrNull(json)&& Statics.IS_DEBUG) 
			{
				json = "{\"models\":[{\"PRICE\":\"4600.00\",\"ROM\":\"16GB\",\"RECORD\":\"支持\",\"PIC\":\"1391167430.jpg\"," +
						"\"RADIO\":\"支持\",\"CPU\":\"1GHz\",\"SCREEN\":\"4\",\"SIZES\":\"123.8 毫米 x 58.6 毫米 x 7.6 毫米  112g\"," +
						"\"VIDEOFORMAT\":\"支持H.264/M4V/MP4/MOV/MPEG-4/AVI等格式\"," +
						"\"JAVA\":\"支持\",\"BATTERYSTORAGE\":\"1440mAh\",\"CAMERA1\":\"800 万像素 iSight 摄像头， 可用于拍照和 1080p HD 高清视频拍摄\"," +
						"\"ACCESSS\":\"带线控功能和麦克风的 Apple EarPods、Lightningto USB 连接线、USB\"," +
						"\"DATATRANS\":\"802.11b/g/n WLAN (仅限 802.11n 2.4GHz) Bluetooth 4.0\",\"CAMERA2\":\"120万像素\"," +
						"\"CALLTIME\":\"长达 225 小时\",\"MODEL_ID\":\"87\",\"ZOOM\":\"轻点自动对焦\",\"SCREENPARAM\":" +
						"\"1136 x 640 像素分辨率， 326 ppiMulti-Touch 显示屏\",\"BATTERY\":\"内置充电式锂离子电池\"," +
						"\"COLOR\":\"白\",\"RECOMMENDED\":\"0\",\"CORENUM\":\"双核\",\"EXTEND\":\"无\",\"NET\":\"UMTS/HSPA+/DC-HSDPA\"," +
						"\"NAME\":\"GT-I5508\",\"RAM\":\"1GB\",\"MUSICFORMAT\":\"支持AAC/Protected AAC/HE-AAC/MP3/\",\"BIGPICS\":" +
						"\"1390960432.jpg,1390963431.jpg,1390964431.jpg,\",\"OP\":\"iOS\",\"WAITTIME\":\"使用 3G 网络时长达 8 小时\"," +
						"\"GPS\":\"支持\",\"BRAND\":\"三星\"}," +
						
						"{\"PRICE\":\"5499.00\",\"ROM\":\"16GB\",\"RECORD\":\"支持\",\"PIC\":" +
						"\"1391208230.jpg\",\"RADIO\":\"支持\",\"CPU\":\"1.1\",\"SCREEN\":\"4\",\"SIZES\":\"123.8 毫米 x 58.6 毫米 x 7.6 毫米\"," +
						"\"VIDEOFORMAT\":\"支持H.264/M4V/MP4/MOV/M等格式\",\"JAVA\":\"支持\",\"BATTERYSTORAGE\":\"1440mAh\",\"CAMERA1\":\"800 万像素\"," +
						"\"ACCESSS\":\"iPhone 5、带线控功能和麦克风的 Apple EarPods、Lightningto USB 连接线、USB 电源适配器、文档资料Lightning 接口、" +
						"3.5 毫米立体声耳机迷你插孔、内置扬声器、内置麦克风、SIM 卡插槽\",\"DATATRANS\":\"802.11b/g/n WLAN (仅限 802.11n 2.4GHz) " +
						"Bluetooth 4.0\",\"CAMERA2\":\"120万像素\",\"CALLTIME\":\"长达 225 小时\",\"MODEL_ID\":\"90\",\"ZOOM\":\"轻点自动对焦\"," +
						"\"SCREENPARAM\":\"Multi-Touch 显示屏，1136 x 640 像素分辨率， 326 ppi\",\"BATTERY\":\"内置充电式锂离子电池\",\"COLOR\":" +
						"\"白\",\"RECOMMENDED\":\"0\",\"CORENUM\":\"单核\",\"EXTEND\":\"无\",\"NET\":\"UMTS/HSPA+/DC-HSDPA\",\"NAME\":\"iPhone5 16G\"," +
						"\"RAM\":\"1GB\",\"MUSICFORMAT\":\"支持AAC/Protected AAC/HE-\",\"BIGPICS\":\"1391243231.jpg,1391244231.jpg,1391245232.jpg\"," +
						"\"OP\":\"iOS\",\"WAITTIME\":\"使用 3G 网络时长达 8 小时\",\"GPS\":\"支持\",\"BRAND\":\"Apple\"}," +
						
						"{\"PRICE\":\"2399.00\"," +
						"\"ROM\":\"16G\",\"RECORD\":\"支持\",\"PIC\":\"1391211330.jpg\",\"RADIO\":\"支持\",\"CPU\":\"1.2GHz\",\"SCREEN\":\"1600\"," +
						"\"SIZES\":\"126mm*62mm*10.2mm\",\"VIDEOFORMAT\":\"支持1080p\",\"JAVA\":\"支持\",\"BATTERYSTORAGE\":\"2000mAh\",\"CAMERA1\":" +
						"\"800万像素\",\"ACCESSS\":\"单电单充\",\"DATATRANS\":\"--\",\"CAMERA2\":\"200万像素\",\"CALLTIME\":\"3G： 待机：450小时\"," +
						"\"MODEL_ID\":\"21\",\"ZOOM\":\"背照式2代CMOS\",\"SCREENPARAM\":\"IPS，1280x720像素\",\"BATTERY\":\"--\",\"COLOR\":\"白\"," +
						"\"RECOMMENDED\":\"1\",\"CORENUM\":\"四核\",\"EXTEND\":\"无\",\"NET\":\"2G:GSM\",\"NAME\":\"2S\",\"RAM\":\"2G\",\"MUSICFORMAT\":" +
						"\"支持mp3、acc、amr、式\",\"BIGPICS\":\"1391206331.jpg,1391204332.jpg,1391203334.jpg,1391205333.jpg\",\"OP\":\"安卓\"," +
						"\"WAITTIME\":\"3G 通话：\",\"GPS\":\"支持\",\"BRAND\":\"小米\"}," +
						
						"{\"PRICE\":\"4125.00\",\"ROM\":\"16GB\",\"RECORD\":" +
						"\"不支持\",\"PIC\":\"1391207530.jpg\",\"RADIO\":\"不支持\",\"CPU\":\"1GHz\",\"SCREEN\":\"3.5\",\"SIZES\":\"\"," +
						"\"VIDEOFORMAT\":\"支持视频格式\",\"JAVA\":\"不支持\",\"BATTERYSTORAGE\":\"暂无信息\",\"CAMERA1\":\"800万\",\"ACCESSS\":" +
						"\"带遥控功能和麦克风的Apple耳机？，Dock Connector to USB线缆，USB 电源适配器？\",\"DATATRANS\":" +
						"\"802.11b/g/n WLAN (仅限 802.11n 2.4GHz) Bluetooth 4.0 无线技术\",\"CAMERA2\":\"正面摄像头可拍摄 VGA\",\"CALLTIME\":" +
						"\"200小时\",\"MODEL_ID\":\"98\",\"ZOOM\":\"轻点自动对焦\",\"SCREENPARAM\":\"Multi-Touch 显示屏，960 x 640 像素分辨率，" +
						"每英寸 326 像素\",\"BATTERY\":\"内置可充电式锂电池\",\"COLOR\":\"黑\",\"RECOMMENDED\":\"0\",\"CORENUM\":\"双核\",\"EXTEND\":\"无\",\"NET\":" +
						"\"UMTS/HSDPA/HSUPA (850\",\"NAME\":\"iPhone4S 16G\",\"RAM\":\"512MB\",\"MUSICFORMAT\":\"支持声音文\",\"BIGPICS\":" +
						"\"1391191531.jpg,1391150532.jpg,1391151533.jpg,1391152534.jpg\",\"OP\":\"iOS\",\"WAITTIME\":\"使用 3G 网络时长达 8 小时\"," +
						"\"GPS\":\"支持\",\"BRAND\":\"Apple\"}," +
						
						"{\"PRICE\":\"4699.00\",\"ROM\":\"32GB\",\"RECORD\":\"支持\",\"PIC\":\"1391210630.jpg\"," +
						"\"RADIO\":\"支持\",\"CPU\":\"1.2GHz\",\"SCREEN\":\"4.7\",\"SIZES\":\"137.8 x 69.3 x 10.4 mm\",\"VIDEOFORMAT\":" +
						"\".3gp、.3g2、.mp4\",\"JAVA\":\"支持\",\"BATTERYSTORAGE\":\"2300 mAh 内嵌式电池\",\"CAMERA1\":\"--\",\"ACCESSS\":\"\"," +
						"\"DATATRANS\":\"无线连接 Bluetooth 4.0, Wi-Fi: IEEE 802.11 a/b/g/n/ac, 支持家用红外线遥控器\",\"CAMERA2\":\"210万\"," +
						"\"CALLTIME\":\"--\",\"MODEL_ID\":\"802\",\"ZOOM\":\"CMOS\",\"SCREENPARAM\":\"其他，468ppi\",\"BATTERY\":\"内嵌式电池\"," +
						"\"COLOR\":\"银\",\"RECOMMENDED\":\"1\",\"CORENUM\":\"四核\",\"EXTEND\":\"支持microSD扩展\",\"NET\":\"SIM1：WCDMA:850/1900/2100," +
						"\",\"NAME\":\"802w\",\"RAM\":\"2GB\",\"MUSICFORMAT\":\"aac、.amr、.ogg、.m4a\",\"BIGPICS\":\"1391195631.jpg,1391196632.jpg," +
						"1391197633.jpg,1391198634.jpg\",\"OP\":\"安卓\",\"WAITTIME\":\"--\",\"GPS\":\"支持\",\"BRAND\":\"HTC\"}," +
						
						
						"{\"PRICE\":\"4199.00\"," +
						"\"ROM\":\"16GB\",\"RECORD\":\"不支持\",\"PIC\":\"1391212730.jpg\",\"RADIO\":\"支持\",\"CPU\":\"1GHz\",\"SCREEN\":\"4.5\"," +
						"\"SIZES\":\"129 x 70.6 x 8.5 毫米\",\"VIDEOFORMAT\":\"H.263, MPEG-4\",\"JAVA\":\"不支持\",\"BATTERYSTORAGE\":\"2000mAh\"," +
						"\"CAMERA1\":\"870万\",\"ACCESSS\":\"充电器、数据线、耳机、充电器、SIM卡针等\",\"DATATRANS\":\"理论上行峰值：5.76Mbps 理论下行峰值：" +
						"21Mbps\",\"CAMERA2\":\"120万\",\"CALLTIME\":\"440小时\",\"MODEL_ID\":\"925\",\"ZOOM\":\"BSI CMOS，数码变焦\",\"SCREENPARAM\":" +
						"\"OLED，1280X768\",\"BATTERY\":\"锂电池\",\"COLOR\":\"灰\",\"RECOMMENDED\":\"1\",\"CORENUM\":\"双核\",\"EXTEND\":\"无\",\"NET\":" +
						"\"GSM 850, 900, 1800,\",\"NAME\":\"925\",\"RAM\":\"1GB\",\"MUSICFORMAT\":\"AAC LC, AMR-NB, AAC+/HEAA\",\"BIGPICS\":" +
						"\"1391199731.jpg,1391200732.jpg,1391201733.jpg,1391202734.jpg\",\"OP\":\"Windows Mobile\",\"WAITTIME\":\"3G 12.8小时；" +
						"2G 18.3小时\",\"GPS\":\"支持\",\"BRAND\":\"诺基亚\"}]," +
						
						"\"brands\":[{\"NAME\":\"MI2\"},{\"NAME\":\"DELL\"},{\"NAME\":\"飞利浦\"}," +
						"{\"NAME\":\"海信\"},{\"NAME\":\"小米\"},{\"NAME\":\"禹华\"},{\"NAME\":\"LANDMARK\"},{\"NAME\":\"科米\"},{\"NAME\":\"UMO\"}," +
						"{\"NAME\":\"长虹\"},{\"NAME\":\"HTC\"},{\"NAME\":\"ABO\"},{\"NAME\":\"上海贝尔\"},{\"NAME\":\"3G\"},{\"NAME\":\"创维（Skyworth）" +
						"\"},{\"NAME\":\"黑莓\"},{\"NAME\":\"TCL\"},{\"NAME\":\"代理商品牌\"},{\"NAME\":\"富春江\"},{\"NAME\":\"三星\"},{\"NAME\":\"天语\"}," +
						"{\"NAME\":\"酷派\"},{\"NAME\":\"ZTE中兴\"},{\"NAME\":\"时代动力\"},{\"NAME\":\"网讯\"},{\"NAME\":\"宏基\"},{\"NAME\":" +
						"\"ALCATEL\"},{\"NAME\":\"Lenovo\"},{\"NAME\":\"Bird\"},{\"NAME\":\"AMOI\"},{\"NAME\":\"VEVA\"},{\"NAME\":\"postcom\"}," +
						"{\"NAME\":\"爱国者\"},{\"NAME\":\"QIGI\"},{\"NAME\":\"海尔\"},{\"NAME\":\"OKWAP\"},{\"NAME\":\"Philip\"},{\"NAME\":\"华为\"}," +
						"{\"NAME\":\"sony ericsson\"},{\"NAME\":\"dopod\"},{\"NAME\":\"夏普\"},{\"NAME\":\"Anycall\"},{\"NAME\":\"大唐\"}," +
						"{\"NAME\":\"LG\"},{\"NAME\":\"诺基亚\"},{\"NAME\":\"摩托罗拉\"},{\"NAME\":\"Apple\"},{\"NAME\":\"GPHONE\"}," +
						"{\"NAME\":\"新势力fd\"}],\"updateurl\":\"/system/update\",\"url\":\"/ZB_GMINI/upload/file\",\"version\":\"1.0.1\"}";
						try
						{
							JSONObject jobj = new JSONObject(json);
							ConstantData.rangeFilePath = jobj.getString("url");
							ConstantData.modelList = ConstantData.getModelList(jobj);
							ConstantData.mBrands = ConstantData.getBrands(jobj);
						}
						catch (JSONException e)
						{
							e.printStackTrace();
						}
			}
			else
			{
				try
				{
					JSONObject jobj = new JSONObject(json);
					String version = jobj.getString("version");
					ConstantData.rangeFilePath = jobj.getString("url");
					if(isDBisNeedUpdate("phoneModel", version))
					{
						// 需要更新，清空本地数据，插入新数据
						ConstantData.modelList = ConstantData.getModelList(jobj);
						ConstantData.setBands(ConstantData.getBrands(jobj));
						phoneDao.clear(); 
						for(int i = 0; i < ConstantData.modelList.size(); i++)
						{
							phoneDao.insertData(ConstantData.modelList.get(i));
						}
					}
					else
					{
						// 不需要更新，从数据库从读取数据到内存
						ConstantData.modelList = phoneDao.getRelativePhones("");
					}
				} 
				catch (JSONException e) 
				{
					Log.i("MODEL_DOWNLOAD", "JSON转化出错");
					ConstantData.modelList = null;
					phoneDao.clear(); 
				}
			}
			
			RecommendPackageDao packDao = new RecommendPackageDao(getBaseContext());
			json = "";
			// 开始获取推荐包参数信息
			try 
			{
				DialogUtil.showProgress("正在获取推荐包信息...");
				Map<String, String> param = new HashMap<String, String>();
				//param.put("psamid", "3801700000000011"); 
				param.put("psamid", psam_id); 
				Log.i("", "*********开始获取推荐包参数信息********");
				json = HttpURLConnectionUtil.doGet(ConstantData.getServerPath()+"/minizx/getRecommandPackage.shtml", param, null, 10000);
				Log.i("最in机型JSON数据：", json);
				//com.sunnada.baseframe.util.Log.e("最in机型JSON数据：", json);
			}  
			catch (Exception e) 
			{
				e.printStackTrace();
				Log.i("RECOMMEND_PACKAGE_DOWNLOAD", "HTTP连接出错");
				ConstantData.mRecommendPackgeList = null;
				packDao.clear();
			}
			
			if (!StringUtil.isEmptyOrNull(json)) 
			{
				try 
				{
					JSONArray jobj = new JSONArray(json);
					ConstantData.rangeFilePath = jobj.getJSONObject(0).getString("url");
					if(isDBisNeedUpdate("recommendPackage", ""))
					{
						// 需要更新，清空本地数据，插入新数据
						ConstantData.mRecommendPackgeList = ConstantData.getRecommendPackageList(jobj, true);
						packDao.clear();
						for(int i = 0; i < ConstantData.mRecommendPackgeList.size(); i++)
						{
							packDao.insertData(ConstantData.mRecommendPackgeList.get(i));
						}
					}
					else
					{
						// 不需要更新，从数据库从读取数据到内存
						ConstantData.mRecommendPackgeList = packDao.getAllRecommendPackages();
					}
				} 
				catch (JSONException e) 
				{
					Log.i("RECOMMEND_PACKAGE_DOWNLOAD", "JSON转化出错");
					ConstantData.mRecommendPackgeList = null;
					packDao.clear();
				}
			}
			
			ProductInfoDao productDao = new ProductInfoDao(getBaseContext());
			json = "";
			// 开始获取套餐包参数信息
			try 
			{
				DialogUtil.showProgress("正在获取套餐包信息...");
				Map<String, String> param = new HashMap<String, String>();
				//param.put("psamid", "3801700000000005"); 
				param.put("psamid", psam_id); 
				Log.i("", "*********开始获取套餐包参数信息********");
				json = HttpURLConnectionUtil.doGet(ConstantData.getServerPath()+"/minizx/getProPackage.shtml", param, null, 10000);
				Log.i("推荐包JSON数据：", json);
				//com.sunnada.baseframe.util.Log.e("推荐包JSON数据：", json);
			}  
			catch (Exception e) 
			{
				e.printStackTrace();
				Log.i("PRODUCT_DOWNLOAD", "HTTP连接出错");
				ConstantData.mProductList = null;
				productDao.clear();
			}
			
			if (!StringUtil.isEmptyOrNull(json)) 
			{
				try
				{
					JSONArray jobj = new JSONArray(json);
					ConstantData.rangeFilePath = jobj.getJSONObject(0).getString("url");
					if(isDBisNeedUpdate("productIndo", ""))
					{
						// 需要更新，清空本地数据，插入新数据
						ConstantData.mProductList = ConstantData.getProductList(jobj, false);
						productDao.clear();
						for(int i = 0; i < ConstantData.mProductList.size(); i++)
						{
							productDao.insertData(ConstantData.mProductList.get(i));
						}
					}
					else
					{
						// 不需要更新，从数据库从读取数据到内存
						ConstantData.mProductList = productDao.getAllProducts();
					}
				}
				catch (JSONException e) 
				{
					Log.i("PRODUCT_DOWNLOAD", "JSON转化出错");
					ConstantData.mProductList = null;
					productDao.clear();
				}
			}
			
			json = "";
			// 开始获取套风向标信息
			// http://172.16.200.23:8110/minizx/getKeyword.shtml
			try 
			{
				DialogUtil.showProgress("正在获取风向标信息...");
				Log.i("", "*********开始获取风向标信息********");
				json = HttpURLConnectionUtil.doGet(ConstantData.getServerPath()+"/minizx/getKeyword.shtml", null, null, 1000);
				//json = HttpURLConnectionUtil.doGet("http://172.16.200.23:8110/minizx/getKeyword.shtml", null);
				Log.i("风向标JSON数据：", json);
			}  
			catch (Exception e) 
			{
				e.printStackTrace();
				Log.i("PRODUCT_DOWNLOAD", "HTTP连接出错");
				ConstantData.mIndicator = null;
			}
			
			if (!StringUtil.isEmptyOrNull(json)) 
			{
				try
				{
					if(isDBisNeedUpdate("inductor", ""))
					{
						ConstantData.mIndicator = ConstantData.getIndicator(json);
						// 风向标数据较小，直接保存在SharedPreferences中
						Editor edit = getSharedPreferences(Statics.CONFIG,Activity.MODE_PRIVATE).edit();
						edit.putString("indicator", "json");
						edit.commit();
					}
					else
					{
						json = getSharedPreferences(Statics.CONFIG,Activity.MODE_PRIVATE).getString("indicator", "");
						ConstantData.mIndicator = ConstantData.getIndicator(json);
					}
				}
				catch (JSONException e) 
				{
					Log.i("PRODUCT_DOWNLOAD", "JSON转化出错");
					ConstantData.mIndicator = null;
					Editor edit = getSharedPreferences(Statics.CONFIG,Activity.MODE_PRIVATE).edit();
					edit.putString("indicator", "");
					edit.commit();
				}
			}
		}
	}
	
	// 比较版本号，判断数据库是否需更新
	private boolean isDBisNeedUpdate(String dbName, String version)
	{
		Log.e("version", version);
		return true;
	}
	
	// 跳转到主界面
	private void jump2DgMain() 
	{
		Intent intent = new Intent(LaunchActivity.this, DgMainActivity.class);
		startActivity(intent);
		finish();
	}
	
	// 初始化数据库对象，创建数据库，获取数据库表中基础信息
	private void initDB() 
	{
		// 初始化数据库
		SystemInfoDao sysinfodao = new SystemInfoDao(this);
		sysinfodao.CreatTable();

		// 获取所有CRC数据用来开机更新比较
		//SystemParamsDao sqlit = new SystemParamsDao(this);
		// sqlit.GetAllCrc();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK) 
		{
			// 释放设备资源
			equipmentService.releaseEquiment();
			if(Statics.mCurMap != null)
			{
				Statics.mCurMap.clear();
				Statics.mCurMap = null;
			}
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	static class FTPThread extends java.lang.Thread 
	{
		private static boolean 	mIsRun 			= false;
		private static int 		DELAY 			= 1000*60*20;		// 20分钟启动一次
		private ImgLogReport 	mImgLogReport 	= null;				// 照片日志上报
		
		public FTPThread(Context context, IEquipmentService equipmentService, ISocketService socketService, IDataBaseService dataBaseService) 
		{
			mImgLogReport = new ImgLogReport(context, equipmentService, socketService, dataBaseService);
		}
		
		@Override
		public void run() 
		{
			mIsRun = true;
			while(mIsRun)
			{
				sleep(1000*20);
				
				// 在这里增加图片处理函数
				try 
				{
					mImgLogReport.tickAction();
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
				try 
				{
					// 心跳间隔
					sleep(DELAY);
				} 
				catch (Exception e) 
				{
					Log.e("ftpthread", "err"+e.getMessage());
				}
			}
		}
		
		public void do_stop() 
		{
			mIsRun = false;
		}
		
		public void sleep(int timeout) 
		{
			int ticks = timeout/1000;
			for(int i=0; i<ticks; i++) 
			{
				SystemClock.sleep(1000);
				if(mIsRun == false) 
				{
					return;
				}
			}
		}
	}
}
