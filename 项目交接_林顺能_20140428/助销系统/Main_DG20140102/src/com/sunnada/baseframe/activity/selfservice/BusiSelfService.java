package com.sunnada.baseframe.activity.selfservice;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.util.Log;

import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.util.ButtonUtil;

public class BusiSelfService extends BaseActivity implements OnClickListener
{
	private static final int	BUSI_SELFSERVICE_CHECK_DAILY		= 0x01;
	private static final int	BUSI_SELFSERVICE_MAILBOX			= 0x02;
	private static final int 	BUSI_SELFSERVICE_BUSI_UPDATE		= 0x03;
	private static final int	BUSI_SELFSERVICE_SHOPINFO			= 0x04;
	private static final int	BUSI_SELFSERVICE_SOFT_UPDATE		= 0x05;
	private static final int    BUSI_SELFSERVICE_ECARD_INQUIRY		= 0x06;
	private static final int    BUSI_SELFSERVICE_INVOICE_PRINT      = 0x07;
	
	private int[]				mBusiResource 			= {	
															R.drawable.selfservice_busi_check,
															R.drawable.selfservice_busi_mailbox,
															R.drawable.selfservice_busi_param_update,
															R.drawable.selfservice_busi_shopinfo_query,
															R.drawable.selfservice_busi_soft_update,
															R.drawable.elec_card_print_center,
															R.drawable.selfservice_busi_invoice_print
														  };
	
	private int[]				mBusiResourceOrange 	= {	
															R.drawable.selfservice_busi_check_orange,
															R.drawable.selfservice_busi_mailbox_orange,
															R.drawable.selfservice_busi_param_update_orange,
															R.drawable.selfservice_busi_shopinfo_query_orange,
															R.drawable.selfservice_busi_soft_update_orange,
															R.drawable.elec_card_print_center_orange,
															R.drawable.selfservice_busi_invoice_print
														  };
	
	private int[]				mBusiID					= {
															BUSI_SELFSERVICE_CHECK_DAILY, 
															BUSI_SELFSERVICE_MAILBOX,
															BUSI_SELFSERVICE_BUSI_UPDATE, 
															BUSI_SELFSERVICE_SHOPINFO,
															BUSI_SELFSERVICE_SOFT_UPDATE,
															BUSI_SELFSERVICE_ECARD_INQUIRY,
															BUSI_SELFSERVICE_INVOICE_PRINT
														};
	
	private int					mBusiType;				// 业务类型
	private LinearLayout 		mMenuBusiAccept			= null;
	
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lay_selfservice);
		
		// 初始化布局
		runOnUiThread(new Runnable() 
		{
			public void run() 
			{
				initViews();
			}
		});
	}
	
	// 初始化布局
	private void initViews() 
	{
		ImageView imageBacktoMainMenu = (ImageView) findViewById(R.id.iv_backto_mainmenu);
		initShow(imageBacktoMainMenu);
		imageBacktoMainMenu.setOnClickListener(this);
		
		mMenuBusiAccept = (LinearLayout) findViewById(R.id.menu_selfservice);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			initSubLayout(mMenuBusiAccept, mBusiResourceOrange, mBusiID);
		}
		else
		{
			initSubLayout(mMenuBusiAccept, mBusiResource, mBusiID);
		}
	}
	
	private void initShow(ImageView imageBacktoMainMenu)
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			imageBacktoMainMenu.setImageResource(R.drawable.iv_backto_mainmenu_orange);
		}
	}
	
	// 初始化子布局
	private void initSubLayout(LinearLayout parentLayout, int[] resID, int[] ctlID)
	{
		// 二级菜单布局(包含垂直分布的线性布局)
		LinearLayout busi = new LinearLayout(this);
		LayoutParams busiParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		busiParams.setMargins(0, 50, 0, 0);
		busi.setLayoutParams(busiParams);
		busi.setOrientation(LinearLayout.VERTICAL);
		// 
		LinearLayout sub_busi = null;
		int count = 0;
		for(int i=0; i<ctlID.length; i++)
		{
			if(!checkAuthority(ctlID[i])) 
			{
				// 是否已经结束
				if(i == ctlID.length-1)
				{
					if(sub_busi != null)
					{
						busi.addView(sub_busi);
					}
				}
				// 未获得菜单权限
				continue;
			}
			// 是否该换行?
			if(count%3 == 0)
			{
				if(count > 0)
				{
					busi.addView(sub_busi);
				}
				sub_busi = new LinearLayout(this);
				LayoutParams subBusiParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				subBusiParams.setMargins(10, 0, 0, 10);
				sub_busi.setLayoutParams(subBusiParams);
			}
			count++;
			Button button = new Button(this);
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(5, 0, 5, 0);
			button.setLayoutParams(params);
			button.setId(ctlID[i]);
			button.setBackgroundDrawable(getResources().getDrawable(resID[i]));
			button.setOnClickListener(this);
			sub_busi.addView(button);
			// 是否已经结束
			if(i == ctlID.length-1)
			{
				busi.addView(sub_busi);
				break;
			}
		}
		parentLayout.addView(busi);
	}
	
	// 获取菜单权限
	private boolean checkAuthority(int id)
	{
		boolean result = false;
		switch (id)
		{
			// 营业对账
			case BUSI_SELFSERVICE_CHECK_DAILY:
				result = getMenuAuthority("0810");
				break;
				
			// 收件箱
			case BUSI_SELFSERVICE_MAILBOX:
				//result = getMenuAuthority("0820");
				// 固化在本地
				result = true;
				break;
				
			// 系统更新
			case BUSI_SELFSERVICE_BUSI_UPDATE:
				//result = getMenuAuthority("0830");
				// 固化在本地
				result = true;
				break;
				
			// 网点信息查询
			case BUSI_SELFSERVICE_SHOPINFO:
				//result = getMenuAuthority("0840");
				// 固化在本地
				result = true;
				break;
			
			// 终端软件升级
			case BUSI_SELFSERVICE_SOFT_UPDATE:
				//result = getMenuAuthority("0850");
				// 固化在本地
				result = true;
				break;
				
			// 电子卡密打印中心
			case BUSI_SELFSERVICE_ECARD_INQUIRY:
				result = getMenuAuthority("0860");
				break;
			
			case BUSI_SELFSERVICE_INVOICE_PRINT:
				// result = getMenuAuthority("0870");
				result = false;
				break;
				
			default:
				break;
		}
		return result;
	}
	
	@Override
	public void onInitSuccess() 
	{
		
	}

	@Override
	public void onInitFail() 
	{
		
	}
	
	@Override
	public void onDestroy() 
	{
		super.onDestroy();
		
		mMenuBusiAccept.removeAllViews();
		mMenuBusiAccept = null;
		// 
		System.gc();
		Log.e("", "自服务类onDestroy()");
	}

	@Override
	public void onClick(View v) 
	{
		if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
		{
			return;
		}
		
		switch(v.getId())
		{
		// 返回到主菜单
		case R.id.iv_backto_mainmenu:
			FrameActivity.mHandler.obtainMessage(FrameActivity.FINISH).sendToTarget();
			return;
			
		// 对账
		case BUSI_SELFSERVICE_CHECK_DAILY:
			mBusiType = BUSI_SELFSERVICE_CHECK_DAILY;
			break;
			
		// 业务更新
		case BUSI_SELFSERVICE_BUSI_UPDATE:
			mBusiType = BUSI_SELFSERVICE_BUSI_UPDATE;
			break;
			
		// 收件箱
		case BUSI_SELFSERVICE_MAILBOX:
			mBusiType = BUSI_SELFSERVICE_MAILBOX;
			break;
			
		// 网点信息查询
		case BUSI_SELFSERVICE_SHOPINFO:
			mBusiType = BUSI_SELFSERVICE_SHOPINFO;
			break;
			
		// 终端软件升级
		case BUSI_SELFSERVICE_SOFT_UPDATE:
			mBusiType = BUSI_SELFSERVICE_SOFT_UPDATE;
			break;
		
		// 电子卡卡密查询
		case BUSI_SELFSERVICE_ECARD_INQUIRY:
			// DialogUtil.MsgBox("温馨提示", "此业务暂未开放！");
			mBusiType = BUSI_SELFSERVICE_ECARD_INQUIRY;
			break;
		
		case BUSI_SELFSERVICE_INVOICE_PRINT:
			mBusiType = BUSI_SELFSERVICE_INVOICE_PRINT;
			break;
			
		default:
			mBusiType = BUSI_SELFSERVICE_CHECK_DAILY;
			break;
		}
		startRotation(v);
		
		Timer timer = new Timer();
		timer.schedule(new TimerTask() 
		{
			@Override
			public void run() 
			{
				synchronized(Statics.INIT_OK_LOCK)
				{
					Intent intent = null;
					// 对账
					if(mBusiType == BUSI_SELFSERVICE_CHECK_DAILY) 
					{
						intent = new Intent(BusiSelfService.this, BusiCheck.class);
					}
					// 业务更新
					else if(mBusiType == BUSI_SELFSERVICE_BUSI_UPDATE)
					{
						intent = new Intent(BusiSelfService.this, BusiUpdate.class);
					}
					// 收件箱
					else if(mBusiType == BUSI_SELFSERVICE_MAILBOX)
					{
						intent = new Intent(BusiSelfService.this, BusiMailBox.class);
					}
					// 网点信息查询
					else if(mBusiType == BUSI_SELFSERVICE_SHOPINFO) 
					{
						intent = new Intent(BusiSelfService.this, BusiShopInfo.class);
					}
					// 终端软件升级
					else if(mBusiType == BUSI_SELFSERVICE_SOFT_UPDATE)
					{
						intent = new Intent(BusiSelfService.this, BusiSoftUpdate.class);
					}
					// 电子卡卡密打印中心
					else if(mBusiType == BUSI_SELFSERVICE_ECARD_INQUIRY) 
					{
						intent = new Intent(BusiSelfService.this, BusiEcardInquiry.class);
					}
					// 发票打印
					else if(mBusiType == BUSI_SELFSERVICE_INVOICE_PRINT)
					{
						intent = new Intent(BusiSelfService.this, BusiInvoicePrint.class);
					}
					startActivity(intent);
				}
			}
		}, 300);
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		DialogUtil.init(this);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			FrameActivity.mHandler.obtainMessage(FrameActivity.FINISH).sendToTarget();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
