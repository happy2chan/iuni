package com.sunnada.baseframe.activity.xhrw;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.activity.busiaccept.base.SimpleTextWatcher;
import com.sunnada.baseframe.bean.IdentifyMsg;
import com.sunnada.baseframe.bean.Productzifei;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.PickNetBusiness;
import com.sunnada.baseframe.dialog.CallBackChildHome;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.dialog.SearchBluetoothDialog;
import com.sunnada.baseframe.identify.IdentifyScan;
import com.sunnada.baseframe.ui.MyCustomButton;
import com.sunnada.baseframe.util.ARGBUtil;
import com.sunnada.baseframe.util.ButtonGroupUtil;
import com.sunnada.baseframe.util.ButtonGroupUtil.ButtonGroupClickListener;
import com.sunnada.baseframe.util.ButtonGroupUtil.ViewGetter;
import com.sunnada.baseframe.util.ButtonUtil;
import com.sunnada.baseframe.util.InputRules;
import com.sunnada.baseframe.util.PictureShowUtils;
import com.sunnada.baseframe.util.FileOpr;
import com.sunnada.baseframe.util.StringUtil;

public class PickNetWorkBusinessIdinfoActivity extends DgActivity implements OnClickListener, ViewGetter
{
	public  int			        ICCID_LENGTH        = 19;
	private TextView			mTvSelectedNum		= null; // 导航栏被选中号码
	private TextView			mTvSelectedPackage	= null; // 导航栏被选中套餐
	
	private TextView			mTvIdType;					// 证件类型
	private View				mLayCover;					// 覆盖屏幕
	private ListView			mLvIdType;					// 证件类型
	
	private EditText			mEtDate				= null; // 有效期
	private EditText			mEtName				= null; // 姓名
	private EditText			mEtIdNum			= null; // 证件号码
	private EditText			mEtAddress			= null; // 地址
	private EditText			mEtIccid			= null; // 空卡卡号
	
	private ButtonGroupUtil		util;                       // 资费组
	private ButtonGroupUtil		sexUtil;                    // 性别组
	
	private Button				mBtnReadIccid		= null; // 读取空卡信息
	private Button              mBtnPrevious;               // 回退按钮
	private LinearLayout        mLayBack;                   // 回退LinearLayout
	private MyCustomButton      mBtnPre;                    // 上一步
	private MyCustomButton	    mBtnNext;                   // 开户
	
	private ImageView			mIvIdentifyScan1	= null; // 身份证扫描正面
	private ImageView			mIvIdentifyScan2	= null; // 身份证扫描反面
	
	private ImageView			mIvTipName;
	private ImageView			mIvTipDate;
	private ImageView			mIvTipIdNum;
	private ImageView			mIvTipAddress;
	private ImageView			mIvTipIccid;
	
	
	private RadioGroup          mRgCard;
	private RadioButton         mRbCard;                    // 空卡
	private RadioButton         mRbWhiteCard;               // 白卡
	private TextView            mTvCard;                    // 空卡文本
	private TextView            mTvWhiteCard;               // 白卡文本
	//private RelativeLayout   	mRelativeLayout1;
	//private RelativeLayout      mRelativeLayout2;
	
	private String				sPicFolder			= "";	// 暂存图片文件的文件夹名
	private String				filePath1			= null;
	private String				filePath2			= null;
	private String				mIdType				= "02"; // 证件类型编码
	private List<Object>		mListIdType;                // 证件编码列表

	private PickNetBusiness     mPickNetBusiness    = null;
	private Context				context;
	
	// 性别组Id
	private int[]               mSexUtilIds         = {R.id.btn_sex_male, R.id.btn_sex_female};
	// 资费组Id
	private int[]               mUtilIds            = {R.id.btn_standard, R.id.btn_all, R.id.btn_half};

	// 使用ButtonGroupUtil改变字体颜色设置
	private final int[][]       mFontColor          = {{0xff,0xff,0xff,0xff,0xff},{0xff,0x6f,0xae,0xc8}};
	private final int[][]       mFontColorOrange    = {{0xff,0xff,0xff,0xff,0xff},{0xff,0xff,0x96,0x00}};
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_picknetwork_business_idinfo2_orange);
		}
		else
		{
			setContentView(R.layout.lay_picknetwork_business_idinfo2);
		}

		// 初始化控件
		initView();
		// 注册监听
		setListenner();
		// 初始化数据
		initData();
		// 初始化显示
		// initShow();
	}

	private void initView()
	{
		mPickNetBusiness = (PickNetBusiness) FrameActivity.mBaseBusiness;
		context = this;
		
		// 回退LinearLayout
		mLayBack = (LinearLayout)this.findViewById(R.id.layBack);
		// 回退按钮
		mBtnPrevious = (Button) findViewById(R.id.btn_previous);
		
		// 导航栏
		mTvSelectedNum = (TextView) findViewById(R.id.tv_selected_num);
		mTvSelectedPackage = (TextView) findViewById(R.id.tv_selected_package);
		
		// 证件类型
		// mLayIdType = findViewById(R.id.layIdType);
		mTvIdType  = (TextView) findViewById(R.id.tvIdType);
		mLvIdType  = (ListView) findViewById(R.id.listIdType);
		mLayCover  = findViewById(R.id.lay_cover);
		
		// 用户信息
		mEtDate = (EditText) findViewById(R.id.et_id_date);
		mEtName = (EditText) findViewById(R.id.et_id_name);
		mEtIdNum = (EditText) findViewById(R.id.et_id_num); 
		mEtAddress = (EditText) findViewById(R.id.et_id_address);
		
		// 正反面拍照
		mIvIdentifyScan1 = (ImageView) findViewById(R.id.iv_id_1);
		mIvIdentifyScan2 = (ImageView) findViewById(R.id.iv_id_2);
		
		// 读卡相应控件
		mEtIccid = (EditText) findViewById(R.id.et_iccid);
		// 不支持手动输入
		mEtIccid.setEnabled(true);
		// 限制输入位数
		mEtIccid.setFilters(new InputFilter[] { new InputFilter.LengthFilter(19)});  
		
		mBtnReadIccid = (Button) findViewById(R.id.btn_read_iccid);
		
		mIvTipAddress = (ImageView) findViewById(R.id.iv_tip_address);
		mIvTipDate    = (ImageView) findViewById(R.id.iv_tip_date);
		mIvTipIccid   = (ImageView) findViewById(R.id.iv_tip_iccid);
		mIvTipIdNum   = (ImageView) findViewById(R.id.iv_tip_idnum);
		mIvTipName    = (ImageView) findViewById(R.id.iv_tip_name);
		
		// 初始化性别组
		try
		{
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				sexUtil = ButtonGroupUtil.createBean(this, new int[]
						{ R.id.btn_sex_male, R.id.btn_sex_female }, new String[]
						{ "男", "女" }, R.drawable.btn_tip_select, R.drawable.btn_tip_selected,
						mFontColor);
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				sexUtil = ButtonGroupUtil.createBean(this, new int[]
						{ R.id.btn_sex_male, R.id.btn_sex_female }, new String[]
						{ "男", "女" }, R.drawable.btn_tip_select_orange, R.drawable.btn_tip_selected_orange, 
						mFontColorOrange);
			}
			sexUtil.setSelected(0);
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}

		// 初始化资费组
		try
		{
			String[] strcode = new String[3];
			String[] strtext = new String[3];
			for (int i = 0; i < PickNetBusiness.productZifei.size(); i++)
			{
				Productzifei pz = PickNetBusiness.productZifei.get(i);
				strcode[i] = pz.getCode() + "";
				strtext[i] = pz.getName();
			}
			if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
			{
				util = ButtonGroupUtil.createBean(this, new int[]
						{ R.id.btn_standard, R.id.btn_all, R.id.btn_half }, strcode, strtext, 
						R.drawable.btn_tip_select, R.drawable.btn_tip_selected, mFontColor);
			}
			else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
			{
				util = ButtonGroupUtil.createBean(this, new int[]
						{ R.id.btn_standard, R.id.btn_all, R.id.btn_half }, strcode, strtext, R.drawable.btn_tip_select_orange,
								R.drawable.btn_tip_selected_orange, mFontColorOrange);
			}
			util.setSelected(0);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		mRgCard = (RadioGroup) this.findViewById(R.id.rg_card);
		mRbCard = (RadioButton) this.findViewById(R.id.rb_card);
		mRbWhiteCard = (RadioButton) this.findViewById(R.id.rb_whitecard);
		mTvCard = (TextView) this.findViewById(R.id.tv_card);
		mTvWhiteCard = (TextView) this.findViewById(R.id.tv_whitecard);
		
		// 上一步按钮
		mBtnPre = (MyCustomButton) findViewById(R.id.btn_pre);
		mBtnPre.setTextViewText1("返回");
		mBtnPre.setImageResource(R.drawable.btn_back);
		mBtnPre.setOnTouchListener(this);
		
		// 下一步按钮
		mBtnNext = (MyCustomButton) findViewById(R.id.btn_next);
		mBtnNext.setTextViewText1("确定");
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mBtnNext.setImageResource(R.drawable.btn_custom_check_orange);
		}
		else
		{
			mBtnNext.setImageResource(R.drawable.btn_custom_check);
		}
		mBtnNext.setOnTouchListener(this);
		
		/*
		mRelativeLayout1 = (RelativeLayout) this.findViewById(R.id.relativeLayout1);
		mRelativeLayout2 = (RelativeLayout) this.findViewById(R.id.relativeLayout2);
		mRelativeLayout1.getBackground().setAlpha(30);
		mRelativeLayout2.getBackground().setAlpha(30);
		*/
	}

	private void setListenner()
	{
		//mLayIdType.setOnClickListener(this);
		mLayCover.setOnClickListener(this);
		
		mBtnPre.setOnClickListener(this);
		mBtnNext.setOnClickListener(this);
		mBtnPrevious.setOnClickListener(this);
		mLayBack.setOnClickListener(this);
		
		mIvIdentifyScan1.setOnClickListener(this);
		mIvIdentifyScan2.setOnClickListener(this);
		mBtnReadIccid.setOnClickListener(this);
		
		mLvIdType.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				TextView tv = (TextView) view.findViewById(R.id.tv_text);
				mTvIdType.setText(tv.getText().toString());
				mIdType = "0"+Integer.toString((position+2));
				
				mLayCover.setVisibility(View.GONE);
			}
		});
		
		// 性别Util
		sexUtil.setClickListener(new ButtonGroupClickListener() 
		{
			@Override
			public void onClick(int previousId, int newId) 
			{
				for(int i = 0;i < mSexUtilIds.length;i++)
				{
					if(mSexUtilIds[i] == newId)
					{
						mPickNetBusiness.mSelectSexUtilPos = i;
						break;
					}
				}
			}
		});
		
		// 资费Util 
		util.setClickListener(new ButtonGroupClickListener() 
		{
			@Override
			public void onClick(int previousId, int newId) 
			{
				for(int i = 0;i < mUtilIds.length;i++)
				{
					if(mUtilIds[i] == newId)
					{
						mPickNetBusiness.mSelectUtilPos = i;
						break;
					}
				}
			}
		});
		// 对话框输入监听，在对话框后面提示输入是否正确
		addEditextListernner();
		
		mRgCard.setOnCheckedChangeListener(new OnCheckedChangeListener() 
		{
			@Override
			public void onCheckedChanged(RadioGroup group, int checkid) 
			{
				if(checkid == R.id.rb_card)
				{
					selectCardType(0);
				}
				else if(checkid == R.id.rb_whitecard)
				{
					selectCardType(1);
				}
			}
		});
		
		mTvCard.setOnClickListener(this);
		mTvWhiteCard.setOnClickListener(this);
	}

	public void selectCardType(int type)
	{
		switch(type)
		{
		case 0:
			mPickNetBusiness.openCardType = 0x00;
			mRbCard.setChecked(true);
			mRbWhiteCard.setChecked(false);
			ICCID_LENGTH = 19;
			mEtIccid.setEnabled(true);
			mEtIccid.setText("");
			mEtIccid.setFilters(new InputFilter[] { new InputFilter.LengthFilter(19)});  
			break;
			
		case 1:
			mPickNetBusiness.openCardType = 0x01;
			mRbCard.setChecked(false);
			mRbWhiteCard.setChecked(true);
			ICCID_LENGTH = 20;
			mEtIccid.setEnabled(false);
			mEtIccid.setText("");
			mEtIccid.setFilters(new InputFilter[] { new InputFilter.LengthFilter(20)});
			break;
			
		default:
			break;
		}
	}
	
	// 对话框输入监听，在对话框后面提示输入是否正确
	private void addEditextListernner()
	{
		mEtName.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipName.setVisibility(View.GONE);
				}
				else
				{
					mIvTipName.setVisibility(View.VISIBLE);
					if (!InputRules.isNameValid(mIdType, s.toString()))
					{
						// mIvTipName.setImageResource(R.drawable.ic_error);
						mIvTipName.setImageDrawable(getResources().getDrawable(R.drawable.ic_error));
					} 
					else 
					{
						// mIvTipName.setImageResource(R.drawable.ic_correct);
						mIvTipName.setImageDrawable(getResources().getDrawable(R.drawable.ic_correct));
					}
				}
			}
		});
			
		mEtIdNum.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipIdNum.setVisibility(View.GONE);
				}
				else
				{
					mIvTipIdNum.setVisibility(View.VISIBLE);
					if (!InputRules.isNoValid(mIdType, s.toString()))
					{
						// mIvTipIdNum.setImageResource(R.drawable.ic_error);
						mIvTipIdNum.setImageDrawable(getResources().getDrawable(R.drawable.ic_error));

					} 
					else 
					{
						// mIvTipIdNum.setImageResource(R.drawable.ic_correct);
						mIvTipIdNum.setImageDrawable(getResources().getDrawable(R.drawable.ic_correct));
					}
				}
			}
		});
			
		mEtDate.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipDate.setVisibility(View.GONE);
				}
				else
				{
					mIvTipDate.setVisibility(View.VISIBLE);
					if (!InputRules.checkIDEndTime(s.toString()).equals(""))
					{
						// mIvTipDate.setImageResource(R.drawable.ic_error);
						mIvTipDate.setImageDrawable(getResources().getDrawable(R.drawable.ic_error));
					} 
					else 
					{
						// mIvTipDate.setImageResource(R.drawable.ic_correct);
						mIvTipDate.setImageDrawable(getResources().getDrawable(R.drawable.ic_correct));
					}
				}
			}
		});
			
		mEtAddress.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipAddress.setVisibility(View.GONE);
				}
				else
				{
					mIvTipAddress.setVisibility(View.VISIBLE);
					if (InputRules.isAddressValid(mIdType, s.toString()) != 0)
					{
						// mIvTipAddress.setImageResource(R.drawable.ic_error);
						mIvTipAddress.setImageDrawable(getResources().getDrawable(R.drawable.ic_error));
					} 
					else 
					{
						// mIvTipAddress.setImageResource(R.drawable.ic_correct);
						mIvTipAddress.setImageDrawable(getResources().getDrawable(R.drawable.ic_correct));
					}
				}
			}
		});
			
		mEtIccid.addTextChangedListener(new SimpleTextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				super.onTextChanged(s, start, before, count);
				if(InputRules.isNull(s.toString()))
				{
					mIvTipIccid.setVisibility(View.GONE);
				}
				else
				{
					mIvTipIccid.setVisibility(View.VISIBLE);
					if (s.length() != ICCID_LENGTH) 
					{
						// mIvTipIccid.setImageResource(R.drawable.ic_error);
						mIvTipIccid.setImageDrawable(getResources().getDrawable(R.drawable.ic_error));
					} 
					else 
					{
						// mIvTipIccid.setImageResource(R.drawable.ic_correct);
						mIvTipIccid.setImageDrawable(getResources().getDrawable(R.drawable.ic_correct));
					}
				}
			}
		});
	}
		
	// 初始化数据
	private void initData()
	{	
		mListIdType = new ArrayList<Object>();
		mListIdType.add("18位身份证");
		//mListIdType.add("驾驶证");
		//mListIdType.add("军官证");
		//mListIdType.add("护照");
		ArrayAdapter<Object> adapter = new ArrayAdapter<Object>(this, R.layout.lay_id_type_list_item,
				R.id.tv_text, mListIdType);
		mLvIdType.setAdapter(adapter);
		
		mTvIdType.setText(mListIdType.get(0).toString());
		resetDate();
	}

	// 重置输入数据
	public void resetDate()
	{
		// mIvIdentifyScan1.setImageResource(R.drawable.iv_id_1);
		// mIvIdentifyScan2.setImageResource(R.drawable.iv_id_2);
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			mIvIdentifyScan1.setImageDrawable(getResources().getDrawable(R.drawable.iv_id_1_orange));
			mIvIdentifyScan2.setImageDrawable(getResources().getDrawable(R.drawable.iv_id_2_orange));
		}
		else
		{
			mIvIdentifyScan1.setImageDrawable(getResources().getDrawable(R.drawable.iv_id_1));
			mIvIdentifyScan2.setImageDrawable(getResources().getDrawable(R.drawable.iv_id_2));
		}
		
		sPicFolder = FileOpr.getSdcardPath() + getString(R.string.identify_photo_path)
				+ String.valueOf(System.currentTimeMillis());
		filePath1 = sPicFolder + "/identify_1.jpg";
		filePath2 = sPicFolder + "/identify_2.jpg";
	}
	
	public void initShow()
	{
		if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			// 背景图 
			RelativeLayout relativeBg = (RelativeLayout) findViewById(R.id.relative_bg);
			relativeBg.setBackgroundResource(R.drawable.bg_main_orange);
			// 主题
			TextView textView1 = (TextView) findViewById(R.id.textView1);
			textView1.setTextColor(ARGBUtil.getArgb(2));
			// 导航条
			ImageView imageView1 = (ImageView) findViewById(R.id.imageView1);
			imageView1.setImageResource(R.drawable.step3_picknet_orange);
			// 证件类型下拉图标
			ImageView ivDown = (ImageView) findViewById(R.id.iv_down);
			ivDown.setImageResource(R.drawable.down_orange);
			
			// 左边UI显示设置
			TextView textView2 = (TextView) findViewById(R.id.textView2);
			textView2.setBackgroundResource(R.drawable.title_idinfo_orange);
			
			TextView textView10 = (TextView) findViewById(R.id.textView10);
			textView10.setBackgroundResource(R.drawable.title_extendinfo_orange);
			// 正反面拍照
			mIvIdentifyScan1.setImageResource(R.drawable.iv_id_1_orange);
			mIvIdentifyScan2.setImageResource(R.drawable.iv_id_2_orange);
			
			mRbCard.setBackgroundResource(R.drawable.ic_cardtype_selector_orange);
			mRbWhiteCard.setBackgroundResource(R.drawable.ic_cardtype_selector_orange);
			
		}
	}
	
	// 重置卡操作到默认操作
	public void resetCardOper()
	{
		mPickNetBusiness.openCardType = 0x00;
		ICCID_LENGTH = 19;
		mEtIccid.setEnabled(true);
		mEtIccid.setText("");
		mEtIccid.setFilters(new InputFilter[] { new InputFilter.LengthFilter(19)});
		mRbCard.setChecked(true);
	}
	
	@Override
	public void onClick(View v)
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 1000)) 
		{
			return;
		}
				
		Bundle bundle = this.getIntent().getBundleExtra("_data");
		Message msg = Message.obtain();

		switch (v.getId())
		{
			case R.id.iv_id_1: // 身份证扫描正面
				new Thread()
				{
					public void run()
					{
						final com.sunnada.baseframe.bean.IdentifyMsg msg = takePhoto(filePath1);
						if (msg != null)
						{
							final Bitmap bitmap2 = PictureShowUtils.decodeFile(filePath1, 206, 130);

							mIvIdentifyScan1.post(new Runnable()
							{
								@Override
								public void run()
								{
									if (!StringUtil.isEmptyOrNull(msg.identify_no))
									{
										mEtIdNum.setText(msg.identify_no);
									}
									if (!StringUtil.isEmptyOrNull(msg.sex))
									{
										if ("01".equals(msg.sex))
										{
											sexUtil.setSelected(1);
										}
										else
										{
											sexUtil.setSelected(0);
										}
									}
									if (!StringUtil.isEmptyOrNull(msg.name))
									{
										mEtName.setText(msg.name);
									}
									if (!StringUtil.isEmptyOrNull(msg.end_time))
									{
										mEtDate.setText(msg.end_time);
									}
									if (!StringUtil.isEmptyOrNull(msg.address))
									{
										mEtAddress.setText(msg.address);
									}
									if (bitmap2 != null)
									{
										mIvIdentifyScan1.setImageDrawable(new BitmapDrawable(bitmap2));
										// mIvIdentifyScan1.setImageBitmap(bitmap2);
									}
								}
							});
						}
					}
				}.start();
				break;
				
			case R.id.iv_id_2:// 身份证扫描反面
				new Thread()
				{
					public void run()
					{
						final com.sunnada.baseframe.bean.IdentifyMsg msg = takePhoto(filePath2);
						if (msg != null)
						{
							final Bitmap bitmap2 = PictureShowUtils.decodeFile(filePath2, 206, 130);

							mIvIdentifyScan2.post(new Runnable()
							{
								@Override
								public void run()
								{
									if (msg != null)
									{
										if (!StringUtil.isEmptyOrNull(msg.identify_no))
										{
											mEtIdNum.setText(msg.identify_no);
										}
										if (!StringUtil.isEmptyOrNull(msg.sex))
										{
											if ("01".equals(msg.sex))
											{
												sexUtil.setSelected(1);
											}
											else
											{
												sexUtil.setSelected(0);
											}
										}
										if (!StringUtil.isEmptyOrNull(msg.name))
										{
											mEtName.setText(msg.name);
										}
										if (!StringUtil.isEmptyOrNull(msg.end_time))
										{
											mEtDate.setText(msg.end_time);
										}
										if (!StringUtil.isEmptyOrNull(msg.address))
										{
											mEtAddress.setText(msg.address);
										}
										if (bitmap2 != null)
										{
											mIvIdentifyScan2.setImageDrawable(new BitmapDrawable(bitmap2));
											// mIvIdentifyScan2.setImageBitmap(bitmap2);
										}
									}
								}
							});
						}
					}
				}.start();
				break;
				
			case R.id.btn_pre:
				backToPrevious();
				break;
				
			case R.id.btn_next:
				if(Statics.IS_DEBUG)
				{
					bundle.putString("className", PickNetWorkNetAccessPermitsActivity.class.getName());
					msg.what = FrameActivity.DIRECTION_NEXT;
					msg.setData(bundle);
					FrameActivity.mHandler.sendMessage(msg);
					return;
				}
				
				String checkResult = checkAllInput();
				
				if(StringUtil.isEmptyOrNull(checkResult))
				{
					getData();

					bundle.putString("className", PickNetWorkNetAccessPermitsActivity.class.getName());
					bundle.putString("name", mEtName.getText().toString());
					bundle.putString("id_no", mEtIdNum.getText().toString());
					msg.what = FrameActivity.DIRECTION_NEXT;
					msg.setData(bundle);
					FrameActivity.mHandler.sendMessage(msg);
				}
				else
				{
					back(null, "温馨提示", "以下开户信息未正确填写：\n"+checkResult,
							"确定", null, false, false, false);
					return;
				}
				break;

			case R.id.btn_read_iccid:
				if (Statics.IS_DEBUG)
				{
					DialogUtil.showProgress("正在读取卡号,请稍候");
					new Thread()
					{
						public void run()
						{
							try
							{
								final String iccid = equipmentService.readIccid();
								runOnUiThread(new Runnable()
								{
									public void run()
									{
										mEtIccid.setText(iccid);
									}
								});
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
							DialogUtil.closeProgress();
						};
					}.start();
				}
				else
				{
					prepareReadIccid();
				}
				break;
				
			// 证件类型
			case R.id.layIdType:
				mLayCover.setVisibility(View.VISIBLE);
				break;
				
			// 覆盖全局
			case R.id.lay_cover:
				mLayCover.setVisibility(View.GONE);
				break;
				
			case R.id.btn_previous:
				back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
				break;
			
			case R.id.layBack:
				back(new CallBackChildHome(), null, null, "确定", "取消", true, false, false);
				break;
			
			case R.id.tv_card:
				selectCardType(0);
				break;
			
			case R.id.tv_whitecard:
				selectCardType(1);
				break;
			default:
				break;
		}
	}

	public void prepareReadIccid()
	{
		DialogUtil.showProgress("正在读取卡号,请稍候");
		readIccid();
	}
	
	public void readIccid()
	{
		new Thread()
		{
			@Override
			public void run()
			{
				// 检测蓝牙设备
				if(equipmentService.handshake() == false) 
				{
					DialogUtil.closeProgress();
					DialogUtil.MsgBox("请连接蓝牙设备", 
							"检测到当前无蓝牙读卡设备, 是否连接蓝牙?", 
							"立即搜索", 
							Statics.MSG_CONNECT_BLUETOOTH, 
							"取消", 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							Statics.MSG_CONNECT_BLUETOOTH_CANCEL, 
							mHandler);
					return;
				}
				
				try
				{
					int iswhiteCard = equipmentService.isWhiteCard((byte) 1);
					// 白卡
					if (iswhiteCard == 1)
					{
						if(mPickNetBusiness.openCardType == 0x00)
						{
							DialogUtil.closeProgress();
							DialogUtil.MsgBox("提示信息", "当前为成卡开卡，请插入成卡！");
							return;
						}
						mPickNetBusiness.openCardType = 1;
						final String iccid = equipmentService.readIccid();
						mPickNetBusiness.iccid = iccid;
						if (iccid != null)
						{
							runOnUiThread(new Runnable()
							{
								public void run()
								{
									DialogUtil.closeProgress();
									ICCID_LENGTH = 20;
									mEtIccid.setEnabled(false);
									mEtIccid.setFilters(new InputFilter[] { new InputFilter.LengthFilter(20)});  
									mRbWhiteCard.setChecked(true);
									mEtIccid.setText(iccid);
									Toast.makeText(context, "当前为白卡，读卡成功", Toast.LENGTH_SHORT).show();
								}
							});
						}
						else
						{
							runOnUiThread(new Runnable()
							{
								public void run()
								{
									DialogUtil.closeProgress();
									back(null, null, "读取失败", "确定", null, false, false, false);
								}
							});
						}
					}
					else if (iswhiteCard == 2)
					{
						if(mPickNetBusiness.openCardType == 0x01)
						{
							DialogUtil.closeProgress();
							DialogUtil.MsgBox("提示信息", "当前为白卡开卡，请插入白卡！");
							return;
						}
						// 成卡
						mPickNetBusiness.openCardType = 0;
						final String iccid = equipmentService.readIccid();
						mPickNetBusiness.iccid = iccid.substring(0, 19);
						
						if (iccid != null)
						{
							runOnUiThread(new Runnable()
							{
								public void run()
								{
									DialogUtil.closeProgress();
									mRbCard.setChecked(true);
									mEtIccid.setEnabled(true);
									mEtIccid.setFilters(new InputFilter[] { new InputFilter.LengthFilter(19)});  
									ICCID_LENGTH = 19;
									mEtIccid.setText(mPickNetBusiness.iccid);
									Toast.makeText(context, "当前为成卡，读卡成功", Toast.LENGTH_SHORT).show();
								}
							});
						}
					}
					else
					{
						runOnUiThread(new Runnable()
						{
							public void run()
							{
								DialogUtil.closeProgress();
								resetCardOper();
								back(null, null, "读卡失败，请正确插入SIM卡，芯片朝上!", "确定", null, false, false, false);
							}
						});
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							DialogUtil.closeProgress();
							resetCardOper();
							back(null, null, "读取失败", "确定", null, false, false, false);
							//Toast.makeText(context, "当前为白卡，读卡成功", Toast.LENGTH_SHORT).show();
						}
					});
				}
			};
		}.start();
	}

	public void getData()
	{
		// 证件类型
		mPickNetBusiness.cust_type = mIdType;
		
		// 用户信息
		mPickNetBusiness.IdentityEndtime = mEtDate.getText().toString();
		mPickNetBusiness.username = mEtName.getText().toString();
		mPickNetBusiness.Identity = mEtIdNum.getText().toString();//  证件号码
		mPickNetBusiness.address = mEtAddress.getText().toString();

		// 卡号
		mPickNetBusiness.iccid = mEtIccid.getText().toString();
		mPickNetBusiness.sPicFolder = sPicFolder;
		
		// 性别信息收集
		try
		{
			if (sexUtil.getValue().equals("男"))
			{
				mPickNetBusiness.sex = "02";
			}
			else
			{
				mPickNetBusiness.sex = "01";
			}
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}
		
		// 资费信息收集
		try
		{
			mPickNetBusiness.zife = Integer.parseInt(util.getValue());
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private String checkAllInput() 
	{
		StringBuffer sBuffer = new StringBuffer();
		sBuffer.append(checkUserInfoInput());
		sBuffer.append(checkMainInput());
		
		if(StringUtil.isEmptyOrNull(sBuffer.toString()))
		{
			return sBuffer.toString();
		}
		else if(sBuffer.toString().trim().endsWith("、"))
		{
			return sBuffer.toString().trim().substring(0, sBuffer.toString().trim().length() - 1)+"。";
		}
		else
		{
			return sBuffer.toString().toString().trim()+"。";
		}
	}
	
	// 主要的信息校验
	private String checkMainInput() 
	{
		StringBuffer sBuffer = new StringBuffer();

		if (mPickNetBusiness.openCardType == 0) 
		{
			if ("".equals(mEtIccid.getText().toString())) 
			{
				sBuffer.append("卡号");
			}
			else if (mEtIccid.getText().toString().length() < 19
					&& !mEtIccid.getText().toString().equals("")) 
			{
				sBuffer.append("卡号");
			}
		}
		else if(mPickNetBusiness.openCardType == 1)
		{
			if ("".equals(mEtIccid.getText().toString())) 
			{
				sBuffer.append("卡号");
			}
			else if (mEtIccid.getText().toString().length() < 20
					&& !mEtIccid.getText().toString().equals("")) 
			{
				sBuffer.append("卡号");
			}
		}
		return sBuffer.toString();
	}

	 // 为没连接时准备的应急Handler
	private String checkUserInfoInput() 
	{
		StringBuffer sBuffer = new StringBuffer();
		if(StringUtil.isEmptyOrNull(mEtDate.getText().toString()))
		{
			sBuffer.append("有效期、");
		}
		else
		{
			if (!InputRules.checkIDEndTime(mEtDate.getText().toString()).equals(""))
			{
				sBuffer.append("有效期、");
			} 
		}
		
		if (StringUtil.isEmptyOrNull(mEtName.getText().toString())) 
		{
			sBuffer.append("姓名、");
		}
		else
		{
			if (!InputRules.isNameValid(mIdType, mEtName.getText().toString()))
			{
				sBuffer.append("姓名、");
			} 
		}
		
		if (StringUtil.isEmptyOrNull(mEtIdNum.getText().toString())) 
		{
			sBuffer.append("证件号码、");
		} 
		else 
		{
			if (!InputRules.isNoValid(mIdType, mEtIdNum.getText().toString()))
			{
				sBuffer.append("证件号码、");
			} 
		}
		
		if (StringUtil.isEmptyOrNull(mEtAddress.getText().toString())) 
		{
			sBuffer.append("地址、");
		}
		else
		{
			if (InputRules.isAddressValid(mIdType, mEtAddress.getText().toString()) != 0)
			{
				sBuffer.append("、");
			} 
		}
		return sBuffer.toString();
	}
	
	private IdentifyMsg takePhoto(String path)
	{
		IdentifyScan photo = new IdentifyScan(this);
		int ret = photo.takePhoto(path);
		IdentifyMsg msg = null;
		if (ret == 1)
		{
			msg = photo.scanIdentifySigal(path);
		}
		return msg;
	}

	// 确定时的动作处理
	protected Handler mHandler = new Handler() 
	{
		public void handleMessage(Message message) 
		{
			switch (message.what) 
			{
			// 连接蓝牙设备
			case Statics.MSG_CONNECT_BLUETOOTH:
				SearchBluetoothDialog dialog = new SearchBluetoothDialog(PickNetWorkBusinessIdinfoActivity.this, mBluetoothClient, equipmentService, 
						mHandler, Statics.MSG_CONNECT_DONE);
				dialog.show();
				break;
				
			// 连接蓝牙设备完成
			case Statics.MSG_CONNECT_DONE:
				/*
				new Thread() 
				{
					public void run() 
					{
						prepareReadIccid();
					}
				}.start();
				*/
				break;
				
			// 取消蓝牙或蓝牙连接失败
			case Statics.MSG_CONNECT_BLUETOOTH_CANCEL:
				resetCardOper();
				break;
				
			default:
				break;
			}
		}
	};
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			backToPrevious();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private void backToPrevious()
	{
		// getData();
		Bundle bundle = getIntent().getBundleExtra("_data");
		bundle.putString("className", PickNetWorkPackageSelectActivity.class.getName());
		
		Message msg = Message.obtain();
		msg.what = FrameActivity.DIRECTION_PREVIOUS;
		msg.setData(bundle);
		FrameActivity.mHandler.sendMessage(msg);
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		Bundle bundle = intent.getBundleExtra("_data");

		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction == FrameActivity.DIRECTION_PREVIOUS)
		{
			return;
		}
		getIntent().putExtra("_data", bundle);
		String selectedNum = bundle.getString("num");
		mTvSelectedNum.setText(selectedNum);	
		mTvSelectedPackage.setText(setStepTwoTvShow());
		initData();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
		Bundle bundle = getIntent().getBundleExtra("_data");

		if (bundle == null)
		{
			return;
		}
		int direction = bundle.getInt("direction");
		if (direction != FrameActivity.DIRECTION_NEXT)
		{
			return;
		}
		
		String selectedNum = bundle.getString("num");
		mTvSelectedNum.setText(selectedNum);	
		mTvSelectedPackage.setText(setStepTwoTvShow());
	}

	// 导航第二步显示内容
	public String setStepTwoTvShow()
	{
		String show = "";
		if(!StringUtil.isEmptyOrNull(mPickNetBusiness.packageName))
		{
			if(!StringUtil.isEmptyOrNull(mPickNetBusiness.mContractName))
			{
				show = mPickNetBusiness.packageName+","+mPickNetBusiness.mContractName;
			}else
			{
				show = mPickNetBusiness.packageName;
			}
		}
		
		if(show != null)
		{
			if(show.length() > 12)
			{
				show = show.substring(0,12)+"...";
			}
		}
		else
		{
			show = "";
		}
		return show;
	}
	
	@Override
	protected void onDestroy() 
	{
		System.gc();
		super.onDestroy();
	}

	@Override
	public void onInitSuccess()
	{
		Log.v("xxxxxx","成功");
	}

	@Override 
	public void onInitFail()
	{
	}
}
