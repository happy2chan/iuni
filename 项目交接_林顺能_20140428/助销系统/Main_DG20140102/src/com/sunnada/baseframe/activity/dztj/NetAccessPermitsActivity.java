package com.sunnada.baseframe.activity.dztj;

import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunnada.baseframe.activity.DgActivity;
import com.sunnada.baseframe.activity.FrameActivity;
import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.business.Business_0E;
import com.sunnada.baseframe.dialog.DialogUtil;
import com.sunnada.baseframe.ui.MyCustomButton;

public class NetAccessPermitsActivity extends DgActivity implements OnClickListener
{
	private Button			mBtnCheck;
	private View			mLayBack;
	private Button 			mBtnPrevious;
	private MyCustomButton	mBtnNetWrite;
	private TextView		mTvContent;
	private boolean			mIsCheck	= false;
	private LinearLayout	mLayCheck	= null;
	private Business_0E		mHaoka		= null;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Statics.SKIN_TYPE == Statics.SKIN_BLUE)
		{
			setContentView(R.layout.lay_netaccesspermits);
		}
		else if(Statics.SKIN_TYPE == Statics.SKIN_ORANGE)
		{
			setContentView(R.layout.lay_netaccesspermits_orange);
		}
		mHaoka = (Business_0E) FrameActivity.mBaseBusiness;
		// 初始化控件
		initViews();
	}

	// 初始化控件
	public void initViews()
	{
		mLayBack = findViewById(R.id.layBack);
		mLayBack.setOnClickListener(this);
		mBtnPrevious = (Button) findViewById(R.id.btn_previous);
		mBtnPrevious.setOnClickListener(this);
		mLayCheck = (LinearLayout) findViewById(R.id.lay_check);
		mLayCheck.setOnClickListener(this);
		
		mBtnNetWrite = (MyCustomButton) findViewById(R.id.net_write);
		mBtnNetWrite.setTextViewText1("签       名");
		mBtnNetWrite.setImageResource(R.drawable.net_write);
		mBtnNetWrite.setOnTouchListener(this);
		mBtnNetWrite.setOnClickListener(this);
		
		mTvContent = (TextView) findViewById(R.id.tv_content);
		// 入网协议
		mTvContent.setText(Business_0E.mStrNetProtocol);

		mBtnCheck = (Button) findViewById(R.id.bt_check);
		mBtnCheck.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
			// 点击打钩框或者文字时都相应
			case R.id.lay_check:
			case R.id.bt_check:
				if (mIsCheck == true)
				{
					mBtnCheck.setBackgroundDrawable(getResources().getDrawable(R.drawable.bt_netcheck));
					mIsCheck = false;
				}
				else
				{
					mBtnCheck.setBackgroundDrawable(getResources().getDrawable(R.drawable.bt_netchecked));
					mIsCheck = true;
				}
				break;
			
			// 签名
			case R.id.net_write:
				write();
				break;
				
			// 返回
			case R.id.btn_previous:
			case R.id.layBack:
				exit();
				break;
				
			default:
				break;
		}
	}

	// 退出
	private void exit()
	{
		DialogUtil.MsgBox("温馨提示", Statics.EXIT_TIP, 
		"确定", new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				new Thread()
				{
					@Override
					public void run()
					{
						mHaoka.bll0E16();// 通知服务器释放资源
					}
				}.start();
				Message msg = new Message();
				msg.what = FrameActivity.FINISH;
				FrameActivity.mHandler.sendMessage(msg);
			}
		}, "取消", null , null);
	}

	// 签名
	private void write()
	{
		final Bundle bundle = this.getIntent().getBundleExtra("_data");
		final Message msg = new Message();
		if (Statics.IS_DEBUG)
		{
			bundle.putString("className", SignatureActivity.class.getName());
			msg.what = FrameActivity.DIRECTION_NEXT;
			msg.setData(bundle);
			FrameActivity.mHandler.sendMessage(msg);
			return;
		}
		if (mIsCheck == true)
		{
			DialogUtil.showProgress("正在获取费用,请稍候...");
			System.out.println("协议界面的haoka:" + mHaoka);
			new Thread()
			{
				@Override
				public void run()
				{
					try
					{
						if(mHaoka.bll0E05())
						{
							DialogUtil.closeProgress();
							bundle.putString("className", SignatureActivity.class.getName());
							msg.what = FrameActivity.DIRECTION_NEXT;
							msg.setData(bundle);
							FrameActivity.mHandler.sendMessage(msg);
						}
						else
						{
							runOnUiThread(new Runnable()
							{
								public void run()
								{
									DialogUtil.closeProgress();
									DialogUtil.MsgBox("温馨提示", mHaoka.getLastknownError(),
									"确定", new OnClickListener()
									{
										@Override
										public void onClick(View v)
										{
											Message msg = new Message();
											msg.what = FrameActivity.FINISH;
											FrameActivity.mHandler.sendMessage(msg);
										}
									}, "", null,
									new OnClickListener()
									{
										@Override
										public void onClick(View v)
										{
											Message msg = new Message();
											msg.what = FrameActivity.FINISH;
											FrameActivity.mHandler.sendMessage(msg);
										}
									});
									/*
									DialogUtil.showMessageAndDeal(mHaoka.getLastknownError(),
									new OnClickListener()
									{
										@Override
										public void onClick(View v)
										{
											Message msg = new Message();
											msg.what = FrameActivity.FINISH;
											FrameActivity.mHandler.sendMessage(msg);
										}
									});
									*/
								}
							});
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
						runOnUiThread(new Runnable()
						{
							public void run()
							{
								DialogUtil.closeProgress();
								DialogUtil.MsgBox("温馨提示", "获取费用失败,将退出当前流程！\n失败原因:[ 未知错误 ]", 
								"确定", new OnClickListener()
								{
									@Override
									public void onClick(View v)
									{
										Message msg = new Message();
										msg.what = FrameActivity.FINISH;
										FrameActivity.mHandler.sendMessage(msg);
									}
								}, 
								"", null, 
								new OnClickListener()
								{
									@Override
									public void onClick(View v)
									{
										Message msg = new Message();
										msg.what = FrameActivity.FINISH;
										FrameActivity.mHandler.sendMessage(msg);
									}
								});
								/*
								DialogUtil.showMessageAndDeal("获取费用失败,将退出当前流程.失败原因:[ 未知错误 ]",
								new OnClickListener()
								{
									@Override
									public void onClick(View v)
									{
										Message msg = new Message();
										msg.what = FrameActivity.FINISH;
										FrameActivity.mHandler.sendMessage(msg);
									}
								});
								*/
							}
						});
					}
				};
			}.start();
		}
		else
		{
			DialogUtil.MsgBox("温馨提示", "同意入网协议后方可继续开户！");
		}
	}
	
	@Override
	public void onInitSuccess()
	{
	}

	@Override
	public void onInitFail()
	{
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			exit();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	protected void onResume()
	{
		super.onResume();
		DialogUtil.init(this);
		mBtnCheck.setBackgroundDrawable(getResources().getDrawable(R.drawable.bt_netchecked));
		mIsCheck = true;
	}
	
	/*
	// 单击按钮监听
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		switch (v.getId())
		{
			case R.id.net_write:
				net_write.onTouch(event);
				break;

			default:
				break;
		}
		return false;
	}
	*/
}
