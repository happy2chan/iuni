package com.sunnada.baseframe.activity.xhrw;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.util.ButtonUtil;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RelativeLayout;

public class Busi_frame extends UILogic
{
	// 框架UI
	protected   ViewStub                    viewStub           = null;
	public RelativeLayout		            mLayMenu		   = null;
	public Button                           mBtnSwitchShow;
	public Button                           mBtnSwitchHide;
	
	public Busi_frame(NumberSelectActivity2 numberSelect) 
	{
		super(numberSelect);
		//this.p_home = BackType.TRACE_HOME;
	}
	
	public void go() 
	{
		goLayout(R.layout.busi_frame);
		initViews();
	}
	
	// 初始化控件
	public void initViews() 
	{
		mLayMenu = (RelativeLayout) mNumberSelect.findViewById(R.id.lay_menu);
		mLayMenu.setVisibility(View.GONE);
		viewStub = (ViewStub) mNumberSelect.findViewById(R.id.lay_content);
		
		mBtnSwitchShow = (Button) mNumberSelect.findViewById(R.id.btn_switch_show);
		mBtnSwitchShow.setVisibility(View.GONE);
		mBtnSwitchHide = (Button) mNumberSelect.findViewById(R.id.btn_switch_hide);
		mBtnSwitchHide.setVisibility(View.GONE);
	}
	
	@Override 
	protected void MsgOK(int msgID) 
	{
		switch (msgID) 
		{
		    /*
			case UIHandle.BUSI_FRAME_GO:
				Class clazz = Class.forName(desClass);
				Constructor constructor = clazz.getConstructor(CUMiniActivity.class,MenuList.class); //构造函数参数列表的class类型
				mNumberSelect.goto_Layout((Busi_frame)constructor.newInstance(main_activity,menuBean)); //传参
				btn_back.setVisibility(View.VISIBLE);
				break;
				
	           case UIHandle.ISoperatingone:
	            // 页面跳转。通过发送消息的方式  
				UIHandle.MsgStr=String.valueOf(listviewposition);
	   			Message m = new Message(); // 创建消息对象
	   			m.what = UIHandle.BUSI_FRAME_GO; // 发送跳转消息
	   			UIHandle.m_Handle.sendMessage(m);
				break;
				*/
			default:
				super.MsgOK(msgID);
				break;
		}
	}
	
	@Override 
	public void onClick(View v)
	{
		// 防止按钮多次触发
		if(ButtonUtil.isFastDoubleClick(v.getId(), 500)) 
		{
			return;
		}
	}
}
