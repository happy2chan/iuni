package com.sunnada.baseframe.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

// 输入规则的校验
public class InputRules 
{	
	private static String[] mSimplePwds = new String[] 
	{
		"000000", "111111", "222222", "333333",
		"444444", "555555", "666666", "777777", 
		"888888" ,"999999", "123456", "234567", 
		"345678", "456789", "567890", "012345",
		"987654", "876543", "765432", "654321", 
		"543210"
	};
	
	// 判断是否简单密码
	public static boolean isSimple(String pwd) 
	{
		for(int i=0; i<mSimplePwds.length; i++)
		{
			if(pwd.equals(mSimplePwds[i]))
			{
				return true;
			}
		}
		return false;
	}
	
	// 判断字符串是否为空
	public static boolean isNull(String s)
	{
		if(s == null || s.equals(""))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	// SQL防注入
	public static boolean hasSQLKey(String valstr, boolean checkSymbol)
	{
		// 检测特殊字符
		// 暂时考虑反斜杠
		if (checkSymbol) {
			if (valstr.indexOf("\\") != -1) 
			{
				return true;
			}
		}

		// 检测SQL关键字
		if (valstr.indexOf("select") != -1 || valstr.indexOf("insert") != -1
				|| valstr.indexOf("update") != -1
				|| valstr.indexOf("delete") != -1
				/*
				 * || valstr.indexOf("and") != -1 || valstr.indexOf("or") != -1
				 * || valstr.indexOf("join") != -1 || valstr.indexOf("union") !=
				 * -1 || valstr.indexOf("truncate") != -1
				 */
				|| valstr.indexOf("drop") != -1
				|| valstr.indexOf("SELECT") != -1
				|| valstr.indexOf("INSERT") != -1
				|| valstr.indexOf("UPDATE") != -1
				|| valstr.indexOf("DELETE") != -1
				/*
				 * || valstr.indexOf("and") != -1 || valstr.indexOf("or") != -1
				 * || valstr.indexOf("join") != -1 || valstr.indexOf("union") !=
				 * -1 || valstr.indexOf("truncate") != -1
				 */
				|| valstr.indexOf("DROP") != -1)
		{
			return true;
		}
		return false;
	}

	// 判断姓名是否合法
	public static boolean isNameValid(String type, String val)
	{
		if (!"08".equals(type)) 
		{
			return (val.matches("[\u4E00-\u9FA5.·()（）]+") && getChineseCount(val) >= 2);
		}
		else 
		{
			try 
			{
				// 判断是否有SQL字符
				if (hasSQLKey(val, true)) 
				{
					return false;
				}
				return val.getBytes("GBK").length >= 3 && !val.matches("\\d*");
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				return false;
			}
		}
	}

	// 判断证件号码是否合法
	public static boolean isNoValid(String type, String val)
	{
		if ("01".equals(type) || "02".equals(type) || "0C".equals(type))// 身份证、户口本
		{
			return check18IdentifyID(val);
		}  
		else if ("0A".equals(type)) // 港澳居民往来内地通行证
		{
			return val.matches("^[H,M](\\d{8}|\\d{10})");
		}
		else if ("0B".equals(type)) // 台湾居民来往大陆通行证
		{
			return val.matches("(\\d{10}[(][0-9a-zA-Z][)])|(\\d{10}[（][0-9a-zA-Z][）])|(\\d{8})");
		} 
		else if ("04".equals(type) || "09".equals(type))// 军人身份证件、武装警察身份证件
		{
			return val.matches("[0-9a-zA-Z]{6,}"); 
		} 
		else if ("08".equals(type)) // 护照
		{
			// 判断是否有SQL字符 
			if (hasSQLKey(val, true))
			{
				return false;
			}
			return val.matches("[0-9a-zA-Z]{6,}");
		}
		return true;
	}

	// 检测身份证合法性
	public static boolean check18IdentifyID(String identifyID) 
	{
		long lSumQT = 0;
		int[] R = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 }; // 加权因子
		char[] sChecker = new char[] { '1', '0', 'X', '9', '8', '7', '6', '5',
				'4', '3', '2' };

		// 检验长度
		if (18 != identifyID.length()) 
		{
			System.out.println("长度不符合....");
			return false;
		}

		// 校验数字
		for (int i = 0; i < 18; i++)
		{
			char c = identifyID.charAt(i);
			if ((c < 48 || c > 57) && !(('X' == c || 'x' == c) && 17 == i)) 
			{
				System.out.println("存在非法字母...");
				return false;
			}
		}
		// 验证最末的校验码
		for (int i = 0; i <= 16; i++)
		{
			lSumQT += (identifyID.charAt(i) - 48) * R[i];
		}

		if (sChecker[(int) (lSumQT % 11)] != identifyID.charAt(17)) 
		{
			System.out.println("校验码不符合...");
			return false;
		}
		return true;
	}

	// 判断地址是否合法  0 合法     1 含sql特殊字符   2 不合法
	public static int isAddressValid(String type, String val) 
	{
		// 防SQL注入
		if (hasSQLKey(val, true)) 
		{
			return 1;
		}
		//
		if ("01".equals(type) || "02".equals(type) || "0C".equals(type)) // 身份证、户口本
		{
			if (getChineseCount(val) < 8)
			{
				return 2;
			}
			return 0;
		} 
		else if ("0A".equals(type) || "04".equals(type) || "09".equals(type))// 港澳居民往来内地通行证、军人身份证件、武装警察身份证件
		{
			if (getChineseCount(val) < 8) 
			{
				return 2;
			}
			return 0;
		} 
		else if ("0B".equals(type)) // 台湾居民来往大陆通行证
		{
			if (getChineseCount(val) < 3)
			{
				return 2;
			}
			return 0;
		} 
		else if ("08".equals(type))  // 护照
		{
			if (getChineseCount(val) < 2) 
			{
				return 2;
			}
			return 0;
		}
		return 2;
	}

	// 判断输入是否为汉字
	public static boolean isChinese(char a)
	{
		int v = (int) a;
		return (v >= 19968 && v <= 171941);
	}

	//  获取中文字数
	public static int getChineseCount(String s)
	{
		int count = 0;
		if (null == s || "".equals(s.trim()))
			return count;
		String[] cha = s.split("");
		for (String str : cha) 
		{
			if (str.matches("[\u4E00-\u9FA5]")) 
			{
				count++;
			}
		}
		return count;
	}
	
	// 检验身份证有效期
	public static String checkIDEndTime(String endtime) 
	{
		if(endtime.equals(""))
		{
			return "日期为空";
		}
		
		if (endtime.length() != 8)
		{
			return "有效期输入有误";
		}
		
		Date date = Calendar.getInstance().getTime();
		int  end  = Integer.parseInt(endtime);
		// 提取年、月、日
		int year  = end/10000;
		int month = (end/100)%100;
		int day   = end%100;
		
		int curYear = Integer.parseInt(new SimpleDateFormat("yyyy").format(date)); 
		int curMonth = Integer.parseInt(new SimpleDateFormat("MM").format(date));
		int curDay = Integer.parseInt(new SimpleDateFormat("dd").format(date));
		
		String 	info = String.format("当前时间为%04d-%02d-%02d, 您输入的时间为: %04d-%02d-%02d", 
				curYear, curMonth, curDay, year, month, day);
		System.out.println("info :" + "当前时间为: " + curYear + "-" + curMonth + "-" + curDay 
				+" 您输入的时间为: " + year + " - " + month + "-" + day);
		if(year < curYear)
		{
			return "身份有效期非法, 已过期\n"+info;
		}
		else if(year == curYear && month < curMonth)
		{
			return "身份有效期非法, 已过期\n"+info;
		}
		else if(year == curYear && month == curMonth && day <= curDay)
		{
			return "身份有效期非法, 已过期\n"+info;
		}
		
		// 检测年
		if(year < date.getYear()+1900 || year > (date.getYear()+1920))
		{
			return "身份有效期非法, 有效期一般为20年\n"+info;
		}
		// 检测月
		if(month < 1 || month > 12) 
		{
			return "时间格式非法(月格式非法)"+info;
		}
		// 检测日
		if(day < 1 || day > 31) 
		{
			return "时间格式非法(日格式非法)"+info;
		}
		// 小月
		if((month == 4 || month == 6 || month == 9 || month == 11) && day > 30) 
		{
			return "时间格式非法(日格式非法)"+info;
		}
		// 2月
		if(month == 2) 
		{
			if((year%4 == 0 && year%100 != 0) || year%400 == 0)
			{
				if(day > 29) 
				{
					return "时间格式非法(日格式非法)"+info;
				}
			}
			else 
			{
				if(day > 28)
				{
					return "时间格式非法(日格式非法)"+info;
				}
			}
		}
		return "";
	}
}
