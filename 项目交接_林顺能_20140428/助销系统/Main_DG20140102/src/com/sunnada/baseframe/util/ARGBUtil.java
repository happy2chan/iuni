package com.sunnada.baseframe.util;

import android.graphics.Color;

public class ARGBUtil 
{
	public  static  int[][] ARGB_VALUE  = 
	{
            {0xFF, 0x12, 0xA2, 0xFD},// ��ɫ1
            {0xFF, 0xFF, 0x96, 0x00},// ��ɫ2
            {0xFF, 0x40, 0x57, 0xEC},// ����ɫ3
            {0xFF, 0xE2, 0xEB, 0xF4},// ǳ��ɫ4
            {0xFF, 0xFF, 0xF2, 0x09},// ����ɫ5
            {0xFF, 0x71, 0xD0, 0x1E},// ��ɫ6
            {0xFF, 0xFF, 0xE6, 0xC7},// ǳ��ɫ7
            {0xFF, 0xBD, 0x71, 0x42},// ��ɫ8
            {0xFF, 0xF7, 0xD7, 0xAD},// �ػ�ɫ9
            {0xFF, 0xFF, 0xFF, 0xFF},// ��ɫ10
            {0xFF, 0xBC, 0x8C, 0x68},// ��ɫ11
            {0xFF, 0xFF, 0xEF, 0xD8} // �ۺ�ɫ12
    };
	
	public static int getArgb(int line)
	{
		return Color.argb(ARGB_VALUE[line - 1][0], 
				          ARGB_VALUE[line - 1][1], 
				          ARGB_VALUE[line - 1][2], 
				          ARGB_VALUE[line - 1][3]
				         );
	}
}
