package com.sunnada.baseframe.util;

public class FileConstant 
{
	// 10寸或10.1寸
	public static final String BASE_DIR                          = "/mnt/sdcard/mini/busi_show/";
	public static final String XHRW_DIR_BIG                      = BASE_DIR+"xhrw_big/";
	public static final String BUSIACCEPT_DIR_BIG                = BASE_DIR+"busiaccept_big/";
	public static final String SELFSERVICE_DIR_BIG               = BASE_DIR+"selfservice_big/";
	
	/*增值业务*/
	// 增值业务路径
	public static final String BUSIADDVALUE_BIG                  = BUSIACCEPT_DIR_BIG+"busiaddvalue/";
	public static final String MOBILEBILL_BIG                    = BUSIACCEPT_DIR_BIG+"mobilebill/";
	// 增值业务图片名称
	public static final String ADDVALUE_EXIT                     = "addvalue_exit.png";
	public static final String ADDVALUE_ORDER                    = "addvalue_order.png";
	public static final String ICON_ADDVALUE                     = "icon_addvalue.png";
	public static final String ICON_MOBILEBILL_QUERY             = "icon_mobilebill_query.png";
	public static final String MOBILEBILL_QUERY_ICON             = "mobilebill_query_icon.png";   

	/*自助服务*/
	// 自助服务路径
	public static final String BUSICHECK                         = SELFSERVICE_DIR_BIG+"busicheck/";
	public static final String BUSIMAILBOX                       = SELFSERVICE_DIR_BIG+"busimailbox/";
	public static final String BUSIUPDATE                        = SELFSERVICE_DIR_BIG+"busiupdate/";
	// 自助服务图标
	public static final String ICON_BUSI_CHECK                   = "icon_busi_check.png";
	public static final String ICON_MAILBOX                      = "icon_mailbox.png";
	public static final String ICON_BUSI_UPDATE                  = "icon_busi_update.png";
	
	/*选号入网*/
	// 选号入网路径
	public static final String XHRW_STEP1                        = XHRW_DIR_BIG+"step1/";
	public static final String XHRW_STEP2                        = XHRW_DIR_BIG+"step2/";
	public static final String XHRW_STEP3                        = XHRW_DIR_BIG+"step3/";
	// 选号入网图标
	// 第一步图标
	public static final String SELECTED_TELNUM_BG_SLIP           = "selected_telnum_bg_slip.png";
	public static final String TELNUM_BG_SLIP                    = "telnum_bg_slip.png";
	public static final String STEP1_PICKNET                     = "step1_picknet.png";
	// 第二步图标
	public static final String TITLE_FEE                         = "title_fee.png";
	public static final String TITLE_PLAN                        = "title_plan.png";
	public static final String TITLE_TYPE                        = "title_type.png";
	public static final String TITLE_ENDING                      = "title_ending.png";
	public static final String STEP2_PICKNET                     = "step2_picknet.png";
	// 第三步图标
	public static final String IC_CARD_SELECT                    = "ic_card_select.png";
	public static final String IC_CARD_SELECTED                  = "ic_card_selected.png";
	public static final String IV_ID_1                           = "iv_id_1.png";
	public static final String IV_ID_2                           = "iv_id_2.png";
	public static final String TITLE_IDINFO                      = "title_idinfo.png";
	public static final String TITLE_IDINFOTITLE_IDINFO          = "title_idinfotitle_idinfo.png";
	public static final String STEP3_PICKNET                     = "step3_picknet.png";
}
