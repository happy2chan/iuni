package com.sunnada.baseframe.util;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil
{
	public static final boolean isEmptyOrNull(String str)
	{
		return str == null || str.trim().length() <= 0 
				|| str.trim().equals("null") || str.trim().equals("NULL");
	}

	public static final boolean isEmptyOrZero(String str)
	{
		return isEmptyOrNull(str) || "0".equals(str);
	}
	
	
	/**
	 * 判断字符是否不为空
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str)
	{
		return !isEmptyOrNull(str);
	}
	
	
	/**
	 * 将格式为20120909121212转换为2012-09-09 12:12:12
	 * @param str
	 * @return
	 */
	public static String converDateString(String str)
	{
		if(isEmptyOrNull(str))
		{
			return "时间格式错误";
		}
		
		StringBuffer sb = new StringBuffer(str.trim());
		if(str.length()>3)
			sb.insert(4, '-');
		
		if(str.length()>6)
			sb.insert(7, '-');
		
		if(str.length()>8)
			sb.insert(10, ' ');
		
		if(str.length()>10)
			sb.insert(13, ':');

		if(str.length()>12)
		{
			sb.insert(16, ':');
		}
		
		return sb.toString();
	}
	
	public static final String formatDate(Date date, String pattern)
	{
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
	
	
	/**
	 * 判断字符串str是否符合pattern类型的正则表达式
	 * @param str 
	 * @param pattern
	 * @return
	 */
	public static final boolean isPattern(String str,String pattern)
	{
//		/^[-|+]?\\d*([.]\\d{0,2})?$
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(str);
		return m.find();
	}
	
	/**
	 * 格式化整型
	 * @param num 整型
 	 * @param pattern 模式如：000    00.00
	 * @return
	 */
	public static final String fomatInteger(int num,String pattern)
	{
		DecimalFormat df = new DecimalFormat(pattern);
		return df.format(num);
	}
	
	
	/**
	 * 采用GBK对字符进行编码
	 * @param buf
	 * @return
	 */
	public static final String encodeWithGBK(byte[] buf)
	{
		return encode(buf, 0, buf.length, "GBK");
	}
	
	
	/**
	 * 采用GBK对字符进行编码
	 * @param buf
	 * @param from
	 * @param len
	 * @return
	 */
	public static final String encodeWithGBK(byte[] buf,int from,int len)
	{
		return encode(buf, from, len, "GBK");
	}
	
	/**
	 * 字符串编码
	 * @param buf byte数组
	 * @param from 开始字节
	 * @param len 总共几个字节
	 * @param charset 编码类型
	 * @return
	 */
	public static final String encode(byte[] buf,int from,int len,String charset)
	{
		String tmp = null;
		
		try
		{
			tmp = new String(buf, from, len, charset);
		} catch (UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		
		return tmp;
	}
	
	/**
	 * 判断字符串str是不是纯数字的字符串
	 * @param str
	 * @return
	 */
	public static boolean isNum(String str)
	{
		return str.matches("^[-+]?(([0-9]+)([.]([0-9]+))?|([.]([0-9]+))?)$");
	}
	
	// 调整字符串长度
	public static String adjustStringLength(String str, int length)
	{
		if(str == null)
		{
			return null;
		}
		if(str.length() > length)
		{
			String result = str.substring(0, length) + "...";
			return result;
		}
		else
		{
			return str;
		}
	}
}