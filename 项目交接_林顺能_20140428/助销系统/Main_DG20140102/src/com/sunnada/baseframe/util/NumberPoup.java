/**
 * $RCSfile: NumberPoup.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-23  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */
package com.sunnada.baseframe.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.sunnada.baseframe.util.StringUtil;

/**
 * <p>Title: NumberPoup</p> 
 * <p>Description: 数字输入法</p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author 丘富铨
 * @date 2013-4-23
 * @version 1.0
 */

public class NumberPoup extends PopupWindow implements OnItemClickListener
{
	private static final String []numbers = new String[]{"1","2","3","4","5","6","7","8","9","0",".","清除"};
	private Context context = null;
	private GridView gridView = null;
	private EditText editText = null;
	
	public NumberPoup(Context context, EditText editText)
	{
		super(new GridView(context));
		this.context = context;
		this.editText = editText;
		initViews();
	}
	
	private void initViews()
	{
		gridView = (GridView) getContentView();
		gridView.setNumColumns(3*4);
		gridView.setBackgroundColor(Color.parseColor("#FFFFFF"));
		gridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
		gridView.setHorizontalSpacing(2);
		gridView.setVerticalSpacing(2);
		gridView.setColumnWidth(GridView.AUTO_FIT);
		gridView.setPadding(2, 2, 2, 2);
		gridView.setFocusable(true);
		gridView.setDrawSelectorOnTop(true);
		
		gridView.setAdapter(new InputAdapter(numbers));
		gridView.setOnItemClickListener(this);
		
		this.setBackgroundDrawable(new BitmapDrawable());
		this.setOutsideTouchable(true);
		this.setFocusable(true);
		setWidth(ViewGroup.LayoutParams.FILL_PARENT);
		setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
		setContentView(gridView);
	}
	
	
	/**
	 * 输入界面适配器
	 * @author aquan
	 *
	 */
	private class InputAdapter extends BaseAdapter
	{
		private String [] inputChars = new String[0];
		
		public InputAdapter(String[] inputChars)
		{
			super();
			this.inputChars = inputChars;
		}

		@Override
		public int getCount()
		{
			return inputChars.length;
		}

		@Override
		public Object getItem(int arg0)
		{
			return inputChars[arg0];
		}

		@Override
		public long getItemId(int arg0)
		{
			return arg0;
		}

		@Override
		public View getView(int postion, View view, ViewGroup viewGroup)
		{
			TextView tv = null;
			
			if(view == null)
			{
				tv = new TextView(context);
				tv.setGravity(Gravity.CENTER);
				tv.setTextColor(Color.WHITE);
				tv.setBackgroundColor(Color.parseColor("#000000"));
				tv.setWidth(50);
				tv.setHeight(50);
				tv.setTextSize(24);
				tv.setFocusable(false);
			
			}else
			{
				tv = (TextView)view;
			}
			
			
			tv.setText(inputChars[postion]);
			
			return tv;
		}
		
	}



	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int postion, long id)
	{
		String str = editText.getText().toString().trim();
		
		if("清除".equals(numbers[postion]))
		{
			if(!StringUtil.isEmptyOrNull(str))
			{
				str = str.substring(0, str.length()-1);
			}
			
		}else
		{
			str = editText.getText().toString()+numbers[postion];
		}
		
		editText.setText(str);
	}
	
	
	
	public static final void fecthEditTextWithNumbers(Context context,final EditText editText)
	{
		final NumberPoup numberPoup = new NumberPoup(context, editText);
		editText.setFocusable(false);
		
		editText.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				if(event.getAction() == MotionEvent.ACTION_UP)
				{
//					numberPoup.showAsDropDown(editText,(editText.getWidth() - numberPoup.getWidth())/2,0);
					numberPoup.showAtLocation(editText, Gravity.BOTTOM,0, 0);
				}
				return false;
			}
		});
	}
}


