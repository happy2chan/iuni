package com.sunnada.baseframe.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class FileHandleUtil
{
	public static Bitmap decodeFile(String sBasedir, String sPicName)
	{
		//动态调整opts的inSampleSize，从而达到每个图片取出来的高度都是75px
		BitmapFactory.Options opts =  new  BitmapFactory.Options();
		//设置inJustDecodeBounds为true后，decodeFile并不分配空间，但可计算出原始图片的长度和宽度，即opts.width和opts.height
		opts.inJustDecodeBounds = true;
		//默认只加载SD卡中的文件，获得这个图片的宽和高
		BitmapFactory.decodeFile(sBasedir+sPicName,opts);		
		opts.inJustDecodeBounds = false;
	    //计算缩放比，固定为高度是75
	    int be = (int)(opts.outHeight / (float)75);
	    /*
	    if (be <= 0)
	        be = 1;
	    */
	    be = 1;
	    opts.inSampleSize = be;
	    //重新读入图片，注意这次要把options.inJustDecodeBounds 设为 false哦
	    Bitmap bitmap = BitmapFactory.decodeFile(sBasedir+sPicName, opts);	
	    /*
	    if(b!=null)
	    {
			map=new HashMap<String,Object>();
			map.put("icon", b);
			list.add(map);
		}
		*/
	    return bitmap;
	}
	
	public static Boolean isBitampAvailable(Bitmap bitmap)
	{
		if(bitmap != null && !bitmap.isRecycled())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static void recycleResource(Map<String, Bitmap> map)
	{
		if(map != null)
		{
			Iterator<Entry<String, Bitmap>> iter = map.entrySet().iterator();
			while (iter.hasNext()) 
			{
				Map.Entry<String, Bitmap> entry = (Map.Entry<String, Bitmap>) iter.next(); 
				Bitmap bitmap = entry.getValue();
				if(bitmap != null && !bitmap.isRecycled())
				{
					bitmap.recycle();
					bitmap = null;
				}
			}
			map = null;
			System.gc();
		}
	}
}
