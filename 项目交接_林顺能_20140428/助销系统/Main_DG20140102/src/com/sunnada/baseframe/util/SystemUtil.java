/**
 * $RCSfile: SystemUtil.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-28  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.util;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;


/**
 * <p>Title: SystemUtil</p> 
 * <p>Description: </p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author 丘富铨
 * @date 2013-4-28
 * @version 1.0
 */

public class SystemUtil
{

	/**
	 * 获取当前网络是否连接
	 * @param context
	 * @return
	 */
	public static final  boolean isConnect(Context context)
	{
		// 获取手机所有连接管理对象（包括对wi-fi,net等连接的管理）
		try
		{
			ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null)
			{
				// 获取网络连接管理的对象
				NetworkInfo info = connectivity.getActiveNetworkInfo();
				if (info != null && info.isConnected())
				{
					// 判断当前网络是否已经连接
					if (info.getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}
				}
			}
		} catch (Exception e)
		{
			// TODO: handle exception
			Log.log("error", e.toString());
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 获取版本号
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static final String getVersionName(Context context,String packageName)
	{
		String version = "0.0.0";
		try
		{
			// 获取packagemanager的实例
			PackageManager packageManager = context.getPackageManager();
			// getPackageName()是你当前类的包名，0代表是获取版本信息
			PackageInfo packInfo = packageManager.getPackageInfo(packageName,0);
			version = packInfo.versionName;
			
		} catch (Exception e)
		{
			Log.log(e);
		}
        return version;
	}
	

	
	// 安装apk
	public static void installApk(File file, Context context)
	{
		Intent intent = new Intent();
		// 执行动作
		intent.setAction(Intent.ACTION_VIEW);
		// 执行的数据类型
		intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
	
	/**
	 * 卸载APK
	 * @param context
	 * @param packageName
	 */
	public static void unInstallApk(Context context,String packageName)
	{
		Uri packageURI=Uri.parse("package:"+packageName);  
		//创建Intent意图  
		Intent intent=new Intent(Intent.ACTION_DELETE);  
		//设置Uri  
		intent.setData(packageURI);  
		//卸载程序  
		context.startActivity(intent);  
	}
}


