package com.sunnada.baseframe.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Environment;

public class PictureShowUtils
{
	// 图片存储位置，SD卡的pic目录下。
	private String	baseDir			= Environment.getExternalStorageDirectory().toString();
	private String	dirName			= Environment.getExternalStorageDirectory().toString();
	private String	sAdditionPath	= "";
	// 用于记录目录下图片名称的字符串数组
	String[]		filenames		= null;

	// 在构造函数中初始化filenames
	public PictureShowUtils(String sAddPath, Context context)
	{
		try
		{
			/* 把附加的目录拼上 */
			if ("".equals(sAddPath) || "null".equals(sAddPath) || sAddPath == null)
			{
			}
			else
			{
				sAdditionPath = sAddPath;
				dirName = dirName + sAddPath;
			}

			filenames = new File(dirName).list();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			filenames = new String[0];
		}
	}

	// 新增一个构造函数,判断是否需要从服务器同步更新(许多场景下并不需要同步更新)
	public PictureShowUtils(String sAddPath, Context context, boolean loadPic)
	{
		try
		{
			/* 把附加的目录拼上 */
			if ("".equals(sAddPath) || "null".equals(sAddPath) || sAddPath == null)
			{
			}
			else
			{
				sAdditionPath = sAddPath;
				dirName = dirName + sAddPath;
			}

			filenames = new File(dirName).list();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			filenames = null;
		}
	}

	// 获取图片数量
	public int getCount()
	{
		if (filenames == null)
			return 0;
		return filenames.length;
	}

	// 获取指定索引的图片
	public Bitmap getImageAt(int i)
	{
		String path = dirName;
		if (i >= filenames.length)
			return null;
		path += filenames[i];
		// 使用BitmapFactory.decodeFile读取图片内容
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = 4;

		Bitmap b = BitmapFactory.decodeFile(path, opts);
		// Bitmap b = BitmapFactory.decodeFile(path);
		return b;
	}

	/**
	 * 判断当前索引的文件是否图片
	 * 
	 * @param i
	 * @return
	 */
	public boolean isImage(int i)
	{
		String path = dirName;
		if (i >= filenames.length)
			return false;
		path += filenames[i];
		// 使用BitmapFactory.decodeFile读取图片内容
		// BitmapFactory.Options opts = new BitmapFactory.Options();
		// opts.inJustDecodeBounds = true;
		// Bitmap b = BitmapFactory.decodeFile(path,opts);
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = 4;

		Bitmap b = BitmapFactory.decodeFile(path, opts);
		if (b == null)
		{
			return false;
		}
		else
		{
			b.recycle();
			return true;
		}
	}

	/**
	 * 返回指定目录下的所有图片文件的文件名列表
	 * 
	 * 如果张数大于1，则把第1张的放到第2个位置（和展示对应，默认展示最新的一张）
	 * 
	 * @return
	 */
	public String[] getImagePositions()
	{
		String[] sImages = new String[0];
		if (filenames != null)
		{
			ArrayList<String> listFiles = new ArrayList<String>();
			for (int i = 0; i < filenames.length; i++)
			{
				if (isImage(i))
				{
					listFiles.add(sAdditionPath + filenames[i]);
				}
			}

			if (listFiles != null)
			{
				sImages = new String[listFiles.size()];
				for (int j = 0; j < listFiles.size(); j++)
				{
					if (listFiles.size() > 1)
					{
						if (j == 0)
						{
							sImages[1] = (String) listFiles.get(j);
						}
						else if (j == 1)
						{
							sImages[0] = (String) listFiles.get(j);
						}
						else
						{
							sImages[j] = (String) listFiles.get(j);
						}
					}
					else
					{
						sImages[j] = (String) listFiles.get(j);
					}
				}
			}
		}
		else
		{
			
		}
		return sImages;
	}

	public String getDirName()
	{
		return dirName;
	}

	public void setDirName(String dirName)
	{
		this.dirName = dirName;
	}

	public String getBaseDir()
	{
		return baseDir;
	}

	public void setBaseDir(String baseDir)
	{
		this.baseDir = baseDir;
	}

	/**
	 * 加载图片
	 * 
	 * @param fileName
	 * @param maxWidth
	 * @param maxHeight
	 * @return
	 */
	public static Bitmap decodeFile(String fileName, int maxWidth, int maxHeight)
	{
		// 动态调整opts的inSampleSize，从而达到每个图片取出来的高度都是75px
		BitmapFactory.Options opts = new BitmapFactory.Options();
		// 设置inJustDecodeBounds为true后，decodeFile并不分配空间，但可计算出原始图片的长度和宽度，即opts.width和opts.height
		opts.inJustDecodeBounds = true;
		// 默认只加载SD卡中的文件，获得这个图片的宽和高
		BitmapFactory.decodeFile(fileName, opts);
		opts.inJustDecodeBounds = false;
		int be = 0;
		float scale = 0;
		if (opts.outWidth == 0 || opts.outHeight == 0)
		{
			return null;
		}
		else
		{
			if (opts.outHeight / opts.outWidth >= (maxHeight / maxWidth))
			{
				maxWidth = (int) (maxHeight * opts.outWidth / opts.outHeight);
				scale = opts.outHeight / (float) maxHeight;
			}
			else
			{
				maxHeight = (int) (maxWidth * opts.outHeight / opts.outWidth);
				scale = opts.outWidth / (float) maxWidth;
			}
		}
		if (scale <= 1)
		{
			be = 1;
		}
		else if (scale % 1 == 0)
		{
			be = (int) scale;
		}
		else
		{
			be = (int) scale + 1;
		}
		opts.inSampleSize = be;
		Bitmap bitmap = BitmapFactory.decodeFile(fileName, opts);
		if(bitmap == null)
		{
			return null;
		}
		Bitmap newBitmap = bitmap;
		if (scale % 1 != 0)
		{
			int width = bitmap.getWidth();
			int height = bitmap.getHeight();

			float scaleWidth = ((float) maxWidth) / width;
			float scaleHeight = ((float) maxHeight) / height;

			// createa matrix for the manipulation
			Matrix matrix = new Matrix();
			// resize the bit map
			matrix.postScale(scaleWidth, scaleHeight);

			// recreate the new Bitmap
			newBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
			if (newBitmap != bitmap)
			{
				// 如果进一步压缩(压缩后是一个新的bitmap对象),则把原先的回收
				bitmap.recycle();
			}
		}
		bitmap = null;
		return newBitmap;
	}

	/**
	 * 压缩图片为800*600,并加上水印
	 * 
	 * @param fileName
	 *            图片路径
	 * @param waterMark
	 *            水印图形
	 * @return 加过水印的图片数据(不改变原文件)
	 * @throws FileNotFoundException
	 */
	public static void addWaterMark(String fileName, Bitmap waterMark)
	{
		Bitmap bitmap = decodeFile(fileName, 800, 600);

		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		int ww = waterMark.getWidth();
		int wh = waterMark.getHeight();

		Bitmap newb = Bitmap.createBitmap(w, h, Config.ARGB_8888);// 创建一个新的和SRC长度宽度一样的位图
		Canvas cv = new Canvas(newb);
		cv.drawBitmap(bitmap, 0, 0, null); // 在 0，0坐标开始画入src
		if (waterMark != null)
		{
			cv.drawBitmap(waterMark, w - ww - 20, h - wh - 20, null); // 画水印
		}
		cv.save(Canvas.ALL_SAVE_FLAG);

		cv.restore();
		FileOutputStream fos = null;
		try
		{
			fos = new FileOutputStream(fileName);
			newb.compress(Bitmap.CompressFormat.JPEG, 100, fos);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (fos != null)
			{
				try
				{
					fos.flush();
					fos.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 压缩图片为800*600,并加上文字水印
	 * 
	 * @param fileName
	 *            图片路径
	 * @param textmark
	 *            水印文字
	 * @return 加过水印的图片数据(不改变原文件)
	 * @throws FileNotFoundException
	 */
	public static void addTextMark(String fileName, String textmark)
	{
		Bitmap bitmap = decodeFile(fileName, 800, 600);

		int w = bitmap.getWidth();
		int h = bitmap.getHeight();

		Bitmap newb = Bitmap.createBitmap(w, h, Config.ARGB_8888);// 创建一个新的和SRC长度宽度一样的位图

		Canvas cv = new Canvas(newb);
		cv.drawBitmap(bitmap, 0, 0, null); // 在 0，0坐标开始画入src

		bitmap.recycle();
		if (textmark != null && !textmark.equals(""))
		{
			String familyName = "宋体";
			Paint p = new Paint();
			Typeface font = Typeface.create(familyName, Typeface.BOLD);
			p.setColor(Color.RED);
			p.setTypeface(font);
			p.setTextSize(25);
			cv.drawText(textmark, 350, 550, p); // 画水印
		}
		cv.save(Canvas.ALL_SAVE_FLAG);

		cv.restore();

		FileOutputStream fos = null;
		try
		{
			fos = new FileOutputStream(fileName, false);
			newb.compress(Bitmap.CompressFormat.JPEG, 80, fos); // 这里设置压缩质量,目前设置80
			newb.recycle();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (fos != null)
			{
				try
				{
					fos.flush();
					fos.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	public static String test()
	{ // 测试方法,程序中不使用
	// String fileName =
	// Environment.getExternalStorageDirectory().toString()+"/";
	// addTextMark(fileName+"123.jpg", "仅供中国联通开户专用");
	// // String str = FileUploadUtil.ftpUpload("192.168.21.28", "21",
	// "testtemp", "pttest", "ffff", fileName, "123.jpg");
	// String str = null;
	// FtpHelper myFtp = new FtpHelper();
	// try {
	// myFtp.connect("192.168.21.28", 21, "testtemp", "pttest");
	// str=myFtp.upload(fileName+"123.jpg", "/ffff/ddds/3124.jpg").toString();
	//
	// myFtp.disconnect();
	// } catch (IOException e) {
	//
	// System.out.println("连接FTP出错：" + e.getMessage());
	// e.printStackTrace();
	// }
		File file = new File(Environment.getExternalStorageDirectory().toString() + "/Reader");

		String str = file.list()[0];

		return str;
	}

	public static boolean compressPic(String fileName, int width, int height)
	{
		// Log.i("内存", "开始进行图片压缩");
		boolean flag = false;
		// Bitmap bitmap = decodeFile(fileName, width, height);
		//
		// int w = bitmap.getWidth();
		// int h = bitmap.getHeight();
		//
		// Bitmap newb = Bitmap.createBitmap(w, h, Config.ARGB_8888);//
		// 创建一个新的和SRC长度宽度一样的位图
		//
		// Canvas cv = new Canvas(newb);
		// cv.drawBitmap(bitmap, 0, 0, null); // 在 0，0坐标开始画入src
		//
		// bitmap.recycle();
		//
		// cv.save(Canvas.ALL_SAVE_FLAG);
		//
		// cv.restore();
		//
		// FileOutputStream fos = null;
		//
		// try {
		// fos = new FileOutputStream(fileName,false);
		// flag = newb.compress(Bitmap.CompressFormat.JPEG, 80, fos);
		// //这里设置压缩质量,目前设置80
		// newb.recycle();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }finally{
		//
		// if(fos!=null){
		// try {
		// fos.flush();
		// fos.close();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// }
		// }
		return flag;
	}
}
