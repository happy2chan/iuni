/**
 * $RCSfile: SharePreferenceUtil.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-21  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.util;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * <p>Title: SharePreferenceUtil</p> 
 * <p>Description: </p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author ����
 * @date 2013-4-21
 * @version 1.0
 */

public class SharePreferenceUtil
{
	public static final SharedPreferences getSharePreference(Context context)
	{
		return context.getSharedPreferences("preference", Context.MODE_PRIVATE);
	}
}


