package com.sunnada.baseframe.util;

// 打印工具类
public class PrintUtil 
{
	public static final String STR_FAIL_TITLE = "重新打印";
	public static final String STR_FAIL_CANCEL = "取消"; 
	public static final String STR_FAIL_OK = "重新打印";
	public static final String STR_FAIL_MSG = "打印失败，是否重新打印?";
	public static final String STR_SUCCESS_OK = "确定";
	public static final String STR_SUCCESS_TITLE = "打印成功";
	public static final String STR_SUCCESS_MSG = "凭条打印成功，业务受理完成!";
	public static final String STR_SUC_MSG_REPRINT = "打印成功, 是否重新打印?";
	
	// 异常信息包装和获取
	public static String errorMsgPgAndGet(String errorMsg)
	{
		return "由于"+errorMsg+"的原因,打印失败,是否重新打印？";
	}
}
