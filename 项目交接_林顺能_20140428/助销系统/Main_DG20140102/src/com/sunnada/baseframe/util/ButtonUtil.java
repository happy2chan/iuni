package com.sunnada.baseframe.util;

public class ButtonUtil
{
	private static long 		mLastClickTime 		= 0;
	private static long 		DIFF 				= 1000;
	private static int 			mLastButtonId 		= -1;
	private static long 		mLastToastTime 		= 0;
	
	// 判断两次点击的间隔，如果小于DIFF，则认为是多次无效点击
	public static boolean isFastDoubleClick()
	{
		return isFastDoubleClick(-1, DIFF);
	}
	
	// 判断两次点击的间隔，如果小于DIFF，则认为是多次无效点击
	public static boolean isFastDoubleClick(int buttonId)
	{
		return isFastDoubleClick(buttonId, DIFF);
	}
	
	// 判断两次点击的间隔，如果小于diff，则认为是多次无效点击
	public static boolean isFastDoubleClick(int buttonId, long diff)
	{
		long curClickTime = System.currentTimeMillis();
		long timeSpace = Math.abs(curClickTime - mLastClickTime);
		
		// 如果不是同一个按键的话，也不能太快
		if (mLastButtonId != buttonId && mLastClickTime > 0 && timeSpace < (diff / 3)) 
		{
			// 非常快就不显示，不然一直显示
			if(curClickTime - mLastToastTime > 1800)
			{
				mLastToastTime = curClickTime;
			}
			mLastClickTime = curClickTime;
			return true;
		}
		
		// 同一按键
		if (mLastButtonId == buttonId && mLastClickTime > 0 && timeSpace < diff)
		{
			// 非常快就不显示，不然一直显示
			if(curClickTime - mLastToastTime > 1800)
			{
				mLastToastTime = curClickTime;
			}
			mLastClickTime = curClickTime;
			return true;
		}
		
		mLastClickTime = curClickTime;
		mLastButtonId  = buttonId;
		return false;
	}
}
