package com.sunnada.baseframe.animation;

import android.view.animation.TranslateAnimation;

public class FlyLeftInAnimation extends TranslateAnimation
{
	public FlyLeftInAnimation()
	{
		super(TranslateAnimation.RELATIVE_TO_SELF,-1f,TranslateAnimation.RELATIVE_TO_SELF,0f,TranslateAnimation.RELATIVE_TO_SELF,0f,TranslateAnimation.RELATIVE_TO_SELF,0f);
		setDuration(250);
	}
}
