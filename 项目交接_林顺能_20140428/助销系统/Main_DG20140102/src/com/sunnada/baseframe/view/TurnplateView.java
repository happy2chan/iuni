package com.sunnada.baseframe.view;

import com.sunnada.baseframe.activity.R;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;

/** 
 * 类名称：TurnplateView
 * 类描述：   
 * 创建时间：2013-10-03
 */
public class TurnplateView extends View implements OnTouchListener 
{
	private OnTurnplateListener onTurnplateListener;

	private Bitmap[]			mIcons				= new Bitmap[1];	// 图形列表
	private Bitmap				mMenuBgIcon			= null;
	
	// 基圆属性
	private Point[]             mPoints;								// point列表
	private int                 mPointNum			= 4;				// 数目
	private int                 mPointX				= 0;				// 圆形x坐标
	private int                 mPointY				= 0;				// 圆形y坐标
	private int                 mRadius				= 0;				// 菜单icon所在的圆半径
	private int                 mDegreeDelta;							// 每两个点间隔的角度
	private int                 mTempDegree			= 0;				// 每次转动的角度差
	
	// 菜单标识
	private int                 mChooseBtn			= 999;				// 选中的图标标识999：未选中任何图标
	private int                 mActionMode			= 0;				// 按下为0,移动为1
	private float               mHotAreaRadius		= 31;				// 菜单响应半径
	private boolean             mIsEnableTouch		= true;				// 是否响应Touch事件
	private int                 mExradius			= 0;				// 外圆半径
	
	private float               mOldMenuPointX		= -10;
	private float               mOldMenuPointY		= -10;
	
	private boolean             mIsBeyondMenu		= false;          // 超出菜单范围
	
	public TurnplateView(Context context)
	{
		super(context);
	}
	
	public TurnplateView(Context context, int px, int py, int radius, int nPointNum) 
	{
		super(context);
		
		mPointX 	= px;
		mPointY 	= py;
		mRadius 	= radius;
		mPointNum 	= nPointNum;
		setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
	}
	
	public void setOnTurnplateListener(OnTurnplateListener onTurnplateListener) 
	{
		this.onTurnplateListener = onTurnplateListener;
	}
	
	// 设置圆环菜单的背景图片
	public void setMenuBgIcon(Bitmap menuBgIcon)
	{
		this.mMenuBgIcon = menuBgIcon;
		this.mExradius = mMenuBgIcon.getWidth();
	}
	
	// 初始化圆环菜单上显示的菜单图片及每个点的关联的数据信息
	public void initDate(Bitmap[] icons)
	{
		if(icons == null)
		{
			loadDefaultIcons();
		}
		else
		{
			this.mIcons = icons;
			mHotAreaRadius = icons[0].getWidth()/2;
		}
		// 初始化每个点
		initPoints();
		// 计算每个点的坐标
		computeCoordinates();
	}
	
	// 获取默认ICON
	private void loadDefaultIcons() 
	{
		Resources r = getResources();
		loadBitmaps(0, r.getDrawable(R.drawable.icon_frame_home));
	}
	
	// 装载图片
	private void loadBitmaps(int nIndex, Drawable drawable) 
	{
		Bitmap bitmap = Bitmap.createBitmap(60, 60, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, 60, 60);
		drawable.draw(canvas);
		mIcons[nIndex] = bitmap;
	}
	
	// 初始化每个点
	private void initPoints() 
	{
		if(mPointNum == 3) 
		{
			mPoints = new Point[3];
			
			mPoints[0] 			= new Point();
			mPoints[0].angle 	= 315;
			mPoints[0].bitmap 	= mIcons[0];
			mPoints[0].flag 	= 0;
			
			mPoints[1] 			= new Point();
			mPoints[1].angle 	= 0;
			mPoints[1].bitmap 	= mIcons[1];
			mPoints[1].flag 	= 1;
			
			mPoints[2] 			= new Point();
			mPoints[2].angle 	= 45;
			mPoints[2].bitmap 	= mIcons[2];
			mPoints[2].flag 	= 2;
			return;
		}
		
		int angle = 0;
		mPoints = new Point[mPointNum];
		mDegreeDelta = 360/mPointNum;
		
		for(int index=0; index<mPointNum; index++) 
		{
			Point point 	= new Point();
			point.angle 	= angle;
			angle 		   += mDegreeDelta;
			point.bitmap 	= mIcons[index];
			point.flag 		= index;
			mPoints[index] 	= point;
		}
	}
	
	// 计算每个点的坐标
	private void computeCoordinates() 
	{
		// 主页和二维码快捷购机
		if(mPointNum == 3) 
		{
			// 第1个点
			mPoints[0].x = mPointX + (float)(mRadius*Math.cos(mPoints[0].angle*Math.PI/180));
			mPoints[0].y = mPointY + (float)(mRadius*Math.sin(mPoints[0].angle*Math.PI/180));
			// 第2个点
			mPoints[1].x = mPointX + (float)(mRadius*Math.cos(mPoints[1].angle*Math.PI/180));
			mPoints[1].y = mPointY + (float)(mRadius*Math.sin(mPoints[1].angle*Math.PI/180));
			// 第3个点
			mPoints[2].x = mPointX + (float)(mRadius*Math.cos(mPoints[2].angle*Math.PI/180));
			mPoints[2].y = mPointY + (float)(mRadius*Math.sin(mPoints[2].angle*Math.PI/180));
			return;
		}
		
		Point point;
		for(int index=0; index<mPointNum; index++) 
		{
			point = mPoints[index];
			point.x = mPointX + (float)(mRadius*Math.cos(point.angle*Math.PI/180));
			point.y = mPointY + (float)(mRadius*Math.sin(point.angle*Math.PI/180));
			//point.x_c = mPointX + (point.x-mPointX)/2;
			//point.y_c = mPointY + (point.y-mPointY)/2;
		}
	}
	
	// 计算偏移角度 
	private int computeMigrationAngle(float x, float y) 
	{
		int a = 0;
		float distance = (float)Math.sqrt(((x-mPointX)*(x-mPointX) + (y-mPointY)*(y-mPointY)));
		int degree = (int)(Math.acos((x-mPointX)/distance)*180/Math.PI);
		if(y < mPointY) 
		{
			degree = -degree;
		}
		
		if(x < 0)
		{
			degree = -degree;
		}
		
		if(mTempDegree != 0) 
		{
			a = degree - mTempDegree;
		}
		mTempDegree = degree;
		return a;
	}
	
	/**
	 * 
	 * 方法名：computeCurrentDistance 
	 * 功能：计算触摸的位置与各个元点的距离
	 * 创建时间：2013-10-03
	 */
	private int computeCurrentDistance(float x, float y) 
	{
		for(Point point:mPoints) 
		{
			float distance = (float)Math.sqrt(((x-point.x)*(x-point.x) + (y-point.y)*(y-point.y)));			
			if(distance < mHotAreaRadius) 
			{
				mChooseBtn = point.flag;
				break;
			}
			else 
			{
				mChooseBtn = 999;
			}
		}
		return mChooseBtn;
	}
	
	// 记录被选中菜单的圆心
	private void hotRreaMenuPointRecord(float x, float y) 
	{
		for(Point point:mPoints) 
		{
			float distance = (float)Math.sqrt(((x-point.x)*(x-point.x) + (y-point.y)*(y-point.y)));
			if(distance < mHotAreaRadius) 
			{
				mOldMenuPointX = point.x;
				mOldMenuPointY = point.y;
				break;
			}
			else 
			{
				mOldMenuPointX = -10;
				mOldMenuPointY = -10;
			}
		}	
	}
	
	// 记录是否移动超出了被按下时记录下的菜单的范围
	private void isBeyondMenuBooleanRecord(float x,float y)
	{
		if(mOldMenuPointX != -10)
		{
			float distance = (float)Math.sqrt(((x-mOldMenuPointX)*(x-mOldMenuPointX) + 
					(y-mOldMenuPointY)*(y-mOldMenuPointY)));
			if(distance > mHotAreaRadius) 
			{
				if(!mIsBeyondMenu) 
				{
					mIsBeyondMenu = true;
				}
			}
		}
	}
	
	// 触摸点是否超过外圆半径
	private boolean isBeyondRadius(float x, float y) 
	{
		float distance = (float)Math.sqrt(((x-mPointX)*(x-mPointX) + (y-mPointY)*(y-mPointY)));		
		if(distance > (mExradius + 1)) 
		{
			mChooseBtn = -1;
			return true;
		}
		return false;
	}
	
	// 再次把关是否是真的移动
	private boolean isMove(float x, float y) 
	{
		if(mOldMenuPointX != -10)
		{
			float distance = (float)Math.sqrt(((x-mOldMenuPointX)*(x-mOldMenuPointX) + 
					(y-mOldMenuPointY)*(y-mOldMenuPointY)));
			if(distance > (mHotAreaRadius+2)) 
			{
				return true;
			}
			else
			{
				if(mIsBeyondMenu)
				{
					return true;
				}
				return false;
			}
		}
		return true;
	}
	
	private void switchScreen(MotionEvent event) 
	{
		computeCurrentDistance(event.getX(), event.getY());
		onTurnplateListener.onPointTouch(mChooseBtn);
	}
	
	private void switchScreen()
	{
		onTurnplateListener.onPointTouch(mChooseBtn);
	}
	
	public void setEnableTouchStatus(boolean isEnable) 
	{
		mIsEnableTouch = isEnable;
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) 
	{		
		if(mIsEnableTouch) 
		{
			int action = event.getAction();
	        switch (action) 
	        {
	        case MotionEvent.ACTION_DOWN:
	        	mActionMode = 0;
	        	// 记录此刻被选中菜单的中心位置
	        	hotRreaMenuPointRecord(event.getX(), event.getY());
	        	if(isBeyondRadius(event.getX(), event.getY())) 
	        	{
	        		switchScreen();
	        	}
	            break;
	            
	        case MotionEvent.ACTION_MOVE:
	        	// 大于3个菜单icon再响应
	        	if(mPointNum > 3) 
	        	{
	        		mActionMode = 1;
		        	resetPointAngle(event.getX(), event.getY());
		        	isBeyondMenuBooleanRecord(event.getX(), event.getY());
		    		computeCoordinates();
		    		invalidate();
	        	}
	            break;
	            
	        case MotionEvent.ACTION_UP:
	        	if(isBeyondRadius(event.getX(), event.getY())) 
	        	{
	        		break;
	        	}
	        	
	        	switch (mActionMode) 
	        	{
				case 0:
					switchScreen(event);
					break;
					
				case 1:
					if(isMove(event.getX(), event.getY()))
		        	{
						mIsBeyondMenu = false;
		        		break;
		        	}
		        	else
		        	{
		        		switchScreen(event);
		        	}
					break;
				}
	        	
	        	mTempDegree = 0;
	        	invalidate();
	            break;
	            
	        case MotionEvent.ACTION_CANCEL:
	        	//系统在运行到一定程度下无法继续响应你的后续动作时会产生此事件。
	        	//一般仅在代码中将其视为异常分支情况处理
	            break;
	        }
		}
		return true;
	}
	
	// 重新计算每个点的角度
	private void resetPointAngle(float x, float y) 
	{
		int degree = computeMigrationAngle(x, y);
		for(int index=0; index<mPointNum; index++) 
		{
			mPoints[index].angle += degree;
			if(mPoints[index].angle > 360) 
			{
				mPoints[index].angle -= 360;
			}
			else if(mPoints[index].angle < 0) 
			{
				mPoints[index].angle += 360;
			}
		}
	}
	
	@Override
	public void onDraw(Canvas canvas) 
	{
		if(mMenuBgIcon != null)
		{
			// 绘制圆环菜单的背景
			Paint paint = new Paint();
			paint.setAlpha(120);
			canvas.drawBitmap(mMenuBgIcon, mPointX, mPointY-(float)mMenuBgIcon.getHeight()/2, paint);
		}
		
		for(int index=0; index<mPointNum; index++) 
		{
			// 绘制圆环菜单上的菜单
			drawInCenter(canvas, mPoints[index].bitmap, mPoints[index].x, mPoints[index].y);
		}
	}
	
	// 把点放到图片中心处
	private void drawInCenter(Canvas canvas, Bitmap bitmap, float left, float top) 
	{
		Paint paint = new Paint();
		/*
		if(mChooseBtn == flag) 
		{
			mMatrix.setScale(mScaleWidth/bitmap.getWidth(),mScaleHeight/bitmap.getHeight());   
			mMatrix.postTranslate(left-mScaleWidth/2, top-mScaleHeight/2);  
			canvas.drawBitmap(bitmap, mMatrix, null); 
		}
		else 
		{
		*/
			canvas.drawBitmap(bitmap, left-bitmap.getWidth()/2, top-bitmap.getHeight()/2, paint);
		//}
	}	
	
	class Point 
	{
		int 	flag; 		// 位置标识
		Bitmap 	bitmap;		// 图片
		int 	angle; 		// 角度
		float 	x; 			// x坐标
		float 	y; 			// y坐标
		//float x_c; 		// 点与圆心的中心x坐标
		//float y_c; 		// 点与圆心的中心y坐标
	}

	public static interface OnTurnplateListener 
	{
		public void onPointTouch(int flag);
	}

	public boolean onTouch(View view, MotionEvent event) 
	{
		return false;
	}
}
