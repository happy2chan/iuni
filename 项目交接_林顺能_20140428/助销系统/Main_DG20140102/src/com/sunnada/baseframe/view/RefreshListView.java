package com.sunnada.baseframe.view;


import com.sunnada.baseframe.activity.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class RefreshListView extends ListView {
	// private boolean isBottom;
	private int start;
	private View subView;
	private static int subViewHeigth = 109;
	private RotateAnimation animation;
	private RotateAnimation reverseAnimation;
	private ImageView arrow;
	private ProgressBar progressBar_refresh;
	private TextView textView_refresh;
	private boolean isArrived;
	private boolean isRefreshing;
	private int multiple;
	private int lenght;
	private OnRefreshListener refreshListener;

	public RefreshListView(Context context, int width) {
		super(context);
		setLayoutParams(new LayoutParams(width, LayoutParams.WRAP_CONTENT));
		init(context);
		// TODO Auto-generated constructor stub
	}

	public RefreshListView(Context context) {
		super(context);
		init(context);
		// TODO Auto-generated constructor stub
	}

	public RefreshListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		init(context);
	}

	public RefreshListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init(context);
	}

	void init(Context context) {
		subView = View.inflate(context, R.layout.subview, null);
		arrow = (ImageView) subView.findViewById(R.id.imageView_arrow);
		progressBar_refresh = (ProgressBar) subView
				.findViewById(R.id.progressBar_refresh);
		textView_refresh = (TextView) subView
				.findViewById(R.id.textView_refresh);
		addFooterView(subView);
		// measureView(subView);
		// subViewHeigth = subView.getMeasuredHeight();
		subView.setVisibility(View.GONE);
		subView.setPadding(0, -subViewHeigth, 0, 0);
	
		animation = new RotateAnimation(0, 180,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		animation.setInterpolator(new LinearInterpolator());
		animation.setDuration(250);
		animation.setFillAfter(true);

		reverseAnimation = new RotateAnimation(180, 0,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f) ;
		reverseAnimation.setInterpolator(new LinearInterpolator());
		reverseAnimation.setDuration(250);
		reverseAnimation.setFillAfter(true);
		
		
		isArrived = false;
		isRefreshing = false;
		multiple = 1;
		start = -1;
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		//System.out.println(ev.getAction());
		switch (ev.getAction()) {
		
		case MotionEvent.ACTION_DOWN:
			subView.setPadding(0, 0, 0, 0);
			subView.setVisibility(View.VISIBLE);
			if (subView.isShown()) {
				start = (int) ev.getY();
			} else {
				start = -1;
			}
			break;
		case MotionEvent.ACTION_MOVE:
			if (subView.isShown() && !isRefreshing) {

				if (start == -1) {
					start = (int) ev.getY();
				}
				lenght = startRecored((int) ev.getY());

				changeView(lenght);

			}
			break;
		case MotionEvent.ACTION_UP:
			if (subView.isShown()) {
				startRefresh(lenght);
			} else {
				subView.setVisibility(View.GONE);
				subView.setPadding(0, -subViewHeigth, 0, 0);
			}
			start = -1;
			isArrived = false;
			break;

		}
		return super.onTouchEvent(ev);
	}

	private int startRecored(int y) {
		int length = start - y;
		if (length >= 0) {
			return length;
		} else {
			return -1;
		}

	}

	private void changeView(int lenght) {

		subView.setPadding(0, 0, 0, lenght);
		if (lenght > subViewHeigth * multiple && !isArrived) {
			arrow.clearAnimation();
			arrow.startAnimation(animation);
			textView_refresh.setText("松开获取数据");
			isArrived = true;
		} else if (lenght <= subViewHeigth * multiple && isArrived) {
			arrow.clearAnimation();
			arrow.startAnimation(reverseAnimation);
			textView_refresh.setText("正在获取数据");
			isArrived = false;
		}
	}

	void startRefresh(int lenght) {
		if (lenght > subViewHeigth * multiple && !isRefreshing) {
			isRefreshing = true;
			arrow.clearAnimation();
			arrow.setVisibility(View.INVISIBLE);
			progressBar_refresh.setVisibility(View.VISIBLE);
			textView_refresh.setText("正在获取数据..");
			subView.setPadding(0, 0, 0, 1);
			onRefresh();
			// onRefreshComplete();
		} else if (lenght <= subViewHeigth * multiple && lenght > 0) {
			subView.setVisibility(View.GONE);
			subView.setPadding(0, -subViewHeigth, 0, 0);
		}

	}

	public void setOnRefreshListener(OnRefreshListener onRefreshListener) {
		this.refreshListener = onRefreshListener;
	}

	public interface OnRefreshListener {
		public void onRefresh();
	}

	public void onRefreshComplete() {
		textView_refresh.setText("涓婃媺鍒锋柊");
		progressBar_refresh.setVisibility(View.INVISIBLE);
		arrow.setVisibility(View.VISIBLE);

		isRefreshing = false;
		subView.setVisibility(View.INVISIBLE);
		subView.setPadding(0, -subViewHeigth, 0, 0);
	};

	private void onRefresh() {
		if (refreshListener != null) {
			refreshListener.onRefresh();
		}
	}

	// private void measureView(View child) {
	// ViewGroup.LayoutParams p = child.getLayoutParams();
	// if (p == null) {
	// p = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
	// ViewGroup.LayoutParams.WRAP_CONTENT);
	// }
	// int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0 + 0, p.width);
	// int lpHeight = p.height;
	// int childHeightSpec;
	// if (lpHeight > 0) {
	// childHeightSpec = MeasureSpec.makeMeasureSpec(lpHeight,
	// MeasureSpec.EXACTLY);
	// } else {
	// childHeightSpec = MeasureSpec.makeMeasureSpec(0,
	// MeasureSpec.UNSPECIFIED);
	// }
	// child.measure(childWidthSpec, childHeightSpec);
	// }
}
