/**
 * $RCSfile: HeartbeatService.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-25  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.sunnada.baseframe.service.DataBaseService.DataBaseBinder;
import com.sunnada.baseframe.service.EquipmentService.EquipmentBinder;
import com.sunnada.baseframe.service.SocketService.SocketBinder;
import com.sunnada.baseframe.thread.Thread;
import com.sunnada.baseframe.activity.BaseActivity;
import com.sunnada.baseframe.business.SystemParamsUpdateBusiness;
import com.sunnada.baseframe.util.Log;

/**
 * <p>Title: HeartbeatService</p> 
 * <p>Description: 心跳服务</p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author 丘富铨
 * @date 2013-4-25
 * @version 1.0
 */
public class HeartbeatService extends Service implements ServiceConnection
{
	private static final int 				HEARTBEAT_TIME 		= 1000*60*3;		// 3分钟心跳一次
	
	private boolean 						heartbeatRunning 	= true;
	
	private ISocketService 					socketService 		= null;
	private IEquipmentService 				equipmentService 	= null;
	private IDataBaseService 				dataBaseService 	= null;
	private SystemParamsUpdateBusiness 		systemParamsUpdateBusiness = null;
	
	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		bindService(new Intent(BaseActivity.DESCRIPTOR_SOCKETSERVICE), this, Context.BIND_AUTO_CREATE);
		bindService(new Intent(BaseActivity.DESCRIPTOR_EUIPMENTSERVICE), this, Context.BIND_AUTO_CREATE);
		bindService(new Intent(BaseActivity.DESCRIPTOR_DATABASEERVICE), this, Context.BIND_AUTO_CREATE);

		Log.log("心跳服务启动....");
		new Thread()
		{
			@Override
			public void runs()
			{
				Log.log("心跳线程启动....");
				
//				try
//				{
//					sleep(HEARTBEATTIME);//首次启动线程等待心跳时间
//				} catch (InterruptedException e1)
//				{
//					e1.printStackTrace();
//				}
				
				Log.log("等待服务绑定");
				while(socketService == null || dataBaseService == null || equipmentService == null);
				Log.log("服务绑定完成");
				
				while(heartbeatRunning)
				{//如果心跳标记位未结束就一直轮询
					Log.log("时间到，准备心跳");
					long lastSendTime = 0L;
					long currentTime = System.currentTimeMillis();
					long diffTime = HEARTBEAT_TIME;
					long waitTime = HEARTBEAT_TIME;
					
					try
					{
						lastSendTime = socketService.getLastSendTime();
						diffTime = currentTime - lastSendTime;	// 获取差时
						waitTime = HEARTBEAT_TIME - diffTime;
						
						if(waitTime<0)
						{
							waitTime = 0;
						}
						// 大于心跳差时，开始心跳
						if(diffTime >= HEARTBEAT_TIME)
						{
							systemParamsUpdateBusiness.heartBeat_0003();
							waitTime = HEARTBEAT_TIME;
						}
						else
						{
							Log.log("间隔上次发送时间还差"+waitTime+"ms，继续等待");
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					
					try
					{
						sleep(waitTime);
					} 
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
				Log.log("心跳线程结束....");
			}
		}.start();
	}
	
	@Override
	public void onDestroy()
	{
		heartbeatRunning = false;//心跳线程结束标记位
		unbindService(this);
		super.onDestroy();
	}
	
	@Override
	public void onServiceConnected(ComponentName name, IBinder binder)
	{
		if(name.getClassName().equals(BaseActivity.DESCRIPTOR_EUIPMENTSERVICE))
		{
			equipmentService = (IEquipmentService)(((EquipmentBinder)binder).getService());
		}
		else if(name.getClassName().equals(BaseActivity.DESCRIPTOR_SOCKETSERVICE))
		{
			socketService = (ISocketService)(((SocketBinder)binder).getService());
		}
		else if(name.getClassName().equals(BaseActivity.DESCRIPTOR_DATABASEERVICE))
		{
			dataBaseService = (IDataBaseService)(((DataBaseBinder)binder).getService());
		}
		
		if(socketService!= null && equipmentService!= null && dataBaseService != null)
		{
			systemParamsUpdateBusiness = new SystemParamsUpdateBusiness(this, equipmentService, socketService,dataBaseService);
		}
		Log.log("心跳服务，绑定服务成功ComponentName = " + name.getClassName());
	}

	@Override
	public void onServiceDisconnected(ComponentName componentName)
	{
	}
}


