/**
 * $RCSfile: DataBaseService.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-30  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.service;

import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.database.SqLitBase;
import com.sunnada.baseframe.util.Log;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;


/**
 * <p>Title: DataBaseService</p> 
 * <p>Description: </p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author ����
 * @date 2013-4-30
 * @version 1.0
 */

public class DataBaseService extends Service implements IDataBaseService
{
	private SqLitBase mDataBase = null;
	
	public class DataBaseBinder extends Binder
	{
		public DataBaseService getService() 
		{
			return DataBaseService.this;
		}
	}
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		Log.log("DataBaseService����...");
		
		mDataBase = new SqLitBase(this);
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		Log.log("DataBaseService�ر�...");
	}

	@Override
	public IBinder onBind(Intent intent) 
	{
		return new DataBaseBinder();
	}
	
	@Override
	public boolean executeUpdateSQL(String sql)
	{
		return mDataBase.executeUpdateSQL(sql);
	}

	@Override
	public boolean executeUpdateSQLWithParams(String sql, String[] params)
	{
		return mDataBase.executeUpdateSQL(sql, params);
	}

	@Override
	public boolean executeUpdateSQLWithTransaction(String[] sqls)
	{
		return mDataBase.executeUpdateSQLWithTransaction(sqls);
	}

	@Override
	public List<Map<String,String>> executeQuerySQL(String sql)
	{
		return mDataBase.executeQuerySQL(sql);
	}

	@Override
	public List<Map<String,String>> executeQuerySQLWithParams(String sql, String[] params)
	{
		return mDataBase.executeQuerySQL(sql, params);
	}

	@Override
	public List<Map<String,String>> executeQuerySQLByPage(String sql, int start, int count)
	{
		return mDataBase.executeQuerySQL(sql, start, count);
	}

	@Override
	public List<Map<String,String>> executeQuerySQLByPageAndParams(String sql, String[] params, int start, int count)
	{
		return mDataBase.executeQuerySQL(sql, params, start, count);
	}
}


