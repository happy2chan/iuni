package com.sunnada.baseframe.service;

import java.util.Map;

import com.sunnada.baseframe.bean.Package;
import com.sunnada.baseframe.bean.PsamInfo;
import com.sunnada.bluetooth.BluetoothClient;

public interface IEquipmentService
{
	// 初始化设备
	public boolean initEquiment();
	// 资源卸载
	public boolean releaseEquiment();
	public boolean isInitEquiment();
	// 握手
	public boolean handshake();
	
	// 初始化声音
	public void initVoice();
	// 获取外设的类型
	public String getDeviceType();
	// 设置设备类型 intelligent blue
	public boolean setDeviceType(String type);
	// 设置蓝牙设备接口
	public boolean setBluetoothClient(BluetoothClient bluetoothClient);
	
	// 获取设备的厂商编码
	public int getFactoryCode();
	// 获取各类版本号
	public String readVersion(byte type);
	// 获取M3应用版本
	public String readM3AppVersion();
	// 获取M3驱动版本
	public String readM3DriverVersion();
	// M3文件升级
	// @param path_name 	文件路径 如"/mnt/sdcard/"
	// @param file_name 	文件名 如"CPU-M66-DRV-STM32CV1.0-20130106_1130.bin"
	// @param file_type		0x11：升级驱动，0x81：应用升级
	// @param updata_sum	文件校验和4个字节(低位在前, 高位在后, 其中实际只使用低位两个字节) 根据终端部说crc可以乱传, 我擦, 那我就传一个0吧
	// @return 				成功为true 失败为false
	public boolean m3Update(String path_name, String file_name, byte file_type, int updata_sum);
	
	// 获取PSAM卡信息
	public Map<String, String> getPsamInfo();
	public PsamInfo getPsamInfo(int type);
	// 获取主备服务器信息
	// 获取主服务器IP
	public String getIp1();
	// 获取主服务器端口
	public int getPort1();
	// 获取备用服务器IP
	public String getIp2();
	// 获取备用服务器端口
	public int getPort2();
	
	// 读取PSAM卡号码
	public String getPsamId();
	// 设置PSAM卡号码
	public void setPsamId(String psamId);
	// 读取PSAM卡信息
	public boolean readPsamInfo();
	// 读取PSAM信息，根据传递参数，可选择从缓存获取，以节省时间
	public boolean readPsamInfoFull(boolean isReload);
	// 获取PSAM卡加密密钥
	public byte[] readPsamExtKey();
	// PSAM卡加密
	public boolean psamEncryptByte(byte[] data, byte len, byte type);
	public byte[] psamEncrypt(Package dataPackage);
	// 检测开机密码
	public boolean PSAM_CheckPasswd(String pin);
	// 更新密码
	public boolean PSAM_ModifyPasswd(String oldpin, String newpin);
	
	// 联通写白卡
	// 读取ICCID
	public String readIccid();
	// 判断成卡、白卡
	// 0错误1白卡2空卡
	public int isWhiteCard(byte szSimType);
	// 联通写白卡
	boolean writeSimCard(byte[] szImsi, byte szSimType);
	// 写短消息中心号
	boolean writesmscentre(String szSmsc, byte szSimType);
	
	// 打印机
	// 打印机状态检测
	public int checkPrinterStatus();
	// 检测打印机是否有纸
	public boolean printerHasPaper();
	// 设置打印机字体
	public void setPrintFont(int size);
	// 打印文本信息
	public void printText(String printContent);
	// 打印LOGO
	public void printLogo(int[] logoData);
	public void printLogoFile(String fileName);
	// 开始打印
	public void startPrint();
	// 黑标是否存在
	public boolean isBlackLabelExist();
	// 是否打印成功
	public boolean isPrintSuccess();
	// 获取打印错误描述
	public String getPrintErrorMsg();
	// 获取打印错误描述
	public String getPrintErrorMessage();
	// 获取广告语
	public String getAdverInfo();
	
	// 移动写卡
	// 读取移动号卡的序列号
	public String readMobilSimSerialNumber();
	// 读取移动号卡的ICCID
	public String readMobilSimIccidNumber();
	// 读取移动号卡的IMSI
	public String readMobilSimImsiNumber();
	// 读取移动号卡的短信中心号码
	public String readMobilSimSmscNumber();
	
	// 是否是移动白卡
	public boolean isFJMobileWhiteCard();
	// 读取福州移动卡序列号
	public String readFZMobileSearialNumber();
	// 读取福州移动白卡ICCID
	public String readFZMobileIccid();
	// 写福州移动白卡ICCID
	public boolean writeFZMobileIccid(String iccid);
	// 读取福州移动IMSI
	public String readFZMobileImsi();
	// 写福州移动IMSI
	public boolean writeFZMobileImsi(String imsi);
	// 读取福州移动短信中心号码
	public String readFZMobileSmsCenter();
	// 写入福州移动短信中心号码
	public boolean writeFZMobileSmsCenter(String smsCenter);
	// 更新福州移动PUK和PIN
	public boolean updateFZMobilePinAndPuk(String oldPin1, String newPin1, String oldPin2, String newPin2, 
			String oldPuk1, String newPuk1, String oldPuk2, String newPuk2);
	// 写福州移动白卡
	public boolean writeFZMobileWhiteCard(String iccid, String imsi, String smsCenter, String oldPin1, String newPin1, 
			String oldPin2, String newPin2, String oldPuk1, String newPuk1, String oldPuk2, String newPuk2, String ki);
	
	// 获取登陆密钥
	public byte[] getLoginKey();
	// 设置当前登陆密钥
	public void setLoginKey(byte[] loginKey);
	
	// 获取IMSI
	public String getIMSI();
	// 获取IMEI号
	public String getIMEI();
	// 获取小区信息
	public String[] getLacCi();
	// 获取本地手机号码
	public String getLocalPhoneNumber();
	// 设置本地手机号码
	public void setLocalPhoneNumber(String localPhoneNumber);
	
	// 获取程序版本
	public String getFrameVersion();
	// 是否软加密
	public boolean isSoftEncrypt();
	// 检测当前终端是否可用
	public boolean isTerminateCanUse();
	// 设置终端是否可用
	public void setTerminateCanUse(boolean isTerminateCanUse);
	// 设置注销
	public void setOffline(boolean enable);
	// 检测是否注销
	public boolean isOffline();
	
	public byte[] calcCrc16(byte[] data, char c);
	public String getLastknownError();
}

