/**
 * $RCSfile: DownLoadService.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-21  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;

import com.sunnada.baseframe.bean.Statics;
import com.sunnada.baseframe.util.FileUtil;
import com.sunnada.baseframe.util.HttpURLConnectionUtil;
import com.sunnada.baseframe.util.Log;
import com.sunnada.baseframe.util.SystemUtil;

/**
 * <p>Title: DownLoadService</p> 
 * <p>Description: 后台下载服务</p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author 丘富铨
 * @date 2013-4-21
 * @version 1.0
 */
public class DownLoadService extends Service
{
	/**
	 * 下载服务的广播Action
	 */
	public static final String BROADCAST_ACTION = "com.sunnada.baseframe.service.DownLoadService";

	private SharedPreferences downLoadPreferences = null;
	
	private OnDownloadListener onDownloadListener = null;//下载监听器
	
	/**
	 * 下载线程映射，保存所有下载线程的句柄
	 */
	private Map<String, DownLoadThread> downloadThreads = new HashMap<String, DownLoadService.DownLoadThread>();
	
	@Override
	public IBinder onBind(Intent intent)
	{
		return new DownloadServiceBinder();
	}
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		downLoadPreferences = getSharedPreferences("downLoadPreferences", MODE_PRIVATE);
		Log.log("DownLoadService启动");
	}
	
	/**
	 * 是否当前网址在下载队列中
	 * @param url
	 * @return
	 */
	public boolean isUrlDownloading(String url)
	{
		return downloadThreads.containsKey(url);
	}
	
	/**
	 * 通过url获取该url下载的断点
	 * @param url
	 * @return
	 */
	public long getDownloadBreakPoint(String url)
	{
		return downLoadPreferences.getLong(url, 0L);
	}
	
	@Override
	public void onStart(Intent intent, int startId)
	{
		super.onStart(intent, startId);
	}

	@Override
	public void onDestroy()
	{
		Log.log("DownLoadService销毁");
		super.onDestroy();
	}
	
	/**
	 * 添加下载服务
	 * @param url
	 */
	public void addDownloadTask(String url)
	{
		if(!downloadThreads.containsKey(url))
		{
			DownLoadThread dt = new DownLoadThread(url, 0);
			downloadThreads.put(url, dt);
			dt.start();
			
		}
	}
	
	/**
	 * 使用断点下载
	 * @param url
	 */
	public void addDownloadTaskWithBreakPoint(String url)
	{
		if(!downloadThreads.containsKey(url))
		{
			DownLoadThread dt = new DownLoadThread(url, downLoadPreferences.getLong(url, 0));
			downloadThreads.put(url, dt);
			dt.start();
		}
	}
	
	public void stopDownload(String url)
	{
		if(downloadThreads.containsKey(url))
		{
			downloadThreads.get(url).setRuning(false);
		}
	}
	
	public void setOnDownloadListener(OnDownloadListener onDownloadListener)
	{
		this.onDownloadListener = onDownloadListener;
	}



	/**
	 * 下载线程
	 * @author aquan
	 */
	private class DownLoadThread extends Thread
	{
		private boolean runing = true;	//线程运行标记位
		private String url = null;	//下载的url
		private long breakPoint = 0L;//下载开始断点
		
		public DownLoadThread(String url, long breakPoint)
		{
			super();
			this.url = url;
			this.breakPoint = breakPoint;
			
		}
		
		@Override
		public void run()
		{

			if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
			{
				Log.log("SD卡未装载");
				if(onDownloadListener!=null)
				{
					onDownloadListener.onDownloadFaild(url, "SD卡未装载");
				}
				return;
			}

			HttpURLConnection urlConnection = null;
			RandomAccessFile accessFilec = null;// new RandomAccessFile("", "rwd");
			InputStream is = null;
			long tempBreakPoint = breakPoint;
			long currentLength = 0;
			
			
			String saveFileName = Statics.PATH_APK+"/"+FileUtil.getFileNameByUrl(url)+".tmp";//后缀为tmp
			File saveFile = new File(saveFileName);

			try
			{
				Log.log("下载地址"+ url+"    -->  "+saveFileName);
				urlConnection = (HttpURLConnection) new URL(url).openConnection();
				urlConnection.setConnectTimeout(HttpURLConnectionUtil.CONNECT_TIMEOUT);
				urlConnection.setReadTimeout(HttpURLConnectionUtil.READ_TIMEOUT);

				if (urlConnection.getResponseCode() != 200)
				{
					Log.log(saveFileName,"服务端响应错误,错误编码:"+urlConnection.getResponseCode());
					if(onDownloadListener!=null)
					{
						onDownloadListener.onDownloadFaild(url, "服务端响应错误");
					}
					return;
				}
				currentLength = urlConnection.getContentLength();
				urlConnection.disconnect();

				Log.log(saveFileName,"下载APK大小:" + currentLength);

				if (!saveFile.getParentFile().exists())
				{//如果文件夹不存在则创建
					saveFile.getParentFile().mkdirs();
				}

				if (saveFile.exists() && breakPoint <= 0)
				{// 如果下载文件存在且断点从0开始则删除文件从新开始下载
					Log.log(saveFileName,"下载文件存在且断点从0开始则删除文件");
					saveFile.delete();
					tempBreakPoint = 0;

				}
				
				if(!saveFile.exists())
				{//如果下载文件不存在，则从新下载
					tempBreakPoint = 0;
				}
				
				if(saveFile.exists() && tempBreakPoint == currentLength)
				{//如果文件存在，且断点大小等于文件长度,说明已经下载完成
				
					//此处无需做任何动作，在finally中会处理
				
				}else
				{
					accessFilec = new RandomAccessFile(saveFile, "rwd");
					if (currentLength != accessFilec.length())
					{// 
						accessFilec.setLength(currentLength);
					}

					urlConnection = (HttpURLConnection) new URL(url).openConnection();
					urlConnection.setConnectTimeout(HttpURLConnectionUtil.CONNECT_TIMEOUT);
					urlConnection.setReadTimeout(HttpURLConnectionUtil.READ_TIMEOUT);
					urlConnection.setRequestProperty("Range", "bytes=" + tempBreakPoint + "-" + currentLength);

					Log.log(saveFileName,"开始断点:" + tempBreakPoint);
					accessFilec.seek(tempBreakPoint);

					byte[] buf = new byte[1024 * 2];
					int len = 0;

					is = urlConnection.getInputStream();

					while ((len = is.read(buf)) > 0)
					{//如果读出的流大于0且当前的running为true
						
						accessFilec.write(buf, 0, len);
						tempBreakPoint += len;
						downLoadPreferences.edit().putLong(url, tempBreakPoint).commit();//保存当前断点
						if(onDownloadListener!=null)
						{
							onDownloadListener.onDownloadUpdate(url, tempBreakPoint, currentLength);
						}
						
						if(!runing)
						{//如果中途取消下载
							Log.log(saveFileName, "取消下载");
							break;
						}
						
					}
				}

			} catch (Exception e)
			{
				e.printStackTrace();

			} finally
			{

				if (is != null)
				{
					try
					{
						is.close();
					} catch (IOException e)
					{
						e.printStackTrace();
					}
				}

				if (accessFilec != null)
				{
					try
					{
						accessFilec.close();
					} catch (IOException e)
					{
						e.printStackTrace();
					}
				}

				if (urlConnection != null)
				{
					urlConnection.disconnect();
				}
				
				Log.log(saveFileName,"当前断点:" + tempBreakPoint);
				Log.log(saveFileName,"是否下载完成:" + (tempBreakPoint == currentLength) +"   当前下载量:"+tempBreakPoint+"  总需下载量:"+currentLength);
				
				if (tempBreakPoint == currentLength && saveFile != null)
				{
					Log.log(saveFileName,"下载成功");
					String trueName = saveFileName.substring(0, saveFileName.lastIndexOf("."));
					File trueFile = new File(trueName);
					saveFile.renameTo(trueFile);
					
					SystemUtil.installApk(trueFile, DownLoadService.this);
					
					if(onDownloadListener!=null)
					{
						onDownloadListener.onDownloadSuccess(url);
					}
					
				}else if(runing==false)
				{
					if(onDownloadListener!=null)
					{
						onDownloadListener.onDownloadCancel(url,tempBreakPoint,currentLength);
					}
					
				}else
				{
					if(onDownloadListener!=null)
					{
						onDownloadListener.onDownloadFaild(url,null);
					}
				}
			}
			
			if(downloadThreads.containsKey(url))
			{//将当前线程从downloadThreads移除
				downloadThreads.remove(url);
			}
		
		}

		public void setRuning(boolean runing)
		{
			this.runing = runing;
		}
	}
	
	/**
	 * 下载监听器
	 * @author aquan
	 *
	 */
	public interface OnDownloadListener
	{
		public void onDownloadUpdate(String url,long currentPoint,long fileSize);
		public void onDownloadSuccess(String url);
		public void onDownloadFaild(String url,String message);
		public void onDownloadCancel(String url,long currentPoint,long fileSize);
	}
	
	
	/**
	 * 下载服务绑定对象
	 * @author aquan
	 *
	 */
	public class DownloadServiceBinder extends Binder
	{
		public DownLoadService getDownLoadService()
		{
			return DownLoadService.this;
		}
	}
	
}


