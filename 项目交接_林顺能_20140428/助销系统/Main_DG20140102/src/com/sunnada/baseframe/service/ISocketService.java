package com.sunnada.baseframe.service;

import com.sunnada.baseframe.bean.Package;
import java.util.List;

/**
 * File: ISocketService.java
 * Description: 
 * 修改此文件需注意以下几点:   ******************************* 重要说明&务必遵守 *******************************
 *							1、新加函数接口应该在旧有函数的后面。
							2、尽量避免删除旧有函数，如果真的要删的话，可以保留函数名字作为占位，返回一个错误码之类的来解决。
							3、不能改变原来的接口声明顺序。
 * @author 丘富铨
 * @date 2013-3-7
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public interface ISocketService
{
	// 获取当前网络是否连接
	public boolean isNetworkOk();
	
	public void setIP1Adress(String ip);
	public void setPort1(int port);
	public void setIP2Adress(String ip);
	public void setPort2(int port);
	public int send(byte[] buf,int offset,int len);
	public boolean connect(boolean isFirstIp);
	public void disConnect();
	public boolean isConnect();
	
	// 获取当前连接的IP
	public String getIp();
	// 获取当前连接的端口
	public int getPort();
	
	// 接收主命令为id的包
	public int receive(String id, List<Package> packages, int timeout);
	// 从接收包队列中删除主命令为id的包
	public void removeSameIdPackages(String id);
	
	// 获取错误编码
	public int getLastknownErrCode();
	// 获取错误描述
	public String getLastKnowError();
	// 获取最后一次数据发送的时间
	public long getLastSendTime();
}
