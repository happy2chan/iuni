package com.sunnada.baseframe.service;

import java.util.List;
import java.util.Map;

/**
 * File: IDataBaseService.aidl
 * Description: 数据库基础服务
 * 修改此文件需注意以下几点:   ******************************* 重要说明&务必遵守 *******************************
 *							1、新加函数接口应该在旧有函数的后面。
							2、尽量避免删除旧有函数，如果真的要删的话，可以保留函数名字作为占位，返回一个错误码之类的来解决。
							3、不能改变原来的接口声明顺序。
 * @author 丘富铨
 * @date 2013-3-7
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public interface IDataBaseService
{
	/**
	 * 执行更新语句，包括增、删、改
	 * @param sql	sql语句
	 * @return	成功返回true失败返回false
	 * add by  qiufq
	 */
	public boolean executeUpdateSQL(String sql);

	/**
	 * 执行更新语句，包括增、删、改
	 * @param sql	sql语句
	 * @param params	参数
	 * @return	成功返回true失败返回false
	 * add by  qiufq
	 */
	public boolean executeUpdateSQLWithParams(String sql, String[] params);
	
	/**
	 * 批量执行更新语句，包括增、删、改，采用事务机制，提高速度
	 * @param sqls	sql数组
	 * @return	成功返回true失败返回false
	 * add by  qiufq
	 */
	public boolean executeUpdateSQLWithTransaction(String[] sqls);
	
	/**
	 * 执行查询语句
	 * @param sql	sql语句
	 * @return	成功返回List对象，失败返回null
	 */
	public List<Map<String,String>> executeQuerySQL(String sql);
	
	/**
	 * 执行查询语句
	 * @param sql	sql语句
	 * @param params	参数
	 * @return	成功返回List对象，失败返回null
	 */
	public List<Map<String,String>> executeQuerySQLWithParams(String sql, String[] params);
	
	/**
	 * 
	 * 执行分页查询语句
	 * @param sql	sql语句
	 * @param start 开始位置
	 * @param count 查询条数
	 * @return	成功返回List对象，失败返回null
	 * @return
	 */
	public List<Map<String,String>> executeQuerySQLByPage(String sql,int start,int count);
	
	/**
	 * 
	 * 执行分页查询语句
	 * @param sql	sql语句
	 * @param params	参数
	 * @param start 开始位置
	 * @param count 查询条数
	 * @return	成功返回List对象，失败返回null
	 * @return
	 */
	public List<Map<String,String>> executeQuerySQLByPageAndParams(String sql, String[] params, int start, int count);
}
