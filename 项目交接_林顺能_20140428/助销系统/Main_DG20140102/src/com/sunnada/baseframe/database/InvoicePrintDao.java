package com.sunnada.baseframe.database;

import java.util.List;
import java.util.Map;

import android.content.Context;

public class InvoicePrintDao extends SqLitBase
{
	private static final String TABLE_NAME 				= "INVOICEPRINT";         // 表名
	private static final String COLUMN_TELNUM 			= "s_telnum";			  // 手机号码
	private static final String COLUMN_BUSIMONEY 	    = "s_busimoney";		  // 交易金额
	private static final String COLUMN_INVOICENUM 	    = "s_invoicenum";		  // 发票号码
	private static final String COLUMN_PRINTTIME		= "s_printtime";		  // 打印时间
	private static final String COLUMN_INVOICETYPE 	    = "s_invoicetype";		  // 交易类型
	private static final String COLUMN_INVOICETIME 		= "s_invoicetime";		  // 交易时间
	private static final String COLUMN_INVOICEBUSNO     = "s_invoicebusno";       // 流水号
	
	private static final String COLUMN_BILLDATE         = "s_billdate";           // 账期
	private static final String COLUMN_BILLTYPE         = "s_billtype";           // 发票类型
	private static final String COLUMN_PRINTTYPE        = "s_printtype";          // 打印类型
	private static final String COLUMN_PRINTCONTENT     = "s_printcontent";       // 打印内容
	private static final String COLUMN_PRINTWAY         = "s_printway";           // 打印方法("1"为打印，"2"为补打,"3"为重打)
	
	private static final String createSQL = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME 
											+ " (" + 
													"_id integer PRIMARY KEY autoincrement, " + // 自增主键
													COLUMN_TELNUM +" TEXT," + 					// 手机号码
													COLUMN_BUSIMONEY +" TEXT,"  + 				// 交易金额
													COLUMN_INVOICENUM +" TEXT," + 				// 发票号码
													COLUMN_PRINTTIME + " TEXT," + 			    // 打印时间
													COLUMN_INVOICETYPE + " TEXT," +			    // 交易类型 
													COLUMN_INVOICETIME + " TEXT," +             // 交易时间
													COLUMN_INVOICEBUSNO + " TEXT," +            // 流水号
													COLUMN_BILLDATE + " TEXT," +                // 账期
													COLUMN_BILLTYPE + " TEXT," +                // 发票类型
													COLUMN_PRINTTYPE + " TEXT," +               // 打印类型
													COLUMN_PRINTCONTENT + " TEXT," +            // 打印内容
													COLUMN_PRINTWAY + " TEXT)";					// 打印方法("1"为打印，"2"为补打,"3"为重打)
	
	// 构造
	public InvoicePrintDao(Context context) 
	{
		super(context);
		CreatTable();
	}
	
	// 每次开机执行一次建表语句
	public void CreatTable() 
	{
		// 建立开机上报更新参数表
		Exec(createSQL);
	}

	// 查询INVOICEPRINT表中最新的那条记录的全部信息
	public List<Map<String, String>> queryPrintInfo()
	{
		String sql = "select * from INVOICEPRINT where  _id=(SELECT max(_id) from INVOICEPRINT)";
		List<Map<String, String>> result = SelectList(sql);
		if (null == result || 0 == result.size()) 
		{
			return null;
		}
		return result;
	}

	public boolean insertPrintInfo(String telnum, String busimoney, String invoicenum, 
			                       String printtime, String invoicetype, String invoicetime,
			                       String invoicebusno, String billDate, String billType,
			                       String printType,String printcontent,String printway)
	{
		String sql = String.format("insert into PARAMS (s_telnum, s_busimoney, s_invoicenum," +
			                     	" s_printtime, s_invoicetype, s_invoicetime, s_invoicebusno," +
			                     	" s_billdate, s_billtype, s_printtype, s_printcontent, s_printway)"
			                     	+ " values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', " +
			                     	"'%s', '%s', '%s', '%s')", telnum, busimoney, invoicenum,
			                     	printtime, invoicetype, invoicetime, invoicebusno, billDate,
			                     	billType, printType, printcontent, printway);
	    return Exec(sql);
	}
	
    //查询打印数
	//使用说明：查看表中有多少条记录,并为其重新取个名字
    public List<Map<String, String>> queryprintcount(String date)
    {
		String sql = "select count(*) as s_telnum from "+TABLE_NAME+" where s_printtime like '"+date+"%'";
		List<Map<String, String>> result = SelectList(sql);
		if (null == result || 0 == result.size()) 
		{
			return null;
		}
		return result;
    }

    public void delete(String date)
    {
	    String sql = String.format("delete from INVOICEPRINT where s_printtime < '%s'", date);
	    Exec(sql);
    }

    //删除数据
  	public void clean(String date)
  	{
  		delete(date);
  	}

    //获取打印数
  	public int getPrintcount(String date)
  	{
  		return queryprintcount(date).size();
  	}

    // 获取打印信息
 	public List<Map<String, String>> getInvoicePrintInfo()
 	{
 		return queryPrintInfo();
 	}

 	public void setPrintInfo(String telnum, String busimoney, String invoicenum,String printtime, String invoicetype, String invoicetime,String invoicebusno,String billDate,String billType,String printType,String printcontent,String printway)
 	{
 		insertPrintInfo(telnum, busimoney, invoicenum,printtime, invoicetype, invoicetime, invoicebusno,billDate,billType,printType,printcontent,printway);
 	}

  	
	public void DeleteParamsValue(String type, String id) 
	{
		String sql = String.format("delete from PARAMS where s_type = '%s' and _id='%s'", type, id);
		Exec(sql);
	}
}
