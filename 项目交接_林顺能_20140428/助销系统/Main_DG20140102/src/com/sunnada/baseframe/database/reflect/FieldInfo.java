package com.sunnada.baseframe.database.reflect;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldInfo
{
	public String fieldName();
	public String describe();
	public String fieldType();
	public boolean isPrimaryKey() default false;
}
