/**
 * $RCSfile: MenuDao.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-30  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.service.IDataBaseService;


/**
 * <p>Title: MenuDao</p> 
 * <p>Description: </p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author 丘富铨
 * @date 2013-4-30
 * @version 1.0
 */
public class MenuDao extends BaseDao
{
	private static final String TABLE_NAME = "Menu";
	private static final String COLUMN_ID = "id";//类型
	private static final String COLUMN_MENU_ID = "menuId";//类型
	
	public MenuDao(IDataBaseService dataBaseService)
	{
		super(dataBaseService);
		
		String sql = "create table if not exists "+TABLE_NAME+" ("+
																	COLUMN_ID+" integer primary key autoincrement,"+
																	COLUMN_MENU_ID+" text unique"+
																")";
		
		executeUpdateSQL(sql);
	}

	
	public void insertMneuId(String menuId)
	{
		String deleteSQL = "delete from "+TABLE_NAME+" where "+COLUMN_MENU_ID+" = '"+menuId+"'";
		String insertSQL = "insert into "+TABLE_NAME+"("+COLUMN_MENU_ID+") values('"+menuId+"')";
		
		executeUpdateSQL(deleteSQL);
		executeUpdateSQL(insertSQL);
	}
	
	public void deleteAllMneuId()
	{
		String deleteSQL = "delete from "+TABLE_NAME;
		executeUpdateSQL(deleteSQL);
	}
	
	public Map<String,String> getAllMenuId()
	{
		Map<String,String> menuIds = new HashMap<String,String>();
		
		String sql = "select * from "+TABLE_NAME;
		List<Map<String, String>> maps = new ArrayList<Map<String,String>>();
		maps = executeQuerySQL(sql);
		for(Map<String,String> map:maps)
		{
			String id = (String) map.get(COLUMN_MENU_ID);
			menuIds.put(id, id);
		}
		
		return menuIds;
	}
}


