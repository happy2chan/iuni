package com.sunnada.baseframe.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.database.Cursor;

import com.sunnada.baseframe.bean.PhoneModel;

public class PhoneParamsDao extends SqLitBase
{
	public static final String	TABLE_NAME				= "PHONE_PARAMS";
	public static final String	COLUMN_ID				= "p_id";
	public static final String	COLUMN_NAME				= "p_name";
	public static final String	COLUMN_BAND				= "p_band";

	public static final String	COLUMN_COLOR			= "p_color";
	public static final String	COLUMN_PIC				= "p_pic";
	public static final String	COLUMN_BIGPIC			= "p_bigpics";
	public static final String	COLUMN_PRICE			= "p_price";
	public static final String	COLUMN_SCREEN			= "p_screen";
	public static final String	COLUMN_SCREENPARAM		= "p_screenparam";
	public static final String	COLUMN_OP				= "p_op";
	public static final String	COLUMN_NET				= "p_net";
	public static final String	COLUMN_CORENUM			= "p_corenum";
	public static final String	COLUMN_CPU				= "p_cpu";
	public static final String	COLUMN_SIZE				= "p_size";
	public static final String	COLUMN_ROM				= "p_rom";
	public static final String	COLUMN_RAM				= "p_ram";
	public static final String	COLUMN_EXTEND			= "p_extend";
	public static final String	COLUMN_BATTERYSTORAGE	= "p_batterystorage";
	public static final String	COLUMN_BATTERY			= "p_battery";
	public static final String	COLUMN_MUSICFORMAT		= "p_musicformat";
	public static final String	COLUMN_VEDIOFORMAT		= "p_vedioformat";
	public static final String	COLUMN_JAVA				= "p_java";
	public static final String	COLUMN_RADIO			= "p_radio";
	public static final String	COLUMN_RECORD			= "p_record";
	public static final String	COLUMN_CAMERA1			= "p_camera1";
	public static final String	COLUMN_CAMERA2			= "p_camera2";
	public static final String	COLUMN_ZOOM				= "p_zoom";
	public static final String	COLUMN_DATATRANS		= "p_datatrans";
	public static final String	COLUMN_GPS				= "p_gps";
	public static final String	COLUMN_CALLTIME			= "p_calltime";
	public static final String	COLUMN_WAITTIME			= "p_waittime";
	public static final String	COLUMN_ACCESS			= "p_access";

	// 构造
	public PhoneParamsDao(Context con)
	{
		super(con);
		// CreatTable();
	}

	/*
	// 每次开机执行一次建表语句
	public void CreatTable()
	{
		// 建立开机上报更新参数表
		Exec(createSQL);
	}
	*/

	public void clear()
	{
		Exec("delete from " + TABLE_NAME + " where 1=1");
	}
	
	public void insertData(PhoneModel model)
	{
		System.out.println("insert into Phone_Params...");
		String sql = "INSERT INTO " + TABLE_NAME + " (" + COLUMN_ID + "," + COLUMN_BAND + "," + COLUMN_NAME + ","
				+ COLUMN_COLOR + "," + COLUMN_PIC + "," + COLUMN_BIGPIC + "," + COLUMN_PRICE + "," + COLUMN_SCREEN
				+ "," + COLUMN_SCREENPARAM + "," + COLUMN_OP + "," + COLUMN_NET + "," + COLUMN_CORENUM + ","
				+ COLUMN_CPU + "," + COLUMN_SIZE + "," + COLUMN_ROM + "," + COLUMN_RAM + "," + COLUMN_EXTEND + ","
				+ COLUMN_BATTERY + "," + COLUMN_BATTERYSTORAGE + "," + COLUMN_MUSICFORMAT + "," + COLUMN_VEDIOFORMAT
				+ "," + COLUMN_JAVA + "," + COLUMN_RADIO + "," + COLUMN_RECORD + "," + COLUMN_CAMERA1 + ","
				+ COLUMN_CAMERA2 + "," + COLUMN_ZOOM + "," + COLUMN_DATATRANS + "," + COLUMN_GPS + ","
				+ COLUMN_CALLTIME + "," + COLUMN_WAITTIME + "," + COLUMN_ACCESS
				+ ") VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',"
				+ "'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',"
				+ "'%s','%s','%s','%s','%s','%s')";
		sql = String.format(sql, model.getId(), model.getBand(), model.getName(), model.getColor(), model.getPicPath(),
				model.getBigPic(), model.getPrice(), model.getScreenSize(), model.getScreenParam(), model.getOp(),
				model.getNet(), model.getCoreNum(), model.getCpu(), model.getSize(), model.getRom(), model.getRam(),
				model.getExtendRom(), model.getBattery(), model.getBatteryStorage(), model.getMusicFormat(),
				model.getVedioFormat(), model.getJava(), model.getRadio(), model.getRecord(), model.getCamera1(),
				model.getCamera2(), model.getZoom(), model.getDataTrans(), model.getGps(), model.getCallTime(),
				model.getWaitTime(), model.getAccess());
		Exec(sql);
	}
	
	
	
	// 根据手机ID获取手机参数信息
	public PhoneModel getModelParamById(String phoneId)
	{
		if (phoneId == null)
		{
			return null;
		}
		String sql = "select " + COLUMN_ID + "," + COLUMN_BAND + "," + COLUMN_NAME + "," + COLUMN_COLOR + ","
				+ COLUMN_PIC + "," + COLUMN_BIGPIC + "," + COLUMN_PRICE + "," + COLUMN_SCREEN + ","
				+ COLUMN_SCREENPARAM + "," + COLUMN_OP + "," + COLUMN_NET + "," + COLUMN_CORENUM + "," + COLUMN_CPU
				+ "," + COLUMN_SIZE + "," + COLUMN_ROM + "," + COLUMN_RAM + "," + COLUMN_EXTEND + "," + COLUMN_BATTERY
				+ "," + COLUMN_BATTERYSTORAGE + "," + COLUMN_MUSICFORMAT + "," + COLUMN_VEDIOFORMAT + "," + COLUMN_JAVA
				+ "," + COLUMN_RADIO + "," + COLUMN_RECORD + "," + COLUMN_CAMERA1 + "," + COLUMN_CAMERA2 + ","
				+ COLUMN_ZOOM + "," + COLUMN_DATATRANS + "," + COLUMN_GPS + "," + COLUMN_CALLTIME + ","
				+ COLUMN_WAITTIME + "," + COLUMN_ACCESS + " from " + TABLE_NAME + " where " + COLUMN_ID + " = '%s'";
		sql = String.format(sql, phoneId);

		List<Map<String, String>> mapList = executeQuerySQL(sql);
		if (mapList != null && mapList.size() > 0)
		{
			Map<String, String> map = mapList.get(0);
			return getModel(map);
		}
		return null;
	}
	
	// 模糊查询，查询手机品牌、型号中包含关键字的手机Model
	public List<PhoneModel> getRelativePhones(String band, String name)
	{
		if (band == null || name == null)
		{
			return null;
		}
		String sql = "select * from " + TABLE_NAME + " where " + COLUMN_BAND + " like " + "'%" + band.trim() + "%' and " + COLUMN_NAME + " like " + "'%" + name.trim() +"%'";
		List<Map<String, String>> mapList = executeQuerySQL(sql);
		List<PhoneModel> result = new ArrayList<PhoneModel>();
		if (mapList != null && mapList.size() > 0)
		{
			for(int i = 0;i < mapList.size(); i++)
			{
				Map<String, String> map = mapList.get(i);
				result.add(getModel(map));
			}
		}
		return result;
	}
	
	// 模糊查询，查询手机品牌、型号中包含关键字的手机Model
	public List<PhoneModel> getRelativePhones(String band)
	{
		if (band == null)
		{
			return null;
		}
		String sql = "select * from " + TABLE_NAME + " where " + COLUMN_BAND + " like " + "'%" + band.trim() + "%' or " + COLUMN_NAME + " like " + "'" + band.trim() +"%'";
		List<Map<String, String>> mapList = executeQuerySQL(sql);
		List<PhoneModel> result = new ArrayList<PhoneModel>();
		if (mapList != null && mapList.size() > 0)
		{
			for(int i = 0;i < mapList.size(); i++)
			{
				Map<String, String> map = mapList.get(i); 
				result.add(getModel(map));
			}
		}
		return result;
	}
	
	// 增加数据方法  20131108 新增
	public <T> long insert(PhoneModel model)
	{
		return insert(model);
	}
	
	// 模糊查询，查询手机品牌、型号中包含关键字的手机Model   20131108 新增
	public <T> List<PhoneModel> getRelativePhones(T t)
	{
		List<Map<String, String>> selectList = new ArrayList<Map<String, String>>();
		Cursor cursor = mistySearch(t, 0, 0, null, null);
		if(cursor != null)
		{
			if (cursor.getCount() == 0)
			{
				return null;
			}
			else
			{
				cursor.moveToFirst();
				// 获取每条记录列名
				String[] colName = cursor.getColumnNames();
				while (!cursor.isAfterLast())
				{
					Map<String, String> map = new HashMap<String, String>();
					// 循环获取列名对应的数据
					for (int j = 0; j < colName.length; j++)
					{
						map.put(colName[j], cursor.getString(cursor.getColumnIndex(colName[j])));
					}
					selectList.add(map);
					cursor.moveToNext();
				}
			}
			// 关闭游标
			cursor.close();
		}
		List<PhoneModel> result = new ArrayList<PhoneModel>();
		if(selectList.size() > 0)
		{
			for(int i = 0; i < selectList.size(); i++)
			{
				result.add(getModel(selectList.get(i)));
			}
		}
		return result;
	}
	
	// 解析手机参数信息
	private PhoneModel getModel(Map<String, String> map)
	{
		PhoneModel model = new PhoneModel();
		model.setId(map.get(COLUMN_ID));
		model.setAccess(map.get(COLUMN_ACCESS));
		model.setBand(map.get(COLUMN_BAND));
		model.setBattery(map.get(COLUMN_BATTERY));
		model.setBatteryStorage(map.get(COLUMN_BATTERYSTORAGE));
		model.setBigPic(map.get(COLUMN_BIGPIC));
		model.setCallTime(map.get(COLUMN_CALLTIME));
		model.setCamera1(map.get(COLUMN_CAMERA1));
		model.setCamera2(map.get(COLUMN_CAMERA2));
		model.setColor(map.get(COLUMN_COLOR));
		model.setCoreNum(map.get(COLUMN_CORENUM));
		model.setCpu(map.get(COLUMN_CPU));
		model.setDataTrans(map.get(COLUMN_DATATRANS));
		model.setExtendRom(map.get(COLUMN_EXTEND));
		model.setGps(map.get(COLUMN_GPS));
		model.setId(map.get(COLUMN_ID));
		model.setJava(map.get(COLUMN_JAVA));
		model.setMusicFormat(map.get(COLUMN_MUSICFORMAT));
		model.setName(map.get(COLUMN_NAME));
		model.setNet(map.get(COLUMN_NET));
		model.setOp(map.get(COLUMN_OP));
		model.setPicPath(map.get(COLUMN_PIC));
		model.setPrice(map.get(COLUMN_PRICE));
		model.setRadio(map.get(COLUMN_RADIO));
		model.setRam(map.get(COLUMN_RAM));
		model.setRecord(map.get(COLUMN_RECORD));
		model.setRom(map.get(COLUMN_ROM));
		model.setScreenParam(map.get(COLUMN_SCREENPARAM));
		model.setScreenSize(map.get(COLUMN_SCREEN));
		model.setSize(map.get(COLUMN_SIZE));
		model.setVedioFormat(map.get(COLUMN_VEDIOFORMAT));
		model.setWaitTime(map.get(COLUMN_WAITTIME));
		model.setZoom(map.get(COLUMN_ZOOM));
		model.isParamLoaded = true;
		return model;
	}
}
