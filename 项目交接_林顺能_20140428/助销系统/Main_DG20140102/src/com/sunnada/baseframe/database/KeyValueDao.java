/**
 * $RCSfile: KeyValueDao.java,v $
 * $Revision: 1.1  $
 * $Date: 2013-4-30  $
 *
 * Copyright (c) 2013 qiufq Incorporated. All rights reserve
 *
 * This software is the proprietary information of Bettem, Inc.
 * Use is subject to license terms.
 */

package com.sunnada.baseframe.database;

import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.service.IDataBaseService;

/**
 * <p>Title: KeyValueDao</p> 
 * <p>Description: </p> 
 * <p>Copyright: Copyright (c) 2013</p> 
 * @author 丘富铨
 * @date 2013-4-30
 * @version 1.0
 */

public class KeyValueDao extends BaseDao
{
	public static final String KEY_PRINT_AD = "KEY_PRINT_AD";//凭条广告的键值
	public static final String KEY_UPDATE_URL = "KEY_UPDATE_URL";//系统更新地址

	public static final String DEVICETYPE="INFO_DEVICETYPE";//设备类型。一体机/蓝牙
	
	public static final String BLUETOOTH="INFO_BLUETOOTH";//蓝牙设备信息
	
	// PSAM卡中存放的一些信息
	public static final String PSAMID="INFO_PSAMID";//psamid
	public static final String PSAM_AGENT_NAME="INFO_PSAM_AGENT_NAME";//代理商名称
	public static final String PSAM_AGENT_ADDR="INFO_PSAM_AGENT_ADDR";//代理商地址
	public static final String PSAM_AGENT_TEL="INFO_PSAM_AGENT_TEL";//代理商电话
	public static final String PSAM_MAIN_IP="INFO_PSAM_MAIN_IP";//主服务器IP
	public static final String PSAM_BACKUP_IP="INFO_PSAM_BACKUP_IP";//备用服务器IP
	public static final String PSAM_MAIN_PORT="INFO_PSAM_MAIN_PORT";//主服务器端口
	public static final String PSAM_BACKUP_PORT="INFO_PSAM_BACKUP_PORT";//备用服务器端口
	public static final String PSAM_CREATE_DATE="INFO_PSAM_CREATE_DATE";//psam开卡时间
	public static final String PSAM_END_DATE="INFO_PSAM_END_DATE";//psam到期时间
	
	public static final String LOGIN_PWD="INFO_LOGINPWD";//LOGIN_PWD，加密之后存储

	private static final String TABLE_NAME = "KeyValue";
	private static final String COLUMN_KEY = "key";//类型
	private static final String COLUMN_VALUE = "value";//类型
	
	public KeyValueDao(IDataBaseService dataBaseService)
	{
		super(dataBaseService);
		
		String sql = "create table if not exists "+TABLE_NAME+" ("+
																	COLUMN_KEY+" text primary key,"+
																	COLUMN_VALUE+" text"+
																")";
		executeUpdateSQL(sql);
	}
	
	public void insert(String key,String value)
	{
		String deleteSQL = "delete from "+TABLE_NAME+" where "+COLUMN_KEY+" = '"+key+"'";
		String insertSQL = "insert into "+TABLE_NAME+"("+COLUMN_KEY+","+COLUMN_VALUE+") values('"+key+"','"+value+"')";
		
		executeUpdateSQL(deleteSQL);
		executeUpdateSQL(insertSQL);
	}
	
	public String get(String key)
	{
		String sql = "select * from "+TABLE_NAME +" where "+COLUMN_KEY+" = '"+key+"'";
		List<Map<String,String>> maps = executeQuerySQL(sql);
		if(maps != null && maps.size() > 0)
		{
			return (String)maps.get(0).get(COLUMN_VALUE);
		}
		return null;
	}
}