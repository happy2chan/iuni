/**  
* @Project: ${SuYuan anti-counterfeiting}
* @Title: ${SettingDataBaseAdapter.java}
* @Package ${com.com.china.AndroidProject}
* @Description: ${用于访问查询历史数据库的操作类}
* @author leiming alpspig@qq.com
* @date ${2011-6-20} 
* @Copyright: ${2011} 福建三元达软件  Inc. All rights reserved.
* @version V1.0  
*/
package com.sunnada.baseframe.database;

import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

/**
 * @ClassName ${BluetoothInfoDBAdapter}
 * @Description ${用于访问蓝牙信息数据库的操作类，可保存当前连接的蓝牙以及提取之前保存的蓝牙直接用于连接}
 * @author  
 * @date ${2011-6-20}
 * ${tags}
 */
public class BluetoothInfoDBAdapter extends DBadapter 
{

	// 数据库表名
	private static final String	DB_TABLE		= "BLUETOOTH";
	
	// 表中一条数据的ID
	public static final String	KEY_ID			= "_id";
	
	//蓝牙连接的标识
	public static final String	KEY_BT_TAG			= "bluetooth_tag";
	
	// 表中字段：蓝牙名称
	public static final String	KEY_BT_NAME		= "bluetooth_name";												

	// 表中字段：蓝牙地址
	public static final String	KEY_BT_MAC		= "bluetooth_mac";												


	
	
	/** 
	* @Title: ${SettingDataBaseAdapter} 
	* @Description: ${构造函数}
	* @param ${Context}     
	* @return ${}   无返回值
	* @throws 
	*/
	public BluetoothInfoDBAdapter(Context context)
	{
		super(context);
	}


	

	/** 
	* @Title: ${insertData} 
	* @Description: ${插入一条数据}
	* @param ${int，int，int}    偏好设置数据
	* @return ${long}   Insert返回值，-1为不成功
	* @throws 
	*/
	public long insertData(String t_tag,String t_name, String t_mac)
	{
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_BT_TAG, t_tag);
		initialValues.put(KEY_BT_NAME, t_name);
		initialValues.put(KEY_BT_MAC, t_mac);
		
		return mDB.insert(DB_TABLE, KEY_ID, initialValues);
	}

	/** 
	* @Title: ${deleteData} 
	* @Description: ${删除一条数据}
	* @param ${long rowId}    KEY_ID 
	* @return ${boolean}   delete返回值
	* @throws 
	*/
	public boolean deleteData(long rowId)
	{
		return mDB.delete(DB_TABLE, KEY_ID + "=" + rowId, null) > 0;
	}

	/** 
	* @Title: ${fetchAllData} 
	* @Description: ${通过Cursor查询所有数据}
	* @param ${}   
	* @return ${Cursor}   数据集合
	* @throws 
	*/
	public Cursor fetchAllData()
	{
		Cursor mCursor = mDB.query(DB_TABLE, new String[] { KEY_ID, KEY_BT_TAG, KEY_BT_NAME, KEY_BT_MAC }, null, null, null, null, null);
		if (mCursor != null)
		{
			mCursor.moveToFirst();
		}
		return mCursor;
	}
	
	public Cursor fetchDataByTag(String t_tag)
	{
		Cursor mCursor = mDB.query(DB_TABLE, new String[] { KEY_ID, KEY_BT_TAG, KEY_BT_NAME, KEY_BT_MAC }, KEY_BT_TAG+"='"+t_tag+"'", 
				null, null, null, null);
		if (mCursor != null)
		{
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	/** 
	* @Title: ${fetchData} 
	* @Description: ${查询指定数据}
	* @param ${long rowId}   KEY_ID
	* @return ${Cursor}   数据集合
	* @throws SQLException
	*/
	public Cursor fetchData(long rowId) throws SQLException
	{
		Cursor mCursor = mDB.query(true, DB_TABLE, new String[] { KEY_ID, KEY_BT_TAG, KEY_BT_NAME, KEY_BT_MAC }, KEY_ID + "=" + rowId, 
				null, null, null, null, null);
		if (mCursor != null)
		{
			mCursor.moveToFirst();
		}
		return mCursor;
	}
                                   
	/** 
	* @Title: ${updateData} 
	* @Description: ${更新偏好设置}
	* @param ${int，int，int}    偏好设置数据
	* @return ${boolean}  	true为设置成功
	* @throws 
	*/
	public boolean updateData(String t_tag,String t_name, String t_mac)
	{
		ContentValues args = new ContentValues();
		
		args.put(KEY_BT_NAME, t_name);
		args.put(KEY_BT_MAC, t_mac);
		
		return mDB.update(DB_TABLE, args, KEY_BT_TAG+"='"+t_tag+"'", null) > 0;
	}
	
	/** 
	* @Title: ${getDate} 
	* @Description: ${从当前游标处获得指定数据}
	* @param ${Cursor cur}    数据集合
	* @param ${String key}    字段名称
	* @return ${int}  	true为成功
	* @throws 
	*/
	public String getDate(Cursor cur, String key)
	{
		return cur.getString(cur.getColumnIndex(key));
	}
	
	public static String getBluetoothMac(Context context,String t_tag)
    {
        // 读取偏好设置数据库
		BluetoothInfoDBAdapter s_MyDataBaseAdapter  = new BluetoothInfoDBAdapter(context);
        s_MyDataBaseAdapter.open();
		Cursor cur = s_MyDataBaseAdapter.fetchAllData();
		s_MyDataBaseAdapter.close();
		
		String mac = null;
		String tag="";
		while (!cur.isAfterLast()) 
		{	
			tag = s_MyDataBaseAdapter.getDate(cur, BluetoothInfoDBAdapter.KEY_BT_TAG);
			if(tag.equalsIgnoreCase(t_tag)) 
			{
				mac = s_MyDataBaseAdapter.getDate(cur, BluetoothInfoDBAdapter.KEY_BT_MAC); 
				break;
			}			
			cur.moveToNext();  
		}
		
		cur.close();
		return mac;
    }
	
	public static void setBluetoothMac(Context context,String name, String mac,String tag)
	{
		BluetoothInfoDBAdapter s_MyDataBaseAdapter  = new BluetoothInfoDBAdapter(context);
		Cursor cur = null;
		
		try {
			s_MyDataBaseAdapter.open();
	        cur = s_MyDataBaseAdapter.fetchDataByTag(tag);
	        if (cur.getCount() == 0)
	        {
	        	s_MyDataBaseAdapter.insertData(tag,name, mac);
	        }else
	        {
	        	s_MyDataBaseAdapter.updateData(tag,name, mac);
	        }
	        
	        
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(cur!=null) cur.close();
			if(s_MyDataBaseAdapter!=null)  s_MyDataBaseAdapter.close();
		}
        
	}
	
	/**
	 * 获取数据库中保存的默认蓝牙信息	
	 * @param context
	 * @param t_tag				标志,从配置文件中取(一个系统一个标志?)
	 * @return
	 */
	public static Map<String, String> getBluetoothData(Context context,String t_tag)
    {
        // 读取偏好设置数据库
		BluetoothInfoDBAdapter s_MyDataBaseAdapter = new BluetoothInfoDBAdapter(context);
        s_MyDataBaseAdapter.open();
		Cursor cur = s_MyDataBaseAdapter.fetchAllData();
		s_MyDataBaseAdapter.close();
		
		Map<String, String> btInfo = new HashMap<String, String>();
		String tag="";
		while (!cur.isAfterLast()) 
		{	
			tag = s_MyDataBaseAdapter.getDate(cur, BluetoothInfoDBAdapter.KEY_BT_TAG);
			if(tag.equalsIgnoreCase(t_tag)) 
			{
				btInfo.put(BluetoothInfoDBAdapter.KEY_BT_NAME, s_MyDataBaseAdapter.getDate(cur, BluetoothInfoDBAdapter.KEY_BT_NAME));
				btInfo.put(BluetoothInfoDBAdapter.KEY_BT_MAC, s_MyDataBaseAdapter.getDate(cur, BluetoothInfoDBAdapter.KEY_BT_MAC));
				break;
			}			
			cur.moveToNext();  
		}
		
		cur.close();
		return btInfo;
    }
}
