package com.sunnada.baseframe.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper 
{
	private static final String 	DB_NAME 		= "Main_DG.db";		// 数据库名称
	private static final int 		DB_VERSION 		= 2;				// 数据库版本
	
	public DBHelper(Context context) 
	{
		super(context, DB_NAME, null, DB_VERSION);
	}
	
	/**
	 * 在数据库第一次生成的时候会调用这个方法，也就是说，只有在创建数据库的时候才会调用，当然也有一些其它的情况，
	 * 一般我们在这个方法里边生成数据库表。
	 */
	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		Log.e("22", "database  onCreate ....");
		// 创建蓝牙信息表
		String db_bluetooth= "CREATE TABLE BLUETOOTH ("
				+ "_id TEXT PRIMARY KEY, " 
				+ "bluetooth_tag TEXT,"
				+ "bluetooth_name TEXT,"
				+ "bluetooth_mac TEXT)";
		db.execSQL(db_bluetooth);
		
		// 创建系统配置信息表
		String db_syscfginfo= "CREATE TABLE SYSCFGINFO ("
				+ "_id TEXT PRIMARY KEY, " 
				+ "file_name TEXT,"
				+ "file_path TEXT,"
				+ "file_tag TEXT)";
		db.execSQL(db_syscfginfo);
		
		// 创建异常交易记录表
		String db_errorinfo = "CREATE TABLE ERRORINFO ("
				+ "busi_id TEXT PRIMARY KEY, " 
				+ "busi_pasmid TEXT,"
				+ "busi_type TEXT,"
				+ "busi_bill TEXT,"
				+ "busi_reason TEXT," 
				+ "busi_crc TEXT,"
				+ "busi_state TEXT)"; 
		db.execSQL(db_errorinfo);
		
		// 创建终端交易记录表
		String  db_bussinessshow= "CREATE TABLE IF NOT EXISTS BUSSINESS ("
				+ "busi_id TEXT PRIMARY KEY, " 
				+ "busi_user_no TEXT,"
				+ "busi_tel TEXT," 
				+ "busi_name TEXT,"
				+ "busi_money REAL,"
				+ "busi_state TEXT," 
				+ "busi_bill TEXT," 
				+ "busi_time TEXT," 
				+ "busi_pos_money TEXT," 
				+ "busi_type TEXT)";
		db.execSQL(db_bussinessshow);
		// 创建手机参数信息表
		String  db_phone_params = "CREATE TABLE IF NOT EXISTS PHONE_PARAMS("
				+ "p_access TEXT,p_band TEXT,p_battery TEXT,p_batterystorage TEXT,p_bigpics TEXT,p_calltime TEXT,p_camera1 TEXT,p_camera2 TEXT,p_color TEXT,p_corenum TEXT,p_cpu TEXT,p_datatrans TEXT,p_extend TEXT,p_gps TEXT,p_id TEXT,p_java TEXT,p_musicformat TEXT,p_name TEXT,p_net TEXT,p_op TEXT,p_pic TEXT,p_price TEXT,p_radio TEXT,p_ram TEXT,p_record TEXT,p_rom TEXT,p_screen TEXT,p_screenparam TEXT,p_size TEXT,p_vedioformat TEXT,p_waittime TEXT,p_zoom TEXT)";
		db.execSQL(db_phone_params);
		// 创建套餐参数信息表
		String db_product_params = "CREATE TABLE IF NOT EXISTS PRODUCT_PARAMS("
				+ "r_id TEXT,r_price TEXT,r_des TEXT,r_area_long TEXT,r_area_short TEXT,r_area_code TEXT,r_contract_code TEXT,r_contract_id TEXT,r_contract_name TEXT,r_contract_date TEXT,r_product_id TEXT,r_product_code TEXT,r_product_name TEXT,r_product_price TEXT,r_product_type TEXT,r_activity_type TEXT,r_activity_content TEXT,r_activity_details TEXT,r_marketing_tag TEXT,r_pic TEXT,r_bigpics TEXT,r_flow TEXT,r_call_time TEXT,r_msg_count TEXT,r_call_free TEXT,r_call_fee TEXT,r_flow_fee TEXT,r_vedio_call TEXT,r_other TEXT,r_is_gift TEXT,r_gift_info TEXT,r_gift1 TEXT,r_gift2 TEXT,r_gift3 TEXT,r_gift4 TEXT,r_gift5 TEXT)";
		db.execSQL(db_product_params);
		// 创建推荐包参数信息
		String db_recommendpack = "CREATE TABLE IF NOT EXISTS RECOMMEND_PACK_PARAMS("
				+ "p_id TEXT,p_access TEXT,p_band TEXT,p_battery TEXT,p_batterystorage TEXT,p_bigpics TEXT,p_calltime TEXT,p_camera1 TEXT,p_camera2 TEXT,p_color TEXT,p_corenum TEXT,p_cpu TEXT,p_datatrans TEXT,p_extend TEXT,p_gps TEXT,p_model_id TEXT,p_java TEXT,p_musicformat TEXT,p_name TEXT,p_net TEXT,p_op TEXT,p_pic TEXT,p_price TEXT,p_radio TEXT,p_ram TEXT,p_record TEXT,p_rom TEXT,p_screen TEXT,p_screenparam TEXT,p_size TEXT,p_vedioformat TEXT,p_waittime TEXT,p_zoom TEXT,r_pack_id TEXT,r_des TEXT,r_area_long TEXT,r_area_short TEXT,r_area_code TEXT,r_contract_id TEXT,r_contract_code TEXT,r_contract_name TEXT,r_contract_date TEXT,r_product_id TEXT,r_product_code TEXT,r_product_name TEXT,r_product_price TEXT,r_product_type TEXT,r_activity_type TEXT,r_activity_content TEXT,r_activity_details TEXT,r_marketing_tag TEXT,r_pic TEXT,r_bigpics TEXT,r_flow TEXT,r_call_time TEXT,r_msg_count TEXT,r_call_free TEXT,r_call_fee TEXT,r_flow_fee TEXT,r_vedio_call TEXT,r_other TEXT,r_is_gift TEXT,r_gift_info TEXT,r_gift1 TEXT,r_gift2 TEXT,r_gift3 TEXT,r_gift4 TEXT,r_gift5 TEXT)";
		db.execSQL(db_recommendpack);
		
		// 创建手机参数历史记录信息表
		String  db_history_phone_params = "CREATE TABLE IF NOT EXISTS HS_PHONE_PARAMS("
				+ "p_access TEXT,p_band TEXT,p_battery TEXT,p_batterystorage TEXT,p_bigpics TEXT,p_calltime TEXT,p_camera1 TEXT,p_camera2 TEXT,p_color TEXT,p_corenum TEXT,p_cpu TEXT,p_datatrans TEXT,p_extend TEXT,p_gps TEXT,p_id TEXT,p_java TEXT,p_musicformat TEXT,p_name TEXT,p_net TEXT,p_op TEXT,p_pic TEXT,p_price TEXT,p_radio TEXT,p_ram TEXT,p_record TEXT,p_rom TEXT,p_screen TEXT,p_screenparam TEXT,p_size TEXT,p_vedioformat TEXT,p_waittime TEXT,p_zoom TEXT,p_edit_time TEXT)";
		db.execSQL(db_history_phone_params);
		// 创建套餐参数历史记录信息表
		String db_history_product_params = "CREATE TABLE IF NOT EXISTS HS_PRODUCT_PARAMS("
				+ "r_id TEXT,r_price TEXT,r_des TEXT,r_area_long TEXT,r_area_short TEXT,r_area_code TEXT,r_contract_code TEXT,r_contract_id TEXT,r_contract_name TEXT,r_contract_date TEXT,r_product_id TEXT,r_product_code TEXT,r_product_name TEXT,r_product_price TEXT,r_product_type TEXT,r_activity_type TEXT,r_activity_content TEXT,r_activity_details TEXT,r_marketing_tag TEXT,r_pic TEXT,r_bigpics TEXT,r_flow TEXT,r_call_time TEXT,r_msg_count TEXT,r_call_free TEXT,r_call_fee TEXT,r_flow_fee TEXT,r_vedio_call TEXT,r_other TEXT,r_is_gift TEXT,r_gift_info TEXT,r_gift1 TEXT,r_gift2 TEXT,r_gift3 TEXT,r_gift4 TEXT,r_gift5 TEXT,r_edit_time TEXT)";
		db.execSQL(db_history_product_params);
		// 创建推荐包历史记录参数信息
		String db_history_recommendpack = "CREATE TABLE IF NOT EXISTS HS_RECOMMEND_PACK_PARAMS("
				+ "p_id TEXT,p_access TEXT,p_band TEXT,p_battery TEXT,p_batterystorage TEXT,p_bigpics TEXT,p_calltime TEXT,p_camera1 TEXT,p_camera2 TEXT,p_color TEXT,p_corenum TEXT,p_cpu TEXT,p_datatrans TEXT,p_extend TEXT,p_gps TEXT,p_model_id TEXT,p_java TEXT,p_musicformat TEXT,p_name TEXT,p_net TEXT,p_op TEXT,p_pic TEXT,p_price TEXT,p_radio TEXT,p_ram TEXT,p_record TEXT,p_rom TEXT,p_screen TEXT,p_screenparam TEXT,p_size TEXT,p_vedioformat TEXT,p_waittime TEXT,p_zoom TEXT,r_pack_id TEXT,r_des TEXT,r_area_long TEXT,r_area_short TEXT,r_area_code TEXT,r_contract_id TEXT,r_contract_code TEXT,r_contract_name TEXT,r_contract_date TEXT,r_product_id TEXT,r_product_code TEXT,r_product_name TEXT,r_product_price TEXT,r_product_type TEXT,r_activity_type TEXT,r_activity_content TEXT,r_activity_details TEXT,r_marketing_tag TEXT,r_pic TEXT,r_bigpics TEXT,r_flow TEXT,r_call_time TEXT,r_msg_count TEXT,r_call_free TEXT,r_call_fee TEXT,r_flow_fee TEXT,r_vedio_call TEXT,r_other TEXT,r_is_gift TEXT,r_gift_info TEXT,r_gift1 TEXT,r_gift2 TEXT,r_gift3 TEXT,r_gift4 TEXT,r_gift5 TEXT,r_edit_time TEXT)";
		db.execSQL(db_history_recommendpack);
		// 创建拍照图片上传信息表
		String db_pack_info = "CREATE TABLE IF NOT EXISTS IMG_LOG ("
				+ "BLLNUM TEXT, PHOTONUM TEXT, PHOTONAMES TEXT, IMG_UPLOAD_LOG TEXT, FILEPATH TEXT, REMOTEPATH TEXT)";
		db.execSQL(db_pack_info);
	}
	
	/**
	 * 当数据库需要升级的时候，Android系统会主动的调用这个方法。一般我们在这个方法里边删除数据表，并建立新的数据表，
	 * 当然是否还需要做其他的操作，完全取决于应用的需求。
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		Log.e("22", "database  update ....");
		db.execSQL("DROP TABLE IF EXISTS BLUETOOTH");
		db.execSQL("DROP TABLE IF EXISTS PARAMS");
		db.execSQL("DROP TABLE IF EXISTS SYSCFGINFO");
		db.execSQL("DROP TABLE IF EXISTS COMMINFO");
		db.execSQL("DROP TABLE IF EXISTS DOPRLOG");
		db.execSQL("DROP TABLE IF EXISTS ERRORINFO");
		db.execSQL("DROP TABLE IF EXISTS INITINVOICE");
		db.execSQL("DROP TABLE IF EXISTS INVOICEPRINT");
		db.execSQL("DROP TABLE IF EXISTS CANCELINVOICE");
		db.execSQL("DROP TABLE IF EXISTS IMG_LOG");
		db.execSQL("DROP TABLE IF EXISTS IMG_UPLOAD_LOG");
		db.execSQL("DROP TABLE IF EXISTS INVOICENUM");
		db.execSQL("DROP TABLE IF EXISTS BUSSINESS");
		
		db.execSQL("DROP TABLE IF EXISTS PHONE_PARAMS");
		db.execSQL("DROP TABLE IF EXISTS PRODUCT_PARAMS");
		db.execSQL("DROP TABLE IF EXISTS RECOMMEND_PACK_PARAMS");
		db.execSQL("DROP TABLE IF EXISTS HS_PHONE_PARAMS");
		db.execSQL("DROP TABLE IF EXISTS HS_PRODUCT_PARAMS");
		db.execSQL("DROP TABLE IF EXISTS HS_RECOMMEND_PACK_PARAMS");
		db.execSQL("DROP TABLE IF EXISTS IMG_LOG");
		onCreate(db);
	}

}
