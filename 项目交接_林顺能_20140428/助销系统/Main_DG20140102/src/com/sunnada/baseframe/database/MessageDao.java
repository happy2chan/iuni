package com.sunnada.baseframe.database;

import java.util.List;
import java.util.Map;

import com.sunnada.baseframe.service.IDataBaseService;

public class MessageDao extends BaseDao 
{
	public static final String TABLE_NAME 				= "message_v1";
	public static final String COLUMN_ID 				= "id";					// 主键
	public static final String COLUMN_TYPE 				= "t_type";				// 消息类型
	public static final String COLUMN_DATE 				= "t_date";				// 消息日期
	public static final String COLUMN_CONTENT 			= "t_content";			// 消息内容
	public static final String COLUMN_ISREAD 			= "t_is_read";			// 是否已经查看
	public static final String COLUMN_NUMBER 			= "t_number";			// 消息编码
	
	public MessageDao(IDataBaseService dataBaseService) 
	{
		super(dataBaseService);
		
		String sql = "create table if not exists "+TABLE_NAME+" ("+
																	COLUMN_ID+" integer primary key autoincrement,"+
																	COLUMN_TYPE+" text,"+
																	COLUMN_DATE+" text,"+
																	COLUMN_ISREAD+" text,"+
																	COLUMN_NUMBER+" text,"+
																	COLUMN_CONTENT+" text"+
																")";
		
		executeUpdateSQL(sql);
	}
	
	/**
	 * 添加一个收件箱邮件
	 * @param number 编号
	 * @param type	类型
	 * @param date 日期
	 * @param content 内容
	 */
	public void insert(String number,String type,String date,String content)
	{
		String insertSQL = "insert into "+TABLE_NAME+"("+COLUMN_NUMBER+","+COLUMN_TYPE+","+COLUMN_DATE+","+COLUMN_CONTENT+","+COLUMN_ISREAD+") values('"+number+"','"+type+"','"+date+"','"+content+"','1')";
		executeUpdateSQL(insertSQL);
	}
	

	/**
	 * 获取收件箱所有内容，数据了大的话会大量消耗设备资源，不建议调用
	 * @return
	 */
	public List<Map<String,String>> getAll()
	{
		String sql = "select * from "+TABLE_NAME +" order by "+COLUMN_DATE+" desc";
		List<Map<String,String>> datas = executeQuerySQL(sql);
		executeUpdateSQL("update "+TABLE_NAME+" set "+COLUMN_ISREAD+" = '0'");
		return datas;
	}
	
	/**
	 * 分页获取收件箱数据，默认按照时间排序
	 * @param start	 开始索引
	 * @param count 个数
	 * @return
	 */
	public List<Map<String,String>> getPage(int start,int count)
	{
		String sql = "select * from "+TABLE_NAME +" order by "+COLUMN_DATE+" desc";
		List<Map<String,String>> datas = executeQuerySQL(sql,start,count);
		executeUpdateSQL("update "+TABLE_NAME+" set "+COLUMN_ISREAD+" = '0'");
		return datas;
	}
	

	/**
	 * 获取收件箱邮件的总数
	 * @return
	 */
	public int getRecrodCount()
	{
		List<Map<String,String>> datas = executeQuerySQL("select count(*) as count from "+TABLE_NAME);
		if(datas!=null && datas.size()>0)
		{
			String countStr = datas.get(0).get("count");
			
			return Integer.parseInt(countStr);
		}
		return 0;
	}
	
	/**
	 * 获取未读信息的总数
	 * @return
	 */
	public int getUnReadMessageCount()
	{
		List<Map<String,String>> datas = executeQuerySQL("select count(*) as count from "+TABLE_NAME +" where "+COLUMN_ISREAD+"='1'");
		if(datas!=null && datas.size()>0)
		{
			String countStr = datas.get(0).get("count");
			return Integer.parseInt(countStr);
		}
		return 0;
	}
}