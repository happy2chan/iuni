package com.sunnada.baseframe.database;

import java.util.List;
import java.util.Map;


import android.content.Context;

/**
 * 拍照图片上传日志的数据库操作类
 * 
 * @author acer
 * 
 */
public class ImgLogDBAdapter extends SqLitBase
{
	private static final String	DB_TABLE		= "IMG_LOG";													// 表名
	private static final String	COL_BLLNUM		= "BLLNUM";													// 流水号
	private static final String	COL_PHOTONUM	= "PHOTONUM";													// 照片数
	private static final String	COL_PHOTONAMES	= "PHOTONAMES";												// 照片文件名

	private static final String	DB_UPLOAD_TABLE	= "IMG_UPLOAD_LOG";											// 服务器上传表的表名
	private static final String	COL_FILEPATH	= "FILEPATH";													// 文件路径
	private static final String	COL_REMOTEPATH	= "REMOTEPATH";												// 要提交的目录

	private static final String	createSQL		= "CREATE TABLE IF NOT EXISTS " + DB_TABLE
														+ " ("
														+ // "_id integer PRIMARY KEY autoincrement, "
															// + // 自增主键
														COL_BLLNUM + " TEXT," + COL_PHOTONUM + " TEXT,"
														+ COL_PHOTONAMES + " TEXT," + DB_UPLOAD_TABLE + " TEXT,"
														+ COL_FILEPATH + " TEXT," + COL_REMOTEPATH + " TEXT)";

	public ImgLogDBAdapter(Context context)
	{
		super(context);
	}

	public void CreatTable()
	{
		// 建立开机上报更新参数表
		Exec(createSQL);
	}

	public void addImgLog(String bllnum, String photonum, String photonames)
	{
		String sql = "INSERT INTO " + DB_TABLE + " (" + COL_BLLNUM + "," + COL_PHOTONUM + "," + COL_PHOTONAMES
				+ ") VALUES ('%s','%s','%s')";
		sql = String.format(sql, bllnum, photonum, photonames);
		Exec(sql);
		/*
		 * 
		 * ContentValues cv = new ContentValues(); cv.put(COL_BLLNUM,bllnum);
		 * cv.put(COL_PHOTONUM, photonum); cv.put(COL_PHOTONAMES, photonames);
		 * getDb().insert(DB_TABLE, null, cv); close();
		 */
	}

	public void deleteImgLog(String bllnum)
	{
		String sql = "DELETE FROM " + DB_TABLE + " WHERE " + COL_BLLNUM + " = '%s'";
		sql = String.format(sql, bllnum);
		Exec(sql);

		// int result = getDb().delete(DB_TABLE, COL_BLLNUM+"= ?",new
		// String[]{bllnum});
		// getDb().execSQL("DELETE FROM "+DB_TABLE+" WHERE "+COL_BLLNUM+" = "+bllnum);

		// close();
		return;
	}

	/**
	 * 根据流水号查询单行数据
	 * 
	 * @param bllnum
	 * @return 
	 */
	public Map<?, ?> queryImgLog(String bllnum)
	{
		String sql = "SELECT " + COL_BLLNUM + ", " + COL_PHOTONAMES + ", " + COL_PHOTONUM + " FROM " + DB_TABLE
				+ " WHERE " + COL_BLLNUM + " = " + bllnum;
		List<Map<String, String>> maps = executeQuerySQL(sql);
		if (maps != null && maps.size() > 0)
		{
			return maps.get(0);
		}
		else
		{
			return null;
		}
		// Cursor cursor = getDb().rawQuery(sql, null);
		// Map result = this.cursorToMap(cursor);
		// close();
		// return result;
	}

	/**
	 * 查询所有的照片日志
	 * 
	 * @return
	 */
	public List<Map<String, String>> queryImgLogs()
	{
		String sql = "SELECT " + COL_BLLNUM + ", " + COL_PHOTONAMES + ", " + COL_PHOTONUM + " FROM " + DB_TABLE;
		List<Map<String, String>> maps = executeQuerySQL(sql);
		return maps;
		// Cursor cursor = getDb().rawQuery(sql, null);
		// List<Map<String, String>> logs = this.cursorToList(cursor);
		// close();
		// return logs;
	}

	/**
	 * 查询图片上传服务器失败的日志
	 * 
	 * @return
	 */
	public List<Map<String, String>> queryUploadLogs()
	{
		String sql = "SELECT " + COL_FILEPATH + ", " + COL_REMOTEPATH + " FROM " + DB_UPLOAD_TABLE;
		List<Map<String, String>> maps = executeQuerySQL(sql);
		return maps;
		// Cursor cursor = getDb().rawQuery(sql, null);
		// List<Map<String, String>> logs = this.cursorToList(cursor);
		// close();
		// return logs;
	}

	/**
	 * 插入图片上传服务器的信息
	 * 
	 * @param filePath
	 * @param remotePath
	 */
	public void addUploadLog(String filePath, String remotePath)
	{
		String sql = "INSERT INTO " + DB_UPLOAD_TABLE + " (" + COL_FILEPATH + "," + COL_REMOTEPATH
				+ ") VALUES ('%s','%s')";
		sql = String.format(sql, filePath, remotePath);
		Exec(sql);

		// ContentValues cv = new ContentValues();
		// cv.put(COL_FILEPATH, filePath);
		// cv.put(COL_REMOTEPATH, remotePath);
		// getDb().insert(DB_UPLOAD_TABLE, null, cv);
		// close();
	}

	public void deleteImgUploadLog(String filePath)
	{
		String sql = "DELETE FROM " + DB_UPLOAD_TABLE + " WHERE " + COL_FILEPATH + " = '%s'";
		sql = String.format(sql, filePath);
		Exec(sql);

		// getDb().delete(DB_UPLOAD_TABLE, COL_FILEPATH + " = ?",
		// new String[] { filePath });
		// close();
	}
}
