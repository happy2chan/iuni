package com.sunnada.baseframe.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.sunnada.baseframe.bean.CRCData;

public class SystemParamsDao extends SqLitBase
{
	private static final String TABLE_NAME 				= "PARAMS";
	private static final String COLUMN_TYPE 			= "s_type";				// 类型
	private static final String COLUMN_CRC 				= "s_crc";				// CRC
	private static final String COLUMN_CODE 			= "s_code";				// 编码
	private static final String COLUMN_VALUE 			= "s_value";			// 值
	private static final String COLUMN_RELATION_CODE 	= "s_relationcode";		// 父编码
	private static final String COLUMN_PARAMS 			= "s_param";			// 不知道
	
	private static final String createSQL = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME 
											+ " (" + 
													"_id integer PRIMARY KEY autoincrement, " + // 自增主键
													COLUMN_TYPE +" TEXT," + 					// 类型
													COLUMN_CRC +" TEXT,"  + 					// crc
													COLUMN_CODE +" TEXT," + 					// 编码
													COLUMN_VALUE + " TEXT," + 					// 值
													COLUMN_RELATION_CODE + " TEXT," +			// 父编码 
													COLUMN_PARAMS + " TEXT)";					// 鬼知道这个是什么参数
	
	// 构造
	public SystemParamsDao(Context context) 
	{
		super(context);
		CreatTable();
	}
	
	// 每次开机执行一次建表语句
	public void CreatTable() 
	{
		// 建立开机上报更新参数表
		Exec(createSQL);
	}

	// 更新参数表的数据 = "01"，先删除表中数据，再重新插入权限数据
	public void DeleteParamsValue(String type) 
	{
		String sql = String.format("delete from PARAMS where s_type = '%s'", type);
		Exec(sql);
	}
	
	public void DeleteParamsValue(String type, String id) 
	{
		String sql = String.format("delete from PARAMS where s_type = '%s' and _id='%s'", type, id);
		Exec(sql);
	}
	
	// 插入参数表的数据 = "01"，先删除表中数据，再重新插入权限数据
	// insertiype 0:菜单 1:套餐
	public void InsertParamsValue(String type, String code, String value, String ralation, String param) 
	{

		String sql = String.format("insert into PARAMS (s_type, s_code, s_value, s_relationcode, s_param) " +
				"values ('%s', '%s', '%s', '%s', '%s')", type, code, value, ralation, param);
		Exec(sql);
	}
	
	// 更新参数表的
	// type: 0x00-代表各版本CRC
	// code: 0x01-菜单 0x02-2G套餐 0x03-增值业务 0x04-特服业务 0x05-选号规则 0x06-品牌 0x07-开机欢迎语 0x08-打印信息 0x09-收件箱 0x0A-3G套餐
	public void UpdateParamsCrc(String type, String code, String value)
	{
		String sql = String.format("select * from PARAMS where s_type = '%s' and s_code = '%s'", type, code);
		Log.e("xxxx", sql);
		
		List<Map<String, String>> result = SelectList(sql);
		// 如果存在数据，执行更新
		if (result != null) 
		{
			Log.e("xxxx", "存在记录, 更新");
			sql = String.format("update PARAMS set s_value = '%s' where s_type = '%s' and s_code = '%s'", value, type, code);
		}
		// 不存在则插入
		else
		{
			Log.e("xxxx", "不存在记录, 插入");
			sql = String.format("insert into PARAMS (s_type, s_code, s_value) values ('%s', '%s', '%s')", type, code, value);
		}
		Exec(sql);
	}
	
	// 获取参数表中对应类型的数据
	public List<Map<String, String>> SelectParamsTypeValues(String type) 
	{
		String sql = String.format("select _id,s_code,s_value,s_param from PARAMS where s_type = '%s'", type);
		List<Map<String, String>> result = SelectList(sql);
		if (null == result || 0 == result.size()) 
		{
			return null;
		}
		return result;
	}
	
	// 获取参数表中对应类型的数据
	public List<Map<String, String>> SelectParamsValues(String code) 
	{
		String sql = String.format("select _id,s_code,s_value,s_param from PARAMS where s_code = '%s'", code);
		List<Map<String, String>> result = SelectList(sql);
		if (null == result || 0 == result.size()) 
		{
			return null;
		}
		return result;
	}
	
	// 获取邮件数据
	public List<Map<String, String>> selectMailData(String strUserNo) 
	{
		String sql = String.format("select _id,s_code,s_value,s_relationcode from PARAMS where s_type = '%s' and s_param = '%s' order by _id desc" , "09", strUserNo);
		List<Map<String, String>> result = SelectList(sql);
		if (null == result || 0 == result.size()) 
		{
			return null;
		}
		return result;
	}
	
	// 更新邮件已读(未读状态)
	// strMailStatus: "0"-未读	1-"已读"
	public boolean updateMailStatus(String strMailID, String strMailStatus) 
	{
		String updateSQL = "update " + TABLE_NAME + " set s_relationcode = '" + strMailStatus 
				+ "' where _id = '" + strMailID + "'";
		return executeUpdateSQL(updateSQL);
	}
	
	// 获取打印广告语
	public String getAdverInfo() 
	{
		String sql = String.format("select s_value from PARAMS where s_type = '%s'", "08");
		List<Map<String, String>> result = SelectList(sql);
		if (null == result || 0 == result.size()) 
		{
			return null;
		}
		String tempstr = result.get(0).get("s_value");
		Log.e("", "凭条打印广告语为: " + tempstr);
		return tempstr;
	}
	
	// 获取参数表中所有CRC
	public void GetAllCrc()
	{
		Log.e("", "更新各版本CRC...");
		String sql = String.format("select s_code,s_value from PARAMS where s_type = '%s'", "00");
		HashMap<String, String> result = SelectHash(sql);
		if (0 != result.size())
		{
			for (String key:result.keySet())
			{
				sycCRCData(key, result.get(key));
				Log.e("", "类型: " + key + ", CRC: " + result.get(key));
			}
		}
		else
		{
			Log.e("", "未查找到记录!");
		}
		Log.e("", "更新各版本CRC结束!");
	}
	
	// 将所有CRC保存在缓存区中
	private void sycCRCData(String crctype, String crc)
	{
		if ("01".equalsIgnoreCase(crctype))
		{
			CRCData.crcmenu = crc;
		} 
		else if ("02".equalsIgnoreCase(crctype))
		{
			CRCData.crcmeal = crc;
		} 
		else if ("03".equalsIgnoreCase(crctype))
		{
			CRCData.crcincrement = crc;
		} 
		else if ("04".equalsIgnoreCase(crctype))
		{
			CRCData.crcespeciallyservice = crc;
		} 
		else if ("05".equalsIgnoreCase(crctype))
		{
			CRCData.crcselnorule = crc;
		} 
		else if ("06".equalsIgnoreCase(crctype))
		{
			CRCData.crcbrand = crc;
		} 
		else if ("07".equalsIgnoreCase(crctype))
		{
			CRCData.crcwelcome = crc;
		} 
		else if ("08".equalsIgnoreCase(crctype))
		{
			CRCData.crcprint = crc;
		}
		else if ("09".equalsIgnoreCase(crctype))
		{
			CRCData.crcinbox = crc;
		}
		else if ("0A".equalsIgnoreCase(crctype))
		{
			CRCData.crc3gmeal = crc;
		}
	}
	
	/**
	 * 插入或者更新系统参数
	 * @param type 类型
	 * @param code 
	 * @param value
	 * @param ralation
	 * @param param
	 */
	public void insertOrUpdateParams(String type, String crc, String code, String value, String ralation, String param)
	{
		String deleteSQL = "delete from "+TABLE_NAME+" where "+type+" = '"+type+"' and code = '"+code+"'";
		executeUpdateSQL(deleteSQL);
	}
}
