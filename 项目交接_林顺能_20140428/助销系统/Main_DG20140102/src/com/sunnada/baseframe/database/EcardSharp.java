package com.sunnada.baseframe.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.sunnada.baseframe.activity.selfservice.BusiEcardInquiry;
import com.sunnada.baseframe.bean.EcardReportData;
import com.sunnada.baseframe.util.StringUtil;

public class EcardSharp 
{
	private final String       QUERY_TYPE        = "query_type";
	private final String       CARD_KEY          = "card_key";
	private final String       FLOW_NO           = "flow_no";
	private BusiEcardInquiry   mInquiry;
	private SharedPreferences  mSharp;
	private Editor             mEditor; 
	
	public EcardSharp(BusiEcardInquiry inquiry)
	{
		this.mInquiry = inquiry;
		mSharp = mInquiry.getPreferences(Context.MODE_PRIVATE);
		mEditor = mSharp.edit();
	}
	
	public void putIntValue(int queryType)
	{
		mEditor.putInt(QUERY_TYPE, queryType);
		mEditor.commit();
	}
	
	public void putCardKeyValue(String cardKey)
	{
		mEditor.putString(CARD_KEY, cardKey);
		mEditor.commit();
	}
	
	public void putFlowNoValue(String flowNo)
	{
		mEditor.putString(FLOW_NO, flowNo);
		mEditor.commit();
	}
	
	public void putValue(int queryType, String cardKey, String flowNo)
	{
		mEditor.putInt(QUERY_TYPE, queryType);
		mEditor.putString(CARD_KEY, cardKey);
		mEditor.putString(FLOW_NO, flowNo);
		mEditor.commit();
	}
	
	public int getIntValue()
	{
		return mSharp.getInt(QUERY_TYPE, -1);
	}
	
	public String getCardKeyValue()
	{
		return mSharp.getString(CARD_KEY, "");
	}
	
	public String getFlowNoValue()
	{
		return mSharp.getString(FLOW_NO, "");
	}
	
	public EcardReportData getValue()
	{
		EcardReportData reportData = new EcardReportData();
		int queryType = mSharp.getInt(QUERY_TYPE, -1);
		String cardKey = mSharp.getString(CARD_KEY, "");
		String flowNo = mSharp.getString(FLOW_NO, "");
		
		if(queryType == -1)
		{
			return null;
		}
		else if(StringUtil.isEmptyOrNull(cardKey))
		{
			return null;
		}
		else if(StringUtil.isEmptyOrNull(flowNo))
		{
			return null;
		}
		
		reportData.setQueryType(queryType);
		reportData.setCardKey(cardKey);
		reportData.setFlowNo(flowNo);
		return reportData;
	}
	
	public void clearData()
	{
		mEditor.clear();
		mEditor.commit();
	}
}
