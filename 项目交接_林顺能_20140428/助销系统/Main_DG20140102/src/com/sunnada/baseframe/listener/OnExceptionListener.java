package com.sunnada.baseframe.listener;

/**异常接口
 * File: OnExceptionListener.java
 * Description:异常接口
 * @author 丘富铨
 * @date 2013-1-18
 * Copyright: Copyright (c) 2012
 * Company: 福建三元达软件有限公司
 * @version 1.0
 */
public interface OnExceptionListener
{
	/**
	 * 异常回调接口，当程序中出现异常时候会回调
	 */
	public void onException(Exception e);
}