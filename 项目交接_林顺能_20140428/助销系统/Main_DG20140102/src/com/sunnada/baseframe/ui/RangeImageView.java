package com.sunnada.baseframe.ui;

import com.sunnada.baseframe.activity.R;
import com.sunnada.baseframe.util.BitmapPoolUtil;
import com.sunnada.baseframe.util.BitmapPoolUtil.onImageGotListener;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

public class RangeImageView extends ImageView implements onImageGotListener 
{
	private Bitmap				mBitmap 		= null;
	private boolean 			mIsLoaded 		= false;
	
	private String 				mStrLocalPath 	= null;
	private String 				mStrRangePath 	= null;
	
	private RangeImageView 		mThis 		= null;

	public RangeImageView(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		mThis = this;
	}

	public RangeImageView(Context context, AttributeSet attrs, int defStyle) 
	{
		super(context, attrs, defStyle);
		mThis = this;
	}

	public RangeImageView(Context context) 
	{
		super(context);
		mThis = this;
	}
	
	public void show(String localPath, String rangePath) 
	{
		try
		{
			this.mStrLocalPath = localPath;
			this.mStrRangePath = rangePath;
			
			mBitmap = BitmapPoolUtil.getBitmap(localPath, rangePath, this);
			if(mBitmap != null) 
			{
				this.post(new Runnable() 
				{
					@Override
					public void run() 
					{
						mThis.setImageBitmap(mBitmap);
					}
				});
			}
			else 
			{
				this.setImageResource(R.drawable.bg_model_loading);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) 
	{
		if(mIsLoaded && (mBitmap == null || mBitmap.isRecycled())) 
		{
			mBitmap = BitmapPoolUtil.getBitmap(mStrLocalPath, mStrRangePath, this);
			mThis.setImageBitmap(mBitmap);
		}
		super.onDraw(canvas);
	}
	
	// 加载成功后显示本图片
	@Override
	public void onImageGot(boolean result) 
	{
		if(result == true) 
		{
			this.mBitmap = BitmapPoolUtil.getBitmap(mStrLocalPath, null, null);
			if(mBitmap != null && !mBitmap.isRecycled()) 
			{
				mIsLoaded = true;
				this.post(new Runnable() 
				{
					@Override
					public void run() 
					{
						mThis.setImageBitmap(mBitmap);
					}
				});
			}
		}
		else
		{
			this.post(new Runnable() 
			{
				@Override
				public void run() 
				{
					mThis.setImageResource(R.drawable.bg_no_model);
				}
			});
		}
	}
	
	// 回收bitmap
	public void clearBitmap() 
	{
		if(mBitmap != null && !mBitmap.isRecycled()) 
		{
			Log.e("bitmap回收", "正在释放" + mStrLocalPath + "的大图图片..");
			mBitmap.recycle();
			mBitmap = null;
		}
	}
}
