package com.sunnada.baseframe.ui;

import android.content.Context;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.Gallery;

public class GalleryFlow extends Gallery
{
	private Camera		mCamera				= new Camera();	// 相机类
	private int			mMaxRotationAngle;					// 最大转动角度
	private int			mCoveflowCenter;					// 半径值

	private Runnable	m_listener;

	public GalleryFlow(Context context)
	{
		super(context);
		// 支持转换 ,执行getChildStaticTransformation方法
		this.setStaticTransformationsEnabled(true);
		this.setCallbackDuringFling(false);
		setFadingEdgeLength(45);
	}

	public GalleryFlow(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.setStaticTransformationsEnabled(true);
	}

	public GalleryFlow(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		this.setStaticTransformationsEnabled(true);
	}
	
	public void setOnScrollListener(Runnable listener)
	{
		this.m_listener = listener;
	}

	public int getmMaxRotationAngle()
	{
		return mMaxRotationAngle;
	}

	public void setmMaxRotationAngle(int mMaxRotationAngle)
	{
		this.mMaxRotationAngle = mMaxRotationAngle;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		//System.out.println("distanceX :" + distanceX + "distanceY:" + distanceY); 
		boolean flag = super.onScroll(e1, e2, distanceX, distanceY);
		if (m_listener != null)
		{
			m_listener.run();
		}
		return flag;
	}

	public int getMaxRotationAngle()
	{
		return mMaxRotationAngle;
	}

	public void setMaxRotationAngle(int maxRotationAngle)
	{
		mMaxRotationAngle = maxRotationAngle;
	}

	private int getCenterOfCoverflow()
	{
		return (getWidth() - getPaddingLeft() - getPaddingRight()) / 2 + getPaddingLeft();
	}

	// 取得半径值
	private static int getCenterOfView(View view)
	{
		// System.out.println("view left :" + view.getLeft());
		// System.out.println("view width :" + view.getWidth());
		return view.getLeft() + view.getWidth() / 2;
	}

	// 控制gallery中每个图片的旋转(重写的gallery中方法)
	@Override
	protected boolean getChildStaticTransformation(View child, Transformation t)
	{
		// 取得当前子view的半径值
		final int childCenter = getCenterOfView(child);
		// System.out.println("childCenter：" + childCenter);
		final int childWidth = child.getWidth();
		// 旋转角度
		int rotationAngle = 0;
		// 重置转换状态
		t.clear();
		// 设置转换类型
		t.setTransformationType(Transformation.TYPE_MATRIX);
		// 如果图片位于中心位置不需要进行旋转
		if (childCenter == mCoveflowCenter)
		{
			transformImageBitmap(child, t, 0);
		}
		else
		{
			// 根据图片在gallery中的位置来计算图片的旋转角度
			// rotationAngle = (mCoveflowCenter - childCenter);
			rotationAngle = (int) (((float) (mCoveflowCenter - childCenter) / childWidth) * mMaxRotationAngle);
			// System.out.println("rotationAngle:" + rotationAngle);
			// 如果旋转角度绝对值大于最大旋转角度返回（-mMaxRotationAngle或mMaxRotationAngle;）
			if (Math.abs(rotationAngle) > mMaxRotationAngle)
			{
				rotationAngle = (rotationAngle < 0) ? -mMaxRotationAngle:mMaxRotationAngle;
			}
			transformImageBitmap(child, t, rotationAngle);
		}
		return true;
	}

	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		mCoveflowCenter = getCenterOfCoverflow();
		super.onSizeChanged(w, h, oldw, oldh);
	}

	private void transformImageBitmap(View child, Transformation t, int rotationAngle)
	{
		// 对效果进行保存
		mCamera.save();
		final Matrix imageMatrix = t.getMatrix();
		// 图片高度
		final int imageHeight = child.getLayoutParams().height;
		// 图片宽度
		final int imageWidth = child.getLayoutParams().width;
		// 返回旋转角度的绝对值
		final int rotation = Math.abs(rotationAngle);

		// 在Z轴上正向移动camera的视角，实际效果为放大图片。
		// 如果在Y轴上移动，则图片上下移动；X轴上对应图片左右移动。
		// Y:-:图片往下,+:图片往上
		mCamera.translate(0.0f, (float) (-120 - rotation * 1.2), (float) (-(mMaxRotationAngle - rotation) * 2.8));
		// As the angle of the view gets less, zoom in
		// if (rotation < mMaxRotationAngle) {
		// float zoomAmount = (float) (mMaxZoom + (rotation * 1.5));
		// mCamera.translate(0.0f, 0.0f, zoomAmount);
		// }
		// 在Y轴上旋转，对应图片竖向向里翻转。
		// 如果在X轴上旋转，则对应图片横向向里翻转。
		// mCamera.rotateY(rotationAngle);
		mCamera.getMatrix(imageMatrix);
		imageMatrix.preTranslate(-(imageWidth / 2), -(imageHeight / 2));
		imageMatrix.postTranslate((imageWidth / 2), (imageHeight / 2));
		mCamera.restore();
	}

	/*
	 private void transformImageBitmap(ImageView child, Transformation t,
	 int rotationAngle) {
	 LayoutParams params = (LayoutParams) child.getLayoutParams();
	 int w = params.width;
	 int h = params.height;
	 rotationAngle = Math.abs(rotationAngle);
	 if(rotationAngle>240){
	 rotationAngle = 240;
	 }
	 rotationAngle = 300-rotationAngle;
	
	 double temp = ((double)rotationAngle)/300d;
	 w = (int) (240*temp);
	 h = (int) (320*temp);
	 if(w !=params.width ){
	 params.width = w;
	 params.height = h;
	 child.setLayoutParams(params);
	 }
	  LayoutParams params = (LayoutParams) child.getLayoutParams();
	  if(Math.abs(rotationAngle)<=10){
	  if(params.width!=480){
	  params.width=480;
	  params.height=640;
	  child.setLayoutParams(params);
	  }
	 
	  }else{
	  if(params.width!=240){
	  params.width=240;
	  params.height=360;
	  child.setLayoutParams(params);
	  }
	  }
	
	 // 对效果进行保存
	  mCamera.save();
	  final Matrix imageMatrix = t.getMatrix();
	 // 图片高度
	  final int imageHeight = child.getLayoutParams().height;
	 // 图片宽度
	  final int imageWidth = child.getLayoutParams().width;
	 // 返回旋转角度的绝对值
	  final int rotation = Math.abs(rotationAngle);
	 
	 // 在Z轴上正向移动camera的视角，实际效果为放大图片。
	 // 如果在Y轴上移动，则图片上下移动；X轴上对应图片左右移动。
	  mCamera.translate(0.0f, 0.0f, rotation * 10);
	  // As the angle of the view gets less, zoom in
	  if (rotation < mMaxRotationAngle) {
	  float zoomAmount = (float) (mMaxZoom + (rotation * 1.5));
	  mCamera.translate(0.0f, 0.0f, zoomAmount);
	  }
	 // 在Y轴上旋转，对应图片竖向向里翻转。
	 // 如果在X轴上旋转，则对应图片横向向里翻转。
	  mCamera.rotateY(rotationAngle);
	  mCamera.getMatrix(imageMatrix);
	  imageMatrix.preTranslate(-(imageWidth / 2), -(imageHeight / 2));
	  imageMatrix.postTranslate((imageWidth / 2), (imageHeight / 2));
	  mCamera.restore();
	 }
	 */
}
