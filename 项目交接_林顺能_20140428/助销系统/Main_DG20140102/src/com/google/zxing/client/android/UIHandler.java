package com.google.zxing.client.android;

import android.graphics.Bitmap;

public class UIHandler 
{
	public static final int			DECODE						= 0x0001;						// ����
	public static final int 		DECODE_SUCCEEDED			= 0x0002;						// ɨ��һά��ɹ�
	public static final int 		DECODE_FAILED				= 0x0003;						// ɨ��һά��ɹ�
	
	public static final int 		RESTART_PREVIEW				= 0x0004;						// ���´�Ԥ��
	public static final int			RETURN_SCAN_RESULT			= 0x0005;
	public static final int 		LAUNCH_PRODUCT_QUERY		= 0x0006;
	public static final int 		QUIT						= 0x0007;						// �˳�
	
	public static Bitmap			mBarCodeBmp					= null;							// һά��ͼƬ
	public static String			mBarCodeStr					= null;							// һά����Ϣ
	
	public static boolean			mLandScape					= true;							// �Ƿ����Ԥ��
	
	public static final int 		QUERYENROLMENT				= 0x0020;
	public static final int 		MSG_SCAN_BARCODE_FAIL		= 0x0021;
}
