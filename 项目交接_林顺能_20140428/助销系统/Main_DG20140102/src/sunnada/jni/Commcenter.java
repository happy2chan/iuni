package sunnada.jni;

import java.util.ArrayList;

public class Commcenter 
{
	static 
	{
		System.loadLibrary("syd_comm");
	}

	/*
	 * 这些函数如果返回0，为出错标志。
	 */

	/************************************************************************
	 * 函数名称：CalcCrc16 函数描述：生成16位CRC码 参数说明： tp： 要计算CRC的数据包; crc_pre： 初始值，一般为0;
	 ***********************************************************************/
	public native static byte[] CalcCrc16(byte[] tp, char crc_pre);

	/************************************************************************
	 * 函数名称：Encrypt 函数描述：(数据头+数据体 )加密， 参数说明： data：数据包 返回值: 加密后的数据
	 ***********************************************************************/
	public native static byte[] Encrypt(byte[] data);

	/************************************************************************
	 * 函数名称：Decrypt 函数描述：(数据头+数据体 )解密， 参数说明： data：数据包 返回值: 解密后的数据
	 ***********************************************************************/
	public native static byte[] Decrypt(byte[] data);

	/************************************************************************
	 * 函数名称：XORCRC 函数描述：PSAM卡号CRC异或加密，在用设备进行des加密之前调用 参数说明： psam：
	 * 指向psam卡号的数组，非字符串 crc: 数据头+数据体 计算出的CRC 返回值: CRC异或后的PSAM卡后
	 ***********************************************************************/
	public native static byte[] XORCRC(byte[] psam, char crc);

	public static byte[] TransferredMeaning(byte[] Source) 
	{
		int i, len = Source.length;
		ArrayList<Byte> list = new ArrayList<Byte>(100);

		list.add((byte) 0x7E);
		for (i = 0; i < len; i++) 
		{
			if (Source[i] == 0x7E) 
			{
				list.add((byte) 0x5E);
				list.add((byte) 0x7D);
			} 
			else if (Source[i] == 0x5E) 
			{
				list.add((byte) 0x5E);
				list.add((byte) 0x5D);
			} 
			else if (Source[i] == 0x7F) 
			{
				list.add((byte) 0x5E);
				list.add((byte) 0x5F);
			} 
			else 
			{
				list.add((byte) Source[i]);
			}
		}
		list.add((byte) 0x7F);
		int des_len = list.size();
		byte res[] = new byte[des_len];

		for (i = 0; i < des_len; i++) 
		{
			res[i] = list.get(i);
		}
		return res;
	}

	public static byte[] ReverseTransferredMeaning(byte[] source) 
	{
		int i, len = source.length;
		ArrayList<Byte> list = new ArrayList<Byte>(100);
		list.add((byte) 0x7E);
		
		for (i = 1; i < len-1; i++) 
		{
			if(i == len -2){
				list.add((byte)source[i]);
				break;
			}
			if (source[i] ==(byte)0x5E && source[i+1] == (byte)0x7D) 
			{
				list.add((byte) 0x7E);
				i++;
			} 
			else if (source[i] == 0x5E &&  source[i+1] == (byte)0x5D) 
			{
				list.add((byte) 0x5E);
				i++;
			} 
			else if (source[i] == 0x5E &&  source[i+1] == (byte)0x5F) 
			{
				list.add((byte) 0x7F);
				i++;
			} 
			else 
			{
				list.add((byte) source[i]);
			}
		}
		//list.add((byte) Source[i]);
		list.add((byte) 0x7F);
		int des_len = list.size();
		byte res[] = new byte[des_len];
		
		for (i = 0; i < des_len; i++) 
		{
			res[i] = list.get(i);
		}
		return res;
	}
}
