package com.ym.idcard.reg;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Build;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.ai.cmccha.sc.idcard.App;
import com.ai.cmccha.sc.idcard.util.BitmapUtil;

public class CameraManager implements Camera.AutoFocusCallback 
{
	//private static final String 		TAG 					= "CameraManager";
	public static final int 			THREE_K 				= 500*1024;
	public static final int 			MSG_PIC_TAKEN_SUC 		= 1;
	public static final int 			MSG_PIC_TAKEN_FAIL 		= 2;
	public static final int 			MSG_PIC_TAKEN_FAIL2 	= 3;
	public static final int 			mHeight 				= 1200;
	public static final int 			mWidth 					= 1600;
	private boolean 					isFocusArea;
	private Handler 					mHandler;
	private String						mStrImageName;
	private Camera 						mCamera;
	
	private Camera.PictureCallback 		jpegCallback = new Camera.PictureCallback() 
	{
		@Override
		public void onPictureTaken(byte[] data, Camera camera) 
		{
			// �洢���ļ�
			writeJpegFile(data);
		}
	};
	
	// �洢��Ƭ���ݵ��ļ�
	private void writeJpegFile(byte[] data) 
	{
		try 
		{
			// �����Ƭ�ļ����Ƿ����
			File imgFile = new File(mStrImageName);
			if (!imgFile.getParentFile().exists())
			{
				imgFile.getParentFile().mkdirs();
			}
			// ͼƬ�ߴ�̫��Ļ�ѹ��ͼƬ
			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
			if(bitmap.getWidth() > 800) 
			{
				float scale = (float)800/(float)bitmap.getWidth();
				Bitmap sb = BitmapUtil.scaleBitmap(bitmap, scale);
				if(sb != bitmap) 
				{
					bitmap.recycle();
					bitmap = null;
				}
				bitmap = sb;
			}
			// �洢��Ƭԭʼ����
			FileOutputStream fos = new FileOutputStream(imgFile);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
			fos.flush();
			fos.close();
			
			bitmap.recycle();
			bitmap = null;
			mHandler.obtainMessage(MSG_PIC_TAKEN_SUC).sendToTarget();
			
			/*
			Message message = mHandler.obtainMessage(MSG_PIC_TAKEN_SUC);
			Bundle bundle = new Bundle();
			bundle.putString("pic_path", imgFile.getAbsolutePath());
			bundle.putString("pic_scale_path", scaleFile.getAbsolutePath());
			message.setData(bundle);
			message.sendToTarget();
			*/
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			mHandler.sendEmptyMessage(MSG_PIC_TAKEN_FAIL);
		} 
		catch(OutOfMemoryError er) 
		{
			er.printStackTrace();
			mHandler.sendEmptyMessage(MSG_PIC_TAKEN_FAIL);
		} 
		finally 
		{
			System.gc();
		}
	}
	
	private Camera.PictureCallback rawCallback = new Camera.PictureCallback() 
	{
		public void onPictureTaken(byte[] data, Camera camera) 
		{
			
		}
	};

	private Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() 
	{
		public void onShutter() 
		{
			
		}
	};

	public class SizeComparator implements Comparator<Size> 
	{
		public int compare(Camera.Size size, Camera.Size size1) 
		{
			return size1.width * size1.height - size.width * size.height;
		}

		public SizeComparator() 
		{
			super();
		}
	}

	public CameraManager(Context context, Handler handler, String strImageName) 
	{
		isFocusArea 	= false;
		mHandler 		= handler;
		mStrImageName 	= strImageName;
	}

	public static int clamp(int i, int j, int k) 
	{
		if (i <= k) 
		{
			if (i < j)
			{
				k = j;
			}
			else
			{
				k = i;
			}
		}
		return k;
	}

	public static void rectFToRect(RectF rectf, Rect rect) 
	{
		rect.left = Math.round(rectf.left);
		rect.top = Math.round(rectf.top);
		rect.right = Math.round(rectf.right);
		rect.bottom = Math.round(rectf.bottom);
	}

	private void takePicture(boolean fause) throws RuntimeException 
	{
		if (mCamera != null) 
		{
			Camera.Parameters params = mCamera.getParameters();
			params.setJpegQuality(100);
			mCamera.setParameters(params);
			mCamera.takePicture(shutterCallback, rawCallback, jpegCallback);
		}
	}

	public void calculateTapArea(int i, int j, float f, int k, int l, int i1, int j1, Rect rect) 
	{
		int k1 = (int) (f * (float) i);
		int l1 = (int) (f * (float) j);
		int i2 = clamp(k - k1 / 2, 0, i1 - k1);
		int j2 = clamp(l - l1 / 2, 0, j1 - l1);
		rectFToRect(new RectF(i2, j2, i2 + k1, j2 + l1), rect);
	}

	public void closeCamera() 
	{
		if (mCamera != null) 
		{
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}

	public String getDefaultFlashMode() 
	{
		String s;
		if (mCamera.getParameters().getSupportedFlashModes() != null)
			s = (String) mCamera.getParameters().getSupportedFlashModes().get(0);
		else
			s = "off";
		return s;
	}

	public String getNextFlashMode(String s) 
	{
		List<String> list = mCamera.getParameters().getSupportedFlashModes();
		String s1 = null;
		if (list != null) 
		{
			list.remove("red-eye");
			list.remove("torch");
			int i = 1 + list.indexOf(s);
			if (i == list.size())
				i = 0;
			s1 = (String) list.get(i);
		}
		return s1;
	}

	public void initDisplay() 
	{
		if (mCamera != null) 
		{
			mCamera.startPreview();
		}
	}

	public boolean isSupportFlash(String model) 
	{
		List<String> list = mCamera.getParameters().getSupportedFlashModes();
		if (mCamera != null && list != null && list.contains(model))
			return true;
		return false;
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public boolean isSupportFocusArea() 
	{
		Camera.Parameters params = mCamera.getParameters();
		List<String> list = params.getSupportedFocusModes();
		if (list != null) 
		{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH
					&& params.getMaxNumFocusAreas() >= 1) 
			{
				return list.contains(Camera.Parameters.FOCUS_MODE_AUTO);
			}
		}
		return false;
	}

	@Override
	public void onAutoFocus(boolean success, Camera camera) 
	{
		if (isFocusArea) 
		{
			isFocusArea = false;
		}
		if(success) 
		{
			takePicture(success);
		} 
		else 
		{   
			App.showToast("�Խ�ʧ������������");
			mHandler.sendEmptyMessage(MSG_PIC_TAKEN_FAIL2);
		}
	}

	public void openCamera(SurfaceHolder holder) throws RuntimeException, IOException 
	{
		if (mCamera == null) 
		{
			mCamera = Camera.open();
			mCamera.setPreviewDisplay(holder);
			setPictureSize();
		}
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public void requestFocusArea(MotionEvent motionevent, SurfaceView surfaceview) 
	{
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) 
		{
			int i = Math.round(motionevent.getX());
			int j = Math.round(motionevent.getY());
			int k = surfaceview.getWidth();
			int l = surfaceview.getHeight();
			ArrayList<Camera.Area> list = new ArrayList<Camera.Area>();
			list.add(new Camera.Area(new Rect(), 1));
			calculateTapArea(200, 200, 1.0F, i, j, k, l, list.get(0).rect);
			Camera.Parameters params = mCamera.getParameters();
			params.setFocusAreas(list);
			mCamera.setParameters(params);
			isFocusArea = true;
			mCamera.autoFocus(this);
		}
	}
	
	public void requestFocus() 
	{
		if (mCamera != null) 
		{
			if (!"SH8118U".equals(Build.MODEL)) 
			{
				if (mCamera.getParameters().getSupportedFocusModes().contains("auto")
						&& !mCamera.getParameters().getFocusMode().equals("auto")) 
				{
					Camera.Parameters parameters = mCamera.getParameters();
					parameters.setFocusMode("auto");
					mCamera.setParameters(parameters);
				}
			} 
			else 
			{
				Camera.Parameters parameters1 = mCamera.getParameters();
				parameters1.setFocusMode("normal");
				mCamera.setParameters(parameters1);
			}
			mCamera.autoFocus(this);
		}
	}

	public void setCameraFlashMode(String model) 
	{
		if (mCamera != null) 
		{
			Camera.Parameters parameters = mCamera.getParameters();
			parameters.setFlashMode(model);
			mCamera.setParameters(parameters);
		}
	}

	private void setPictureSize() 
	{
		Camera.Parameters params = mCamera.getParameters();
		params.setPictureFormat(256);
		/*
		List<Camera.Size> list = params.getSupportedPictureSizes();
		Collections.sort(list, new SizeComparator());
		if (list != null) 
		{
			int j = 0;
			for (int k = 0; k < list.size(); k++) 
			{
				if (list.get(k).width == 1600) 
				{
					j = k;
					break;
					// continue; // Loop/switch isn't completed
				}
				if (list.get(k).width >= 1600) 
				{

				} 
				else 
				{
					j = k - 1;
					if (j < 0)
						j = 0;
					if (-1600 + list.get(j).width > 1600 - list.get(k).width)
						j = k;
				}
			}
			params.setPictureSize(list.get(j).width, list.get(j).height);
		}
		*/
		List<Size> picturesize = params.getSupportedPictureSizes();
		if(picturesize != null)
		{
			List<Integer> pic_width = new ArrayList<Integer>();
			int pic_size = picturesize.size();
			System.out.printf(" CV: picture count=%d, size= \r\n", pic_size);
			for (int i=0; i<pic_size; i++)
			{
				pic_width.add(picturesize.get(i).width);
				System.out.printf(" (%d,%d) \r\n", picturesize.get(i).width, picturesize.get(i).height);
			}
			System.out.println();
			
			Collections.sort(pic_width);
			int pic_index = Collections.binarySearch(pic_width, 1600);
			//System.out.printf("\r\n index1=%d... \r\n", pic_index);
			if (pic_index < 0) 
			{
				pic_index = -pic_index - 1;
			}
			//System.out.printf("\r\n index2=%d... \r\n", pic_index);
			if (pic_index > -1) 
			{
				if(pic_index >= pic_width.size())
				{
					pic_index = pic_width.size()-1;
				}
				int picture_width = pic_width.get(pic_index);
				for (int i=0; i<pic_size; i++)
				{
					if (picturesize.get(i).width == picture_width)
					{
						int nRatioX = picturesize.get(i).width;
						int nRatioY = picturesize.get(i).height;
						System.out.println(" CV: gRatioX="+nRatioX+", gRatioY="+nRatioY);
						params.setPictureSize(picturesize.get(i).width, nRatioY);
						break;
					}
				}
			}
			//System.out.println(" CV: set picture size ok...");
		}
		mCamera.setParameters(params);
	}
	
	public void setPreviewSize(int width, int height) 
	{
//		if (!"ME525+".equals(Build.MODEL) && mCamera != null) {
//			Camera.Parameters parameters = mCamera.getParameters();
//			List<Camera.Size> list = parameters.getSupportedPreviewSizes();
//			Collections.sort(list, new SizeComparator());
//			if (list != null) {
//				int l = 0;
//				for (int i = 0; i < list.size(); i++) {
//					if (list.get(i).width == width) {
//						l = i;
//						break;
//					} else {
//						if (list.get(i).width >= width) {
//
//						} else {
//							l = i - 1;
//							if (l < 0)
//								l = 0;
//							if (list.get(l).width - width > width - list.get(i).width) {
//								l = i;
//							}
//						}
//					}
//				}
//				parameters.setPreviewSize(list.get(l).width, list.get(l).height);
//			}
//			mCamera.setParameters(parameters);
//		}
	}

	public void stopPreview() 
	{
		if (mCamera != null) 
		{
			mCamera.stopPreview();
		}
	}
}
