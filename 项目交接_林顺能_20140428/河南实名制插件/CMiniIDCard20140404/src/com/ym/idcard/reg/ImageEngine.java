package com.ym.idcard.reg;

import com.ai.cmccha.sc.idcard.util.StringUtil;

public class ImageEngine 
{
	public static final int IMG_COMPONENT_GRAY 		= 1;
	public static final int IMG_COMPONENT_RGB 		= 3;
	public static final int IMG_FMT_BMP 			= 1;
	public static final int IMG_FMT_JPG 			= 2;
	public static final int IMG_FMT_UNK 			= 0;
	public static final int RET_ERR_ARG 			= -2;
	public static final int RET_ERR_MEM 			= -3;
	public static final int RET_ERR_PTR 			= -1;
	public static final int RET_ERR_UNK 			= 0;
	public static final int RET_OK 					= 1;
	protected long 			mEngine;
	protected long 			mImage;
	protected NativeImage 	mImageEngine;

	public class ImageProperty 
	{

		public int mComponent;
		public int mHeight;
		public int mWidth;

		public ImageProperty() 
		{
			
		}
	}

	public ImageEngine() 
	{
		mImageEngine = new NativeImage();
		mEngine = mImageEngine.createEngine();
	}

	public void finalize() 
	{
		if (mImageEngine != null && mEngine != 0L) 
		{
			mImageEngine.freeImage(mEngine);
			mImageEngine.closeEngine(mEngine);
			mEngine = 0L;
			mImage = 0L;
		}
	}

	public int getComponent() 
	{
		int i;
		if (mImageEngine != null)
		{
			i = mImageEngine.getImageComponent(mEngine);
		}
		else
		{
			i = 0;
		}
		return i;
	}

	public byte[] getData() 
	{
		byte abyte0[];
		if (mImageEngine != null)
		{
			abyte0 = mImageEngine.getImageData(mEngine);
		}
		else
		{
			abyte0 = null;
		}
		return abyte0;
	}

	public long getDataEx() 
	{
		long l;
		if (mImageEngine != null)
		{
			l = mImageEngine.getImageDataEx(mEngine);
		}
		else
		{
			l = 0L;
		}
		return l;
	}

	public int getHeight() 
	{
		int i;
		if (mImageEngine != null)
		{
			i = mImageEngine.getImageHeight(mEngine);
		}
		else
		{
			i = 0;
		}
		return i;
	}

	public ImageProperty getProperty(String s) 
	{
		int i = mImageEngine.getProperty(mEngine, StringUtil.convertToUnicode(s));
		ImageProperty imageproperty = null;
		if (i == 1) 
		{
			imageproperty = new ImageProperty();
			imageproperty.mWidth = getWidth();
			imageproperty.mHeight = getHeight();
			imageproperty.mComponent = getComponent();
		}
		return imageproperty;
	}

	public int getWidth() {
		int i;
		if (mImageEngine != null)
			i = mImageEngine.getImageWidth(mEngine);
		else
			i = 0;
		return i;
	}

	public boolean init(int i, int j) {
		boolean flag = true;
		if (mImageEngine == null || mImageEngine.initImage(mEngine, i, j) != 1)
			flag = false;
		return flag;
	}

	public boolean isGray() {
		boolean flag = true;
		if (mImageEngine == null || mImageEngine.isGrayImage(mEngine) != 1)
			flag = false;
		return flag;
	}

	public boolean load(byte abyte0[], String s) {
		boolean flag = true;
		if (mImageEngine == null
				|| mImageEngine.loadmemjpg(mEngine, abyte0, abyte0.length) != 1)
			flag = false;
		return flag;
	}

	public boolean save(String s) {
		boolean flag = true;
		if (mImageEngine == null
				|| mImageEngine.saveImage(mEngine,
						StringUtil.convertToUnicode(s)) != 1)
			flag = false;
		return flag;
	}

	public boolean scale(String s, String s1, int i, int j) {
		boolean flag = true;
		if (mImageEngine == null
				|| mImageEngine.scale(StringUtil.convertToUnicode(s),
						StringUtil.convertToUnicode(s1), i, j) != 1)
			flag = false;
		return flag;
	}

	public boolean setSaveParams(byte abyte0[], int i, int j, int k) {
		boolean flag;
		if (mImageEngine != null
				&& mImageEngine.setSaveParams(mEngine, abyte0, i, j, k) == 1)
			flag = true;
		else
			flag = false;
		return flag;
	}
}
