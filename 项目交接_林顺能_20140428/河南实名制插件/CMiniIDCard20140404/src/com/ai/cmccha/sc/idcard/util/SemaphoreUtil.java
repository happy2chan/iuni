package com.ai.cmccha.sc.idcard.util;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class SemaphoreUtil 
{
	private Semaphore mSem = null;
	
	public SemaphoreUtil(int nPermits) 
	{
		mSem = new Semaphore(nPermits);
	}
	
	// 获得许可
	public boolean acquire() 
	{
		try
		{
			mSem.acquire();
			return true;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// 获取许可, 有超时时间
	public boolean tryAcquire(long nTimeout, TimeUnit unit) 
	{
		try 
		{
			if(mSem.tryAcquire(nTimeout, unit)) 
			{
				return true;
			}
			return false;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// 释放许可
	public void release() 
	{
		mSem.release();
	}
}
