package com.ai.cmccha.sc.idcard.util;

import com.ai.cmccha.sc.idcard.uilogic.UIHandler;

public class ButtonUtil
{
	private static long 		mLastClickTime 		= 0;
	private static int 			mLastButtonId 		= -1;
	private static long 		DIFF 				= 1000;
	private static long 		mLastToastTime 		= 0;

	// 判断两次点击的间隔，如果小于1000，则认为是多次无效点击
	public static boolean isFastDoubleClick()
	{
		return isFastDoubleClick(-1, DIFF);
	}
	
	// true: 连续按下多个点击
	// false:单次点击
	public static boolean isFastDoubleClick(int buttonId)
	{
		return isFastDoubleClick(buttonId, DIFF);
	}

	// 判断两次点击的间隔，如果小于diff，则认为是多次无效点击
	public static boolean isFastDoubleClick(int buttonId, long diff)
	{
		long clickTime = System.currentTimeMillis();
		long timeSpace = Math.abs(clickTime-mLastClickTime);
		
		// 不同按钮
		if (mLastButtonId != buttonId) 
		{
			if(mLastClickTime > 0 && timeSpace < diff/3) 
			{
				// 非常快就不显示，不然一直显示
				if(clickTime-mLastToastTime > 1800)
				{
					UIHandler.sendMessage(UIHandler.MSG_TOAST, "温馨提示: 操作太快,请稍候");
					mLastToastTime = clickTime;
				}
				mLastClickTime = clickTime;
				return true;
			}
		}
		else 
		{
			if(mLastClickTime > 0 && timeSpace < diff) 
			{
				// 非常快就不显示，不然一直显示
				if(clickTime-mLastToastTime > 1800)
				{
					UIHandler.sendMessage(UIHandler.MSG_TOAST, "温馨提示: 操作太快,请稍候");
					mLastToastTime = clickTime;
				}
				mLastClickTime = clickTime;
				return true;
			}
		}
		mLastClickTime = clickTime;
		mLastButtonId  = buttonId;
		return false;
	}
}
