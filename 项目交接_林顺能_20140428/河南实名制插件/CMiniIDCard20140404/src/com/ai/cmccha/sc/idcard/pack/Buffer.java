package com.ai.cmccha.sc.idcard.pack;

import java.util.ArrayList;
 

public class Buffer 
{
	private ArrayList<Byte> mList = new ArrayList<Byte>(1024);

	public void add(byte b) 
	{
		mList.add(b);
	}
  
	public void add(byte[] b) 
	{
		int i=0;
		int len = b.length;
		while(i < len)
		{
			mList.add(b[i]);
			i++;
		}
	}

	public void add(String str) 
	{
		add(str.getBytes());
	}

	public void clear(){
		mList.clear();
	}

	public byte[] getbytes()
	{
		int len = mList.size();
		byte[] res = new byte[len];
		for(int i=0; i<len; i++)
		{
			res[i] = mList.get(i);
		}
		return res;
	}
}
