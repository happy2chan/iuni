package com.ai.cmccha.sc.idcard;

import java.io.File;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Environment;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.Toast;

import com.ai.cmccha.sc.idcard.bean.Product;
import com.ai.cmccha.sc.idcard.util.StringUtil;
import com.ai.cmccha.sc.plugin.SerializableCookie;
import com.ym.idcard.reg.PreferencesBCR;

public class App extends Application 
{
	protected static String 		TAG = App.class.getSimpleName();
	private static String 			APP_PACKAGE_NAME 						= "scan.bcr";
	private static String 			APP_PATH 								= "/data/data/scan.bcr";
	public static String 			FIELD_DIR 								= "/data/data/scan.bcr/tmp";
	public static final String 		FSPACE 									= "  ";
	public static final String 		ICON_FOLDER 							= "/icons";
	public static final int 		ICON_HEIGHT 							= 60;
	public static final String 		ICON_PREFIX 							= "/icon_";
	public static final int 		ICON_WIDTH 								= 80;
	public static final String 		IMAGE_FOLDER 							= "/images";
	public static final String 		SDCARD_ROOT_PATH 						= Environment.getExternalStorageDirectory().getPath();
	public static final String 		SDCARD_BASE_PATH 						= SDCARD_ROOT_PATH + "/idcard";
	public static final String 		SPACE 									= " ";
	public static final String 		TAB 									= "    ";
	public static final String		ShARED_FILE_NAME						= "config";
	public static String			URL1									= "url1";
	public static String			URL2									= "url2";
	public static String			URL3									= "url3";
	public static String			URL4									= "url4";
	public static String			URL5									= "url5";
	//public static String			cgxjjgl									= "cgxjjgl";
	//public static String			SvcNum									= "SvcNum";
	public static String			sc_tel									= "sc_tel";
	public static String			ms_tel									= "ms_tel";
	public static String			bossCode								= "bossCode";
	public static String			optrid   								= "optrid";
	// 上一次缓存套餐时间
	public static String			LAST_EDIT_TIME   						= "last_edit_time";					
	
	public static String			COOKIE									= "cookie";
	private static final String 	AGENT_ID 								= "key_agent_id";
	private static final String 	AGENT_PWD								= "key_agent_pwd";
	private static final String 	ACCESS_TOKEN 							= "key_access_token";
	private static final String 	FIRST 									= "key_first_run";
	private static final String 	AGENT_NAME 								= "key_agent_name";
	// 自动登录标志
	private static final String		AUTO_LOGIN								= "key_auto_login";
	private static Context 			context;
	private static Resources 		res; 
	public static List<SerializableCookie> cookieList;
 
	public App() 
	{
	}

	public static boolean checkExternalMemoryAvailable() 
	{
		if (getExternalMemoryAvailableSize() / 1024L / 1024L < 1L)
			return false;
		else
			return true;
	}

	public static DisplayMetrics getDisplayMetrics() 
	{
		DisplayMetrics displaymetrics = new DisplayMetrics();
		((WindowManager) App.context().getSystemService("window"))
				.getDefaultDisplay().getMetrics(displaymetrics);
		return displaymetrics;
	}
	
	public static float dpToPixel(float dp) 
	{
		return dp * (getDisplayMetrics().densityDpi / 160F);
	}
	
	public static String getAppLibPath() 
	{
		return APP_PATH + "/lib";
	}

	public static String getAppPackageName() 
	{
		return APP_PACKAGE_NAME;
	}

	public static String getAppPath() 
	{
		return APP_PATH;
	}

	public static String getBaseDir()
	{
		return SDCARD_BASE_PATH;
	}

	public static long getExternalMemoryAvailableSize()
	{
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) 
		{
			StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
			return (long) statfs.getBlockSize() * (long) statfs.getAvailableBlocks();
		} 
		else 
		{
			return -1L;
		}
	}

	public static long getExternalMemoryTotalSize() 
	{
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) 
		{
			StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
			return (long) statfs.getBlockSize() * (long) statfs.getBlockCount();
		} 
		else 
		{
			return -1L;
		}
	}

	public static String getImagePath(String imgName) 
	{
		return SDCARD_BASE_PATH + "/images/" + imgName;
	}
	
	public static String getImagesDir() 
	{
		return SDCARD_BASE_PATH + "/images/";
	}
	
	private void initDir() 
	{
		File file = new File(getImagesDir());
		if (!file.exists())
		{
			file.mkdirs();
		}
		File file4 = new File(FIELD_DIR);
		if (!file4.exists()) 
		{
			file4.mkdir();
		}
	}
	
	public static void updateUiLang(Context context) 
	{
		Configuration config = context.getResources().getConfiguration();
		switch (PreferencesBCR.getUiLanguage(context))
		{
		case 1:
			config.locale = Locale.ENGLISH;
			break;
			
		case 2:
			config.locale = Locale.JAPANESE;
			break;
			
		case 3:
			config.locale = Locale.SIMPLIFIED_CHINESE;
			break;
			
		case 4:
			config.locale = Locale.TRADITIONAL_CHINESE;
			break;
			
		default:
			config.locale = Locale.SIMPLIFIED_CHINESE;
			PreferencesBCR.saveUiLanguage(context, 3);
			break;
		}
		context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
	}
	
	public void onConfigurationChanged(Configuration configuration)
	{
		super.onConfigurationChanged(configuration);
	}

	public void onCreate() 
	{
		super.onCreate();
		
		context = getApplicationContext();
		res = getResources();
		APP_PACKAGE_NAME = getPackageName();
		APP_PATH = "/data/data/" + APP_PACKAGE_NAME;
		FIELD_DIR = getFilesDir() + "/tmp";
		Product.init(APP_PACKAGE_NAME);
		// 
		initDir();
		// 
		//CrashHandler crashHandler = CrashHandler.getInstance();
        //crashHandler.init(getApplicationContext());
	}
	
	public void onLowMemory() 
	{
		super.onLowMemory();
	}

	public void onTerminate() 
	{
		super.onTerminate();
	}
	
	// 是否已经登录
	public static boolean hasAccessToken() 
	{
		String token = getAccessToken();
		if (StringUtil.isEmptyOrNull(token)) 
		{
			return false;
		}
		return true;
	}
	
	public static String getAccessToken() 
	{
		return getPreferences().getString(ACCESS_TOKEN, null);
	}
	
	public static void setAccessToken(String token) 
	{
		Editor ed = getPreferences().edit();
		ed.putString(ACCESS_TOKEN, token);
		ed.commit();
	}
	
	// 设置是否自动登录
	public static boolean getAutoLoginFlag() 
	{
		return getPreferences().getBoolean(AUTO_LOGIN, false);
	}
	
	// 设置是否自动登录
	public static void setAutoLoginFlag(boolean flag)
	{
		Editor ed = getPreferences().edit();
		ed.putBoolean(AUTO_LOGIN, flag);
		ed.commit();
	}

	public static long getLastPauseTime() 
	{
		return getPreferences().getLong("PAUSE_TIME", 0);
	}
	
	public static void setLastPauseTime(long time) 
	{
		Editor ed = getPreferences().edit();
		ed.putLong("PAUSE_TIME", time);
		ed.commit();
	}
	
	// APP是否第一次运行
	public static boolean isFirstRun() 
	{
		return getPreferences().getBoolean(FIRST, true);
	}

	// 设置APP的运行状态
	public static void setFirstRun(boolean b) 
	{
		Editor ed = getPreferences().edit();
		ed.putBoolean(FIRST, b);
		ed.commit();
	}
	
	public static String getAgentId() 
	{
		return getPreferences().getString(AGENT_ID, null);
	}
	
	public static void setAgentId(String agentId)
	{
		Editor ed = getPreferences().edit();
		ed.putString(AGENT_ID, agentId);
		ed.commit();
	}
	
	public static String getAgentPwd()
	{
		return getPreferences().getString(AGENT_PWD, null);
	}
	
	public static void setAgentPwd(String agentPwd)
	{
		Editor ed = getPreferences().edit();
		ed.putString(AGENT_PWD, agentPwd);
		ed.commit();
	}
	

	public static void setAgentName(String agentName)
	{
		Editor ed = getPreferences().edit();
		ed.putString(AGENT_NAME, agentName);
		ed.commit();
	}
	
	public static String getAgentName() {
		return getPreferences().getString(AGENT_NAME, null);
	}
	
	public static SharedPreferences getPreferences() 
	{
		return context().getSharedPreferences("idcard", 0);
	}

	public static Context context() 
	{
		return  context;
	}

	public static Resources resources() 
	{
		return res;
	}
	
	public static void showToast(int message) 
	{
		Toast.makeText(context(), message, Toast.LENGTH_SHORT).show();
	}

	public static void showToast(String message) 
	{
		Toast.makeText(context(), message, Toast.LENGTH_SHORT).show();
	}
	
	public static void saveDisplaySize(Activity activity) 
	{
		DisplayMetrics displaymetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		SharedPreferences.Editor editor = getPreferences().edit();
		editor.putInt("screen_width", displaymetrics.widthPixels);
		editor.putInt("screen_height", displaymetrics.heightPixels);
		editor.putFloat("density", displaymetrics.density);
		editor.commit();
	}
	
	public static int[] getDisplaySize(Context context) 
	{
		SharedPreferences sp = getPreferences();
		return new int[] { sp.getInt("screen_width", 480), sp.getInt("screen_height", 854)};
	}
}
