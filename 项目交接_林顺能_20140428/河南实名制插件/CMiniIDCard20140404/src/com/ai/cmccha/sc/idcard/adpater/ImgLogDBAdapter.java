package com.ai.cmccha.sc.idcard.adpater;

import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

/**
 * 拍照图片上传日志的数据库操作类
 * 
 * @author acer
 * 
 */
public class ImgLogDBAdapter extends DBadapter
{
	private static final String	DB_TABLE		= "IMG_LOG";		// 表名
	private static final String	DB_UPLOAD_TABLE	= "IMG_UPLOAD_LOG"; // 服务器上传表的表名
	private static final String	COL_BLLNUM		= "BLLNUM";			// 流水号
	private static final String	COL_PHOTONUM	= "PHOTONUM";		// 照片数
	private static final String	COL_PHOTONAMES	= "PHOTONAMES";		// 照片文件名
	private static final String	COL_FILEPATH	= "FILEPATH";		// 文件路径
	private static final String	COL_REMOTEPATH	= "REMOTEPATH";		// 要提交的目录

	public ImgLogDBAdapter(Context context)
	{
		super(context);
	}

	public void addImgLog(String bllnum, String photonum, String photonames)
	{
		ContentValues cv = new ContentValues();
		cv.put(COL_BLLNUM, bllnum);
		cv.put(COL_PHOTONUM, photonum);
		cv.put(COL_PHOTONAMES, photonames);
		open();
		db.insert(DB_TABLE, null, cv);
		close();
	}

	public void deleteImgLog(String bllnum)
	{
		open();
		db.delete(DB_TABLE, COL_BLLNUM + "= ?", new String[]
		{ bllnum });
		close();
	}

	/**
	 * 根据流水号查询单行数据
	 * 
	 * @param bllnum
	 * @return
	 */
	public Map<String, String> queryImgLog(String bllnum)
	{
		String sql = "SELECT " + COL_BLLNUM + ", " + COL_PHOTONAMES + ", " + COL_PHOTONUM + " FROM " + DB_TABLE
				+ " WHERE " + COL_BLLNUM + " = " + bllnum;
		open();
		Cursor cursor = db.rawQuery(sql, null);
		Map<String, String> result = cursorToMap(cursor);
		close();
		return result;
	}

	/**
	 * 查询所有的照片日志
	 * 
	 * @return
	 */
	public List<Map<String, String>> queryImgLogs()
	{
		String sql = "SELECT " + COL_BLLNUM + ", " + COL_PHOTONAMES + ", " + COL_PHOTONUM + " FROM " + DB_TABLE;
		open();
		Cursor cursor = db.rawQuery(sql, null);
		List<Map<String, String>> logs = cursorToList(cursor);
		close();
		return logs;
	}

	/**
	 * 查询图片上传服务器失败的日志
	 * 
	 * @return
	 */
	public List<Map<String, String>> queryUploadLogs()
	{
		String sql = "SELECT " + COL_FILEPATH + ", " + COL_REMOTEPATH + " FROM " + DB_UPLOAD_TABLE;
		open();
		Cursor cursor = db.rawQuery(sql, null);
		List<Map<String, String>> logs = cursorToList(cursor);
		close();
		return logs;
	}

	/**
	 * 插入图片上传服务器的信息
	 * 
	 * @param bllnum
	 * @param photonum
	 * @param photonames
	 */
	public void addUploadLog(String filePath, String remotePath)
	{
		ContentValues cv = new ContentValues();
		cv.put(COL_FILEPATH, filePath);
		cv.put(COL_REMOTEPATH, remotePath);
		open();
		db.insert(DB_UPLOAD_TABLE, null, cv);
		close();
	}

	public void deleteImgUploadLog(String filePath)
	{
		open();
		db.delete(DB_UPLOAD_TABLE, COL_FILEPATH + " = ?", new String[]
		{ filePath });
		close();
	}
}
