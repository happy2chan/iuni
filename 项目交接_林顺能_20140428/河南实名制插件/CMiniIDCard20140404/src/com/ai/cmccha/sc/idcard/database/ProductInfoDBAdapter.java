package com.ai.cmccha.sc.idcard.database;

import java.util.ArrayList;
import java.util.List;

import com.ai.cmccha.sc.idcard.bean.ProductInfo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class ProductInfoDBAdapter extends DBadapter
{
	// 数据库表名
	private static final String	DB_TABLE			= "PRODUCT_INFO";
	// 表中一条数据的ID
	public static final String	KEY_ID				= "_id";
	// 套餐ID
	private static final String	KEY_PRO_ID			= "pro_id";
	// 套餐名称
	private static final String	KEY_PRO_NAME		= "pro_name";
	// 套餐note
	private static final String	KEY_PRO_NOTE		= "pro_note";
	// 套餐编号
	private static final String	KEY_PRO_CODE		= "pro_code";
	// 套餐品牌ID
	private static final String	KEY_PRO_BRAND_ID	= "pro_brand_id";
	// 套餐错误信息
	private static final String	KEY_PRO_ERRMSG		= "pro_errmsg";
	// 套餐类型
	private static final String	KEY_PRO_TYPE		= "pro_type";
	// 套餐描述
	private static final String	KEY_PRO_DESC		= "pro_desc";
	// 是否可售
	private static final String	KEY_PRO_CANCEL		= "pro_cancel";
	// 品牌类型 全球通、动感地带、神州行
	private static final String	KEY_PRO_BRAND_TYPE	= "pro_brand_type";

	public ProductInfoDBAdapter(Context context)
	{
		super(context);
	}

	public long insert(ProductInfo product)
	{
		ContentValues values = new ContentValues();
		values.put(KEY_PRO_ID, product.getID());
		values.put(KEY_PRO_NAME, product.getName());
		values.put(KEY_PRO_CODE, product.getCode());
		values.put(KEY_PRO_DESC, product.getDesc());
		values.put(KEY_PRO_ERRMSG, product.getErrMsg());
		values.put(KEY_PRO_NOTE, product.getNote());
		values.put(KEY_PRO_TYPE, product.getType());
		values.put(KEY_PRO_BRAND_ID, product.getBrandID());
		values.put(KEY_PRO_CANCEL, product.isCanCel());
		values.put(KEY_PRO_BRAND_TYPE, product.getBrandType());
		return db.insert(DB_TABLE, KEY_ID, values);
	}

	public void clear()
	{
		db.execSQL("delete from " + DB_TABLE + " where 1=1");
	}

	public List<ProductInfo> fetchDataByTag(String type)
	{
		Cursor cursor = db.rawQuery("select * from " + DB_TABLE + " where " + KEY_PRO_BRAND_TYPE + " = '" + type + "'",
				null);
		List<ProductInfo> result = new ArrayList<ProductInfo>();
		if (cursor != null)
		{
			int nCount = cursor.getCount();
			Log.e("", "总共搜索到" + nCount + "条套餐记录");
			if (cursor.moveToFirst())
			{
				do
				{
					ProductInfo pro = new ProductInfo();
					pro.setBrandID(cursor.getString(cursor.getColumnIndex(KEY_PRO_BRAND_ID)));
					pro.setBrandType(cursor.getString(cursor.getColumnIndex(KEY_PRO_BRAND_TYPE)));
					// pro.setCanCel(cursor.getString(cursor.getColumnIndex(KEY_PRO_BRAND_ID)));
					pro.setCode(cursor.getString(cursor.getColumnIndex(KEY_PRO_CODE)));
					pro.setDesc(cursor.getString(cursor.getColumnIndex(KEY_PRO_DESC)));
					pro.setErrMsg(cursor.getString(cursor.getColumnIndex(KEY_PRO_ERRMSG)));
					pro.setID(cursor.getString(cursor.getColumnIndex(KEY_PRO_ID)));
					pro.setName(cursor.getString(cursor.getColumnIndex(KEY_PRO_NAME)));
					pro.setNote(cursor.getString(cursor.getColumnIndex(KEY_PRO_NOTE)));
					pro.setType(cursor.getString(cursor.getColumnIndex(KEY_PRO_TYPE)));
					result.add(pro);
				}while (cursor.moveToNext());
			}
			cursor.close();
		}
		return result;
	}
}
