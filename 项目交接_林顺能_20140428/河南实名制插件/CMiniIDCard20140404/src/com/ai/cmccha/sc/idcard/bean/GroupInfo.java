package com.ai.cmccha.sc.idcard.bean;

public class GroupInfo 
{

	private String 		groupname;
	private Long 		groupid;
	
	public String getGroupname() {
		return groupname;
	}
	
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	
	public Long getGroupid() {
		return groupid;
	}
	
	public void setGroupid(Long groupid) {
		this.groupid = groupid;
	}

}
