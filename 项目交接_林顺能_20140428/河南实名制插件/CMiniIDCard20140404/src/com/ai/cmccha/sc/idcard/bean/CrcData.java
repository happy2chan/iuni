package com.ai.cmccha.sc.idcard.bean;

public class CrcData 
{
	// 0x01: 业务菜单
	public static String 		mCrcMenu		= "0000";
	
	// 0x02:号段
	public static String		mCrcNumber		= "0000";
	
	// 0x03: 打印凭条广告信息
	public static String 		mCrcPrint		= "0000";
	
	// 0x04: 终端收件箱内容更新
	public static String 		mCrcMail		= "0000";
}
