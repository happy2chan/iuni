package com.ai.cmccha.sc.plugin;

import com.ai.cmccha.sc.idcard.R;

import me.sfce.android.framework.plugin.annotation.PluginDescriptionA;

@PluginDescriptionA
public class PluginDescription extends PluginInterface
{
	public PluginDescription()
	{
		this.label = "Test";
		this.icon = R.drawable.project_icon;
		this.desc = "this is test plugin";
	}
}
