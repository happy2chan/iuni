package com.ai.cmccha.sc.idcard.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// 传出参数
public class BillParam 
{
	//	0001	销售信息上报
	//	0002	订货信息上报
	
	//	0003	供求信息上报
	public Map<String, Object> mSupplyInfo	  = new HashMap<String,Object>();
	//	0004	投诉信息上报
	public Map<String, Object> mComplainInfo  = new HashMap<String,Object>();
	//	0005	商务信息获取
	public Map<String, Object> mBusinessInfo  = new HashMap<String,Object>();
	//	0006	政务信息获取
	public Map<String, Object> mGovmentInfo   = new HashMap<String,Object>();
	
	//	0007	供求反馈信息获取
	public Map<String, Object> mSupplyFeedback   	 = new HashMap<String,Object>();
	//	0008	投诉处理信息获取
	public Map<String, Object> mComplainFeedback   	 = new HashMap<String,Object>();
	
	//  0009商品信息下发、更新商品信息
	public List<List<String>> mUpdategoodinfo = new ArrayList<List<String>>();
	//	000A	商户信息获取
	public List<String> 	  mShopinfo		  = new ArrayList<String>();
	
	//	000B	登录请求
	//	000D	心跳同步
	//	0010	登出请求（HTTP）
	
	//	0011	订单处理状态查询
	
	//  0012价格行情
	public List<List<String>> mPricedetails   = new ArrayList<List<String>>();
	//  0013商品信息上报反馈
	public List<List<String>> mLocalGood      = new ArrayList<List<String>>();
	//	0014	天气预报查询
	public List<Map<String, Object>> mWeather = new ArrayList<Map<String, Object>>();
	//	0015	黄历信息查询
	public List<Map<String, Object>> mCalendar = new ArrayList<Map<String,Object>>();
	//  0016           全量商品信息 
	public List<List<String>> mFulldoseinfo   = new ArrayList<List<String>>();
}
