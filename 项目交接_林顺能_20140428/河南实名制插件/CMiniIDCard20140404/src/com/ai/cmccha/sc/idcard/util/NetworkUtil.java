package com.ai.cmccha.sc.idcard.util;

import com.ai.cmccha.sc.idcard.uilogic.UILogic;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

public class NetworkUtil 
{
	// 获取IMSI
	public static String getImsi(Context context)
	{
		try
		{
			if(context != null)
			{
				TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
				if(manager != null) 
				{
					return manager.getSubscriberId();
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "";
	}
	
	// 获取IMEI
	public static String getImei(Context context)
	{
		try
		{
			if(context != null) 
			{
				TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
				if(manager != null) 
				{
					return manager.getDeviceId();
				}
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "";
	}
	
	// 获取电话号码
	public static String getPhoneNum(Context context) 
	{
		try
		{
			if(context != null) 
			{
				TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
				if(manager != null) 
				{
					return manager.getLine1Number();
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return "";
	}
	
	// 获取小区信息
	public static String getLacci(Context context) 
	{
		try
		{
			TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			if(manager != null) 
			{
				GsmCellLocation gcl = (GsmCellLocation) manager.getCellLocation();
				String lac = String.valueOf(gcl.getLac());
				String ci = String.valueOf(gcl.getCid());
				return lac + ci;
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return "";
	}
	
	// 获取当前网络是否连接
	public static boolean isConnect(Context context)
	{
		try
		{
			if(context != null) 
			{
				ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
				if (manager != null)
				{
					NetworkInfo networkInfo = manager.getActiveNetworkInfo();
					if (networkInfo != null && networkInfo.isConnected()) 
					{
						// 判断当前网络是否已经连接
						if (networkInfo.getState() == NetworkInfo.State.CONNECTED)
						{
							return true;
						}
					}
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	// 得到当前连接的网络类型
	public static String getNetWorkType(Context context) 
	{
		try
		{
			if(context != null) 
			{
				ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
				String sNetType = "";
				switch(manager.getActiveNetworkInfo().getType()) 
				{
					case ConnectivityManager.TYPE_WIFI:
						sNetType = "WIFI网络";
						break;
						
					case ConnectivityManager.TYPE_MOBILE:
						sNetType = "移动网络";
						break;
						
					default:
						sNetType = "其它网络";
						break;			
				}
				return sNetType;
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return "未知网络";
	}
	
	// 检查服务器IP的格式
	public static boolean checkServerIP(String tempstr) 
	{
		for(int i=0; i<3; i++) 
		{
			try 
			{
				int nIndex = tempstr.indexOf('.');
				if(nIndex == -1)
				{
					Log.e("", "没有'.' ");
					return false;
				}
				// 截取点分十进制IP
				Log.d("", "1." + tempstr);
				String temp = tempstr.substring(0, nIndex);
				Log.d("", "2." + temp);
				Log.d("", "3." + tempstr);
				tempstr = tempstr.substring(nIndex+1, tempstr.length());
				Log.d("", "4." + tempstr);
				if(UILogic.isNUll(temp) || UILogic.isNUll(tempstr)) 
				{
					Log.e("", "点分十进制字段存在为空的情况 ");
					return false;
				}
				// 转换成十进制
				int nTemp = Integer.parseInt(temp);
				if(nTemp > 255 || nTemp < 0) 
				{
					Log.e("", "IP字段不合法1");
					return false;
				}
				// 最后一个点分十进制
				if(i >= 2)
				{
					nTemp = Integer.parseInt(tempstr);
					if(nTemp > 255 || nTemp < 0)
					{
						Log.e("", "IP字段不合法2 ");
						return false;
					}
				}
			}
			catch(Exception e) 
			{
				e.printStackTrace();
				Log.e("", "转换IP字段异常 ");
				return false;
			}
		}
		return true;
	}
		
	// 检查服务器端口的格式
	public static boolean checkServerPort(String tempstr) 
	{
		try
		{
			int nPort = Integer.parseInt(tempstr);
			if(nPort <= 0 || nPort > 65535) 
			{
				return false;
			}
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean checkIsUseWIFI(Context context) 
	{
		ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if(manager != null) 
		{
			NetworkInfo networkInfo = manager.getActiveNetworkInfo();
			if(networkInfo != null) 
			{
				return networkInfo.getType() == ConnectivityManager.TYPE_WIFI;
			}
		}
		return false;
	}
}
