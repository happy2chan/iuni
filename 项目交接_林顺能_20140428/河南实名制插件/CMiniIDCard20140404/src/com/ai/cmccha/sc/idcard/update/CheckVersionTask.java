package com.ai.cmccha.sc.idcard.update;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;

import com.ai.cmccha.sc.idcard.MainActivity;
import com.ai.cmccha.sc.idcard.global.Global;
import com.ai.cmccha.sc.idcard.util.FileOpr;
import com.ai.cmccha.sc.idcard.util.StringUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;

// 从服务器获取xml解析并进行比对版本号 
public class CheckVersionTask 
{ 
	private static final String 	TAG 				= "升级";
	protected static final int 		DOWN_ERROR 			= 0;
	
	private UpdataInfo 				mUpdataInfo;
	private String 					mStrErrMsg 			= "";
	private String 					mStrVersionName 	= null;
	protected Context 				mContext;
	
	public CheckVersionTask(Context context) 
	{
		mContext 		= context;
		mStrVersionName = getVersionName(); 
	}
	
	// 获取当前程序的版本号
	public String getVersionName()
	{
		if(mStrVersionName != null) 
		{
			return mStrVersionName;
		}
		
		try 
		{
			// 获取PackageManager的实例
			PackageManager packageManager = mContext.getPackageManager();
			// getPackageName()是你当前类的包名，0代表是获取版本信息
			PackageInfo packInfo = packageManager.getPackageInfo(mContext.getPackageName(), 0);
			return packInfo.versionName;
		} 
		catch (NameNotFoundException e) 
		{
			e.printStackTrace();
			return "0.0.0";
		}
	}
	
	public void findHistoryVersionAndUnintall() 
	{
		// 获得PackageManager对象 
		PackageManager pm = mContext.getPackageManager();
		Intent mainIntent = new Intent(Intent.ACTION_MAIN, null); 
		mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);  
		List<ApplicationInfo> resolveInfos = pm.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES);  
        if (resolveInfos != null) 
        {  
            for (ApplicationInfo reInfo : resolveInfos) 
            {  
            	// 获得应用程序的包名 
            	String pkgName = reInfo.packageName;
                Log.v("xxx", "包名" + pkgName);
                if("com.ai.cmccha.sc.idcard".equals(pkgName.trim())) 
                {
                	Log.v("xxx", "检测到历史遗留版本，开始进行卸载");
                	new AlertDialog.Builder(mContext)
                		.setTitle("注意")
                		.setMessage("检测到早期版本的程序，该程序已经不能使用，我们将进行卸载")
                		.setCancelable(false)
                		.setPositiveButton("确定", new DialogInterface.OnClickListener()
                		{
							@Override
							public void onClick(DialogInterface arg0, int arg1) 
							{
								uninstallAPK(mContext);
							}
                		})
                		.show();
                	break;
                }
            }
        }
	}
	
	// 检测升级
	public int checkUpdate() 
	{
		try 
		{	
			int flag = isApkUpdate();
			if (flag == 0) 
			{
				Log.i(TAG, "版本号相同无需升级");
				return 0;
			} 
			else 
			{
				Log.e(TAG, "版本号不同, 需要升级, 当前版本为: " + mStrVersionName + ", 服务器版本为: " + mUpdataInfo.getVersion());
				return 1;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return 0;
		}
	}
	
	// 检测服务器上是否有更新版本,本接口是检验APK升级
	// 0-不需要更新	1-提示更新
	public int isApkUpdate() 
	{
		int bUpdateFlag = 0;
		Log.e(TAG, "软件升级url为: " + Global.mStrServerUrlBase);
		
		if(!StringUtil.isEmptyOrNull(Global.mStrServerUrlBase)) 
		{
			try 
			{
				// 下载升级配置文件，解析是否该升级
				getUpdataXmlInfo();
				if(mUpdataInfo == null) 
				{
					bUpdateFlag = 0;
					mStrErrMsg = "没有找到版本信息文件";
					Log.e(TAG, mStrErrMsg);
				}
				else
				{
					// 提示更新
					if (versionCompare(mStrVersionName.trim(), mUpdataInfo.getVersion().trim())) 
					{
						bUpdateFlag = 1;
					}
				}
			} 
			catch (Exception e) 
			{
				bUpdateFlag = 0;
				e.printStackTrace();
			}
		}
		return bUpdateFlag;
	}
	
	// 版本判断，每个.来判断
	// true 升级
	private boolean versionCompare(String strOld, String strNew)
	{
		String str1,str2;
		int pos;
		for (int i = 0; i < 3; i++) 
		{
			System.out.println("Old : " + strOld + "  New : " + strNew);
			pos = strOld.indexOf(".");
			if(pos <= 0)
			{
				// 最后一个
				str1 = strOld;
			}
			else 
			{
				str1 = strOld.substring(0, pos);
				strOld = strOld.substring(pos + 1);
			} 
			
			pos = strNew.indexOf(".");
			if(pos <= 0 || pos >= strNew.length())
			{
				str2 = strNew;
			}
			else 
			{
				str2 = strNew.substring(0, pos);
				strNew = strNew.substring(pos + 1);
			}
			 
			System.out.println("str1 : " + str1 + "  str2 : " + str2);
			if(Integer.parseInt(str1) > Integer.parseInt(str2))
			{
				return false;
			}
			else if(Integer.parseInt(str1) < Integer.parseInt(str2))
			{
				return true;
			}
		}
		return false;
	}
	
	// 获得服务器上的版本信息
	private void getUpdataXmlInfo() 
	{		
		try 
		{
			// 包装成URL的对象
			URL url = new URL(Global.mStrServerUrlBase + "/update/version.xml");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			if(conn != null) 
			{
				conn.setConnectTimeout(Global.CONNECT_TIMEOUT);
				conn.setReadTimeout(Global.READ_TIMEOUT);
				// 判断是否连接上了
				InputStream is = conn.getInputStream();
				if(is != null) 
				{
					// 解析各个字段
					mUpdataInfo = analyXmlUpdataInfo(is);
				}
				conn.disconnect();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			
		}
	}
	
	/* 
	 * 用pull解析器解析服务器返回的xml文件 (xml封装了版本号) 
	 * 这里是遍历XML，首先找到area_code，只要sBaseRemotePath最后的区域编码一致。
	 * 注意area_code在XML要在节点的最前。此时返回的是XML最后area_code匹配的信息
	 */  
	public static UpdataInfo analyXmlUpdataInfo(InputStream is) throws Exception
	{  	
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int len = 0;
		int total_len = 0;
		byte [] buf = new byte[1024*5];
		
		while((len = is.read(buf)) > 0)
		{
			baos.write(buf, 0, len);
			total_len += len;
		}
		buf = baos.toByteArray();
		baos.close();
		is.close();
		
		String xml = new String(buf,"GBK");
		Log.v("xml", xml);
		// 文件是空的
		if(xml == "" || total_len <= 0) 
		{
			return null;
		}
		
	    XmlPullParser parser = Xml.newPullParser();
	    // 设置解析的数据源 
	    parser.setInput(new StringReader(xml));
	    int type = parser.getEventType();
	    UpdataInfo info = new UpdataInfo();
	    
	    while(type != XmlPullParser.END_DOCUMENT)
	    {  
	        switch (type) 
	        {  
	        case XmlPullParser.START_TAG:
	        	// 获取版本号
	            if("version".equals(parser.getName())) 
	            {
	                info.setVersion(parser.nextText());
	            }
	            // 获取要升级的APK文件
	            else if ("url".equals(parser.getName())) 
	            {
	                info.setUrl(parser.nextText());
	            }
	            // 获取该文件的信息
	            else if ("description".equals(parser.getName())) 
	            {
	                info.setDescription(parser.nextText());
	            }
	            break;
	        }
	        type = parser.next();  
	    }  
	    return info;  
	}  
	
	private static final String KEY_ISDOWNLOADSUCCESS = "isDownloadSuccess";
	private static final String KEY_BREAKPOINT = "breakPoint";
	private static final String KEY_LASTVERSION = "lastVersion";
	
	// 检测断点下载
	public void checkBreakDownLoad() 
	{
		SharedPreferences sp = ((MainActivity)mContext).getPreferences(Context.MODE_PRIVATE);
		// 是否已经下载成功
		boolean isDownloadSuccess = sp.getBoolean(KEY_ISDOWNLOADSUCCESS, true);
		// 断点的长度
		final long breakPoint = sp.getLong(KEY_BREAKPOINT, 0L);
		// 当前下载前的版本
		String version = sp.getString(KEY_LASTVERSION, "0.0.0");
		
		Log.v("isDownloadSuccess", isDownloadSuccess+"");
		Log.v("breakPoint", breakPoint+"");
		Log.v("lastversion", version+"");
		Log.v("currentversion", mUpdataInfo.getVersion()+"");
		
		// 与上次更新的版本相同
		if(version.equals(mUpdataInfo.getVersion())) 
		{
			Log.v(TAG, "检测上次是否下载完成...");
			// 上次未更新完成，则从breakPoint处重新下载
			if(!isDownloadSuccess) 
			{
				Log.v(TAG, "检测上次下载未完成, 继续下载, 下载点: " + breakPoint);
				downLoadAPKWithBreakPoint(breakPoint);
			}
			else
			{
				Log.v(TAG, "上次下载已完成, 重新下载");
				downLoadAPKWithBreakPoint(0);
			}
		}
		else
		{
			Log.v(TAG, "版本不同重新下载");
			downLoadAPKWithBreakPoint(0);
		}
	}
	
	// 断点下载
	private void downLoadAPKWithBreakPoint(final long breakPoint) 
	{
		if(FileOpr.isSdcardReady() == false) 
		{
			Log.e(TAG, "SD卡不可用!");
			return;
		}
		
		HttpURLConnection urlConnection = null;
		RandomAccessFile accessFilec = null;
		InputStream is = null;
		long 	tempBreakPoint = breakPoint;
		long 	totalLength = 0;
		File 	file = null;
		byte[] 	buf = new byte[1024*2];
		int 	len = 0;
		
		try
		{
			Log.v(TAG, "下载URL: " + mUpdataInfo.getUrl());
			urlConnection = (HttpURLConnection)new URL(mUpdataInfo.getUrl()).openConnection();
			urlConnection.setConnectTimeout(Global.CONNECT_TIMEOUT);
			urlConnection.setReadTimeout(Global.READ_TIMEOUT);
			if(urlConnection.getResponseCode() != 200) 
			{  
				Log.e(TAG, "服务端响应错误!");
				return;
			}
			
			totalLength = urlConnection.getContentLength();
			urlConnection.disconnect();
			urlConnection = null;
			
			Log.v(TAG, "当前下载文件大小: " + totalLength);
			
			// 确保该文件目录存在
			file = new File(Environment.getExternalStorageDirectory().toString() + "/mini/apk/CUAndroidMini.apk");
			if(!file.getParentFile().exists())
			{
				file.getParentFile().mkdirs();
			}
			if(file.exists() && (breakPoint <= 0 && totalLength != file.length())) 
			{
				// 如果重新下载则先删除文件
				Log.v(TAG, "文件是否存在: " + file.exists());
				Log.v(TAG, "文件断点是否为0: " + breakPoint);
				Log.v(TAG, "下载文件大小是否和本地文件大小相同: " + totalLength + "==" + file.length());
				Log.v(TAG, "删除文件");
				
				file.delete();
				tempBreakPoint = 0;
			}
			
			accessFilec = new RandomAccessFile(file, "rwd");
			// 设定文件的大小
			if(totalLength != accessFilec.length())
			{
				accessFilec.setLength(totalLength);
			}
			Log.v(TAG, "设定断点: " + tempBreakPoint);
			accessFilec.seek(tempBreakPoint);
			// 更新配置
			updateSp(false, tempBreakPoint, mUpdataInfo.getVersion());
			
			urlConnection = (HttpURLConnection) new URL(mUpdataInfo.getUrl()).openConnection();
			urlConnection.setConnectTimeout(Global.CONNECT_TIMEOUT);
			urlConnection.setReadTimeout(Global.READ_TIMEOUT);
			urlConnection.setRequestProperty("Range", "bytes=" + tempBreakPoint + "-" + totalLength);
			
			is = urlConnection.getInputStream();
			while((len = is.read(buf)) > 0)
			{
				accessFilec.write(buf, 0, len);
				tempBreakPoint += len;
				// 更新配置
				updateSp(false, tempBreakPoint, mUpdataInfo.getVersion());
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();	
		}
		finally
		{
			Log.v(TAG, "当前保存的断点: " + tempBreakPoint);
			Log.v(TAG, "当前保存的是否成功: " + (tempBreakPoint == totalLength));
			Log.v(TAG, "当前保存的最后版本: " + mUpdataInfo.getVersion());
			updateSp((tempBreakPoint==totalLength? true:false), tempBreakPoint, mUpdataInfo.getVersion());
			
			if(is != null) 
			{
				try
				{
					is.close();
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			
			if(accessFilec != null)
			{
				try
				{
					accessFilec.close();
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			
			if(urlConnection != null)
			{
				urlConnection.disconnect();
				urlConnection = null;
			}
		}
	}
	
	// 更新SP配置
	private void updateSp(boolean status, long breakPoint, String strVersion) 
	{
		SharedPreferences preferences = ((MainActivity)mContext).getPreferences(Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putBoolean(KEY_ISDOWNLOADSUCCESS, status);
		editor.putLong(KEY_BREAKPOINT, breakPoint);
		editor.putString(KEY_LASTVERSION, strVersion);
		editor.commit();
	}
	
	// 卸载APK
	// @param packageName
	public static void uninstallAPK(Context context)
	{
		try
		{
	        Uri uri = Uri.parse("package:com.ai.cmccha.sc.idcard");
	        Intent intent=new Intent(Intent.ACTION_DELETE, uri);
	        context.startActivity(intent);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			Log.v("提示", "本机上未找到早版本的apk");
		}
    }  
	
	// 安装APK
	public static void installApk(File file, Context context) 
	{
		 Intent intent = new Intent();
		 intent.setAction(Intent.ACTION_VIEW);
		 intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
		 context.startActivity(intent);
	}
	
	// 安装APK
	public static void installApkRequestCode(File file, Activity activity, int requestCode) 
	{
		 Intent intent = new Intent();
		 intent.setAction(Intent.ACTION_VIEW);
		 intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
		 activity.startActivityForResult(intent, requestCode);
	}
}