package com.ai.cmccha.sc.idcard.uilogic;

import java.util.Calendar;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ai.cmccha.sc.idcard.LoginLayout;
import com.ai.cmccha.sc.idcard.MainActivity;
import com.ai.cmccha.sc.idcard.MainMenuLayout;
import com.ai.cmccha.sc.idcard.R;
import com.ai.cmccha.sc.idcard.dialog.DateTimePickerDialog;
import com.ai.cmccha.sc.idcard.dialog.MessageBoxDialog;
import com.ai.cmccha.sc.idcard.dialog.ProgressDialog;
import com.ai.cmccha.sc.idcard.util.ButtonUtil;

/**
 * 界面逻辑 抽象类，所有的界面逻辑类继承于此类
 * 此类继承于Handler，实现OnClickListener接口
 * @author Administrator
 */
public abstract class UILogic extends Handler implements OnClickListener 
{
	protected 	MainActivity 				mMainActivity;
	protected 	static final int 			ProBar_MAX = 100;				// 进度框的最大值
	protected 	static final int 			ProBar_MIN = 0;					// 进度框的最小值
	private 	static ProgressDialog 		mProDialog = null;				// 进度框
	public 		boolean 					mProDialogShow = false;			// 进度框是否在显示状态
	public 		int  						mDateType = 0;
	public 		static int 					mCurrentViewId = -1;
	public		static String 				mStrProTitle = "";				// 滚动条的标题
	public     	static long 				mLastToastTime = 0;
	
	protected UILogic(MainActivity context) 
	{
		mMainActivity 		= context;
		UIHandler.mHandler 	= this;
	}
	
	// 根据传递的参数，得到具体的子类
	public static UILogic getInstance(int nViewID, MainActivity activity)
	{
		UILogic uilogic 	= null;
		mCurrentViewId 		= nViewID;

		switch (nViewID) 
		{
		// 登录界面
		case R.layout.lay_login:
			uilogic = new LoginLayout(activity);
			break;
			
		// 主界面
		case R.layout.lay_mainmenu:
			uilogic = new MainMenuLayout(activity);
			break;
			
		default:
			mCurrentViewId = -1;
			break;
		}
		return uilogic;
	}
	
	public abstract void go();
	
	// 初始化控件
	protected abstract void initViews();
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
			default:
				break;
		}
	}

	// 接管Activity的按钮事件的接口
	// 若某个界面需要接管某个按钮事件,就继承此方法进行实现,并返回true
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		return false;
	}

	// 每个子类自定义的回调事件监听器,处理事件时优先处理该事件.
	// 每个回调事件都是一次性的,因此每次都要重新设置监听器
	protected interface OnCallBackListener
	{
		public void onCallBack();
	}

	// 自定义回调消息
	protected OnCallBackListener mCallBackListener;
	protected void sendCustomMessage(OnCallBackListener listener)
	{
		mCallBackListener = listener;
		sendEmptyMessage(UIHandler.MSG_CUSTOM);
	}
	
	public boolean onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		return false;
	}
	
	// 当界面被切换时调用此方法.类似于finalize()
	public void onClose() 
	{
		
	}
	
	protected void MsgOK(int msgID) 
	{
		switch (msgID) 
		{
		default:
			break;
		}
	}
	
	protected void MsgCancle(int msgID) 
	{
		switch (msgID) 
		{
		default:
			break;
		}
	}
	
	protected View getview(int id) 
	{
		return mMainActivity.findViewById(id);
	}
	
	// 处理消息
	@Override
	public void handleMessage(Message msg) 
	{
		try 
		{
			boolean isActed = true;
			//System.out.printf("*** msgID = 0x%04x \n", msg.what);
			// 特殊消息动作的响应
			switch(msg.what) 
			{
				// TOAST
				case UIHandler.MSG_TOAST:
					Toast(0, UIHandler.mToastStr);
					break;
			
				// 开启滚动条
				case UIHandler.MSG_PRO_START:
					showProgressDialog(UIHandler.mProTitleStr, UIHandler.mProMsgStr, UIHandler.mProMaxValue, false);
					break;
					
				// 设置滚动条
				case UIHandler.MSG_PRO_STEP:
					setProgress(UIHandler.mProMsgStr, UIHandler.mProValue);
					break;
					
				// 停止滚动条
				case UIHandler.MSG_PRO_STOP:
					cancleProgress();
					break;

				// 开启旋转滚动条
				case UIHandler.MSG_PRO_START_SPINNER:
					showProgressDialog(UIHandler.mProTitleStr, UIHandler.mProMsgStr, UIHandler.mProValue, true);
					break;

				// 弹出无按钮的消息框
				case UIHandler.MSGBOX_NONE:
					MsgBoxNone(UIHandler.mTitleStr, UIHandler.mMsgStr, UIHandler.mMsgID, UIHandler.mMsgLevel);
					break;
					
				// 弹出只有一个按钮的消息框
				case UIHandler.MSGBOX_ONE:
					MsgBoxOne(UIHandler.mTitleStr, UIHandler.mMsgStr, UIHandler.mMsgID, UIHandler.mMsgLevel);
					break;
					
				// 弹出两个按钮的消息框
				case UIHandler.MSGBOX_NORMAL:
					MsgBox(UIHandler.mTitleStr, UIHandler.mMsgStr, UIHandler.mMsgID, UIHandler.mMsgLevel);
					break;
					
				// MsgBox(String titleStr, String msgStr, String okStr, String cancelStr, final int msgID, int iLevel) 
				// 弹出并且可以自定义确定取消按钮的消息框
				case UIHandler.MSGBOX_TWO:
					MsgBox(UIHandler.mTitleStr,UIHandler.mMsgStr,UIHandler.mMsgBtnName1,UIHandler.mMsgBtnName2,UIHandler.mMsgID,UIHandler.mMsgLevel);
					break;
					
				default:
					isActed = false;
					break;
			}

			// 如果上面的case没有响应到，则响应MsgOk方法
			if(!isActed)
			{
				//System.out.printf("### msgID = 0x%04x \n", msg.what);
				MsgOK(msg.what);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	// 重载的toast
	protected void Toast(final String msgStr, final int nType)
	{
		long curTime = System.currentTimeMillis();
		long timeSpace = Math.abs(curTime-mLastToastTime);
		
		if(timeSpace > 2000)
		{
			mMainActivity.runOnUiThread(new Runnable() 
			{
				@Override
				public void run() 
				{
					Toast(nType, msgStr);
				}
			});
			mLastToastTime = curTime;
		}
	}
	
	// 界面toast
	// nType: 	0-短时间内的toast, 1-长时间的toast
	// msgStr:	想要的toast的内容
	protected void Toast(int nType, String msgStr) 
	{
		LayoutInflater inflater = mMainActivity.getLayoutInflater();
		View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup)mMainActivity.findViewById(R.id.toast_layout_root));
		
		//ImageView image = (ImageView)layout.findViewById(R.id.iv_toast_image);
		//image.setImageResource(R.drawable.note);
		TextView text = (TextView)layout.findViewById(R.id.tv_toast_text);
		text.setText(msgStr);
		
		Toast toast = new Toast(mMainActivity.getApplicationContext());
		toast.setGravity(Gravity.BOTTOM, 0, 0);
		toast.setDuration(nType == 0 ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG);	
		toast.setView(layout);
		toast.show();
	}
	
	// 存在两个按钮的消息提示框
	public void MsgBox(String msgStr, final int msgID) 
	{
		MsgBox("提示信息", msgStr, msgID);
	}

	public void MsgBox(String titleStr, String msgStr, final int msgID) 
	{
		MsgBox(titleStr, msgStr, msgID, UIHandler.MSG_INFO);
	}

	public void MsgBox(String titleStr, String msgStr, final int msgID, int iLevel) 
	{		
		MsgBox(titleStr, msgStr, "确定", "取消", msgID, iLevel);
	}
	
	public void MsgBox(String titleStr, String msgStr, String okStr, String cancelStr, final int msgID, int iLevel) 
	{		
		// 如果存在进度条则取消进度条
		if (mProDialogShow == true) 
		{
			cancleProgress();
		}
		
		MessageBoxDialog dialog = new MessageBoxDialog(mMainActivity, R.style.transparent_dialog);
		dialog.setTitle(titleStr);
		dialog.setMessage(msgStr);
		dialog.setIcon(iLevel);
		dialog.setNegativeListener(cancelStr, new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				v.setEnabled(false);
				MsgCancle(msgID);				
			}
		});
		dialog.setPositiveListener(okStr, new View.OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				v.setEnabled(false);
				MsgOK(msgID);
			}
		});
		// 显示dialog
		dialog.show();
	}

	// 只有<确定>按钮的消息提示框
	public void MsgBoxOne(String msgStr, final int msgID) 
	{
		MsgBoxOne("提示信息", msgStr, msgID);
	}

	public void MsgBoxOne(String titleStr, String msgStr, final int msgID) 
	{
		MsgBoxOne(titleStr, msgStr, msgID, UIHandler.MSG_INFO);
	}
	
	public void MsgBoxOne(String titleStr, String msgStr, final int msgID, int iLevel)
	{
		// 如果存在进度条则取消进度条
		if (mProDialogShow == true) 
		{
			cancleProgress();
		}

		MessageBoxDialog dialog = new MessageBoxDialog(mMainActivity, R.style.transparent_dialog);
		dialog.setTitle(titleStr);
		dialog.setMessage(msgStr);
		dialog.setIcon(iLevel);
		dialog.setPositiveListener("确定", new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				v.setEnabled(false);
				MsgOK(msgID);
			}
		});
		dialog.show();
	}
	
	// 没有按钮显示，界面卡死
	public void MsgBoxNone(String titleStr, String msgStr, final int msgID, int iLevel)
	{
		// 如果存在进度条则取消进度条
		if (mProDialogShow == true) 
		{
			cancleProgress();
		}

		MessageBoxDialog dialog = new MessageBoxDialog(mMainActivity, R.style.transparent_dialog);
		dialog.setTitle(titleStr);
		dialog.setMessage(msgStr);
		dialog.setIcon(iLevel);
		dialog.show();
	}
	
	// 显示水平滚动条
	protected void showDeterminateProgressDialog(String titleStr, String msgStr) 
	{
		showProgressDialog(titleStr, msgStr, ProBar_MAX, false);
	}
	
	// 显示旋转滚动条
	protected void showInDeterminateProgressDialog(String titleStr, String msgStr) 
	{
		showProgressDialog(titleStr, msgStr, ProBar_MAX, true);
	}

	protected void showProgressDialog(String titleStr, String msgStr, int iMax, boolean indeterminate) 
	{
		mStrProTitle = titleStr;
		if(mProDialogShow)
		{
			// 重新开启滚动条的话,进度为0
			mProDialog.setProgress(0);
			// 设置ProgressDialog 的进度条是否不明确
			mProDialog.setIndeterminate(indeterminate);
			// 设置ProgressDialog 的最大值
			mProDialog.setMax(iMax); 
			// 设置ProgressDialog 标题
			mProDialog.setTitle(titleStr); 
			// 设置ProgressDialog 提示信息
			mProDialog.setMessage(msgStr); 
		}
		else 
		{
			// 创建ProgressDialog对象
			mProDialog = new ProgressDialog(mMainActivity, R.style.transparent_dialog);
			// 设置ProgressDialog 的进度条是否不明确
			mProDialog.setIndeterminate(indeterminate);
			// 设置ProgressDialog 是否可以按退回按键取消
			mProDialog.setCancelable(false); 
			// 设置ProgressDialog 的最大值
			mProDialog.setMax(iMax); 
			// 设置ProgressDialog 标题
			mProDialog.setTitle(titleStr); 
			// 设置ProgressDialog 提示信息
			mProDialog.setMessage(msgStr); 
			// 显示
			mProDialog.show();
			mProDialogShow = true;
		}
	}

	protected void setProgress(String msgStr, int nProgress) 
	{
		if(mProDialogShow) 
		{
			mProDialog.setMessage(msgStr);
			mProDialog.setProgress(nProgress);
		}
		/*
		else 
		{
			// 如果没开启，直接再开启
			showDeterminateProgressDialog(mStrProTitle, msgStr);
			mProDialog.setProgress(value);
		}
		*/
	}

	protected void cancleProgress() 
	{
		if (mProDialog != null)
		{
			mProDialog.cancel();
			mProDialog = null;
			System.out.println("UILogic: ProDialog delete...");
		}
		mProDialogShow = false;
	}

	// 用于判断是否为空的方法
	public static boolean isNUll(String str)
	{
		if(str == null || "null".equals(str) || "".equals(str))
		{
			return true;
		}
		return false;
	}
	
	// 显示日期对话框
	public void showDateDialog(final EditText eText, int type)
    {
    	if(ButtonUtil.isFastDoubleClick(eText.getId()))
    	{
    		return;
    	}
    	int year		=	0 ;
    	int monthOfYear	=	0 ;
    	int dayOfMonth	=	1 ;
    	mDateType 		= 	type;
    	String sInputDate = eText.getText().toString();
    	Log.e("input date", sInputDate);
    	
    	if("".equals(sInputDate))
    	{
    		 Calendar c = Calendar.getInstance();
    		 year = c.get(Calendar.YEAR);
    		 monthOfYear = c.get(Calendar.MONTH);
    		 dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
    	}
    	else
    	{
    		// 拆分输入框的数据
    		if(sInputDate.length() >= 4)
    		{
    			year = Integer.parseInt(sInputDate.substring(0,4));
    		}
    		if(sInputDate.length() >= 6)
    		{
    			monthOfYear = Integer.parseInt(sInputDate.substring(4,6)) - 1;
    		}
    		if(sInputDate.length()>=8)
    		{
    			dayOfMonth = Integer.parseInt(sInputDate.substring(6,8));
    		}
    		Log.e("date","year="+year+" month="+monthOfYear+" date="+dayOfMonth);
    	}
    	
    	DateTimePickerDialog date = DateTimePickerDialog.getYearMonthDateDialog(mMainActivity);
		if(mDateType == 1)
		{
			date.setDateVisible(false);
		}
    	
		date.setOnDateTimeSelectListener(new DateTimePickerDialog.OnDateTimeSelectListener()
		{	
			@Override
			public void onDateTimeSelected(Calendar c)
			{
    			if(mDateType == 1)
    			{
    				eText.setText(new StringBuilder().append(c.get(Calendar.YEAR)).append(
    		              (c.get(Calendar.MONTH) + 1) < 10 ? "0" + (c.get(Calendar.MONTH) + 1) : (c.get(Calendar.MONTH) + 1)));
    			}
    			else
    			{
    				eText.setText(new StringBuilder().append(c.get(Calendar.YEAR)).append(
      		              (c.get(Calendar.MONTH) + 1) < 10 ? "0" + (c.get(Calendar.MONTH) + 1) : (c.get(Calendar.MONTH) + 1)).append(
      		              (c.get(Calendar.DATE) < 10) ? "0" + c.get(Calendar.DATE) : c.get(Calendar.DATE)));
    			}
			}
		});	
    	date.show();	
    }	
	
	// 显示日期时间对话框
	public void showDateTimeDialog(final EditText eText, int type)
    {
    	if(ButtonUtil.isFastDoubleClick(eText.getId()))
    	{
    		return;
    	}
    	int year		=	0 ;
    	int monthOfYear	=	0 ;
    	int dayOfMonth	=	1 ;
    	
    	int hour = 0;
    	int minute = 0;
    	
    	mDateType 		= 	type;
    	String sInputDate = eText.getText().toString();
    	Log.e("input date", sInputDate);
    	
    	if("".equals(sInputDate))
    	{
    		 Calendar c = Calendar.getInstance();    		
    		 year = c.get(Calendar.YEAR);
    		 monthOfYear = c.get(Calendar.MONTH);
    		 dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
    		 
    		 hour= c.get(Calendar.HOUR_OF_DAY);
    		 minute = c.get(Calendar.MINUTE);
    	}
    	else
    	{
    		// 拆分输入框的数据
    		if(sInputDate.length() >= 4) 
    		{
    			year = Integer.parseInt(sInputDate.substring(0,4));
    		}
    		if(sInputDate.length() >= 6)
    		{
    			monthOfYear = Integer.parseInt(sInputDate.substring(4,6)) - 1;
    		}
    		if(sInputDate.length()>=8)
    		{
    			dayOfMonth = Integer.parseInt(sInputDate.substring(6,8));
    		}
    		if(sInputDate.length()>=10)
    		{
    			hour = Integer.parseInt(sInputDate.substring(8,10));
    		}
    		if(sInputDate.length()>=12)
    		{
    			minute = Integer.parseInt(sInputDate.substring(10,12));
    		}
    		Log.e("date","year="+year+" month="+monthOfYear+" date="+dayOfMonth+"hour="+hour+"minute"+minute);
    	}
    	
    	DateTimePickerDialog date = DateTimePickerDialog.getYearMonthDateDayDialog(mMainActivity);
    	
		if(mDateType == 1)
		{
			date.setDateVisible(false);
		}
    	
		date.setOnDateTimeSelectListener(new DateTimePickerDialog.OnDateTimeSelectListener()
		{	
			@Override
			public void onDateTimeSelected(Calendar c)
			{
			
    			if(mDateType == 1)
    			{
    				eText.setText(new StringBuilder().append(c.get(Calendar.YEAR)).append(
    		              (c.get(Calendar.MONTH) + 1) < 10 ? "0" + (c.get(Calendar.MONTH) + 1) : (c.get(Calendar.MONTH) + 1)));
    			}
    			else
    			{
    				eText.setText(new StringBuilder().append(c.get(Calendar.YEAR)).append(
      		              (c.get(Calendar.MONTH) + 1) < 10 ? "0" + (c.get(Calendar.MONTH) + 1) : (c.get(Calendar.MONTH) + 1)).append(
      		              (c.get(Calendar.DATE) < 10) ? "0" + c.get(Calendar.DATE) : c.get(Calendar.DATE)).append(
      		              (c.get(Calendar.HOUR_OF_DAY)<10)  ? "0"+(c.get(Calendar.HOUR_OF_DAY)) : (c.get(Calendar.HOUR_OF_DAY))).append(
      		            		(c.get(Calendar.MINUTE)	<10) ? "0"+(c.get(Calendar.MINUTE)):(c.get(Calendar.MINUTE)	)));
    			}
			}
		});
    	date.show();	
    }	
}
