package com.ai.cmccha.sc.idcard.bean;


public class ProductInfo
{
	public static final String	goTone	= "goTone";
	public static final String	mZone	= "mZone";
	public static final String	easyOwn	= "easyOwn";
	private String mID;
	private String mNote;
	private String mCode;
	private String mBrandID;
	private boolean mCanCel;
	private String mName;
	private String mErrMsg;
	private String mType;
	private String mDesc;
	// Ʒ������ 
	private String mBrandType;
	private boolean mIsSelected = false;
	
	public String getID()
	{
		return mID;
	}
	
	public void setID(String iD)
	{
		mID = iD;
	}

	public boolean isIsSelected()
	{
		return mIsSelected;
	}

	public void setIsSelected(boolean mIsSelected)
	{
		this.mIsSelected = mIsSelected;
	}

	public String getName()
	{
		return mName;
	}

	public void setName(String name)
	{
		mName = name;
	}

	public String getNote()
	{
		return mNote;
	}

	public void setNote(String mNote)
	{
		this.mNote = mNote;
	}

	public String getCode()
	{
		return mCode;
	}

	public void setCode(String mCode)
	{
		this.mCode = mCode;
	}

	public String getBrandID()
	{
		return mBrandID;
	}

	public void setBrandID(String mBrandID)
	{
		this.mBrandID = mBrandID;
	}

	public boolean isCanCel()
	{
		return mCanCel;
	}

	public void setCanCel(boolean mCanCel)
	{
		this.mCanCel = mCanCel;
	}

	public String getErrMsg()
	{
		return mErrMsg;
	}

	public void setErrMsg(String mErrMsg)
	{
		this.mErrMsg = mErrMsg;
	}

	public String getType()
	{
		return mType;
	}

	public void setType(String mType)
	{
		this.mType = mType;
	}

	public String getDesc()
	{
		return mDesc;
	}

	public void setDesc(String mDesc)
	{
		this.mDesc = mDesc;
	}

	public String getBrandType()
	{
		return mBrandType;
	}

	public void setBrandType(String mBrandType)
	{
		this.mBrandType = mBrandType;
	}

	@Override
	public String toString()
	{
		return "name:" + mName + " id:" + mID;
	}
	
}
