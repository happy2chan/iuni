package com.ai.cmccha.sc.idcard.bean;

public class IDCardInfo
{
	// 手机号码
	private String	mPhoneNum;
	// 客户姓名
	private String	mName;
	// 证件类型
	private String	mCustNum;
	// 性别
	private String	mSex;
	// 名族
	private String	mNation;
	// 出生日期
	private String	mBirthDay;
	// 证件地址
	private String	mAddress;
	// 证件有效期
	private String	mExpdate;
	// 签发机关
	private String	mAuthority;
	// 身份证正面照片
	private byte[]	mPicZ;
	// 身份证反面照片
	private byte[]	mPicF;

	public String getmPhoneNum()
	{
		return mPhoneNum;
	}

	public void setmPhoneNum(String mPhoneNum)
	{
		this.mPhoneNum = mPhoneNum;
	}

	public String getmName()
	{
		return mName;
	}

	public void setmName(String mName)
	{
		this.mName = mName;
	}

	public String getmCustNum()
	{
		return mCustNum;
	}

	public void setmCustNum(String mCustNum)
	{
		this.mCustNum = mCustNum;
	}

	public String getmSex()
	{
		return mSex;
	}

	public void setmSex(String mSex)
	{
		this.mSex = mSex;
	}

	public String getmNation()
	{
		return mNation;
	}

	public void setmNation(String mNation)
	{
		this.mNation = mNation;
	}

	public String getmBirthDay()
	{
		return mBirthDay;
	}

	public void setmBirthDay(String mBirthDay)
	{
		this.mBirthDay = mBirthDay;
	}

	public String getmAddress()
	{
		return mAddress;
	}

	public void setmAddress(String mAddress)
	{
		this.mAddress = mAddress;
	}

	public String getmExpdate()
	{
		return mExpdate;
	}

	public void setmExpdate(String mExpdate)
	{
		this.mExpdate = mExpdate;
	}

	public String getmAuthority()
	{
		return mAuthority;
	}

	public void setmAuthority(String mAuthority)
	{
		this.mAuthority = mAuthority;
	}

	public byte[] getmPicZ()
	{
		return mPicZ;
	}

	public void setmPicZ(byte[] mPicZ)
	{
		this.mPicZ = mPicZ;
	}

	public byte[] getmPicF()
	{
		return mPicF;
	}

	public void setmPicF(byte[] mPicF)
	{
		this.mPicF = mPicF;
	}
	
}
