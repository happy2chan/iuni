package com.ai.cmccha.sc.idcard.pack;

import android.util.Log;

import com.ai.cmccha.sc.idcard.ajni.IDevice;
import com.ai.cmccha.sc.idcard.bean.Buffer;
import com.ai.cmccha.sc.idcard.bean.PSAM_Data;
import com.ai.cmccha.sc.idcard.util.Commen;
import com.ai.cmccha.sc.idcard.util.DesUtil;

public class PacketManager
{
	//private static byte[] mDesKey = "yife0003".getBytes();
	private static byte 	biaoshi = (byte) 0x00;
	
	public static byte getBiaoshi() 
	{
		return biaoshi++;
	}
	
	/*
	// DES加密,用于怡丰报文体加密
	private static byte[] DesEncrypt(byte[] data, byte[] key) 
	{
		// 判断密钥长度
		if((key.length%8) != 0) 
		{
			Log.v("", "DES密钥长度错误!!!");
			return null;
		}
		// 填充,不足8个字节的部分补0
		int templen = data.length-19;
		int needlen = ((templen-1)/8+1)*8;
		//Log.v("加密", "实际长度: " + templen + ", 加密后长度: " + needlen);
		byte[] temp = new byte[needlen];
		System.arraycopy(data, 19, temp, 0, templen);
		temp = DesUtil.desEncrypt(key, temp);
		// 拷贝回原缓冲
		byte[] dest = new byte[needlen+19];
		System.arraycopy(data, 0, dest, 0, 19);
		System.arraycopy(temp, 0, dest, 19, needlen);
		//Commen.printhax(dest, dest.length, "加密后密文数据: ", 'e');

		temp = null;
		return dest;
	}
	*/
	
	// 数据包打包
	public static byte[] pack(InData indata, boolean isMockEquipment, IDevice device, boolean isSoftEncrypt, byte[] loginKeypwd) 
	{
		Buffer buffer = new Buffer();
		buffer.add("06");  							// 1B 通讯模式：以太网方式传输 0x06	Ethernet
		buffer.add("20");  							// 1B 协议版本, 现在一体机和安卓平板都改为20
		buffer.add("02");  							// 1B 厂家标识：0x02	福建三元达软件有限公司
		
		byte[] psamid = Commen.hexstr2byte(PSAM_Data.mPsamInfo.getPsamID());
		buffer.add(psamid);							// 8B psam卡号：8位（16）   要通过psaminfo.psamid获得

													// 低4位	数据压缩算法	0: 未压缩 1: Huffman算法 2~7: 未定义。
													// 高4位	数据加密算法	0: 未加密 1: DES加密 2: 3DES加密 3~7: 未定义，保留
		buffer.add("00");	 						// 1B 数据状态码
		buffer.add("0000");							// 2B 安全数据码：系统分配给各厂家随机密钥随机加密的生成值。未分配的默认为0x0000		
		buffer.add(getBiaoshi());

		buffer.add("01"); 							// 1B 通信包总数
		buffer.add("01");							// 1B 通信包序号
		
		buffer.add(indata.getServiceCommandID());  	// 2B 平台通信包标识(平台自动机标识)  每个新业务发启时 终端以0xFFFF
		
		buffer.add("00");	 						// 1B 开机随机验证
		buffer.add(indata.getSubid()); 				// 1B 子业务
		buffer.add(indata.getId());					// 1B 业务
		buffer.add("FF");  							// 1B 应答标识

		// 命令体：数据长度+子命令标识+命令内容+数据长度+子命令标识+命令内容
		buffer.add(indata.getPdu().toString());
		byte[] data = buffer.getbytes();
		Commen.printhax(data, data.length, "send_明文", 'e');
		
		//Log.d("", "开始加密...");
		CommCenter.Encrypt(data); 								// 发送的消息加密
		//Log.d("", "开始计算鉴权码CRC...");
		byte[] crc = new byte[2];
		CommCenter.CalcCrc16(data, (short)0, crc);
		buffer.clear();
		buffer.add(data);
		buffer.add(crc);
		
		// 生成 鉴权码
		int z = 0;
		for(z=0; z<4; z++) 
		{
			psamid[z] ^= crc[0];
		}
		for(z=4; z<8; z++) 
		{
			psamid[z] ^= crc[1];
		}
		
		if(isMockEquipment) 
		{
			System.arraycopy(DesUtil.desEncrypt(PSAM_Data.mExtKey, psamid), 0, psamid, 0, 8);  		// 软件加密
		}
		else 
		{
			if(isSoftEncrypt) 
			{
				System.arraycopy(DesUtil.desEncrypt(PSAM_Data.mExtKey, psamid), 0, psamid, 0, 8);  	// 软件加密
			} 
			else 
			{
				if(loginKeypwd == null) 
				{
					Log.d("", "使用PSAM卡加密鉴权码...");
					//Commen.printhax(psamid, psamid.length, "psamid_明文", 'e');
					if(!device.psamEncrypt(psamid, (byte)0x08, (byte)0x00)) 
					{
						Log.e("", "鉴权码加密失败..");
						return null;
					}
					//Commen.printhax(psamid, psamid.length, "psamid_密文", 'e');
				}
				else 
				{
					Log.d("", "使用签到密钥进行加密鉴权码！");
					System.arraycopy(DesUtil.desEncrypt(loginKeypwd, psamid), 0, psamid,0, 8);
				}
			}
		}
		// PSAM鉴权码
		buffer.add(psamid);
		return CommCenter.transferredMeaning(buffer.getbytes());
	}
	
	/*
	// DES解密
	private static byte[] DesDecrypt(byte[] data, byte[] key) 
	{
		int templen = data.length-19;
		byte[] temp = new byte[templen];
		System.arraycopy(data, 19, temp, 0, templen);
		temp = DesUtil.desDecrypt(mDesKey, temp);
		//Commen.printhax(temp, temp.length, "解密后密文数据: ", 'e');
		// 拷贝回原缓冲区
		System.arraycopy(temp, 0, data, 19, templen);
		return data;
	}
	*/
	
	public static OutData Unpack(byte[] out) 
	{
		byte dest[] = CommCenter.reverseTransferredMeaning(out);
		byte temp[] = new byte[dest.length-4];
		System.arraycopy(dest, 1, temp, 0, dest.length-4);
		
		byte[] recv = CommCenter.Decrypt(temp);
		Commen.printhax(recv, recv.length, "rec_明文", 'e');
		
		OutData pdata = new OutData();
		pdata.setServiceCommandID(Commen.hex2str(recv, 17, 2));
		pdata.setCheckByte(recv[19]);
		pdata.setId(Commen.hex2str(recv, 21, 1));
		pdata.setSubid(Commen.hex2str(recv, 20, 1));
		
		if (recv[22] == (byte)0xFF) 
		{
			// 2 为平台主动下推包
			pdata.setType(2);
		} 
		else 
		{
			// 1 为响应包
			pdata.setType(1);
		}
		// 数据域内容(19+4)
		byte[] pduData = new byte[recv.length-23];
		System.arraycopy(recv, 23, pduData, 0, recv.length-23);
		
		Pdu pdu = new Pdu();
		if(!pdu.parse(pduData)) 
		{
			Log.e("pdu校验", "接收数据校验：pdu数据单元有误！");
		}
		else 
		{
			pdata.setPdu(pdu);
		}
		return pdata;
	}
	
	public static OutData Unpack_2(byte[] out) 
	{
		byte dest[] = CommCenter.reverseTransferredMeaning(out);
		byte temp[] = new byte[dest.length-4];
		System.arraycopy(dest, 1, temp, 0, dest.length-4);
		byte[] recv = CommCenter.Decrypt(temp);
		
		OutData pdata = new OutData();
		pdata.setId(Commen.hex2str(recv, 21, 1));
		pdata.setSubid(Commen.hex2str(recv, 20, 1));
		return pdata;
	}
}