package com.ai.cmccha.sc.idcard.bean;

import java.util.ArrayList;

import com.ai.cmccha.sc.idcard.util.Commen;

public class Buffer 
{
	private ArrayList<Byte> list = new ArrayList<Byte>(1024);

	public void add(byte b) 
	{
		list.add(b);
	}

	public void add(int idata) 
	{
		this.add(Commen.int2bytes(idata));
	}

	public void add(short idata)
	{
		this.add(Commen.short2bytes(idata));
	}

	public void add(float idata) 
	{
		int t = (int)idata;
		this.add(Commen.int2bytes(t));
	}

	public void add(byte[] b) 
	{
		int i=0;
		int len = b.length;
		while(i < len){
			list.add(b[i]);
			i++;
		}
	}
	
	public void add(byte[] srcbytes, int nStart, int nLen) 
	{
		for(int i=nStart; i<(nStart+nLen); i++) 
		{
			list.add(srcbytes[i]);
		}
	}

	public void add(String str) 
	{
		add(Commen.hexstr2byte(str));
	}

	public void clear() 
	{
		list.clear();
	}

	public byte[] getbytes() 
	{
		int len = list.size();
		byte[] res = new byte[len];

		for(int i=0; i<len; i++){
			res[i] = list.get(i);
		}
		return res;
	}

	public static void main(String[] args) 
	{
		Buffer buffer = new Buffer();
		buffer.add("01");
		
		byte[] b = buffer.getbytes();
		System.out.println(b);
	} 
}
