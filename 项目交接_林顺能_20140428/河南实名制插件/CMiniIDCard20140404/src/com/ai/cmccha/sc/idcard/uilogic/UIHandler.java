package com.ai.cmccha.sc.idcard.uilogic;

import com.ai.cmccha.sc.idcard.R;

import android.os.Handler;
import android.os.Message;

public class UIHandler 
{
	public static final boolean				IS_DEBUG						= false;
	public static boolean					IS_BACK_TRAIL					= false;				// 是否补录
	public static Handler 					mHandler 						= null;					// 界面Handler
	public static String					mTitleStr						= null;					// 消息框标题
	public static String					mMsgStr							= null;					// 消息框内容
	public static int						mMsgID							= 0;					// 消息框消息ID
	public static int						mMsgLevel						= 0;					// 消息框样式(成功提示框、错误框、信息框)
	
	public final static int 				MSG_INFO 						= R.drawable.msg_icon;
	public final static int 				MSG_WARN 						= R.drawable.icon_warn;
	public final static int 				MSG_CONFIRM 					= R.drawable.icon_confirm;
	public final static int 				MSG_ERROR						= R.drawable.icon_error;
	public final static int 				MSG_SUCCESS						= R.drawable.icon_success;
	
	public static String					mMsgBtnName1					= null;					// 自定义按钮相关样式
	public static String					mMsgBtnName2					= null;
	public static int						mMsgID1							= 0;
	public static int						mMsgID2							= 0;
	
	public static String					mToastStr						= null;					// toast的消息内容

	public static String					mProTitleStr					= null;					// 进度条标题
	public static String					mProMsgStr						= null;					// 进度条消息
	public static int						mProMaxValue					= 0;					// 进度条的最大值
	public static int						mProValue						= 0;					// 进度条当前进度值
	public static final int					PRO_MAX_VALUE					= 100;

	public static final int					MSG_TOAST						= 0xF101;				// Toast消息
	public static final int					MSG_CUSTOM						= 0xF102;				// 回调消息
	public static final int					MSG_PRO_START					= 0xF103;				// 启动水平进度条
	public static final int					MSG_PRO_STEP					= 0xF104;				// 设置水平进度条
	public static final int					MSG_PRO_STOP					= 0xF105;				// 停止水平进度条
	public static final int					MSG_PRO_START_SPINNER			= 0xF106;				// 启动旋转进度条
	
	public static final int 				MSGBOX_TWO						= 0xF10B;               // 弹出确定按钮并且可以自动定义功能
	public static final int					MSGBOX_NONE						= 0xF107;				// 弹出消息提示框<无按钮样式>
	public static final int					MSGBOX_ONE						= 0xF108;				// 弹出消息提示框<确定按钮>
	public static final int					MSGBOX_NORMAL					= 0xF109;				// 弹出消息提示框<确定和取消按钮>
	public static final int					MSGBOX_CUSTOM					= 0xF10A;				// 弹出消息提示框<自定义样式>
	
	// 主界面消息
	public static final int					MSG_CONNECT_BLUE_DONE			= 0x0001;				// 连接蓝牙完成
	public static final int					MSG_SYSTEM_INIT_DONE			= 0x0002;				// 系统初始化完成
	
	public static final int					MSG_COMMIT						= 0x0003;				// 提交信息
	public static final int					MSG_PRO_SELECTED				= 0x0004;				// 提交信息
	public static final int					MSG_GET_PROLIST_SUC				= 0x0005;				// 获取套餐列表成功
	public static final int					MSG_COMMIT_SUC					= 0x0006;				// 提交信息
	
	// 发送消息 
	public static void sendMessage(int msgID)
	{
		Message msg = mHandler.obtainMessage();
		msg.what = msgID;  
		mHandler.sendMessage(msg);
	}

	public static void sendMessage(int msgID, String msgStr)
	{
		mToastStr = msgStr;
		sendMessage(msgID);
	}
	
	// 开启水平进度条
	public static void initBusiStart(String titleStr) 
	{
		initBusiStart(titleStr, PRO_MAX_VALUE, "开始进行业务办理");
	}

	public static void initBusiStart(String titleStr, int iMaxBar) 
	{
		initBusiStart(titleStr, iMaxBar, "开始进行业务办理");
	}
	
	public static void initBusiStart(String titleStr, int iMaxBar, String msgStr) 
	{
		mProTitleStr 	= titleStr;
		mProMsgStr 		= msgStr;
		mProMaxValue	= iMaxBar;
		
		Message message = mHandler.obtainMessage();
		message.what = MSG_PRO_START;
		mHandler.sendMessage(message);
	}

	// 设置水平进度条
	public static void initBusiStep(String titleStr, String msgStr, int nValue)
	{
		mProTitleStr 	= titleStr;
		mProMsgStr 		= msgStr;
		mProValue		= nValue;

		Message message = mHandler.obtainMessage();
		message.what = MSG_PRO_STEP;
		mHandler.sendMessage(message);
	}

	public static void initBusiStep(String titleStr, String msgStr, String strValue) 
	{
		initBusiStep(titleStr, msgStr, Integer.parseInt(strValue));
	}
	
	public static void initBusiStep(String msgStr, int nValue)
	{
		initBusiStep(mProTitleStr, msgStr, nValue);
	}
	
	// 停止进度条
	public static void initBusiStop()
	{
		Message message = mHandler.obtainMessage();
		message.what = MSG_PRO_STOP;
		mHandler.sendMessage(message);
	}
	
	// 开启旋转进度条
	public static void initProgressStart(String titleStr, String msgStr) 
	{
		mProTitleStr = titleStr;
		mProMsgStr = msgStr;
		
		Message message = mHandler.obtainMessage();
		message.what = MSG_PRO_START_SPINNER;
		mHandler.sendMessage(message);
	}
	
	// 关闭旋转进度条
	public static void InitProgressStop() 
	{
		initBusiStop();
	}

	// 没有按钮
	public static void MsgBoxNone(String msgStr)
	{
		mTitleStr 		= "提示信息";
		mMsgStr 		= msgStr;
		mMsgID 			= 0;
		mMsgLevel		= MSG_INFO;
		
		Message message = mHandler.obtainMessage();
		message.what = MSGBOX_NONE;
		mHandler.sendMessage(message);
	}
	
	// 开启<确定>按钮的消息提示框
	public static void MsgBoxOne(String msgStr) 
	{
		MsgBoxOne("提示信息", msgStr, 0, MSG_INFO);
	}

	public static void MsgBoxOne(String titleStr, String msgStr)
	{
		MsgBoxOne(titleStr, msgStr, 0);
	}
	
	public static void MsgBoxOne(String titleStr, String msgStr, int nMsgID)
	{
		MsgBoxOne(titleStr, msgStr, nMsgID, MSG_INFO);
	}
	
	public static void MsgBoxOne(String titleStr, String msgStr, int nMsgID, int iMsgLevel) 
	{
		mTitleStr 		= titleStr;
		mMsgStr 		= msgStr;
		mMsgID 			= nMsgID;
		mMsgLevel		= iMsgLevel;
		
		mHandler.sendEmptyMessage(MSGBOX_ONE);
	}
	
	// 开启消息提示框(两个按钮, 确定和取消)
	public static void MsgBox(String msgStr, int nMsgID) 
	{
		MsgBox("提示信息", msgStr, nMsgID);
	}
	
	public static void MsgBox(String titleStr, String msgStr, int nMsgID) 
	{
		MsgBox(titleStr, msgStr, nMsgID, MSG_INFO);
	}
	
	public static void MsgBox(String titleStr, String msgStr, int nMsgID, int iMsgLevel) 
	{
		mTitleStr 		= titleStr;
		mMsgStr 		= msgStr;
		mMsgID 			= nMsgID;
		mMsgLevel		= iMsgLevel;
		
		mHandler.sendEmptyMessage(MSGBOX_NORMAL);
	}
	
	// 自定义消息框
	public static void MsgBox(String msgStr, String strBtn1, int btnID1, String strBtn2, int btnID2) 
	{
		mMsgStr 		= msgStr;
		mMsgBtnName1	= strBtn1;
		mMsgBtnName1	= strBtn2;
		mMsgID1			= btnID1;
		mMsgID2			= btnID2;
		
		mHandler.sendEmptyMessage(MSGBOX_CUSTOM);
	}
	
	// 自定义确定取消按钮
	public static void MsgBox(String titleStr, String msgStr, String btnSt1,String btnSt2,int nMsgID,int iMsgLevel)
	{
		mTitleStr 	  = titleStr;
		mMsgStr 	  = msgStr;
		mMsgBtnName1  = btnSt1;
		mMsgBtnName2  = btnSt2;
		mMsgID 		  = nMsgID;
		mMsgLevel	  = iMsgLevel;
		
		mHandler.sendEmptyMessage(MSGBOX_TWO);
	}
	
	// 发送字节数据
	public static void sendMessageByte(int what, Byte[] bytes)
	{
		mHandler.obtainMessage(what, bytes).sendToTarget();
	}
}