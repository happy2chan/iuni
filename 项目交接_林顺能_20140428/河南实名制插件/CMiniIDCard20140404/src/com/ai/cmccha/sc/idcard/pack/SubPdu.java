package com.ai.cmccha.sc.idcard.pack;

import java.util.ArrayList;
  
public class SubPdu
{
	// pdu的父类
	// 各字段
	protected ArrayList<String> mStrField = new ArrayList<String>();
	// 如果有子单元
	protected ArrayList<SubPdu> mPduField = new ArrayList<SubPdu>();
	// 分隔符
	protected final static String SEPARATOR_LEVEL_0 = "|";
	protected final static String SEPARATOR_LEVEL_1 = "~";
	protected final static String SEPARATOR_LEVEL_2 = "^";
	// 字段间分隔符
	protected String mSeparator = SEPARATOR_LEVEL_0;
	
	// 0->"|", 1->"~", 2->"^"
	protected void setSeparator(int level)
	{
		if (level == 0) 
		{
			mSeparator = SEPARATOR_LEVEL_0;
		}
		else if (level == 1) 
		{
			mSeparator = SEPARATOR_LEVEL_1;
		}
		else 
		{
			mSeparator = SEPARATOR_LEVEL_2;
		}
	}
	
	// 增加字段
	public void add(String value)
	{
		mStrField.add(value);
	} 
	
	// 加入子单元
	public void add(SubPdu pdu)
	{   
		mPduField.add(pdu);
	}
	
	/*
	需要实现深度copy
	// 清空
	public void clear()
	{
		mStrField.clear();
		mPduField.clear();
	}
	*/
	
	// 获取某个字段
	public String getStrField(int field_id)
	{
		if (mStrField.size() > field_id) 
		{
			return mStrField.get(field_id);
		}
		return "";
	}
	// 获取字段长度
	public int  getStrFieldSize()
	{
		return mStrField.size();
	}
	// 获取Pdu长度
	public int  getPduFieldSize()
	{
		return mPduField.size();
	}
	
	// 获取某个字段
	public SubPdu getPduField(int field_id)
	{
		if (mPduField.size() > field_id) 
		{
			return mPduField.get(field_id);
		}
		return null;
	}
	
	// 打包
	public byte[] pack(int level)
	{
		Buffer buffer = new Buffer();
		
		// 设置分隔符
		setSeparator(level);
		
		// 先打包子单元
		int size = mPduField.size();
		for (int i = 0; i < size; i++) 
		{
			// System.out.println(mPduField.get(i).pack(level + 1));
			buffer.add(mPduField.get(i).pack(level + 1));
			
			// 不是最后一个，加上分隔符
			if (i != (size - 1)) 
			{
				buffer.add(mSeparator);
			}
		}
		
		// 打包子字段
		size = mStrField.size();
		for (int i = 0; i < size; i++) 
		{
			buffer.add(mStrField.get(i));
			
			// 不是最后一个，加上分隔符
			if (i != (size - 1)) 
			{
				buffer.add(mSeparator);
			}
		}
		
		return buffer.getbytes();
	}
	
	// 解析
	// 有子单元就返回1
	public int parse(String pdu, int level)
	{
		// 子单元
		SubPdu subPdu = null;
		String[] strParse = null;
		// 是否成功
		int result = 0;
		
		try 
		{ 
			mStrField.clear();
			mPduField.clear();
			
			// 设置等级
			setSeparator(level);
			
			// 解析
			strParse = pdu.split("\\" + mSeparator);
			
			if (strParse.length == 1) 
			{
				// 没有子字段
				add(strParse[0]);
				return result;
			}
			
			for (int i = 0; i < strParse.length; i++) 
			{
				// 增加
				subPdu = new SubPdu();
				// 再次解析
				if(subPdu.parse(strParse[i], level+1) == 1)
				{
					// 加入队列
					add(subPdu);
				}
				else 
				{
					// 没法解析
					add(strParse[i]);
				}
				subPdu = null;
				result = 1;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return result;
	}
}
