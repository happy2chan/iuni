package com.ai.cmccha.sc.idcard.adpater;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;

public class SpecialTitleAdapter extends SimpleAdapter  
{
	private int[] colors = new int[] { 0x300000FF, 0x30FF0000 };

	public SpecialTitleAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) 
	{
		super(context, data, resource, from, to);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		View view = super.getView(position, convertView, parent);
		int colorPos = position % colors.length;
		if (colorPos == 1) {
			view.setBackgroundColor(Color.argb(250, 224, 243, 250));
		} else {
			view.setBackgroundColor(Color.argb(250, 255, 255, 255));
		}
		return view;
	}
}
