package com.ai.cmccha.sc.idcard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.ai.cmccha.sc.idcard.uilogic.UIHandler;
import com.ai.cmccha.sc.idcard.uilogic.UILogic;
import com.ai.cmccha.sc.idcard.util.ButtonUtil;
import com.ai.cmccha.sc.idcard.util.DimensionUtil;
import com.ai.cmccha.sc.idcard.util.HttpClientUtil;
import com.ai.cmccha.sc.idcard.util.Log;
import com.ai.cmccha.sc.idcard.util.NetworkUtil;
import com.ai.cmccha.sc.idcard.util.SharedUtil;

// 主界面
public class MainMenuLayout extends UILogic
{
	private static final String			TAG					= "GetInfo";
	private final int					MSG_GET_INFO_SUC	= 0xF001;
	private final String				KEY_NAME			= "name";
	private final String				KEY_PHONE			= "phone";
	private final String				KEY_TIME			= "time";
	private final String				KEY_IDCARD			= "idcard";
	private ListView					mListView;
	private List<Map<String, String>>	mListInfo			= new ArrayList<Map<String, String>>();

	public MainMenuLayout(MainActivity activity)
	{
		super(activity);
	}

	@Override
	public void go()
	{
		// 设置布局
		mMainActivity.setContentView(R.layout.lay_mainmenu);
		// 初始化控件
		initViews();
		// 获取未开户的采集信息列表
		getInfoList();
	}

	// 初始化控件
	@Override
	public void initViews()
	{
		mListView = (ListView) mMainActivity.findViewById(R.id.list_info);
		// 注销到登录界面
		mMainActivity.findViewById(R.id.btn_back).setOnClickListener(this);
		// 信息采集
		mMainActivity.findViewById(R.id.btn_pickup_info).setOnClickListener(this);
		// 信息补录
		mMainActivity.findViewById(R.id.btn_back_pick_info).setOnClickListener(this);
	}

	// 获取未开户的采集信息列表
	private void getInfoList()
	{
		if (!NetworkUtil.isConnect(mMainActivity))
		{
			UIHandler.MsgBoxOne("获取列表失败，请检查当前的网络连接状态！");
			return;
		}
		UIHandler.initProgressStart("证件信息获取", "正在获取信息列表...");
		new Thread()
		{
			public void run()
			{
				try
				{
					mListInfo.clear();
					Map<CharSequence, CharSequence> params = new HashMap<CharSequence, CharSequence>();
					// 加密
					params.put("MS_VERSION", "MS");
					String ms_tel = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.ms_tel, "");
					String bossCode = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.bossCode, "");
					String optrid = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.optrid, "");
					String scTel = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.sc_tel, "");
					//String cgxjjgl = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.cgxjjgl, "");
					//String SvcNum = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.SvcNum, "");
					Log.e(TAG, "读取msTel:" + ms_tel);
					Log.e(TAG, "读取bossCode:" + bossCode);
					Log.e(TAG, "读取optrid:" + optrid);
					Log.e(TAG, "读取scTel:" + scTel);
					//Log.e(TAG, "读取cgxjjgl:" + cgxjjgl);
					//Log.e(TAG, "读取SvcNum:" + SvcNum);
					params.put("msTel", ms_tel);
					params.put("bossCode", bossCode);
					params.put("optrid", optrid);
					params.put("scTel", scTel);
					//params.put("cgxjjgl", cgxjjgl);
					//params.put("SvcNum", SvcNum);
					String url = SharedUtil.getString(App.ShARED_FILE_NAME, mMainActivity, App.URL1, "");
					Log.e(TAG, "读取url1:" + url);
					String response = HttpClientUtil.post(mMainActivity, url, 5000, params);
					Log.e(TAG, "获取信息列表返回数据 :" + response);
					UIHandler.initBusiStop();
					// 开始解析JSON数据
					JSONObject res = new JSONObject(response);
					String returnCode = res.getString("returnCode");
					//String returnMsg = res.getString("returnMessage");
					if (returnCode.equals("0000"))
					{
						Log.e(TAG, "获取列表信息成功...");
						// 获取列表成功
						JSONArray infos = res.getJSONArray("infos");
						Log.e(TAG, "信息个数：" + infos.length());
						for (int i = 0; i < infos.length(); i++)
						{
							// {"infos":[{"BIRTHDAY":"2012-12-09","DONE_DATE":"2014-02-25 17:41:50",
							// "NOTES":"","NATION":"汉","ISSUING_AUTHORITY":"海南省巩义市回郭镇小訾殿村政通街１０号",
							// "ORG_ID":"100001","CREATE_DATE":"2014-02-25 17:41:50","CUST_CERT_TYPE":"1","EXT1":"",
							// "CUST_NAME":"闫建强1","OP_ID":"100005788302","EXPIRE_DATE":"2014-02-26 19:42:11",
							// "REGION_ID":"A","CUST_CERT_NO":"410181198308276916",
							// "CUST_CERT_ADDR":"海南省巩义市回郭镇小訾殿村政通街１０号",
							// "CERT_EXPDATE":"2017-12-09","GENDER":"1","EXT2":"",
							// "BILL_ID":"15238358312","VALID_DATE":"2014-02-25 17:42:11"}],
							// "returnCode":"0000","returnMessage":"调用成功"}
							JSONObject modelJson = infos.getJSONObject(i);
							String name = modelJson.getString("CUST_NAME");
							String phone = modelJson.getString("BILL_ID");
							String time = modelJson.getString("CREATE_DATE");
							String idcard = modelJson.getString("CUST_CERT_NO");
							Map<String, String> map = new HashMap<String, String>();
							map.put(KEY_NAME, name);
							map.put(KEY_PHONE, phone);
							map.put(KEY_TIME, time);
							map.put(KEY_IDCARD, idcard);
							mListInfo.add(map);
						}
						UIHandler.sendMessage(MSG_GET_INFO_SUC);
					}
					else
					{
						Log.e(TAG, "获取列表信息失败..");
						// 获取列表失败
						UIHandler.MsgBoxOne("获取列表失败， 没有查询到您收集的还未开户的实名制信息！");
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					Log.e("MainMenuLayout", e.toString());
					UIHandler.MsgBoxOne("获取列表失败，失败原因：\n未知异常！");
				}
				/*
				 for (int i = 0; i < 10; i++) { Map<String, String> map = new
				 HashMap<String, String>(); map.put(KEY_NAME, "张三");
				 map.put(KEY_PHONE, "186088888888"); map.put(KEY_TIME,
				 "2014-01-22 15:30:30"); map.put(KEY_IDCARD,
				 "123456789012345678"); mListInfo.add(map); } // 获取成功
				 UIHandler.sendMessage(MSG_GET_INFO_SUC);
				 */
			}
		}.start();
	}

	@Override
	public void onClick(final View v)
	{
		if(ButtonUtil.isFastDoubleClick(v.getId()))
		{
			return;
		}
		switch (v.getId())
		{
			// 注销
			case R.id.btn_back:
				// mMainActivity.goto_Layout(R.layout.lay_login);
				mMainActivity.finish();
				return;
				
			// 信息采集
			case R.id.btn_pickup_info:
				UIHandler.IS_BACK_TRAIL = false;
				mMainActivity.goto_Layout(BusiPickUpInfo.class);
				break;
				
			// 信息补录
			case R.id.btn_back_pick_info:
				UIHandler.IS_BACK_TRAIL = true;
				mMainActivity.goto_Layout(BusiPickUpInfo.class);
				break;
			default:
				break;
		}
	}

	@Override
	public void MsgOK(int msgID)
	{
		switch (msgID)
		{
		// 获取信息列表成功
			case MSG_GET_INFO_SUC:
				procGetInfoOk();
				break;
			default:
				super.MsgOK(msgID);
				break;
		}
	}

	// 获取采集信息列表成功
	private void procGetInfoOk()
	{
		SimpleAdapter adapter = new IdcardInfoAdapter(mMainActivity, mListInfo, R.layout.lay_listitem, new String[]
		{ KEY_NAME, KEY_PHONE, KEY_TIME, KEY_IDCARD }, new int[]
		{ R.id.tv_name, R.id.tv_phone, R.id.tv_time, R.id.tv_idcard_no });
		mListView.setAdapter(adapter);
		DimensionUtil.setListViewHeightBasedOnChildren(mListView);
	}

	private class IdcardInfoAdapter extends SimpleAdapter
	{
		public IdcardInfoAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from,
				int[] to)
		{
			super(context, data, resource, from, to);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = super.getView(position, convertView, parent);
			}
			Map<String, String> map = mListInfo.get(position);
			((TextView) convertView.findViewById(R.id.tv_name)).setText(map.get(KEY_NAME));
			((TextView) convertView.findViewById(R.id.tv_phone)).setText(map.get(KEY_PHONE));
			((TextView) convertView.findViewById(R.id.tv_time)).setText(map.get(KEY_TIME));
			((TextView) convertView.findViewById(R.id.tv_idcard_no)).setText(map.get(KEY_IDCARD));
			return convertView;
		}
	}

	// 调转到主界面
	public void jumpToMain()
	{
		// 注册监听事件
		mMainActivity.goto_Layout(R.layout.main);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			// mMainActivity.goto_Layout(MainActivity.class);
			mMainActivity.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
