package com.ai.cmccha.sc.idcard.util;

import java.util.HashMap;
import java.util.Map;

import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ButtonGroupUtil 
{
	private Map<Object, String> 		mDataList 				= null;
	private int[] 						mBtnIDs;
	private int 						mBackground;
	private int 						mBackgroundPressed;
	private ButtonGroupClickListener 	mClickListener;
	
	private int 						mCurBtnID;
	private ViewGetter 					mViewGetter;
	private int[][]                     mFontColor;

	private ButtonGroupUtil(ViewGetter viewGetter, int[] btn_ids, String[] values, int background, int background_pressed) throws Exception 
	{
		if (btn_ids == null || values == null || btn_ids.length == 0 || values.length == 0) 
		{
			throw new Exception("按钮数组或值数组不能为空");
		} 
		else if (btn_ids.length != values.length) 
		{
			throw new Exception("按钮数组与值数组的长度必须相同");
		} 
		else 
		{
			mDataList = new HashMap<Object, String>();
			mCurBtnID = 0;
			for (int i = 0; i < btn_ids.length; i++) 
			{
				final int btn_id = btn_ids[i];
				View view = viewGetter.findViewById(btn_id);
				if (view == null || !(view instanceof Button)) 
				{
					throw new Exception("按钮无法找到");
				}
				((Button)view).setOnClickListener(new OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						int temp = mCurBtnID;
						// 设置按钮选中状态
						if (v.getId() != mCurBtnID) 
						{
							checkButtonStatus(btn_id);
						}
						// 触发按钮的监听事件
						if(mClickListener != null) 
						{
							mClickListener.onClick(temp, v.getId());
						}
					}
				});
				mDataList.put(btn_id, values[i]);
			}
			this.mBtnIDs 			= btn_ids;
			this.mBackground 		= background;
			this.mBackgroundPressed = background_pressed;
			this.mViewGetter 		= viewGetter;
		}
	}
	
	private ButtonGroupUtil(ViewGetter viewGetter, int[] btn_ids, String[] values, String[] texts, int background, int background_pressed) throws Exception 
	{
		if (btn_ids == null || values == null || btn_ids.length == 0 || values.length == 0 || texts == null || texts.length == 0) 
		{
			throw new Exception("按钮数组或值数组不能为空");
		} 
		else if (btn_ids.length != values.length || values.length != texts.length) 
		{
			throw new Exception("按钮数组与值数组的长度必须相同");
		} 
		else 
		{
			mDataList = new HashMap<Object, String>();
			mCurBtnID = 0;
			for (int i = 0; i < btn_ids.length; i++) 
			{
				final int btn_id = btn_ids[i];
				View view = viewGetter.findViewById(btn_id);
				if (view == null || !(view instanceof Button)) 
				{
					throw new Exception("按钮无法找到");
				}
				((Button) view).setText(texts[i]);
				((Button) view).setHint(values[i]);
				((Button) view).setOnClickListener(new OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						int temp = mCurBtnID;
						// 设置按钮选中状态
						if (v.getId() != mCurBtnID) 
						{
							checkButtonStatus(btn_id);
						}
						// 触发按钮的监听事件
						if(mClickListener != null) 
						{
							mClickListener.onClick(temp, v.getId());
						}
					}
				});
				mDataList.put(btn_id, values[i]);
			}
			this.mBtnIDs 			= btn_ids;
			this.mBackground 		= background;
			this.mBackgroundPressed = background_pressed;
			this.mViewGetter 		= viewGetter;
		}
	}
	
	private ButtonGroupUtil(ViewGetter viewGetter, int[] btn_ids, String[] values, int background, int background_pressed, int[][] fontColor) throws Exception 
	{
		if (btn_ids == null || values == null || btn_ids.length == 0 || values.length == 0) 
		{
			throw new Exception("按钮数组或值数组不能为空");
		} 
		else if (btn_ids.length != values.length) 
		{
			throw new Exception("按钮数组与值数组的长度必须相同");
		} 
		else 
		{
			mDataList = new HashMap<Object, String>();
			mCurBtnID = 0;
			for (int i = 0; i < btn_ids.length; i++) 
			{
				final int btn_id = btn_ids[i];
				View view = viewGetter.findViewById(btn_id);
				if (view == null || !(view instanceof Button)) 
				{
					throw new Exception("按钮无法找到");
				}
				((Button)view).setOnClickListener(new OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						int temp = mCurBtnID;
						// 设置按钮选中状态
						if (v.getId() != mCurBtnID) 
						{
							checkButtonStatus(btn_id);
						}
						// 触发按钮的监听事件
						if(mClickListener != null) 
						{
							mClickListener.onClick(temp, v.getId());
						}
					}
				});
				mDataList.put(btn_id, values[i]);
			}
			this.mBtnIDs 			= btn_ids;
			this.mBackground 		= background;
			this.mBackgroundPressed = background_pressed;
			this.mViewGetter 		= viewGetter;
			if(fontColor != null)
			{
				mFontColor = fontColor;
			}
		}
	}
	
	private ButtonGroupUtil(ViewGetter viewGetter, int[] btn_ids, String[] values, String[] texts, int background, int background_pressed, int[][] fontColor) throws Exception 
	{
		if (btn_ids == null || values == null || btn_ids.length == 0 || values.length == 0 || texts == null || texts.length == 0) 
		{
			throw new Exception("按钮数组或值数组不能为空");
		} 
		else if (btn_ids.length != values.length || values.length != texts.length) 
		{
			throw new Exception("按钮数组与值数组的长度必须相同");
		} 
		else 
		{
			mDataList = new HashMap<Object, String>();
			mCurBtnID = 0;
			for (int i = 0; i < btn_ids.length; i++) 
			{
				final int btn_id = btn_ids[i];
				View view = viewGetter.findViewById(btn_id);
				if (view == null || !(view instanceof Button)) 
				{
					throw new Exception("按钮无法找到");
				}
				((Button) view).setText(texts[i]);
				((Button) view).setHint(values[i]);
				((Button) view).setOnClickListener(new OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						int temp = mCurBtnID;
						// 设置按钮选中状态
						if (v.getId() != mCurBtnID) 
						{
							checkButtonStatus(btn_id);
						}
						// 触发按钮的监听事件
						if(mClickListener != null) 
						{
							mClickListener.onClick(temp, v.getId());
						}
					}
				});
				mDataList.put(btn_id, values[i]);
			}
			this.mBtnIDs 			= btn_ids;
			this.mBackground 		= background;
			this.mBackgroundPressed = background_pressed;
			this.mViewGetter 		= viewGetter;
			if(fontColor != null)
			{
				mFontColor = fontColor;
			}
		}
	}
	
	public static ButtonGroupUtil createBean(ViewGetter viewGetter, int[] btn_ids, String[] values, int background, int background_pressed)
			throws Exception 
	{
		ButtonGroupUtil util = new ButtonGroupUtil(viewGetter, btn_ids, values, background, background_pressed);
		return util;
	}
	
	public static ButtonGroupUtil createBean(ViewGetter viewGetter, int[] btn_ids, String[] values, String[] texts, int background, int background_pressed)
			throws Exception 
	{
		ButtonGroupUtil util = new ButtonGroupUtil(viewGetter, btn_ids, values, texts, background, background_pressed);
		return util;
	}
	
	public static ButtonGroupUtil createBean(ViewGetter viewGetter, int[] btn_ids, String[] values, int background, int background_pressed, int[][] fontColor)
			throws Exception 
	{
		ButtonGroupUtil util = new ButtonGroupUtil(viewGetter, btn_ids, values, background, background_pressed,fontColor);
		return util;
	}
	
	public static ButtonGroupUtil createBean(ViewGetter viewGetter, int[] btn_ids, String[] values, String[] texts, int background, int background_pressed, int[][] fontColor)
			throws Exception 
	{
		ButtonGroupUtil util = new ButtonGroupUtil(viewGetter, btn_ids, values, texts, background, background_pressed, fontColor);
		return util;
	}
	
	// 设置按钮状态
	private void checkButtonStatus(int nBtnID) 
	{
		for (int btnid:mBtnIDs) 
		{
			Button view = (Button) mViewGetter.findViewById(btnid);
			if (btnid == nBtnID) 
			{
				view.setBackgroundResource(mBackgroundPressed);
				if(mFontColor != null)
				{
					view.setTextColor(Color.argb(mFontColor[0][0], 
							mFontColor[0][1], mFontColor[0][2], mFontColor[0][3]));
				}
				//view.setTextColor(Color.parseColor("#FF3506"));
			} 
			else 
			{
				view.setBackgroundResource(mBackground);
				if(mFontColor != null)
				{
					view.setTextColor(Color.argb(mFontColor[1][0], 
							mFontColor[1][1], mFontColor[1][2], mFontColor[1][3]));
				}
				//view.setTextColor(Color.BLACK);
			}
		}
		mCurBtnID = nBtnID;
	}
	
	// 获得按钮文字
	public String getValue() throws Exception 
	{
		if (mDataList == null) 
		{
			throw new Exception("获取绑定数据异常");
		} 
		else if (mCurBtnID == 0) 
		{
			return null;
		} 
		else 
		{
			return mDataList.get(mCurBtnID);
		}
	}
	
	// 清除选中
	public void clearSelection() 
	{
		mCurBtnID = 0;
		for (int btnid:mBtnIDs) 
		{
			Button view = (Button) mViewGetter.findViewById(btnid);
			view.setBackgroundResource(mBackground);
			view.setTextColor(Color.BLACK);
		}
	}
	
	// 设置按钮组的监听事件
	public void setClickListener(ButtonGroupClickListener listener) 
	{
		this.mClickListener = listener;
	}
	
	public interface ButtonGroupClickListener 
	{
		public void onClick(int previousId,int newId);
	}
	
	public void setSelected(int nIndex) 
	{
		for(int i = 0; i<mBtnIDs.length; i++) 
		{
			int btnid = mBtnIDs[i];
			if(i == nIndex) 
			{
				mCurBtnID = btnid;
				Button view = (Button) mViewGetter.findViewById(btnid);
				view.setBackgroundResource(mBackgroundPressed);
				if(mFontColor != null)
				{
					view.setTextColor(Color.argb(mFontColor[0][0], 
							mFontColor[0][1], mFontColor[0][2], mFontColor[0][3]));
				}
			}
			else
			{
				Button view = (Button) mViewGetter.findViewById(btnid);
				view.setBackgroundResource(mBackground);
				if(mFontColor != null)
				{
					view.setTextColor(Color.argb(mFontColor[1][0], 
							mFontColor[1][1], mFontColor[1][2], mFontColor[1][3]));
				}
			}
		}
	}
	
	public String getValue(int id) throws Exception 
	{
		if(mDataList == null) 
		{
			throw new Exception("获取绑定数据异常");
		}
		return mDataList.get(id);
	}
	
	public interface ViewGetter 
	{
		public View findViewById(int id);
	}
}
