package com.ai.cmccha.sc.idcard.global;

import java.util.Map;

import android.os.Environment;

public class Global 
{
	// 是否在调试模式
	public static boolean			mIsDebug				= false;
	// SD卡根目录
	public static final String		SDCARD_PATH				= Environment.getExternalStorageDirectory().toString();
	// 终端类型 0x00-平板 0x01-M66 0x02-71
	public static final int			mSystemType				= 0x00;
	// 终端是否可用
	public static boolean			mIsTerCanUse			= true;
	// 终端禁用描述
	public static String			mStrTerminalErrMsg		= null;
	// 签到密钥
	public static byte[]			mLoginKey				= null;
	// 业务办理的最后一次时间
	public static long				mLastBusiDealTime		= 0;
	// 是否在进行业务办理
	public static boolean			mIsBusiDeal				= false;
	// 最后一次数据发送的时间
	public static long				mLastMsgSendTime		= 0;
	// 最后一次数据是否发送
	public static boolean			mIsLastMsgSend			= false;
	// 有收到数据包
	public static boolean			mDataExist				= false;
	// 是否模拟设备
	public static boolean			mIsMockEquipment		= false;
	// 是否软加密
	public static boolean			mIsSoftEncrypt			= false;
	
	// 等待时间
	public static int 				nTimeoutMedium			= 45;
	
	// 蓝牙名称
	public static String			BLUETOOTH_NAME			= "bluetooth_name";
	public static String			BLUETOOTH_MAC			= "bluetooth_mac";
	public static String			BLUETOOTH_STATUS		= "bluetooth_status";
	public static Map<String, String> mCurDevice			= null;
	// 外设初始化OK
	public static boolean			mDeviceIncInitOk		= false;
	
	// 代理商地址
	public static String			mStrAgentAddress		= null;
	// 代理商电话
	public static String			mStrAgentTel			= null;
	
	// 基础URL
	public static String			mStrBusiUrlBase			= null;
	// 终端软件升级URL
	public static String			mStrServerUrlBase		= null;				// 终端软件升级URL
	// 证件地址上传URL
	public static String			mStrUploadUrl			= null;
	
	public static int				CONNECT_TIMEOUT			= 30*1000;
	public static int				READ_TIMEOUT			= 15*1000;
}
