package com.ai.cmccha.sc.idcard;

import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.ai.cmccha.sc.idcard.global.Global;
import com.ai.cmccha.sc.idcard.uilogic.UIHandler;
import com.ai.cmccha.sc.idcard.uilogic.UILogic;
import com.ai.cmccha.sc.idcard.util.NetworkUtil;
import com.ai.cmccha.sc.idcard.util.StringUtil;

public class LoginLayout extends UILogic
{
	private static final String		TITLE_STR 			= "系统登录";
	private EditText				mEtPhone;								// 手机号码输入框
	private EditText				mEtRandom;								// 随机码输入框
	
	public LoginLayout(MainActivity activity) 
	{
		super(activity);
	}
	
	@Override
	public void go() 
	{
		// 设置布局
		mMainActivity.setContentView(R.layout.lay_login);
		// 初始化控件
		initViews();
	}
	
	@Override
	protected void initViews() 
	{
		mEtPhone = (EditText) mMainActivity.findViewById(R.id.et_phone);
		mEtRandom = (EditText) mMainActivity.findViewById(R.id.et_random);
		// 登录按钮
		mMainActivity.findViewById(R.id.btn_login).setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 登录
		case R.id.btn_login:
			// 处理登陆操作
			procLogin();
			break;
			
		default:
			break;
		}
	}
	
	// 处理登陆操作
	private void procLogin() 
	{
		// 检测网络
		if(NetworkUtil.isConnect(mMainActivity) == false) 
		{
			UIHandler.MsgBoxOne(TITLE_STR, mMainActivity.getString(R.string.network_unavailable), 0, UIHandler.MSG_ERROR);
			return;
		}
		// 校验手机号码
		final String strPhone = mEtPhone.getText().toString();
		if(StringUtil.isEmptyOrNull(strPhone) || strPhone.length() != 11) 
		{
			UIHandler.MsgBoxOne(TITLE_STR, "请输入11位手机号码！", 0, UIHandler.MSG_ERROR);
			return;
		}
		// 校验随机码
		final String strRandom = mEtRandom.getText().toString();
		if(StringUtil.isEmptyOrNull(strRandom) || strRandom.length() != 6) 
		{
			UIHandler.MsgBoxOne(TITLE_STR, "请输入6位随机码！", 0, UIHandler.MSG_ERROR);
			return;
		}
		// 处理操作
		new Thread() 
		{
			public void run() 
			{
				// 跳转到主界面
				UIHandler.initBusiStop();
				UIHandler.sendMessage(UIHandler.MSG_SYSTEM_INIT_DONE);
			}
		}.start();
	}
	
	@Override
	public void MsgOK(int msgID) 
	{
		switch(msgID) 
		{
		// 初始化完成
		case UIHandler.MSG_SYSTEM_INIT_DONE:
			// 跳转到主界面
			mMainActivity.goto_Layout(R.layout.lay_mainmenu);
			break;
			
		default:
			super.MsgOK(msgID);
			break;
		}
	}
	
	@Override
	public void MsgCancle(int msgID) 
	{
		switch(msgID) 
		{
		default:
			super.MsgCancle(msgID);
			break;
		}
	}
	
	// 处理返回事件
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK) 
		{
			Global.mCurDevice = null;
			mMainActivity.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
