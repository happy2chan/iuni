package com.ai.cmccha.sc.idcard.adpater;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

// DBHelper做创建数据表和更新数据表的操作
public class DBHelper extends SQLiteOpenHelper
{
	// 数据库名称为data
	private static final String	DB_NAME			= "CM_IDCARD.db";
	// 数据库版本
	private static final int	DB_VERSION		= 1;

	public DBHelper(Context context)
	{
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	/**
	 * 在数据库第一次生成的时候会调用这个方法，也就是说，只有在创建数据库的时候才会调用，当然也有一些其它的情况，
	 * 一般我们在这个方法里边生成数据库表。
	 */
	public void onCreate(SQLiteDatabase db)
	{
		// 创建图片日志表(接口)
		String db_imgLog = "CREATE TABLE IF NOT EXISTS IMG_LOG (" + "_id INTEGER PRIMARY KEY, " + "BLLNUM TEXT,"
				+ "PHOTONUM INTEGER," + "PHOTONAMES TEXT" + ")";
		db.execSQL(db_imgLog);
		// 创建图片日志表(服务器)
		String db_imgUploadLog = "CREATE TABLE IF NOT EXISTS IMG_UPLOAD_LOG (" + "_id INTEGER PRIMARY KEY, "
				+ "FILEPATH TEXT," + "REMOTEPATH TEXT" + ")";
		db.execSQL(db_imgUploadLog);
	}

	@Override
	/**
	 * 当数据库需要升级的时候，Android系统会主动的调用这个方法。一般我们在这个方法里边删除数据表，并建立新的数据表，
	 * 当然是否还需要做其他的操作，完全取决于应用的需求。
	 */
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS IMG_LOG");
		db.execSQL("DROP TABLE IF EXISTS IMG_UPLOAD_LOG");
		onCreate(db);
	}
}
