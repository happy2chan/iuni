package com.ai.cmccha.sc.idcard.bean;

public class ProgressMsg 
{
	// 登录步骤提示
	public static String		mLoginTitle = "系统登录";
	public static String[][]	mLoginInfo = 
	{
		{"正在进行系统初始化...",		"10"}, 
		{"正在校验登录密码...",		"20"}, 
		{"正在连接服务器...",		"25"}, 
		{"正在登录上报...",			"30"}, 
		{"正在获取安全密钥...",		"40"}
	};
	
	// 系统更新
	public static String 		mParamUpdateTitle = "参数更新";
	public static String[][] 	mParamUpdateInfo = 
	{
		{"正在进行菜单更新...",		"50"}, 
		{"正在进行号段更新...",		"50"}, 
		{"正在进行邮件更新...",		"50"}
	};
	
}
