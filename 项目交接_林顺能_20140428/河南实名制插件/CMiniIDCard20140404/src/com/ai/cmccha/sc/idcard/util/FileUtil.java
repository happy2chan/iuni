package com.ai.cmccha.sc.idcard.util;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.UUID;

import org.apache.http.util.EncodingUtils;

import android.os.Environment;

public class FileUtil
{
	public static String newImageName() 
	{
		return String.valueOf(UUID.randomUUID()).replaceAll("-", "") + ".j";
	}
	
	public static String newImageScaleName() 
	{
		return String.valueOf(UUID.randomUUID()+"-scale").replaceAll("-", "") + ".j";
	}
	
	// 拷贝文件
	// from 源文件, to 目标文件
	public static final boolean copyFile(File from, File to) 
	{
		boolean ret = true;
		
		InputStream is = null;
		OutputStream os = null;
		int len = 0;
		byte [] buf = new byte[1024];
		
		try
		{
			is = new FileInputStream(from);
			os = new FileOutputStream(to);
			
			while((len = is.read(buf))!=-1)
			{
				os.write(buf,0,len);
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(os != null) 
			{
				try
				{
					os.close();
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
				os = null;
			}
			
			if(is != null)
			{
				try
				{
					is.close();
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
				is = null;
			}
		}
		return ret;
	}
	
	// 将文件直接读取为字符串返回
	// path 文件路径
	// return 文件内容
	@SuppressWarnings("deprecation")
	public static String file2String(String path)
	{
		DataInputStream dis = null;
		StringBuffer sb = new StringBuffer("");
		String line = null;
		
		try
		{
			dis = new DataInputStream(new FileInputStream(path));
			
			while((line = dis.readLine()) != null) 
			{
				sb.append(new String(line.getBytes("iSO-8859-1"), "UTF-8"));
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(dis != null) 
			{
				try
				{
					dis.close();
					dis = null;
				} 
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}
	
	/**
	 * 将文件直接读取为字符串返回
	 * @param path 文件路径
	 * @return 文件内容
	 */
	@SuppressWarnings("deprecation")
	public static String file2String(String path, String encode) 
	{
		DataInputStream dis = null;
		StringBuffer sb = new StringBuffer("");
		String line = null;
		
		try
		{
			dis = new DataInputStream(new FileInputStream(path));
			
			while((line = dis.readLine()) != null) 
			{
				sb.append(new String(line.getBytes(),encode));
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(dis != null) 
			{
				try
				{
					dis.close();
					dis = null;
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}
	
	/**
	 * 将字符串存为文件
	 * @param path 文件路径
	 * @return 文件内容
	 */
	public static boolean string2File(String data, File f) 
	{
		OutputStream os = null;
		
		try
		{
			os = new FileOutputStream(f);
			os.write(data.getBytes());
			os.flush();
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			if(os != null) 
			{
				try
				{
					os.close();
					os = null;
				} 
				catch (IOException e)
				{
					e.printStackTrace();
					return false;
				}
			}
		}
		return true;
	}
	
	// 删除指定的sdka的数据
	public static boolean  delSdkFile(String fileName)
	{
		String fileAllName = fileName+".txt";
		String DIR = Environment.getExternalStorageDirectory()+"/sunnada";
		File dir = new File(DIR);
		File file = new File(dir,fileAllName);
		if(file.exists())
		{
			file.delete();
		}
		return true;
	}
	
	// 判断sd卡中是否存在该文件
	public static boolean isFileExit(String fileName) 
	{
		String fileAllName = fileName+".txt";
		String DIR = Environment.getExternalStorageDirectory()+"/sunnada";
		File dir = new File(DIR);
		File file = new File(dir,fileAllName);
		if(file.exists()) 
		{
			return true;
		}
		return false;
	}
	
	// 读取sd卡该文件的数据
	// 文件名字
	public static String readFromSdCard(String fileName)
	{
		String s = "";
		String fileAllName = fileName + ".txt";
		String DIR = Environment.getExternalStorageDirectory() + "/sunnada";
		File file = new File(DIR, fileAllName);
		FileInputStream is = null;
		if (file.exists()) 
		{
			try 
			{
				is = new FileInputStream(file);
				byte buffer[] = new byte[is.available()];
				is.read(buffer);
				s = EncodingUtils.getString(buffer, "GBK");
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			} 
			finally 
			{
				if (is != null) 
				{
					try 
					{
						is.close();
						is = null;
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
			}
		}
		return s;
	}
	
	// 通过url地址解析文件名
	public static final String getFileNameByUrl(String url)
	{
		String name = null;
		name = url.substring(url.lastIndexOf("/")+1, url.length());
		return name;
	}
	
	// 将字符串保存在Sdk文件下 
	// fileName-文件名称, data-要保持文件数据
	public static boolean string2SDcardFile(String fileName, String data) 
	{
		String fileAllName = fileName + ".txt";
		String DIR = Environment.getExternalStorageDirectory() + "/sunnada";
		File a = new File(DIR);
		File file = new File(a, fileName);
		// 判断文件夹是否存在，不存在则创建
		if (!a.exists()) 
		{
			a.mkdir();
		}
		// 判断文件是否存在不存在创建
		if (!file.exists()) 
		{
			file = new File(a, fileAllName);
			FileOutputStream outStream = null;
			OutputStreamWriter write = null;
			BufferedWriter writer = null;
			try 
			{
				outStream = new FileOutputStream(file);
				write = new OutputStreamWriter(outStream, "GBK");
				writer = new BufferedWriter(write);
				writer.write(data);
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				return false;
			} 
			finally 
			{
				if (writer != null) 
				{
					try 
					{
						writer.close();
						writer = null;
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
						return false;
					}
				}
			}
		}
		return true;
	}
	
	// 从文件中获取数据
	public static byte[] getBytesFromFile(File file) throws IOException 
	{
		FileInputStream fileinputstream;
		fileinputstream = new FileInputStream(file);
		long l = file.length();
		if (l > 0x7fffffffL) 
		{
			fileinputstream.close();
			throw new IOException((new StringBuilder("File is to large "))
					.append(file.getName()).toString());
		}
		byte[] abyte0 = new byte[(int) l];
		int i = 0;
		while (i < abyte0.length) 
		{
			int j = fileinputstream.read(abyte0, i, abyte0.length - i);
			if (j >= 0) 
			{
				i += j;
			}
		}
		if (i < abyte0.length)
			throw new IOException((new StringBuilder(
					"Could not completely read file ")).append(file.getName())
					.toString());
		fileinputstream.close();
		return abyte0;
	}
	
	public static byte[] getBytesFromFile(String s) throws IOException 
	{
		return getBytesFromFile(new File(s));
	}
}
