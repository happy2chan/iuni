package com.ai.cmccha.sc.idcard.bean;

import java.io.Serializable;

// 键值对，用以给spinner赋值
@SuppressWarnings("serial")
public class KeyTextBean implements Serializable 
{
	private String mStrKey;
	private String mStrText;

	public KeyTextBean() 
	{

	}

	public KeyTextBean(String key, String text) 
	{
		this.mStrKey = key;
		this.mStrText = text;
	}

	@Override
	public String toString() 
	{
		return mStrText;
	}

	// 获取键值
	public String getKey() 
	{
		return mStrKey;
	}

	// 设置键值
	public void setKey(String strKey) 
	{
		this.mStrKey = strKey;
	}

	// 获取值
	public String getText() 
	{
		return mStrText;
	}
	
	// 设置值
	public void setText(String strText) 
	{
		this.mStrText = strText;
	}
}
	 