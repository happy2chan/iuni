package com.ai.cmccha.sc.idcard.dialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ai.cmccha.sc.idcard.R;
import com.ai.cmccha.sc.idcard.bean.ProductInfo;
import com.ai.cmccha.sc.idcard.database.ProductInfoDBAdapter;
import com.ai.cmccha.sc.idcard.uilogic.UIHandler;
import com.ai.cmccha.sc.idcard.util.ButtonUtil;
import com.ai.cmccha.sc.idcard.util.InputMethodUtil;

public class SelectProductDialog extends Dialog implements android.view.View.OnClickListener
{
	// 全球通
	private Button							mBtnQqt;
	// 动感地带
	private Button							mBtnDgdd;
	// 神州行
	private Button							mBtnSzx;
	// 套餐搜索
	private EditText						mEtProduct;
	// 搜索按钮
	private Button							mBtnSearch;
	// 套餐列表
	private ListView						mLvProduct;
	// 确定
	private Button							mBtnOk;
	// 取消
	private Button							mBtnCancel;
	// 三种类型的套餐数据
	private Map<String, List<ProductInfo>>	mData;
	// 套餐数据
	private List<ProductInfo>				mCurProList;
	// 选中的套餐
	private ProductInfo						mCurrdPro;
	// 显示列表适配器
	private ProAdapter						mAdapter;
	// 上一次选中的下标
	private int 							mOldPos = -1;
	// 当前选中的品牌类型
	private String							mCurType;
	
	private Handler							mHandler;
	
	public SelectProductDialog(Context context)
	{
		super(context);
		initView();
	}
	
	public SelectProductDialog(Context context, Handler handler, Map<String, List<ProductInfo>> data)
	{
		super(context);
		mData = data;
		mHandler = handler;
		initView(); 
	}

	public SelectProductDialog(Context context, boolean cancelable, OnCancelListener cancelListener)
	{
		super(context, cancelable, cancelListener);
		initView();
	}

	public SelectProductDialog(Context context, int theme)
	{
		super(context, theme);
		initView();
	}

	private void initView()
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dlg_select_package);
		mCurProList = new ArrayList<ProductInfo>();
		// 默认为全球通
		mCurType = ProductInfo.goTone;
		mEtProduct = (EditText) findViewById(R.id.et_search_con);
		mBtnQqt = (Button) findViewById(R.id.btn_go_tone);
		mBtnQqt.setOnClickListener(this);
		mBtnDgdd = (Button) findViewById(R.id.btn_mzone);
		mBtnDgdd.setOnClickListener(this);
		mBtnSzx = (Button) findViewById(R.id.btn_easy_own);
		mBtnSzx.setOnClickListener(this);
		mBtnSearch = (Button) findViewById(R.id.btn_search);
		mBtnSearch.setOnClickListener(this);
		mBtnOk = (Button) findViewById(R.id.btn_ok);
		mBtnOk.setOnClickListener(this);
		mBtnCancel = (Button) findViewById(R.id.btn_cancel);
		mBtnCancel.setOnClickListener(this);
		mLvProduct = (ListView) findViewById(R.id.list_product);
		mLvProduct.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3)
			{
				if(pos != mOldPos)
				{
					// 取消原来的选中状态
					if(mCurrdPro != null)
					{
						mCurrdPro.setIsSelected(false);
						mCurrdPro = null;
					}
					mCurrdPro = mCurProList.get(pos);
					mCurrdPro.setIsSelected(true);
					mOldPos = pos;
				}
				else
				{
					mCurrdPro = mCurProList.get(pos);
					if(mCurrdPro.isIsSelected())
					{
						// 如果选中，则取消
						mCurrdPro.setIsSelected(false);
						mCurrdPro = null;
					}
					else
					{
						// 如果没选中，则选中
						mCurrdPro.setIsSelected(true);
					}
				}
				mAdapter.notifyDataSetChanged();
			}
		});
	}

	@Override
	public void onClick(View v)
	{
		if(ButtonUtil.isFastDoubleClick(v.getId()))
		{
			return;
		}
		switch (v.getId())
		{
			// 全球通
			case R.id.btn_go_tone:
				mCurType = ProductInfo.goTone;
				setBrandSelected();
				mBtnQqt.setBackgroundResource(R.drawable.btn_qqt_selected);
				break;
				
			// 动感地带
			case R.id.btn_mzone:
				mCurType = ProductInfo.mZone;
				setBrandSelected();
				mBtnDgdd.setBackgroundResource(R.drawable.btn_dgdd_selected);
				break;
				
			// 神州行
			case R.id.btn_easy_own:
				mCurType = ProductInfo.easyOwn;
				setBrandSelected();
				mBtnSzx.setBackgroundResource(R.drawable.btn_szx_selected);
				break;
				
			// 搜索
			case R.id.btn_search:
				String con = mEtProduct.getText().toString().trim();
				InputMethodUtil.closeSoftKeyboard(SelectProductDialog.this);
				// 刷新列表前，取消当前选中状态
				if(mCurrdPro != null)
				{
					mCurrdPro.setIsSelected(false);
					mCurrdPro = null;
				}
				mCurProList = searchByCondition(con);
				mAdapter.notifyDataSetChanged();
				break;
				 
			// 确定
			case R.id.btn_ok:
				 if(mCurrdPro != null)
				 {
					Log.e("SelectProductDialog", mCurrdPro.toString());
					if(mHandler != null)
					{
						Message msg = UIHandler.mHandler.obtainMessage();
						msg.what = UIHandler.MSG_PRO_SELECTED;
						msg.obj = mCurrdPro;
						mHandler.sendMessage(msg);
					}
				 }
				 dismiss();
				break;
				
			// 取消
			case R.id.btn_cancel:
				dismiss();
				break;
			default:
				break;
		}
	}
	
	// 根据关键字查找符合条件的套餐
	private List<ProductInfo> searchByCondition(String con)
	{
		List<ProductInfo> result = new ArrayList<ProductInfo>();
		List<ProductInfo> curList = new ArrayList<ProductInfo>();
		// 每次都从当前选中的类型中查找，而不是当前显示列表
		curList.addAll(mData.get(mCurType)); 
		for(ProductInfo pro : curList)
		{
			if(pro.getName().contains(con))
			{
				Log.e("", "找个符合条件套餐：" + pro.toString());
				result.add(pro);
			}
		}
		return result;
	}

	// 选择品牌
	private void setBrandSelected()
	{
		mBtnQqt.setBackgroundResource(R.drawable.btn_qqt);
		mBtnDgdd.setBackgroundResource(R.drawable.btn_dgdd);
		mBtnSzx.setBackgroundResource(R.drawable.btn_szx);
		if(mCurrdPro != null)
		{
			//mSelectedPro.setIsSelected(false);
			//mSelectedPro = null;
			mOldPos = -1;
		}
		mEtProduct.setText("");
		mCurProList.clear();
		mCurProList.addAll(mData.get(mCurType));
		mAdapter.notifyDataSetChanged();
	}

	class ProAdapter extends BaseAdapter
	{
		@Override
		public int getCount()
		{
			return mCurProList.size();
		}

		@Override
		public Object getItem(int arg0)
		{
			return mCurProList.get(arg0);
		}

		@Override
		public long getItemId(int arg0)
		{
			return arg0;
		}

		@Override
		public View getView(int position, View converView, ViewGroup arg2)
		{
			ViewHolder holder = null;
			if(converView == null)
			{
				LayoutInflater inflater = LayoutInflater.from(getContext());
				converView = inflater.inflate(R.layout.pack_list_item, null);
				holder = new ViewHolder();
				holder.name = (TextView) converView.findViewById(R.id.tv_name);
				holder.status = (ImageView) converView.findViewById(R.id.iv_status);
				converView.setTag(holder);
			}
			else
			{
				holder = (ViewHolder) converView.getTag();
			}
			final ProductInfo product = mCurProList.get(position);
			holder.name.setText(product.getName());
			final ImageView status = holder.status;
			if(product.isIsSelected())
			{
				status.setBackgroundResource(R.drawable.icon_status_check);
			}
			else
			{
				status.setBackgroundResource(R.drawable.icon_status);
			}
			return converView;
		}
		
		class ViewHolder
		{
			TextView name = null;
			ImageView status = null;
		}
	}
	
	@Override
	public void show()
	{
		if(mData != null)
		{
			mCurProList.addAll(mData.get(mCurType));
			mAdapter = new ProAdapter();
			mLvProduct.setAdapter(mAdapter);
		}
		super.show();
		InputMethodUtil.closeSoftKeyboard(this);
	}
	
}
