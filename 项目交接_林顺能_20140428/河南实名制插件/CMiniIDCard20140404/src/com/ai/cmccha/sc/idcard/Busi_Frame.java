package com.ai.cmccha.sc.idcard;

import java.util.ArrayList;
import java.util.List;

import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewStub;
import android.view.ViewStub.OnInflateListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.ai.cmccha.sc.idcard.uilogic.UIHandler;
import com.ai.cmccha.sc.idcard.uilogic.UILogic;
import com.ai.cmccha.sc.idcard.util.InputMethodUtil;

// 业务抽象类
public class Busi_Frame extends UILogic
{
	// 选项名称
	private   List<String>  			mOptionName    		= new ArrayList<String>();
	// 选项的消息ID
	private   List<Integer>           	mOptionMsgId   		= new ArrayList<Integer>();
	protected TextView 					mTvBusiFrameTitle;
	protected Button					mBtnBusiFrameBack;
	protected ViewStub					mMainViewStub;
	
	protected View						mMainView			= null;
	protected EditText					mEditInput			= null;
	protected PopupWindow				mSelectPop			= null;
	
	public Busi_Frame(MainActivity context) 
	{
		super(context);
	}
	
	@Override
	public void go()
	{
		mMainActivity.setContentView(R.layout.lay_busi_frame);
		// 初始化控件
		this.initViewStub();
	}
	
	// 初始化控件
	protected void initViews() 
	{
		
	}
	
	private void initViewStub() 
	{
		// 标题
		mTvBusiFrameTitle = (TextView) mMainActivity.findViewById(R.id.tv_busi_frame_title);
		// 返回
		mBtnBusiFrameBack = (Button) mMainActivity.findViewById(R.id.btn_busi_frame_back);
		mBtnBusiFrameBack.setOnClickListener(this);
		
		// 关闭软键盘
		mMainViewStub = (ViewStub) mMainActivity.findViewById(R.id.busi_frame_stub);
		mMainViewStub.setOnInflateListener(new OnInflateListener() 
		{
			@Override
			public void onInflate(ViewStub stub, View inflated) 
			{
				System.out.println("viewStub onInflate");
				inflated.setOnTouchListener(new OnTouchListener() 
				{
					@Override
					public boolean onTouch(View arg0, MotionEvent arg1) 
					{
						System.out.println("viewStub onTouch");
						InputMethodUtil.closeSoftKeyboard(mMainActivity);
						return false;
					}
				});
			}
		});
	}
	
	// 如果有选项则设置,该函数需要在registerListener前调用
	protected void initOptionData(int nOptionSize, String[] optionName, int[] optionMsgId) 
	{
		mOptionMsgId.clear();
		mOptionName.clear();
		
		for (int i = 0; i < nOptionSize; i++) 
		{
			mOptionName.add(optionName[i]);
			mOptionMsgId.add(optionMsgId[i]);
		}
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 返回主界面
		case R.id.btn_busi_frame_back:
			mMainActivity.goto_Layout(R.layout.lay_mainmenu);
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public void MsgOK(int msgID) 
	{
		switch(msgID) 
		{
		default:
			super.MsgOK(msgID);
			break;
		}
	}
	
	@SuppressWarnings("deprecation")
	public void initPop(String[] soruce,final int[] handles) 
	{	
		LayoutInflater Inflater = LayoutInflater.from(mMainActivity);
		View view = Inflater.inflate(R.layout.pop_set, null);
		// 后期需要计算按钮大小动态设置宽度 高度采用listview小于屏幕即可
		mSelectPop = new PopupWindow(view, 190, 275, false);
		// 设置弹出pop外部点击消失
		mSelectPop.setBackgroundDrawable(new BitmapDrawable());   
		mSelectPop.setOutsideTouchable(true);
		mSelectPop.setFocusable(true);
		
		ListView more = (ListView) view.findViewById(R.id.listView1);
		more.setAdapter(new ArrayAdapter<String>(mMainActivity, R.layout.pop_item, R.id.textView1, soruce));
		more.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
			{

					UIHandler.mMsgID=handles[position];
					MsgOK(UIHandler.mMsgID);
					mSelectPop.dismiss();
			}
		});
	}
	
	public void showPopMenu(View v) 
	{
		if(!mSelectPop.isShowing())
		{
			int[] location = new int[2];  
			v.getLocationOnScreen(location);   
			mSelectPop.showAtLocation(v, Gravity.NO_GRAVITY, location[0], location[1]-250); 
		}
		else 
		{
			mSelectPop.dismiss();
		}
	}
}
