package com.ym.idcard.reg;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ScreenSetting extends ImageView {
	private int mBottom;
	private Context mContext;
	private int mLeft;
	private Paint mPaint;
	private int mRight;
	private int mScreenHeight;
	private int mScreenWidth;
	private int mTop;
	
	public ScreenSetting(Context context) {
		super(context);
		mContext = null;
		mPaint = null;
		mScreenWidth = 0;
		mScreenHeight = 0;
		mLeft = 0;
		mTop = 0;
		mRight = 0;
		mBottom = 0;
		mContext = context;
		initPoint();
	}

	public ScreenSetting(Context context, AttributeSet attributeset) {
		super(context, attributeset);
		mContext = null;
		mPaint = null;
		mScreenWidth = 0;
		mScreenHeight = 0;
		mLeft = 0;
		mTop = 0;
		mRight = 0;
		mBottom = 0;
		mContext = context;
		initPoint();
	}

	private void getScreenSize() {
		mScreenHeight = getHeight();
		mScreenWidth = getWidth();
		if (mScreenHeight > mScreenWidth) {
			int i = mScreenHeight;
			mScreenHeight = mScreenWidth;
			mScreenWidth = i;
		}
	}

	private void initPoint() {
		getScreenSize();
		mPaint = new Paint();
		mPaint.setStyle(android.graphics.Paint.Style.FILL);
		mPaint.setColor(mContext.getResources().getColor(0x1060000));
		mPaint.setStrokeWidth(4F);
		mLeft = (2 * mScreenWidth) / 100;
		mRight = (98 * mScreenWidth) / 100;
		mTop = (3 * mScreenHeight) / 100;
		mBottom = (97 * mScreenHeight) / 100;
	}

	public void invalidate() {
		initPoint();
		super.invalidate();
	}

	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawLine(-2 + mLeft, mTop, 4 * mLeft, mTop, mPaint);
		canvas.drawLine(mLeft, mTop, mLeft, 4 * mTop, mPaint);
		canvas.drawLine(-2 + mLeft, mBottom, 4 * mLeft, mBottom, mPaint);
		canvas.drawLine(mLeft, mBottom, mLeft, mBottom - 3 * mTop, mPaint);
		canvas.drawLine(2 + mRight, mTop, mRight - 3 * mLeft, mTop, mPaint);
		canvas.drawLine(mRight, mTop, mRight, 4 * mTop, mPaint);
		canvas.drawLine(2 + mRight, mBottom, mRight - 3 * mLeft, mBottom,mPaint);
		canvas.drawLine(mRight, mBottom, mRight, mBottom - 3 * mTop, mPaint);
	}

	public void onWindowFocusChanged(boolean flag) {
		if (flag)
			invalidate();
		super.onWindowFocusChanged(flag);
	}
}
