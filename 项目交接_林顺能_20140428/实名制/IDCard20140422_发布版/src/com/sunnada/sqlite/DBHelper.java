package com.sunnada.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper
{
	private static final String 		DB_NAME 		= "IDCARD.db";
	private static final int 			DB_VERSION 		= 4;
	
	private static final String 		mDbMsg = 
			"CREATE TABLE MSG ("
			+ "_id integer PRIMARY KEY autoincrement, " 
			+ "title TEXT, "
			+ "text TEXT, " 
			+ "flag TEXT)";
	
	public DBHelper(Context context) 
	{
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		db.execSQL(mDbMsg);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		db.execSQL("DROP TABLE IF EXISTS MSG");
		onCreate(db);
	}

}
