package com.sunnada.sqlite;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MsgBean implements Serializable
{
	private String			mStrKeyID;
	private String 			mStrKeyTitle;
	private String 			mStrKeyText;
	private String			mStrKeyFlag;

	public MsgBean() 
	{
		
	}

	public MsgBean(String idStr, String titleStr, String textStr, String flagStr) 
	{        
		super();
		
		this.mStrKeyID 	= idStr;        
		this.mStrKeyTitle 	= titleStr;
		this.mStrKeyText 	= textStr;
		this.mStrKeyFlag 	= flagStr;
	}    

	@Override
	public String toString() 
	{
		return "key_id = " + mStrKeyID + ", key_title = " + mStrKeyTitle 
				+ ", key_text=" + mStrKeyText + ", key_flag=" + mStrKeyFlag;
	}
	
	// 获取行号
	public String getID() 
	{
		return mStrKeyID;
	}

	// 设置行号
	public void setID(String id) 
	{
		this.mStrKeyID = id;
	}

	// 获取标题
	public String getTitle() 
	{
		return mStrKeyTitle;
	}

	// 设置标题
	public void setTitle(String key) 
	{
		this.mStrKeyTitle = key;
	}

	// 获取内容
	public String getText() 
	{
		return mStrKeyText;
	}

	// 设置内容
	public void setText(String text) 
	{
		this.mStrKeyText = text;
	}
	
	// 获取标志
	public String getFlag() 
	{
		return mStrKeyFlag;
	}

	// 设置标志
	public void setFlag(String text) 
	{
		this.mStrKeyFlag = text;
	}
}
