package com.sunnada.sqlite;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.kit.util.PLog;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class MsgDBAdapter extends DBadapter 
{
	private static final String TAG				= "MsgDBAdapter";
	private static final String DB_TABLE 		= "MSG";
	private static final String KEY_ID 			= "_id";
	private static final String KEY_TITLE 		= "title";
	private static final String KEY_TEXT 		= "text";
	private static final String KEY_FLAG 		= "flag";

	public MsgDBAdapter(Context context) 
	{
		super(context);
	}

	public long insertData(String titleStr, String textStr, String flagStr)  
	{
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_TITLE, 	titleStr);
		initialValues.put(KEY_TEXT, 	textStr);
		initialValues.put(KEY_FLAG, 	flagStr);
		
		return mDB.insert(DB_TABLE, KEY_ID, initialValues);
	}
	
	public void updateData(String idStr, String flagStr) 
	{
		ContentValues args = new ContentValues();
		args.put(KEY_FLAG, flagStr);
		mDB.update(DB_TABLE, args, KEY_ID + " = ?", new String[] { idStr });
	}
	
	// 取指定数据的行号
	public String fetchDataByTag(String title) 
	{
		Cursor cursor = mDB.query(DB_TABLE, new String[] { KEY_TEXT }, KEY_TITLE + " = ?" , new String[] { title }, null, null, null);
		if (cursor != null && cursor.getCount() != 0) 
		{
			cursor.moveToFirst();
			int index = cursor.getColumnIndex(KEY_TEXT);
			Log.e(TAG, "index = " + index);
			String tempstr = cursor.getString(index);
			cursor.close();
			return tempstr;
		}
		return null;
	}
	
	// 更新指定行的数据
	public static boolean updateDataByID(Context context, String idStr, String flagStr) 
	{
		MsgDBAdapter adapter = null;
		boolean result = false;
		
		try 
		{
			adapter = new MsgDBAdapter(context);
			adapter.open();
			// 更新
			ContentValues args = new ContentValues();
			args.put(KEY_FLAG, flagStr);
			if(mDB.update(DB_TABLE, args, KEY_ID + " = ?", new String[] { idStr }) > 0) 
			{
				result = true;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(adapter != null)
			{
				adapter.close();
			}
		}
		return result;
	}
	
	// 删除指定行的数据
	public static boolean deleteDataByID(Context context, String idStr) 
	{
		MsgDBAdapter adapter = null;
		boolean result = false;
		
		try 
		{
			adapter = new MsgDBAdapter(context);
			adapter.open();
			// 删除
			PLog.v("正在删除ID为" + idStr + "的公告...");
			if(mDB.delete(DB_TABLE, KEY_ID + "=" + idStr, null) > 0) 
			{
				result = true;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(adapter != null)
			{
				adapter.close();
			}
		}
		return result;
	}
	
	// 获取所有数据
	public static List<MsgBean> getAllData(Context context, int nStartIdx, int nCount, int[] nTotal) 
	{
		MsgDBAdapter adapter = new MsgDBAdapter(context);
		List<MsgBean> listMsg = null;
		
		try 
		{
			adapter.open();
			//Cursor cur = mDB.query(DB_TABLE, new String[] { KEY_ID, KEY_TITLE, KEY_TEXT, KEY_FLAG}, null, null, null, null, null);
			String sqlStr = "select * from MSG order by _id desc";
			Cursor cur = mDB.rawQuery(sqlStr, null);
			if (cur != null) 
			{
				// 公告总数
				nTotal[0] = cur.getCount();
				if(nTotal[0] != 0) 
				{
					listMsg = new ArrayList<MsgBean>();
					cur.moveToFirst();
					
					for(int i=0; i<nStartIdx; i++) 
					{
						cur.moveToNext();
					}
					if(nTotal[0]-nStartIdx < nCount) 
					{
						nCount = nTotal[0]-nStartIdx;
					}
					Log.v(TAG, String.format("公告总数: %d, 当前索引: %d, 当前提取总数: %d", nTotal[0], nStartIdx, nCount));
					
					for(int i=nStartIdx; i<nStartIdx+nCount; i++) 
					{
						MsgBean bean = new MsgBean();
						bean.setID(cur.getString(cur.getColumnIndex(KEY_ID)));
						bean.setTitle(cur.getString(cur.getColumnIndex(KEY_TITLE)));
						bean.setText(cur.getString(cur.getColumnIndex(KEY_TEXT)));
						bean.setFlag(cur.getString(cur.getColumnIndex(KEY_FLAG)));
						Log.d(TAG, bean.getID() + "|" + bean.getTitle() + "|"  + bean.getText() + "|" + bean.getFlag());
						
						listMsg.add(bean);
						cur.moveToNext();
					}
				}
				cur.close();
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(adapter != null) 
			{
				adapter.close();
			}
		}
		return listMsg;
	}
}
