package com.chinaunicom.custinforegist.kit.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.chinaunicom.custinforegist.api.robospice.BaseRequest;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

public class HttpUtils 
{
	private static final String		TAG							= "HTTP下载";
	public static final int 		DEF_CONNECT_TIMEOUT 		= 10000;
	public static final int 		DEF_SOKET_TIMEOUT 			= 10000;
	public static final int 		DEFAULT_PROXY_PORT 			= 80;
	public static final int 		HTTP_OK_CODE 				= 202;
	public static final int 		TYPE_WAP 					= 1;
	public static final int 		TYPE_NET 					= 2;
	public static final int 		TYPE_UNKNOWN 				= 3;
	public static final String 		DEFAULT_ENCODE 				= HTTP.UTF_8;
	private static final String 	HTTP_ACCEPT_ENCODING 		= "Accept-Encoding";

	public static interface OnDownloadListener 
	{
		public abstract void onError(String url);

		public abstract void onDownload(int readLen, long copyLen, long fileLen);
	}

	public HttpUtils() 
	{
		
	}

	private static HttpUriRequest getHttpGetRequest(String url, boolean flag, Context context) 
	{
		HttpGet httpget = new HttpGet(url);
		httpget.setHeader(HTTP_ACCEPT_ENCODING, "gzip, deflate");
		httpget.getParams().setBooleanParameter("http.protocol.handle-redirects", flag);
		return httpget;
	}

	public static HttpClient getHttpClient(int connectTimeout, int soketTimeout, Context context) 
	{
		BasicHttpParams baseParams = new BasicHttpParams();
		HttpProtocolParams.setVersion(baseParams, HttpVersion.HTTP_1_1);//
		HttpProtocolParams.setContentCharset(baseParams, "utf-8");//
		HttpConnectionParams.setConnectionTimeout(baseParams, connectTimeout);
		HttpConnectionParams.setSoTimeout(baseParams, soketTimeout);
		// HttpUtils.fillProxy(context, baseParams);
		DefaultHttpClient client = new DefaultHttpClient(baseParams);
		client.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(3, true));
		return client;
	}

	public static HttpResponse doRawGet(String url) throws IllegalStateException, IOException 
	{
		return doRawGet(url, false, DEF_CONNECT_TIMEOUT, DEF_SOKET_TIMEOUT, null);
	}

	private static HttpResponse doRawGet(String url, boolean flag, int connectTimeout, int soketTimeout, Context context)
			throws IllegalStateException, IOException 
	{
		// get http uri request
		HttpUriRequest uriRequest = getHttpGetRequest(url, flag, context);
		// get http client
		HttpClient client = getHttpClient(connectTimeout, soketTimeout, context);
		// execute uri request
		HttpResponse response = client.execute(uriRequest);
		return response;
	}

	private static HttpEntity doGet(String url, boolean flag, int timeout, Context context) throws IllegalStateException, IOException 
	{
		HttpEntity httpEntity = null;
		HttpResponse response = doRawGet(url, flag, timeout, timeout, context);
		int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode == HttpStatus.SC_OK) 
		{
			httpEntity = response.getEntity();
		}
		return httpEntity;
	}

	public static InputStream getStringStream(String str) 
	{
		ByteArrayInputStream bais = null;
		if (str != null && !str.trim().equals("")) 
		{
			bais = new ByteArrayInputStream(str.getBytes());
		}
		return bais;
	}

	public static String entityToString(HttpEntity entity) throws IOException 
	{
		return entityToString(entity, DEFAULT_ENCODE);
	}

	private static boolean isGzipEntity(HttpEntity entity) 
	{
		if (entity.getContentEncoding() != null && entity.getContentEncoding().getValue().contains("gzip")
				|| entity.getContentType() != null && "application/zip".equals(entity.getContentType().getValue())) 
		{
			return true;
		}
		return false;
	}

	public static String entityToString(HttpEntity entity, String encode) throws IOException 
	{
		if (entity != null) 
		{
			String content;
			if (isGzipEntity(entity)) 
			{
				GZIPInputStream is = new GZIPInputStream(entity.getContent());
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				IOUtils.copy(is, bos);
				bos.close();
				content = new String(bos.toByteArray(), encode);
			} else {
				content = EntityUtils.toString(entity, encode);
			}
			return content;
		}
		return null;
	}

	/**
	 * 下载文件
	 * 
	 * @param url
	 * @param filePath
	 * @param context
	 * @return
	 */
	public static File toFile(String url, String filePath, Context context) {
		return toFile(url, filePath, context, null);
	}

	/**
	 * 下载文件
	 * 
	 * @param url
	 * @param filePath
	 * @param context
	 * @param listener
	 * @return
	 */
	public static File toFile(String url, String filePath, Context context, final OnDownloadListener listener) 
	{
		if (TextUtils.isEmpty(url)) 
		{
			if (listener != null) {
				listener.onError(url);
			}
			return null;
		}
		if (TextUtils.isEmpty(filePath)) 
		{
			if (listener != null) {
				listener.onError(url);
			}
			return null;
		}
		InputStream is = null;
		FileOutputStream fos = null;
		try {
			File file = new File(filePath);
			file.getParentFile().mkdirs();
			HttpEntity entity = doGet(url, true, DEF_CONNECT_TIMEOUT, context);
			if (entity != null) {
				is = entity.getContent();
				final long fileLength = entity.getContentLength();
				fos = new FileOutputStream(file);
				if (listener != null) {
					IOUtils.copy(is, fos, new IOUtils.CopyListener() {
						public void onCopy(int readLen, long copyLen) {
							listener.onDownload(readLen, copyLen, fileLength);
						}
					});
				} else {
					IOUtils.copy(is, fos);
				}
				return file;
			} else {
				if (listener != null) {
					listener.onError(url);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			if (listener != null) {
				listener.onError(url);
			}
		} finally {
			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(fos);
		}
		return null;
	}

	public static void toFile(String host, int port, String username, String password, String remoteFile, String path,
			final OnDownloadListener listener) 
	{
		FTPClient ftpClient = new FTPClient();
		FileOutputStream fos = null;
		
		try {
			// 连接FTP服务器
			ftpClient.connect(host, port);
			int reply = ftpClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) 
			{
				throw new Exception("Connect failed: " + ftpClient.getReplyString());
			}
			if (!ftpClient.login(username,password)) 
			{
				throw new Exception("Login failed: " + ftpClient.getReplyString());
			}
			// 进入被动模式
			ftpClient.enterLocalPassiveMode();
			if (!ftpClient.setFileType(FTP.BINARY_FILE_TYPE)) 
			{
				
			}
           
			File file = new File(path);
			file.getParentFile().mkdirs();
			fos = new FileOutputStream(file);
			// 获取文件大小
			final long fileSize = getFileSize(ftpClient, remoteFile);
			if(!(fileSize == 0)) 
			{
				// 获取文件
               	InputStream is = retrieveFileStream(ftpClient, remoteFile);
               	if (listener != null) 
               	{
    				IOUtils.copy(is, fos, new IOUtils.CopyListener() 
    				{
    					public void onCopy(int readLen, long copyLen) 
    					{
    						listener.onDownload(readLen, copyLen, fileSize);
    					}
    				});
    			} 
               	else 
               	{
    				IOUtils.copy(is, fos);
    			}
               	is.close();
			}
			// no such file
			else 
			{ 
        	   Log.e("", "没有找到文件或者文件长度为0!");
        	   if (listener != null) 
        	   {
   				   listener.onError(remoteFile);
   			   }
           }
			// 等待服务器的226回应
			if (!ftpClient.completePendingCommand()) 
			{
				throw new Exception("Pending command failed: " + ftpClient.getReplyString());
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			Log.e("", "下载文件异常!");
			if (listener != null) 
			{
				listener.onError(remoteFile);
			}
		} 
		finally 
		{
			IOUtils.closeQuietly(fos);
			try 
			{
				ftpClient.disconnect();
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				Log.e("", "断开与FTP服务器的连接异常!");
				if (listener != null) 
				{
					listener.onError(remoteFile);
				}
			}
		}
	}

	private static InputStream retrieveFileStream(FTPClient ftp, String filePath) throws Exception 
	{
		InputStream is = ftp.retrieveFileStream(filePath);
		int reply = ftp.getReplyCode();
		if (is == null || (!FTPReply.isPositivePreliminary(reply) && !FTPReply.isPositiveCompletion(reply))) 
		{
			throw new Exception(ftp.getReplyString());
		}
		return is;
	}

	private static long getFileSize(FTPClient ftp, String filePath) throws Exception 
	{
		long fileSize = 0;
		FTPFile[] files = ftp.listFiles(filePath);
		if (files.length == 1 && files[0].isFile()) 
		{
			fileSize = files[0].getSize();
		}
		return fileSize;
	}
	
	/**
	 * 断点下载
	 * @param breakPoint 如果为0则重新下载
	 */
	public static void downLoadAPKWithBreakPoint(final String url, final String localPath, final long breakPoint, final OnDownloadListener listener)
	{
		HttpURLConnection 	urlConnection 		= null;
		InputStream 		inputStream 		= null;
		RandomAccessFile	accessFile			= null;
		long 				tempBreakPoint 		= breakPoint;
		long 				totalLength 		= 0;
		File 				file 				= null;

		try
		{
			try
			{
				// 打开HTTP连接
				Log.e(TAG, "下载URL为" + url);
				urlConnection = (HttpURLConnection)new URL(url).openConnection();
				urlConnection.setConnectTimeout(BaseRequest.CONNECT_TIMEOUT);
				urlConnection.setReadTimeout(BaseRequest.HTTP_READ_TIMEOUT);
				if(urlConnection.getResponseCode() != 200)
				{
					String errorStr = "服务端响应错误, 升级失败！";
					if(listener != null)
					{
						listener.onError(errorStr);
					}
					Log.e(TAG, errorStr);
					return;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				String errorStr = "连接升级服务器失败！";
				if(listener != null)
				{
					listener.onError(errorStr);
				}
				Log.e(TAG, errorStr);
				return;
			}
			// 获取文件总大小
			totalLength = urlConnection.getContentLength();
			urlConnection.disconnect();
			urlConnection = null;
			Log.v(TAG, "当前下载文件大小:" + totalLength);
			// 确保该文件目录存在
			file = new File(localPath);
			if(!file.getParentFile().exists())
			{
				file.getParentFile().mkdirs();
			}
			// 如果重新下载则先删除文件
			if(file.exists() && (breakPoint<=0 && totalLength != file.length()))
			{
				Log.e(TAG, "文件是否存在: " + file.exists());
				Log.e(TAG, "文件断点是否为0: " + breakPoint);
				Log.e(TAG, "下载文件大小是否和本地文件大小相同:" + totalLength + "==" + file.length());
				Log.e(TAG, "删除文件");

				file.delete();
				tempBreakPoint = 0;
			}
			// 打败本地文件
			try
			{
				accessFile = new RandomAccessFile(file, "rwd");
				if(totalLength != accessFile.length())
				{
					// 设定文件的大小
					accessFile.setLength(totalLength);
				}
				accessFile.seek(tempBreakPoint);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				String errorStr = "打开本地缓存文件失败, 请检测SD卡是否存在！";
				if(listener != null)
				{
					listener.onError(errorStr);
				}
				Log.e(TAG, errorStr);
				return;
			}
			
			try
			{
				urlConnection = (HttpURLConnection)new URL(url).openConnection();
				urlConnection.setConnectTimeout(BaseRequest.CONNECT_TIMEOUT);
				urlConnection.setReadTimeout(BaseRequest.READ_TIMEOUT);
				urlConnection.setRequestProperty("Range", "bytes=" + tempBreakPoint + "-" + totalLength);
				Log.e(TAG, "设定断点: " + tempBreakPoint);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				String errorStr = "连接升级服务器失败！";
				if(listener != null)
				{
					listener.onError(errorStr);
				}
				Log.e(TAG, errorStr);
				return;
			}

			/*
			SharedPreferences preferences = main_activity.getPreferences(Context.MODE_PRIVATE);
			Editor editor = preferences.edit();
			editor.putBoolean(KEY_ISDOWNLOADSUCCESS, false);
			editor.putString(KEY_LASTVERSION, mUpdataInfo.getVersion());
			editor.commit();
			*/
			
			byte[] buf = new byte[1024*2];
			int len = 0;
			inputStream = urlConnection.getInputStream();
			// 
			while((len = inputStream.read(buf)) > 0)
			{
				accessFile.write(buf, 0, len);
				tempBreakPoint += len;
				Log.e(TAG, "当前下载量 = " + tempBreakPoint + ", 文件总长度: " + totalLength);
				// 
				if(listener != null)
				{
					listener.onDownload(len, tempBreakPoint, totalLength);
				}
				// 下载完成
				if(tempBreakPoint == totalLength)
				{
					break;
				}

				/*
				if(count == 5)
				{
					count = 0;
					// 界面更新
					UIHandle.InitBusiStep("请稍后","正在下载中(单位：KB)...", (int) (tempBreakPoint*100.0f/totalLength));
					android.util.Log.v("当前下载量:", tempBreakPoint+"/"+totalLength);
					editor = preferences.edit();
					editor.putLong(KEY_BREAKPOINT, tempBreakPoint);
					editor.putBoolean(KEY_ISDOWNLOADSUCCESS, false);
					editor.commit();
				}
				*/
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			String errorStr = "下载升级文件数据失败！";
			if(listener != null)
			{
				listener.onError(errorStr);
			}
			Log.e(TAG, errorStr);
		}
		finally
		{
			boolean status = (tempBreakPoint == totalLength);
			Log.e(TAG, "当前保存的断点: " + tempBreakPoint);
			Log.e(TAG, "当前保存的是否成功: " + status);

			/*
			SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
			Editor editor = preferences.edit();
			editor.putLong(KEY_BREAKPOINT, tempBreakPoint);
			editor.putBoolean(KEY_ISDOWNLOADSUCCESS, tempBreakPoint == totalLength ? true:false);
			editor.putString(KEY_LASTVERSION, mUpdataInfo.getVersion());
			editor.commit();
			*/
			
			// 关闭网络输入流
			if(inputStream != null)
			{
				try
				{
					inputStream.close();
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			// 关闭本地文件流
			if(accessFile != null)
			{
				try
				{
					accessFile.close();
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
			// 关闭HTTP连接
			if(urlConnection != null)
			{
				urlConnection.disconnect();
				urlConnection = null;
			}
		}
	}
}
