package com.chinaunicom.custinforegist.kit.util;

import android.os.SystemClock;


public class ButtonUtil
{
	private static long lastClickTime = 0;
	private static long DIFF = 1000;
	private static long DIFFLOGIN = 6000;
	private static int lastButtonId = -1;
	private static long lastToastTime = 0;
	
	public static boolean isFastDoubleClick()
	{
		return isFastDoubleClick(-1, DIFF); 
	}
	
	public static boolean isFastDoubleClick(int buttonId)
	{
		return isFastDoubleClick(buttonId, DIFF);
	}

	public static boolean isFastDoubleClicklogin(int buttonId)
	{
		return isFastDoubleClick(buttonId, DIFFLOGIN);
	}
	
	public static boolean isFastDoubleClick(int buttonId, long diff)
	{
		long curClickTime = SystemClock.elapsedRealtime();
		long timeSpace = Math.abs(curClickTime - lastClickTime);
		
		if (lastButtonId != buttonId && lastClickTime > 0 && timeSpace < (diff / 3)) 
		{
			if(curClickTime - lastToastTime > 1800)
			{
				lastToastTime = curClickTime;
			}
			lastClickTime = curClickTime;
			return true;
		}
		
		if (lastButtonId == buttonId && lastClickTime > 0 && timeSpace < diff)
		{
			if(curClickTime - lastToastTime > 1800)
			{
				lastToastTime = curClickTime;
			}
			lastClickTime = curClickTime;
			return true;
		}
		
		lastClickTime = curClickTime;
		lastButtonId  = buttonId;
		return false;
	}
}
