package com.chinaunicom.custinforegist.kit.util;

import android.os.SystemClock;

public class EventUtil 
{
	private static long lastClickTime;

	public EventUtil() 
	{
	}

	public static boolean isDoubleClick() 
	{
		long l = SystemClock.elapsedRealtime();
		boolean flag;
		if (l - lastClickTime < 2000L) 
		{
			flag = true;
		} 
		else 
		{
			lastClickTime = l;
			flag = false;
		}
		return flag;
	}
}
