package com.chinaunicom.custinforegist.kit.util;

import java.io.File;
import java.io.FileInputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import com.chinaunicom.custinforegist.base.App;

public class ImageUtil 
{
	private static int mAngle = 0;
	private static Context mContext;
	private static Bitmap mImage;
	private static int mImageHeight;
	private static int mImageWidth;
	private static int mMinHeight;
	private static int mMinWidth;
	private static int mProcImageHeight;
	private static int mProcImageWidth;
	private static Bitmap mResizedBitmap;
	private static int mScaleHeight;
	private static int mScaleWidth;
	private static int mViewHeight;
	private static int mViewWidth;

	public ImageUtil(Context context, int i, int j) 
	{
		mContext = context;
		mViewWidth = i;
		mViewHeight = j;
	}

	private static void InitScaleSize() 
	{
		if (mImageWidth * mViewHeight > mImageHeight * mViewWidth) {
			mScaleWidth = mViewWidth;
			mScaleHeight = (mViewWidth * mImageHeight) / mImageWidth;
		} else {
			mScaleHeight = mViewHeight;
			mScaleWidth = (mViewHeight * mImageWidth) / mImageHeight;
		}
		mProcImageWidth = mImageWidth;
		mProcImageHeight = mImageHeight;
		mMinWidth = mScaleWidth;
		mMinHeight = mScaleHeight;
	}

	private static void clipScaleSize() {
		if (mScaleWidth > mProcImageWidth)
			mScaleWidth = mProcImageWidth;
		if (mScaleHeight > mProcImageHeight)
			mScaleHeight = mProcImageHeight;
		if (mScaleWidth < mMinWidth)
			mScaleWidth = mMinWidth;
		if (mScaleHeight < mMinHeight)
			mScaleHeight = mMinHeight;
	}

	private static int computeInitialSampleSize(
			android.graphics.BitmapFactory.Options options, int i, int j) {
		int k;
		int l;
		double d = options.outWidth;
		double d1 = options.outHeight;
		if (j == -1)
			k = 1;
		else
			k = (int) Math.ceil(Math.sqrt((d * d1) / (double) j));
		if (i == -1)
			l = 512;
		else
			l = (int) Math.min(Math.floor(d / (double) i),
					Math.floor(d1 / (double) i));

		if (l >= k) {
			if (j == -1 && i == -1)
				k = 1;
			else if (i != -1)
				k = l;
		}// goto _L2; else goto _L1
		return k;
	}

	public static int computeSampleSize(
			android.graphics.BitmapFactory.Options options, int i, int j) {
		int k;
		int l;
		k = computeInitialSampleSize(options, i, j);
		if (k > 8) {
			l = 8 * ((k + 7) / 8);
		} else {
			l = 1;
		}
		while (l < k) {
			l <<= 1;
		}
		return l;
	}

	public static Bitmap loadImageByName(String s) {
		try {
			File file;
			FileInputStream fileinputstream;
			file = new File(App.getImagePath(s));
			fileinputstream = null;
			FileInputStream fileinputstream1 = new FileInputStream(file);
			fileinputstream = fileinputstream1;
			BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
			options.inJustDecodeBounds = false;
			options.inPreferredConfig = android.graphics.Bitmap.Config.RGB_565;
			options.inPurgeable = true;
			options.inInputShareable = true;
			options.inSampleSize = computeSampleSize(options, -1, 0x40000);
			mImage = BitmapFactory.decodeStream(fileinputstream, null, options);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return mImage;
		// FileNotFoundException filenotfoundexception;
		// filenotfoundexception;
		// filenotfoundexception.printStackTrace();
		// if(true) goto _L2; else goto _L1
		// _L1:
	}

	public static Bitmap loadImageBypath(String s) {
		Bitmap bitmap = null;
		try {
			File file = new File(s);
			FileInputStream fis = new FileInputStream(file);
			android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
			options.inJustDecodeBounds = false;
			options.inPreferredConfig = android.graphics.Bitmap.Config.RGB_565;
			options.inPurgeable = true;
			options.inInputShareable = true;
			//options.inSampleSize = 7;
			mImage = BitmapFactory.decodeStream(fis, null, options);
			mImageWidth = mImage.getWidth();
			mImageHeight = mImage.getHeight();
			InitScaleSize();
			bitmap = mImage;
		} catch (Exception e) {
			mImage = BitmapFactory.decodeResource(mContext.getResources(),0x7f0200aa);
			mImageWidth = mImage.getWidth();
			mImageHeight = mImage.getHeight();
			InitScaleSize();
			bitmap = mImage;
		}
		// _L2:
		return bitmap;
		// Exception exception;
		// exception;
		// _L3:
		// exception.printStackTrace();
		// mImage = BitmapFactory.decodeResource(mContext.getResources(),
		// 0x7f0200aa);
		// mImageWidth = mImage.getWidth();
		// mImageHeight = mImage.getHeight();
		// InitScaleSize();
		// bitmap = mImage;
		// if(true) goto _L2; else goto _L1
		// _L1:
		// exception;
		// goto _L3
	}

	public static Bitmap ratoteLeftWithScale(Bitmap bitmap) {
		if (bitmap != null)
			try {
				mAngle = 90 + mAngle;
				mAngle %= 360;
				swapScaleSize();
				clipScaleSize();
				float f = (float) mScaleWidth / (float) mProcImageWidth;
				float f1 = (float) mScaleHeight / (float) mProcImageHeight;
				Matrix matrix = new Matrix();
				matrix.postScale(f, f1);
				matrix.postRotate(mAngle);
				mResizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, mImageWidth,
						mImageHeight, matrix, true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return mResizedBitmap;
	}

	private static void swapScaleSize() {
		int i = mScaleWidth;
		mScaleWidth = mScaleHeight;
		mScaleHeight = i;
		int j = mProcImageWidth;
		mProcImageWidth = mProcImageHeight;
		mProcImageHeight = j;
		int k = mMinWidth;
		mMinWidth = mMinHeight;
		mMinHeight = k;
	}

	public void safeClearBitmap() {
		if (mImage != null && !mImage.isRecycled()) {
			mImage.recycle();
			mImage = null;
		}
		if (mResizedBitmap != null && !mResizedBitmap.isRecycled()) {
			mResizedBitmap.recycle();
			mResizedBitmap = null;
		}
	}
	
	public static Bitmap scaleBitmap(Bitmap bitmap, float scale) {
		if (scale == 1.0f) {
			return bitmap;
		} else {
			return Bitmap.createScaledBitmap(bitmap,
					(int) (scale * (float) bitmap.getWidth()),
					(int) (scale * (float) bitmap.getHeight()), true);
		}
	}

	public static Bitmap scaleBitmap(String filePath, float scale) {
		Bitmap bitmap = BitmapFactory.decodeFile(filePath);
		if (bitmap != null)
			return scaleBitmap(bitmap, scale);
		return null;
	}
}
