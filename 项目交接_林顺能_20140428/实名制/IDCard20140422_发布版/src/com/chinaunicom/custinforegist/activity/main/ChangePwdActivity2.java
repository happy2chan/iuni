package com.chinaunicom.custinforegist.activity.main;

import com.chinaunicom.custinforegist.R;

import android.view.View;

/*
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
//import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.simico.idcard.R;
import com.simico.idcard.activity.ActivityHelper;
import com.simico.idcard.activity.BaseActivity;
import com.simico.idcard.activity.SimpleTextWatcher;
import com.simico.idcard.api.robospice.AlterAgentPasswdRequest2;
import com.simico.idcard.api.robospice.BaseRequest;
import com.simico.idcard.api.robospice.GetIdentifyIDRequest2;
import com.simico.idcard.base.App;
import com.simico.idcard.kit.util.CheckUtils;
import com.simico.idcard.kit.util.DeviceUtils;
import com.simico.idcard.kit.util.PLog;
import com.simico.idcard.ui.progress.ProgressView;
*/

public class ChangePwdActivity2 extends ChangePwdActivity
{
	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
		case R.id.btn_back:
			finish();
			break;
			
		default:
			super.onClick(v);
			break;
		}
	}
}

/*
@SuppressLint("HandlerLeak")
public class ChangePwdActivity2 extends BaseActivity implements OnClickListener 
{
	private static final int 			MSG_GET_IDENTITY_ID_SUC 	= 0x01;
	private static final int 			MSG_GET_IDENTITY_ID_FAIL 	= 0x02;
	private static final int			MSG_GET_IDENTITY_ID_TIMEOUT = 0x03;
	private static final int 			MSG_CHANGE_PWD_SUC 			= 0x04;
	private static final int 			MSG_CHANGE_PWD_FAIL 		= 0x05;
	private static final int			MSG_CHANGE_PWD_TIMEOUT		= 0x06;
	
	private EditText 					etNo, etTel, etOldPwd, etNewPwd, etNewPwd2, etCode;
	private ImageView 					ivNo, ivTel, ivOldPwd, ivNewPwd, ivNewPwd2, ivCode;
	private ProgressView 				mProgress;
	private TextView 					mTvSurplusSecond;
	private Button 						mBtnCode;
	private Button 						mBtnSubmit;
	private Button 						mBtnBack;
	
	private boolean 					needGetCode = true;
	private String 						communicaID;
	private String 						agentTel;
	private String 						agentNo;
	private String 						oldPwd;
	private String 						newPwd;
	
	private GetIdentifyIDRequest2 		request;
	public AlterAgentPasswdRequest2 	alterRequest;
	public String 						registerKey;
	
	private CountDownTimer timer = new CountDownTimer(60*1000, 1000) 
	{
		@Override
		public void onTick(long millisUntilFinished) 
		{
			needGetCode = false;
			mBtnCode.setEnabled(false);
			findViewById(R.id.ly_code_less).setVisibility(View.VISIBLE);
			mTvSurplusSecond.setText(String.valueOf(millisUntilFinished/1000));
		}

		@Override
		public void onFinish() 
		{
			needGetCode = true;
			mBtnCode.setEnabled(true);
			mBtnSubmit.setEnabled(false);
			findViewById(R.id.ly_code_less).setVisibility(View.GONE);
			App.showAlert(ChangePwdActivity2.this, "请重新获取验证码!", R.string.button_ok, null);
		}
	};

	Handler mHandler = new Handler() 
	{
		public void handleMessage(android.os.Message msg)
		{
			mProgress.setState(ProgressView.STATE_HIDE);
			switch (msg.what)
			{
			// 获取验证码成功
			case MSG_GET_IDENTITY_ID_SUC:
				// 让验证码输入框获得输入焦点
				etCode.requestFocus();
				etCode.setText("");
				// 开始倒计时
				startCountDownTimer();
				mBtnCode.setEnabled(true);
				break;
			
			// 获取验证码失败
			case MSG_GET_IDENTITY_ID_FAIL:
				String desc = (String)msg.obj;
				if(TextUtils.isEmpty(desc)  || "anyType{}".equals(desc)) {
					App.showAlert(ChangePwdActivity2.this, "您本次获取验证码失败, 请重新获取!", R.string.button_ok, null);
				} else {
					App.showAlert(ChangePwdActivity2.this, desc+"", R.string.button_ok, null);
				}
				mBtnCode.setEnabled(true);
				break;
			
			// 获取验证码超时
			case MSG_GET_IDENTITY_ID_TIMEOUT:
				App.showAlert(ChangePwdActivity2.this, "服务请求超时, 请重试!", R.string.button_ok, null);
				mBtnCode.setEnabled(true);
				break;
			
			// 修改密码成功
			case MSG_CHANGE_PWD_SUC:
				App.showAlert(ChangePwdActivity2.this,
						"您的登录密码已修改成功，请您妥善保存修改后的密码!", R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								ActivityHelper.goMain(ChangePwdActivity2.this);
								sendBroadcast(new Intent("close.login"));
								finish();
							}
						});
				break;
				
			// 修改密码失败
			case MSG_CHANGE_PWD_FAIL:
				desc = (String)msg.obj;
				if(TextUtils.isEmpty(desc)  || "anyType{}".equals(desc)) {
					App.showAlert(ChangePwdActivity2.this, "修改密码失败, 请重试!", R.string.button_ok, null);
				} else {
					App.showAlert(ChangePwdActivity2.this, desc+"", R.string.button_ok, null);
				}
				mBtnSubmit.setEnabled(true);
				break;
				
			// 修改密码超时
			case MSG_CHANGE_PWD_TIMEOUT:
				App.showAlert(ChangePwdActivity2.this, "服务请求超时, 请重试!", R.string.button_ok, null);
				mBtnSubmit.setEnabled(true);
				break;
				
			default:
				break;
			}
		}
	};

	// 开始倒计时
	private void startCountDownTimer() 
	{
		timer.cancel();
		timer.start();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		// 初始化控件和布局
		setContentView(R.layout.activity_change_pwd2);
		initViews();
		//
		isNeedCheckTimeout = false;
	}
	
	// 初始化控件
	private void initViews() 
	{
		mProgress = (ProgressView) findViewById(R.id.progress);
		mProgress.setText(R.string.logging);
		
		mTvSurplusSecond = (TextView) findViewById(R.id.tv_surplus_second);

		ivNo 		= (ImageView) findViewById(R.id.iv_tip_no);
		ivTel 		= (ImageView) findViewById(R.id.iv_tip_tel);
		ivOldPwd 	= (ImageView) findViewById(R.id.iv_tip_old_pwd);
		ivNewPwd 	= (ImageView) findViewById(R.id.iv_tip_new_pwd);
		ivNewPwd2 	= (ImageView) findViewById(R.id.iv_tip_new_pwd2);
		ivCode 		= (ImageView) findViewById(R.id.iv_tip_code);

		etNo = (EditText) findViewById(R.id.et_no);
		etNo.addTextChangedListener(new SimpleTextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				super.onTextChanged(s, start, before, count);
				ivNo.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(s)) {
					ivNo.setImageResource(R.drawable.ic_error);
				} else {
					ivNo.setImageResource(R.drawable.ic_correct);
				}
			}
		});

		etTel = (EditText) findViewById(R.id.et_tel);
		etTel.addTextChangedListener(new SimpleTextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				super.onTextChanged(s, start, before, count);
				ivTel.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(s) || s.length() != 11) {
					ivTel.setImageResource(R.drawable.ic_error);
				} else {
					ivTel.setImageResource(R.drawable.ic_correct);
				}
			}
		});

		etOldPwd = (EditText) findViewById(R.id.et_old_pwd);
		etOldPwd.addTextChangedListener(new SimpleTextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				super.onTextChanged(s, start, before, count);
				ivOldPwd.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(s) || !CheckUtils.isCorrentPassword(s.toString())) {
					ivOldPwd.setImageResource(R.drawable.ic_error);
				} else {
					ivOldPwd.setImageResource(R.drawable.ic_correct);
				}
				changeBtnState();
			}
		});

		etNewPwd = (EditText) findViewById(R.id.et_new_pwd);
		etNewPwd.addTextChangedListener(new SimpleTextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				super.onTextChanged(s, start, before, count);
				ivNewPwd.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(s)
						|| !CheckUtils.isCorrentPassword(s.toString())) {
					ivNewPwd.setImageResource(R.drawable.ic_error);
				} else {
					ivNewPwd.setImageResource(R.drawable.ic_correct);
				}
				String new2 = etNewPwd2.getText().toString();
				PLog.log("new:"+s+" new2:"+new2);
				if (!TextUtils.isEmpty(new2) && !TextUtils.isEmpty(s)) {
					if(CheckUtils.isCorrentPassword(new2) && CheckUtils.isCorrentPassword(s.toString()) && new2.equals(s.toString())){
						ivNewPwd2.setImageResource(R.drawable.ic_correct);
					} else {
						ivNewPwd2.setImageResource(R.drawable.ic_error);
					}
				}
				changeBtnState();
			}
		});

		etNewPwd2 = (EditText) findViewById(R.id.et_new_pwd2);
		etNewPwd2.addTextChangedListener(new SimpleTextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				super.onTextChanged(s, start, before, count);
				ivNewPwd2.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(s)
						|| !CheckUtils.isCorrentPassword(s.toString())
						|| !s.toString().equals(etNewPwd.getText().toString())) {
					ivNewPwd2.setImageResource(R.drawable.ic_error);
				} else {
					ivNewPwd2.setImageResource(R.drawable.ic_correct);
				}
				changeBtnState();
			}
		});

		etCode = (EditText) findViewById(R.id.et_auth_code);
		etCode.addTextChangedListener(new SimpleTextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				if (!TextUtils.isEmpty(s) && !needGetCode) {
					ivCode.setVisibility(View.VISIBLE);
					ivCode.setImageResource(R.drawable.ic_correct);
					findViewById(R.id.btn_submit).setEnabled(true);
				} else {
					ivCode.setVisibility(View.INVISIBLE);
				}
				changeBtnState();
			}
		});

		mBtnCode = (Button)findViewById(R.id.btn_get_auth_code);
		mBtnCode.setOnClickListener(this);
		
		mBtnSubmit = (Button)findViewById(R.id.btn_submit);
		mBtnSubmit.setOnClickListener(this);
		
		mBtnBack = (Button)findViewById(R.id.btn_back);
		mBtnBack.setOnClickListener(this);
		
		findViewById(R.id.root).setOnClickListener(hideSoftKeyBoardListener);
		findViewById(R.id.f_1).setOnClickListener(hideSoftKeyBoardListener);
		findViewById(R.id.f_2).setOnClickListener(hideSoftKeyBoardListener);
		findViewById(R.id.ly_auth_code_container).setOnClickListener(hideSoftKeyBoardListener);
		
		changeBtnState();
	}
	
	private void changeBtnState() 
	{
		if(TextUtils.isEmpty(etOldPwd.getText().toString()) ||
		  !CheckUtils.isCorrentPassword(etOldPwd.getText().toString()) ||
		  TextUtils.isEmpty(etNewPwd.getText().toString()) ||
		  !CheckUtils.isCorrentPassword(etNewPwd.getText().toString()) ||
		  TextUtils.isEmpty(etNewPwd2.getText().toString()) ||
		  !etNewPwd.getText().toString().equals(etNewPwd2.getText().toString())	||
		  TextUtils.isEmpty(etCode.getText().toString()) || needGetCode) {
			findViewById(R.id.btn_submit).setEnabled(false);
		} else {
			findViewById(R.id.btn_submit).setEnabled(true);
		}
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		mHandler.removeCallbacksAndMessages(null);
		mHandler = null;
		timer.cancel();
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
			// 获取验证码
			case R.id.btn_get_auth_code:
				handleGetAuthCode();
				break;
				
			// 提交修改密码请求
			case R.id.btn_submit:
				handleSubmit();
				break;
				
			// 返回
			case R.id.btn_back:
				finish();
				break;
				
			default:
			break;
		}
	}
	
	// 获取验证码
	protected void handleGetAuthCode() 
	{
		agentNo = etNo.getText().toString();
		if (TextUtils.isEmpty(agentNo)) 
		{
			etNo.requestFocus();
			ivNo.setVisibility(View.VISIBLE);
			ivNo.setImageResource(R.drawable.ic_error);
			App.showToast("请输入发展人编码!");
			return;
		}

		agentTel = etTel.getText().toString();
		if (TextUtils.isEmpty(agentTel)) 
		{
			etTel.requestFocus();
			ivTel.setVisibility(View.VISIBLE);
			ivTel.setImageResource(R.drawable.ic_error);
			App.showToast("请输入发展人手机号!");
			return;
		}
		
		if (agentTel.length() != 11) 
		{
			etTel.requestFocus();
			ivTel.setVisibility(View.VISIBLE);
			ivTel.setImageResource(R.drawable.ic_error);
			App.showToast("您输入的发展人手机号不正确, 请重新输入!");
			return;
		}
		
		if(!DeviceUtils.hasInternet())
		{
			App.showAlert(ChangePwdActivity2.this, R.string.network_exception, R.string.button_ok, null);
			return;
		}
		
		mProgress.setText(R.string.getting_identify_id);
		mProgress.setState(ProgressView.STATE_LOADING);
		mBtnCode.setEnabled(false);
		
		request = new GetIdentifyIDRequest2(agentNo.trim(), agentTel.trim());
		executeRequest(mHandler, MSG_GET_IDENTITY_ID_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, request, new GetIdentifyIdRequestListener());
		//getSpiceManager().execute(request, new GetIdentifyIdRequestListener());
	}
	
	public final class GetIdentifyIdRequestListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException arg0) 
		{
			// 取消定时器
			cancelTimer();
			//
			if(arg0.getCause() instanceof java.net.SocketTimeoutException) {
				mHandler.obtainMessage(MSG_GET_IDENTITY_ID_FAIL, "服务请求超时, 请重试!").sendToTarget();
			} else {
				mHandler.sendEmptyMessage(MSG_GET_IDENTITY_ID_FAIL);
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try 
			{
				// 取消定时器
				cancelTimer();
				//
				Map<String,String> result = request.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					communicaID = result.get(BaseRequest.COMMUNICATION_ID);
					mHandler.sendEmptyMessage(MSG_GET_IDENTITY_ID_SUC);
				} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_GET_IDENTITY_ID_FAIL, desc).sendToTarget();
				}
			} catch (Exception e) {
				e.printStackTrace();
				mHandler.sendEmptyMessage(MSG_GET_IDENTITY_ID_FAIL);
			}
		}
	}

	// 提交修改密码的请求
	protected void handleSubmit() 
	{
		if (needGetCode) 
		{
			App.showToast("请先获取验证码!");
			mBtnCode.setEnabled(true);
			mBtnSubmit.setEnabled(false);
			return;
		}

		agentNo = etNo.getText().toString();
		if (TextUtils.isEmpty(agentNo)) 
		{
			etNo.requestFocus();
			ivNo.setVisibility(View.VISIBLE);
			ivNo.setImageResource(R.drawable.ic_error);
			App.showToast("请输入发展人编码!");
			return;
		}
		
		oldPwd = etOldPwd.getText().toString();
		if (TextUtils.isEmpty(oldPwd)) 
		{
			etOldPwd.requestFocus();
			ivOldPwd.setVisibility(View.VISIBLE);
			ivOldPwd.setImageResource(R.drawable.ic_error);
			App.showToast("请输入原密码!");
			return;
		}
		if (!CheckUtils.isCorrentPassword(etOldPwd.getText().toString())) 
		{
			App.showToast("原密码位数有误!");
			etOldPwd.requestFocus();
			ivOldPwd.setVisibility(View.VISIBLE);
			ivOldPwd.setImageResource(R.drawable.ic_error);
			return;
		}
		newPwd = etNewPwd.getText().toString();
		if (TextUtils.isEmpty(newPwd)) 
		{
			etNewPwd.requestFocus();
			ivNewPwd.setVisibility(View.VISIBLE);
			ivNewPwd.setImageResource(R.drawable.ic_error);
			App.showToast("请输入新密码!");
			return;
		}

		if (!CheckUtils.isCorrentPassword(etNewPwd.getText().toString())) 
		{
			App.showToast("新密码输入有误!");
			etNewPwd.requestFocus();
			ivNewPwd.setVisibility(View.VISIBLE);
			ivNewPwd.setImageResource(R.drawable.ic_error);
			return;
		}

		String v = etNewPwd2.getText().toString();
		if (TextUtils.isEmpty(v)) 
		{
			etNewPwd2.requestFocus();
			ivNewPwd2.setVisibility(View.VISIBLE);
			ivNewPwd2.setImageResource(R.drawable.ic_error);
			App.showToast("请重复输入新密码!");
			return;
		}

		if (!etNewPwd.getText().toString().equals(etNewPwd2.getText().toString())) 
		{
			App.showToast("您重复输入的新密码不一致, 请重新输入!");
			etNewPwd2.requestFocus();
			ivNewPwd2.setVisibility(View.VISIBLE);
			ivNewPwd2.setImageResource(R.drawable.ic_error);
			return;
		}
		
		String identifyId = etCode.getText().toString();
		if (TextUtils.isEmpty(identifyId)) 
		{
			App.showToast("请输入验证码!");
			etCode.requestFocus();
			return;
		}
		
		if(!DeviceUtils.hasInternet())
		{
			App.showAlert(ChangePwdActivity2.this, R.string.network_exception, R.string.button_ok, null);
			return;
		}

		mProgress.setText("正在修改…");
		mProgress.setState(ProgressView.STATE_LOADING);
		mBtnSubmit.setEnabled(false);
		
		alterRequest = new AlterAgentPasswdRequest2(agentNo,oldPwd,newPwd,identifyId,communicaID);
		executeRequest(mHandler, MSG_CHANGE_PWD_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, alterRequest, new AlterRequestListener());
		//getSpiceManager().execute(alterRequest, new AlterRequestListener());
	}
	
	public final class AlterRequestListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException arg0)
		{
			// 取消定时器
			cancelTimer();
			// 
			if(arg0.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_CHANGE_PWD_FAIL, "服务请求超时, 请重试!").sendToTarget();
			} 
			else
			{
				mHandler.obtainMessage(MSG_CHANGE_PWD_FAIL).sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try 
			{
				// 取消定时器
				cancelTimer();
				// 
				Map<String,String> result = alterRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					registerKey = result.get("registerKey");
					PLog.log("key:"+registerKey);
					App.setAccessToken(registerKey);
					mHandler.sendEmptyMessage(MSG_CHANGE_PWD_SUC);
				} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_CHANGE_PWD_FAIL, desc).sendToTarget();
				}
			} catch (Exception e) {
				e.printStackTrace();
				mHandler.obtainMessage(MSG_CHANGE_PWD_FAIL).sendToTarget();
			}
		}
	}
}
*/
