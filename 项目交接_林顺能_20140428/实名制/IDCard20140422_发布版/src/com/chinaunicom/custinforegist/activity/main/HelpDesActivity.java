package com.chinaunicom.custinforegist.activity.main;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.R;

public class HelpDesActivity extends BaseActivity implements OnClickListener
{
	//private TextView 	mTextTitle;
	private TextView 	mTextInfo;
	private ImageView 	mIvTip;
	
	/*
	private static final String mStrHelp3 = 
			"1）下载安装：\n"
			+ "Android版客户端通过“沃商店”等应用商店，搜索“电子实名客户端”下载安装；iPhone版客户端通过AppStore，搜索“电子实名客户端”下载安装。\n"
			+ "2）版本更新：\n"
			+ "电子实名客户端使用过程中，可以看到新版本发布提示，请及时安装。\n"
			+ "3）卸载：\n"
			+ "Android到手机“设置”->“应用”->“已下载应用”，点击进入”联通实名制”应用信息页，点击卸载。\n"
			+ "iPhone手机长按电子实名客户端图标，出现小红叉后，点击卸载。\n";
	
	private static final String mStrHelp7 = 
			"办理过程出现错误提示后，请按提示要求办理，不能继续销售。\n"
			+"提示中的序号和数字用于区分错误的类型。";

	private static final String mStrHelp8 = 
			"1）无法登录怎么办？\n"
			+ "需要联系本地联通公司确认登录帐号（发展人编码）是否正确，与本人手机号的对应关系是否正确。\n"
			+ "2）激活操作无法收到短信验证码怎么办？\n"
			+ "尝试重新发起激活请求。\n"
			+ "3）扫描出的身份证信息不准确怎么办？\n"
			+ "依赖于手机的拍照能力、环境等，软件不保障完全的准确识别，在提交前请完成资料核对校正。\n"
			+ "4）ICCID无法扫描通过怎么办？\n"
			+ "依赖于手机的拍照能力，如不能识别ICCID信息，需手工录入。\n"
			+ "5）提交信息有错误需要调整怎么办\n"
			+ "提交后将无法通过客户端软件修改，需要到联通营业厅办理资料变更。\n"
			+ "6）系统维护或故障怎么办？\n"
			+ "系统维护或故障期间不能办理，不能销售，请关注系统公告。\n"
			+ "7）遇到其它技术问题？\n"
			+ "建议重启手机或卸载重新安装软件尝试。\n";

	
	private static final String mStrHelp10 = 
			"确保与中国联通签订实名制补充协议；确保客户资料的真实、完整、准确、安全，"
			+ "因代理商所填写的客户资料与客户证件不符或资料登记遗漏等情况引起客户申告或纠纷，" 
			+ "由代理商负责解决，如造成中国联通损失（包括但不限于客户欠费等）的，由代理商全额赔偿中国联通。\n"
			+ "确保客户资料的信息安全，不得泄露、篡改或毁损、交易、非法向他人提供以及通过其他形式不当利用客户资料，"
			+ "不得为任何目的以任何形式将客户资料用于任何本协议约定范围外的用途。";

	private static final String mStrHelp11 = 
			"2013年9月1日 Android版电子实名客户端V1.0 发布 \n"
			+ "2013年9月15日 iPhone版电子实名客户端V1.0 发布  \n"
			+ "中国联通版权所有  \n";
	*/
	
	private static final String mStrHelp3 = 
			"1）无法登录怎么办？\n" +
			"请联系当地联通公司确认： \n" + 
			"（1）登录帐号（发展人编码）是否正确。 \n" + 
			"（2）发展人编码是否和联通手机号码绑定。\n" + 
			"2）我要更换发展人编码关联的手机号码怎么办？\n" + 
			"请联系当地联通公司处理。\n" + 
			"3）我要更换手机，还能使用电子实名客户端吗？\n" +
			"可以通过“激活”功能重新绑定后再登录继续使用。\n" + 
			"4）登录提示“发展人编码与手机绑定关系已变更”怎么办？\n" + 
			"请通过“激活”功能重新绑定后再登录。\n" + 
			"5）首次使用激活时未收到短信验证码怎么办？\n" + 
			"请让短信再飞一会，或退出再次尝试。\n" + 
			"6）登记客户信息出现错误怎么办？\n" + 
			"销售人员在提交返档客户信息前务必对资料进行认真核对，一旦提交无法修改。如特殊原因登记错误，请至联系联通公司协调处理。\n" + 
			"7）系统维护期或故障怎么办？\n" + 
			"系统维护或故障期间不能办理，不能销售，请关注系统公告。\n" + 
			"8）遇到其它技术问题？\n" + 
			"建议重启手机或重新安装应用。\n";
	
	private static final String mStrHelp4 = 
			"2014年1月10日 Android版电子实名客户端V1.2 发布 \n"
			+ "2014年1月10日 iPhone版电子实名客户端V1.2 发布  \n"
			+ "中国联通版权所有  \n";

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// 初始化控件和布局
		setContentView(R.layout.activity_help_des);
		initViews();
		// 初始化显示内容
		initScreen();
	}
	
	// 初始化控件
	private void initViews()
	{
		//mTextTitle = (TextView)findViewById(R.id.text_help_title);
		mTextInfo = (TextView)findViewById(R.id.text_help_text);
		mIvTip	  = (ImageView) findViewById(R.id.iv_tip);
		// 返回按钮
		findViewById(R.id.btn_back).setOnClickListener(this);
	}
	
	// 初始化显示内容
	private void initScreen()
	{
		String type = getIntent().getStringExtra(Interface.HELP_TYPE);
		if(type.equals("01")) 
		{
			mTextInfo.setText(getString(R.string.app_help1) + "\n" + getString(R.string.app_help1_des));
		}
		else if(type.equals("02")) 
		{
			mTextInfo.setText(getString(R.string.app_help2) + "\n" + getString(R.string.app_help2_des));
		}
		else if(type.equals("03")) 
		{
			mTextInfo.setText(getString(R.string.app_help3) + "\n" + mStrHelp3);
		}
		else if(type.equals("04")) 
		{
			mTextInfo.setText(getString(R.string.app_help4) + "\n" + mStrHelp4);
		}
		/*
		else if(type.equals("05")) 
		{
			mTextInfo.setText(getString(R.string.app_help5) + "\n" + getString(R.string.app_help5_des));
		}
		else if(type.equals("06")) 
		{
			mTextInfo.setText(getString(R.string.app_help6) + "\n" + getString(R.string.app_help6_des));
		}
		else if(type.equals("07")) 
		{
			mTextInfo.setText(getString(R.string.app_help7) + "\n" + mStrHelp7);
		}
		else if(type.equals("08")) 
		{
			mTextInfo.setText(getString(R.string.app_help8) + "\n" + mStrHelp8);
		}
		else if(type.equals("09")) 
		{
			mTextInfo.setText(getString(R.string.app_help9) + "\n" + getString(R.string.app_help9_des));
		}
		else if(type.equals("10")) 
		{
			mTextInfo.setText(getString(R.string.app_help10) + "\n" + mStrHelp10);
		}
		else if(type.equals("11")) 
		{
			mTextInfo.setText(getString(R.string.app_help11) + "\n" + mStrHelp11);
		}
		*/
		else if(type.equals("f1")) 
		{
			mTextInfo.setText(getString(R.string.app_help_register_phone));
		}
		else if(type.equals("g1")) 
		{
			mTextInfo.setText(getString(R.string.app_help_register_iccid));
			mIvTip.setBackgroundResource(R.drawable.scan_iccid_tip);
			mIvTip.setVisibility(View.VISIBLE);
		}
		else if(type.equals("f2")) 
		{
			mTextInfo.setText(getString(R.string.app_help_register_2));
		}
		else if(type.equals("f3")) 
		{
			mTextInfo.setText(getString(R.string.app_help_register_3));
			mIvTip.setBackgroundResource(R.drawable.scan_idcard_tip);
			mIvTip.setVisibility(View.VISIBLE);
		}
		else if(type.equals("f4")) 
		{
			mTextInfo.setText(getString(R.string.app_help_register_4));
		}
	}
	
	@Override
	public void onClick(View v)
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch(v.getId())
		{
		case R.id.btn_back:
			finish();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		
		// 调用垃圾回收器
		Interface.causeGC();
	}
}
