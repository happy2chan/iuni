package com.chinaunicom.custinforegist.activity.login;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.R;
import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.base.App;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

// 新特性介绍界面
public class WelcomeActivity extends Activity implements View.OnClickListener
{
	private ViewPager 		mViewPager;
	private List<View>		mListViews = new ArrayList<View>();			// View列表
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_welcome);
		
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	private void initViews() 
	{
		mViewPager = (ViewPager) findViewById(R.id.viewpager_guide);
		
		LayoutInflater inflater = this.getLayoutInflater();
		mListViews.clear();
		mListViews.add(inflater.inflate(R.layout.lay_welcome1, null));
		mListViews.add(inflater.inflate(R.layout.lay_welcome2, null));
		// 
		View view4 = inflater.inflate(R.layout.lay_welcome3, null);
		view4.findViewById(R.id.lay_go).setOnClickListener(this);
		mListViews.add(view4);
		
		PagerAdapter adapter = new MyPagerAdapter();
		mViewPager.setAdapter(adapter);
		mViewPager.setCurrentItem(0x00);
	}
	
	// ViewPager适配器
	private class MyPagerAdapter extends PagerAdapter 
	{
		// 获取总数目
		@Override
		public int getCount() 
		{
			if(mListViews != null) 
			{
				return mListViews.size();
			}
			return 0;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) 
		{
			return arg0==arg1;
		}
		
		// 初始化实例
		@Override 
		public Object instantiateItem(View container, int position) 
		{
			((ViewPager)container).addView(mListViews.get(position));
			return mListViews.get(position);
		}

		@Override 
		public void destroyItem(View container, int position, Object object) 
		{
			((ViewPager)container).removeView(mListViews.get(position));
		}
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) 
		{
		// 立即体验
		case R.id.lay_go:
			// 展示登录阴影界面
			App.setFirstRun(0x01);
			ActivityHelper.goLogin(WelcomeActivity.this, "true");
			finish();
			break;
			
		default:
			break;
		}
	}
}
