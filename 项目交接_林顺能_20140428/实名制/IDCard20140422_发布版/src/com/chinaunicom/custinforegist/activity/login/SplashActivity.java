package com.chinaunicom.custinforegist.activity.login;

import java.io.File;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.FileUtil;
import com.chinaunicom.custinforegist.R;

// 欢迎界面
@SuppressLint("HandlerLeak")
public class SplashActivity extends BaseActivity 
{
	Handler mHandler = new Handler() 
	{
		public void handleMessage(android.os.Message msg) 
		{
			// 如果未注销
			if(App.hasAccessToken()) 
			{
				if(App.getAutoLoginFlag()) 
				{
					ActivityHelper.goMain(SplashActivity.this);
				}
				else
				{
					ActivityHelper.goLogin(SplashActivity.this, "true");
				}
			} 
			// 如果已注销
			else 
			{
				int nStatus = App.isFirstRun();
				// 展示新特性介绍界面
				if(nStatus == 0x00) 
				{
					ActivityHelper.goWelcome(SplashActivity.this);
				}
				else
				{
					ActivityHelper.goLogin(SplashActivity.this, "true");
				}
			}
			finish();
		}
	};
	
	@Override
	protected void onCreate(Bundle bundle) 
	{
		super.onCreate(bundle);
		isNeedCheckTimeout = false;
		setContentView(R.layout.activity_splash);
		clearImages();
		//ActivityHelper x = null;
		//x.notify();
	}
	
	private void clearImages() 
	{
		try 
		{
			FileUtil.deleteDirectory(new File(App.getImagesDir()));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() 
	{
		super.onResume();
		Log.e("SplashActivity", "OnResume...");
		mHandler.sendEmptyMessageDelayed(0, 2*1000);
		/*
		long pauseTime = App.getLastPauseTime();
		Log.e("SplashActivity pauseTime", pauseTime + "  " + System.currentTimeMillis() + "  " + (System.currentTimeMillis() - pauseTime));
		if(pauseTime !=0 && System.currentTimeMillis() - pauseTime >= 1000*60*60*12) 
		{
			Log.e("SplashActivity", "timeout...");
			App.setAccessToken(null);
			//App.showToast("您登陆的代理商客户端系统已超时, 请重新登陆!");
			App.showAlert(SplashActivity.this, R.string.network_exception, R.string.button_ok, null);
		}
		*/
	}
}
