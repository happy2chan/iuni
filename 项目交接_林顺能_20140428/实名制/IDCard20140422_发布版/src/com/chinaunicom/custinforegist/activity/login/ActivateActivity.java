package com.chinaunicom.custinforegist.activity.login;

import java.util.Map;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.SimpleTextWatcher;
import com.chinaunicom.custinforegist.activity.main.ChangePwdActivity;
import com.chinaunicom.custinforegist.api.robospice.AgentActivateRequest;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.chinaunicom.custinforegist.R;

@SuppressLint("HandlerLeak")
public class ActivateActivity extends BaseActivity implements OnClickListener 
{
	/*
	private static final String		TAG									= "ActivateActivity";
	private static final int 		MSG_GET_IDENTITY_ID_SUC 			= 0x01;
	private static final int 		MSG_GET_IDENTITY_ID_FAIL 			= 0x02;
	private static final int		MSG_GET_IDENTIFY_ID_TIMEOUT			= 0x03;
	private static final int 		MSG_ACTIVATE_SUC 					= 0x04;
	private static final int 		MSG_ACTIVATE_FAIL 					= 0x05;
	private static final int		MSG_ACTIVATE_TIMEOUT				= 0x06;
	*/
	private static final int		MSG_GET_LOGIN_PSWD_SUC				= 0x01;
	private static final int		MSG_GET_LOGIN_PSWD_FAIL				= 0x02;
	private static final int		MSG_GET_LOGIN_PSWD_TIMEOUT			= 0x03;

	private Button					mBtnBack;
	private EditText 				mEtAgentNo;
	private ImageView 				mIvTipNo;
	private Button					mBtnGetLoginPswd;

	/*
	private EditText				mEtAgentTel;
	private EditText 				mEtAuthCode;
	private TextView 				mTvSurplusSecond;
	private ImageView 				mIvTipNo, mIvTipTel;
	private Button 					mBtnLogin;
	private Button 					mBtnGetAuthCode;
	private ProgressView 			mProgress;

	private String 					agentTel;
	private String 					identifyId;
	private boolean 				needGetCode = true;

	private GetIdentifyIDRequest 	mRequest;
	*/
	//private String 				mCommunicaID = "FFFF";
	private String					mAgentID;
	private AgentActivateRequest 	mActivateRequest;
	private String					mStrActivateOk;
	
	/*
	// 获取验证码成功倒计时
	private CountDownTimer 			mCountDownTimer = new CountDownTimer(60*1000, 1000) 
	{
		@Override
		public void onTick(long millisUntilFinished) 
		{
			showAuthCodeContainer();
			needGetCode = false;
			mBtnGetAuthCode.setEnabled(false);
			mTvSurplusSecond.setText(String.valueOf(millisUntilFinished/1000));
		}

		@Override
		public void onFinish() 
		{
			hideAuthCodeContainer();
			needGetCode = true;
			mBtnGetAuthCode.setEnabled(true);
			mBtnLogin.setEnabled(false);
			App.showAlert(ActivateActivity.this, "请重新获取验证码!", R.string.button_ok, null);
		}
	};
	*/

	/*
	Handler mHandler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			mProgress.setState(ProgressView.STATE_HIDE);
			switch (msg.what) 
			{
			// 获取验证码成功
			case MSG_GET_IDENTITY_ID_SUC:
				// 让验证码输入框获得焦点
				mEtAuthCode.setText("");
				mEtAuthCode.requestFocus();
				// 启动倒计时
				mBtnGetAuthCode.setEnabled(true);
				startCountDownTimer();
				break;
				
			// 获取校验码失败
			case MSG_GET_IDENTITY_ID_FAIL:
				String desc = (String)msg.obj;
				if(TextUtils.isEmpty(desc)  || "anyType{}".equals(desc)) {
					App.showAlert(ActivateActivity.this, "您本次获取验证码失败，请重新获取!", R.string.button_ok, null);
				} else {
					App.showAlert(ActivateActivity.this, desc+"", R.string.button_ok, null);
				}
				mBtnGetAuthCode.setEnabled(true);
				break;
			
			// 获取验证码超时
			case MSG_GET_IDENTIFY_ID_TIMEOUT:
				App.showAlert(ActivateActivity.this, "服务请求超时, 请重试!", R.string.button_ok, null);
				mBtnGetAuthCode.setEnabled(true);
				break;
			
			// 激活成功
			case MSG_ACTIVATE_SUC:
				// 停止倒计时
				mCountDownTimer.cancel();
				App.setFirstRun(false);
				// 更改用户初始密码
				App.setAgentId(agentId);
				App.showAlert(ActivateActivity.this,
						R.string.change_pwd_message, R.string.change_pwd_ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) 
							{
								Intent intent = ActivityHelper.getChangePwdIntent(ActivateActivity.this);
								intent.putExtra("communicaID", communicaID);
								intent.putExtra("identifyId", identifyId);
								startActivity(intent);
								arg0.dismiss();
								finish();
							}
						});
				break;
			
			// 激活失败
			case MSG_ACTIVATE_FAIL:
				desc = (String)msg.obj;
				if(TextUtils.isEmpty(desc)  || "anyType{}".equals(desc)) {
					App.showAlert(ActivateActivity.this, "激活失败, 请重试!", R.string.button_ok, null);
				} else {
					App.showAlert(ActivateActivity.this, desc+"", R.string.button_ok, null);
				}
				mBtnLogin.setEnabled(true);
				break;

			// 激活超时
			case MSG_ACTIVATE_TIMEOUT:
				App.showAlert(ActivateActivity.this, "服务请求超时, 请重试!", R.string.button_ok, null);
				mBtnLogin.setEnabled(true);
				break;
			
			default:
				break;
			}
		}
	};
	*/
	
	Handler mHandler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			Interface.detachProgressDialog();
			switch (msg.what) 
			{
			// 激活成功
			case MSG_GET_LOGIN_PSWD_SUC:
				App.setFirstRun(0x02);
				// 更改用户初始密码
				App.setAgentId(mAgentID);
				// 
				if(isNull(mStrActivateOk)) 
				{
					mStrActivateOk = "已将登录密码以短信方式发送到您的手机, 请注意查收!";
				}
				App.showAlert(ActivateActivity.this,
					mStrActivateOk, R.string.change_pwd_ok,
					new DialogInterface.OnClickListener() 
					{
						@Override
						public void onClick(DialogInterface arg0, int arg1) 
						{
							Intent intent = new Intent(ActivateActivity.this, ChangePwdActivity.class);
							startActivity(intent);
							arg0.dismiss();
							finish();
						}
					}
				);
				/*
				App.showTransparentAlert(ActivateActivity.this,
					mStrActivateOk, 
					"关闭",
					new View.OnClickListener() 
					{
						@Override
						public void onClick(View v) 
						{
							Intent intent = new Intent(ActivateActivity.this, ChangePwdActivity.class);
							startActivity(intent);
							finish();
						}
					});
				*/
				break;
			
			// 激活失败
			case MSG_GET_LOGIN_PSWD_FAIL:
				String desc = (String)msg.obj;
				if(TextUtils.isEmpty(desc)  || "anyType{}".equals(desc)) {
					App.showAlert(ActivateActivity.this, "激活失败, 请重试!");
				} else {
					App.showAlert(ActivateActivity.this, desc+"");
				}
				break;

			// 激活超时
			case MSG_GET_LOGIN_PSWD_TIMEOUT:
				App.showAlert(ActivateActivity.this, "服务请求超时, 请重试!");
				break;
			
			default:
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle bundle) 
	{
		super.onCreate(bundle);
		// 初始化控件和布局
		setContentView(R.layout.activity_activate);
		initViews();
	}
	
	// 初始化状态
	private void initViews() 
	{
		// 发展人编码校验
		mIvTipNo = (ImageView) findViewById(R.id.iv_tip_no);			// 发展人编码状态提示
		mEtAgentNo = (EditText) findViewById(R.id.et_agent_no);
		mEtAgentNo.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				mIvTipNo.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(s)) {
					mIvTipNo.setImageResource(R.drawable.ic_error);
				} else {
					mIvTipNo.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		
		/*
		// 发展人手机号校验
		mIvTipTel = (ImageView) findViewById(R.id.iv_tip_tel);			// 发展人手机号状态提示
		mEtAgentTel = (EditText) findViewById(R.id.et_agent_tel);
		mEtAgentTel.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				mIvTipTel.setVisibility(View.VISIBLE);
				if (TextUtils.isEmpty(s) || s.length() != 11) {
					mIvTipTel.setImageResource(R.drawable.ic_error);
				} else {
					mIvTipTel.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		// 验证码校验
		mEtAuthCode = (EditText) findViewById(R.id.et_auth_code);
		mEtAuthCode.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				super.onTextChanged(s, start, before, count);
				if (!TextUtils.isEmpty(s) && !needGetCode) {
					mBtnLogin.setEnabled(true);
				} else {
					mBtnLogin.setEnabled(false);
				}
			}
		});
		*/

		/*
		mTvSurplusSecond = (TextView) findViewById(R.id.tv_surplus_second);
		mBtnGetAuthCode = (Button) findViewById(R.id.btn_get_auth_code);
		mBtnGetAuthCode.setOnClickListener(this);
		mProgress = (ProgressView) findViewById(R.id.progress);
		mProgress.setText(R.string.logging);
		*/
		
		// 返回
		mBtnBack = (Button)findViewById(R.id.btn_back);
		mBtnBack.setOnClickListener(this);
		// 获取登陆密码
		mBtnGetLoginPswd = (Button)findViewById(R.id.btn_activate);
		mBtnGetLoginPswd.setOnClickListener(this);

		/*
		findViewById(R.id.root).setOnClickListener(hideSoftKeyBoardListener);
		findViewById(R.id.f_1).setOnClickListener(hideSoftKeyBoardListener);
		findViewById(R.id.ly_auth_code_container).setOnClickListener(hideSoftKeyBoardListener);
		*/
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if (keyCode == KeyEvent.KEYCODE_BACK) 
		{
			//sendBroadcast(new Intent("exit"));
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		// 
		mHandler.removeCallbacksAndMessages(null);
		mHandler = null;
		// 调用垃圾回收器
		Interface.causeGC();
	}

	@Override
	public void onClick(View v) 
	{
		
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch (v.getId()) 
		{
		/*
		// 获取验证码
		case R.id.btn_get_auth_code:
			handleGetAuthCode();
			break;
		*/
		
		// 返回
		case R.id.btn_back:
			finish();
			break;
		
		// 激活
		case R.id.btn_activate:
			handleActivate();
			break;
			
		default:
			break;
		}
	}
	
	/*
	private void showAuthCodeContainer() 
	{
		findViewById(R.id.ly_code_less).setVisibility(View.VISIBLE);
	}
	
	private void hideAuthCodeContainer() 
	{
		findViewById(R.id.ly_code_less).setVisibility(View.GONE);
	}

	// 启动倒计时
	private void startCountDownTimer() 
	{
		mCountDownTimer.cancel();
		mCountDownTimer.start();
	}
	*/

	/*
	// 获取校验码
	private void handleGetAuthCode() 
	{
		agentId = mEtAgentNo.getText().toString();
		if (TextUtils.isEmpty(agentId)) 
		{
			mIvTipNo.setVisibility(View.VISIBLE);
			mIvTipNo.setImageResource(R.drawable.ic_error);
			mEtAgentNo.requestFocus();
			App.showToast("请输入发展人编码!");
			return;
		}
		
		agentTel = mEtAgentTel.getText().toString();
		if (TextUtils.isEmpty(agentTel)) 
		{
			mIvTipTel.setVisibility(View.VISIBLE);
			mIvTipTel.setImageResource(R.drawable.ic_error);
			mEtAgentTel.requestFocus();
			App.showToast("请输入发展人手机号!");
			return;
		}
		
		if (agentTel.length() != 11) 
		{
			mIvTipTel.setVisibility(View.VISIBLE);
			mIvTipTel.setImageResource(R.drawable.ic_error);
			mEtAgentTel.requestFocus();
			App.showToast("您输入的发展人手机号不正确, 请重新输入!");
			return;
		}
		
		if(!DeviceUtils.hasInternet()) 
		{
			App.showAlert(ActivateActivity.this, R.string.network_exception, R.string.button_ok, null);
			return;
		}
		
		mProgress.setText(R.string.getting_identify_id);
		mProgress.setState(ProgressView.STATE_LOADING);
		mBtnGetAuthCode.setEnabled(false);// 使按钮不可操作
		// 请求服务
		mRequest = new GetIdentifyIDRequest(agentId.trim(), agentTel.trim());
		executeRequest(mHandler, MSG_GET_IDENTIFY_ID_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mRequest, new GetIdentifyIdRequestListener());
		//executeRequest(mHandler, 15*1000, null, null);
	}
	
	public final class GetIdentifyIdRequestListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException exception) 
		{
			// 取消定时器
			cancelTimer();
			// 
			if(exception.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_GET_IDENTITY_ID_FAIL, "服务请求超时, 请重试!").sendToTarget();
				Log.e(TAG, "GetIdentifyIdRequest Timeout!");
			}
			else 
			{
				mHandler.sendEmptyMessage(MSG_GET_IDENTITY_ID_FAIL);
				Log.e(TAG, "GetIdentifyIdRequest Fail!");
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			// 取消定时器
			cancelTimer();
			// 
			try {
				Map<String,String> result = mRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					communicaID = result.get(BaseRequest.COMMUNICATION_ID);
					mHandler.sendEmptyMessage(MSG_GET_IDENTITY_ID_SUC);
					Log.e(TAG, "GetIdentifyIdRequest OK!");
				} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					Log.e(TAG, "GetIdentifyIdRequest Fail, ErrDes = " + desc);
					mHandler.obtainMessage(MSG_GET_IDENTITY_ID_FAIL, desc).sendToTarget();
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.sendEmptyMessage(MSG_GET_IDENTITY_ID_FAIL);
				Log.e(TAG, "GetIdentifyIdRequest Fail!!");
			}
		}
	}
	*/

	// 处理代理商激活
	private void handleActivate() 
	{
		mAgentID = mEtAgentNo.getText().toString();
		Log.e("", "mAgentID1 = " + mAgentID);
		if (TextUtils.isEmpty(mAgentID)) 
		{
			mIvTipNo.setVisibility(View.VISIBLE);
			mIvTipNo.setImageResource(R.drawable.ic_error);
			mEtAgentNo.requestFocus();
			App.showAlert(this, "请输入发展人编码！", R.string.ok, null);
			return;
		}
		Log.e("", "mAgentID2 = " + mAgentID);
		
		/*
		if (needGetCode) 
		{
			 App.showToast("请先获取验证码!");
			 mBtnGetAuthCode.setEnabled(true);
			 mBtnLogin.setEnabled(false);
			 return;
		}
		
		identifyId = mEtAuthCode.getText().toString();
		if (TextUtils.isEmpty(identifyId)) 
		{
			App.showToast("请输入验证码!");
			mEtAuthCode.requestFocus();
			return;
		}
		*/
		
		if(!DeviceUtils.hasInternet())
		{
			App.showAlert(ActivateActivity.this, getString(R.string.network_exception));
			return;
		}
		
		Interface.attachProgressDialog(ActivateActivity.this, "激活", "正在激活中...");
		// 请求服务
		Log.e("", "mAgentID3 = " + mAgentID);
		// 旧流程
		//mActivateRequest = new AgentActivateRequest(mCommunicaID, mAgentID, null);
		executeRequest(mHandler, MSG_GET_LOGIN_PSWD_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mActivateRequest, new ActivateRequestListener());
		/*
		new Thread() 
		{
			public void run() 
			{
				SystemClock.sleep(2000);
				mHandler.sendEmptyMessage(MSG_GET_LOGIN_PSWD_SUC);
			}
		}.start();
		*/
	}

	public final class ActivateRequestListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException arg0) 
		{
			// 取消定时器
			cancelTimer();
			// 
			if(arg0.getCause() instanceof java.net.SocketTimeoutException) {
				mHandler.obtainMessage(MSG_GET_LOGIN_PSWD_FAIL, "服务请求超时, 请重试！").sendToTarget();
			} else {
				mHandler.obtainMessage(MSG_GET_LOGIN_PSWD_FAIL).sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			// 取消定时器
			cancelTimer();
			//
			try {
				Map<String,String> result = mActivateRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					// 保存密钥
					String key = result.get(BaseRequest.REGISTER_KEY);
					App.setDesKey(key);
					// 激活描述语
					mStrActivateOk = result.get(BaseRequest.DESCRIPTION);
					Log.e("", "desc: " + mStrActivateOk);
					mHandler.sendEmptyMessage(MSG_GET_LOGIN_PSWD_SUC);
				} else {
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_GET_LOGIN_PSWD_FAIL, desc).sendToTarget();
				}
			} catch (Exception e) {
				e.printStackTrace();
				mHandler.obtainMessage(MSG_GET_LOGIN_PSWD_FAIL).sendToTarget();
			}
		}
	}
}
