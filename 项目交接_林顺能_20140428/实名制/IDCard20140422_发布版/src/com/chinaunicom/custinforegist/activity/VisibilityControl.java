package com.chinaunicom.custinforegist.activity;

public interface VisibilityControl {

	public abstract boolean isVisible();
}
