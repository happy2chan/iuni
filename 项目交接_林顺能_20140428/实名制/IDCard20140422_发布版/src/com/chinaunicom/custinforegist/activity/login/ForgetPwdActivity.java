package com.chinaunicom.custinforegist.activity.login;

import java.util.Map;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.SimpleTextWatcher;
import com.chinaunicom.custinforegist.api.robospice.AlterAgentPasswdRequest2;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.api.robospice.GetIdentifyIDRequest2;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.chinaunicom.custinforegist.kit.util.StringUtil;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.chinaunicom.custinforegist.R;

@SuppressLint("HandlerLeak")
public class ForgetPwdActivity extends BaseActivity implements OnClickListener 
{
	protected static final int 	MSG_GET_IDENTIFY_ID_SUC 		= 0x01;
	protected static final int 	MSG_GET_IDENTIFY_ID_FAIL 		= 0x02;
	protected static final int	MSG_GET_IDENTIFY_ID_TIMEOUT 	= 0x03;

	protected static final int 	MSG_MODIFY_PSWD_SUC 			= 0x04;
	protected static final int 	MSG_MODIFY_PSWD_FAIL 			= 0x05;
	protected static final int 	MSG_MODIFY_PSWD_TIMEOUT 		= 0x06;
	
	protected EditText 			mEtNo, mEtCode, mEtPswd, mEtNewPswd;
	protected ImageView 		mIvNo, mIvCode, mIvPswd, mIvNewPswd;

	protected RelativeLayout	mLayoutRecPerson, mLayoutGetIdentifyID, mLayoutModifyPswd;
	protected TextView			mIvRemainTime;

	protected String 			mStrCommunicaID;
	//private String 			identifyId;
	protected String 			mStrAgentID, mStrIdentifyID, mStrPswd, mStrNewPswd;
	//private String 			mAgentTel;
	//private boolean 			needGetCode = true;
	
	private GetIdentifyIDRequest2 		mGetIdentifyIDRequest;
	private AlterAgentPasswdRequest2 	mAlertAgentPswdRequest;
	
	// 变换布局
	// 是否以藏修改密码的布局
	private void hideLayoutModifyPswd(boolean flag)
	{
		if(flag)
		{
			mLayoutModifyPswd.setVisibility(View.GONE);
			mLayoutRecPerson.setVisibility(View.VISIBLE);
			mLayoutGetIdentifyID.setVisibility(View.VISIBLE);
		}
		else
		{
			mLayoutModifyPswd.setVisibility(View.VISIBLE);
			mLayoutRecPerson.setVisibility(View.GONE);
			mLayoutGetIdentifyID.setVisibility(View.GONE);
		}
	}
	
	private CountDownTimer mCountDownTimer = new CountDownTimer(180*1000, 1000) 
	{
		@Override
		public void onTick(long millisUntilFinished) 
		{
			String tempstr = String.format("剩余%d秒", millisUntilFinished/1000);
			mIvRemainTime.setText(tempstr);
		}
		
		@Override
		public void onFinish() 
		{
			// 隐藏修改密码的布局
			hideLayoutModifyPswd(true);
		}
	};
	
	Handler mHandler = new Handler()
	{
		public void handleMessage(Message msg) 
		{
			Interface.detachProgressDialog();
			switch (msg.what)
			{
			// 获取验证码成功
			case MSG_GET_IDENTIFY_ID_SUC:
				// 显示修改密码的布局
				hideLayoutModifyPswd(false);
				// 验证码输入框获得输入焦点
				mEtCode.requestFocus();
				mEtCode.setText("");
				mEtPswd.setText("");
				mEtNewPswd.setText("");
				// 开启倒计时
				startCountDownTimer();
				// 显示提示信息
				App.showAlert(ForgetPwdActivity.this, (String)msg.obj);
				break;
				
			// 获取验证码失败
			case MSG_GET_IDENTIFY_ID_FAIL:
				String desc = (String)msg.obj;
				if(TextUtils.isEmpty(desc)  || "anyType{}".equals(desc)) 
				{
					App.showAlert(ForgetPwdActivity.this, "您本次获取验证码失败, 请重新获取！");
				} 
				else 
				{
					App.showAlert(ForgetPwdActivity.this, desc+"");
				}
				break;
			
			// 获取验证码超时
			case MSG_GET_IDENTIFY_ID_TIMEOUT:
				App.showAlert(ForgetPwdActivity.this, "服务请求超时, 请重试！");
				break;
			
			// 修改密码成功
			case MSG_MODIFY_PSWD_SUC:
				// 保存发展人编码和密码
				App.setAgentId(mStrAgentID);
				App.setAgentPwd(mStrPswd);
				App.showAlert(ForgetPwdActivity.this,
						"您的登录密码已修改成功，请您妥善保存修改后的密码！", R.string.ok,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) 
							{
								dialog.dismiss();
								ActivityHelper.goLogin(ForgetPwdActivity.this, "true");
								finish();
							}
						});
				break;
				
			// 修改密码失败
			case MSG_MODIFY_PSWD_FAIL:
				String desc2 = (String)msg.obj;
				if(TextUtils.isEmpty(desc2) || "anyType{}".equals(desc2))
				{
					//App.showToast("请求失败, 请重试!");
					App.showAlert(ForgetPwdActivity.this, "请求失败, 请重试！", R.string.ok, null);
				}
				else
				{
					//App.showToast(desc2+"");
					App.showAlert(ForgetPwdActivity.this, desc2+"", R.string.ok, null);
				}
				// 隐藏修改密码的布局
				hideLayoutModifyPswd(true);
				break;
			
			// 修改密码超时
			case MSG_MODIFY_PSWD_TIMEOUT:
				App.showAlert(ForgetPwdActivity.this, "服务请求超时, 请重试！");
				// 隐藏修改密码的布局
				hideLayoutModifyPswd(true);
				break;
				
			default:
				break;
			}
		}
	};
	
	// 开启倒计时
	private void startCountDownTimer() 
	{
		mCountDownTimer.cancel();
		mCountDownTimer.start();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		// 初始化布局和控件
		setContentView(R.layout.activity_getpwd);
		initViews();
	}
	
	// 初始化控件
	protected void initViews() 
	{
		mIvNo = (ImageView)findViewById(R.id.iv_tip_no);
		mEtNo = (EditText) findViewById(R.id.et_agent_no);
		mEtNo.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				if(StringUtil.isEmptyOrNull(s.toString())) 
				{
					mIvNo.setVisibility(View.INVISIBLE);
					return;
				}
				else
				{
					mIvNo.setVisibility(View.VISIBLE);
					if (s.length() < 10) 
					{
						mIvNo.setImageResource(R.drawable.ic_error);
					}
					else
					{
						mIvNo.setImageResource(R.drawable.ic_correct);
					}
				}
			}
		});
		
		mIvCode = (ImageView)findViewById(R.id.iv_tip_identify_id);
		mEtCode = (EditText) findViewById(R.id.et_identify_id);
		mEtCode.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				if(StringUtil.isEmptyOrNull(s.toString())) 
				{
					mIvCode.setVisibility(View.INVISIBLE);
					return;
				}
				mIvCode.setVisibility(View.VISIBLE);
				if(s.length() < 6) 
				{
					mIvCode.setImageResource(R.drawable.ic_error);
				} 
				else 
				{
					mIvCode.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		
		mIvPswd = (ImageView)findViewById(R.id.iv_tip_pswd);
		mEtPswd = (EditText) findViewById(R.id.et_pswd);
		mEtPswd.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				if(StringUtil.isEmptyOrNull(s.toString())) 
				{
					mIvPswd.setVisibility(View.INVISIBLE);
					return;
				}
				mIvPswd.setVisibility(View.VISIBLE);
				if(s.length() < 6) 
				{
					mIvPswd.setImageResource(R.drawable.ic_error);
				} 
				else 
				{
					mIvPswd.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		
		mIvNewPswd = (ImageView)findViewById(R.id.iv_tip_new_pswd);
		mEtNewPswd = (EditText) findViewById(R.id.et_new_pswd);
		mEtNewPswd.addTextChangedListener(new SimpleTextWatcher() 
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) 
			{
				super.onTextChanged(s, start, before, count);
				if(StringUtil.isEmptyOrNull(s.toString())) 
				{
					mIvNewPswd.setVisibility(View.INVISIBLE);
					return;
				}
				mIvNewPswd.setVisibility(View.VISIBLE);
				if(s.length() < 6) 
				{
					mIvNewPswd.setImageResource(R.drawable.ic_error);
				} 
				else 
				{
					mIvNewPswd.setImageResource(R.drawable.ic_correct);
				}
			}
		});
		
		// 发展人布局
		mLayoutRecPerson = (RelativeLayout)findViewById(R.id.layout_rec_person);
		mLayoutRecPerson.setVisibility(View.VISIBLE);
		// 获取验证码布局
		mLayoutGetIdentifyID = (RelativeLayout)findViewById(R.id.layout_get_identify_id);
		mLayoutGetIdentifyID.setVisibility(View.VISIBLE);
		// 修改密码布局
		mLayoutModifyPswd = (RelativeLayout)findViewById(R.id.layout_modify_pswd);
		mLayoutModifyPswd.setVisibility(View.GONE);
		mIvRemainTime = (TextView)findViewById(R.id.text_time);

		// 返回
		findViewById(R.id.btn_back).setOnClickListener(this);
		// 获取验证码
		findViewById(R.id.btn_get_identify_id).setOnClickListener(this);
		// 修改密码
		findViewById(R.id.btn_modify_pswd).setOnClickListener(this);
	}

	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		
		mHandler.removeCallbacksAndMessages(null);
		mHandler = null;
		// 
		mCountDownTimer.cancel();
		// 调用垃圾回收器
		Interface.causeGC();
	}

	@Override
	public void onClick(View v) 
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch (v.getId()) 
		{
		/*
		// 获取验证码
		case R.id.btn_get_auth_code:
			handleGetAuthCode();
			break;
		*/

		// 获取校验码
		case R.id.btn_get_identify_id:
			// 隐藏修改密码的布局
			mLayoutModifyPswd.setVisibility(View.GONE);
			// 取消倒计时
			mCountDownTimer.cancel();
			// 获取校验码
			handleGetIdentifyID();
			break;
			
		// 修改密码
		case R.id.btn_modify_pswd:
			handleModifyPswd();
			break;
		
		// 返回
		case R.id.btn_back:
			finish();
			break;
			
		default:
			break;
		}
	}

	/*
	// 获取验证码
	protected void handleGetAuthCode() 
	{
		agentId = etNo.getText().toString();
		if(TextUtils.isEmpty(agentId)) 
		{
			etNo.requestFocus();
			ivNo.setVisibility(View.VISIBLE);
			ivNo.setImageResource(R.drawable.ic_error);
			App.showToast("请输入发展人编码!");
			return;
		}
		
		agentTel = etTel.getText().toString();
		if(TextUtils.isEmpty(agentTel)) 
		{
			etTel.requestFocus();
			ivTel.setVisibility(View.VISIBLE);
			ivTel.setImageResource(R.drawable.ic_error);
			App.showToast("请输入发展人手机号!");
			return;
		}
		
		if(agentTel.length() != 11) 
		{
			etTel.requestFocus();
			ivTel.setVisibility(View.VISIBLE);
			ivTel.setImageResource(R.drawable.ic_error);
			App.showToast("您输入的发展人手机号不正确，请重新输入!");
			return;
		}
		
		if(!DeviceUtils.hasInternet()) 
		{
			App.showToast(R.string.network_exception);
			return;
		}
		
		mProgress.setText("正在获取验证码...");
		mProgress.setState(ProgressView.STATE_LOADING);
		btnCode.setEnabled(false);
		
		request = new GetIdentifyIDRequest(agentId.trim(), agentTel.trim());
		executeRequest(mHandler, MSG_GET_IDENTIFY_ID_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, request, new GetIdentifyIdRequestListener());
		//getSpiceManager().execute(request, new GetIdentifyIdRequestListener());
	}
	
	public final class GetIdentifyIdRequestListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e) 
		{
			cancelTimer();
			if(e.getCause() instanceof java.net.SocketTimeoutException) {
				mHandler.obtainMessage(MSG_GET_IDENTITY_ID_FAIL, "服务请求超时, 请重试!").sendToTarget();
			} else {
				mHandler.sendEmptyMessage(MSG_GET_IDENTITY_ID_FAIL);
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try {
				cancelTimer();
				Map<String,String> result = request.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					communicaID = result.get(BaseRequest.COMMUNICATION_ID);
					mHandler.sendEmptyMessage(MSG_GET_IDENTITY_ID_SUC);
				} else {
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_GET_IDENTITY_ID_FAIL, desc).sendToTarget();
				}
			} catch (Exception e) {
				e.printStackTrace();
				mHandler.sendEmptyMessage(MSG_GET_IDENTITY_ID_FAIL);
			}
		}
	}
	*/

	// 找回密码
	protected void handleGetIdentifyID() 
	{
		/*
		if(needGetCode) 
		{
			App.showToast("请先获取验证码!");
			btnCode.setEnabled(true);
			btnSubmit.setEnabled(false);
			return;
		}
		identifyId = etCode.getText().toString();
		if (TextUtils.isEmpty(identifyId)) 
		{
			App.showToast("请输入验证码!");
			etCode.requestFocus();
			return;
		}
		*/
		
		mStrAgentID = mEtNo.getText().toString();
		if (isNull(mStrAgentID)) 
		{
			//App.showToast("请输入发展人编码！");
			App.showAlert(this, "请输入发展人编码！", R.string.ok, null);
			mEtNo.requestFocus();
			return;
		}
		// 保存发展人编码
		App.setAgentId(mStrAgentID);
		
		if(!DeviceUtils.hasInternet()) 
		{
			//App.showToast(R.string.network_exception);
			App.showAlert(this, R.string.network_exception, R.string.ok, null);
			return;
		}

		Interface.attachProgressDialog(ForgetPwdActivity.this, "获取校验码", "正在获取校验码...");
		mGetIdentifyIDRequest = new GetIdentifyIDRequest2(mStrAgentID, null);
		executeRequest(mHandler, MSG_GET_IDENTIFY_ID_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mGetIdentifyIDRequest, new GetIdentifyIDListener());
	}

	public final class GetIdentifyIDListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e)
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL, "服务请求超时, 请重试！").sendToTarget();
			} else {
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL).sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try {
				// 取消定时器
				cancelTimer();
				//
				Map<String,String> result = mGetIdentifyIDRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				String desc = result.get(BaseRequest.DESCRIPTION);
				
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					//String key = result.get("registerKey");
					//App.setAccessToken(key);
					mStrCommunicaID = result.get(BaseRequest.COMMUNICATION_ID);
					mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_SUC, desc).sendToTarget();
				} 
				else 
				{
					mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL, desc).sendToTarget();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.obtainMessage(MSG_GET_IDENTIFY_ID_FAIL).sendToTarget();
			}
		}
	}
	
	// 修改密码
	protected void handleModifyPswd()
	{
		// 检测发展人编码
		mStrAgentID = mEtNo.getText().toString();
		if (isNull(mStrAgentID)) 
		{
			//App.showToast("请输入验证码！");
			App.showAlert(this, "请输入发展人编码！");
			return;
		}
		// 检测校验码
		mStrIdentifyID = mEtCode.getText().toString();
		if (isNull(mStrIdentifyID)) 
		{
			//App.showToast("请输入验证码！");
			App.showAlert(this, "请输入验证码！");
			return;
		}
		if (mStrIdentifyID.length() != 6) 
		{
			//App.showToast("请正确输入验证码！");
			App.showAlert(this, "请正确输入验证码！");
			return;
		}
		// 检测密码
		mStrPswd = mEtPswd.getText().toString();
		if (isNull(mStrPswd)) 
		{
			//App.showToast("请输入新密码！");
			App.showAlert(this, "请输入新密码！");
			return;
		}
		if (mStrPswd.length() != 6) 
		{
			//App.showToast("请正确输入新密码！");
			App.showAlert(this, "请正确输入新密码！");
			return;
		}
		// 检测确认密码
		mStrNewPswd = mEtNewPswd.getText().toString();
		if (isNull(mStrNewPswd)) 
		{
			//App.showToast("请输入确认密码！");
			App.showAlert(this, "请输入确认密码！");
			return;
		}
		if (mStrNewPswd.length() != 6) 
		{
			//App.showToast("请正确输入确认密码！");
			App.showAlert(this, "请正确输入确认密码！");
			return;
		}
		// 检测新密码和确认密码的一致性
		if(mStrPswd.equals(mStrNewPswd) == false)
		{
			//App.showToast("新密码与确认密码不一致, 请重新输入！");
			App.showAlert(this, "新密码与确认密码不一致, 请重新输入！");
			return;
		}
		// 
		if(Interface.isSimple(mStrNewPswd))
		{
			App.showAlert(this, "您输入的新密码过于简单，请重新输入！");
			return;
		}
		// 检测网络
		if(!DeviceUtils.hasInternet()) 
		{
			//App.showToast(R.string.network_exception);
			App.showAlert(this, R.string.network_exception, R.string.ok, null);
			return;
		}
		
		// 取消倒计时
		//mCountDownTimer.cancel();
		// 
		Interface.attachProgressDialog(ForgetPwdActivity.this, "修改密码", "正在修改登录密码...");
		mAlertAgentPswdRequest = new AlterAgentPasswdRequest2(mStrAgentID, null, mStrPswd, mStrIdentifyID, mStrCommunicaID);
		executeRequest(mHandler, MSG_MODIFY_PSWD_TIMEOUT, BaseRequest.PROGRESS_TIMEOUT, mAlertAgentPswdRequest, new ModifyPswdListener());
	}
	
	// 修改密码返回
	public final class ModifyPswdListener implements RequestListener<String> 
	{
		@Override
		public void onRequestFailure(SpiceException e)
		{
			// 取消定时器
			cancelTimer();
			// 
			if(e.getCause() instanceof java.net.SocketTimeoutException) 
			{
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL, "服务请求超时, 请重试！").sendToTarget();
			} else {
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL).sendToTarget();
			}
		}

		@Override
		public void onRequestSuccess(String arg0) 
		{
			try {
				// 取消定时器
				cancelTimer();
				//
				Map<String,String> result = mAlertAgentPswdRequest.getResult();
				String tradeState = result.get(BaseRequest.TRADE_STATE);
				if (BaseRequest.TRADE_STATE_OK.equals(tradeState)) 
				{
					mHandler.obtainMessage(MSG_MODIFY_PSWD_SUC).sendToTarget();
				} 
				else 
				{
					String desc = result.get(BaseRequest.DESCRIPTION);
					mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL, desc).sendToTarget();
				}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				mHandler.obtainMessage(MSG_MODIFY_PSWD_FAIL).sendToTarget();
			}
		}
	}
}
