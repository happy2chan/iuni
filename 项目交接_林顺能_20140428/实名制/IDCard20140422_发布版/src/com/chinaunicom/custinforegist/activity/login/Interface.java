package com.chinaunicom.custinforegist.activity.login;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.chinaunicom.custinforegist.R;
import com.chinaunicom.custinforegist.activity.main.NewMsgActivity;
import com.chinaunicom.custinforegist.api.ApiClient;
import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.api.model.Version;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;
import com.chinaunicom.custinforegist.kit.util.StringUtil;
import com.chinaunicom.custinforegist.kit.util.PLog;
import com.chinaunicom.custinforegist.ui.dialog.ProgressDialog;
import com.sunnada.sqlite.MsgDBAdapter;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.text.TextUtils;

public class Interface 
{
	private static ProgressDialog 		mProgress;
	public static final int 			CLICK_TIME 			= 500;
	public static String				mStrMenuFlag		= "11100000";			// 菜单开关
	public static String				mStrCertifyFlag 	= "1000000";			// 证件类型开关
	public static final String			IDCARD_ONLY			= "1000000";			// 仅开放身份证
	public static String				mStrGztFlag			= "0";					// 国政通开关标志
	public static String				mInputType			= "3";					// 
	
	private static final String 		SDCARD_ROOT_PATH 	= Environment.getExternalStorageDirectory().getPath();
	public  static final String 		IMAGE_PATH 			= SDCARD_ROOT_PATH + "/ocr/";
	
	public static final String			HELP_TYPE 			= "help_type";
	public static final String			FIRST_LAUNCH 		= "first_launch";
	public static char[] 				mSymbolChar 		= new char[] {'-', '<', '>', '.', '(', ')', '!', '@', 
																		'#', '$', '%', '^', '&', '*',
																		'+', '/', ',', ';', ':', '[', 
																		']', '{', '}', '|', '、'};

	public static String[] 				mSymbolStr 			= new String[] {"-", "<", ">", ".", "(", ")", 
																		"!", "@", "#", "$", "%", "^", "&", "*",
																		"+", "/", ",", ";", ":", "[", "]", "{", 
																		"}", "|", "、"};
	
	public static boolean				mHasMail			= false;
	public static String				mStrMailTitle		= null;
	public static String				mStrMailText		= null;
	public static int					mNotifyID			= 1;
	
	public void finalize()
	{
		System.out.println("Interface finalize...");
	}
	
	public static String GetSubString(String str)
    {
    	return str.substring(0, str.indexOf(0));
    }
	
	public static void attachProgressDialog(Context context, String titleStr, String contentStr, boolean enable)
	{
		if(mProgress != null) 
		{
			return;
		}

		mProgress = new ProgressDialog(context);
		mProgress.setMax(100);
		mProgress.setTitle(titleStr);
		mProgress.setMessage(contentStr);
		mProgress.setCancelable(enable);
		mProgress.show();
	}

	public static void setProgressDialog(String str) 
	{
		if(mProgress != null) 
		{
			mProgress.setMessage(str);
		}
	}

	public static void detachProgressDialog() 
	{
		if(mProgress != null) 
		{
			mProgress.dismiss();
			mProgress = null;
		}
	}

	public static void attachProgressDialog(Context context, String titleStr, String contentStr)
	{
		attachProgressDialog(context, titleStr, contentStr, false);
	}
	
	public static boolean isSymbol(CharSequence s) 
	{
		if(s.length() > 0)
		{
			char c = s.charAt(s.length() - 1);
			for(int i = 0; i < mSymbolChar.length; i++)
			{
				if(c == mSymbolChar[i])
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isSymbol(String s) 
	{
		if(s.length() > 0)
		{
			try
			{
				String regEx = "[1234567890-`~!@#$%^&*+=|{}':;',\\[\\]<>/?~@#￥%……&*——+|{}【】‘；：”“’。，、？！]";
				Pattern p = Pattern.compile(regEx);
				Matcher m = p.matcher(s);
				if(m.find())
				{
					return true;
				}
				return false;
			}
			catch(Exception e)
			{
				e.printStackTrace();
				return false;
			}
			/*
			String ss = s.substring(s.length() - 1);
			for(int i = 0; i < symbol_string.length; i++)
			{
				if(ss.equals(symbol_string[i]))
				{
					return true;
				}
			}
			*/
		}
		return false;
	}
	
	private static String[] mSimplePwds = new String[] 
	{
		"000000", "111111", "222222", "333333",
		"444444", "555555", "666666", "777777", 
		"888888" ,"999999", "123456", "234567", 
		"345678", "456789", "567890", "012345",
		"987654", "876543", "765432", "654321", 
		"543210"
	};

	public static boolean isSimple(String pwd) 
	{
		for(int i=0; i<mSimplePwds.length; i++)
		{
			if(pwd.equals(mSimplePwds[i]))
			{
				return true;
			}
		}
		return false;
	}
	
	// 调用垃圾回收器
	public static void causeGC()
	{
		try 
		{
			System.gc();
			Thread.sleep(100);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	@SuppressWarnings("deprecation")
	public static void attachProgressDialog2(Context context, String title, String content, final int type)
	{
		m_progress = new ProgressDialog(context);
		m_progress.setMax(100);
		m_progress.incrementProgressBy(1);
		m_progress.setTitle(title);
		m_progress.setMessage(content);
		m_progress.setCancelable(false);
		m_progress.setButton("确定", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int whichButton) {
				if(type == 0x01) {
					mCycleExit = 0x01;
				}else {
					mSemaphore.release();
				}
				detachProgressDialog();
			}
		});
		m_progress.show();
	}
	*/
	
	// 检测FTP升级服务器信息
	public static void checkFtpInfo(Map<String, String> result) 
	{
		try
		{
			// FTP服务器IP
			String strFtpSerIP = result.get("ftpSerIP");
			strFtpSerIP = DES.decryptDES(strFtpSerIP, ApiClient.mEncryptKey);
			ApiClient.FTP_URL = strFtpSerIP;
			// FTP服务器端口
			String strFtpSerPort = result.get("ftpSerPort");
			strFtpSerPort = DES.decryptDES(strFtpSerPort, ApiClient.mEncryptKey);
			ApiClient.FTP_PORT = strFtpSerPort;
			// FTP服务器用户名
			String strFtpSerUser = result.get("ftpSerUser");
			strFtpSerUser = DES.decryptDES(strFtpSerUser, ApiClient.mEncryptKey);
			ApiClient.FTP_USER_NAME = strFtpSerUser;
			// FTP服务器密码
			String strFtpSerPass = result.get("ftpSerPass");
			strFtpSerPass = DES.decryptDES(strFtpSerPass, ApiClient.mEncryptKey);
			ApiClient.FTP_USER_PWD = strFtpSerPass;
			// 上传路径
			String strUpLoadPATH = result.get("upLoadPATH");
			strUpLoadPATH = DES.decryptDES(strUpLoadPATH, ApiClient.mEncryptKey);
			ApiClient.FTP_DIR = strUpLoadPATH;
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	// 检测升级
	public static Version checkUpdate(Map<String, String> result, String tag)  
	{
		try
		{
			// 新版本信息
			String strNewVersionCode = result.get("newVerCode");
			strNewVersionCode = DES.decryptDES(strNewVersionCode, ApiClient.mEncryptKey);
			PLog.v(tag, "新版本信息: " + strNewVersionCode);
			// 需要更新
			if (Double.parseDouble(DeviceUtils.getVersionName()) < Double.parseDouble(strNewVersionCode)) 
			{
				String strNewVerName = result.get("newVerName");
				strNewVerName = DES.decryptDES(strNewVerName, ApiClient.mEncryptKey);
				String strNewVerTitle = result.get("newVerTitle");
				strNewVerTitle = DES.decryptDES(strNewVerTitle, ApiClient.mEncryptKey);
				String strNewVerLog = result.get("newVerLog");
				strNewVerLog = DES.decryptDES(strNewVerLog, ApiClient.mEncryptKey);
				String strUpgradePATH = result.get("upgradePATH");
				strUpgradePATH = DES.decryptDES(strUpgradePATH, ApiClient.mEncryptKey);
				// 
				PLog.v(tag, "newVersionCode = " 	+ strNewVersionCode);
				PLog.v(tag, "newVerName = " 		+ strNewVerName);
				PLog.v(tag, "newVerTitle = " 	+ strNewVerTitle);
				PLog.v(tag, "newVerLog = " 		+ strNewVerLog);
				PLog.v(tag, "ftpSerIP = " 		+ ApiClient.FTP_URL);
				PLog.v(tag, "ftpSerPort = " 		+ ApiClient.FTP_PORT);
				PLog.v(tag, "upgradePATH = " 	+ strUpgradePATH);
				
				Version version = new Version();
				version.setUpdateTitle(strNewVerTitle);
				version.setUpdateLog(strNewVerLog);
				version.setVersionCode(strNewVersionCode);
				version.setVersionName(strNewVerName);
				version.setFtpPath(strUpgradePATH);
				
				return version;
			}
			else
			{
				String strUpgradePATH = result.get("upgradePATH");
				strUpgradePATH = DES.decryptDES(strUpgradePATH, ApiClient.mEncryptKey);
				
				PLog.v(tag, "ftpSerIP = " + ApiClient.FTP_URL);
				PLog.v(tag, "ftpSerPort = " + ApiClient.FTP_PORT);
				PLog.v(tag, "upgradePATH = " + strUpgradePATH);
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	// 检测菜单开关
	public static void checkMenuType(Map<String, String> result) 
	{
		Interface.mStrMenuFlag = result.get(BaseRequest.MENU_FLAG);
		if(TextUtils.isEmpty(Interface.mStrMenuFlag)) 
		{
			Interface.mStrMenuFlag = "11100000";
		}
		PLog.v("", "菜单权限为: " + Interface.mStrMenuFlag);
	}
	
	// 检测证件类型开关
	public static void checkCertType(Map<String, String> result) 
	{
		Interface.mStrCertifyFlag = result.get(BaseRequest.CERTIFY_FLAG);
		if(StringUtil.isEmptyOrNull(Interface.mStrCertifyFlag)) 
		{
			Interface.mStrCertifyFlag = "1111111";
		}
		//Interface.mStrCertifyFlag = "1000000";
		PLog.v("", "证件类型开关为: " + Interface.mStrCertifyFlag);
	}
	
	// 检测公告
	public static void checkMail(Context context, Map<String, String> result, boolean isSendNotify) 
	{
		try 
		{
			String[] titleStr = new String[6];
			String[] msgStr = new String[6];
			int nPos = 0x00;;
			
			/*
			// 解析旧公告
			String noticeFlag = result.get(BaseRequest.NOTICE_FLAG);
			if("1".equals(noticeFlag)) 
			{
				// 公告标题
				titleStr[nPos] = result.get(BaseRequest.NOTICE_TITLE);
				// 公告内容
				msgStr[nPos] = result.get(BaseRequest.NOTICE_MSG);
				// 
				PLog.i("", "旧协议公告标题: " + titleStr[nPos] + ", 公告内容: " + msgStr[nPos]);
				nPos++;
			}
			else 
			{
				// 公告标题
				titleStr[nPos] = "测试消息标题";
				// 公告内容
				msgStr[nPos] = "测试消息内容测试消息内容测试消息内容测试消息内容";
				nPos++;
			}
			*/
			
			// 解析新公告
			String numStr = result.get(BaseRequest.NEW_NOTICE_NUM);
			if(!StringUtil.isEmptyOrNull(numStr)) 
			{
				int nNum = Integer.parseInt(numStr);
				if(nNum > 0) 
				{
					String tempTitle = result.get(BaseRequest.NEW_NOTICE_TITLE);
					String tempMsg = result.get(BaseRequest.NEW_NOTICE_MSG);
					// 
					if(nNum == 1) 
					{
						// 新公告标题
						titleStr[nPos] = tempTitle;
						// 新公告内容
						msgStr[nPos] = tempMsg;
						// 
						PLog.i("", "新协议公告标题: " + titleStr[nPos] + ", 公告内容: " + msgStr[nPos]);
						nPos++;
					}
					else
					{
						if(nNum > 5) 
						{
							nNum = 5;
						}
						// 截取内容
						String[] tempTitles = tempTitle.split("\\|\\|\\|");
						String[] tempMsgs = tempMsg.split("\\|\\|\\|");
						if(tempTitles != null && tempMsgs != null) 
						{
							for(int i=0; i<nNum; i++) 
							{
								// 新公告标题
								titleStr[nPos] = tempTitles[i];
								// 新公告内容
								msgStr[nPos] = tempMsgs[i];
								// 
								PLog.i("", "新协议索引: " + i + ", 公告标题: " + titleStr[nPos] + ", 公告内容: " + msgStr[nPos]);
								nPos++;
							}
						}
					}
				}
			}
			
			// 如果没有邮件
			if(nPos == 0x00) 
			{
				return;
			}
			
			// 更新数据库
			MsgDBAdapter adapter = null;
			try 
			{
				adapter = new MsgDBAdapter(context);
				adapter.open();
				
				for(int i=0; i<nPos; i++) 
				{
					/*
					// 看数据库里面是不是有相同的记录
					if(adapter.fetchDataByTag(titleStr[i]) == null) 
					{
						PLog.e("", "insert msg, title=" + titleStr[i]);
						adapter.insertData(titleStr[i], msgStr[i]);
						// 发送公告通知
						Interface.sendMsgNotify(context, titleStr[i], msgStr[i]);
					}
					else
					{
						PLog.e("", "msg \"" + titleStr[i] + "\" already exists!");
					}
					*/
					PLog.v("", "insert msg, title=" + titleStr[i]);
					long id = adapter.insertData(titleStr[i], msgStr[i], "1");
					PLog.i("", "insert ok, id=" + id);
					// 发送公告通知
					if(isSendNotify) 
					{
						Interface.sendMsgNotify(context, String.valueOf(id), titleStr[i], msgStr[i]);
					}
					else
					{
						Interface.mHasMail = true;
						Interface.mStrMailTitle = titleStr[i];
						Interface.mStrMailText = msgStr[i];
					}
				}
			}
			catch(Exception e) 
			{
				e.printStackTrace();
			}
			finally
			{
				if(adapter != null) 
				{
					adapter.close();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// 发送升级通知
	@SuppressWarnings("deprecation")
	public static void sendUpdateNotify(Context context, String titleStr, String msgStr, Version version) 
	{
		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notice = new Notification(R.drawable.icon_main, "发现新版本！", System.currentTimeMillis());
		notice.flags |= Notification.FLAG_AUTO_CANCEL;
		// 
		Intent intent = new Intent(context, UpdateActivity.class);
		// 弹出升级通知 0x00-不弹出升级提示, 立刻处理升级
		intent.putExtra("updateType", "01");
		intent.putExtra("version", 	version);
		notice.setLatestEventInfo(App.context(), titleStr, msgStr, PendingIntent.getActivity(App.context(), 0, intent, 0));
		try
		{
			manager.cancel(0);
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		try
		{
			manager.notify(0, notice);
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	// 发送公告通知
	@SuppressWarnings("deprecation")
	public static void sendMsgNotify(Context context, String idStr, String titleStr, String msgStr) 
	{
		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notice = new Notification(R.drawable.icon_main, "您有新的公告！", System.currentTimeMillis());
		notice.flags |= Notification.FLAG_AUTO_CANCEL;
		// 
		Intent intent = new Intent(App.context(), NewMsgActivity.class);
		intent.putExtra("id", 		idStr);
		intent.putExtra("title", 	titleStr);
		intent.putExtra("msg", 		msgStr);
		notice.setLatestEventInfo(App.context(), titleStr, msgStr, PendingIntent.getActivity(App.context(), mNotifyID, intent, 0));
		manager.notify(mNotifyID++, notice);
	}

	// 取消通知栏信息
	public static void cancelAllNotify(Context context)
	{
		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		manager.cancelAll();
	}
	
	// 检测输入状态
	public static void checkInputType(Map<String, String> result)
	{
		try
		{
			String inputType = result.get(BaseRequest.INPUT_TYPE);
			PLog.e("", "inputType :" + inputType);
			if(!StringUtil.isEmptyOrNull(inputType))
			{
				mInputType = inputType;
			}
			else
			{
				mInputType = "3";
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
