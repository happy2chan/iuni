package com.chinaunicom.custinforegist.activity.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.ImageView;

import com.chinaunicom.custinforegist.activity.BaseActivity;
import com.chinaunicom.custinforegist.activity.login.Interface;
import com.chinaunicom.custinforegist.base.App;
import com.chinaunicom.custinforegist.kit.util.ButtonUtil;
import com.chinaunicom.custinforegist.kit.util.StringUtil;
import com.chinaunicom.custinforegist.kit.util.PLog;
import com.chinaunicom.custinforegist.R;
import com.sunnada.sqlite.MsgBean;
import com.sunnada.sqlite.MsgDBAdapter;
import com.chinaunicom.custinforegist.ui.view.DragListView;
import com.chinaunicom.custinforegist.ui.view.DragListView.OnRefreshLoadingMoreListener;

// 公告箱
public class MsgActivity extends BaseActivity implements OnClickListener, OnRefreshLoadingMoreListener
{
	private final String				TITLE_STR	= "公告箱";
	private DragListView				mLvMsg;
	
	private int[]						mTotalMsg 	= new int[1];
	private int							mStartIdx;
	private final int					MAX_SIZE 	= 10;
	
	private SimpleAdapter	 			mAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_msg);
		
		// 初始化控件
		initViews();
		// 加载公告
		Interface.attachProgressDialog(MsgActivity.this, TITLE_STR, "正在加载公告箱...");
		new Thread() 
		{
			public void run()
			{
				refreshListView();
				Interface.detachProgressDialog();
			}
		}.start();
	}
	
	// 初始化控件
	private void initViews()
	{
		// 返回按钮
		findViewById(R.id.btn_back).setOnClickListener(this);
		// 列表控件
		initMailView();
	}
	
	// 初始化公告列表控件
	private void initMailView() 
	{
		mLvMsg = (DragListView)findViewById(R.id.lv_msg);
		mLvMsg.setOnItemClickListener(new OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, final int pos, long id) 
			{
				App.showAlert(MsgActivity.this, "请选择操作！", 
						// 删除
						R.string.delete, 
						new DialogInterface.OnClickListener() 
						{
							@Override
							public void onClick(DialogInterface dialog, int arg1) 
							{
								dialog.dismiss();
								// 删除公告并重新刷新
								procDeleteMsg(pos-1);
							}
						}, 
						// 详情
						R.string.detail, 
						new DialogInterface.OnClickListener() 
						{
							@Override
							public void onClick(DialogInterface dialog, int which) 
							{
								dialog.dismiss();
								// 显示详情
								procShowDetail(pos-1);
							}
						});
			}
		});
		mLvMsg.setOnRefreshListener(this);
	}
	
	// 删除公告
	private void procDeleteMsg(final int position) 
	{
		Interface.attachProgressDialog(MsgActivity.this, TITLE_STR, "正在删除公告...");
		// 从数据库里面删除记录
		new Thread() 
		{
			@SuppressWarnings("unchecked")
			public void run() 
			{
				// 删除收件箱
				Map<String, String> map = (Map<String, String>) mAdapter.getItem(position);
				MsgDBAdapter.deleteDataByID(MsgActivity.this, map.get("msg_id"));
				// 刷新
				refreshListView();
				Interface.detachProgressDialog();
			}
		}.start();
	}
	
	// 显示公告详情
	@SuppressWarnings("unchecked")
	private void procShowDetail(final int position) 
	{
		final Map<String, String> map = (Map<String, String>) mAdapter.getItem(position);
		App.showAlert(MsgActivity.this, 
				map.get("msg_title"), 
				map.get("msg_text"), 
				R.string.ok, 
				null, 
				R.string.cancel, 
				null);
		// 更新
		new Thread() 
		{
			public void run() 
			{
				if(map.get("msg_flag").equals("1")) 
				{
					MsgDBAdapter.updateDataByID(MsgActivity.this, map.get("msg_id"), "0");
				}
				// 刷新
				refreshListView();
			}
		}.start();
	}
	
	// 刷新ListView
	private void refreshListView() 
	{
		final List<Map<String, String>> listData = new ArrayList<Map<String, String>>();
		List<MsgBean> listMsg = MsgDBAdapter.getAllData(MsgActivity.this, mStartIdx, MAX_SIZE, mTotalMsg);
		if(listMsg == null) 
		{
			runOnUiThread(new Runnable() 
			{
				public void run() 
				{
					App.showAlert(MsgActivity.this, "公告箱为空！", R.string.ok, new DialogInterface.OnClickListener() 
					{
						@Override
						public void onClick(DialogInterface dialog, int which) 
						{
							dialog.dismiss();
							finish();
						}
					});
				}
			});
		}
		else
		{
			// 清除邮件数据
			for(int i=0; i<listMsg.size(); i++) 
			{
				Map<String, String> map = new HashMap<String, String>();
				// 序号
				map.put("msg_id", listMsg.get(i).getID());
				// 标题
				map.put("msg_title", listMsg.get(i).getTitle());
				// 内容
				map.put("msg_text", listMsg.get(i).getText());
				// 标志
				map.put("msg_flag", listMsg.get(i).getFlag());
				listData.add(map);
			}
			PLog.i("load mail data ok!");
		}
		
		// 显示公告
		runOnUiThread(new Runnable() 
		{
			public void run() 
			{
				mAdapter = new MsgAdapter(MsgActivity.this, 
						listData, 
						R.layout.lay_msg_listitem, 
						new String[] {"msg_title", "msg_text"}, 
						new int[] {R.id.tv_msg_title, R.id.tv_msg_text});
				mLvMsg.setAdapter(mAdapter);
			}
		});
	}
	
	private class MsgAdapter extends SimpleAdapter
	{
		private List<Map<String, String>> mListData;
		
		@SuppressWarnings("unchecked")
		public MsgAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) 
		{
			super(context, data, resource, from, to);
			mListData = (List<Map<String, String>>) data;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) 
		{
			if(convertView == null) 
			{
				convertView = super.getView(position, convertView, parent);
			}
			// 标题
			((TextView) convertView.findViewById(R.id.tv_msg_title)).setText(mListData.get(position).get("msg_title"));
			// 内容
			String strText = StringUtil.getStrByLen(mListData.get(position).get("msg_text"), 30);
			((TextView) convertView.findViewById(R.id.tv_msg_text)).setText(strText);
			// 已读未读标志
			ImageView imageView = (ImageView) convertView.findViewById(R.id.iv_status);
			if("1".equals(mListData.get(position).get("msg_flag"))) 
			{
				imageView.setImageResource(R.drawable.icon_msg_unopen);
			}
			else
			{
				imageView.setImageResource(R.drawable.icon_msg_open);
			}
			return convertView;
		}
	}
	
	// 上拉加载
	@Override
	public void onRefresh() 
	{
		if(mStartIdx <= 0) 
		{
			App.showAlert(MsgActivity.this, "已经是第一页");
			mLvMsg.onRefreshComplete();
		}
		else
		{
			mStartIdx -= 10;
			Interface.attachProgressDialog(MsgActivity.this, TITLE_STR, "正在加载...");
			new Thread() 
			{
				public void run() 
				{
					// 加载公告并刷新
					refreshListView();
					Interface.detachProgressDialog();
					// 
					runOnUiThread(new Runnable()  
					{
						public void run() 
						{
							mLvMsg.onRefreshComplete();
							mLvMsg.onLoadMoreComplete(false);
						}
					});
				}
			}.start();
		}
	}
	
	// 点击加载更多
	@Override
	public void onLoadMore() 
	{
		if(mStartIdx + MAX_SIZE >= mTotalMsg[0]) 
		{
			App.showAlert(MsgActivity.this, "已经是最后一页!");
			mLvMsg.onLoadMoreComplete(true);
		}
		else
		{
			mStartIdx += MAX_SIZE;
			Interface.attachProgressDialog(MsgActivity.this, TITLE_STR, "正在加载...");
			new Thread() 
			{
				public void run() 
				{
					// 加载公告并刷新
					refreshListView();
					Interface.detachProgressDialog();
					// 
					runOnUiThread(new Runnable()  
					{
						public void run() 
						{
							if(mStartIdx + MAX_SIZE >= mTotalMsg[0]) 
							{
								mLvMsg.onLoadMoreComplete(true);
							}
							else
							{
								mLvMsg.onLoadMoreComplete(false);
							}
						}
					});
				}
			}.start();
		}
	}
	
	@Override
	public void onClick(View v) 
	{
		// 防止1秒内快速点击
		if (ButtonUtil.isFastDoubleClick(v.getId(), Interface.CLICK_TIME)) 
		{
			return;
		}
		switch(v.getId())
		{
		case R.id.btn_back:
			finish();
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		
		// 调用垃圾回收器
		Interface.causeGC();
	}
}
