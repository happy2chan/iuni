package com.chinaunicom.custinforegist.ui.dialog;

import com.chinaunicom.custinforegist.R;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

// 升级提示Dialog
public class UpdateDialog extends Dialog implements View.OnClickListener
{
	private TextView 				mTvTitle;
	private TextView 				mTvContent;
	private Button					mBtnCancel;
	private Button					mBtnOk;
	
	private View.OnClickListener	mNegativeListener;
	private View.OnClickListener	mPositiveListener;
	
	public UpdateDialog(Context context) 
	{
		super(context, R.style.transparent_dialog);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		this.setContentView(R.layout.dialog_update);
		this.setCanceledOnTouchOutside(false);
		// 初始化控件
		initViews();
	}
	
	// 初始化控件
	private void initViews() 
	{
		mTvTitle = (TextView) findViewById(R.id.tv_title);
		mTvContent = (TextView) findViewById(R.id.tv_content);
		
		// 取消
		mBtnCancel = (Button) findViewById(R.id.btn_cancel);
		mBtnCancel.setOnClickListener(this);
		// 确定
		mBtnOk = (Button) findViewById(R.id.btn_ok);
		mBtnOk.setOnClickListener(this);
	}
	
	// 设置标题
	public void setTitle(String titleStr) 
	{
		mTvTitle.setText(titleStr);
	}
	
	// 设置内容
	public void setMessage(String msgStr) 
	{
		mTvContent.setText(msgStr);
	}
	
	// 设置取消监听
	public void setNegativeButton(String negativeStr, View.OnClickListener negativeListener) 
	{
		mBtnCancel.setText(negativeStr);
		mNegativeListener = negativeListener;
	}
	
	// 设置确定监听
	public void setPositiveButton(String positiveStr, View.OnClickListener positiveListener) 
	{
		mBtnOk.setText(positiveStr);
		mPositiveListener = positiveListener;
	}
	
	@Override
	public void onClick(View v) 
	{
		dismiss();
		
		if(v == mBtnCancel) 
		{
			if(mNegativeListener != null) 
			{
				mNegativeListener.onClick(v);
			}
		}
		else if(v == mBtnOk) 
		{
			if(mPositiveListener != null) 
			{
				mPositiveListener.onClick(v);
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK) 
		{
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
