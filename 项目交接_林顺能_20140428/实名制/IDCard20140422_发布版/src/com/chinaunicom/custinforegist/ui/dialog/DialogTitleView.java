package com.chinaunicom.custinforegist.ui.dialog;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chinaunicom.custinforegist.R;

public class DialogTitleView extends FrameLayout 
{
	public static final int 	MODE_REGULAR = 0;
	public static final int 	MODE_SMALL = 1;
	
	private LinearLayout 		mLayWell;
	private TextView 			mTvTitle;
	private TextView 			mTvSubTitle;
	
	public DialogTitleView(Context context) 
	{
		super(context);
		init();
	}

	public DialogTitleView(Context context, AttributeSet attributeset) 
	{
		super(context, attributeset);
		init();
	}

	public DialogTitleView(Context context, AttributeSet attributeset, int i) 
	{
		super(context, attributeset, i);
		init();
	}
	
	private void init() 
	{
		inflate(getContext(), R.layout.view_dialog_header, this);
		
		mLayWell = (LinearLayout) findViewById(R.id.lay_well);
		mTvTitle = (TextView) findViewById(R.id.tv_title);
		mTvSubTitle = (TextView) findViewById(R.id.tv_subtitle);
		//mLayDivider = findViewById(R.id.lay_title_divder);
	}
	
	public TextView getTvTitle() 
	{
		return mTvTitle;
	}
	
	public TextView getTvSubTitle() 
	{
		return mTvSubTitle;
	}

//	public static final Button makeButton(Context context, int i) {
//		return makeButton(context, context.getString(i));
//	}
//
//	public static final Button makeButton(Context context, String s) {
//		return makeButton(context, s, (int) context.getResources()
//				.getDimension(R.dimen.button_height));
//	}
//
//	public static final Button makeButton(Context context, String s, int i) {
//		Button button = (Button) ViewHelper.viewById(context, R.layout.view_btn_red);
//		android.view.ViewGroup.LayoutParams layoutparams = button
//				.getLayoutParams();
//		if (layoutparams == null)
//			layoutparams = new android.view.ViewGroup.LayoutParams(-2, i);
//		else
//			layoutparams.height = i;
//		button.setLayoutParams(layoutparams);
//		button.setText(s);
//		return button;
//	}

//	public void addAction(int i,
//			android.view.View.OnClickListener listener) {
//		ImageView imageview = new ImageView(getContext(), null, R.style.dialog_well_button);
//		imageview.setOnClickListener(listener);
//		imageview.setImageResource(i);
//		buttonWell.addView(imageview);
//	}

	public void addAction(View view, android.view.View.OnClickListener onclicklistener) 
	{
		view.setOnClickListener(onclicklistener);
		mLayWell.addView(view);
	}

	public void setMode(int i) 
	{
		int j = (int) getContext().getResources().getDimension(R.dimen.global_dialog_padding);
		if (i == 1) 
		{
			mLayWell.removeAllViews();
			mLayWell.setVisibility(View.GONE);
			mTvTitle.setTextSize(1, 16F);
			j /= 2;
		} 
		else 
		{
			mTvTitle.setTextSize(1, 22F);
		}
		mTvTitle.setPadding(j, j, j, j);
	}
}
