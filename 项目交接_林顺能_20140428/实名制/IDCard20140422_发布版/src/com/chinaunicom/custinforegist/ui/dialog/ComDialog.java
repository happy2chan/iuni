package com.chinaunicom.custinforegist.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.chinaunicom.custinforegist.R;

// 消息提示框
public class ComDialog extends Dialog 
{
	private View 							barDivider;
	private View 							buttonDivider;
	private FrameLayout 					container;
	private View 							content;
	private final int 						contentPadding;
	private DialogTitleView 				mLayHeader;
	private Button 							negativeBt;
	private Button 							positiveBt;
	public DialogInterface.OnClickListener 	listener;
	
	private DialogInterface.OnClickListener dismissClick = new OnClickListener() 
	{
		@Override
		public void onClick(DialogInterface dialog, int which) 
		{
			dialog.dismiss();
		}
	};
	
	public ComDialog(Context context) 
	{
		super(context);
		contentPadding = (int)getContext().getResources().getDimension(R.dimen.global_dialog_padding);
		init(context);
	}
	
	public ComDialog(Context context, int i) 
	{
		super(context, i);
		contentPadding = (int)getContext().getResources().getDimension(R.dimen.global_dialog_padding);
		init(context);
	}
	
	protected ComDialog(Context context, boolean flag, DialogInterface.OnCancelListener listener) 
	{
		super(context, flag, listener);
		contentPadding = (int)getContext().getResources().getDimension(R.dimen.global_dialog_padding);
		init(context);
	}
	
	private void init(Context context) 
	{
		requestWindowFeature(1);
		this.setCanceledOnTouchOutside(false);
		setCancelable(false);
		
		content = LayoutInflater.from(context).inflate(R.layout.dialog_comm, null);
		mLayHeader = (DialogTitleView) content.findViewById(R.id.dialog_header);
		container = (FrameLayout) content.findViewById(R.id.content_container);
		barDivider = content.findViewById(R.id.button_bar_divider);
		buttonDivider = content.findViewById(R.id.button_divder);
		positiveBt = (Button) content.findViewById(R.id.positive_bt);
		negativeBt = (Button) content.findViewById(R.id.negative_bt);
		super.setContentView(content);
	}
	
	// 设置标题
	@Override
	public void setTitle(int i) 
	{
		setTitle((getContext().getResources().getString(i)));
	}
	
	@Override
	public void setTitle(CharSequence title) 
	{
		if (title != null && title.length() > 0) 
		{
			mLayHeader.getTvTitle().setText(title);
			mLayHeader.setVisibility(View.VISIBLE);
		} 
		else 
		{
			mLayHeader.setVisibility(View.GONE);
		}
	}
	
	// 设置副标题
	public void setSubTitle(int i) 
	{
		setSubTitle((getContext().getResources().getString(i)));
	}

	public void setSubTitle(CharSequence charsequence) 
	{
		if (charsequence != null && charsequence.length() > 0) 
		{
			mLayHeader.getTvSubTitle().setText(charsequence);
			mLayHeader.getTvSubTitle().setVisibility(View.VISIBLE);
		} 
		else 
		{
			mLayHeader.getTvSubTitle().setVisibility(View.GONE);
		}
	}
	
	public TextView getTitleTextView() 
	{
		return mLayHeader.getTvTitle();
	}
	
	public void setMessage(int resId) 
	{
		setMessage(getContext().getResources().getString(resId));
	}
	
	public void setMessage(String message) 
	{
		setMessage(Html.fromHtml(message));
	}
	
	@SuppressWarnings("deprecation")
	public void setMessage(Spanned spanned) 
	{
		findViewById(R.id.root).setBackgroundDrawable(null);
		ScrollView scrollview = new ScrollView(getContext());
		scrollview.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT));
		
		TextView textview = new TextView(getContext(), null, R.style.dialog_pinterest_text);
		textview.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
		textview.setPadding(contentPadding, contentPadding, contentPadding, contentPadding);
		textview.setLineSpacing(0.0F, 1.3F);
		textview.setText(spanned);
		scrollview.addView(textview);
		setContent(scrollview, 0);
	}
	
	@Override
	public void setContentView(int i) 
	{
		setContent(null);
	}

	public void setContent(View view) 
	{
		setContent(view, contentPadding);
	}
	
	public void setContent(View view, int i) 
	{
		container.removeAllViews();
		container.setPadding(i, i, i, i);
		container.addView(view);
	}
	
	@Override
	public void setContentView(View view) 
	{
		setContentView(null, null);
	}
	
	@Override
	public void setContentView(View view, android.view.ViewGroup.LayoutParams layoutparams) 
	{
		throw new Error("PinterestDialog: User setContent (View view) instead!");
	}
	
	/*
	private void setItems(int itemsId, AdapterView.OnItemClickListener itemListener) 
	{
		setItems(itemsId, new Drawable[0], itemListener);
	}
	
	private void setItems(int itemsResId, Drawable[] itemIcons, AdapterView.OnItemClickListener itemClicklistener) 
	{
		setItems(getContext().getResources().getStringArray(itemsResId), itemIcons, itemClicklistener, false , 0);
	}
	*/

	public void setSingleChoiceItems(String[] items, int index, OnItemClickListener itemListener) 
	{
		setItems(items, new Drawable[0], itemListener, true , index);
	}
	
	/*
	private void setSingleChoiceItems(int itemsResId, Drawable[] itemIcons, int index, OnItemClickListener itemListener) 
	{
		setItems(getContext().getResources().getStringArray(itemsResId), itemIcons, itemListener, true , index);
	}
	*/
	
	private void setItems(String[] items, Drawable[] itemIcons, AdapterView.OnItemClickListener itemListener, boolean isSingleChoice ,int index) 
	{
		getTitleTextView().setTextColor(getContext().getResources().getColor(android.R.color.black));
		getTitleTextView().setTextSize(22.0f);
		
		ListView listview = new ListView(content.getContext());
		//listview.setBackgroundResource(R.drawable.transparent);
		listview.setCacheColorHint(0);
		//listview.setSelector(R.drawable.transparent);
		listview.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
		
		DialogAdapter adapter = new DialogAdapter(items, itemIcons);
		adapter.setIsSingleChoice(isSingleChoice);
		adapter.setIndex(index);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(itemListener);
		setContent(listview, 0);
	}
	
	public void setNegativeButton(int resId, final DialogInterface.OnClickListener onclicklistener) 
	{
		setNegativeButton(getContext().getString(resId), onclicklistener);
	}
	
	public void setNegativeButton(String text, final DialogInterface.OnClickListener listener) 
	{
		if (!TextUtils.isEmpty(text)) 
		{
			negativeBt.setText(text);
			negativeBt.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View view) 
				{
					if (listener != null)
					{
						listener.onClick(ComDialog.this, 0);
					}
					else
					{
						dismissClick.onClick(ComDialog.this, 0);
					}
				}
			});
			negativeBt.setVisibility(0);
			if (positiveBt.getVisibility() == 0) 
			{
				buttonDivider.setVisibility(0);
			}
		} 
		else 
		{
			negativeBt.setVisibility(8);
			buttonDivider.setVisibility(8);
		}
		if (positiveBt.getVisibility() == 0 || negativeBt.getVisibility() == 0) 
		{
			barDivider.setVisibility(0);
		}
		else
		{
			barDivider.setVisibility(8);
		}
	}
	
	public void setPositiveButton(int resId, DialogInterface.OnClickListener onclicklistener) 
	{
		setPositiveButton(getContext().getString(resId), onclicklistener);
	}

	public void setPositiveButton(String text, final DialogInterface.OnClickListener listener) 
	{
		if (!TextUtils.isEmpty(text)) 
		{
			positiveBt.setText(text);
			positiveBt.setOnClickListener(new View.OnClickListener() 
			{
				@Override
				public void onClick(View view) 
				{
					if (listener != null)
					{
						listener.onClick(ComDialog.this, 0);
					}
					else
					{
						dismissClick.onClick(ComDialog.this, 0);
					}
				}
			});
			positiveBt.setVisibility(0);
			if (negativeBt.getVisibility() == 0)
			{
				buttonDivider.setVisibility(0);
			}
		} 
		else 
		{
			positiveBt.setVisibility(8);
			buttonDivider.setVisibility(8);
		}
		if (positiveBt.getVisibility() == 0 || negativeBt.getVisibility() == 0)
		{
			barDivider.setVisibility(0);
		}
		else
		{
			barDivider.setVisibility(8);
		}
	}
	
	@SuppressWarnings("deprecation")
	public void setType() 
	{
		findViewById(R.id.root).setBackgroundResource(R.drawable.dialog_background_a);
		findViewById(R.id.top).setBackgroundColor(0);
		findViewById(R.id.button_bar).setBackgroundColor(0);
		findViewById(R.id.rl_opt).setBackgroundDrawable(null);
	}
	
	/*
	@Override
	protected void onCreate(Bundle bundle) 
	{
		super.onCreate(bundle);
		if (PDevice.isTablet()) 
		{
			int i = (int) PDevice.dpToPixel(360F);
			if (i < PDevice.getScreenWidth()) 
			{
				WindowManager.LayoutParams params = getWindow().getAttributes();
				params.width = i;
				getWindow().setAttributes(params);
			}
		}
	}
	*/
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK) 
		{
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
