package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;

public class LoginRequest extends BaseRequest 
{
	private String		mStrCommunicationID;
	private String 		mStrAgentID;
	private String 		mStrAgentPswd;
	private String 		mStrLac;
	private String 		mStrCi;
	private String		mStrRandom;
	
	public LoginRequest(String strCommunicationID, String strAgentID, String strAgentPswd, String strLac, String strCi, String strRandom) 
	{
		mStrCommunicationID = strCommunicationID;
		mStrAgentID 		= strAgentID;
		mStrAgentPswd 		= strAgentPswd;
		mStrLac 			= strLac;
		mStrCi 				= strCi;
		mStrRandom 			= strRandom;
	}
	
	@Override
	protected String getMethod() 
	{
		return "clientLoginSecond";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "clientLoginSecondResponse";
	}
	
	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		params.add(new Param(COMMUNICATION_ID, 	mStrCommunicationID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(mStrAgentID, mEncryptKey)));
		params.add(new Param(AGENT_PASSWORD, 	DES.encryptDES(mStrAgentPswd, mEncryptKey)));
		params.add(new Param(CLIENT_TYPE, 		DES.encryptDES("01", mEncryptKey)));
		params.add(new Param(VERSION_CODE, 		DES.encryptDES("1." + DeviceUtils.getVersionCode(), mEncryptKey)));
		params.add(new Param(VERSION_NAME, 		DES.encryptDES(DeviceUtils.getVersionName(), mEncryptKey)));
		params.add(new Param(LAC, 				mStrLac));
		params.add(new Param(CI, 				mStrCi));
		params.add(new Param(IDENTIFY_ID2, 		DES.encryptDES(mStrRandom, mEncryptKey)));
		return params;
	}
}
