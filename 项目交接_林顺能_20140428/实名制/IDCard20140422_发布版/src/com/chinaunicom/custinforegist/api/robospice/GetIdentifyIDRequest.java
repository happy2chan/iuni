package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.kit.util.DeviceUtils;

public class GetIdentifyIDRequest extends BaseRequest 
{
	private String 				mAgentID;
	//private String 			mAgentTel;
	
	public GetIdentifyIDRequest(String agentId, String agentTel) 
	{
		super();
		
		this.mAgentID 		= agentId;
		//this.mAgentTel 	= agentTel;
	}

	@Override
	protected String getMethod() 
	{
		return "getIdentifyID";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "getIdentifyIDResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		params.add(new Param(COMMUNICATION_ID, 	COMMUNICA_ID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(mAgentID, mEncryptKey)));
		params.add(new Param(AGENT_TEL, 		null));
		params.add(new Param(VERSION_CODE, 		"1."+DeviceUtils.getVersionCode()));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		return params;
	}
}
