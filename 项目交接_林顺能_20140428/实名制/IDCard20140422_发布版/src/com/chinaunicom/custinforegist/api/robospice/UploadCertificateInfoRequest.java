package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.chinaunicom.custinforegist.api.DES;

public class UploadCertificateInfoRequest extends BaseRequest 
{
	private String 		mCommunicaID;
	private String 		mAgentId;
	private String 		mDesKey;
	private String 		mTelphone;
	private String 		mStrCertificateName;
	private String 		mStrCertificateNum;
	private String 		mStrCertificateType;
	private String 		mStrCertificateAdd;
	
	private String		mContactName;			// 联系人姓名
	private String		mContactTel;			// 联系人电话
	private String		mContactAddr;			// 联系人通信地址

	public UploadCertificateInfoRequest(String communicaID, String agentId,
			String desKey, String telphone, String certificateName,
			String certificateNum, String certificateType, String certificateAdd,
			String contactName, String contactTel, String contactAddr) 
	{
		super();
		
		this.mCommunicaID 			= communicaID;
		this.mAgentId 				= agentId;
		this.mDesKey 				= desKey;
		this.mTelphone 				= telphone;
		this.mStrCertificateName 	= certificateName;
		this.mStrCertificateNum 	= certificateNum;
		this.mStrCertificateType 	= certificateType;
		this.mStrCertificateAdd 	= certificateAdd;
		
		this.mContactName			= contactName;
		this.mContactTel			= contactTel;
		this.mContactAddr			= contactAddr;
		
		Log.e("", "*** 证件地址为: " + certificateAdd);
	}

	@Override
	protected String getMethod() 
	{
		return "uploadCertificateInfo";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "uploadCertificateInfoResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		
		params.add(new Param(COMMUNICATION_ID, 	mCommunicaID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(mAgentId, 			mDesKey)));
		params.add(new Param(USER_TEL, 			DES.encryptDES(mTelphone, 			mDesKey)));
		params.add(new Param(USER_NAME, 		DES.encryptDES(mStrCertificateName, mDesKey)));
		params.add(new Param(USER_TYPE, 		DES.encryptDES(mStrCertificateType, mDesKey)));
		params.add(new Param(USER_NO, 			DES.encryptDES(mStrCertificateNum, 	mDesKey)));
		params.add(new Param(USER_ADDR, 		DES.encryptDES(mStrCertificateAdd, 	mDesKey)));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		
		params.add(new Param(CONTACT_NAME, 		DES.encryptDES(mContactName, 		mDesKey)));
		params.add(new Param(CONTACT_TEL, 		DES.encryptDES(mContactTel, 		mDesKey)));
		params.add(new Param(CONTACT_ADDR, 		DES.encryptDES(mContactAddr, 		mDesKey)));
		return params;
	}
}
