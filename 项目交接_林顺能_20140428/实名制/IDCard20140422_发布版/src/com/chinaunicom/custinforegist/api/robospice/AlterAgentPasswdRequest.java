package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.api.DES;

public class AlterAgentPasswdRequest extends BaseRequest 
{
	private String 		agentId;
	private String 		oldPasswd;
	private String 		newPasswd;

	public AlterAgentPasswdRequest(String agentId, String oldPasswd, String newPasswd) 
	{
		super();
		this.agentId = agentId;
		this.oldPasswd = oldPasswd;
		this.newPasswd = newPasswd;
	}

	@Override
	protected String getMethod() 
	{
		return "alterAgentPasswd";
	}

	@Override
	protected String getResponseMethod() 
	{
		return "alterAgentPasswdResponse";
	}

	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();

		params.add(new Param(COMMUNICATION_ID, 	COMMUNICA_ID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(agentId, mEncryptKey)));
		params.add(new Param(OLD_PASSWORD, 		DES.encryptDES(oldPasswd, mEncryptKey)));
		params.add(new Param(NEW_PASSWORD, 		DES.encryptDES(newPasswd, mEncryptKey)));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		return params;
	}
}
