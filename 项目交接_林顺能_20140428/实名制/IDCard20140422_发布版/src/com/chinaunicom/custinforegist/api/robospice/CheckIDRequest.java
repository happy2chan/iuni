package com.chinaunicom.custinforegist.api.robospice;

import java.util.ArrayList;
import java.util.List;

import com.chinaunicom.custinforegist.api.DES;
import com.chinaunicom.custinforegist.base.App;

public class CheckIDRequest extends BaseRequest 
{
	private String 			mCommunicaID;
	private String 			mAgentID;
	private String 			mTelephone;
	private String			mUserName;
	private String			mCustType;
	private String			mUserNo;

	public CheckIDRequest(String communicaID, String agentID, String telephone, String userName, String custType, String userNo) 
	{
		super();
		
		this.mCommunicaID 	= communicaID;
		this.mAgentID 		= agentID;
		this.mTelephone 	= telephone;
		this.mUserName		= userName;
		this.mCustType		= custType;
		this.mUserNo		= userNo;
	}
	
	@Override
	protected String getMethod() 
	{
		return "callGztInterface";
	}
	
	@Override
	protected String getResponseMethod() 
	{
		return "callGztInterfaceResponse";
	}
	
	@Override
	protected List<Param> getParams() 
	{
		List<Param> params = new ArrayList<Param>();
		
		params.add(new Param(COMMUNICATION_ID, 	mCommunicaID));
		params.add(new Param(AGENT_ID, 			DES.encryptDES(mAgentID, 	App.getDesKey())));
		params.add(new Param(USER_TEL2, 		DES.encryptDES(mTelephone, 	App.getDesKey())));
		params.add(new Param(USER_NAME, 		DES.encryptDES(mUserName, 	App.getDesKey())));
		params.add(new Param(USER_TYPE, 		DES.encryptDES(mCustType, 	App.getDesKey())));
		params.add(new Param(USER_NO, 			DES.encryptDES(mUserNo, 	App.getDesKey())));
		params.add(new Param(CLIENT_TYPE, 		mClientType));
		return params;
	}
}
