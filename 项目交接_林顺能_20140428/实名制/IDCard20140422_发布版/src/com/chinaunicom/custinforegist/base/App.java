package com.chinaunicom.custinforegist.base;

import java.io.File;
import java.util.Locale;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Environment;
import android.os.IBinder;
import android.os.StatFs;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.chinaunicom.custinforegist.activity.ActivityHelper;
import com.chinaunicom.custinforegist.api.Product;
import com.chinaunicom.custinforegist.api.robospice.BaseRequest;
import com.chinaunicom.custinforegist.crash.CrashHandler;
import com.chinaunicom.custinforegist.kit.util.StringUtil;
import com.chinaunicom.custinforegist.service.HeartbeatService;
import com.chinaunicom.custinforegist.service.SampleSpiceService;
import com.chinaunicom.custinforegist.ui.dialog.ComDialog;
import com.chinaunicom.custinforegist.ui.dialog.MessageboxDialog;
import com.chinaunicom.custinforegist.R;
import com.octo.android.robospice.SpiceManager;
import com.ym.idcard.reg.PreferencesBCR;

public class App extends Application 
{
	// 福州测试环境
	//private static final String 	SERVER_ADDR = "http://220.249.190.132:23515";
	// 福州开发环境
	//private static final String 	SERVER_ADDR = "http://220.249.190.132:13515";
	// 现网环境
	private static final String 	SERVER_ADDR = "http://123.125.96.6:8090";
	
	protected static String 		TAG = App.class.getSimpleName();
	private static String 			APP_PACKAGE_NAME 						= "scan.bcr";
	private static String 			APP_PATH 								= "/data/data/scan.bcr";
	public static String 			FIELD_DIR 								= "/data/data/scan.bcr/tmp";
	public static final String 		FSPACE 									= "  ";
	public static final String 		ICON_FOLDER 							= "/icons";
	public static final int 		ICON_HEIGHT 							= 60;
	public static final String 		ICON_PREFIX 							= "/icon_";
	public static final int 		ICON_WIDTH 								= 80;
	public static final String 		IMAGE_FOLDER 							= "/images";
	public static final String 		SDCARD_ROOT_PATH 						= Environment.getExternalStorageDirectory().getPath();
	public static final String 		SDCARD_BASE_PATH 						= SDCARD_ROOT_PATH + "/idcard";
	public static final String 		SPACE 									= " ";
	public static final String 		TAB 									= "    ";
	
	private static final String 	AGENT_ID 								= "key_agent_id";
	private static final String 	AGENT_PWD								= "key_agent_pwd";
	private static final String 	ACCESS_TOKEN 							= "key_access_token";
	// 为了展示新特性介绍界面改了KEY_FIRST
	private static final String 	KEY_FIRST 								= "key_first_run_2";
	private static final String 	AGENT_NAME 								= "key_agent_name";
	// 自动登录标志
	private static final String		AUTO_LOGIN								= "key_auto_login";
	// 加密密码
	private static final String		DES_KEY									= "des_key";
	private static final String		ACCESS_ENV								= "key_access_env";
	
	private static Context 			context;
	private static Resources 		res;
	
	private static SpiceManager 	mSpiceManager 							= new SpiceManager(SampleSpiceService.class);
	//private static Semaphore		mSemSend								= new Semaphore(1);
	
	private static HeartbeatService	mHeartbeatService;
	
	public App() 
	{
		
	}
	
	// 获取SpiceManager
	public static SpiceManager getSpiceManager() 
	{
		return mSpiceManager;
	}
	
	/*
	// 获取发送许可
	public static boolean trySendAcquire(long nTimeout) 
	{
		try
		{
			if(mSemSend.tryAcquire(nTimeout, TimeUnit.MILLISECONDS)) 
			{
				while(mSemSend.tryAcquire());
				return true;
			}
		}
		catch(Exception e) 
		{
			e.printStackTrace();
			mSemSend = null;
			mSemSend = new Semaphore(1);
		}
		return false;
	}
	
	// 释放发送许可
	public static void releaseSendAcquire() 
	{
		mSemSend.release();
	}
	*/
	
	private ServiceConnection mServiceConnection = new ServiceConnection() 
	{
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) 
		{
			mHeartbeatService = ((HeartbeatService.HeartbeatBinder)service).getHeartbeatService();
		}
		
		@Override
		public void onServiceDisconnected(ComponentName name) 
		{
			mHeartbeatService = null;
		}
	};
	
	// 复位心跳定时器
	public static void resetHeartbeatTimer() 
	{
		if(mHeartbeatService != null) 
		{
			mHeartbeatService.resetHeartbeatTimer();
		}
	}

	public static DisplayMetrics getDisplayMetrics() 
	{
		DisplayMetrics displaymetrics = new DisplayMetrics();
		((WindowManager) App.context().getSystemService("window")).getDefaultDisplay().getMetrics(displaymetrics);
		return displaymetrics;
	}
	
	public static float dpToPixel(float dp) 
	{
		return dp * (getDisplayMetrics().densityDpi / 160F);
	}
	
	public static String getAppLibPath() 
	{
		return APP_PATH + "/lib";
	}

	public static String getAppPackageName() 
	{
		return APP_PACKAGE_NAME;
	}

	public static String getAppPath() 
	{
		return APP_PATH;
	}

	public static String getBaseDir()
	{
		return SDCARD_BASE_PATH;
	}

	public static long getExternalMemoryAvailableSize()
	{
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) 
		{
			StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
			return (long) statfs.getBlockSize() * (long) statfs.getAvailableBlocks();
		} 
		else 
		{
			return -1L;
		}
	}

	public static long getExternalMemoryTotalSize() 
	{
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) 
		{
			StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
			return (long) statfs.getBlockSize() * (long) statfs.getBlockCount();
		} 
		else 
		{
			return -1L;
		}
	}

	public static String getImagePath(String imgName) 
	{
		return SDCARD_BASE_PATH+"/images"+File.separator+imgName;
	}
	
	public static String getImagesDir() 
	{
		return SDCARD_BASE_PATH+"/.images";
	}
	
	private void initDir() 
	{
		File file = new File(getImagesDir());
		if (!file.exists())
		{
			file.mkdirs();
		}
		File file4 = new File(FIELD_DIR);
		if (!file4.exists()) 
		{
			file4.mkdir();
		}
	}
	
	public static void updateUiLang(Context context) 
	{
		Configuration config = context.getResources().getConfiguration();
		switch (PreferencesBCR.getUiLanguage(context))
		{
		case 1:
			config.locale = Locale.ENGLISH;
			break;
			
		case 2:
			config.locale = Locale.JAPANESE;
			break;
			
		case 3:
			config.locale = Locale.SIMPLIFIED_CHINESE;
			break;
			
		case 4:
			config.locale = Locale.TRADITIONAL_CHINESE;
			break;
			
		default:
			config.locale = Locale.SIMPLIFIED_CHINESE;
			PreferencesBCR.saveUiLanguage(context, 3);
			break;
		}
		context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
	}
	
	public void onConfigurationChanged(Configuration configuration)
	{
		super.onConfigurationChanged(configuration);
	}

	public void onCreate() 
	{
		super.onCreate();
		
		context = getApplicationContext();
		res = getResources();
		APP_PACKAGE_NAME = getPackageName();
		APP_PATH = "/data/data/" + APP_PACKAGE_NAME;
		FIELD_DIR = getFilesDir() + "/tmp";
		Product.init(APP_PACKAGE_NAME);
		// Setting.CONFIG_NAME = Product.getCfgName(APP_PACKAGE_NAME);
		// Register.init(getBaseContext());
		initDir();
		
		CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(getApplicationContext());
		
		mSpiceManager.start(this);
		// 绑定心跳服务
		Intent intent = new Intent(context, HeartbeatService.class);
		bindService(intent, mServiceConnection, Service.BIND_AUTO_CREATE);
	}
	
	public void onLowMemory() 
	{
		super.onLowMemory();
	}
	
	public void onTerminate() 
	{
		super.onTerminate();
		
		mSpiceManager.shouldStop();
		// 解绑心跳服务
		unbindService(mServiceConnection);
	}
	
	// 是否已经登录
	public static boolean hasAccessToken() 
	{
		String token = getAccessToken();
		if (StringUtil.isEmptyOrNull(token)) 
		{
			return false;
		}
		//return token.equals("login-success");
		return true;
	}
	
	public static String getAccessToken() 
	{
		return getPreferences().getString(ACCESS_TOKEN, null);
	}
	
	public static void setAccessToken(String token) 
	{
		Editor ed = getPreferences().edit();
		ed.putString(ACCESS_TOKEN, token);
		ed.commit();
	}
	
	// 获取加密key
	public static String getDesKey()
	{
		return getPreferences().getString(DES_KEY, BaseRequest.mEncryptKey);
	}
	
	// 设置加密key
	public static void setDesKey(String key)
	{
		Editor ed = getPreferences().edit();
		ed.putString(DES_KEY, key);
		ed.commit();
	}
	
	// 设置是否自动登录
	public static boolean getAutoLoginFlag() 
	{
		boolean flag = getPreferences().getBoolean(AUTO_LOGIN, false);
		//Log.v("", "自动登录标志位: " + flag);
		return flag;
	}
	
	// 设置是否自动登录
	public static void setAutoLoginFlag(boolean flag)
	{
		Editor ed = getPreferences().edit();
		ed.putBoolean(AUTO_LOGIN, flag);
		ed.commit();
		//Log.v("", "设置自动登录标志位为: " + flag);
	}

	public static long getLastPauseTime() 
	{
		return getPreferences().getLong("PAUSE_TIME", 0);
	}
	
	public static void setLastPauseTime(long time) 
	{
		Editor ed = getPreferences().edit();
		ed.putLong("PAUSE_TIME", time);
		ed.commit();
	}
	
	// APP是否第一次运行 0x00-展示新特性介绍界面 0x01-展示登录阴影界面 0x02-正常登陆界面
	public static int isFirstRun() 
	{
		return getPreferences().getInt(KEY_FIRST, 0x00);
	}

	// 设置APP的运行状态
	public static void setFirstRun(int status) 
	{
		Editor ed = getPreferences().edit();
		ed.putInt(KEY_FIRST, status);
		ed.commit();
	}
	
	public static String getAgentId() 
	{
		return getPreferences().getString(AGENT_ID, null);
	}
	
	public static void setAgentId(String agentId)
	{
		Editor ed = getPreferences().edit();
		ed.putString(AGENT_ID, agentId);
		ed.commit();
	}
	
	public static String getAgentPwd()
	{
		return getPreferences().getString(AGENT_PWD, null);
	}
	
	public static void setAgentPwd(String agentPwd)
	{
		Editor ed = getPreferences().edit();
		ed.putString(AGENT_PWD, agentPwd);
		ed.commit();
	}
	
	public static String getAgentName() 
	{
		return getPreferences().getString(AGENT_NAME, null);
	}
	
	public static void setAgentName(String agentName)
	{
		Editor ed = getPreferences().edit();
		ed.putString(AGENT_NAME, agentName);
		ed.commit();
	}
	
	public static String getAccessEnv() 
	{
		return getPreferences().getString(ACCESS_ENV, SERVER_ADDR);
	}
	
	public static void setAccessEnv(String strEnv) 
	{
		Editor ed = getPreferences().edit();
		ed.putString(ACCESS_ENV, strEnv);
		ed.commit();
	}
	
	public static SharedPreferences getPreferences() 
	{
		return context().getSharedPreferences("idcard", 0);
	}

	public static Context context() 
	{
		return context;
	}

	public static Resources resources() 
	{
		return res;
	}
	
	public static void showToast(int message) 
	{
		Toast.makeText(context(), message, Toast.LENGTH_SHORT).show();
	}

	public static void showToast(String message) 
	{
		Toast.makeText(context(), message, Toast.LENGTH_SHORT).show();
	}
	
	public static void saveDisplaySize(Activity activity) 
	{
		DisplayMetrics displaymetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		SharedPreferences.Editor editor = getPreferences().edit();
		editor.putInt("screen_width", displaymetrics.widthPixels);
		editor.putInt("screen_height", displaymetrics.heightPixels);
		editor.putFloat("density", displaymetrics.density);
		editor.commit();
	}
	
	public static int[] getDisplaySize(Context context) 
	{
		SharedPreferences sp = getPreferences();
		return new int[] { sp.getInt("screen_width", 480), sp.getInt("screen_height", 854)};
	}
	
	// 显示透明对话框
	public static void showTransparentAlert(Activity activity, String message, String okStr, View.OnClickListener okListener)
	{
		MessageboxDialog messageboxDialog = new MessageboxDialog(activity, R.style.transparent_dialog);
		messageboxDialog.setMessage(message);
		messageboxDialog.setPositiveListener(okStr, okListener);
		messageboxDialog.show();
	}
	
	// 显示提示框
	public static void showAlert(Context context, int message, int positive, DialogInterface.OnClickListener positiveListener) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible()) 
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			//dialog.setTitle(title);
			dialog.setPositiveButton(positive, positiveListener);
			//dialog.setNegativeButton(activity.getString(R.string.cancel), null);
			dialog.setMessage(message);
			dialog.setCancelable(false);
			dialog.show();
		}
	}
	
	public static void showAlert(Context context, int title, int message, int positive, DialogInterface.OnClickListener positiveListener) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible()) 
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			dialog.setTitle(title);
			dialog.setPositiveButton(positive, positiveListener);
			dialog.setNegativeButton(R.string.cancel, null);
			dialog.setMessage(message);
			dialog.setCancelable(true);
			dialog.show();
		}
	}

	public static void showAlert(Context context, String title, String message, int positive, DialogInterface.OnClickListener positiveListener, 
			int cancel, DialogInterface.OnClickListener negativeListener) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible()) 
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			dialog.setTitle(title);
			dialog.setPositiveButton(positive, positiveListener);
			dialog.setNegativeButton(cancel, negativeListener);
			dialog.setMessage(message);
			dialog.setCancelable(true);
			dialog.show();
		}
	}

	public static void showAlert(Context context, int title, String message,
			int positive, DialogInterface.OnClickListener positiveListener,
			int cancel, DialogInterface.OnClickListener negativeListener) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible())
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			dialog.setTitle(title);
			dialog.setPositiveButton(positive, positiveListener);
			dialog.setNegativeButton(R.string.cancel, null);
			dialog.setMessage(message);
			dialog.setCancelable(true);
			dialog.show();
		}
	}

	public static void showAlert(Context context, int title, int message, int positive, DialogInterface.OnClickListener positiveListener,
			int negative, DialogInterface.OnClickListener negativeListener) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible()) 
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			dialog.setTitle(title);
			dialog.setPositiveButton(positive, positiveListener);
			dialog.setNegativeButton(negative, negativeListener);
			dialog.setMessage(message);
			dialog.setCancelable(true);
			dialog.show();
		}
	}
	
	// 显示只有确定按钮, 无监听事件的消息框
	public static void showAlert(Context context, String message) 
	{
		showAlert(context, message, R.string.ok, null);
	}
	
	public static void showAlert(Context context, String title, String message, DialogInterface.OnClickListener positiveListener) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible()) 
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			dialog.setTitle(title);
			dialog.setPositiveButton(R.string.ok, positiveListener);
			dialog.setMessage(message);
			dialog.setCancelable(true);
			dialog.show();
		}
	}

	public static void showAlert(Context context, String title, String message, String positive, DialogInterface.OnClickListener positiveListener) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible()) 
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			dialog.setTitle(title);
			dialog.setPositiveButton(positive, positiveListener);
			dialog.setNegativeButton(R.string.cancel, null);
			dialog.setMessage(message);
			dialog.setCancelable(true);
			dialog.show();
		}
	}
	
	public static void showAlert(Context context, int message, int positive, DialogInterface.OnClickListener positiveListener,
			int negative, DialogInterface.OnClickListener negativeListener) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible()) 
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			dialog.setPositiveButton(positive, positiveListener);
			dialog.setNegativeButton(negative, negativeListener);
			dialog.setMessage(message);
			dialog.setCancelable(false);
			dialog.show();
		}
	}
	
	public static void showAlert(Context context, String message, int positive, DialogInterface.OnClickListener positiveListener, 
			int negative, DialogInterface.OnClickListener negativeListener) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible()) 
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			dialog.setPositiveButton(positive, positiveListener);
			dialog.setNegativeButton(negative, negativeListener);
			dialog.setMessage(message);
			dialog.setCancelable(true);
			dialog.show();
		}
	}

	public static void showAlert(Context context, String title, String message, String positive,
			DialogInterface.OnClickListener onclickListener, String negative, DialogInterface.OnClickListener negativeListener) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible()) 
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			dialog.setTitle(title);
			dialog.setPositiveButton(positive, onclickListener);
			dialog.setNegativeButton(negative, negativeListener);
			dialog.setMessage(message);
			dialog.setCancelable(true);
			dialog.show();
		}
	}

	public static void showAlert(Context context, int title, int message, int positive,
			DialogInterface.OnClickListener onclickListener, int negative, DialogInterface.OnClickListener negativeListener,
			DialogInterface.OnDismissListener ondissmess, DialogInterface.OnCancelListener oncanel) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible()) 
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			dialog.setTitle(title);
			dialog.setPositiveButton(positive, onclickListener);
			dialog.setNegativeButton(negative, negativeListener);
			dialog.setMessage(message);
			dialog.setCancelable(true);
			dialog.setOnDismissListener(ondissmess);
			dialog.setOnCancelListener(oncanel);
			dialog.show();
		}
	}

	public static void showAlert(Context context, String message, int positive, DialogInterface.OnClickListener onclickListener) 
	{
		//if ((activity instanceof VisibilityControl) && ((VisibilityControl) activity).isVisible()) 
		{
			ComDialog dialog = ActivityHelper.getPinterestDialog(context);
			dialog.setPositiveButton(positive, onclickListener);
			dialog.setMessage(message);
			dialog.setCancelable(false);
			dialog.show();
		}
	}
}
