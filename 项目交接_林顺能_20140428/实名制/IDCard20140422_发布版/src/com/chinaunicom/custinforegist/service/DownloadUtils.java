package com.chinaunicom.custinforegist.service;

import java.util.HashMap;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.ServiceConnection;
import android.util.Log;

import com.chinaunicom.custinforegist.service.DownloadService.DownloadServiceBinder;

public class DownloadUtils 
{
	public static DownloadService 					sService = null;
	private static HashMap<Context, ServiceBinder> 	sConnectionMap = new HashMap<Context, ServiceBinder>();
	
	public static class ServiceToken 
	{
		ContextWrapper mWrappedContext;

		ServiceToken(ContextWrapper context) 
		{
			mWrappedContext = context;
		}
	}
	
	// 绑定服务
	public static ServiceToken bindToService(Activity context) 
	{
		return bindToService(context, null);
	}

	public static ServiceToken bindToService(Activity context, ServiceConnection callback) 
	{
		Activity realActivity = context.getParent();
		if (realActivity == null) 
		{
			realActivity = context;
		}
		ContextWrapper cw = new ContextWrapper(realActivity);
		cw.startService(new Intent(cw, DownloadService.class));
		ServiceBinder sb = new ServiceBinder(callback);
		if (cw.bindService((new Intent()).setClass(cw, DownloadService.class), sb, 0)) 
		{
			sConnectionMap.put(cw, sb);
			return new ServiceToken(cw);
		}
		Log.e("DownloadUtils", "Failed to bind to service");
		return null;
	}
	
	// 解绑定服务
	public static void unbindFromService(ServiceToken token) 
	{
		if (token == null) 
		{
			Log.e("DownloadUtils", "Trying to unbind with null token");
			return;
		}
		ContextWrapper cw = token.mWrappedContext;
		ServiceBinder sb = sConnectionMap.remove(cw);
		if (sb == null) 
		{
			Log.e("DownloadUtils", "Trying to unbind for unknown Context");
			return;
		}
		cw.unbindService(sb);
		if (sConnectionMap.isEmpty()) 
		{
			// presumably there is nobody interested in the service at this
			// point,
			// so don't hang on to the ServiceConnection
			sService = null;
		}
	}
	
	private static class ServiceBinder implements ServiceConnection 
	{
		ServiceConnection mCallback;

		ServiceBinder(ServiceConnection callback) 
		{
			mCallback = callback;
		}

		public void onServiceConnected(ComponentName className, android.os.IBinder service)
		{
			sService = ((DownloadServiceBinder)service).getService();
			if (mCallback != null) 
			{
				mCallback.onServiceConnected(className, service);
			}
		}

		public void onServiceDisconnected(ComponentName className) 
		{
			if (mCallback != null) 
			{
				mCallback.onServiceDisconnected(className);
			}
			sService = null;
		}
	}	
	
	public static boolean hasTask(String urlKey) 
	{
		if(sService != null) 
		{
			return sService.hasTask(urlKey);
		}
		Log.d("DownloadUtils", "下载服务被关闭..");
		return false;
	}
	
	public static void addTask(DownloadTask task) 
	{
		if(sService != null) 
		{
			if(sService.addTask(task)) 
			{
				Log.i("DownloadUtils", "添加下载任务成功!");
			}
			else 
			{
				Log.e("DownloadUtils", "添加下载任务失败!");
			}
		} 
		else 
		{
			Log.d("DownloadUtils", "下载服务被关闭..");
		}
	}
}
