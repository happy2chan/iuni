.class public Landroid/view/AuroraAdBlockHost;
.super Ljava/lang/Object;
.source "AuroraAdBlockHost.java"


# static fields
.field static final PluginClassName:Ljava/lang/String; = "com.aurora.adblock.AdBlockClass"

.field static final PluginPkg:Ljava/lang/String; = "com.aurora.adblock"

.field private static final TAG:Ljava/lang/String;

.field static sGlobalLock:Ljava/lang/Object;

.field static sInstance:Landroid/view/AuroraAdBlockHost;


# instance fields
.field private adBlockObject:Landroid/view/AuroraAdBlockInterface;

.field private isDuringLoadPlugin:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private isUserApp:Z

.field private final mLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/view/AuroraAdBlockHost;->sGlobalLock:Ljava/lang/Object;

    const-class v0, Landroid/view/AuroraAdBlockHost;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/view/AuroraAdBlockHost;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/view/AuroraAdBlockHost;->adBlockObject:Landroid/view/AuroraAdBlockInterface;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid/view/AuroraAdBlockHost;->mLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Landroid/view/AuroraAdBlockHost;->isDuringLoadPlugin:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/AuroraAdBlockHost;->isUserApp(Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    iput-boolean v0, p0, Landroid/view/AuroraAdBlockHost;->isUserApp:Z

    iget-boolean v0, p0, Landroid/view/AuroraAdBlockHost;->isUserApp:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Landroid/view/AuroraAdBlockHost;->loadPlugin(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Landroid/view/AuroraAdBlockHost;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Landroid/view/AuroraAdBlockHost;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Landroid/view/AuroraAdBlockHost;->isDuringLoadPlugin:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$200(Landroid/view/AuroraAdBlockHost;Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/AuroraAdBlockHost;->getPackageInfo(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Landroid/view/AuroraAdBlockHost;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Landroid/view/AuroraAdBlockHost;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$402(Landroid/view/AuroraAdBlockHost;Landroid/view/AuroraAdBlockInterface;)Landroid/view/AuroraAdBlockInterface;
    .locals 0

    iput-object p1, p0, Landroid/view/AuroraAdBlockHost;->adBlockObject:Landroid/view/AuroraAdBlockInterface;

    return-object p1
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/view/AuroraAdBlockHost;
    .locals 2

    sget-object v1, Landroid/view/AuroraAdBlockHost;->sGlobalLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Landroid/view/AuroraAdBlockHost;->sInstance:Landroid/view/AuroraAdBlockHost;

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/AuroraAdBlockHost;

    invoke-direct {v0, p0}, Landroid/view/AuroraAdBlockHost;-><init>(Landroid/content/Context;)V

    sput-object v0, Landroid/view/AuroraAdBlockHost;->sInstance:Landroid/view/AuroraAdBlockHost;

    :cond_0
    sget-object v0, Landroid/view/AuroraAdBlockHost;->sInstance:Landroid/view/AuroraAdBlockHost;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getPackageInfo(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 4

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private isSystemApp(Landroid/content/pm/ApplicationInfo;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isSystemUpdateApp(Landroid/content/pm/ApplicationInfo;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isUserApp(Landroid/content/pm/ApplicationInfo;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1}, Landroid/view/AuroraAdBlockHost;->isSystemApp(Landroid/content/pm/ApplicationInfo;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Landroid/view/AuroraAdBlockHost;->isSystemUpdateApp(Landroid/content/pm/ApplicationInfo;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private loadPlugin(Landroid/content/Context;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    iget-object v0, p0, Landroid/view/AuroraAdBlockHost;->isDuringLoadPlugin:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/view/AuroraAdBlockHost;->isDuringLoadPlugin:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    new-instance v0, Landroid/view/AuroraAdBlockHost$1;

    invoke-direct {v0, p0, p1}, Landroid/view/AuroraAdBlockHost$1;-><init>(Landroid/view/AuroraAdBlockHost;Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/view/AuroraAdBlockHost$1;->start()V

    goto :goto_0
.end method


# virtual methods
.method public auroraHideAdView(Landroid/content/Context;Landroid/view/View;)Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Landroid/view/AuroraAdBlockHost;->isUserApp:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/view/AuroraAdBlockHost;->adBlockObject:Landroid/view/AuroraAdBlockInterface;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/view/AuroraAdBlockHost;->adBlockObject:Landroid/view/AuroraAdBlockInterface;

    invoke-interface {v0, p1, p2}, Landroid/view/AuroraAdBlockInterface;->auroraHideAdView(Landroid/content/Context;Landroid/view/View;)Z

    move-result v0

    goto :goto_0
.end method
