.class Landroid/widget/AuroraFrameworksUiDateTimeView$1;
.super Landroid/content/BroadcastReceiver;
.source "AuroraFrameworksUiDateTimeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AuroraFrameworksUiDateTimeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/AuroraFrameworksUiDateTimeView;


# direct methods
.method constructor <init>(Landroid/widget/AuroraFrameworksUiDateTimeView;)V
    .locals 0

    iput-object p1, p0, Landroid/widget/AuroraFrameworksUiDateTimeView$1;->this$0:Landroid/widget/AuroraFrameworksUiDateTimeView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Landroid/widget/AuroraFrameworksUiDateTimeView$1;->this$0:Landroid/widget/AuroraFrameworksUiDateTimeView;

    #getter for: Landroid/widget/AuroraFrameworksUiDateTimeView;->mUpdateTimeMillis:J
    invoke-static {v3}, Landroid/widget/AuroraFrameworksUiDateTimeView;->access$000(Landroid/widget/AuroraFrameworksUiDateTimeView;)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Landroid/widget/AuroraFrameworksUiDateTimeView$1;->this$0:Landroid/widget/AuroraFrameworksUiDateTimeView;

    const/4 v2, 0x0

    iput-object v2, v1, Landroid/widget/AuroraFrameworksUiDateTimeView;->mLastFormat:Ljava/text/DateFormat;

    iget-object v1, p0, Landroid/widget/AuroraFrameworksUiDateTimeView$1;->this$0:Landroid/widget/AuroraFrameworksUiDateTimeView;

    invoke-virtual {v1}, Landroid/widget/AuroraFrameworksUiDateTimeView;->update()V

    goto :goto_0
.end method
