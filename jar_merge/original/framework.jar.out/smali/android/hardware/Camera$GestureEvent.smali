.class public Landroid/hardware/Camera$GestureEvent;
.super Ljava/lang/Object;
.source "Camera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/Camera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GestureEvent"
.end annotation


# static fields
.field private static final ACTION_BASE:I = 0x0

.field public static final ACTION_END:I = 0x3

.field public static final ACTION_MOVE:I = 0x2

.field public static final ACTION_START:I = 0x1

.field public static final ACTION_UNKNOWN:I = -0x1

.field private static final GESTURE_EVENT_TYPE_BASE:I = 0x0

.field public static final GESTURE_EVENT_TYPE_FACE_PRESENCE:I = 0x2

.field public static final GESTURE_EVENT_TYPE_FIST_PRESENCE:I = 0x3

.field public static final GESTURE_EVENT_TYPE_OPEN_HAND_PRESENCE:I = 0x1

.field public static final GESTURE_EVENT_TYPE_UNKNOWN:I = -0x1


# instance fields
.field private mAction:I

.field private mObjectId:I

.field private mType:I

.field private mX:I

.field private mY:I

.field private mZ:I

.field final synthetic this$0:Landroid/hardware/Camera;


# direct methods
.method public constructor <init>(Landroid/hardware/Camera;)V
    .locals 2

    const/4 v1, -0x1

    const/4 v0, 0x0

    iput-object p1, p0, Landroid/hardware/Camera$GestureEvent;->this$0:Landroid/hardware/Camera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Landroid/hardware/Camera$GestureEvent;->mType:I

    iput v1, p0, Landroid/hardware/Camera$GestureEvent;->mAction:I

    iput v0, p0, Landroid/hardware/Camera$GestureEvent;->mObjectId:I

    iput v0, p0, Landroid/hardware/Camera$GestureEvent;->mX:I

    iput v0, p0, Landroid/hardware/Camera$GestureEvent;->mY:I

    iput v0, p0, Landroid/hardware/Camera$GestureEvent;->mZ:I

    return-void
.end method


# virtual methods
.method public getAction()I
    .locals 1

    iget v0, p0, Landroid/hardware/Camera$GestureEvent;->mAction:I

    return v0
.end method

.method public getObjectId()I
    .locals 1

    iget v0, p0, Landroid/hardware/Camera$GestureEvent;->mObjectId:I

    return v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Landroid/hardware/Camera$GestureEvent;->mType:I

    return v0
.end method

.method public getX()I
    .locals 1

    iget v0, p0, Landroid/hardware/Camera$GestureEvent;->mX:I

    return v0
.end method

.method public getY()I
    .locals 1

    iget v0, p0, Landroid/hardware/Camera$GestureEvent;->mY:I

    return v0
.end method

.method public getZ()I
    .locals 1

    iget v0, p0, Landroid/hardware/Camera$GestureEvent;->mZ:I

    return v0
.end method

.method public setGestureValue(IIIIII)V
    .locals 0

    iput p1, p0, Landroid/hardware/Camera$GestureEvent;->mType:I

    iput p2, p0, Landroid/hardware/Camera$GestureEvent;->mAction:I

    iput p3, p0, Landroid/hardware/Camera$GestureEvent;->mObjectId:I

    iput p4, p0, Landroid/hardware/Camera$GestureEvent;->mX:I

    iput p5, p0, Landroid/hardware/Camera$GestureEvent;->mY:I

    iput p6, p0, Landroid/hardware/Camera$GestureEvent;->mZ:I

    return-void
.end method
