.class public Landroid/widget/AuroraFrameworksUiTextView;
.super Landroid/widget/TextView;
.source "AuroraFrameworksUiTextView.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "AuroraFrameworksUiTextView"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/AuroraFrameworksUiTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, "AuroraFrameworksUiTextView(Context context, AttributeSet attrs)"

    invoke-direct {p0, v0}, Landroid/widget/AuroraFrameworksUiTextView;->log(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/AuroraFrameworksUiTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string v0, "AuroraFrameworksUiTextView(Context context)"

    invoke-direct {p0, v0}, Landroid/widget/AuroraFrameworksUiTextView;->log(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AuroraFrameworksUiTextView(Context context, AttributeSet attrs, int defStyle)  textsize =  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/AuroraFrameworksUiTextView;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/AuroraFrameworksUiTextView;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/widget/AuroraFrameworksUiTextView;->auroraSetDefaultBaseLinePadding()V

    return-void
.end method

.method private log(Ljava/lang/String;)V
    .locals 1

    const-string v0, "AuroraFrameworksUiTextView"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    const-string/jumbo v0, "onDraw(Canvas canvas)"

    invoke-direct {p0, v0}, Landroid/widget/AuroraFrameworksUiTextView;->log(Ljava/lang/String;)V

    return-void
.end method
