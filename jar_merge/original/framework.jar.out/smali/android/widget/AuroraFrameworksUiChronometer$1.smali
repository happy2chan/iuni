.class Landroid/widget/AuroraFrameworksUiChronometer$1;
.super Landroid/os/Handler;
.source "AuroraFrameworksUiChronometer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/widget/AuroraFrameworksUiChronometer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/widget/AuroraFrameworksUiChronometer;


# direct methods
.method constructor <init>(Landroid/widget/AuroraFrameworksUiChronometer;)V
    .locals 0

    iput-object p1, p0, Landroid/widget/AuroraFrameworksUiChronometer$1;->this$0:Landroid/widget/AuroraFrameworksUiChronometer;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p0, Landroid/widget/AuroraFrameworksUiChronometer$1;->this$0:Landroid/widget/AuroraFrameworksUiChronometer;

    #getter for: Landroid/widget/AuroraFrameworksUiChronometer;->mRunning:Z
    invoke-static {v0}, Landroid/widget/AuroraFrameworksUiChronometer;->access$000(Landroid/widget/AuroraFrameworksUiChronometer;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/widget/AuroraFrameworksUiChronometer$1;->this$0:Landroid/widget/AuroraFrameworksUiChronometer;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    #calls: Landroid/widget/AuroraFrameworksUiChronometer;->updateText(J)V
    invoke-static {v0, v1, v2}, Landroid/widget/AuroraFrameworksUiChronometer;->access$100(Landroid/widget/AuroraFrameworksUiChronometer;J)V

    iget-object v0, p0, Landroid/widget/AuroraFrameworksUiChronometer$1;->this$0:Landroid/widget/AuroraFrameworksUiChronometer;

    invoke-virtual {v0}, Landroid/widget/AuroraFrameworksUiChronometer;->dispatchChronometerTick()V

    const/4 v0, 0x2

    invoke-static {p0, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Landroid/widget/AuroraFrameworksUiChronometer$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    return-void
.end method
