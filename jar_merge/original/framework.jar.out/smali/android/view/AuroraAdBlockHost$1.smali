.class Landroid/view/AuroraAdBlockHost$1;
.super Ljava/lang/Thread;
.source "AuroraAdBlockHost.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/view/AuroraAdBlockHost;->loadPlugin(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/view/AuroraAdBlockHost;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/view/AuroraAdBlockHost;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Landroid/view/AuroraAdBlockHost$1;->this$0:Landroid/view/AuroraAdBlockHost;

    iput-object p2, p0, Landroid/view/AuroraAdBlockHost$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const/4 v10, 0x0

    invoke-static {}, Landroid/view/AuroraAdBlockHost;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string v8, "load Plugin in thread"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Landroid/view/AuroraAdBlockHost$1;->val$context:Landroid/content/Context;

    if-nez v7, :cond_0

    iget-object v7, p0, Landroid/view/AuroraAdBlockHost$1;->this$0:Landroid/view/AuroraAdBlockHost;

    #getter for: Landroid/view/AuroraAdBlockHost;->isDuringLoadPlugin:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v7}, Landroid/view/AuroraAdBlockHost;->access$100(Landroid/view/AuroraAdBlockHost;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v7, p0, Landroid/view/AuroraAdBlockHost$1;->this$0:Landroid/view/AuroraAdBlockHost;

    iget-object v8, p0, Landroid/view/AuroraAdBlockHost$1;->val$context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "com.aurora.adblock"

    #calls: Landroid/view/AuroraAdBlockHost;->getPackageInfo(Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    invoke-static {v7, v8, v9}, Landroid/view/AuroraAdBlockHost;->access$200(Landroid/view/AuroraAdBlockHost;Landroid/content/Context;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v6

    if-nez v6, :cond_1

    invoke-static {}, Landroid/view/AuroraAdBlockHost;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "pkg= com.aurora.adblock is not install"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Landroid/view/AuroraAdBlockHost$1;->this$0:Landroid/view/AuroraAdBlockHost;

    #getter for: Landroid/view/AuroraAdBlockHost;->isDuringLoadPlugin:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v7}, Landroid/view/AuroraAdBlockHost;->access$100(Landroid/view/AuroraAdBlockHost;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    :cond_1
    iget-object v7, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v7, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iget-object v7, p0, Landroid/view/AuroraAdBlockHost$1;->val$context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iget-object v2, v7, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    iget-object v7, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v7, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    new-instance v0, Ldalvik/system/DexClassLoader;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-direct {v0, v3, v2, v5, v7}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    :try_start_0
    const-string v7, "com.aurora.adblock.AdBlockClass"

    invoke-virtual {v0, v7}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    iget-object v7, p0, Landroid/view/AuroraAdBlockHost$1;->this$0:Landroid/view/AuroraAdBlockHost;

    #getter for: Landroid/view/AuroraAdBlockHost;->mLock:Ljava/lang/Object;
    invoke-static {v7}, Landroid/view/AuroraAdBlockHost;->access$300(Landroid/view/AuroraAdBlockHost;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v9, p0, Landroid/view/AuroraAdBlockHost$1;->this$0:Landroid/view/AuroraAdBlockHost;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/AuroraAdBlockInterface;

    #setter for: Landroid/view/AuroraAdBlockHost;->adBlockObject:Landroid/view/AuroraAdBlockInterface;
    invoke-static {v9, v7}, Landroid/view/AuroraAdBlockHost;->access$402(Landroid/view/AuroraAdBlockHost;Landroid/view/AuroraAdBlockInterface;)Landroid/view/AuroraAdBlockInterface;

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    iget-object v7, p0, Landroid/view/AuroraAdBlockHost$1;->this$0:Landroid/view/AuroraAdBlockHost;

    #getter for: Landroid/view/AuroraAdBlockHost;->isDuringLoadPlugin:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v7}, Landroid/view/AuroraAdBlockHost;->access$100(Landroid/view/AuroraAdBlockHost;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    :catchall_0
    move-exception v7

    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v7
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Landroid/view/AuroraAdBlockHost;->access$000()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "loadPlugin error:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
