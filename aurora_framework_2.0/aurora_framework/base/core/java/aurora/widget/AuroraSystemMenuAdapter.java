package aurora.widget;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class AuroraSystemMenuAdapter extends AuroraMenuAdapterBase {

	private boolean mMatchParent = false; 
	private LinearLayout linear = null;
	
	public AuroraSystemMenuAdapter(Context context,
			ArrayList<AuroraMenuItem> lists) {
		super(context, lists);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		iconId = com.aurora.internal.R.id.aurora_menu_item_icon;
		titleId = com.aurora.internal.R.id.aurora_menu_item_text;
		layoutId = com.aurora.R.layout.aurora_menu_item;
		if (convertView == null) {
			convertView = layoutInflater.inflate(layoutId, null);
			viewHolder = new ViewHolder();
			viewHolder.title = (TextView) convertView.findViewById(titleId);
			viewHolder.icon = (ImageView) convertView.findViewById(iconId);
			viewHolder.menuItemLayout = (LinearLayout) convertView;
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		linear = (LinearLayout) convertView.findViewById(com.aurora.internal.R.id.aurora_menu_normal_layout);
		AuroraMenuItem auroraMenuItem = menuItems.get(position);
		if (auroraMenuItem != null) {
			viewHolder.icon.setBackgroundResource(auroraMenuItem.getIcon());
//			if ( auroraMenuItem.getIcon() == 0 ) {
//				LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
//						LayoutParams.WRAP_CONTENT);
//				LayoutParams lp = (LayoutParams)viewHolder.title.getLayoutParams();
//				lp.leftMargin = 0;
//				viewHolder.title.setLayoutParams(lp);
//			}
			if (mMatchParent) {
				LayoutParams lp = (LayoutParams)viewHolder.title.getLayoutParams();
				lp.gravity = Gravity.CENTER;
				lp.leftMargin = 0;
				viewHolder.title.setLayoutParams(lp);
				linear.setGravity(Gravity.CENTER);
			}
//			if (auroraMenuItem.getTitle() != 0) {
//				viewHolder.title.setText(activity.getResources().getString(
//						auroraMenuItem.getTitle()));
			if (auroraMenuItem.getTitleText() != null) {
				viewHolder.title.setText(auroraMenuItem.getTitleText());
			} else {
				viewHolder.title.setText("");
			}
			menuIds.put(auroraMenuItem.getId(), position);// 设置itemId,position对应关系
			viewHolder.title.setEnabled(isItemEnable(position));
			
//			if (isEnabled(position)) {
//				viewHolder.menuItemLayout.setBackgroundDrawable(activity
//						.getResources().getDrawable(
//								com.aurora.R.drawable.aurora_menu_item_select));
//			} 
		}
		return convertView;
	}
	
	public void setAdapterLayoutItemMatchParent(boolean flag) {
		this.mMatchParent = flag;
	}
}
