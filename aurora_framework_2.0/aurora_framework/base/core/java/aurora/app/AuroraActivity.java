package aurora.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.xmlpull.v1.XmlPullParser;

import com.aurora.utils.DensityUtil;
import com.aurora.utils.HomeKeyWatcher;
import com.aurora.utils.SystemUtils;
import com.aurora.utils.HomeKeyWatcher.OnHomePressedListener;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.SearchView;
import android.widget.TextView;
import aurora.widget.AuroraActionBar;
import aurora.widget.AuroraActionBar.OnActionbarSearchViewQuitListener;
import aurora.widget.AuroraActionBar.OnAuroraActionBarItemClickListener;
import aurora.widget.AuroraActionBar.Type;
import aurora.widget.AuroraActionBarBase;
import aurora.widget.AuroraActionBarHost;
import aurora.widget.AuroraActionBarItem;
import aurora.widget.AuroraActionBottomBarMenuAdapter;
import aurora.widget.AuroraAlphaListener;
import aurora.widget.AuroraCustomActionBar;
import aurora.widget.AuroraCustomMenu;
import aurora.widget.AuroraCustomMenu.CallBack;
import aurora.widget.AuroraCustomMenu.OnMenuItemClickLisener;
import aurora.widget.AuroraMenu;
import aurora.widget.AuroraMenuAdapterBase;
import aurora.widget.AuroraMenuBase;
import aurora.widget.AuroraMenuItem;
import aurora.widget.AuroraSearchView;
import aurora.widget.AuroraSearchView.OnKeyBackListener;
import aurora.widget.AuroraSearchView.OnQueryTextListener;
import aurora.widget.AuroraSystemMenu;
import aurora.widget.AuroraSystemMenuAdapter;
import aurora.widget.AuroraUtil;
import android.net.Uri;

import com.aurora.utils.AuroraLog;
/**
 * @author leftaven
 * @2013年9月12日 for base aurora activity
 */
public class AuroraActivity extends Activity implements AuroraActionBarBase,CallBack,OnHomePressedListener{
	
	private static final String TAG = "AuroraActivity";
	/**
	 * Uri for change statusbar colors
	 */
	public static final Uri STATUS_BAR_COLOR_URI = Uri.parse("content://"+"com.android.systemui.statusbar.phone"+"/"+"immersion"+"?notify=true");
	
	/**
	 * key for stored status bar color in SystemUi process
	 */
	public static final String KEY_FOR_STATUS_BAR_COLOR = "imersion_color";
	
	/**
	 * used this value to change status bar icon's color to white
	 */
	private static final int STATUS_BAR_WHITE = 0;
	/**
	 * used this value to change status bar icon's color to black
	 */
	private static final int STATUS_BAR_BLACK = 1;
	
	protected static final String AURORA_BLACK_IMMERSE = "aurorablackBG8345";
 	protected static final String AURORA_WHITE_IMMERSE = "aurorawhiteBG653";
	
	private static final int MSG_CHANGE_STATUS_BAR_COLOR = 0x001;
	private static final int  MSG_NOT_CHANGE_STATUS_BAR_COLOR = 0x002;
	
	private TextView mLeftView;
	private TextView mRightView;

	private View currentView;

	private View mCoverView = null;
	private FrameLayout windowLayout;
	private Animation coverAnimation;

	// ActionBar init start
	/**
	 * mActionBarType mActionBarHost mActionBar
	 */
	private AuroraActionBar.Type mActionBarType;
	private AuroraActionBarHost mActionBarHost;
	private AuroraActionBar mActionBar;
	private OnAuroraActionBarItemClickListener mActionBarListener;
	private List<View> views;
	// ActionBar init end

	// Menu init start
	private AuroraMenu auroraActionBottomBarMenu;
	private AuroraSystemMenu auroraMenu;
	private AuroraCustomMenu auroraCustomMenu;
	private ArrayList<AuroraMenuItem> mAuroraMenumenuItems;
	private ArrayList<AuroraMenuItem> mAuroraSystemMenumenuItems;
	private ArrayList<AuroraMenuItem> mAddMenuItems = new ArrayList<AuroraMenuItem>();
    private AuroraMenuAdapterBase auroraCustomMenuAdapter;
	private AuroraMenuAdapterBase auroraMenuAdapter;
	private AuroraMenuBase.OnAuroraMenuItemClickListener auroraMenuCallBack;
	private AuroraMenuBase.OnAuroraMenuItemClickListener auroraSystemMenuCallBack;
	private Boolean menuIsEnable = true;
	private Map<Integer, Integer> mAuroraMenumenuIds;
	private Map<Integer, Integer> mAuroraSystemMenumenuIds;
	private Map<Integer, Integer> menuCustomIds = new HashMap<Integer, Integer>();
	private AuroraMenuAdapterBase auroraActionBottomBarMenuAdapter;

	// Menu init end

	private AuroraAlphaListener alphaListener;
	private Boolean firstCreateAllOperation=true;
	
	private FrameLayout mSearchviewlayout;
	private LinearLayout mSearchviewBack;
	private Button cancelBtn;
	private boolean isSearchviewAnimRun = false;
	private boolean isCanClickToHide = false;
	private boolean mClearSearchViewText = true;
	
	// Aurora <Luofu> <2013-11-28> modify for searchView begin
	private AuroraSearchView mSearchView;
	private View mSearchBackgroud;
	private OnSearchViewQueryTextChangeListener mSearchViewQueryTextListener;
	private boolean mNeedSearchView = false;
	
	private View mSearchViewBorder;
	// Aurora <Luofu> <2013-11-28> modify for SearchView end
	
	//aurora add by tangjun start 2014.1.9
	private int lastMenu = 0;
	//aurora add by tangjun end 2014.1.9
	
	//aurora add by tangjun start 2014.5.7
	private LinearLayout cancelBtnLinear;
	private boolean isOutOfBounds = false;
	//aurora add by tangjun end 2014.5.7
	
	
	private Animation mSearchViewUpAnimation;
	private Animation mSearchViewScaleBigAnimation;
	private Animation mSearchViewButtonShowAnimation;
	
	private Animation mSearchViewDownAnimation;
	private Animation mSearchViewScaleSmallAnimation;
	private Animation mSearchViewButtonHideAnimation;
	
	private Drawable mSearchViewAnimStopBackGdDrawable = null;
	private Drawable mSearchViewAnimRunBackGdDrawable = null;
	
	private Drawable mWhiteActionBarBg;
	private Drawable mGreenActionBarBg;
	
	AuroraCustomActionBar mAuroraCustomActionBar;
	
	private long mSearchViewAnimDuration = -1;
	
	private boolean mInvokeAuroraSetContentView;
	
	private boolean mIsNeedShowMenuWhenKeyMenuClick = true;
	

	private LinearLayout mAuroraCustomActionBarHost;
	private int mCoverViewColor = 0x99000000;
	
	private HomeKeyWatcher mHomeKeyWatcher;
	
	
	private Runnable mTransparentStatusBarRunnable = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			transparentStatusBar();
		}
	};
	
	private Handler mHandler = new Handler();
	
	private boolean mStatusBarTransparent = false;
	/**
	 * HandlerThread will run in a new thread
	 */
	private HandlerThread mStatusThread ;
 	private Looper mStatusLooper;
 	private Handler mStatusHandler;
 	
 	private LayoutInflater mInflater;
 	
 	/**
 	 * init new thread to handle event of change status bar colors
 	 */
 	private void initStatusHandler(){
 		mStatusThread = new HandlerThread("change_status_bar_color_Thr_aurora");
 		mStatusThread.start();
 		mStatusLooper = mStatusThread.getLooper();
 		mStatusHandler = new Handler(mStatusLooper){
 			public void handleMessage(android.os.Message msg) {
// 				if(msg.what == MSG_CHANGE_STATUS_BAR_COLOR){
// 					transparentStatusBar();
// 				}
 				changeStatusBar(msg.what == MSG_CHANGE_STATUS_BAR_COLOR);
 				
 			};
 		};
 	}
 	
 	/**
 	 * quit handlerThread safely
 	 */
 	private void destoryStatusThread(){
 		if(mStatusThread != null){
 			mStatusThread.quitSafely();
 		}
 	}
 	
	public AuroraActivity() {
		this(AuroraActionBar.Type.Normal);// 默认为一般模式，有返回键，有标题
	}

	public AuroraActivity(AuroraActionBar.Type actionBarType) {
		super();
		mActionBarType = actionBarType;
	}
	
	/**
	 * called this method to change status bar's background to transparent
	 * @since Android L
	 */
   protected void setStatusBarBackgroundTransparent(){
		//Window window = getWindow();  
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS  
          //      | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);  
       // window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN  
       //                 | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);  
       // window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);  
      //  window.setStatusBarColor(Color.TRANSPARENT); 
   }
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	    // TODO Auto-generated method stub
		mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    super.onCreate(savedInstanceState);
	    initStatusHandler();
	    /*
	     * 
	     * set status bar background to transparent  since android L
	     * 
	     */
	    setStatusBarBackgroundTransparent();
        
	    
	    mHomeKeyWatcher = new HomeKeyWatcher(this);
	    mHomeKeyWatcher.setOnHomePressedListener(this);
	    mHomeKeyWatcher.startWatch();
	    
//	    new Thread(new Runnable() {
//			
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				judgeIfApkNeedToUpdate( );
//			}
//		}).start();
	}
	
	private AuroraCustomMenu getCustomMenu(){
		if (auroraCustomMenu == null) {
			auroraCustomMenu = new AuroraCustomMenu(this,
					com.aurora.R.style.PopupAnimation,
					com.aurora.internal.R.layout.aurora_menu_layout);
			auroraCustomMenu.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss() {
					// TODO Auto-generated method stub
					removeCoverView();
				}
			});
			auroraCustomMenu.setCallBack(this);
		}
		return auroraCustomMenu;
	}
	
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		if(mSearchView != null && mSearchviewlayout.getVisibility() == View.VISIBLE && hasFocus){
			mSearchView.getFocus();
		}

		if (mActionBarType != Type.NEW_COSTOM) { 
			if(getAuroraActionBar() != null && getAuroraActionBar().getVisibility() != View.VISIBLE) {
				setAuroraActionbarSplitLineVisibility(getAuroraActionBar().getVisibility());
			}
		}
	}
	
	
		@Override
	protected void onResume() {
		// TODO Auto-generated method stub
			if(statusBarIsShowing()){
				
				
				mHandler.post(mTransparentStatusBarRunnable);
				AuroraLog.e("activity", "transparent statusbar");
			}else{
				resetContentPadding();
			}
//			transparentStatusBar();
		super.onResume();
	}

	private void resetContentPadding(){
		if(getAuroraActionBar() != null){
			ViewGroup.LayoutParams params = (ViewGroup.LayoutParams)getAuroraActionBar().getLayoutParams();
			params.height = DensityUtil.dip2px(this,53);
			getAuroraActionBar().setLayoutParams(params);
			getAuroraActionBar().setPadding(getAuroraActionBar().getPaddingLeft(),0
					,getAuroraActionBar().getPaddingRight(),getAuroraActionBar().getPaddingBottom());
		}
	}
		
	/**
	 * status bar showing or hidden
	 * @return 
	 */
	private boolean statusBarIsShowing(){
		WindowManager.LayoutParams attrs = getWindow().getAttributes();
		return ((attrs.flags & WindowManager.LayoutParams.FLAG_FULLSCREEN) != WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	/**
	 * get the target view's draw cache 
	 * @param view target view
	 * @param widthSpec
	 * @param heightSpec
	 * @return  bitmap of target view's drawcache
	 */
	private  Bitmap getDrawCache(View view,int widthSpec,int heightSpec) {
		view.measure(widthSpec,heightSpec);
		view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
		view.buildDrawingCache();
		Bitmap bitmap = view.getDrawingCache();

		return bitmap;
	}
	/**
	 * you must destory drawcache bitmap,if not it will caused oom
	 * @param view
	 */
	private void destoryDrawCache(View view){
		view.destroyDrawingCache();
	}
		
	/**
	 * change statusbar color  by given actionbar's color
	 */
	private void transparentStatusBar() {
		
		int white = -1;
		if (mActionBarType == Type.NEW_COSTOM) {
			Drawable drawable = mAuroraCustomActionBar.getBackground();
			if (drawable != null) {
				mStatusHandler.sendEmptyMessage(MSG_NOT_CHANGE_STATUS_BAR_COLOR);
			}

		} else {
			if (windowLayout != null) {
				if (getAuroraActionBar() != null) {
					Bitmap drawCache = getDrawCache(getAuroraActionBar(),getAuroraActionBar().getWidthMeasureSpec(),getAuroraActionBar().getHeightMeasureSpec());//.getDrawingCache();
					if (drawCache != null) {
						Drawable bg = getAuroraActionBar().getBackground();
						if(! (bg instanceof ColorDrawable)){
							
							return;
						}
						int statusBarColor = ((ColorDrawable)bg).getColor()/*getActionBarColor(drawCache, this);*/;
						int colorR = Color.red(statusBarColor);
						int colorG = Color.green(statusBarColor);
						int colorB = Color.blue(statusBarColor);
						
						int whiteR = Color.red(white);
						int whiteG = Color.green(white);
						int whiteB = Color.blue(white);
						int whiteRange = 60;
						if ((whiteR - colorR)<whiteRange
								&&(whiteG - colorG)<whiteRange 
								&&(whiteB - colorB)<whiteRange) {
							TextView titleView = getAuroraActionBar().getTitleView();
							CharSequence title = null;
							if(titleView != null){
							  title = titleView.getText();
							}
							if(getAuroraActionBar().getVisibility() != View.GONE){
								mStatusHandler.sendEmptyMessage(MSG_CHANGE_STATUS_BAR_COLOR);
							//	changeStatusBar(true);
							}
						} else {
							mStatusHandler.sendEmptyMessage(MSG_NOT_CHANGE_STATUS_BAR_COLOR);
							//changeStatusBar(false);
						}
					}
					destoryDrawCache(getAuroraActionBar());
					drawCache = null;
				}
			}
			
		}
		
	}

	private boolean hasActionBar() {
		return getAuroraActionBar() == null ? false : (getAuroraActionBar()
				.getVisibility() == View.VISIBLE);
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		//changeStatusBar(false);
		super.onPause();
				if( mSearchView != null ) {
			mSearchView.clearEditFocus();
		}
	}
	/*@Override
	public boolean moveTaskToBack(boolean nonRoot) {
		// TODO Auto-generated method stub
		String pkgName = getPackageName();
		
		if("com.android.browser".equals(pkgName)
				||"com.aurora.privacymanage".equals(pkgName)){
			
			return super.moveTaskToBack(nonRoot); 
		}
		Intent intent = getIntent();
		if(intent != null){
			String action = intent.getAction();
			if("android.intent.action.MAIN".equals(action)){
				back();
				return true;
			}
		}
		
		return super.moveTaskToBack(nonRoot);
		
	}
	private void back(){
		ActionBar actionBar = getActionBar();
        if (actionBar != null && actionBar.collapseActionView()) {
            return;
        }

        
        if (!getFragmentManager().popBackStackImmediate()) {
            finishAfterTransition();
        }

	}*/
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		destoryStatusThread();
		mHomeKeyWatcher.stopWatch();
		super.onDestroy();
	}
	
	private void updateStatusBarColor(int colorType){
		ContentResolver cr = this.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(KEY_FOR_STATUS_BAR_COLOR,Integer.toHexString(colorType) );
        int count = 0;
		try {
			count = cr.update(STATUS_BAR_COLOR_URI, values, null, null);
			if (count == 0) {
				cr.insert(STATUS_BAR_COLOR_URI, values);
			}
		} catch (Exception ex) {
			Log.d(TAG, "catched update url exception--> " + ex);
		}
	}
	
	public void changeStatusBar(boolean white){
		/* NotificationManager notificationManager = ( NotificationManager )getSystemService(Context.NOTIFICATION_SERVICE);
         Notification.Builder builder = new Notification.Builder(this);
         builder.setSmallIcon(com.aurora.R.drawable.aurora_switch_on);
         String tag=AURORA_WHITE_IMMERSE;
         if(white){
        	 tag = AURORA_BLACK_IMMERSE;
         }*/
         int colorType = STATUS_BAR_WHITE;
         if(white){
        	 colorType = STATUS_BAR_BLACK;
         }
         
         
         updateStatusBarColor(colorType);
        // notificationManager.notify(tag, 0, builder.build());
	}
	private void judgeIfApkNeedToUpdate( ) {
		
		if ( getIntent() == null || getIntent().getAction() == null || 
				!getIntent().getAction().equals("android.intent.action.MAIN") ) {
			return;
		}
		
		long aa = System.currentTimeMillis();
		
		Uri updateUri = Uri.parse("content://com.aurora.appupdate.provider.forceupprovider/");
		
		String packageName = this.getPackageName();
		
		Cursor cursor = getContentResolver().query(updateUri, null, null, new String[]{packageName}, null);

		//Log.e("111111", "----cursor = ------" + cursor);
		//Log.e("111111", "----this.getPackageName() = ------" + this.getPackageName());
		
		if( cursor != null ) {
			try {
				if (cursor.moveToNext()) {
					int apkId = cursor.getInt(0);
					Log.e("111111", "----apkId = ------" + apkId);
					Intent intent = new Intent();
					intent.setAction("com.aurora.appupdate.UPDATE");
					intent.putExtra("apkId", apkId);
					startActivity(intent);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			} finally {
				cursor.close();
			}
		}
		
		long bb = System.currentTimeMillis() - aa;
		//Log.e("111111", "--111cost bb = -----" + bb);
		
	}

	   // Aurora <Luofu> <2014-1-7> modify for Add menu begin
    public void addMenu(int menuId,int titleRes,OnMenuItemClickLisener listener){
    	getCustomMenu().addMenu(menuId, getResources().getString(titleRes), listener);
    }
    public void addMenu(int menuId,CharSequence menuTitle,OnMenuItemClickLisener listener){
    	getCustomMenu().addMenu(menuId, menuTitle, listener);
    }
    public void addMenu(int menuId,View menuView,OnMenuItemClickLisener listener){
    	getCustomMenu().addMenu(menuId, menuView,listener);
    }
    
    public void removeMenuById(int menuId){
    	getCustomMenu().removeMenuById(menuId);
    }
    
    private AuroraMenuItem createMenuItem(int menuId){
        AuroraMenuItem menu = new AuroraMenuItem();
        menu.setId(menuId);
        return menu;
    }
    
    public void hideCustomMenu(){
        if(auroraCustomMenu == null){
            return;
        }
        if(getCustomMenu().isShowing()){
        	getCustomMenu().dismiss();
        }
    }
    
    public void showCustomMenu(){
//        Log.e("mm", "+++++++++++++");
        if (auroraCustomMenu != null) {
            if (getCustomMenu().isShowing()) {
                Log.e("mm", "----------------");
                dismissAuroraMenu();
            } else {
            	getCustomMenu().showAtLocation(getWindow().getDecorView(),
                        Gravity.BOTTOM, 0, 0);
                addCoverView();
                // alphaListener.startMenuShowThread();
            }
        }  
    }
    // Aurora <Luofu> <2014-1-7> modify for Add menu end
	public void setAuroraMagicBarNull() { // add for contact
	}

	public boolean isOptionsMenuExpand() {// add for mms
		return auroraMenu.isShowing() ? true : false;
	}

	private void ensureLayout() {
		if (!verifyLayout()) {
			setContentView(createLayout());
		}
	}

	private boolean verifyLayout() {
		return mActionBarHost != null;
	}

	@Override
	public int createLayout() {// 3种类型的actionbar样式
	    if(mActionBarType == null){
	        return com.aurora.R.layout.aurora_content_normal;
	    }
		switch (mActionBarType) {
		case Dashboard:
			return com.aurora.R.layout.aurora_content_dashboard;
		case Empty:
			return com.aurora.R.layout.aurora_content_empty;
		case Custom:
			return com.aurora.R.layout.aurora_content_custom;
		case Normal:
		default:
			return com.aurora.R.layout.aurora_content_normal;
		}
	}
	
	/**
	 * 设置搜索框的背景 tangjun 2014.06.19
	 */
	public void setSearchviewBarBackgroundResource(int stopResid, int runResid) {
		mSearchViewAnimRunBackGdDrawable = this.getResources().getDrawable(runResid);
		mSearchViewAnimStopBackGdDrawable = this.getResources().getDrawable(stopResid);
	}
	
	/**
	 * 设置搜索框的背景 tangjun 2014.06.19
	 */
	public void setSearchviewBarBackgroundDrawable(Drawable stopDrawable, Drawable runDrawable) {
		mSearchViewAnimRunBackGdDrawable = runDrawable;
		mSearchViewAnimStopBackGdDrawable = stopDrawable;
	}
	
	/**
	 * addSearchview tangjun 2013.11.28
	 */
	private void addSearchview( ) {
		//long aa = System.currentTimeMillis();
		mSearchviewlayout = (FrameLayout) mInflater.inflate(com.aurora.R.layout.aurora_searchview_activity, null);
		//long bb = System.currentTimeMillis();
		//Log.e("222222", "addSearchview --1 = " + String.valueOf(bb - aa) );
		windowLayout.addView(mSearchviewlayout);
		//long cc = System.currentTimeMillis();
		//Log.e("222222", "addSearchview --1 = " + String.valueOf(cc - bb) );
		cancelBtn = (Button)findViewById(com.aurora.R.id.aurora_activity_searchviewcancelbtn);
		cancelBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if ( mSearchViewButtonClickListener != null ) {
					mSearchViewButtonClickListener.onSearchViewButtonClick();
				} else {
					hideSearchviewLayout();
				}
			}
		});
		mSearchviewBack = (LinearLayout)findViewById(com.aurora.R.id.aurora_activity_searchviewbar);
		
		cancelBtnLinear = (LinearLayout)findViewById(com.aurora.internal.R.id.cancelbuttonlinear);
		//cancelBtnLinear.setBackgroundColor(Color.RED);
		cancelBtnLinear.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				switch ( event.getAction() ){
				case MotionEvent.ACTION_DOWN:
					cancelBtn.setPressed(true);
					break;
				case MotionEvent.ACTION_UP:
					if(!isOutOfBounds){
						if ( mSearchViewButtonClickListener != null ) {
							mSearchViewButtonClickListener.onSearchViewButtonClick();
						} else {
							hideSearchviewLayout();
						}
					}
					cancelBtn.setPressed(false);
					isOutOfBounds = false;
					break;
				case MotionEvent.ACTION_MOVE:
					if(isOutOfBounds(AuroraActivity.this, event, cancelBtnLinear)){
						isOutOfBounds = true;
						cancelBtn.setPressed(false);
					}
					break;
				default:
					break;
				}
				return true;
			}
		});
	}
	
	
	
	/**
	 * 设置actionbar view tangjun 2013.11.28
	 * 
	 * @param resID
	 */
	public void setAuroraContentView(int resID,
			AuroraActionBar.Type actionBarType) {
		setAuroraContentView(resID, actionBarType, false);
	}
	
	/**
	 * 设置actionbar view tangjun 2013.11.28
	 * 
	 * @param resID
	 */
	public void setAuroraContentView(int resID,
			AuroraActionBar.Type actionBarType, boolean needSearchview) {
		mInvokeAuroraSetContentView = true;
	    if(actionBarType == Type.NEW_COSTOM){
	        mActionBarType = actionBarType;
	        setContentView(com.aurora.R.layout.aurora_activity_content_layout);
	            mAuroraCustomActionBar = (AuroraCustomActionBar)findViewById(com.aurora.R.id.aurora_custom_action_bar_layout);
	            if(mAuroraCustomActionBar != null){
	                mAuroraCustomActionBar.bindToActivity(this);
	                mAuroraCustomActionBar.showSearchView(false, 0);
	                mAuroraCustomActionBar.addContentView(resID);
	            }
	           
//	        }
	        windowLayout = (FrameLayout) getWindow().getDecorView();// (FrameLayout) findViewById(com.aurora.internal.R.id.windowLayout);
	        mAuroraCustomActionBarHost = (LinearLayout)findViewById(com.aurora.internal.R.id.aurora_action_bar_host);
	        Log.e("101010", "---mAuroraCustomActionBarHost = -----" + mAuroraCustomActionBarHost);
	    }else{
    		mActionBarType = actionBarType;
    		ensureLayout();
    		onPreContentChanged();
    		FrameLayout contentView = getContentView();// 获得内容区域，前端设置界面
    		if ( contentView.getChildCount() > 0 ) {
    			contentView.removeAllViews();
    		}
    		currentView = mInflater.inflate(resID, contentView);
//    		setContentView(windowLayout);
    		onContentChanged();
	    }
		mNeedSearchView = needSearchview;
		if (needSearchview) {
//			initSearchViewAnimation();
			addSearchview();
			
			initSearchView();
		}
		
		AuroraLog.e("activity", "actionBarType:"+mActionBarType.getClass().getName()+" value:"+mActionBarType);
//		transparentStatusBar();
	}
	
	@Override
	public void setContentView(int layoutResId){
		super.setContentView(layoutResId);
		AuroraLog.e("activity", "setContentView(int layoutResId)");
	}
	
	/**
	 * get color of actionbar background,we used this color to set to status bar background.
	 * @param bmp
	 * @return
	 */
	private int getActionBarColor(Bitmap drawable,Activity activity){
		int color = 0;
		int width = drawable.getWidth();
		int height = drawable.getHeight();
		color = drawable.getPixel(width/2, height/2);
		return color;
	}
	
	
	private int getMaxCountColor(int[] pixels){

		Map map = new HashMap();

		for (int i = 0; i < pixels.length; i++) {
			int count = 0;
			for (int j = 0; j < pixels.length; j++) {
				if (pixels[i] == (pixels[j])) {
					count = count + 1;
				}
			}
			map.put(pixels[i], count);
		}
		Set key = map.keySet();
		int k = key.size();
		int ii[] = new int[k];
		k = 0;
		int temp = 0;
		int temp2 = 0;
		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			int name = Integer.parseInt(iterator.next().toString());
			int count2 = Integer.parseInt(map.get(name).toString());
			if (temp < count2) {
				temp = count2;

			}
		}

		for (Iterator iterator = key.iterator(); iterator.hasNext();) {
			int name = Integer.parseInt(iterator.next().toString());
			int count2 = Integer.parseInt(map.get(name).toString());
			if (temp == count2) {
				ii[k] = name;
				k++;
			}
		}

		for (int i = 0; i < ii.length; i++) {
			if (ii[i] != 0) {
				return ii[i];
			}

		}
	
		return pixels[pixels.length/2];
	}
	
	
	public AuroraCustomActionBar getCustomActionBar(){
        return mAuroraCustomActionBar;
    }
	/**
	 * 设置actionbar 为图库设定单独一种actionbar布局 tangjun 2013.12.26
	 * 
	 * @param resID
	 */
	public void setAuroraPicContentView(int resID) {
		mActionBarType = AuroraActionBar.Type.Normal;
		setContentView(com.aurora.R.layout.aurora_content_normal_pic);
		onPreContentChanged();
		FrameLayout contentView = getContentView();// 获得内容区域，前端设置界面
		if ( contentView.getChildCount() > 0 ) {
			contentView.removeAllViews();
		}
		currentView = mInflater.inflate(resID, contentView);
		//setContentView(windowLayout);
		resetPaddingForPicContentView();
	}

	private void resetPaddingForPicContentView(){
		if(statusBarIsShowing()){
			View actionBar = findViewById(com.aurora.internal.R.id.aurora_action_bar);
			if(actionBar != null){
				actionBar.setPadding(actionBar.getPaddingLeft(),
						getResources().getDimensionPixelSize(com.aurora.R.dimen.status_bar_height),
						actionBar.getPaddingRight(),actionBar.getPaddingBottom());
				ViewGroup.LayoutParams params = actionBar.getLayoutParams();
				params.height = getResources().getDimensionPixelSize(com.aurora.R.dimen.aurora_action_bar_height_without_trasparent_status_2);
				actionBar.setLayoutParams(params);
				AuroraLog.e("activity","reset pic actionBar height");
			}
		}
	}

	// Aurora <Luofu> <2013-11-28> modify for SearchView begin
	
	public void addSearchviewInwindowLayout( ) {
		mNeedSearchView = true;
		addSearchview();
		initSearchView();
	}
	
	public void setAuroraActionbarSplitLineVisibility(int visibility) {
		View splitLineView = findViewById(com.aurora.internal.R.id.aurora_action_bar_split_line);
		if ( splitLineView != null ) {
			splitLineView.setVisibility(visibility);
		}
	}
	
	public void setOnQueryTextListener(OnSearchViewQueryTextChangeListener listener){
	    this.mSearchViewQueryTextListener = listener;
	}
	
	public AuroraSearchView getSearchView(){
		if (mNeedSearchView) {
			if(mSearchviewlayout == null){
				addSearchview();
			}
			
			if(mSearchView == null){
				initSearchView();
			}
		}
	    return mSearchView;
	}
	
	public View getSearchViewGreyBackground(){
		getSearchView();
	    return mSearchBackgroud;
	}
	
	public View getWindowLayout ( ) {
		return windowLayout;
	}
	
	public void initSearchView(){
		if(mActionBarType == Type.NEW_COSTOM){
			mSearchView = (AuroraSearchView)findViewById(com.aurora.R.id.aurora_activity_searchviewwidget);
		} else {
			if (getAuroraActionBar() != null) {
				mSearchView = getAuroraActionBar().getAuroraActionbarSearchView();
			}
		}
	    mSearchBackgroud = findViewById(com.aurora.R.id.aurora_activity_searchview_background);
	    mSearchViewBorder = mSearchView.getBackgroundView();
//	    mSearchView.setKeyBackListener(new OnKeyBackListener() {
//            
//            @Override
//            public void pressed() {
//                // TODO Auto-generated method stub
//                if(mSearchviewlayout != null){
//                    if(mSearchviewlayout.getVisibility() == View.VISIBLE){
//                        hideSearchviewLayout();
//                    }
//                }
//            }
//        });
	    if(mSearchBackgroud != null){
	        mSearchBackgroud.setOnClickListener(new OnClickListener() {
                
	        	@Override
	        	public void onClick(View v) {
	        		hideSearchviewLayout( );
	        		if ( searchBackgroundClickListener != null ) {
	        			searchBackgroundClickListener.searchBackgroundClick();
	        		}
	        	}
	        });
	    }
	    mSearchView.setOnQueryTextListener(new OnQueryTextListener() {
            
            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub
                if(mSearchViewQueryTextListener != null){
                	if ( !TextUtils.isEmpty(query) ) {
                		mSearchBackgroud.setVisibility(View.GONE);
                	} else {
                		mSearchBackgroud.setVisibility(View.VISIBLE);
                	}
                   return mSearchViewQueryTextListener.onQueryTextSubmit(query);
                }
                return false;
            }
            
            @Override
            public boolean onQueryTextChange(String newText) {
                // TODO Auto-generated method stub
                if(mSearchViewQueryTextListener != null){
                	if ( !TextUtils.isEmpty(newText) ) {
                		mSearchBackgroud.setVisibility(View.GONE);
                	} else {
                		mSearchBackgroud.setVisibility(View.VISIBLE);
                	}
                    return mSearchViewQueryTextListener.onQueryTextChange(newText);
                 }
                return false;
            }
        });
	}
	
	// Aurora <Luofu> <2013-11-28> modify for SearchView end

	public View getCurrentView() {
		return currentView;
	}

	public void setAuroraContentView(int resID) {
		setAuroraContentView(resID, AuroraActionBar.Type.Normal);
	}

	public void onPreContentChanged() {
		windowLayout = (FrameLayout) getWindow().getDecorView();//(FrameLayout) findViewById(com.aurora.internal.R.id.windowLayout);
		mActionBarHost = (AuroraActionBarHost) findViewById(com.aurora.internal.R.id.aurora_action_bar_host);
		if (mActionBarHost == null) {
			throw new RuntimeException(
					"Your content must have an ActionBarHost whose id attribute is R.id.aurora_action_bar_host");
		}
		mActionBar = mActionBarHost.getActionBar();
		mActionBarHost.getActionBar().setOnAuroraActionBarListener(
				mActionBarListener);
		// windowLayout.setBackgroundDrawable(getWallpaper());
	}

	@Override
	public FrameLayout getContentView() {// 获取内容区域
		
		if(mActionBarType == Type.NEW_COSTOM){
		    FrameLayout contentView = (FrameLayout)findViewById(com.aurora.R.id.aurora_custom_action_bar_bottom_widget);
		    return contentView;
		}
		ensureLayout();
		return mActionBarHost.getContentView();
	}

	public AuroraActionBar getAuroraActionBar() {// 获得actionbar
		if(mInvokeAuroraSetContentView){
		ensureLayout();
		if (mActionBar == null) {
		    if(mActionBarHost == null){
		      mActionBarHost = (AuroraActionBarHost) findViewById(com.aurora.internal.R.id.aurora_action_bar_host);
		    }
		    mActionBar = mActionBarHost == null?null:mActionBarHost.getActionBar();
		}
	}
		return mActionBar;
	}
	
	public Button getSearchViewRightButton(){
	    return cancelBtn;
	}
	
	private void initSearchViewAnimation(){
	    if(mSearchViewUpAnimation == null||mSearchViewScaleBigAnimation == null ||mSearchViewButtonShowAnimation == null
				|| mSearchViewDownAnimation == null
				|| mSearchViewScaleSmallAnimation == null
				|| mSearchViewButtonHideAnimation == null) {
			mSearchViewUpAnimation = AnimationUtils.loadAnimation(this,
					com.aurora.R.anim.aurora_activity_searchviewlayout_enter);
			mSearchViewScaleBigAnimation = AnimationUtils.loadAnimation(this,
					com.aurora.R.anim.aurora_activity_searchviewwidget_enter);
			mSearchViewButtonShowAnimation = AnimationUtils.loadAnimation(this,
					com.aurora.R.anim.aurora_activity_searchviewbutton_enter);

			mSearchViewDownAnimation = AnimationUtils.loadAnimation(this,
					com.aurora.R.anim.aurora_activity_searchviewlayout_exit);
			mSearchViewScaleSmallAnimation = AnimationUtils.loadAnimation(this,
					com.aurora.R.anim.aurora_activity_searchviewwidget_exit);
			mSearchViewButtonHideAnimation = AnimationUtils.loadAnimation(this,
					com.aurora.R.anim.aurora_activity_searchviewbutton_exit);
		}
	   
	}
	
	public void setSearchViewAnimDuration( long searchViewAnimDuration) {
		mSearchViewAnimDuration = searchViewAnimDuration;
	}
	
	public void showSearchviewLayoutWithOnlyAlphaAnim( ) {
		getSearchView();
		if(!mNeedSearchView){
	        return;
	    }
		
		if ( !isSearchviewAnimRun ) {

			isCanClickToHide = true;
			
			Animation searchviewlayoutAlphaAnim = AnimationUtils.loadAnimation(this, com.aurora.R.anim.aurora_activity_searchviewonlyalpha_enter);
			
			mSearchviewlayout.setVisibility(View.VISIBLE);
			mSearchBackgroud.setVisibility(View.VISIBLE);
			
			searchviewlayoutAlphaAnim.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					
					mSearchView.getFocus();
					
					isSearchviewAnimRun = false;
					mSearchviewlayout.clearAnimation();
				}
			});

			mSearchviewlayout.startAnimation(searchviewlayoutAlphaAnim);
			isSearchviewAnimRun = true;
			
			if ( mSearchViewAnimStopBackGdDrawable != null ) {
				mSearchviewBack.setBackground(mSearchViewAnimStopBackGdDrawable);
			} else {
				mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg);
			}
		}
	}
	
	public void hideSearchViewLayoutWithOnlyAlphaAnim( ) {
		getSearchView();
        if(!mNeedSearchView){
            return;
        }
        initSearchViewAnimation();
       // initSearchViewAnimation();
        if ( !isSearchviewAnimRun && isCanClickToHide ) {

            isCanClickToHide = false;
			
            if(mClearSearchViewText){
                mSearchView.clearText();
            }
            
			Animation searchviewlayoutAlphaAnim = AnimationUtils.loadAnimation(this, com.aurora.R.anim.aurora_activity_searchviewonlyalpha_exit);

			searchviewlayoutAlphaAnim.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					mSearchView.clearEditFocus();
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					mSearchviewlayout.setVisibility(View.GONE);
                    mSearchBackgroud.setVisibility(View.GONE);

                    mSearchviewlayout.clearAnimation();
                    isSearchviewAnimRun = false;

                    if(quitListener != null){
                        quitListener.quit();
                    }
				}
			});
            
            mSearchviewlayout.startAnimation(searchviewlayoutAlphaAnim);
            isSearchviewAnimRun = true;
            
			if ( mSearchViewAnimStopBackGdDrawable != null ) {
				mSearchviewBack.setBackground(mSearchViewAnimStopBackGdDrawable);
			} else {
				mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg);
			}
        }
	}
	
	public void setSearchViewTextSize(float size){
		if(mSearchView != null){
			mSearchView.setTextSize(size);
		}
	}
	
	private float getAuroraActionbarHeight( ) {
		
		//DensityUtil.dip2px(AuroraActivity.this, 42);
		float height = this.getResources().getDimensionPixelSize(com.aurora.R.dimen.aurora_activity_upanim_height);
		
		Log.e("666666", "-getAuroraActionbarHeight height = ---" + height);
		
		return height;
	}
	
	/**
	 * showSearchviewLayout tangjun 2013.11.28
	 */
	public void showSearchviewLayout( ) {
		
		if (mActionBarType == Type.NEW_COSTOM) {
			getSearchView();
		    if(!mNeedSearchView){
		        return;
		    }
		    initSearchViewAnimation();
			if ( !isSearchviewAnimRun ) {

				isCanClickToHide = true;
				if ( mSearchViewAnimRunBackGdDrawable != null ) {
					mSearchviewBack.setBackground(mSearchViewAnimRunBackGdDrawable);
				} else {
					mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg2);
				}

				float actionbarHeight = getAuroraActionbarHeight( );
				Log.e("111111", "---actionbarHeight = ----" + actionbarHeight);
				//Animation mActionBarHostAnimation = AnimationUtils.loadAnimation(this, com.aurora.R.anim.aurora_activity_searchviewhost_enter);
				Animation mActionBarHostAnimation = new TranslateAnimation(0, 0, actionbarHeight, 0);
				mActionBarHostAnimation.setFillAfter(true);
				mActionBarHostAnimation.setDuration(250);
				mActionBarHostAnimation.setInterpolator(new DecelerateInterpolator());
				//对actionbar单独跑动画是因为在U3上面对mActionBarHost先设置TranslationY再跑动画会有问题(U2无此问题)
				Animation mActionBarAnimation = AnimationUtils.loadAnimation(this, com.aurora.R.anim.aurora_activity_searchviewactionbar_enter);
				if ( mSearchViewAnimDuration != -1 ) {
					mActionBarHostAnimation.setDuration(mSearchViewAnimDuration);
					mActionBarAnimation.setDuration(mSearchViewAnimDuration);
				}
				if(mActionBarType == Type.NEW_COSTOM){
					mAuroraCustomActionBarHost.setTranslationY(-actionbarHeight);
					mAuroraCustomActionBarHost.startAnimation(mActionBarHostAnimation);
					mAuroraCustomActionBar.startAnimation(mActionBarAnimation);
				}else{
					mActionBarHost.setTranslationY(-actionbarHeight);
					mActionBarHost.startAnimation(mActionBarHostAnimation);
					mActionBar.startAnimation(mActionBarAnimation);
				}

				mSearchviewlayout.setVisibility(View.VISIBLE);
				mSearchBackgroud.setVisibility(View.VISIBLE);
				mSearchView.getFocus();
				
				mActionBarHostAnimation.setAnimationListener(new AnimationListener() {
					
					@Override
					public void onAnimationStart(Animation animation) {
						// TODO Auto-generated method stub

					}
					
					@Override
					public void onAnimationRepeat(Animation animation) {
						// TODO Auto-generated method stub
						
					}
					
					@Override
					public void onAnimationEnd(Animation animation) {
						// TODO Auto-generated method stub
						isSearchviewAnimRun = false;
						cancelBtn.clearAnimation();
						mSearchViewBorder.clearAnimation();
						mSearchviewlayout.clearAnimation();
						
						if ( mSearchViewAnimStopBackGdDrawable != null ) {
							mSearchviewBack.setBackground(mSearchViewAnimStopBackGdDrawable);
						} else {
							mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg);
						}
						
						if(mActionBarType == Type.NEW_COSTOM){
				            mAuroraCustomActionBarHost.clearAnimation();
				            mAuroraCustomActionBar.clearAnimation();
				        }else{
				            mActionBarHost.clearAnimation();
				            mActionBar.clearAnimation();
				        }
						
						int searchContentPaddingTop = mSearchviewBack.getMeasuredHeight();
						mSearchBackgroud.setPadding(mSearchBackgroud.getPaddingLeft(),
								searchContentPaddingTop, mSearchBackgroud.getPaddingRight(), mSearchBackgroud.getPaddingBottom());
						
					}
				});
			    
				cancelBtn.startAnimation(mSearchViewButtonShowAnimation);
				mSearchViewBorder.startAnimation(mSearchViewScaleBigAnimation);
				mSearchviewlayout.startAnimation(mSearchViewUpAnimation);
				if ( mSearchViewAnimDuration != -1 ) {
					mSearchViewUpAnimation.setDuration(mSearchViewAnimDuration);
					mSearchViewButtonShowAnimation.setDuration(mSearchViewAnimDuration);
					mSearchViewScaleBigAnimation.setDuration(mSearchViewAnimDuration);
				}
				isSearchviewAnimRun = true;
			}
		} else {
			if( getAuroraActionBar() != null && !getAuroraActionBar().getIsPlaySearchViewAnim()) {
				getAuroraActionBar().playAuroraActionbarSearchViewEntryAnim();
			}
		}
	}
	
	/**
	 * showSearchviewLayout tangjun 2013.11.28
	 */
	public void showSearchviewLayout_Ex( ) {
		getSearchView();
	    if(!mNeedSearchView){
	        return;
	    }
		
		if ( !isSearchviewAnimRun ) {

			isCanClickToHide = true;
			if ( mSearchViewAnimRunBackGdDrawable != null ) {
				mSearchviewBack.setBackground(mSearchViewAnimRunBackGdDrawable);
			} else {
				mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg2);
			}
			/*
			AuroraTranslateAnimation anim2 = new AuroraTranslateAnimation(mActionBarHost, -AuroraUtil.ACTION_BAR_HEIGHT_PX, 0);
		    anim2.setDuration(300);
		    anim2.setInterpolator(this, android.R.anim.accelerate_interpolator);
		    mActionBarHost.startAnimation(anim2);
		    */
			 
            float actionbarHeight = getAuroraActionbarHeight( );
//            Log.e("222222", "--actionbarHeight = ---" + actionbarHeight);
            ObjectAnimator translateOut = null;
            if(mActionBarType == Type.NEW_COSTOM){
//                Log.e("new", "********");
                translateOut = ObjectAnimator.ofFloat(mAuroraCustomActionBarHost, "TranslationY", 0f,-actionbarHeight);
            }else{
                translateOut = ObjectAnimator.ofFloat(mActionBarHost, "TranslationY", 0f,-actionbarHeight);
            }
			 
			if ( mSearchViewAnimDuration != -1 ) {
				translateOut.setDuration(mSearchViewAnimDuration);
			} else {
				translateOut.setDuration(250);
			}
			translateOut.setInterpolator(new AccelerateInterpolator());
			translateOut.addListener(new AnimatorListener() {
				
				@Override
				public void onAnimationStart(Animator animation) {
					// TODO Auto-generated method stub
					mSearchviewlayout.setVisibility(View.VISIBLE);
					mSearchBackgroud.setVisibility(View.VISIBLE);
					
					mSearchView.getFocus();
				}
				
				@Override
				public void onAnimationRepeat(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animator animation) {
					// TODO Auto-generated method stub
					isSearchviewAnimRun = false;
					//mActionBarHost.clearAnimation();
					cancelBtn.clearAnimation();
					mSearchViewBorder.clearAnimation();
					mSearchviewlayout.clearAnimation();
					
					if ( mSearchViewAnimStopBackGdDrawable != null ) {
						mSearchviewBack.setBackground(mSearchViewAnimStopBackGdDrawable);
					} else {
						mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg);
					}
				}
				
				@Override
				public void onAnimationCancel(Animator animation) {
					// TODO Auto-generated method stub
					
				}
			});
			translateOut.start();
		    
			cancelBtn.startAnimation(mSearchViewButtonShowAnimation);
			mSearchViewBorder.startAnimation(mSearchViewScaleBigAnimation);
			mSearchviewlayout.startAnimation(mSearchViewUpAnimation);
			if ( mSearchViewAnimDuration != -1 ) {
				mSearchViewUpAnimation.setDuration(mSearchViewAnimDuration);
				mSearchViewButtonShowAnimation.setDuration(mSearchViewAnimDuration);
				mSearchViewScaleBigAnimation.setDuration(mSearchViewAnimDuration);
			}
			isSearchviewAnimRun = true;
			
			/*
			anim2.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					
					mSearchviewlayout.setVisibility(View.VISIBLE);
					mSearchBackgroud.setVisibility(View.VISIBLE);
					
					mSearchView.getFocus();
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					
					isSearchviewAnimRun = false;
					mActionBarHost.clearAnimation();
					cancelBtn.clearAnimation();
					mSearchViewBorder.clearAnimation();
					//mSearchviewlayout.clearAnimation();
					
//					FrameLayout.LayoutParams layoutParamsOfMoveView = (FrameLayout.LayoutParams) mActionBarHost.getLayoutParams();
//					layoutParamsOfMoveView.topMargin = layoutParamsOfMoveView.topMargin - AuroraUtil.ACTION_BAR_HEIGHT_PX;
//					mActionBarHost.setLayoutParams(layoutParamsOfMoveView);
//					mActionBarHost.requestLayout();
					
					//mSearchView.getFocus();
					
					mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg);
				}
			});
			*/
		}
	}
	
	public void showSearchviewLayoutWithNoAnim( ) {
		getSearchView();
	    if(!mNeedSearchView){
	        return;
	    }
		
		float actionbarHeight = getAuroraActionbarHeight( );
        
        if(mActionBarType == Type.NEW_COSTOM){
            mAuroraCustomActionBarHost.setTranslationY(-actionbarHeight);
        }else{
            mActionBarHost.setTranslationY(-actionbarHeight);
        }
        
		mSearchviewlayout.setVisibility(View.VISIBLE);
		mSearchBackgroud.setVisibility(View.VISIBLE);
		
		mSearchView.getFocus();
		
		isSearchviewAnimRun = false;
		cancelBtn.clearAnimation();
		mSearchViewBorder.clearAnimation();
		mSearchviewlayout.clearAnimation();
		
		if ( mSearchViewAnimStopBackGdDrawable != null ) {
			mSearchviewBack.setBackground(mSearchViewAnimStopBackGdDrawable);
		} else {
			mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg);
		}
	}
	
	public void hideSearchViewLayoutWithNoAnim ( ) {
		getSearchView();
	    if(!mNeedSearchView){
	        return;
	    }
	    initSearchViewAnimation();
		float actionbarHeight = getAuroraActionbarHeight( );
        
        if(mActionBarType == Type.NEW_COSTOM){
            mAuroraCustomActionBarHost.setTranslationY(0);
        }else{
            mActionBarHost.setTranslationY(0);
        }
        
		mSearchView.clearEditFocus();
		mSearchviewlayout.setVisibility(View.GONE);
        mSearchBackgroud.setVisibility(View.GONE);
        //mActionBarHost.clearAnimation();
        cancelBtn.clearAnimation();
        mSearchViewBorder.clearAnimation();
        mSearchviewlayout.clearAnimation();
        isSearchviewAnimRun = false;

        if(quitListener != null){
            quitListener.quit();
        }
	}
	
	public void hideSearchViewLayout(boolean clearText){
		
		if (mActionBarType == Type.NEW_COSTOM) {
		    mClearSearchViewText = clearText;
		    hideSearchViewInternal();
		} else {
			if( getAuroraActionBar() != null && getAuroraActionBar().getIsPlaySearchViewAnim()) {
				getAuroraActionBar().playAuroraActionbarSearchViewExitAnim();
			}
		}
	}
	
	private void hideSearchViewInternal_Ex(){
		getSearchView();
        if(!mNeedSearchView){
            return;
        }
        initSearchViewAnimation();
        if ( !isSearchviewAnimRun && isCanClickToHide ) {

            isCanClickToHide = false;
            
			if ( mSearchViewAnimRunBackGdDrawable != null ) {
				mSearchviewBack.setBackground(mSearchViewAnimRunBackGdDrawable);
			} else {
				mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg2);
			}
			
            if(mClearSearchViewText){
                mSearchView.clearText();
            }
            
            float actionbarHeight = getAuroraActionbarHeight( );
			//Animation mActionBarHostAnimation = AnimationUtils.loadAnimation(this, com.aurora.R.anim.aurora_activity_searchviewhost_exit);
            Animation mActionBarHostAnimation = new TranslateAnimation(0, 0, -actionbarHeight, 0);
            mActionBarHostAnimation.setDuration(250);
            mActionBarHostAnimation.setInterpolator(new DecelerateInterpolator());
			if ( mSearchViewAnimDuration != -1 ) {
				mActionBarHostAnimation.setDuration(mSearchViewAnimDuration);
			}
			if(mActionBarType == Type.NEW_COSTOM){
				mAuroraCustomActionBarHost.setTranslationY(0);
				mAuroraCustomActionBarHost.startAnimation(mActionBarHostAnimation);
            }else{
            	mActionBarHost.setTranslationY(0);
                mActionBarHost.startAnimation(mActionBarHostAnimation);
            }
			mActionBarHostAnimation.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					mSearchView.clearEditFocus();
				}
				
				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					mSearchviewlayout.setVisibility(View.GONE);
                    mSearchBackgroud.setVisibility(View.GONE);
                    cancelBtn.clearAnimation();
                    mSearchViewBorder.clearAnimation();
                    mSearchviewlayout.clearAnimation();
                    isSearchviewAnimRun = false;
                    
        			if ( mSearchViewAnimStopBackGdDrawable != null ) {
        				mSearchviewBack.setBackground(mSearchViewAnimStopBackGdDrawable);
        			} else {
        				mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg);
        			}

                    if(quitListener != null){
                        quitListener.quit();
                    }
                    
                    if(mActionBarType == Type.NEW_COSTOM){
			            mAuroraCustomActionBarHost.clearAnimation();
			        }else{
			            mActionBarHost.clearAnimation();
			        }
				}
			});
            
            cancelBtn.startAnimation(mSearchViewButtonHideAnimation);
            mSearchViewBorder.startAnimation(mSearchViewScaleSmallAnimation);
            mSearchviewlayout.startAnimation(mSearchViewDownAnimation);
            if ( mSearchViewAnimDuration != -1 ) {
            	mSearchViewButtonHideAnimation.setDuration(mSearchViewAnimDuration);
            	mSearchViewDownAnimation.setDuration(mSearchViewAnimDuration);
			}
            isSearchviewAnimRun = true;
        }
	}
	
	private void hideSearchViewInternal(){
		getSearchView();
        if(!mNeedSearchView){
            return;
        }
        initSearchViewAnimation();
        if ( !isSearchviewAnimRun && isCanClickToHide ) {

            isCanClickToHide = false;
            
			if ( mSearchViewAnimRunBackGdDrawable != null ) {
				mSearchviewBack.setBackground(mSearchViewAnimRunBackGdDrawable);
			} else {
				mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg2);
			}
			
            if(mClearSearchViewText){
                mSearchView.clearText();
            }
            /*
            AuroraTranslateAnimation anim2 = new AuroraTranslateAnimation(mActionBarHost, AuroraUtil.ACTION_BAR_HEIGHT_PX, 0);
            anim2.setDuration(300);
            anim2.setInterpolator(this, android.R.anim.accelerate_interpolator);
            mActionBarHost.startAnimation(anim2);
            */
            
            float actionbarHeight = getAuroraActionbarHeight( );
            ObjectAnimator translateOut = null;
            if(mActionBarType == Type.NEW_COSTOM){
                translateOut = ObjectAnimator.ofFloat(mAuroraCustomActionBarHost, "TranslationY", -actionbarHeight, 0f);
            }else{
                translateOut = ObjectAnimator.ofFloat(mActionBarHost, "TranslationY", -actionbarHeight, 0f);
            }
			
			if ( mSearchViewAnimDuration != -1 ) {
				translateOut.setDuration(mSearchViewAnimDuration);
			} else {
				translateOut.setDuration(250);
			}
			translateOut.addListener(new AnimatorListener() {
				
				@Override
				public void onAnimationStart(Animator animation) {
					// TODO Auto-generated method stub
					
					mSearchView.clearEditFocus();
				}
				
				@Override
				public void onAnimationRepeat(Animator animation) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animator animation) {
					// TODO Auto-generated method stub
					
					mSearchviewlayout.setVisibility(View.GONE);
                    mSearchBackgroud.setVisibility(View.GONE);
                    //mActionBarHost.clearAnimation();
                    cancelBtn.clearAnimation();
                    mSearchViewBorder.clearAnimation();
                    mSearchviewlayout.clearAnimation();
                    isSearchviewAnimRun = false;
                    
        			if ( mSearchViewAnimStopBackGdDrawable != null ) {
        				mSearchviewBack.setBackground(mSearchViewAnimStopBackGdDrawable);
        			} else {
        				mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg);
        			}

                    if(quitListener != null){
                        quitListener.quit();
                    }
				}
				
				@Override
				public void onAnimationCancel(Animator animation) {
					// TODO Auto-generated method stub
					
				}
			});
			translateOut.start();
            
            cancelBtn.startAnimation(mSearchViewButtonHideAnimation);
            mSearchViewBorder.startAnimation(mSearchViewScaleSmallAnimation);
            mSearchviewlayout.startAnimation(mSearchViewDownAnimation);
            if ( mSearchViewAnimDuration != -1 ) {
            	mSearchViewButtonHideAnimation.setDuration(mSearchViewAnimDuration);
            	mSearchViewDownAnimation.setDuration(mSearchViewAnimDuration);
			}
            isSearchviewAnimRun = true;
            /*
            anim2.setAnimationListener(new AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {
                    // TODO Auto-generated method stub
                    mSearchView.clearEditFocus();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    // TODO Auto-generated method stub
                    
                    mSearchviewlayout.setVisibility(View.GONE);
                    mSearchBackgroud.setVisibility(View.GONE);
                    mActionBarHost.clearAnimation();
                    cancelBtn.clearAnimation();
                    mSearchViewBorder.clearAnimation();
                    isSearchviewAnimRun = false;
                    mSearchviewBack.setBackgroundResource(com.aurora.internal.R.drawable.aurora_activity_searchbar_bg);

                    if(quitListener != null){
                        quitListener.quit();
                    }
                    
                }
            });
            */
        }
	}
	
	
	/**
	 * hideSearchviewLayout tangjun 2013.11.28
	 */
	public void hideSearchviewLayout( ) {
		if (mActionBarType == Type.NEW_COSTOM) {
		    hideSearchViewInternal();
		} else {
			if( getAuroraActionBar() != null && getAuroraActionBar().getIsPlaySearchViewAnim()) {
				getAuroraActionBar().playAuroraActionbarSearchViewExitAnim();
			}
		}
	}
	
	public boolean isSearchviewLayoutShow( ) {
		return isCanClickToHide;
	}

	public AuroraActionBarItem addAuroraActionBarItem(AuroraActionBarItem item) {
		return getAuroraActionBar().addItem(item);
	}

	public AuroraActionBarItem addAuroraActionBarItem(AuroraActionBarItem item,
			int itemId) {
		return getAuroraActionBar().addItem(item, itemId);
	}

	public AuroraActionBarItem addAuroraActionBarItem(
			AuroraActionBarItem.Type actionBarItemType) {
		return getAuroraActionBar().addItem(actionBarItemType);
	}

	public AuroraActionBarItem addAuroraActionBarItem(
			AuroraActionBarItem.Type actionBarItemType, int itemId) {
		return getAuroraActionBar().addItem(actionBarItemType, itemId);
	}

	@Override
	/**
	 * 拦截MENU
	 */
	public boolean onMenuOpened(int featureId, Menu menu) {
		if (mActionBarHost != null && mIsNeedShowMenuWhenKeyMenuClick) {
			showAuroraMenu();
			return false;
		}
		return true;// 返回为true 则显示系统menu
	}
	
	public void setIsNeedShowMenuWhenKeyMenuClick(boolean isNeedShowMenuWhenKeyMenuClick) {
		mIsNeedShowMenuWhenKeyMenuClick = isNeedShowMenuWhenKeyMenuClick;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("menu");
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * 菜单消失
	 */
	public void dismissAuroraMenu() {
		if (auroraMenu != null && auroraMenu.isShowing()) {
			auroraMenu.dismiss();
		}
		if (auroraCustomMenu != null && auroraCustomMenu.isShowing()) {
//		    Log.e("mm", "************");
            auroraCustomMenu.dismiss();
		}
		removeCoverView();
		// alphaListener.startMenuDismissThread();
	}

	private void loadAnimation(int animId) {
//		try {
//			coverAnimation = AnimationUtils.loadAnimation(this, animId);
//			mCoverView.startAnimation(coverAnimation);
//			
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
	}
	
	public View getCoverView(){
		return mCoverView;
	}

	public void addCoverView() {
//		//if (mCoverView == null) {
//			mCoverView = new TextView(this);
//			mCoverView.setBackgroundColor(mCoverViewColor);
//			mCoverView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.FILL_PARENT));
//			windowLayout.addView(mCoverView);
//			Log.e("111111", "----addCoverView----------");
//		//}
//		loadAnimation(com.aurora.R.anim.aurora_menu_cover_enter);
	}

	public void removeCoverView() {
//		Log.e("111111", "----removeCoverView----------");
//	    if(mCoverView != null && windowLayout!=null){
//	        windowLayout.removeView(mCoverView);
//	    }
//		loadAnimation(com.aurora.R.anim.aurora_menu_cover_exit);
	}

	/**
	 * 显示菜单
	 */
	public void showAuroraMenu() {
		getAuroraActionBar().setMenuType(AuroraMenu.Type.System);
		if (auroraMenu != null && menuIsEnable) {
			if (auroraMenu.isShowing()) {
				dismissAuroraMenu();
			} else {
				auroraMenu.showAtLocation(getWindow().getDecorView(),
						Gravity.BOTTOM, 0, 0);
				// alphaListener.startMenuShowThread();
			}
		}
	}
	
	public void showAuroraMenu(View parent, int gravity, int x, int y) {
		if (auroraMenu != null && menuIsEnable) {
			if (auroraMenu.isShowing()) {
				dismissAuroraMenu();
			} else {
				auroraMenu.showAtLocation(parent, gravity, x, y);
				addCoverView();
			}
		}
	}

	public void setMenuEnable(Boolean enable) {
		menuIsEnable = enable;
	}

	/**
	 * @return 得到 aurora menu 当前状态
	 */
	public Boolean auroraMenuIsEnable() {
		return menuIsEnable;
	}

	/**
	 * 设置menu菜单
	 * 
	 * @param menu
	 */
	public void setAuroraMenuItems(int menu, int resId) {
		//aurora add by tangjun start 2014.1.9
//		Log.e("222222", "setAuroraMenuItems----lastMenu = " + lastMenu);
//		Log.e("222222", "setAuroraMenuItems----menu = " + menu);
		if ( lastMenu == menu ) {
			//aurora add by tangjun 2014.5.20 修改两次menu相等直接返回时要把该标志位置为false，
			//防止下次由AuroraMenu变成AuroraSystemMenu时，initMenuData()方法还是会跑AuroraMenu的bug
			getAuroraActionBar().setActionBottomBarMenu(false);
			return;
		}
		lastMenu = menu;
		//aurora add by tangjun end 2014.1.9
		
		parseMenu(menu);
		initMenuData(resId);
	}

	public void setAuroraMenuItems(int menu) {
		setAuroraMenuItems(menu, com.aurora.R.layout.aurora_menu_normal);
	}

	/**
	 * @return 得到aurora菜单
	 */
	public AuroraSystemMenu getAuroraMenu() {
		return auroraMenu;
	}
	
	/**
	 * @return 得到auroracustommenu菜单
	 */
	public AuroraCustomMenu getAuroraCustomMenu() {
		return auroraCustomMenu;
	}

	/**
	 * 解析menu.xml
	 * 
	 * @param menu
	 */
	private void parseMenu(int menu) {
		if (getAuroraActionBar().isActionBottomBarMenu()) {
			mAuroraMenumenuItems = new ArrayList<AuroraMenuItem>();
			mAuroraMenumenuIds = new HashMap<Integer, Integer>();
		} else {
			mAuroraSystemMenumenuItems = new ArrayList<AuroraMenuItem>();
			mAuroraSystemMenumenuIds = new HashMap<Integer, Integer>();
		}

		AuroraMenuItem item = null;
		try {
			XmlResourceParser xpp = getResources().getXml(menu);
			xpp.next();
			int eventType = xpp.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_TAG) {
					startParseMenuDataXml(xpp);
				}
				eventType = xpp.next();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 开始解析menu xml
	 * 
	 * @param xpp
	 */
	private void startParseMenuDataXml(XmlResourceParser xpp) {
		AuroraMenuItem item;
		if (AuroraUtil.MENU_ITEM.equals(xpp.getName())) {

			String titleId = xpp.getAttributeValue(AuroraUtil.ANDROID_XMLNS,
					AuroraUtil.MENU_TITLE);
			String iconId = xpp.getAttributeValue(AuroraUtil.ANDROID_XMLNS,
					AuroraUtil.MENU_ICON);
			String resId = xpp.getAttributeValue(AuroraUtil.ANDROID_XMLNS,
					AuroraUtil.MENU_ID);
			item = new AuroraMenuItem();
			if (titleId != null) {
				item.setTitle(Integer.valueOf(titleId.replace("@", "")));// 通过R.string设置
				item.setTitleText(this.getResources().getString(item.getTitle()));
			}
			if (iconId != null) {
				item.setIcon(Integer.valueOf(iconId.replace("@", "")));
			}
			if (resId != null) {
				item.setId(Integer.valueOf(resId.replace("@", "")));
			}
			if (getAuroraActionBar().isActionBottomBarMenu()) {
				mAuroraMenumenuItems.add(item);
				mAuroraMenumenuIds.put((Integer) item.getId(), mAuroraMenumenuItems.indexOf(item));// 设置itemId,position对应关系
			} else {
				mAuroraSystemMenumenuItems.add(item);
				mAuroraSystemMenumenuIds.put((Integer) item.getId(), mAuroraSystemMenumenuItems.indexOf(item));// 设置itemId,position对应关系
			}
		}
	}

	public void initMenuData(int resId) {
		if (getAuroraActionBar().isActionBottomBarMenu()) {
			//if (this.auroraActionBottomBarMenuAdapter == null) {
				this.auroraActionBottomBarMenuAdapter = new AuroraActionBottomBarMenuAdapter(
						this, mAuroraMenumenuItems);
			//}
			auroraActionBottomBarMenu = new AuroraMenu(this,
					this.getAuroraActionBar(), this.auroraMenuCallBack,
					this.auroraActionBottomBarMenuAdapter,
					com.aurora.R.style.ActionBottomBarPopupAnimation, resId);
			auroraActionBottomBarMenu.setMenuIds(mAuroraMenumenuIds);
			getAuroraActionBar().setAuroraActionBottomBarMenu(
					auroraActionBottomBarMenu);
			auroraActionBottomBarMenu.update();
			getAuroraActionBar().setActionBottomBarMenu(false);
			return;
		}

		//if (this.auroraMenuAdapter == null) {
			this.auroraMenuAdapter = new AuroraSystemMenuAdapter(this,
					mAuroraSystemMenumenuItems);
		//}
		auroraMenu = new AuroraSystemMenu(this, this.auroraSystemMenuCallBack,
				this.auroraMenuAdapter, com.aurora.R.style.PopupAnimation,
				resId);
		auroraMenu.setMenuIds(mAuroraSystemMenumenuIds);
		auroraMenu.setPullLvHeight(auroraMenu.menuListView);
		auroraMenu.update();
		alphaListener = new AuroraAlphaListener(this);
	}

	public AuroraMenuAdapterBase getAuroraMenuAdapter() {
		return auroraMenuAdapter;
	}

	public void setAuroraMenuAdapter(AuroraMenuAdapterBase auroraMenuAdapter) {
		this.auroraMenuAdapter = auroraMenuAdapter;
	}

	public AuroraMenuAdapterBase getAuroraActionBottomBarMenuAdapter() {
		return auroraActionBottomBarMenuAdapter;
	}

	public void setAuroraActionBottomBarMenuAdapter(
			AuroraMenuAdapterBase auroraActionBottomBarMenuAdapter) {
		this.auroraActionBottomBarMenuAdapter = auroraActionBottomBarMenuAdapter;
	}

	public void setAuroraBottomBarMenuCallBack(
			AuroraMenuBase.OnAuroraMenuItemClickListener auroraMenuCallBack) {
		this.auroraMenuCallBack = auroraMenuCallBack;
		if (auroraActionBottomBarMenu != null) {
			auroraActionBottomBarMenu.setMenuItemClickListener(auroraMenuCallBack);
		}
	}
	
	public void setAuroraSystemMenuCallBack(
			AuroraMenuBase.OnAuroraMenuItemClickListener auroraSystemMenuCallBack) {
		this.auroraSystemMenuCallBack = auroraSystemMenuCallBack;
		if (auroraMenu != null) {
			auroraMenu.setSystemMenuItemClickListener(auroraSystemMenuCallBack);
		}
	}

	public void addAuroraMenuItemById(int itemId) {
		auroraMenu.addMenuItemById(itemId);
	}

	public void removeAuroraMenuItemById(int itemId) {
		auroraMenu.removeMenuItemById(itemId);
	}

	public void addAuroraActionBottomBarMenuItemById(int itemId) {
		auroraActionBottomBarMenu.addMenuItemById(itemId);
	}

	public void removeAuroraActionBottomBarMenuItemById(int itemId) {
		auroraActionBottomBarMenu.removeMenuItemById(itemId);
	}

	// Aurora <aven> <2013年9月16日> modify for custom view begin
	public AuroraActionBarHost getAuroraActionBarHost() {
		return mActionBarHost;
	}

	/**
	 * 设置actionbar的布局
	 * 
	 * @param resId
	 */
	public void setCustomView(int resId) {
		if (verifyLayout()) {
			ViewGroup view = (ViewGroup) mInflater.inflate(
					resId, null);
			views = new ArrayList<View>();
			for (int i = 0; i < mActionBar.getChildCount(); i++) {
				View view2 = mActionBar.getChildAt(i);
				views.add(view2);
			}
			mActionBar.removeAllViews();
			LayoutParams lp = new LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT);
			mActionBar.addView(view, lp);
		}
	}

	public void goToSelectView() {
		if (verifyLayout()&&firstCreateAllOperation) {
			views = new ArrayList<View>();
			for (int i = 0; i < mActionBar.getChildCount(); i++) {
				View view2 = mActionBar.getChildAt(i);
				views.add(view2);
			}
			mActionBar.removeAllViews();
			mInflater.inflate(
					com.aurora.R.layout.aurora_action_bar_dashboard,
					mActionBar, true);
			Animation myAnimation_Alpha;
			myAnimation_Alpha = new AlphaAnimation(0.1f, 1.0f);
			myAnimation_Alpha.setDuration(1000);
			// mActionBar.startAnimation(myAnimation_Alpha);
			mLeftView = (TextView) mActionBar
					.findViewById(com.aurora.internal.R.id.aurora_action_bar_btn_cancel);
			mRightView = (TextView) mActionBar
					.findViewById(com.aurora.internal.R.id.aurora_action_bar_btn_right);
			mLeftView.setText(getResources().getString(
					com.aurora.R.string.aurora_action_bar_cancel_btn));
			mRightView.setText(getResources().getString(
					com.aurora.R.string.aurora_action_bar_ok_btn));

			mLeftView.startAnimation(myAnimation_Alpha);
			mRightView.startAnimation(myAnimation_Alpha);
			firstCreateAllOperation=false;
		}
	}

	public View getLeftButton() {
		return mLeftView;
	}

	public View getRightButton() {
		return mRightView;
	}

	/**
	 * 返回到初始布局
	 */
	public void backToFrontView() {
		mActionBar.removeAllViews();
		for (int i = 0; i < views.size(); i++) {
			View view2 = views.get(i);
			mActionBar.addView(view2);
		}
		firstCreateAllOperation=true;
	}

	/**
	 * @param viewId
	 * @return 得到actionbar的自定义布局
	 */
	public ViewGroup getCustomView() {
		return (ViewGroup) mActionBar.getChildAt(0);
	}

	// Aurora <aven> <2013年9月16日> modify for custom view end

	/*
	 * 对action bar menu 消失的处理，返回键消失
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		AuroraMenu auroraActionBarMenu = null;
		AuroraMenu actionBottomBarMenu = null;
		AuroraSystemMenu auroraMenu = null;
		boolean opt = false;

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			opt = closeMenu(auroraActionBarMenu, actionBottomBarMenu,
					auroraMenu, opt);
			if (mActionBarType == Type.NEW_COSTOM) {
				if(mSearchviewlayout != null){
				    if(mSearchviewlayout.getVisibility() == View.VISIBLE){
		                hideSearchviewLayout();
		                return true;
		            }
				}
			} else {
				if(getAuroraActionBar() != null && getAuroraActionBar().getIsPlaySearchViewAnim()) {
					hideSearchviewLayout();
					return true;
				}
			}
			
		} else if ( keyCode == KeyEvent.KEYCODE_MENU ) {
			if ( this.auroraMenuCallBack == null && this.auroraSystemMenuCallBack == null ) {
				return true;
			}
		}

		if (opt)
			return false;
		return super.onKeyDown(keyCode, event);
	}

	private boolean closeMenu(AuroraMenu auroraActionBarMenu,
			AuroraMenu actionBottomBarMenu, AuroraSystemMenu auroraMenu,
			boolean opt) {
		if (mActionBar != null) {
			auroraMenu = getAuroraMenu();
			auroraActionBarMenu = mActionBar.getActionBarMenu();
			actionBottomBarMenu = mActionBar.getAuroraActionBottomBarMenu();
		}
		if (auroraActionBarMenu != null && auroraActionBarMenu.isShowing()) {
			// auroraActionBarMenu.dismiss();
			mActionBar.showActionBarMenu();
			opt = true;
		}
		if (mActionBar != null && mActionBar.auroraIsEditMode()) {
			mActionBar.showActionBarDashBoard();
			opt = true;
		}
		if (actionBottomBarMenu != null && actionBottomBarMenu.isShowing()) {
			actionBottomBarMenu.dismiss();
			getAuroraActionBar().contentViewFloatDown();
			opt = true;
		}

		if (auroraMenu != null && auroraMenu.isShowing()) {
			dismissAuroraMenu();
			opt = true;
		}
		return opt;
	}
	
	
	
	
	public interface OnSearchViewQueryTextChangeListener{
	    
        public boolean onQueryTextSubmit(String query) ;
        
        public boolean onQueryTextChange(String newText);
	}
	public interface OnSearchViewQuitListener{
	    public boolean quit();
	}
	
	private OnSearchViewQuitListener quitListener;
	private OnActionbarSearchViewQuitListener actionbarQuitListener = new OnActionbarSearchViewQuitListener() {
		
		@Override
		public boolean quit() {
			// TODO Auto-generated method stub
	        if(quitListener != null){
	            quitListener.quit();
	        }
			return false;
		}
	};
	public void setOnSearchViewQuitListener(OnSearchViewQuitListener quitListener){
	    this.quitListener = quitListener;
	    if (mActionBarType != Type.NEW_COSTOM) {
	    	getAuroraActionBar().setOnActionbarSearchViewQuitListener(actionbarQuitListener);
	    }
	}
	
	public interface OnSearchBackgroundClickListener{
	    public boolean searchBackgroundClick();
	}
	
	private OnSearchBackgroundClickListener searchBackgroundClickListener;
	public void setOnSearchBackgroundClickListener(OnSearchBackgroundClickListener searchBackgroundClickListener){
	    this.searchBackgroundClickListener = searchBackgroundClickListener;
	}
	
	
	public class AuroraTranslateAnimation extends Animation {
	    private FrameLayout.LayoutParams mLayoutParamsOfMoveView;  
	    private View moveView;
	    private View changeAlphaView;
	    private int movingDistance;
	    private AnimationListener listener;

	    /**
	     * 
	     * @param moveView
	     * @param movingDistance
	     * @param type RIGHT = 0,LEFT = 1;
	     */
	    public AuroraTranslateAnimation(View moveView,int movingDistance,int type) {
	        this.moveView = moveView;
	        this.movingDistance = movingDistance;
	        this.changeAlphaView = changeAlphaView;
	        mLayoutParamsOfMoveView = ((FrameLayout.LayoutParams) moveView.getLayoutParams());
	    }
	    

	    @Override
	    protected void applyTransformation(float interpolatedTime, Transformation t) {
	        //super.applyTransformation(interpolatedTime, t);
	        if(movingDistance >0){
	        	//changeAlphaView.setAlpha(1-interpolatedTime);
	            mLayoutParamsOfMoveView.topMargin = (int)(movingDistance*interpolatedTime)-movingDistance;
            
	        }else{
	        	//changeAlphaView.setAlpha(interpolatedTime);
	            mLayoutParamsOfMoveView.topMargin = (int)(movingDistance*interpolatedTime);
	        }
	        moveView.setLayoutParams(mLayoutParamsOfMoveView);
	        
	    }
	}


    @Override
    public void callBack(boolean flag) {
        // TODO Auto-generated method stub
        if(auroraCustomMenu != null && auroraCustomMenu.isShowing()){
            auroraCustomMenu.dismiss();
        }
    }
	
	
	private boolean isOutOfBounds(Context context, MotionEvent event,View target) {
		final int x = (int) event.getX();
		final int y = (int) event.getY();
		final int slop = ViewConfiguration.get(context).getScaledWindowTouchSlop();
		return (x < -slop) || (y < -slop)
				|| (x > (target.getWidth()+slop))
				|| (y > (target.getHeight()+slop));
	}
	
	public interface OnSearchViewButtonClickListener{
	    public boolean onSearchViewButtonClick();
	}
	private OnSearchViewButtonClickListener mSearchViewButtonClickListener;
	public void setOnSearchViewButtonListener(OnSearchViewButtonClickListener searchViewButtonClickListener){
	    this.mSearchViewButtonClickListener = searchViewButtonClickListener;
	}
	
	@Override
	public void onHomePressed() {
		// TODO Auto-generated method stub
		//changeStatusBar(false);
	}

	@Override
	public void onHomeLongPressed() {
		// TODO Auto-generated method stub
		
	}
}
