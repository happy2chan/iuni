/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.app;

import com.android.internal.R;
import com.android.internal.app.AlertController.AlertParams;
import com.android.internal.app.AuroraResolverGridView.onItemSeletctedListener;
import com.android.internal.content.PackageMonitor;
import com.aurora.utils.Utils2Icon;

import android.app.ActivityManager;
import android.app.ActivityManagerNative;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.PatternMatcher;
import android.os.RemoteException;
import android.os.UserHandle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import aurora.app.AuroraAlertActivity;
import aurora.widget.AuroraCheckBox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * This activity is displayed when the system attempts to start an Intent for
 * which there is more than one matching activity, allowing the user to decide
 * which to go to.  It is not normally used directly by application developers.
 */
public class AuroraResolverActivity extends AuroraAlertActivity implements AdapterView.OnItemClickListener ,onItemSeletctedListener{
    private static final String TAG = "ResolverActivity";
    private static final boolean DEBUG = false;

    private int mLaunchedFromUid;
    private PackageManager mPm;
    boolean mAlwaysUseOption;
    boolean mShowExtended;
    private GridView mGrid;
    private Button mAlwaysButton;
    private Button mOnceButton;
    private AuroraCheckBox mAlwaysCheckBox;
    private TextView mTitle;
    private int mIconDpi;
    private int mIconSize;
    private int mMaxColumns;
    private int mLastSelected = GridView.INVALID_POSITION;

    private View mContentView;
    
    private boolean mRegistered;
    
    private AuroraResolverGridView mPagerGridView;
    
    List<DisplayResolveInfo> mList = new ArrayList<AuroraResolverActivity.DisplayResolveInfo>();
    
    private List<ResolveInfo> mBaseResolveList;
    
    private  Intent[] mInitialIntents;
    private  Intent mIntent;
    
    private final PackageMonitor mPackageMonitor = new PackageMonitor() {
        @Override public void onSomePackagesChanged() {
        	
        	if(mPagerGridView != null){
        		mPagerGridView.handlePackagesChanged();
        	}
        }
    };

    private Intent makeMyIntent() {
        Intent intent = new Intent(getIntent());
        // The resolver activity is set to be hidden from recent tasks.
        // we don't want this attribute to be propagated to the next activity
        // being launched.  Note that if the original Intent also had this
        // flag set, we are now losing it.  That should be a very rare case
        // and we can live with this.
        intent.setFlags(intent.getFlags()&~Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, makeMyIntent(),
                getResources().getText(com.aurora.R.string.whichApplication),
                null, null, true);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	// TODO Auto-generated method stub
    	
    	return super.onTouchEvent(event);
    }

    protected void onCreate(Bundle savedInstanceState, Intent intent,
            CharSequence title, Intent[] initialIntents, List<ResolveInfo> rList,
            boolean alwaysUseOption) {
    	
		onCreateInner( savedInstanceState,  intent,
             title, initialIntents,  rList,
             alwaysUseOption);
    }
	
    private void setGravity(){
    	Window window = getWindow();
    	WindowManager.LayoutParams attr = window.getAttributes();
    	attr.gravity = Gravity.BOTTOM;
    	window.setAttributes(attr);
    }
	
    public Intent intentForPosition(int position) {
        DisplayResolveInfo dri = mList.get(position);
        
        Intent intent = new Intent(dri.origIntent != null
                ? dri.origIntent : mIntent);
        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT
                |Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP);
        ActivityInfo ai = dri.ri.activityInfo;
        intent.setComponent(new ComponentName(
                ai.applicationInfo.packageName, ai.name));
        return intent;
    }
    
	protected void onCreateInner(Bundle savedInstanceState, Intent intent,
            CharSequence title, Intent[] initialIntents, List<ResolveInfo> rList,
            boolean alwaysUseOption) {
		mBaseResolveList = rList;
        setTheme(com.aurora.R.style.Theme_Aurora_Dialog_Alert_Chooser);
    
        showNaviBar();
        
        notAllowHideNaviBar();
        
        getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,   
                android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
                
        super.onCreate(savedInstanceState);
     // Aurora <Luofu> <2014-5-27> modify for share dialog begin
//        auroraSetParams(mAlertParams);
       // Aurora <Luofu> <2014-5-27> modify for share dialog end
        setGravity();
        try {
            mLaunchedFromUid = ActivityManagerNative.getDefault().getLaunchedFromUid(
                    getActivityToken());
        } catch (RemoteException e) {
            mLaunchedFromUid = -1;
        }
        mPm = getPackageManager();
        mAlwaysUseOption = alwaysUseOption;
        //mMaxColumns = getResources().getInteger(R.integer.config_maxResolverActivityColumns);
		
		setAlwaysUseOption();
//        setMaxColumns();
        
        intent.setComponent(null);
		
       com.aurora.internal.app.AuroraAlertController.AlertParams ap = mAlertParams;
		 
//        ap.mTitle = title;
		 
        mPackageMonitor.register(this, getMainLooper(), false);
        mRegistered = true;
		
        final ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        mIconDpi = am.getLauncherLargeIconDensity();
        mIconSize = am.getLauncherLargeIconSize();
		setImageSize();
		mIntent = new Intent(intent);
		mInitialIntents = initialIntents;
		 rebuildList();
       
        int count = mList.size();
        
        if (mLaunchedFromUid < 0 || UserHandle.isIsolated(mLaunchedFromUid)) {
            // Gulp!
            finish();
            return;
        } else if (count > 1) {
			
			
//            if (alwaysUseOption) {
//                mGrid.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
//            }
//			
        	mPagerGridView = new AuroraResolverGridView(this);
        	mPagerGridView.setCallBack(this);
        	mPagerGridView.init(mList, mIntent, mInitialIntents, mBaseResolveList, mLaunchedFromUid, mPm, this);
        	initGridView();
        	mTitle = (TextView)mPagerGridView.findViewById(com.aurora.R.id.aurora_alertTitle);
        	mTitle.setText(title);
        } else if (count == 1) {
            startActivity(intentForPosition(0));
            mPackageMonitor.unregister();
            mRegistered = false;
            finish();
            return;
        } else {
            ap.mTitle = getResources().getText(com.aurora.R.string.aurora_no_application_can_perform_this_operation);
            ap.mPositiveButtonText = getResources().getText(com.aurora.R.string.aurora_datepicker_done);
            ap.mNegativeButtonText = getResources().getText(com.aurora.R.string.aurora_datepicker_cancel);
        }
		 
        setupAlert();
		if(count > 1){
			setBottomBar();
		}
		
		mContentView = findViewById(com.aurora.R.id.aurora_parentPanel);
		
		View parent = findViewById(com.aurora.R.id.aurora_customPanel);
		
		if(parent != null){
			parent.setBackgroundColor(Color.TRANSPARENT);
		}
		
		
		View customer = findViewById(com.aurora.R.id.aurora_customPanel);
		if(customer != null){
			customer.setPadding(0,  customer.getPaddingTop(),0, customer.getPaddingBottom());
		}
		
    }
	
	  void rebuildList() {
	        List<ResolveInfo> currentResolveList;

	        mList.clear();
	        if (mBaseResolveList != null) {
	            currentResolveList = mBaseResolveList;
	        } else {
	            currentResolveList = mPm.queryIntentActivities(
	                    mIntent, PackageManager.MATCH_DEFAULT_ONLY
	                    | (mAlwaysUseOption ? PackageManager.GET_RESOLVED_FILTER : 0));
	            // Filter out any activities that the launched uid does not
	            // have permission for.  We don't do this when we have an explicit
	            // list of resolved activities, because that only happens when
	            // we are being subclassed, so we can safely launch whatever
	            // they gave us.
	            if (currentResolveList != null) {
	                for (int i=currentResolveList.size()-1; i >= 0; i--) {
	                    ActivityInfo ai = currentResolveList.get(i).activityInfo;
	                    int granted = ActivityManager.checkComponentPermission(
	                            ai.permission, mLaunchedFromUid,
	                            ai.applicationInfo.uid, ai.exported);
	                    if (granted != PackageManager.PERMISSION_GRANTED) {
	                        // Access not allowed!
	                        currentResolveList.remove(i);
	                    }
	                }
	            }
	        }
	        int N;
	        if ((currentResolveList != null) && ((N = currentResolveList.size()) > 0)) {
	            // Only display the first matches that are either of equal
	            // priority or have asked to be default options.
	            ResolveInfo r0 = currentResolveList.get(0);
	            for (int i=1; i<N; i++) {
	                ResolveInfo ri = currentResolveList.get(i);
	                if (r0.priority != ri.priority ||
	                    r0.isDefault != ri.isDefault) {
	                    while (i < N) {
	                        currentResolveList.remove(i);
	                        N--;
	                    }
	                }
	            }
	            if (N > 1) {
	                ResolveInfo.DisplayNameComparator rComparator =
	                        new ResolveInfo.DisplayNameComparator(mPm);
	                Collections.sort(currentResolveList, rComparator);
	            }
	            // First put the initial items at the top.
	            if (mInitialIntents != null) {
	                for (int i=0; i<mInitialIntents.length; i++) {
	                    Intent ii = mInitialIntents[i];
	                    if (ii == null) {
	                        continue;
	                    }
	                    ActivityInfo ai = ii.resolveActivityInfo(
	                    		getPackageManager(), 0);
	                    if (ai == null) {
	                        Log.w("ResolverActivity", "No activity found for "
	                                + ii);
	                        continue;
	                    }
	                    ResolveInfo ri = new ResolveInfo();
	                    ri.activityInfo = ai;
	                    if (ii instanceof LabeledIntent) {
	                        LabeledIntent li = (LabeledIntent)ii;
	                        ri.resolvePackageName = li.getSourcePackage();
	                        ri.labelRes = li.getLabelResource();
	                        ri.nonLocalizedLabel = li.getNonLocalizedLabel();
	                        ri.icon = li.getIconResource();
	                    }
	                    mList.add(new DisplayResolveInfo(ri,
	                            ri.loadLabel(getPackageManager()), null, ii));
	                }
	            }

	            // Check for applications with same name and use application name or
	            // package name if necessary
	            r0 = currentResolveList.get(0);
	            int start = 0;
	            CharSequence r0Label =  r0.loadLabel(mPm);
	            mShowExtended = false;
	            for (int i = 1; i < N; i++) {
	                if (r0Label == null) {
	                    r0Label = r0.activityInfo.packageName;
	                }
	                ResolveInfo ri = currentResolveList.get(i);
	                CharSequence riLabel = ri.loadLabel(mPm);
	                if (riLabel == null) {
	                    riLabel = ri.activityInfo.packageName;
	                }
	                if (riLabel.equals(r0Label)) {
	                    continue;
	                }
	                processGroup(currentResolveList, start, (i-1), r0, r0Label);
	                r0 = ri;
	                r0Label = riLabel;
	                start = i;
	            }
	            // Process last group
	            processGroup(currentResolveList, start, (N-1), r0, r0Label);
	        }
	    }

	    private void processGroup(List<ResolveInfo> rList, int start, int end, ResolveInfo ro,
	            CharSequence roLabel) {
	        // Process labels from start to i
	        int num = end - start+1;
	        if (num == 1) {
	            // No duplicate labels. Use label for entry at start
	            mList.add(new AuroraResolverActivity.DisplayResolveInfo(ro, roLabel, null, null));
	        } else {
	        	mShowExtended = true;
	            boolean usePkg = false;
	            CharSequence startApp = ro.activityInfo.applicationInfo.loadLabel(mPm);
	            if (startApp == null) {
	                usePkg = true;
	            }
	            if (!usePkg) {
	                // Use HashSet to track duplicates
	                HashSet<CharSequence> duplicates =
	                    new HashSet<CharSequence>();
	                duplicates.add(startApp);
	                for (int j = start+1; j <= end ; j++) {
	                    ResolveInfo jRi = rList.get(j);
	                    CharSequence jApp = jRi.activityInfo.applicationInfo.loadLabel(mPm);
	                    if ( (jApp == null) || (duplicates.contains(jApp))) {
	                        usePkg = true;
	                        break;
	                    } else {
	                        duplicates.add(jApp);
	                    }
	                }
	                // Clear HashSet for later use
	                duplicates.clear();
	            }
	            for (int k = start; k <= end; k++) {
	                ResolveInfo add = rList.get(k);
	                if (usePkg) {
	                    // Use application name for all entries from start to end-1
	                    mList.add(new DisplayResolveInfo(add, roLabel,
	                            add.activityInfo.packageName, null));
	                } else {
	                    // Use package name for all entries from start to end-1
	                    mList.add(new DisplayResolveInfo(add, roLabel,
	                            add.activityInfo.applicationInfo.loadLabel(mPm), null));
	                }
	            }
	        }
	        }
	
    

    Drawable getIcon(Resources res, int resId) {
        Drawable result;
        try {
            result = res.getDrawableForDensity(resId, mIconDpi);
        } catch (Resources.NotFoundException e) {
            result = null;
        }

        return result;
    }

    Drawable loadIconForResolveInfo(ResolveInfo ri) {
        Drawable dr = null;
        try {
//            if (ri.resolvePackageName != null && ri.icon != 0) {
//                dr = getIcon(mPm.getResourcesForApplication(ri.resolvePackageName), ri.icon);
//            }
//            final int iconRes = ri.getIconResource();
//            if (iconRes != 0) {
//                dr = getIcon(mPm.getResourcesForApplication(ri.activityInfo.packageName), iconRes);
//            }
//            if (dr != null) {
//            	dr = Utils2Icon.getInstance(this).getSystemIconDrawable(dr, Utils2Icon.OUTER_NONE_SHADOW_WITH_BOTTOM);
//            	return dr;
//            }
        	dr = Utils2Icon.getInstance(this).getIconDrawable(ri, Utils2Icon.INTER_SHADOW);
        	if(dr != null){
        		return dr;
        	}
        } catch (Exception e) {
            Log.e(TAG, "Couldn't find resources for package", e);
        }
        return ri.loadIcon(mPm);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!mRegistered) {
            mPackageMonitor.register(this, getMainLooper(), false);
            mRegistered = true;
        }
//        mAdapter.handlePackagesChanged();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRegistered) {
            mPackageMonitor.unregister();
            mRegistered = false;
        }
        if ((getIntent().getFlags()&Intent.FLAG_ACTIVITY_NEW_TASK) != 0) {
            // This resolver is in the unusual situation where it has been
            // launched at the top of a new task.  We don't let it be added
            // to the recent tasks shown to the user, and we need to make sure
            // that each time we are launched we get the correct launching
            // uid (not re-using the same resolver from an old launching uid),
            // so we will now finish ourself since being no longer visible,
            // the user probably can't get back to us.
            if (!isChangingConfigurations()) {
//                finish();
            }
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (mAlwaysUseOption) {/*
            final int checkedPos = mGrid.getCheckedItemPosition();
            final boolean enabled = checkedPos != GridView.INVALID_POSITION;
            mLastSelected = checkedPos;
            mAlwaysButton.setEnabled(enabled);
            mOnceButton.setEnabled(enabled);
            if (enabled) {
                mGrid.setSelection(checkedPos);
            }
        */}
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
/*        final int checkedPos = mGrid.getCheckedItemPosition();
        final boolean hasValidSelection = checkedPos != GridView.INVALID_POSITION;
      
        if (mAlwaysUseOption && (!hasValidSelection || mLastSelected != checkedPos)) {
            mAlwaysButton.setEnabled(hasValidSelection);
            mOnceButton.setEnabled(hasValidSelection);
            if (hasValidSelection) {
                mGrid.smoothScrollToPosition(checkedPos);
            }
            mLastSelected = checkedPos;
        } else {
            startSelected(position, false);
        }*/
    }

    public void onButtonClick(View v) {
		/*
        final int id = v.getId();
        startSelected(mGrid.getCheckedItemPosition(), id == R.id.button_always);
        dismiss();
        */
       auroraOnButtonClick(v); 
    }

    void startSelected(int which, boolean always) {
//        ResolveInfo ri = mAdapter.resolveInfoForPosition(which);
//        Intent intent = mAdapter.intentForPosition(which);
//        onIntentSelected(ri, intent, always);
        finish();
    }
    void startSelected(ResolveInfo ri,Intent intent){
    	onIntentSelected(ri, intent, mAlwaysCheckBox.isChecked());
    	finish();
    }

    protected void onIntentSelected(ResolveInfo ri, Intent intent, boolean alwaysCheck) {
        if (alwaysCheck) {
            // Build a reasonable intent filter, based on what matched.
            IntentFilter filter = new IntentFilter();

            if (intent.getAction() != null) {
                filter.addAction(intent.getAction());
            }
            Set<String> categories = intent.getCategories();
            if (categories != null) {
                for (String cat : categories) {
                    filter.addCategory(cat);
                }
            }
            filter.addCategory(Intent.CATEGORY_DEFAULT);

            int cat = ri.match&IntentFilter.MATCH_CATEGORY_MASK;
            Uri data = intent.getData();
            if (cat == IntentFilter.MATCH_CATEGORY_TYPE) {
                String mimeType = intent.resolveType(this);
                if (mimeType != null) {
                    try {
                        filter.addDataType(mimeType);
                    } catch (IntentFilter.MalformedMimeTypeException e) {
                        Log.w("ResolverActivity", e);
                        filter = null;
                    }
                }
            }
            if (data != null && data.getScheme() != null) {
                // We need the data specification if there was no type,
                // OR if the scheme is not one of our magical "file:"
                // or "content:" schemes (see IntentFilter for the reason).
                if (cat != IntentFilter.MATCH_CATEGORY_TYPE
                        || (!"file".equals(data.getScheme())
                                && !"content".equals(data.getScheme()))) {
                    filter.addDataScheme(data.getScheme());

                    // Look through the resolved filter to determine which part
                    // of it matched the original Intent.
                    Iterator<IntentFilter.AuthorityEntry> aIt = ri.filter.authoritiesIterator();
                    if (aIt != null) {
                        while (aIt.hasNext()) {
                            IntentFilter.AuthorityEntry a = aIt.next();
                            if (a.match(data) >= 0) {
                                int port = a.getPort();
                                filter.addDataAuthority(a.getHost(),
                                        port >= 0 ? Integer.toString(port) : null);
                                break;
                            }
                        }
                    }
                    Iterator<PatternMatcher> pIt = ri.filter.pathsIterator();
                    if (pIt != null) {
                        String path = data.getPath();
                        while (path != null && pIt.hasNext()) {
                            PatternMatcher p = pIt.next();
                            if (p.match(path)) {
                                filter.addDataPath(p.getPath(), p.getType());
                                break;
                            }
                        }
                    }
                }
            }

            if (filter != null) {
                final int N = mList.size();
                ComponentName[] set = new ComponentName[N];
                int bestMatch = 0;
                for (int i=0; i<N; i++) {
                    ResolveInfo r = mList.get(i).ri;
                    set[i] = new ComponentName(r.activityInfo.packageName,
                            r.activityInfo.name);
                    if (r.match > bestMatch) bestMatch = r.match;
                }
                getPackageManager().addPreferredActivity(filter, bestMatch, set,
                        intent.getComponent());
            }
        }

        if (intent != null) {
			startActivity(intent);
        }
    }

    void showAppDetails(ResolveInfo ri) {
        Intent in = new Intent().setAction("android.settings.APPLICATION_DETAILS_SETTINGS")
                .setData(Uri.fromParts("package", ri.activityInfo.packageName, null))
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        startActivity(in);
    }

     final static class DisplayResolveInfo {
        ResolveInfo ri;
        CharSequence displayLabel;
        Drawable displayIcon;
        CharSequence extendedInfo;
        Intent origIntent;

        DisplayResolveInfo(ResolveInfo pri, CharSequence pLabel,
                CharSequence pInfo, Intent pOrigIntent) {
            ri = pri;
            displayLabel = pLabel;
            extendedInfo = pInfo;
            origIntent = pOrigIntent;
        }
    }

    
	
//    class ItemLongClickListener implements AdapterView.OnItemLongClickListener {
//
//        @Override
//        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//            ResolveInfo ri = mAdapter.resolveInfoForPosition(position);
//            showAppDetails(ri);
//            return true;
//        }
//
//    }
    
     /************************************************************
     * 															*
     * 															*				
     * 				IUNI add for shared control					*
     * 															*
     * 															*	
     * ************************************************************/
	public int mPosition;
	
	public boolean myAlwaysUseOption = false;
	
	public int mImageSize;
	
	public boolean mChangedNaviBar = false;
	
    @Override
    protected void onResume()
    {
		super.onResume();
		
		initAll();
	}
    
    @Override
    protected void onPause()
    {
		super.onPause();
		
		auroraOnPause();
	}
	
	
	public void auroraOnPause()
	{
		resumeNaviBarAttrs();
		
		//finish();
	}
	
	public void showNaviBar(){
	}
	
	public void notAllowHideNaviBar(){
		
	}
	
	public void resumeNaviBarAttrs(){	
	}
	
    public void initAll() {
    	resetContentPadding();
    } 
    
    public void resetWindowAttributes(){/*
    	android.view.WindowManager.LayoutParams lp = getWindow().getAttributes();
    
    
    	int screenWidth  = getWindowManager().getDefaultDisplay().getWidth(); 
    	
    	lp.width = screenWidth - getResources().getInteger(com.aurora.internal.R.integer.auroraSharedWindowPaddings);
		
    	if(mAdapter != null && getResources().getConfiguration().orientation == android.content.res.Configuration.ORIENTATION_PORTRAIT){
			final int maxHeightPx = (int)(getResources().getDimension(com.aurora.R.dimen.aurora_resolver_window_max_height));
			
			final int size = mAdapter.getCount();
			
			final int lines = (size-1)/mMaxColumns + 1;
			
			if( lines > 2 ){
				lp.height = maxHeightPx;
			}
		}
    	
    	getWindow().setAttributes(lp);
    	
    */}
    
    public void resetContentPadding(){
    	View parent = getWindow().findViewById(com.aurora.R.id.aurora_custom);
    	
    	parent.setPadding(0, 0, 0, 0);
    	
    }
    
    public void setMaxColumns()	 {
		mMaxColumns = getResources().getInteger(com.aurora.internal.R.integer.auroraSharedGridViewMaxColumns);
	}
	
	
	public void setImageSize(){
		mImageSize = mIconSize + 12;
	}
	
	public View getResolverView(){
		return getLayoutInflater().inflate(com.aurora.R.layout.aurora_resolver_grid, null);
	}
	
	public GridView getGridView(View v){
		if(v == null)return null;
		
		return (GridView)v.findViewById(com.aurora.R.id.aurora_resolver_grid); 
	}
	
	
	public void auroraOnButtonClick(View v) {
		
        final int id = v.getId();
        
       // startSelected(mGrid.getCheckedItemPosition(), id == com.aurora.R.id.aurora_button_always);
      
        dismiss();
    }
	
	public android.graphics.Bitmap drawableToBitmap(Drawable drawable, int alpha) {       
		
		if(drawable == null)return null;
		
		int bitmapWidth = mImageSize;
		
		int bitmapHeight = mImageSize;
		
        android.graphics.Bitmap bitmap = android.graphics.Bitmap.createBitmap(

                                        bitmapWidth,

                                        bitmapHeight,

                                        drawable.getOpacity() != android.graphics.PixelFormat.OPAQUE ?
                                        				android.graphics.Bitmap.Config.ARGB_8888

                                                        : android.graphics.Bitmap.Config.RGB_565
                                                        );

        android.graphics.Canvas canvas = new android.graphics.Canvas(bitmap);
		
        drawable.setBounds(0, 0, bitmapWidth, bitmapHeight);
        
        drawable.setAlpha(alpha);
        
        drawable.draw(canvas);

        return bitmap;
	}
	

	public android.graphics.Bitmap getRoundedCornerBitmap(android.graphics.Bitmap bitmap) {
		if(bitmap == null)return null;
		
		int bitmapWidth = mImageSize;
		
		int bitmapHeight = mImageSize;
		
	    android.graphics.Bitmap output = android.graphics.Bitmap.createBitmap(bitmapWidth,
	        bitmapHeight, android.graphics.Bitmap.Config.ARGB_8888);
	        
	    android.graphics.Canvas canvas = new android.graphics.Canvas(output);
	 
	    final android.graphics.Paint paint = new android.graphics.Paint();
	    
	    final android.graphics.Rect rect = new android.graphics.Rect(0, 0, bitmapWidth, bitmapHeight);
	    
	    final android.graphics.RectF rectF = new android.graphics.RectF(rect);
	    
	    final float roundPx = 10;
	 
	    paint.setAntiAlias(true);
	    
	    canvas.drawARGB(0, 0, 0, 0);
	    
	    canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
	 
	    paint.setXfermode(new android.graphics.PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC_IN));
	    
	    canvas.drawBitmap(bitmap, rect, rect, paint);
	 
	    return output;
	}
	
	public android.graphics.drawable.StateListDrawable getImageDrawable(Drawable src){
		android.graphics.drawable.StateListDrawable auroraImageDrawable = new android.graphics.drawable.StateListDrawable();
		
		android.graphics.Bitmap auroraImageBitmapPressed = getRoundedCornerBitmap(drawableToBitmap(src,100));
		
		android.graphics.Bitmap auroraImageBitmap = getRoundedCornerBitmap(drawableToBitmap(src,255));
		
		auroraImageDrawable.addState(new int[]{android.R.attr.state_pressed}, new android.graphics.drawable.BitmapDrawable(auroraImageBitmapPressed));
		
		auroraImageDrawable.addState(new int[]{0}, new android.graphics.drawable.BitmapDrawable(auroraImageBitmap));
		
		return auroraImageDrawable;
	}
	
	public void setImageDrawable(ImageView icon,Drawable src){
		if(icon == null) return;
		if(icon.getDrawable() == null || icon.getDrawable() != src){
			icon.setImageDrawable(getImageDrawable(src));
		}
	}
	
	
	public ViewGroup getButtomBarLayout(){
		return (ViewGroup) findViewById(com.aurora.R.id.aurora_button_bar);
	}
	
	public void setButtonIds(){
		 ViewGroup buttonLayout = getButtomBarLayout();
		 
		 mAlwaysCheckBox = (AuroraCheckBox) buttonLayout.findViewById(android.R.id.text1);
		 
		 mAlwaysButton = (Button) buttonLayout.findViewById(com.aurora.R.id.aurora_button_always);
		 
         mOnceButton = (Button) buttonLayout.findViewById(com.aurora.R.id.aurora_button_once);
	}
	
	public void showCancelButton(){
		com.aurora.internal.app.AuroraAlertController.AlertParams ap = mAlertParams;
		
		if(ap != null && ap.mView != null){
			Button auroracancel = (Button)ap.mView.findViewById(com.aurora.R.id.aurora_resolve_cancel_button); 
            
            if(auroracancel != null) {
				auroracancel.setOnClickListener(cancelLisener);
			
				auroracancel.setVisibility(View.GONE);
			}
		}
	}
	
	public void initGridView(){
		com.aurora.internal.app.AuroraAlertController.AlertParams ap = mAlertParams;
		
        //reset
        ap.mView = mPagerGridView/*getResolverView()*/;
            
        //reset
//        mGrid = getGridView(ap.mView);
          
	}
	
	public void setAlwaysUseOption(){
		myAlwaysUseOption = mAlwaysUseOption;
	}
	
	public void setBottomBar(){

		   final ViewGroup buttonLayout = getButtomBarLayout();
		   setButtonIds();
		if (myAlwaysUseOption)  {
            if (buttonLayout != null) {
            	mAlwaysCheckBox.setVisibility(View.VISIBLE);
            	mAlwaysCheckBox.setChecked(true);
            } else {
                mAlwaysUseOption = false;
            }
        }
        else {
        	mAlwaysCheckBox.setVisibility(View.GONE);
		}
	}
	
	public AuroraClickListener cancelLisener = new AuroraClickListener();
	
	private class AuroraClickListener implements android.widget.Button.OnClickListener{
		public AuroraClickListener(){
			
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			finish();
		}
				
	}



	@Override
	public void onSelected(int position, ResolveInfo resolverInfo, Intent intent) {
		// TODO Auto-generated method stub
		startSelected(resolverInfo, intent);
	}

	@Override
	public void onLongSelected(int position, ResolveInfo resolverInfo,
			Intent intent) {
		// TODO Auto-generated method stub
      showAppDetails(resolverInfo);
	}
	
	
}

