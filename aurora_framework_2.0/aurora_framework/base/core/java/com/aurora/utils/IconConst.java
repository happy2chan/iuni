package com.aurora.utils;

import android.os.Environment;

public class IconConst {
	public static final String TAG = "IconGetter";
	//Local path
	public static String icon_dir_prefix = "/data/aurora/icons";
	public static String icon_sddir_prefix ;
	public static String inter_icon_dir="/data/aurora/icons/inter_icons/";
	public static String outer_icon_dir="/data/aurora/icons/outer_icons/";
	public static String inter_icon_sddir=icon_sddir_prefix+"/inter_icons/";
	public static String outer_icon_sddir=icon_sddir_prefix+"/outer_icons/";
	//Launcher RES version info
	public static final String ICON_VERSION_KEY = "icon_version";
	public static final String ICON_VERSION_FILENAME = "/version.config";
	//Drawable Names:mask,shadow&& 3rd app bg.
	public static final String BACKGROUND_SCALE = "bg_scale";
	public static final String BACKGROUND = "bg";
	public static final String SHADOW = "shadow";
	public static final String MASK = "mask";
	public static final String BACKGROUND_SCALE_3RD = "bg_scale3rd";
	public static final String BACKGROUND3RD = "bg3rd";
	public static final String SHADOW3RD = "shadow3rd";
	public static final String MASK3RD = "mask3rd";
	public static final String VISIBLE_CIRCLE_ICONSIZE = "visible_circle_icon_size";
	public static final String VISIBLE_REC_ICONSIZE = "visible_rec_icon_size";
	public static final String FINALICONSZIE = "final_icon_size";
	
	static{
		inter_icon_dir="/data/aurora/icons/inter_icons/";
		outer_icon_dir="/data/aurora/icons/outer_icons/";
		icon_sddir_prefix = Environment.getExternalStorageDirectory().getPath()+"/icons";
		inter_icon_sddir=icon_sddir_prefix+"/inter_icons/";
		outer_icon_sddir=icon_sddir_prefix+"/outer_icons/";
		Log.d(TAG,"Launcher Res Local Pathes:\n"
				+icon_dir_prefix+"\n"
				+inter_icon_dir+"\n"
				+icon_sddir_prefix+"\n"
				+inter_icon_sddir+"\n"
				+outer_icon_sddir);
	}
}
