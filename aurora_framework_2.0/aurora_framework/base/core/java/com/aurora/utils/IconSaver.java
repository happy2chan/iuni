package com.aurora.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import android.graphics.Bitmap;

public class IconSaver {
	public static final String TAG = IconSaver.class.getSimpleName();
	private ExecutorService mThreadPool;
	public IconSaver(){
		initThreadPool();
	}
	private void initThreadPool() {
		mThreadPool = Executors.newCachedThreadPool();
	}
	
	public void saveIcon(Bitmap bitmap,String path,String iconName){
		mThreadPool.execute(new SaveIconRunnable(bitmap, path, iconName));
	}
	
	class SaveIconRunnable implements Runnable{
		Bitmap savingBitmap = null;
		String iconName;
		String iconPath;
		public SaveIconRunnable(Bitmap bitmap,String path,String iconName){
			savingBitmap = bitmap;
			this.iconName=iconName;
			this.iconPath=path;
		}
		@Override
		public void run() {
			if(savingBitmap ==null||iconPath==null||iconName==null){
				Log.d(TAG,"The bitmap,iconPath and iconName must be not null.");
				return;
			}
			saveBitMap(savingBitmap,iconPath,iconName);
		}
		public void saveBitMap(Bitmap savingBitmap, String iconPath, String iconName){
			File f = new File(iconPath + iconName);
			FileOutputStream fOut = null;
			try {
				f.createNewFile();
				fOut = new FileOutputStream(f);
				Log.i(TAG,"bitmap = "+savingBitmap+"  ,  fOut = "+fOut);
				savingBitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}catch(Exception e){
				Log.d(TAG,"saveBitMap ------->",e);
			}finally{
				try {
					fOut.flush();
					fOut.close();
				} catch (IOException e) {
					//e.printStackTrace();
				}
			}
		}
	}
}
