package com.aurora.utils;

import android.util.Log;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

public class SystemUtils {
	/**
	 * Uri for change statusbar colors
	 */
	public static final Uri STATUS_BAR_COLOR_URI = Uri.parse("content://"+"com.android.systemui.statusbar.phone"+"/"+"immersion"+"?notify=true");
	
	/**
	 * key for stored status bar color in SystemUi process
	 */
	public static final String KEY_FOR_STATUS_BAR_COLOR = "imersion_color";
	
	/**
	 * used this value to change status bar icon's color to white
	 */
	private static final int STATUS_BAR_WHITE = 0;
	/**
	 * used this value to change status bar icon's color to black
	 */
	private static final int STATUS_BAR_BLACK = 1;
	
	/**
	 * in this mode ,icons in status bar will change to white 
	 */
	public static final int STATUS_BAR_MODE_WHITE = 0;
	
	/**
	 * in this mode ,icons in status bar will change to black
	 */
	public static final int STATUS_BAR_MODE_BLACK = 1;
	/**
	 * get current sdk version
	 * @return
	 */
	public static int getCurrentVersion(){
		
		return android.os.Build.VERSION.SDK_INT;
	}
	
	public static String getMode(){
		return android.os.Build.MODEL;
	}

	/**
	 * 将drawable转换为bitmaps
	 * @param drawable
	 * @return
	 */
	public static Bitmap drawableToBitmap(Drawable drawable,int width,int height){
		 Bitmap bitmap = Bitmap.createBitmap(width, height, drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);    
	       Canvas canvas = new Canvas(bitmap);    
	       drawable.setBounds(0, 0, width, height);    
	       drawable.draw(canvas);    
	       return bitmap;    
	}
	
	public static Bitmap drawableToBitmap(int resID,Context context,int width,int height){
		return drawableToBitmap(context.getResources().getDrawable(resID),width,height);
	}
	 
	 
	 private static int indexOfViewInParent(View view, ViewGroup parent)
	 {
	         int index;
	         for (index = 0; index < parent.getChildCount(); index++)
	         {
	                 if (parent.getChildAt(index) == view)
	                         break;
	         }
	         return index;
	}
	 
		private static void updateStatusBarColor(int colorType,Context context){
			ContentResolver cr = context.getContentResolver();
	        ContentValues values = new ContentValues();
	        values.put(KEY_FOR_STATUS_BAR_COLOR,Integer.toHexString(colorType) );
	        int count = 0;
	        try{
	        count = cr.update(STATUS_BAR_COLOR_URI, values, null, null);
	        if(count==0){
	        	cr.insert(STATUS_BAR_COLOR_URI, values);
	        }
	        }catch(Exception ex){
	        	Log.e("SystemUtils", "catched update statusbar color exception-->"+ex);
	        }
		}
		
	 /**
	  * @since Android L
	  * @param mode SystemUtils.STATUS_BAR_MODE_BLACK to change icons to black ,
	  *                            SystemUtils.STATUS_BAR_MODE_WHITE is white
	  * @param context
	  */
	 public static void  switchStatusBarColorMode(int mode,Context context){
		   int colorType = STATUS_BAR_WHITE;
	         if(mode == STATUS_BAR_MODE_BLACK){
	        	 colorType = STATUS_BAR_BLACK;
	         }
	         updateStatusBarColor(colorType, context);
	 }
	 
	 /**
	  * @since Android L
	  * set status bar background to transparent,and activity will layout for fullscreen mode
	  * @param activity
	  */
	  public static void setStatusBarBackgroundTransparent(Activity activity){
			Window window =activity.getWindow();  
	        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS  
	                | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);  
	        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN  
	                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);  
	        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);  
	        window.setStatusBarColor(Color.TRANSPARENT); 
	   }
	
}
