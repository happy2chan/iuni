package gionee.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.telephony.MSimTelephonyManager;
import android.telephony.TelephonyManager;
import android.os.ServiceManager;
import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.Phone;
import android.os.SystemClock;
import com.android.internal.telephony.msim.ITelephonyMSim;

public class GnConnectivityManager {
    //Gionee guoyx 20130328 added for CR00787716 begin
    private static int backupDataEnabled = -1;
	//Gionee guoyx 20130328 added for CR00787716 end
    public static int startUsingNetworkFeatureGemini(ConnectivityManager ConnMgr, int slotId) {
//        ConnMgr.startUsingNetworkFeatureGemini(
//                ConnectivityManager.TYPE_MOBILE, GnPhone.FEATURE_ENABLE_MMS, slotId);
        //Gionee guoyx 20130328 added for CR00787716 begin
        boolean dataEnabled = getMobileDataEnabledGemini(ConnMgr, 0L);
        Log.d("Mms/Txn", "GnConnectivityManager: GnConnectivityManager."); 
        long startTime=0;
        if(slotId==0)
        {
           if(!dataEnabled)
           {
             backupDataEnabled = 0;
             setMobileDataEnabledGemini(ConnMgr, true);
           } 
        }
        else if(slotId==1)
        {
    
          if(dataEnabled)
          {
            backupDataEnabled=0;
            setMobileDataEnabledGemini(ConnMgr, false);
          }
          
          Log.d("Mms/Txn", "setMobileDataEnabledGemini: dataEnabled="+dataEnabled);  
          if(MSimTelephonyManager.getDefault().getSimState(0)==TelephonyManager.SIM_STATE_ABSENT&&
                  MSimTelephonyManager.getDefault().getSimState(1)==TelephonyManager.SIM_STATE_READY)
              {
                try{
                 ITelephonyMSim itel=ITelephonyMSim.Stub.asInterface(
                   ServiceManager.checkService("phone_msim"));
                 itel.enableApnType(PhoneConstants.APN_TYPE_DEFAULT);
                     }catch(Exception e){}
              }
          MSimTelephonyManager.getDefault().setPreferredDataSubscription(slotId);
          setMobileDataEnabledGemini(ConnMgr, true);
           startTime= SystemClock.elapsedRealtime();
          while(true)
                 {
                   try{
                   Thread.sleep(300);
                   Log.d("Mms/Txn", "setMobileDataEnabledGemini: sleep="); 
                 }catch(Exception e){
                   Log.d("Mms/Txn", "setMobileDataEnabledGemini: Exception="+e);  
                  }
                   if(ConnMgr.getMobileDataEnabled())
                               break;
                   if(SystemClock.elapsedRealtime()-startTime>10000)
                         break;
                 }        
        } 
		//Gionee guoyx 20130328 added for CR00787716 end
        int result=-1;
        startTime = SystemClock.elapsedRealtime();
        while(true)
         {
         result=ConnMgr.startUsingNetworkFeature(
                ConnectivityManager.TYPE_MOBILE, Phone.FEATURE_ENABLE_MMS);
         if(result==0)
             break;
         else
           {
               try{
                   Thread.sleep(300);
              }catch(Exception e){}
           }
         if(SystemClock.elapsedRealtime()-startTime>10000)
                     break;     
         }
        Log.d("Mms/Txn", "setMobileDataEnabledGemini: result="+result); 
        return result;
    }
    
    public static int stopUsingNetworkFeatureGemini(ConnectivityManager ConnMgr, int slotId) {
//        ConnMgr.stopUsingNetworkFeatureGemini(
//                ConnectivityManager.TYPE_MOBILE, GnPhone.FEATURE_ENABLE_MMS, slotId);
        //Gionee guoyx 20130328 modified for CR00787716 begin
        int result = -1;
        result = ConnMgr.stopUsingNetworkFeature(
                ConnectivityManager.TYPE_MOBILE, Phone.FEATURE_ENABLE_MMS);
        if(slotId==0)
        {
           if(backupDataEnabled==0)
           {
              backupDataEnabled=-1;
              setMobileDataEnabledGemini(ConnMgr, false);
           }

        }
        else if(slotId==1)
        {
           Log.d("Mms/Txn", "setMobileDataEnabledGemini: stopUsingNetworkFeatureGemini");            
           MSimTelephonyManager.getDefault().setPreferredDataSubscription(0); 
           setMobileDataEnabledGemini(ConnMgr, false); 
           if(MSimTelephonyManager.getDefault().getSimState(0)==TelephonyManager.SIM_STATE_ABSENT&&
                  MSimTelephonyManager.getDefault().getSimState(1)==TelephonyManager.SIM_STATE_READY)
              {
                try{
                 ITelephonyMSim itel=ITelephonyMSim.Stub.asInterface(
                   ServiceManager.checkService("phone_msim"));
                 itel.disableApnType(PhoneConstants.APN_TYPE_DEFAULT);
                     }catch(Exception e){}
              }
           Log.d("Mms/Txn", "setMobileDataEnabledGemini: backupDataEnabled="+backupDataEnabled);   
           if(backupDataEnabled==0)
            {
               backupDataEnabled=-1;
               setMobileDataEnabledGemini(ConnMgr, true);
            }


        } 
/*        MSimTelephonyManager.getDefault().setPreferredDataSubscription(0);
        if (backupDataEnabled == 0) {
            Log.d("Mms/Txn", "stopUsingNetworkFeatureGemini: backup data is closed! will close it.");
            backupDataEnabled = -1;
            if(slotId==1)
            {
              if(MSimTelephonyManager.getDefault().getSimState(0)!=TelephonyManager.SIM_STATE_READY&&
                  MSimTelephonyManager.getDefault().getSimState(1)==TelephonyManager.SIM_STATE_READY)
              {
                try{
                 ITelephony itel=ITelephony.Stub.asInterface(
                   ServiceManager.checkService(Context.TELEPHONY_SERVICE));
                 itel.disableApnType(PhoneConstants.APN_TYPE_DEFAULT);
                     }catch(Exception e){}
              }
            }
            setMobileDataEnabledGemini(ConnMgr, false);
        }*/
      return result;
	  //Gionee guoyx 20130328 modified for CR00787716 end
  }

    public static boolean getMobileDataEnabledGemini(
            ConnectivityManager ConnMgr, long simId) {
        //return ConnMgr.getMobileDataEnabledGemini(simId);
		//Gionee guoyx 20130328 added for CR00787716 begin
        return ConnMgr.getMobileDataEnabled();
		//Gionee guoyx 20130328 added for CR00787716 end
    }
    
	//Gionee guoyx 20130328 added for CR00787716 begin
    public static void setMobileDataEnabledGemini(
            ConnectivityManager ConnMgr, boolean enabled) {
        //return ConnMgr.getMobileDataEnabledGemini(simId);
        ConnMgr.setMobileDataEnabled(enabled);
    }
	//Gionee guoyx 20130328 added for CR00787716 end
}
