package gionee.app;

import com.android.internal.statusbar.IStatusBarService;
import android.app.StatusBarManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Slog;

/**
 * this class not used should be deleted. 2012.08.04
 * @author guoyangxu
 *
 */
public class GnStatusBarManager /*extends StatusBarManager*/{

    private static final String TAG = "GnStatusBarManager";
    private static Context mContext;
    private IStatusBarService mService;
    
    GnStatusBarManager(Context context) {
        mContext = context;
    }
        
    
    private synchronized IStatusBarService getService() {
        if (mService == null) {
            mService = IStatusBarService.Stub.asInterface(
                    ServiceManager.getService(Context.STATUS_BAR_SERVICE));
            if (mService == null) {
                Slog.w("StatusBarManager", "warning: no STATUS_BAR_SERVICE");
            }
        }
        return mService;
    }
    
    //Gionee guoyangxu 20120618 add for support_MTK_mms_app
    public static void showSIMIndicator(StatusBarManager statusBar, ComponentName componentName, String businessType) {
    	statusBar.showSIMIndicator(componentName, businessType);
    }
    
    public static void hideSIMIndicator(StatusBarManager statusBar, ComponentName componentName) {
        statusBar.hideSIMIndicator(componentName);
    }

}
