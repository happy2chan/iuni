#
# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
BUILD_MODULE=qccm
ifeq ($(strip $(BUILD_MODULE)),MTK)
LOCAL_PATH := $(call my-dir)
# the library
# ============================================================
include $(CLEAR_VARS)

GN_FRAMEWORKS_BASE_SUBDIRS := \
	$(addsuffix /java, \
	    core \
	    drm \
	    telephony \
	 )

GN_FRAMEWORKS_BASE_JAVA_SRC_DIRS := \
	$(addprefix base/,$(GN_FRAMEWORKS_BASE_SUBDIRS))

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under,$(GN_FRAMEWORKS_BASE_JAVA_SRC_DIRS))

LOCAL_MODULE := gnframework-static

LOCAL_STATIC_JAVA_LIBRARIES := CellConnUtil

LOCAL_JAVA_LIBRARIES += mediatek-framework
LOCAL_JAVA_LIBRARIES += mms-common

include $(BUILD_STATIC_JAVA_LIBRARY)
endif
ifeq ($(strip $(TARGET_PRODUCT)),gionee89_dwe_jb2)
LOCAL_PATH := $(call my-dir)
# the library
# ============================================================
include $(CLEAR_VARS)

GN_FRAMEWORKS_BASE_SUBDIRS := \
	$(addsuffix /java, \
	    core \
	    drm \
	    telephony \
	 )

GN_FRAMEWORKS_BASE_JAVA_SRC_DIRS := \
	$(addprefix base_google/,$(GN_FRAMEWORKS_BASE_SUBDIRS))

LOCAL_SRC_FILES := $(call all-java-files-under,$(GN_FRAMEWORKS_BASE_JAVA_SRC_DIRS))

# AIDL Files Include
# LOCAL_SRC_FILES += \
#        base/core/java/com/android/internal/theme/IThemeManagerService.aidl \

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := gnframework-static

#LOCAL_JAVA_LIBRARIES += qcnvitems qcrilhook

# LOCAL_STATIC_JAVA_LIBRARIES := com.mediatek.CellConnUtil

LOCAL_JAVA_LIBRARIES += telephony-common mediatek-framework

include $(BUILD_STATIC_JAVA_LIBRARY)


else
    ifeq ($(strip $(GN_PROJECT)),NBL8910A01_A_IUNI)
	LOCAL_PATH := $(call my-dir)
	# the library
	# ============================================================
	include $(CLEAR_VARS)

	GN_FRAMEWORKS_BASE_SUBDIRS := \
	$(addsuffix /java, \
	core \
	drm \
	telephony \
	)

	GN_FRAMEWORKS_BASE_JAVA_SRC_DIRS := \
	$(addprefix base_qccm_dualsim/,$(GN_FRAMEWORKS_BASE_SUBDIRS))

	LOCAL_SRC_FILES := $(call all-java-files-under,$(GN_FRAMEWORKS_BASE_JAVA_SRC_DIRS))

	# AIDL Files Include
	# LOCAL_SRC_FILES += \
	#        base/core/java/com/android/internal/theme/IThemeManagerService.aidl \

	LOCAL_MODULE_TAGS := optional
	LOCAL_MODULE := gnframework-static

	LOCAL_JAVA_LIBRARIES += qcnvitems qcrilhook

	# LOCAL_STATIC_JAVA_LIBRARIES := com.mediatek.CellConnUtil

	LOCAL_JAVA_LIBRARIES += telephony-common telephony-msim

	include $(BUILD_STATIC_JAVA_LIBRARY)
    else
	ifeq ($(strip $(TARGET_PRODUCT)),full_gionee6752_lwt_l)
		LOCAL_PATH := $(call my-dir)
		# the library
		# ============================================================
		include $(CLEAR_VARS)

		GN_FRAMEWORKS_BASE_SUBDIRS := \
			$(addsuffix /java, \
			    core \
			    drm \
			    telephony \
			 )

		GN_FRAMEWORKS_BASE_JAVA_SRC_DIRS := \
			$(addprefix base_mtk_5.0/,$(GN_FRAMEWORKS_BASE_SUBDIRS))

		LOCAL_SRC_FILES := $(call all-java-files-under,$(GN_FRAMEWORKS_BASE_JAVA_SRC_DIRS))

		# AIDL Files Include
		# LOCAL_SRC_FILES += \
		#        base/core/java/com/android/internal/theme/IThemeManagerService.aidl \

		LOCAL_MODULE_TAGS := optional
		LOCAL_MODULE := gnframework-static

		# LOCAL_STATIC_JAVA_LIBRARIES := com.mediatek.CellConnUtil

		LOCAL_JAVA_LIBRARIES += telephony-common mediatek-framework

		include $(BUILD_STATIC_JAVA_LIBRARY)
	    else
		LOCAL_PATH := $(call my-dir)
		# the library
		# ============================================================
		include $(CLEAR_VARS)

		GN_FRAMEWORKS_BASE_SUBDIRS := \
			$(addsuffix /java, \
			    core \
			    drm \
			    telephony \
			 )

		GN_FRAMEWORKS_BASE_JAVA_SRC_DIRS := \
			$(addprefix base_qccm/,$(GN_FRAMEWORKS_BASE_SUBDIRS))

		LOCAL_SRC_FILES := $(call all-java-files-under,$(GN_FRAMEWORKS_BASE_JAVA_SRC_DIRS))

		# AIDL Files Include
		# LOCAL_SRC_FILES += \
		#        base/core/java/com/android/internal/theme/IThemeManagerService.aidl \

		LOCAL_MODULE_TAGS := optional
		LOCAL_MODULE := gnframework-static

		LOCAL_JAVA_LIBRARIES += qcnvitems qcrilhook

		# LOCAL_STATIC_JAVA_LIBRARIES := com.mediatek.CellConnUtil

		LOCAL_JAVA_LIBRARIES += telephony-common 

		include $(BUILD_STATIC_JAVA_LIBRARY)
  	endif
  endif
endif

